

<!doctype html>
<html lang="en">
<head>
    <title>{{ $title_header_global }}</title>
    <meta name="description" content="{{ $metta_header_global }}">
	<link href="https://demo.dashboardpack.com/architectui-html-pro/main.d810cf0ae7f39f28f336.css" rel="stylesheet"></head>

<body>
	<div class="overflow-hidden" id="form_register_customer">
		<div class="app-container app-theme-white body-tabs-shadow">
			<div class="app-container">
				<div class="h-100">
					<div class="h-100 no-gutters row">
						<div class="h-100 d-md-flex d-sm-block bg-white justify-content-center align-items-center col-md-12 col-lg-7">
							<div class="mx-auto app-login-box col-sm-12 col-md-10 col-lg-9" >
								<div>
									<img src="{{ url('/get/logo/from-storage/'.$get_logo_astech) }}" alt="" style="width:150px;height:auto">
								</div><br>
								<h4>
									<div>Welcome,</div>
									<span>It only takes a <span class="text-success">few seconds</span> to create your account</span>
								</h4>
									@if(session()->has('error'))
										<div class="alert alert-danger" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<strong>{{ session()->get('error') }} </strong>
										</div>
									@endif
									@if(session()->has('success'))
										<div class="alert alert-primary" role="alert">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<strong>{{ session()->get('success') }} </strong>
										</div>
									@endif
								<div>
										{{-- <div class="alert alert-danger" role="alert" id="alert_condition">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<strong>You must be Agree to our term and condition </strong>
										</div> --}}
									{{-- <form id="" class="col-md-10 mx-auto validate-form register-users"> --}}
									<form action="{{ url('/registers') }}" method="post">
										@csrf
										<div class="form-row">
											<div class="col-md-6">
												<div class="position-relative form-group">
													<label for="exampleName" class=""><span class="text-danger">*</span> Name</label>
													<input type="text" class="form-control" id="name" name="name" placeholder="First name" value="{{Request::old('name') }}">
													@error('name')
														<strong style="color:red">{{ $message }}</strong>
													@enderror
												</div>
												
											</div>
											<div class="col-md-6">
												<div class="position-relative form-group">
													<label for="exampleName" class="">Email</label>
													<input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{Request::old('email') }}">
													<span id="error-email" style="color:red"></span>
													@error('email')
														<strong style="color:red">{{ $message }}</strong>
													@enderror
												</div>
											</div>
											<div class="col-md-6">
												<div class="position-relative form-group">
													<label for="examplePassword" class=""><span class="text-danger">*</span> Password</label>
													<input type="password" class="form-control" id="password" name="password" placeholder="Password">
													@error('password')
														<strong style="color:red">{{ $message }}</strong>
													@enderror
												</div>
											</div>
											<div class="col-md-6">
												<div class="position-relative form-group">
													<label for="examplePasswordRep" class=""><span class="text-danger">*</span> Repeat Password</label>
													<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Password">
													@error('password_confirmation')
														<strong style="color:red">{{ $message }}</strong>
													@enderror
												</div>
											</div>
											<div class="col-md-6">
												<div class="position-relative form-group">
													<label for="examplePasswordRep" class=""><span class="text-danger">*</span> Phone</label>
													<input type="text" class="form-control phone mask_phoned" id="phone" name="phone" placeholder="Phone Number" value="{{Request::old('phone') }}">
													<span id="error-phone" style="color:red"></span>
													@error('phone')
														<strong style="color:red">{{ $message }}</strong>
													@enderror
												</div>
											</div>
											<div class="col-md-6">
												<div class="position-relative form-group">
													<label for="examplePasswordRep" class=""><span class="text-danger">*</span> Register As</label>
													<select id="role" class="form-control" name="role_id">
														<option value="3">Customer</option>
														<option value="2">Technician</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-4">
											<label for="ReCaptcha">Recaptcha:</label>
												{!! NoCaptcha::renderJs() !!}
												{!! NoCaptcha::display() !!}
											</div>
											
										  </div>
										  @error('g-recaptcha-response')
											<strong style="color:red">{{ $message }}</strong>
										@enderror
										<div class="form-group">
											<div>
												<div class="form-check">
													<input type="checkbox" name="agree" value="1" class="form-check-input"  />
													<a href="" class="form-check-label" data-toggle="modal" data-target="#exampleModalLong">Agree to our term and condition </a>
												</div>
											</div>
										</div>
										
										<div class="mt-4 d-flex align-items-center">
											<h5 class="mb-0">Already have an account? <a href="{{ url('/') }}" class="text-primary">Log in</a></h5>
											<div class="ml-auto">
												<button class="btn-wide btn-pill btn-shadow btn-hover-shine btn btn-primary btn-lg btn_loading" type="submit">Create Account</button>
												{{-- <button class="btn-wide btn-pill btn-shadow btn-hover-shine btn btn-primary btn-lg" id="btn_click" type="submit"> </button> --}}
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="d-lg-flex d-xs-none col-lg-5">
							<div class="slider-light">
								<div class="slick-slider slick-initialized">
									<div>
										<div class="position-relative h-100 d-flex justify-content-center align-items-center bg-premium-dark"
											tabindex="-1">
											<div class="slide-img-bg"
												style="background-image: url('{{ asset('image2.jpg')}}');"></div>
											<div class="slider-content">
												{{-- <h3>Scalable, Modular, Consistent</h3> --}}
												<p style="opacity: 0.0;">Easily exclude the components you don't require. Lightweight, consistent
													Bootstrap based styles across all elements and components
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
					

				</div>
			</div>
		</div>
	</div>

	

	<!-- Modal -->
	<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">{{ $getModalTitle->value }}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>{!! $getAgreement->value !!}</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
    
		<script src="{{ asset('main.js') }}"></script>
		@include('admin.template._mainScript')
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
		<script>
			$('#success_register').hide();
			$('#alert_condition').hide();
			$(document).ready(function(){
				$('.mask_phoned').mask('6280000000000');
			});

			$('.btn_loading').on('click', function(){
				Helper.loadingStart();
			})

			// $('.register-users').submit(function(e){
			// 		data = Helper.serializeForm($(this));
			// 		getAgree = $('input[name="agree"]:checked').serialize();
			// 		// alert(getAgree);
			// 		Helper.loadingStart();
			// 		// post data
			// 		Axios.post(Helper.apiUrl('/register-customer'), data)
			// 			.then(function(response) {
			// 				if(getAgree == 'agree=1'){
			// 					$('#form_register_customer').hide();
			// 					$('#success_register').show();
			// 					Helper.successNotif();
			// 					helper.loadingStop();
			// 					// window.location.href = Helper.redirectUrl('/user/register');
								
			// 				}else if(getAgree == ''){
			// 					// Helper.errorNotif('You must be Agree to our term and condition');
			// 					$('#alert_condition').show();
			// 					helper.loadingStop();
			// 				}
			// 			})
			// 			.catch(function(error) {
			// 				// helper.loadingStop();
			// 				console.log(error)
			// 				// Helper.handleErrorResponse(error)
			// 				Helper.loadingStop();
			// 				if(error.response.status == 422){
			// 					error = error.response.data.data;
								
			// 					_.each(error, function(pesan, field){
			// 						$('#error-'+ field).text(pesan[0])
			// 					})
			// 				}
							
			// 			});
			// 	e.preventDefault();
			// })

			

			// $(document).on('click', '#register', function(e) {
			// 	// id = $(this).attr('data-id');
			// 	Helper.confirm(function(){
			// 		Helper.loadingStart();
			// 		$.ajax({
			// 			url:Helper.apiUrl('/register-customer'),
			// 			type: 'post',
			// 			data:{
			// 				name : $('input[name="name"]').val(),
			// 				email : $('input[name="email"]').val(),
			// 				phone : $('input[name="phone"]').val(),
			// 				password : $('input[name="password"]').val(),
			// 				password_confirmation : $('input[name="password_confirmation"]').val(),
			// 				role_id : $('select[name="role_id"]').val(),
			// 			},
			// 			success: function(res) {
			// 				Helper.successNotif('Success');
			// 				Helper.loadingStop();
			// 				Helper.redirectTo('/');
			// 			},
			// 			error: function(res) {
			// 				console.log(res);
			// 				Helper.loadingStop();
			// 			}
			// 		})
			// 	})

			// 	e.preventDefault()
			// })
		</script>
	</body>
</html>



