@extends('admin.home')
@section('content')
<div class="col-md-6 col-xl-4">
    <div class="card mb-3 widget-content bg-midnight-bloom">
        <div class="widget-content-wrapper text-white">
            @if(Auth::user()->status == 6)
                <p>Akun Anda Sedang di Verify Admin</p>
            @else
                <div class="widget-content-left">
                    <div class="widget-heading">Teknisi</div>
                    <div class="widget-subheading">Ini teknisi</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-white"><span>1111</span></div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection