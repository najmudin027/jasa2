@extends('admin.home')
@section('content')
    @php
        if(Auth::user()->status == 7){
            $disabled = 'disabled';
        }else{
            $disabled = '';
        }
    @endphp

    <div class="col-md-12">
        @if( is_null(Auth::user()->email_verified_at) )
            <div class="mb-3 card text-white card-body bg-warning">
                <h5 class="text-white card-title">Your account has not been verified</h5>
                <form action="{{ url('/email/resend') }}" method="post">
                    @csrf
                    <p>please check your email account to verify it. If you do not receive a verification email, please click the Send Email button <button  class="btn bg-warning" style="text-decoration: underline;">Link Button</button></p>
                </form>
            </div>
        @else
            <div class="card">
                <div class="card-body">
                    <div class="card-header-tab card-header">Tehcnician Profile</div>
                    <div class="card-body">
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">First Name</label>
                                <div class="col-sm-10">
                                    <input name="first_name" id="examplename" value="{{ !empty($additionalInfo) ? $additionalInfo->first_name : '' }}" placeholder="First Name" type="text" class="form-control" required {{ $disabled }}>
                                    <strong><span id="error-first_name" style="color:red"></span></strong>
                                </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Last Name</label>
                            <div class="col-sm-10">
                                <input name="last_name" id="examplename" value="{{ !empty($additionalInfo) ? $additionalInfo->last_name : '' }}" placeholder="Last Name" type="text" class="form-control" {{ $disabled }}>
                                <strong><span id="error-last_name" style="color:red"></span></strong>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Date Of Birth</label>
                            <div class="col-sm-10">
                                <input name="date_of_birth" id="datetimepicker" value="{{ !empty($additionalInfo) ? $additionalInfo->date_of_birth : '' }}" placeholder="" type="text" class="form-control" {{ $disabled }}>
                                <strong><span id="error-date_of_birth" style="color:red"></span></strong>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">City Of Birth</label>
                            <div class="col-sm-10">
                                <input name="place_of_birth" id="datetimepicker" value="{{ !empty($additionalInfo) ? $additionalInfo->place_of_birth : '' }}" placeholder="" type="text" class="form-control" {{ $disabled }}>
                                <strong><span id="error-place_of_birth" style="color:red"></span></strong>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Marital</label>
                            <div class="col-sm-10">
                                @if(Auth::user()->status == 7)
                                    <input type="text" name="" value="{{ !empty($additionalInfo) ? $additionalInfo->marital->status : '' }}" class="form-control" {{ $disabled }}>
                                @else
                                    <select class="js-example-basic-single" id="marital" name="ms_marital_id" style="width:100%">
                                        <option value="{{ !empty($additionalInfo) ? $additionalInfo->ms_marital_id : '' }}" {{ !empty($additionalInfo->ms_marital_id) ? "selected" : '' }}>{{ !empty($additionalInfo) ? $additionalInfo->marital->status : ''}}</option>
                                    </select>
                                    <strong><span id="error-ms_marital_id" style="color:red"></span></strong>
                                @endif
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Religion</label>
                            <div class="col-sm-10">
                                @if(Auth::user()->status == 7)
                                    <input type="text" name="" value="{{ !empty($additionalInfo) ? $additionalInfo->religion->name : '' }}" class="form-control" {{ $disabled }}>
                                @else
                                    <select class="js-example-basic-single" id="religion" name="ms_religion_id" style="width:100%">
                                        <option value="{{ !empty($additionalInfo) ? $additionalInfo->ms_religion_id : '' }}" {{ !empty($additionalInfo->ms_religion_id) ? "selected" : '' }}>{{ !empty($additionalInfo) ? $additionalInfo->religion->name : ''}}</option>
                                    </select>
                                    <strong><span id="error-ms_religion_id" style="color:red"></span></strong>
                                @endif
                                
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Gender</label>
                            <div class="col-sm-10">
                                @if(Auth::user()->status == 7)
                                    @if(!empty($additionalInfo))
                                        <input type="text" name="" value="{{ $additionalInfo->gender == 0 ? 'Male' : 'Female' }}" class="form-control" {{ $disabled }}>
                                    @endif
                                @else
                                    <select class="js-example-basic-single" id="gender" name="gender" style="width:100%">
                                        @if(!empty($additionalInfo))
                                            <option value="0" {{ $additionalInfo->gender == 0 ? "selected" : '' }}>Male</option>
                                            <option value="1" {{ $additionalInfo->gender == 1 ? "selected" : '' }}>Woman</option>
                                        @else
                                            <option value="0" >Male</option>
                                            <option value="1" >Woman</option>
                                        @endif
                                        <!-- <option value="2">another one</option> -->
                                    </select>
                                    <strong><span id="error-gender" style="color:red"></span></strong>
                                @endif
                                
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Document Card</label>
                            <div class="col-sm-10">
                                @if(Auth::user()->status == 7)
                                    @if (!empty($teknisiInfo->attachment))
                                        <a href="{{asset('admin/storage/attachment/' . $teknisiInfo->attachment)}}" target="_blank">
                                            <span class="badge badge-success badge-pill"><strong>Open Attachment</strong></span>
                                        </a>
                                    @else
                                        <span class="badge badge-warning badge-pill"><strong>This technician has not uploaded a photo of the identity card</strong></span>
                                    @endif
                                @elseif(Auth::user()->status == 8)
                                    <input name="attachment" id="image_icon" value={{ $teknisiInfo->attachment }}  placeholder="Icons" type="file" class="form-control">
                                        <small style="color: red">* KTP, SIM, Passport</small><br>  
                                    <a href="{{asset('admin/storage/attachment/' . $teknisiInfo->attachment)}}" target="_blank">
                                        <span class="badge badge-success badge-pill"><strong>Open Attachment</strong></span>
                                    </a>
                                @else
                                    <input name="attachment" id="image_icon"  placeholder="Icons" type="file" class="form-control">
                                    <small style="color: red">* KTP, SIM, Passport</small><br>    
                                    <strong><span id="error-attachment" style="color:red"></span></strong>
                                @endif
                                
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Document Number</label>
                            <div class="col-sm-10">
                                <input name="no_identity" id="no_identity" value="{{ !empty($teknisiInfo) ? $teknisiInfo->no_identity : '' }}" id="examplename" placeholder="0012XXX" type="number" class="form-control" {{ $disabled }}>
                                <small style="color: red">* KTP, SIM, Passport</small><br>    
                                <strong><span id="error-no_identity" style="color:red"></span></strong>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Phone Number</label>
                            <div class="col-sm-10">
                                @if (Auth::user()->phone == null)
                                    <input name="phone1" id="examplename" placeholder="+628xxx" type="number" class="form-control">
                                    <strong><span id="error-phone1" style="color:red"></span></strong>
                                @else
                                    <input name="phone1" id="examplename" placeholder="+628xxx" value="{{ Auth::user()->phone }}" type="text" class="form-control" readonly>
                                @endif
                            </div>
                        </div>

                        <!-- <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Curriculum</label>
                            <div class="col-sm-10">
                                <select class="js-example-basic-single" id="curriculum" name="ms_curriculum_id" style="width:100%">

                                </select>
                                <strong><span id="error-ms_curriculum_id" style="color:red"></span></strong>
                            </div>
                        </div> -->

                        <input name="url" placeholder="" id="url" type="hidden" class="form-control" value="{{ Request::segment(1) }}">

                        <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label">Type</label>
                            <div class="col-sm-10">
                                @if(Auth::user()->status == 7)
                                    <!--<input type="text" name="" value="{{ !empty($address->types) ? $address->types->name : '' }}" class="form-control" {{ $disabled }} hidden>-->
                                    <select class="js-example-basic-single" id="address_type" name="ms_address_type_id" style="width:100%" disabled>
                                        <option value="{{ !empty($address) ? $address->ms_address_type_id : '' }}" {{ !empty($address->ms_address_type_id) ? "selected" : '' }}>{{!empty($address->types) ? $address->types->name : ''}}</option>
                                    </select>
                                @else
                                    <select class="js-example-basic-single" id="address_type" name="ms_address_type_id" style="width:100%">
                                        <option value="{{ !empty($address) ? $address->ms_address_type_id : '' }}" {{ !empty($address->ms_address_type_id) ? "selected" : '' }}>{{!empty($address->types) ? $address->types->name : ''}}</option>
                                    </select>
                                    <!-- <strong><span id="error-gender" style="color:red"></span></strong> -->
                                @endif
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">City Name</label>
                            <div class="col-sm-10">
                                @if(Auth::user()->status == 7)
                                    <!--<input type="text" name="" value="{{ !empty($address->city) ? $address->city->name : '' }}" class="form-control" {{ $disabled }} hidden>-->
                                    <select class="js-example-basic-single" id="city-select" name="ms_city_id" style="width:100%" disabled>
                                        <option value="{{ !empty($address) ? $address->ms_city_id : '' }}" {{ !empty($address->ms_city_id) ? "selected" : '' }}>{{!empty($address->city) ? $address->city->name : ''}}</option>
                                    </select>
                                @else
                                    <select class="js-example-basic-single" id="city-select" name="ms_city_id" style="width:100%">
                                        <option value="{{ !empty($address) ? $address->ms_city_id : '' }}" {{ !empty($address->ms_city_id) ? "selected" : '' }}>{{!empty($address->city) ? $address->city->name : ''}}</option>
                                    </select>
                                    <strong><span id="error-gender" style="color:red"></span></strong>
                                    <input type="hidden" name="get_city" id="get_city" />
                                @endif
                                
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Address</label>
                            <div class="col-sm-10">
                                <textarea name="address" class="form-control" id="exampleFormControlTextarea1" rows="3" {{ $disabled }}>{{ !empty($address->address) ? $address->address : '' }}</textarea>
                                <strong><span id="error-address" style="color:red"></span></strong>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Maps</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <input type="text" name="searchMap" style="margin-top: 7px; width: 70%; border:2px solid #4DB6AC;" id="searchMap" class="form-control" required>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <div id="map" style="height:400px"></div>
                                    </div>
                                    <div class="row" hidden>
                                        <div class="form-group col-md-5 col-xs-5 mr-4">
                                            <label class="control-label">Latitude</label>
                                            <input size="20" type="text" id="latbox" class="form-control" value="{{ !empty($address) ? $address->latitude : '' }}"  name="lat" onchange="inputLatlng('new')" required {{ $disabled }}>
                                        </div>
                                        <div class="form-group col-md-5 col-xs-5 mr-4">
                                            <label class="control-label">Longitude</label>
                                            <input size="20" type="text" id="lngbox"  class="form-control" value="{{ !empty($address) ? $address->longitude : '' }}"  name="long" onchange="inputLatlng('new')" required {{ $disabled }}>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10" hidden>
                                    <div class="row">
                                        <div class="form-group col-md-5 col-xs-5 mr-4">
                                            <label class="control-label">City / Regency</label>
                                            <input size="20" type="text" id="cities" class="form-control"  name="cities" readonly>
                                        </div>
                                        <div class="form-group col-md-5 col-xs-5 mr-4">
                                            <label class="control-label">Province</label>
                                            <input size="20" type="text" id="province" class="form-control"  name="prv" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if(Auth::user()->status != 7)
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button class="btn_login" type="submit" id="save" style="float: right">Save All</button>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endif
    </div>
@include('admin.setting.profile.profileCss')
@endsection

@section('script')
    @include('admin.template._mapsScript')
    @include('admin.teknisi.technician._job_experience_table_script')
    @include('admin.script.profile._addressScript')
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    {{-- jquery autocomplate --}}
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/datePicker/jquery.datetimepicker.css') }}" >
    <script src="{{ asset('/datePicker/jquery.datetimepicker.full.min.js') }}"></script>
    <script>
        //marital
        $("#marital").select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/marital/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.status,
                            id: item.id
                        }
                    });
    
                    return {
                        results: res
                    };
                }
            }
        });
        var get_kota =  $("#city-select option:selected").text()
        $('#get_city').val(get_kota);
        globalCRUD
            .select2("#religion", '/religion/select2')
            .select2('#address_type', '/address_type/select2')
            .select2("#city-select", '/city/select2')
    
    
            $(document).ready(function() {
                $('#gender').select2();
            });
    </script>

    <script>
        $(document).ready(function(){
            
            jQuery('#datetimepicker').datetimepicker({
                timepicker:false,
                format:'Y-m-d',
                mask:true
            });
        });
    </script>

    <script>
        $('.opt-token-area').show();

        // agar user profile bisa diclik
        $(".show-userinfo-menu").click(function() {
            $('.widget-content-wrapper').toggleClass('show')
            $('.dropdown-menu-right').toggleClass('show')
        })

        //untuk save all data
        $("#save").click(function() {
            var redirect = $('input[name  = "url"]').val();
            var fd = new FormData();
            Helper.loadingStart();
            console.log($('#image_icon')[0])
            attachmen = $('#image_icon')[0].files[0];
            if(attachmen){
                fd.append('attachment', attachmen);
            }

            fd.append('first_name', $('input[name="first_name"]').val());
            fd.append('last_name', $('input[name="last_name"]').val());
            fd.append('date_of_birth', $('input[name="date_of_birth"]').val());
            fd.append('place_of_birth', $('input[name="place_of_birth"]').val());
            fd.append('ms_marital_id', $('select[name="ms_marital_id"]').val());
            fd.append('ms_religion_id', $('select[name="ms_religion_id"]').val());
            fd.append('ms_city_id', $('select[name="ms_city_id"]').val());
            fd.append('gender', $('select[name="gender"]').val());  
            fd.append('no_identity', $('input[name="no_identity"]').val());
            fd.append('phone1', $('input[name="phone1"]').val());
            fd.append('ms_address_type_id', $('select[name="ms_address_type_id"]').val());
            fd.append('address', $('textarea[name="address"]').val());
            fd.append('lat', $("#latbox").val());
            fd.append('long', $("#lngbox").val());
            fd.append('search_map', $("#searchMap").val());
            fd.append('province', $("#province").val());
            fd.append('cities', $("#cities").val());
            
            console.log(fd)
            $.ajax({
                url:Helper.apiUrl('/teknisi_info/update/all'),
                data: fd,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function(response) {
                    console.log(response)
                    if (response != 0) {
                        iziToast.success({
                            title: 'OK',
                            position: 'topRight',
                            message: 'Data Has Been Saved',
                        });
                        console.log('url TO', redirect)
                        if(redirect == 'teknisi'){
                            window.location = Helper.url('/teknisi/profile'); 
                        }
                        if(redirect == 'customer'){
                            window.location = Helper.url('/customer/dashboard'); 
                        }
                    }
                },
                error: function(xhr, status, error) {
                    Helper.loadingStop();
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                        })
                    }
                },
            });
        });

    </script>

   <script>
       $("#curriculum").select2({
           
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/development_program/select2'),
                // url: 'http://localhost:8001/api/address_search/find_district?city_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.program_name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
   </script>
@endsection
