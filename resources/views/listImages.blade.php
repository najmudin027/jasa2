<!doctype html>
<html lang="en">
  <head>
    <title></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('newWindows/css/style.css') }}">
    
    <link href="{{  asset('adminAssets/css/dropzone.css') }}" rel="stylesheet" />
  </head>
  <body>
    <style>
      .clasImg{
          border: 1px #000000 solid;
      }
    </style> 
    <div class="wrapper d-flex align-items-stretch">
        <nav id="sidebar">
            <div class="custom-menu">
                <button type="button" id="sidebarCollapse" class="btn btn-primary">
                <i class="fa fa-bars"></i>
                <span class="sr-only">Toggle Menu</span>
                </button>
            </div>
            <div class="p-4">
                <ul class="list-unstyled components mb-5">
                    <li><a href="{{url('/admin/gallery-image/uploads?type=' . $_GET['type'])}}"><span class="fa fa-home mr-3"></span> Upload Image</a></li>
                    <li  class="active"><a href="#"><span class="fa fa-user mr-3"></span> List Image</a></li>
                </ul> 
                <div class="footer"></div>
            </div>
        </nav>
        <div id="content" class="p-4 p-md-5 pt-5">
            <div class="card-body">     
                <div class="container">
                    <div class="row">
                        <div class="row" id="load">
                        @foreach ($viewGalery as $item)
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb" i>
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                                    data-image=""
                                    data-target="#image-gallery">
                                    <img class="img-thumbnail image "
                                            src="{{asset('/images/'. $item->filename)}}"
                                            alt="Another alt text"
                                            data-img = "{{ $item->id }}"
                                            data-src="{{asset($item->filename)}}" style="width:450px;height:126px">
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div><br>
                <div style="float:right">
                    @if ($counter)
                        <a href="http://" id="loadMore" type="button" data-page="1"> Load More . . . . </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @section('script')
        <script src="{{ asset('newWindows/js/jquery.min.js') }}"></script>
        <script src="{{ asset('newWindows/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('newWindows/js/main.js') }}"></script>
        <script type="text/javascript" src="{{ asset('adminAssets/js/dropzone.js') }}"></script>
        <script type="text/javascript" src="{{ asset('adminAssets/js/jquery-3.3.1.js') }}"></script>

        <script>
            $(document).on('click', '.img-thumbnail', function() {
                var type = getRequest('type')
                if(type == 'galery'){
                        var selectedImage = $(this).attr('data-select');
                    if (getImageSelected().length >= 200) {
                        alert('Gambar Melebihi Batas Ketentuan')
                    } else {
                        if(selectedImage == 1){
                            $(this).attr('data-select', 0).toggleClass('clasImg')
                            console.log(selectedImage)
                            console.log(getImageSelected())
                        }else{
                            $(this).attr('data-select', 1).toggleClass('clasImg')
                            console.log(getImageSelected())
                        }
                        
                    }
                }
                if(type == 'main'){
                    var images = { id: $(this).attr('data-img'), src: $(this).attr('src') };
                    localStorage.setItem('idSelect', JSON.stringify(images))
                }
            })
    
            function getImageSelected() {
                var images = $('.img-thumbnail').map(function() {
                        var cek = $(this).attr('data-select')
                        if (cek == 1) {
                            return {
                                id: $(this).attr('data-img'),
                                src: $(this).attr('src')
                            }
                        }
                    }).get()
                    localStorage.setItem('id', JSON.stringify(images))
                return images
            }

            function getRequest(name){
                if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
                    return decodeURIComponent(name[1]);
            }
        </script>

        <script>
            $(function() {
                $(document).on('click', '#loadMore', function(e) {
                    e.preventDefault();
                    var page = parseInt($(this).attr('data-page')) + 1;
                    var url = Helper.apiUrl('/gallery-image/show-gallery?page=' + page)
                    console.log(url)

                    $(this).attr('data-page', page)
                    getArticles(url);
                });

                function getArticles(url) {
                    $.ajax({
                        url : url  
                    }).done(function (data) {
                        console.log(data)
                        $('#load').append(data);  
                    }).fail(function () {
                        alert('Articles could not be loaded.');
                    });
                }
            });
        </script>
    @endsection
  </body>
</html>