<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" style="line-height: 1.15;-webkit-text-size-adjust: 100%;font-family: sans-serif;">
{{--
    <body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #f6f8f8;line-height: inherit;">
        <div class="col-md-12" style="line-height: inherit;">
            <img alt="Image" border="0" src="http://astechindo.com/an-component/media/upload-gambar-pendukung/AstLogo.png" style="text-decoration: none;-ms-interpolation-mode: bicubic;border: 0;height: auto;width: 100%;max-width: 150px;display: block;line-height: inherit;border-style: none;" title="I'm an image" width="150">
        </div>
        <div class="col-md-12" style="line-height: inherit;">
            {!! $replace_string !!}
        </div>
        <!--[if (IE)]></div><![endif]-->
    </body> --}}
    <head style="line-height: inherit;">
        <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" style="line-height: inherit;">
        <meta content="width=device-width" name="viewport" style="line-height: inherit;">
        <!--[if !mso]><!-->
        <meta content="IE=edge" http-equiv="X-UA-Compatible" style="line-height: inherit;">
        <!--<![endif]-->
        <title style="line-height: inherit;"></title>
        <!--[if !mso]><!-->
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css" style="line-height: inherit;">
        <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.3/build/pure-min.css" integrity="sha384-cg6SkqEOCV1NbJoCu11+bm0NvBRc8IYLRGXkmNrqUBfTjmMYwNKPWBTIKyw9mHNJ" crossorigin="anonymous" style="line-height: inherit;">

        <!--<![endif]-->.
        <style style="line-height: inherit;">
            /*!
Pure v2.0.3
Copyright 2013 Yahoo!
Licensed under the BSD License.
https://github.com/pure-css/pure/blob/master/LICENSE.md
*/
/*!
normalize.css v | MIT License | git.io/normalize
Copyright (c) Nicolas Gallagher and Jonathan Neal
*/
/*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}main{display:block}h1{font-size:2em;margin:.67em 0}hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}pre{font-family:monospace,monospace;font-size:1em}a{background-color:transparent}abbr[title]{border-bottom:none;text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted}b,strong{font-weight:bolder}code,kbd,samp{font-family:monospace,monospace;font-size:1em}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}img{border-style:none}button,input,optgroup,select,textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}button,input{overflow:visible}button,select{text-transform:none}[type=button],[type=reset],[type=submit],button{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=reset]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{border-style:none;padding:0}[type=button]:-moz-focusring,[type=reset]:-moz-focusring,[type=submit]:-moz-focusring,button:-moz-focusring{outline:1px dotted ButtonText}fieldset{padding:.35em .75em .625em}legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}progress{vertical-align:baseline}textarea{overflow:auto}[type=checkbox],[type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}[type=number]::-webkit-inner-spin-button,[type=number]::-webkit-outer-spin-button{height:auto}[type=search]{-webkit-appearance:textfield;outline-offset:-2px}[type=search]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}details{display:block}summary{display:list-item}template{display:none}[hidden]{display:none}html{font-family:sans-serif}.hidden,[hidden]{display:none!important}.pure-img{max-width:100%;height:auto;display:block}.pure-g{letter-spacing:-.31em;text-rendering:optimizespeed;font-family:FreeSans,Arimo,"Droid Sans",Helvetica,Arial,sans-serif;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-ms-flex-flow:row wrap;flex-flow:row wrap;-ms-flex-line-pack:start;align-content:flex-start}@media all and (-ms-high-contrast:none),(-ms-high-contrast:active){table .pure-g{display:block}}.opera-only :-o-prefocus,.pure-g{word-spacing:-.43em}.pure-u{display:inline-block;letter-spacing:normal;word-spacing:normal;vertical-align:top;text-rendering:auto}.pure-g [class*=pure-u]{font-family:sans-serif}.pure-u-1,.pure-u-1-1,.pure-u-1-12,.pure-u-1-2,.pure-u-1-24,.pure-u-1-3,.pure-u-1-4,.pure-u-1-5,.pure-u-1-6,.pure-u-1-8,.pure-u-10-24,.pure-u-11-12,.pure-u-11-24,.pure-u-12-24,.pure-u-13-24,.pure-u-14-24,.pure-u-15-24,.pure-u-16-24,.pure-u-17-24,.pure-u-18-24,.pure-u-19-24,.pure-u-2-24,.pure-u-2-3,.pure-u-2-5,.pure-u-20-24,.pure-u-21-24,.pure-u-22-24,.pure-u-23-24,.pure-u-24-24,.pure-u-3-24,.pure-u-3-4,.pure-u-3-5,.pure-u-3-8,.pure-u-4-24,.pure-u-4-5,.pure-u-5-12,.pure-u-5-24,.pure-u-5-5,.pure-u-5-6,.pure-u-5-8,.pure-u-6-24,.pure-u-7-12,.pure-u-7-24,.pure-u-7-8,.pure-u-8-24,.pure-u-9-24{display:inline-block;letter-spacing:normal;word-spacing:normal;vertical-align:top;text-rendering:auto}.pure-u-1-24{width:4.1667%}.pure-u-1-12,.pure-u-2-24{width:8.3333%}.pure-u-1-8,.pure-u-3-24{width:12.5%}.pure-u-1-6,.pure-u-4-24{width:16.6667%}.pure-u-1-5{width:20%}.pure-u-5-24{width:20.8333%}.pure-u-1-4,.pure-u-6-24{width:25%}.pure-u-7-24{width:29.1667%}.pure-u-1-3,.pure-u-8-24{width:33.3333%}.pure-u-3-8,.pure-u-9-24{width:37.5%}.pure-u-2-5{width:40%}.pure-u-10-24,.pure-u-5-12{width:41.6667%}.pure-u-11-24{width:45.8333%}.pure-u-1-2,.pure-u-12-24{width:50%}.pure-u-13-24{width:54.1667%}.pure-u-14-24,.pure-u-7-12{width:58.3333%}.pure-u-3-5{width:60%}.pure-u-15-24,.pure-u-5-8{width:62.5%}.pure-u-16-24,.pure-u-2-3{width:66.6667%}.pure-u-17-24{width:70.8333%}.pure-u-18-24,.pure-u-3-4{width:75%}.pure-u-19-24{width:79.1667%}.pure-u-4-5{width:80%}.pure-u-20-24,.pure-u-5-6{width:83.3333%}.pure-u-21-24,.pure-u-7-8{width:87.5%}.pure-u-11-12,.pure-u-22-24{width:91.6667%}.pure-u-23-24{width:95.8333%}.pure-u-1,.pure-u-1-1,.pure-u-24-24,.pure-u-5-5{width:100%}.pure-button{display:inline-block;line-height:normal;white-space:nowrap;vertical-align:middle;text-align:center;cursor:pointer;-webkit-user-drag:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;-webkit-box-sizing:border-box;box-sizing:border-box}.pure-button::-moz-focus-inner{padding:0;border:0}.pure-button-group{letter-spacing:-.31em;text-rendering:optimizespeed}.opera-only :-o-prefocus,.pure-button-group{word-spacing:-.43em}.pure-button-group .pure-button{letter-spacing:normal;word-spacing:normal;vertical-align:top;text-rendering:auto}.pure-button{font-family:inherit;font-size:100%;padding:.5em 1em;color:rgba(0,0,0,.8);border:none transparent;background-color:#e6e6e6;text-decoration:none;border-radius:2px}.pure-button-hover,.pure-button:focus,.pure-button:hover{background-image:-webkit-gradient(linear,left top,left bottom,from(transparent),color-stop(40%,rgba(0,0,0,.05)),to(rgba(0,0,0,.1)));background-image:linear-gradient(transparent,rgba(0,0,0,.05) 40%,rgba(0,0,0,.1))}.pure-button:focus{outline:0}.pure-button-active,.pure-button:active{-webkit-box-shadow:0 0 0 1px rgba(0,0,0,.15) inset,0 0 6px rgba(0,0,0,.2) inset;box-shadow:0 0 0 1px rgba(0,0,0,.15) inset,0 0 6px rgba(0,0,0,.2) inset;border-color:#000}.pure-button-disabled,.pure-button-disabled:active,.pure-button-disabled:focus,.pure-button-disabled:hover,.pure-button[disabled]{border:none;background-image:none;opacity:.4;cursor:not-allowed;-webkit-box-shadow:none;box-shadow:none;pointer-events:none}.pure-button-hidden{display:none}.pure-button-primary,.pure-button-selected,a.pure-button-primary,a.pure-button-selected{background-color:#0078e7;color:#fff}.pure-button-group .pure-button{margin:0;border-radius:0;border-right:1px solid rgba(0,0,0,.2)}.pure-button-group .pure-button:first-child{border-top-left-radius:2px;border-bottom-left-radius:2px}.pure-button-group .pure-button:last-child{border-top-right-radius:2px;border-bottom-right-radius:2px;border-right:none}.pure-form input[type=color],.pure-form input[type=date],.pure-form input[type=datetime-local],.pure-form input[type=datetime],.pure-form input[type=email],.pure-form input[type=month],.pure-form input[type=number],.pure-form input[type=password],.pure-form input[type=search],.pure-form input[type=tel],.pure-form input[type=text],.pure-form input[type=time],.pure-form input[type=url],.pure-form input[type=week],.pure-form select,.pure-form textarea{padding:.5em .6em;display:inline-block;border:1px solid #ccc;-webkit-box-shadow:inset 0 1px 3px #ddd;box-shadow:inset 0 1px 3px #ddd;border-radius:4px;vertical-align:middle;-webkit-box-sizing:border-box;box-sizing:border-box}.pure-form input:not([type]){padding:.5em .6em;display:inline-block;border:1px solid #ccc;-webkit-box-shadow:inset 0 1px 3px #ddd;box-shadow:inset 0 1px 3px #ddd;border-radius:4px;-webkit-box-sizing:border-box;box-sizing:border-box}.pure-form input[type=color]{padding:.2em .5em}.pure-form input[type=color]:focus,.pure-form input[type=date]:focus,.pure-form input[type=datetime-local]:focus,.pure-form input[type=datetime]:focus,.pure-form input[type=email]:focus,.pure-form input[type=month]:focus,.pure-form input[type=number]:focus,.pure-form input[type=password]:focus,.pure-form input[type=search]:focus,.pure-form input[type=tel]:focus,.pure-form input[type=text]:focus,.pure-form input[type=time]:focus,.pure-form input[type=url]:focus,.pure-form input[type=week]:focus,.pure-form select:focus,.pure-form textarea:focus{outline:0;border-color:#129fea}.pure-form input:not([type]):focus{outline:0;border-color:#129fea}.pure-form input[type=checkbox]:focus,.pure-form input[type=file]:focus,.pure-form input[type=radio]:focus{outline:thin solid #129fea;outline:1px auto #129fea}.pure-form .pure-checkbox,.pure-form .pure-radio{margin:.5em 0;display:block}.pure-form input[type=color][disabled],.pure-form input[type=date][disabled],.pure-form input[type=datetime-local][disabled],.pure-form input[type=datetime][disabled],.pure-form input[type=email][disabled],.pure-form input[type=month][disabled],.pure-form input[type=number][disabled],.pure-form input[type=password][disabled],.pure-form input[type=search][disabled],.pure-form input[type=tel][disabled],.pure-form input[type=text][disabled],.pure-form input[type=time][disabled],.pure-form input[type=url][disabled],.pure-form input[type=week][disabled],.pure-form select[disabled],.pure-form textarea[disabled]{cursor:not-allowed;background-color:#eaeded;color:#cad2d3}.pure-form input:not([type])[disabled]{cursor:not-allowed;background-color:#eaeded;color:#cad2d3}.pure-form input[readonly],.pure-form select[readonly],.pure-form textarea[readonly]{background-color:#eee;color:#777;border-color:#ccc}.pure-form input:focus:invalid,.pure-form select:focus:invalid,.pure-form textarea:focus:invalid{color:#b94a48;border-color:#e9322d}.pure-form input[type=checkbox]:focus:invalid:focus,.pure-form input[type=file]:focus:invalid:focus,.pure-form input[type=radio]:focus:invalid:focus{outline-color:#e9322d}.pure-form select{height:2.25em;border:1px solid #ccc;background-color:#fff}.pure-form select[multiple]{height:auto}.pure-form label{margin:.5em 0 .2em}.pure-form fieldset{margin:0;padding:.35em 0 .75em;border:0}.pure-form legend{display:block;width:100%;padding:.3em 0;margin-bottom:.3em;color:#333;border-bottom:1px solid #e5e5e5}.pure-form-stacked input[type=color],.pure-form-stacked input[type=date],.pure-form-stacked input[type=datetime-local],.pure-form-stacked input[type=datetime],.pure-form-stacked input[type=email],.pure-form-stacked input[type=file],.pure-form-stacked input[type=month],.pure-form-stacked input[type=number],.pure-form-stacked input[type=password],.pure-form-stacked input[type=search],.pure-form-stacked input[type=tel],.pure-form-stacked input[type=text],.pure-form-stacked input[type=time],.pure-form-stacked input[type=url],.pure-form-stacked input[type=week],.pure-form-stacked label,.pure-form-stacked select,.pure-form-stacked textarea{display:block;margin:.25em 0}.pure-form-stacked input:not([type]){display:block;margin:.25em 0}.pure-form-aligned input,.pure-form-aligned select,.pure-form-aligned textarea,.pure-form-message-inline{display:inline-block;vertical-align:middle}.pure-form-aligned textarea{vertical-align:top}.pure-form-aligned .pure-control-group{margin-bottom:.5em}.pure-form-aligned .pure-control-group label{text-align:right;display:inline-block;vertical-align:middle;width:10em;margin:0 1em 0 0}.pure-form-aligned .pure-controls{margin:1.5em 0 0 11em}.pure-form .pure-input-rounded,.pure-form input.pure-input-rounded{border-radius:2em;padding:.5em 1em}.pure-form .pure-group fieldset{margin-bottom:10px}.pure-form .pure-group input,.pure-form .pure-group textarea{display:block;padding:10px;margin:0 0 -1px;border-radius:0;position:relative;top:-1px}.pure-form .pure-group input:focus,.pure-form .pure-group textarea:focus{z-index:3}.pure-form .pure-group input:first-child,.pure-form .pure-group textarea:first-child{top:1px;border-radius:4px 4px 0 0;margin:0}.pure-form .pure-group input:first-child:last-child,.pure-form .pure-group textarea:first-child:last-child{top:1px;border-radius:4px;margin:0}.pure-form .pure-group input:last-child,.pure-form .pure-group textarea:last-child{top:-2px;border-radius:0 0 4px 4px;margin:0}.pure-form .pure-group button{margin:.35em 0}.pure-form .pure-input-1{width:100%}.pure-form .pure-input-3-4{width:75%}.pure-form .pure-input-2-3{width:66%}.pure-form .pure-input-1-2{width:50%}.pure-form .pure-input-1-3{width:33%}.pure-form .pure-input-1-4{width:25%}.pure-form-message-inline{display:inline-block;padding-left:.3em;color:#666;vertical-align:middle;font-size:.875em}.pure-form-message{display:block;color:#666;font-size:.875em}@media only screen and (max-width :480px){.pure-form button[type=submit]{margin:.7em 0 0}.pure-form input:not([type]),.pure-form input[type=color],.pure-form input[type=date],.pure-form input[type=datetime-local],.pure-form input[type=datetime],.pure-form input[type=email],.pure-form input[type=month],.pure-form input[type=number],.pure-form input[type=password],.pure-form input[type=search],.pure-form input[type=tel],.pure-form input[type=text],.pure-form input[type=time],.pure-form input[type=url],.pure-form input[type=week],.pure-form label{margin-bottom:.3em;display:block}.pure-group input:not([type]),.pure-group input[type=color],.pure-group input[type=date],.pure-group input[type=datetime-local],.pure-group input[type=datetime],.pure-group input[type=email],.pure-group input[type=month],.pure-group input[type=number],.pure-group input[type=password],.pure-group input[type=search],.pure-group input[type=tel],.pure-group input[type=text],.pure-group input[type=time],.pure-group input[type=url],.pure-group input[type=week]{margin-bottom:0}.pure-form-aligned .pure-control-group label{margin-bottom:.3em;text-align:left;display:block;width:100%}.pure-form-aligned .pure-controls{margin:1.5em 0 0 0}.pure-form-message,.pure-form-message-inline{display:block;font-size:.75em;padding:.2em 0 .8em}}.pure-menu{-webkit-box-sizing:border-box;box-sizing:border-box}.pure-menu-fixed{position:fixed;left:0;top:0;z-index:3}.pure-menu-item,.pure-menu-list{position:relative}.pure-menu-list{list-style:none;margin:0;padding:0}.pure-menu-item{padding:0;margin:0;height:100%}.pure-menu-heading,.pure-menu-link{display:block;text-decoration:none;white-space:nowrap}.pure-menu-horizontal{width:100%;white-space:nowrap}.pure-menu-horizontal .pure-menu-list{display:inline-block}.pure-menu-horizontal .pure-menu-heading,.pure-menu-horizontal .pure-menu-item,.pure-menu-horizontal .pure-menu-separator{display:inline-block;vertical-align:middle}.pure-menu-item .pure-menu-item{display:block}.pure-menu-children{display:none;position:absolute;left:100%;top:0;margin:0;padding:0;z-index:3}.pure-menu-horizontal .pure-menu-children{left:0;top:auto;width:inherit}.pure-menu-active>.pure-menu-children,.pure-menu-allow-hover:hover>.pure-menu-children{display:block;position:absolute}.pure-menu-has-children>.pure-menu-link:after{padding-left:.5em;content:"\25B8";font-size:small}.pure-menu-horizontal .pure-menu-has-children>.pure-menu-link:after{content:"\25BE"}.pure-menu-scrollable{overflow-y:scroll;overflow-x:hidden}.pure-menu-scrollable .pure-menu-list{display:block}.pure-menu-horizontal.pure-menu-scrollable .pure-menu-list{display:inline-block}.pure-menu-horizontal.pure-menu-scrollable{white-space:nowrap;overflow-y:hidden;overflow-x:auto;padding:.5em 0}.pure-menu-horizontal .pure-menu-children .pure-menu-separator,.pure-menu-separator{background-color:#ccc;height:1px;margin:.3em 0}.pure-menu-horizontal .pure-menu-separator{width:1px;height:1.3em;margin:0 .3em}.pure-menu-horizontal .pure-menu-children .pure-menu-separator{display:block;width:auto}.pure-menu-heading{text-transform:uppercase;color:#565d64}.pure-menu-link{color:#777}.pure-menu-children{background-color:#fff}.pure-menu-disabled,.pure-menu-heading,.pure-menu-link{padding:.5em 1em}.pure-menu-disabled{opacity:.5}.pure-menu-disabled .pure-menu-link:hover{background-color:transparent}.pure-menu-active>.pure-menu-link,.pure-menu-link:focus,.pure-menu-link:hover{background-color:#eee}.pure-menu-selected>.pure-menu-link,.pure-menu-selected>.pure-menu-link:visited{color:#000}.pure-table{border-collapse:collapse;border-spacing:0;empty-cells:show;border:1px solid #cbcbcb}.pure-table caption{color:#000;font:italic 85%/1 arial,sans-serif;padding:1em 0;text-align:center}.pure-table td,.pure-table th{border-left:1px solid #cbcbcb;border-width:0 0 0 1px;font-size:inherit;margin:0;overflow:visible;padding:.5em 1em}.pure-table thead{background-color:#e0e0e0;color:#000;text-align:left;vertical-align:bottom}.pure-table td{background-color:transparent}.pure-table-odd td{background-color:#f2f2f2}.pure-table-striped tr:nth-child(2n-1) td{background-color:#f2f2f2}.pure-table-bordered td{border-bottom:1px solid #cbcbcb}.pure-table-bordered tbody>tr:last-child>td{border-bottom-width:0}.pure-table-horizontal td,.pure-table-horizontal th{border-width:0 0 1px 0;border-bottom:1px solid #cbcbcb}.pure-table-horizontal tbody>tr:last-child>td{border-bottom-width:0}
        </style>
        <style type="text/css" style="line-height: inherit;">
            body {
                margin: 0;
                padding: 0;
            }

            table,
            td,
            tr {
                vertical-align: top;
                border-collapse: collapse;
            }

            * {
                line-height: inherit;
            }

            a[x-apple-data-detectors=true] {
                color: inherit !important;
                text-decoration: none !important;
            }
        </style>
        <style id="media-query" type="text/css" style="line-height: inherit;">
            @media (max-width: 670px) {

                .block-grid,
                .col {
                    min-width: 320px !important;
                    max-width: 100% !important;
                    display: block !important;
                }

                .block-grid {
                    width: 100% !important;
                }

                .col {
                    width: 100% !important;
                }

                .col>div {
                    margin: 0 auto;
                }

                img.fullwidth,
                img.fullwidthOnMobile {
                    max-width: 100% !important;
                }

                .no-stack .col {
                    min-width: 0 !important;
                    display: table-cell !important;
                }

                .no-stack.two-up .col {
                    width: 50% !important;
                }

                .no-stack .col.num4 {
                    width: 33% !important;
                }

                .no-stack .col.num8 {
                    width: 66% !important;
                }

                .no-stack .col.num4 {
                    width: 33% !important;
                }

                .no-stack .col.num3 {
                    width: 25% !important;
                }

                .no-stack .col.num6 {
                    width: 50% !important;
                }

                .no-stack .col.num9 {
                    width: 75% !important;
                }

                .video-block {
                    max-width: none !important;
                }

                .mobile_hide {
                    min-height: 0px;
                    max-height: 0px;
                    max-width: 0px;
                    display: none;
                    overflow: hidden;
                    font-size: 0px;
                }

                .desktop_hide {
                    display: block !important;
                    max-height: none !important;
                }
            }
        </style>
    </head>

    <body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #F5F5F5;line-height: inherit;">
        <table bgcolor="#F5F5F5" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed;vertical-align: top;min-width: 320px;margin: 0 auto;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #F5F5F5;width: 100%;line-height: inherit;" valign="top" width="100%">
            <tbody style="line-height: inherit;">
                <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                    <td style="word-break: break-word;vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                        <div style="background-color: transparent;line-height: inherit;">
                            <div class="block-grid" style="margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;line-height: inherit;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;line-height: inherit;">
                                    <div class="col num12" style="min-width: 320px;max-width: 650px;display: table-cell;vertical-align: top;width: 650px;line-height: inherit;">
                                        <div style="width: 100% !important;line-height: inherit;">
                                            <div style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-bottom: 0px solid transparent;border-right: 0px solid transparent;padding-top: 5px;padding-bottom: 5px;padding-right: 0px;padding-left: 0px;line-height: inherit;">
                                                <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;line-height: inherit;" valign="top" width="100%">
                                                    <tbody style="line-height: inherit;">
                                                        <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                            <td class="divider_inner" style="word-break: break-word;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px;line-height: inherit;border-collapse: collapse;" valign="top">
                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="10" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;border-top: 0px solid transparent;height: 10px;line-height: inherit;" valign="top" width="100%">
                                                                    <tbody style="line-height: inherit;">
                                                                        <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                                            <td height="10" style="word-break: break-word;vertical-align: top;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;line-height: inherit;border-collapse: collapse;" valign="top"><span style="line-height: inherit;"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background-color: transparent;line-height: inherit;">
                            <div class="block-grid two-up no-stack" style="margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;line-height: inherit;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color: #FFFFFF;line-height: inherit;">
                                    <div class="col num6" style="min-width: 320px;max-width: 325px;display: table-cell;vertical-align: top;width: 325px;line-height: inherit;">
                                        <div style="width: 100% !important;line-height: inherit;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-bottom: 0px solid transparent;border-right: 0px solid transparent;padding-top: 25px;padding-bottom: 25px;padding-right: 0px;padding-left: 25px;line-height: inherit;">
                                                <!--<![endif]-->
                                                <div align="left" class="img-container left fullwidthOnMobile fixedwidth" style="padding-right: 0px;padding-left: 0px;line-height: inherit;">
                                                    <div style="font-size:1px;line-height:5px"> </div><img alt="Image" border="0" class="left fullwidthOnMobile fixedwidth" src="{{ url('/get/logo/from-storage/'.$get_img_astech->logo) }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;border: 0;height: auto;width: 100%;max-width: 150px;display: block;line-height: inherit;border-style: none;" title="I'm an image" width="150">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col num6" style="min-width: 320px;max-width: 325px;display: table-cell;vertical-align: top;width: 325px;line-height: inherit;">
                                        <div style="width: 100% !important;line-height: inherit;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-bottom: 0px solid transparent;border-right: 0px solid transparent;padding-top: 25px;padding-bottom: 25px;padding-right: 25px;padding-left: 0px;line-height: inherit;">
                                                <!--<![endif]-->
                                                <div align="right" class="button-container" style="padding-top: 10px;padding-right: 0px;padding-bottom: 10px;padding-left: 10px;line-height: inherit;">
                                                    <!-- <a href="#" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #F5F5F5; background-color: #E32142; border-radius: 14px; -webkit-border-radius: 14px; -moz-border-radius: 14px; width: auto; width: auto; border-top: 1px solid #E32142; border-right: 1px solid #E32142; border-bottom: 1px solid #E32142; border-left: 1px solid #E32142; padding-top: 3px; padding-bottom: 3px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:15px;padding-right:15px;font-size:14px;display:inline-block;">
                                                            <span style="font-size: 16px; line-height: 32px;"><span style="font-size: 14px; line-height: 28px;">My account</span></span>
                                                        </span></a>
                                                    [if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif] -->
                                                </div>
                                                <!--[if (!mso)&(!IE)]><!-->
                                            </div>
                                            <!--<![endif]-->
                                        </div>
                                    </div>
                                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                </div>
                            </div>
                        </div>
                        <div style="background-color: transparent;line-height: inherit;">
                            <div class="block-grid" style="margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;line-height: inherit;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color: #FFFFFF;line-height: inherit;">
                                    <div class="col num12" style="min-width: 320px;max-width: 650px;display: table-cell;vertical-align: top;width: 650px;line-height: inherit;">
                                        <div style="width: 100% !important;line-height: inherit;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-bottom: 0px solid transparent;border-right: 0px solid transparent;padding-top: 15px;padding-bottom: 5px;padding-right: 0px;padding-left: 0px;line-height: inherit;">
                                                <div style="color:#052d3d;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="line-height: 14px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; font-size: 14px; margin-left:20px; margin-right:20px; color: #052d3d;">
                                                        {!! $replace_string !!}
                                                        <div style="text-align:center;">
                                                            @if($name_module_email == 'review' && $type == 1)
                                                                <a href="{{ url('customer/reviews/waiting') }}" style="line-height: normal;font-family: inherit;font-size: 100%;margin: 0;overflow: visible;text-transform: none;-webkit-appearance: button;display: inline-block;white-space: nowrap;vertical-align: middle;text-align: center;cursor: pointer;-webkit-user-drag: none;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;-webkit-box-sizing: border-box;box-sizing: border-box;padding: .5em 1em;color: #fff;border: none transparent;background-color: #0078e7;text-decoration: none;border-radius: 2px;">Beri Ulasan</a>
                                                            @elseif($name_module_email == 'review' && $type == 2)
                                                                <a href="{{ url('/teknisi/request-job-accept/'.$modul['order_id'].'') }}" style="line-height: normal;font-family: inherit;font-size: 100%;margin: 0;overflow: visible;text-transform: none;-webkit-appearance: button;display: inline-block;white-space: nowrap;vertical-align: middle;text-align: center;cursor: pointer;-webkit-user-drag: none;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;-webkit-box-sizing: border-box;box-sizing: border-box;padding: .5em 1em;color: #fff;border: none transparent;background-color: #0078e7;text-decoration: none;border-radius: 2px;">Lihat Ulasan</a>
                                                            @endif
                                                        </div>
                                                        <br/>
                                                        <br/>
                                                        @if($is_order == true)
                                                            <font size="1" style="line-height: inherit;">
                                                                <div style="width: 100%;margin-top: 45px:margin-bottom:25px;line-height: inherit;">
                                                                    <p>Order Code : #{{ $modul->code }}</p>
                                                                    <table class="pure-table" border="1" width="100%" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-spacing: 0;empty-cells: show;border: 1px solid #cbcbcb;">
                                                                        <thead style="line-height: inherit;background-color: #e0e0e0;color: #000;text-align: left;vertical-align: bottom;">
                                                                            <tr style="line-height: inherit;vertical-align: top;border-collapse: collapse;">
                                                                                <th width="20%" style="line-height: inherit;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;">Name Service</th>
                                                                                <th width="20%" style="line-height: inherit;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;">Service Type Product group</th>
                                                                                <th width="15%" class="center" style="line-height: inherit;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;">UNIT</th>
                                                                                <th width="15%" class="right" style="line-height: inherit;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;">COST</th>
                                                                                <th width="15%" class="right" style="line-height: inherit;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;">Total</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody style="line-height: inherit;">
                                                                            @php
                                                                                $total_price_service = 0
                                                                            @endphp
                                                                            @foreach($modul->service_detail as $key => $service_detail)
                                                                                @php
                                                                                    $harga_service = isset($service_detail->price) ? $service_detail->price * $service_detail->unit : 0
                                                                                @endphp
                                                                                <tr style="line-height: inherit;vertical-align: top;border-collapse: collapse;">
                                                                                    @if ($service_detail->symptom_name !== null)
                                                                                        <td class="left" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">{{ $service_detail->symptom_name }}</td>
                                                                                        <td class="left" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">{{ $service_detail->service_type_name }}&nbsp;{{ $modul->product_group_name }}</td>
                                                                                    @else
                                                                                        <td class="left" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">{{ $service_detail->symptom->name }}</td>
                                                                                        <td class="left" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">{{ $service_detail->services_type->name }}&nbsp;{{ $modul->product_group->name }}</td>
                                                                                    @endif

                                                                                    <td class="center" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;"><b style="line-height: inherit;font-weight: bolder;">{{ $service_detail->unit }}</b></td>
                                                                                    <td class="right" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">Rp.{{ number_format($service_detail->price) }}</td>
                                                                                    <td class="right" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">Rp.{{ number_format($harga_service) }}</td>
                                                                                    @php $total_price_service += $harga_service @endphp
                                                                                </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>

                                                                </div>
                                                                <br>
                                                                @if(count($modul->sparepart_detail) > 0 || count($item_details)  > 0)
                                                                    <div class="table-responsive-sm" style="width: 100%;line-height: inherit;">
                                                                        <table class="pure-table" border="1" width="100%" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-spacing: 0;empty-cells: show;border: 1px solid #cbcbcb;">
                                                                            <thead style="line-height: inherit;background-color: #e0e0e0;color: #000;text-align: left;vertical-align: bottom;">
                                                                                <tr style="line-height: inherit;vertical-align: top;border-collapse: collapse;">
                                                                                    <th width="20%" style="line-height: inherit;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;">Item</th>
                                                                                    <th width="20%" style="line-height: inherit;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;">Description</th>
                                                                                    <th width="10%" class="center" style="line-height: inherit;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;">QTY</th>
                                                                                    <th width="15%" class="right" style="line-height: inherit;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;">COST</th>
                                                                                    <th width="15%" class="right" style="line-height: inherit;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;">Total</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody style="line-height: inherit;">
                                                                                @php
                                                                                    $total_price = 0
                                                                                @endphp
                                                                                    @foreach($modul->sparepart_detail as $key => $sparepart)
                                                                                        @php
                                                                                            $harga_sparepart = isset($sparepart->price) ? $sparepart->price * $sparepart->quantity : 0
                                                                                        @endphp
                                                                                        <tr style="line-height: inherit;vertical-align: top;border-collapse: collapse;">
                                                                                            <td class="left" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">{{ $sparepart->name_sparepart }}</td>
                                                                                            @if(empty($sparepart->technician_sparepart->note))
                                                                                                <td class="left" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">-</td>
                                                                                            @else
                                                                                                <td class="left" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">{{ $sparepart->technician_sparepart->note }}</td>
                                                                                            @endif
                                                                                            <td class="center" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;"><b style="line-height: inherit;font-weight: bolder;">{{ $sparepart->quantity }}</b></td>
                                                                                            <td class="right" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">Rp.{{ number_format($sparepart->price) }}</td>
                                                                                            <td class="right" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">Rp.{{ number_format($harga_sparepart) }}</td>
                                                                                            @php $total_price += $harga_sparepart @endphp
                                                                                        </tr>
                                                                                    @endforeach
                                                                                @foreach($item_details as $key => $item) <!-- part form product inventory -->
                                                                                    @php
                                                                                        $harga_item = isset($item->price) ? $item->price * $item->quantity : 0
                                                                                    @endphp
                                                                                    <tr style="line-height: inherit;vertical-align: top;border-collapse: collapse;">
                                                                                        <td class="left" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">{{ $item->name_product }}</td>
                                                                                        @if(empty($item->technician_sparepart->description))
                                                                                            <td class="left" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">-</td>
                                                                                        @else
                                                                                            <td class="left" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">{{ $item->technician_sparepart->description }}</td>
                                                                                        @endif
                                                                                        <td class="center" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;"><b style="line-height: inherit;font-weight: bolder;">{{ $item->quantity }}</b></td>
                                                                                        <td class="right" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">Rp.{{ number_format($item->price) }}</td>
                                                                                        <td class="right" style="line-height: inherit;vertical-align: top;border-collapse: collapse;border-left: 1px solid #cbcbcb;border-width: 0 0 0 1px;font-size: inherit;margin: 0;overflow: visible;padding: .5em 1em;background-color: transparent;">Rp.{{ number_format($harga_item) }}</td>
                                                                                    </tr>
                                                                                    @php $total_price += $harga_item; $totalPart = $total_price + $modul->grand_total; @endphp
                                                                                @endforeach
                                                                                <!-- @if($total_price == 0)
                                                                                    <h5>No Extra Part Needed.</h5>
                                                                                @endif -->

                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                @endif
                                                            </font>
                                                        @endif
                                                    </div>
                                                </div>
                                                <!--[if mso]></td></tr></table><![endif]-->
                                                <!--[if (!mso)&(!IE)]><!-->
                                            </div>
                                            <!--<![endif]-->
                                        </div>
                                    </div>
                                    <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                    <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                </div>
                            </div>
                        </div>
                        <div style="background-color: transparent;line-height: inherit;">
                            <div class="block-grid" style="margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;line-height: inherit;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color: #FFFFFF;line-height: inherit;">
                                    <div class="col num12" style="min-width: 320px;max-width: 650px;display: table-cell;vertical-align: top;width: 650px;line-height: inherit;">
                                        <div style="width: 100% !important;line-height: inherit;">
                                            <!--[if (!mso)&(!IE)]><!-->
                                            <div style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-bottom: 0px solid transparent;border-right: 0px solid transparent;padding-top: 0px;padding-bottom: 20px;padding-right: 0px;padding-left: 0px;line-height: inherit;">
                                                <!--<![endif]-->
                                                <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;line-height: inherit;" valign="top" width="100%">
                                                    <tbody style="line-height: inherit;">
                                                        <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                            <td class="divider_inner" style="word-break: break-word;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px;line-height: inherit;border-collapse: collapse;" valign="top">
                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;border-top: 0px solid transparent;height: 0px;line-height: inherit;" valign="top" width="100%">
                                                                    <tbody style="line-height: inherit;">
                                                                        <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                                            <td height="0" style="word-break: break-word;vertical-align: top;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;line-height: inherit;border-collapse: collapse;" valign="top"><span style="line-height: inherit;"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="background-color: transparent;line-height: inherit;">
                            <div class="block-grid" style="margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #F0F0F0;line-height: inherit;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color: #F0F0F0;line-height: inherit;">
                                    <div class="col num12" style="min-width: 320px;max-width: 650px;display: table-cell;vertical-align: top;width: 600px;line-height: inherit;">
                                        <div style="width: 100% !important;line-height: inherit;">
                                            <div style="border-top: 18px solid #FFFFFF;border-left: 25px solid #FFFFFF;border-bottom: 18px solid #FFFFFF;border-right: 25px solid #FFFFFF;padding-top: 15px;padding-bottom: 5px;padding-right: 35px;padding-left: 35px;line-height: inherit;">
                                                <div style="color:#052d3d;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%;padding-top:15px;padding-right:15px;padding-bottom:10px;padding-left:15px;">
                                                    <div style="line-height: 14px; font-size: 12px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #052d3d;">
                                                        <p style="line-height: 40px; font-size: 12px; text-align: center; margin: 0;"><span style="font-size: 34px;line-height: inherit;"><span style="color: #fc7318; font-size: 34px; line-height: 40px;"><strong style="line-height: inherit;font-weight: bolder;"><span style="line-height: 40px; font-size: 25px;">Troubles? <br style="line-height: inherit;"></span></strong></span><span style="line-height: 40px; font-size: 25px;">We're here to help you</span></span></p>
                                                    </div>
                                                </div>
                                                <div style="color:#787878;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;padding-top:0px;padding-right:10px;padding-bottom:30px;padding-left:10px;">
                                                    <div style="font-size: 12px; line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #787878;">
                                                        <p style="font-size: 14px; line-height: 27px; text-align: center; margin: 0;"><span style="font-size: 15px;line-height: inherit;">Contact us at <strong style="line-height: inherit;font-weight: bolder;"><a href="#" rel="noopener" style="text-decoration: none;color: #2190E3;line-height: inherit;background-color: transparent;" target="_blank">{{ $get_email == null ? '' : $get_email->value }}</a></strong></span><br style="line-height: inherit;"><span style="font-size: 15px; line-height: 27px;">or call us at <span style="color: #2190e3; font-size: 15px; line-height: 27px;"><strong style="line-height: inherit;font-weight: bolder;">{{ $get_number == null ? '' : $get_number->value }}</strong></span> </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="background-color: transparent;line-height: inherit;">
                            <div class="block-grid" style="margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;line-height: inherit;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;line-height: inherit;">
                                    <div class="col num12" style="min-width: 320px;max-width: 650px;display: table-cell;vertical-align: top;width: 650px;line-height: inherit;">
                                        <div style="width: 100% !important;line-height: inherit;">
                                            <div style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-bottom: 0px solid transparent;border-right: 0px solid transparent;padding-top: 20px;padding-bottom: 60px;padding-right: 0px;padding-left: 0px;line-height: inherit;">
                                                <table cellpadding="0" cellspacing="0" class="social_icons" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;line-height: inherit;" valign="top" width="100%">
                                                    <tbody style="line-height: inherit;">
                                                        <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                            <td style="word-break: break-word;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px;line-height: inherit;border-collapse: collapse;" valign="top">
                                                                <table activate="activate" align="center" alignment="alignment" cellpadding="0" cellspacing="0" class="social_table" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: undefined;mso-table-tspace: 0;mso-table-rspace: 0;mso-table-bspace: 0;mso-table-lspace: 0;line-height: inherit;" to="to" valign="top">
                                                                    <tbody style="line-height: inherit;">
                                                                        <tr align="center" style="vertical-align: top;display: inline-block;text-align: center;line-height: inherit;border-collapse: collapse;" valign="top">
                                                                            <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $twiter->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="Twitter" height="32" src="{{ asset('twitter_2x.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Twitter" width="32"></a></td>
                                                                            <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $instagram->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="Instagram" height="32" src="{{ asset('instagram_2x.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                                                            <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $youtube->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="youtube" height="32" src="{{ asset('youtube.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                                                            <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $facebook->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="facebook" height="32" src="{{ asset('facebook_2x.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div style="color:#555555;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 12px; line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #555555;">
                                                      <div style="font-size: 12px; line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #787878;">
                                                          <p style="font-size: 14px; line-height: 27px; text-align: center; margin: 0;">
                                                            <span style="font-size: 13px;line-height: inherit;"><center>{!! $get_address == null ? '' : $get_address->value !!}</center></strong></span>
                                                            {{-- <span style="font-size: 13px;line-height: inherit;">3rd Floor WISMA SSK BUILDING,<strong style="line-height: inherit;font-weight: bolder;"></strong></span><br style="line-height: inherit;">
                                                            <span style="font-size: 13px;line-height: inherit;">Daan Mogot Rd No.Km. 11, RT.5/RW.4,<strong style="line-height: inherit;font-weight: bolder;"></strong></span><br style="line-height: inherit;">
                                                            <span style="font-size: 13px;line-height: inherit;">Kedaung Kali Angke, Cengkareng, West Jakarta City,<strong style="line-height: inherit;font-weight: bolder;"></strong></span><br style="line-height: inherit;">
                                                            <span style="font-size: 13px;line-height: inherit;">Jakarta 11710<strong style="line-height: inherit;font-weight: bolder;"></strong></span><br style="line-height: inherit;"> --}}
                                                      </p></div>
                                                    </div>
                                                </div>
                                                <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;line-height: inherit;" valign="top" width="100%">
                                                    <tbody style="line-height: inherit;">
                                                        <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                            <td class="divider_inner" style="word-break: break-word;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px;line-height: inherit;border-collapse: collapse;" valign="top">
                                                                <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 60%;border-top: 1px dotted #C4C4C4;height: 0px;line-height: inherit;" valign="top" width="60%">
                                                                    <tbody style="line-height: inherit;">
                                                                        <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                                            <td height="0" style="word-break: break-word;vertical-align: top;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif;color: #787878;line-height: inherit;border-collapse: collapse;" valign="top"><span style="line-height: inherit;"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>

