<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta content="width=device-width" name="viewport" />
    <!--[if !mso]><!-->
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <!--<![endif]-->
    <title></title>
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css" />
    <!--<![endif]-->
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
        }

        table,
        td,
        tr {
            vertical-align: top;
            border-collapse: collapse;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors=true] {
            color: inherit !important;
            text-decoration: none !important;
        }
    </style>
    <style id="media-query" type="text/css">
        @media (max-width: 670px) {

            .block-grid,
            .col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .block-grid {
                width: 100% !important;
            }

            .col {
                width: 100% !important;
            }

            .col>div {
                margin: 0 auto;
            }

            img.fullwidth,
            img.fullwidthOnMobile {
                max-width: 100% !important;
            }

            .no-stack .col {
                min-width: 0 !important;
                display: table-cell !important;
            }

            .no-stack.two-up .col {
                width: 50% !important;
            }

            .no-stack .col.num4 {
                width: 33% !important;
            }

            .no-stack .col.num8 {
                width: 66% !important;
            }

            .no-stack .col.num4 {
                width: 33% !important;
            }

            .no-stack .col.num3 {
                width: 25% !important;
            }

            .no-stack .col.num6 {
                width: 50% !important;
            }

            .no-stack .col.num9 {
                width: 75% !important;
            }

            .video-block {
                max-width: none !important;
            }

            .mobile_hide {
                min-height: 0px;
                max-height: 0px;
                max-width: 0px;
                display: none;
                overflow: hidden;
                font-size: 0px;
            }

            .desktop_hide {
                display: block !important;
                max-height: none !important;
            }
        }
    </style>
</head>

<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #F5F5F5;">
    <table bgcolor="#F5F5F5" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #F5F5F5; width: 100%;" valign="top" width="100%">
        <tbody>
            <tr style="vertical-align: top;" valign="top">
                <td style="word-break: break-word; vertical-align: top;" valign="top">
                    <div style="background-color:transparent;">
                        <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div style="width:100% !important;">
                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                            <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="10" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 0px solid transparent; height: 10px;" valign="top" width="100%">
                                                                <tbody>
                                                                    <tr style="vertical-align: top;" valign="top">
                                                                        <td height="10" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid two-up no-stack" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                                <div class="col num6" style="min-width: 320px; max-width: 325px; display: table-cell; vertical-align: top; width: 325px;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:25px; padding-bottom:25px; padding-right: 0px; padding-left: 25px;">
                                            <!--<![endif]-->
                                            <div align="left" class="img-container left fullwidthOnMobile fixedwidth" style="padding-right: 0px;padding-left: 0px;">
                                                <div style="font-size:1px;line-height:5px"> </div><img alt="Image" border="0" class="left fullwidthOnMobile fixedwidth" src="{{ url('/get/logo/from-storage/'.$get_img_astech->logo) }}" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 150px; display: block;" title="I'm an image" width="150" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col num6" style="min-width: 320px; max-width: 325px; display: table-cell; vertical-align: top; width: 325px;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:25px; padding-bottom:25px; padding-right: 25px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <div align="right" class="button-container" style="padding-top:10px;padding-right:0px;padding-bottom:10px;padding-left:10px;">
                                                <!-- <a href="#" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #F5F5F5; background-color: #E32142; border-radius: 14px; -webkit-border-radius: 14px; -moz-border-radius: 14px; width: auto; width: auto; border-top: 1px solid #E32142; border-right: 1px solid #E32142; border-bottom: 1px solid #E32142; border-left: 1px solid #E32142; padding-top: 3px; padding-bottom: 3px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:15px;padding-right:15px;font-size:14px;display:inline-block;">
                                                        <span style="font-size: 16px; line-height: 32px;"><span style="font-size: 14px; line-height: 28px;">My account</span></span>
                                                    </span></a>
                                                [if mso]></center></v:textbox></v:roundrect></td></tr></table><![endif] -->
                                            </div>
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                                <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                            <div style="color:#052d3d;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                <div style="color:#052d3d;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    {!! $replace_string !!}
                                                </div>
                                            </div>
                                            <!--[if mso]></td></tr></table><![endif]-->
                                            <!--[if (!mso)&(!IE)]><!-->
                                        </div>
                                        <!--<![endif]-->
                                    </div>
                                </div>
                                <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
                                <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                                <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div style="width:100% !important;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:20px; padding-right: 0px; padding-left: 0px;">
                                            <!--<![endif]-->
                                            <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 0px solid transparent; height: 0px;" valign="top" width="100%">
                                                                <tbody>
                                                                    <tr style="vertical-align: top;" valign="top">
                                                                        <td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div align="center" class="button-container" style="padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                <!-- <a href="#" style="-webkit-text-size-adjust: none; text-decoration: none; display: inline-block; color: #ffffff; background-color: #fc7318; border-radius: 1px; -webkit-border-radius: 1px; -moz-border-radius: 1px; width: auto; width: auto; border-top: 1px solid #fc7318; border-right: 1px solid #fc7318; border-bottom: 1px solid #fc7318; border-left: 1px solid #fc7318; padding-top: 5px; padding-bottom: 5px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; text-align: center; mso-border-alt: none; word-break: keep-all;" target="_blank"><span style="padding-left:20px;padding-right:20px;font-size:18px;display:inline-block;">
                                                        <span style="font-size: 16px; line-height: 32px;"><span style="font-size: 18px; line-height: 36px;"><strong>Verification </strong></span></span>
                                                    </span></a> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="background-color: transparent;line-height: inherit;">
                        <div class="block-grid" style="margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;line-height: inherit;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color: #FFFFFF;line-height: inherit;">
                                <div class="col num12" style="min-width: 320px;max-width: 650px;display: table-cell;vertical-align: top;width: 650px;line-height: inherit;">
                                    <div style="width: 100% !important;line-height: inherit;">
                                        <!--[if (!mso)&(!IE)]><!-->
                                        <div style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-bottom: 0px solid transparent;border-right: 0px solid transparent;padding-top: 0px;padding-bottom: 20px;padding-right: 0px;padding-left: 0px;line-height: inherit;">
                                            <!--<![endif]-->
                                            <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;line-height: inherit;" valign="top" width="100%">
                                                <tbody style="line-height: inherit;">
                                                    <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                        <td class="divider_inner" style="word-break: break-word;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px;line-height: inherit;border-collapse: collapse;" valign="top">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 100%;border-top: 0px solid transparent;height: 0px;line-height: inherit;" valign="top" width="100%">
                                                                <tbody style="line-height: inherit;">
                                                                    <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                                        <td height="0" style="word-break: break-word;vertical-align: top;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;line-height: inherit;border-collapse: collapse;" valign="top"><span style="line-height: inherit;"></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="background-color: transparent;line-height: inherit;">
                        <div class="block-grid" style="margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #F0F0F0;line-height: inherit;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color: #F0F0F0;line-height: inherit;">
                                <div class="col num12" style="min-width: 320px;max-width: 650px;display: table-cell;vertical-align: top;width: 600px;line-height: inherit;">
                                    <div style="width: 100% !important;line-height: inherit;">
                                        <div style="border-top: 18px solid #FFFFFF;border-left: 25px solid #FFFFFF;border-bottom: 18px solid #FFFFFF;border-right: 25px solid #FFFFFF;padding-top: 15px;padding-bottom: 5px;padding-right: 35px;padding-left: 35px;line-height: inherit;">
                                            <div style="color:#052d3d;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%;padding-top:15px;padding-right:15px;padding-bottom:10px;padding-left:15px;">
                                                <div style="line-height: 14px; font-size: 12px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #052d3d;">
                                                    <p style="line-height: 40px; font-size: 12px; text-align: center; margin: 0;"><span style="font-size: 34px;line-height: inherit;"><span style="color: #fc7318; font-size: 34px; line-height: 40px;"><strong style="line-height: inherit;font-weight: bolder;"><span style="line-height: 40px; font-size: 25px;">Troubles? <br style="line-height: inherit;"></span></strong></span><span style="line-height: 40px; font-size: 25px;">We're here to help you</span></span></p>
                                                </div>
                                            </div>
                                            <div style="color:#787878;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;padding-top:0px;padding-right:10px;padding-bottom:30px;padding-left:10px;">
                                                <div style="font-size: 12px; line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #787878;">
                                                    <p style="font-size: 14px; line-height: 27px; text-align: center; margin: 0;"><span style="font-size: 15px;line-height: inherit;">Contact us at <strong style="line-height: inherit;font-weight: bolder;"><a href="#" rel="noopener" style="text-decoration: none;color: #2190E3;line-height: inherit;background-color: transparent;" target="_blank">{{ $get_email->value }}</a></strong></span><br style="line-height: inherit;"><span style="font-size: 15px; line-height: 27px;">or call us at <span style="color: #2190e3; font-size: 15px; line-height: 27px;"><strong style="line-height: inherit;font-weight: bolder;">{{ $get_number->value }}</strong></span> </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style="background-color: transparent;line-height: inherit;">
                        <div class="block-grid" style="margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;line-height: inherit;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;line-height: inherit;">
                                <div class="col num12" style="min-width: 320px;max-width: 650px;display: table-cell;vertical-align: top;width: 650px;line-height: inherit;">
                                    <div style="width: 100% !important;line-height: inherit;">
                                        <div style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-bottom: 0px solid transparent;border-right: 0px solid transparent;padding-top: 20px;padding-bottom: 60px;padding-right: 0px;padding-left: 0px;line-height: inherit;">
                                            <table cellpadding="0" cellspacing="0" class="social_icons" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;line-height: inherit;" valign="top" width="100%">
                                                <tbody style="line-height: inherit;">
                                                    <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                        <td style="word-break: break-word;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px;line-height: inherit;border-collapse: collapse;" valign="top">
                                                            <table activate="activate" align="center" alignment="alignment" cellpadding="0" cellspacing="0" class="social_table" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: undefined;mso-table-tspace: 0;mso-table-rspace: 0;mso-table-bspace: 0;mso-table-lspace: 0;line-height: inherit;" to="to" valign="top">
                                                                <tbody style="line-height: inherit;">
                                                                    <tr align="center" style="vertical-align: top;display: inline-block;text-align: center;line-height: inherit;border-collapse: collapse;" valign="top">
                                                                        <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $twiter->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="Twitter" height="32" src="{{ asset('twitter_2x.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Twitter" width="32"></a></td>
                                                                        <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $instagram->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="Instagram" height="32" src="{{ asset('instagram_2x.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                                                        <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $youtube->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="youtube" height="32" src="{{ asset('youtube.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                                                        <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $facebook->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="facebook" height="32" src="{{ asset('facebook_2x.png') }}" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div style="color:#555555;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                <div style="font-size: 12px; line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #555555;">
                                                  <div style="font-size: 12px; line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #787878;">
                                                      <p style="font-size: 14px; line-height: 27px; text-align: center; margin: 0;">
                                                        <span style="font-size: 13px;line-height: inherit;"><center>{!! $get_address->value !!}</center></strong></span>
                                                        {{-- <span style="font-size: 13px;line-height: inherit;">3rd Floor WISMA SSK BUILDING,<strong style="line-height: inherit;font-weight: bolder;"></strong></span><br style="line-height: inherit;">
                                                        <span style="font-size: 13px;line-height: inherit;">Daan Mogot Rd No.Km. 11, RT.5/RW.4,<strong style="line-height: inherit;font-weight: bolder;"></strong></span><br style="line-height: inherit;">
                                                        <span style="font-size: 13px;line-height: inherit;">Kedaung Kali Angke, Cengkareng, West Jakarta City,<strong style="line-height: inherit;font-weight: bolder;"></strong></span><br style="line-height: inherit;">
                                                        <span style="font-size: 13px;line-height: inherit;">Jakarta 11710<strong style="line-height: inherit;font-weight: bolder;"></strong></span><br style="line-height: inherit;"> --}}
                                                  </p></div>
                                                </div>
                                            </div>
                                            <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;line-height: inherit;" valign="top" width="100%">
                                                <tbody style="line-height: inherit;">
                                                    <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                        <td class="divider_inner" style="word-break: break-word;vertical-align: top;min-width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px;line-height: inherit;border-collapse: collapse;" valign="top">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed;vertical-align: top;border-spacing: 0;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 60%;border-top: 1px dotted #C4C4C4;height: 0px;line-height: inherit;" valign="top" width="60%">
                                                                <tbody style="line-height: inherit;">
                                                                    <tr style="vertical-align: top;line-height: inherit;border-collapse: collapse;" valign="top">
                                                                        <td height="0" style="word-break: break-word;vertical-align: top;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif;color: #787878;line-height: inherit;border-collapse: collapse;" valign="top"><span style="line-height: inherit;"></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div style="background-color:transparent;">
                        <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #F0F0F0;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#F0F0F0;">
                                <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 600px;">
                                    <div style="width:100% !important;">
                                        <div style="border-top:18px solid #FFFFFF; border-left:25px solid #FFFFFF; border-bottom:18px solid #FFFFFF; border-right:25px solid #FFFFFF; padding-top:15px; padding-bottom:5px; padding-right: 35px; padding-left: 35px;">
                                            <div style="color:#052d3d;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:120%;padding-top:15px;padding-right:15px;padding-bottom:10px;padding-left:15px;">
                                                <div style="line-height: 14px; font-size: 12px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #052d3d;">
                                                    <p style="line-height: 40px; font-size: 12px; text-align: center; margin: 0;"><span style="font-size: 34px;"><span style="color: #fc7318; font-size: 34px; line-height: 40px;"><strong><span style="line-height: 40px; font-size: 25px;">Troubles? <br /></span></strong></span><span style="line-height: 40px; font-size: 25px;">We're here to help you</span></span></p>
                                                </div>
                                            </div>
                                            <div style="color:#787878;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;padding-top:0px;padding-right:10px;padding-bottom:30px;padding-left:10px;">
                                                <div style="font-size: 12px; line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #787878;">
                                                    <p style="font-size: 14px; line-height: 27px; text-align: center; margin: 0;"><span style="font-size: 15px;">Contact us at <strong><a href="#" rel="noopener" style="text-decoration: none; color: #2190E3;" target="_blank">Email@astech.co.id</a></strong></span><br /><span style="font-size: 15px; line-height: 27px;">or call us at <span style="color: #2190e3; font-size: 15px; line-height: 27px;">(<strong>021</strong>)</span> <span style="color: #2190e3; font-size: 18px; line-height: 27px;"><strong>123456789</strong></span><br /></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
                                <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div style="width:100% !important;">
                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                            <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 0px solid transparent; height: 0px;" valign="top" width="100%">
                                                                <tbody>
                                                                    <tr style="vertical-align: top;" valign="top">
                                                                        <td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top"><span></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="background-color:transparent;">
                        <div class="block-grid" style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;">
                            <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                <div class="col num12" style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top; width: 650px;">
                                    <div style="width:100% !important;">
                                        <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:60px; padding-right: 0px; padding-left: 0px;">
                                            <table cellpadding="0" cellspacing="0" class="social_icons" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td style="word-break: break-word; vertical-align: top; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                                                            <table activate="activate" align="center" alignment="alignment" cellpadding="0" cellspacing="0" class="social_table" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: undefined; mso-table-tspace: 0; mso-table-rspace: 0; mso-table-bspace: 0; mso-table-lspace: 0;" to="to" valign="top">
                                                                <tbody>
                                                                    <tr align="center" style="vertical-align: top;display: inline-block;text-align: center;line-height: inherit;border-collapse: collapse;" valign="top">

                                                                            <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $twiter->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="Twitter" height="32" src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1564284279/twitter_2x.png" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Twitter" width="32"></a></td>
                                                                            <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $instagram->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="Instagram" height="32" src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1564284280/instagram_2x.png" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                                                            <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $youtube->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="youtube" height="32" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA8FBMVEX////xWUvu7u7p6env7+/q6url5eXxW0z//v/xWEr///38///wWkv2n5jwSDX//v34q6bwTj3vPivyVknt///uOiXwUUHzdGfwW0/vycTs7uvyZFn5vLPu+vzvNB7q8fTo2dXqjIbxSDDyXEj+8u/6z8rr/vnwTEDt7PLvlYjwVD3wrKX2V07tSTXsT0HtPyjw0s70iX7twrvzdW37xMHn2tfxe3X/7OnquLD94Nr0dmr5s63ybFrtZ1765OL1oZn4vbjj5+L3oZ/qz836ycDugnjsFgDvkozr9fz7u7b3jYr0h3zsmY3soZntsafqva9W5U2pAAAU2klEQVR4nO1dC0ObMNeGCguBGAoBRBxlK63ttK23aeecur3Or5ube///v/lOaKddbVOurXvn2a1rBPJwknNPItWmSKmp0p+kwnd/cyuQ9IJw/b18QfiC8AXh+nv5gvAF4b+FsFbbmPnpjb++9V9D+DxHWtFRqjzQwvfx17YmJG1MkbrxauanX8F3f3MrkJT74r+ldZbWP7TKbp2l5yAeym2dpech4sts/fcQPs+RVuYofZ69fEH4gvA59ON/QVtM+gF/BSXf+bloCzUAoyPgxJHOvun/DW1RG7OP/1n3KK3EPgyicEJOGEa1lT13lqqz8UPl9OZwm9Phm1dKtOBatXzfYtqVKuPWmqQbk9YgioBfwZvt/beXg4MGkJsQ/9SwB5dv929OVfiJKAqSa3VJMrTikGb9w9J9fMPUNF0ydSl0otPtt2c+bTap5TGMMZEJJ1mGz4xZ0ND3z0bbVzByJV3XkwuNvM+d0zr28afFUBlTHDgIGA3DHH4Y+bZLrZj0iIyQzLCcQAOggFGWCfL9Ho4t6jb90YehBJfohgEY8z13fmutglibpgM85by+ZdG+B0wDVDGHxmQZyQk0+A98TvAi3oqJ16fWx/q5AgBNLedzF7VWEU00WhefLNuDnnNIBPAB0zBwMYE2IcTB868JjF3Ex6xtXV60jALPTYMw/3gI4NeGZEIHW+2tRpM9QklPmB00PrYBJIzUDUXpFu9VmQj5vwGwb1i/tCliOfBxYj61E0ZqQbcbdAv3qlQeKrUgkq42rQPL9xnKBxD7zPf3DrzrKykC2y54VggBn3N6NGgy5IM6yDNGxxgxln3WHByd7kZBCb0qk4dOMLKol+B7EJnZAYIAYtj3B9QaBU5UAcLsUov7Ql2wOIN7m+YdnHMIYWrfB043SCCsUVuoCjcealG47dO4l3tsziEse3RnOwxhfitPubRCbQEdUCPny629J/fK4yAn1CNW83bDAXzBImc65SgtaJcGu+GvTh/LMWJxiQD5jPRx0/tvGCpdZS4PV5N7UoPd6Mz2cGKs+CUi9MHSwXyo3oa7qrqe3JOqbnwBFf++38mr35cTQqxjfTX4q1Qfnryq3BMXoqqmXVMvl4WWimCoyuBj1XWY8KoSrD731A1bZy7X7iiv/ltKCCESuz9aYU3ZyGvizOVhGkEcdJ0b1kncI1IZQo6xF1P0ZqwaU6mHsqKJSuAcWx3io8rGKPcnYaAyxJBlbTuBkqJXJY3SJKYbOfeuV64KFEBl7n6ocFO8u5qoPjwpCut2dTL0KcSBvQ/2zcSKqxwhWBmhc2TH8qpYCI6jjO13TqR2lZUghCEavHPlmKyOiaQXM3ekhKKRVeYojZx3NjfTKhQyM4R4OMv+6Yw1XqXagtv6UfjWrVA/LCb3XRh152AoU1uoihoEzr2Ny7Sy01LM7H3golqptgCXt+uAFI3XwsMYA8RutbkncAid4wZj/go1xQNhH/TitqNmdKZmSWynf9kIb/Y8n8irEzLTCLHveW/CJw5RWbknlbdqLdZZnRp8SsjyW9yZmupWebknMJpUSTvrrGUKPkKkRyYIvKCC3JPCW41rN17HAH0kHLt1Q1UVJU2fE0ofawNjbfc9ZXidg5Qb4XG/vRtMgygrmhgo6m5EvbhKdykFxYhYFhjhORGKOR6FZxXGZNIT7tyGQTedy58FIbi8v+gKTVEBMfohTJnVyIIwiL50vCpjMimJ51332EZUPg+j8JbiamMyaQgBQuI3j8LyETrbNi4z5psbYRK8sbfDXAgXak/u9PreWmy1eYR8ayd4CE2Voy3AZaJx/sxnyYSRT/edoDtOE5egLXiGMHD3emtWhY9EUC+2o26iMkrJPXFNMaJg2j8XhBimo33vBMKZlSH3pG4E0anFC7WKG2yIU3GIYFnhzqsoUMvLPR01xf2KWS8pDfoNhGDG4HdC3pisPyn5bjAYeOMf4uODJPFJjHiBEZD4iah5t6TPC2iev6xLV4OB+KXyWjUUM4DRp00biAL1LYAApjra4bT1JyXf+QgzQGlZ/Mf5ZU24yPLi2F/O6gEbSubCPi+m+cV4m/YyVYhi2rCty7O7ev3i++ev7fbh+dWwZZpLHsfJNFvDYZvT5+8XF/W7sx3LbaSQ3Af3k0LGEqKJw068BGFMB6O22TIlY+Z+vKjSnAAx53yC9pkrDN7+9W5Al8TzfG/QEvQ5G8L6wRJFgRs/h5P+6WMESx4zh/hFpjSpv9Wl4bvGkmci+2KqGDU1wlmOa7omtXb2BJUWIGEoHhqangPVQgLOGleY+gJxQ+S9yxav8jOKRfUNYErb7gkmBfbp1pCXvs4OtyJk8CrVq0sqcGWITOy2ZGiGVgghMEYztigWjFLfwi1D06BPZSI0+S2HcWfhYwGiT295WXmxzIwBTGw1kEg5MXpocIGhzQqZQgg1fkuj3RTMRcJIo2UYRXNPmmlc2MxfnKhA9CO8cNPUjDLnoWnA9IDX+5EufLWgaIn9mku2YtpCk8xPC4MzvIIZ21eGVOYUnIIpGec8y7V4LjJ4u1pBbWFI51QwUMC02DKNJ1qwHIL31vrEiwUWQsQevN6iuSejbgsQglKqw0Apc4D+gdC4tsHGXSwEQCVmlTSzNp6mb1mLEQLExrCiMSpxI0A6b4jkuGydwNML5p6Gg8XWExjVnQZfP1IRRh1u3BB6GXvcciuWewo/UEEUGPv9j9WAe6BLCwvMjZh++PZlts+Zck9B+La/WJRhGdN6xQg3qdBRtH6GwcwV2XJPUeh7ywynaum9jUVRaG8nLBZNjE4FkpTHSypH2LaJqPYfN09nlzJmiyaG2y5efP8Yy/3zqhE2ZUEPiOwehhkQPh2lzogK5jnxcecqRS+5TOSObR7D4KojnIcxHc0izBbVD88swRghciwPU/RS07mVaUhaDvdjyIQZPWLdFkMY+LG8WB8imOipIjH6+3PwII08TGztDET+BY53ZmVpNoRv+sKAEPZO0vRSM0b2XSsfD/UtMULZelMI4TbtiQJCsXeWppem9J8mZRd6LvvuxIsXT8Q47tGbDAifytL9pi/I+2IEXnYq2qSIuZ3PJnfeYdDqGZzJM0sgamKG6K+ZC7Jpi7fUF41SRI/SIoTp7NmXbcCm80BT+hl5K0JIoAujDAifjtJLCwmyhujp7Rcj7MWMeI2zc85DI0PI444K3zGyZkVBttyTxZAg44SQvZkaYRwjBMO6eTQWq6kRjkQIoWlAZi7Ilns6YKKUGpHt69QIYazBb8SoXc+kNTapoAyLyD1GZy5InXtSVTWKGsLQOg87p0Y4xXi33oKhysWOqS+NgNRFORMkE3YQBrysMEfuSalFijiyngshDwPa/mseTQP1aCxlJyAUd8INoppgos3S1DRVgui0EoSMyIPGzlce2JaWxwfqtjhpgt3TSCQsZ2m6VC+IDpchdF/nQAjXeWTP/niepGGWyZwlPMTcu5iu5EsfTQyU4NuhK35/eRFin4Eio0c8PVYUIXG3Q6UWpEY4XcgWgHcoLIEC7ywXQk4+IZg27pZH6uquwHvCfKYch7WpMtgsUf0aRyh6f0UQEhYjzA7c10tlqXgcTRCmn4ePCLvVIpR9H3uu/7pVHOGHUBGUKi5GWDUPcWwPLlrLk3LV8bDaeYhws3E9BJ24XB+K5iGXNBnn4YwsbcD7E8RLkfs9G0LGb4YY2G70XSvdpWJZCqauuz2FMFvuKQhvGkIeZkfIRSjCPqUnQ+NhY69lCEWjFPgL+jALwmmbphZtNLBoHSX23Yw2DeZbl/QbH9uGoetaKpcfEIrecj6bZtouFQQrcyCEQeW5W1/BJNWkFHNwjFAwSvliaDfpa57cU+JbiBGirAhl3LC+JzvP6SkH6RKEmPsWwkVCS/xDvpxZFC+199N0Ujc2KUGkB06Fd5GOc490LcorgM+51D8U+/gDJq5cT+cBa9I1xczH1L4eGulm3yPNMRamRpFMlvr4U7Izc5wGrOd3qRAam/2YUPfuSkpi35nojormCVkep6n9QXNibSIWEnonpaL/0I59e85j+1rWsoYfliCKAQgzxdrmxksFogaTfqpoIvDw/7xz0wB3l5cWZUN4Zol4iP3mhwwIn47Sm6ZodyQcW6li3pJ+/TkbrGmEHYFtjGKWKeY9J28hKsQA6zldVF/XzKyz75E+itZ0Ysys00IIVT8W2aW8XigNQl3LXfXW2mHClUh7O7Mb9mbOH4pUPlg1aexnLbOKeKShLMofgigtmD/cHYkkGQF9myZDqidWWj666oi8GxbTzd0MCOfn8QUIY59Wncc/7IsMR8Lcw2KVCtGpLQrrM9SsvhZDHNCkG1kQzslyh75oJyFWfT1N2yaiAuUU9TRiu1QNR9ZihBjhZkoXODdd8KzV4ndMN51lNVHL69qEtnc/ZQIxN531fQFATI+X1rUJmjlgfTgQSGuCOlQqt0p/inSuZA4GIhffG+iF1z0ZW95igJjghr48MJ+T+AqcVgOLaqL6Z+BIF1r3ZJqiGmG+317js5Ey4JIDoW68dkUA5aSUvVAlO3DnvC9AiIl1BDZZRVXQhqSfecJwpjcEk7BYJbtmmFuLJyKvkrBaRqkLgh5JM4whZbLAtRicgEtWeN2TcXGA5cUqCdnXVSEEJo4WhxIRXyr73ZjT52wI4SmthhwL5KnXG+Z3jYQELPQWizlEGGpwi7cgQvB7pI/CsiG/eVfqcplH0o2jpiCIQnz6Q9Kkggh5oh0MJ18UUozd+2pkqbHpigDiUtaujdcfXu71Fj+J55lTRdwy0x1MQoG67+1d8rFjZlx/OFcuXRyIq9uw7W9rPEhv8tIDna995atB+dK+5FMSgOI3GjuJ3Fcc/9ecfJm0mvAydX4HHo4zjMNLW7gt3GOZRAmrZIcdwaYfCGw37Nlnn1sPwZik++D0mknPJ84vYJ2Yd+ZkebDJa2p4ioaH+ZNLxpfz5ZrnI6spLNKXkcdyrQOey3HhWm4CHipixHIblz+uv78/PB+a090VFgU9No0/tIbn5+33r6+POi5lCImTv8mKq4V9XkjzbDxTuhKZ39y08fmye2ZRu2lTyxtgf2fn08nJ2Y8fo9Hm5vX310Dv379vtw85JSvT33Oq16+vN4+Ozk5OPu3s+PKgA3eAX33q8QMIxHupeGxo5lmPP99ON44Wr3Tkxim4wsDGGPkk5gE+vo1CpzPeR2C8kwCQ+0Djz5Ovk60J+uNNFpIyQcYPaAFdxw8VEhwkgUBJ6aI+PzJN7B+Oj5hK9sXwly3Kn+1DFsp0ZxL7iA02ouBLSXvuBV2+t4mc/9yKsonIuNe8350c4VbCnnuBWvsW2XFv7btg/SbG96cJu0EtKmvPPXhVzr4tML9XTDiW6S8nmuyjVMIuSkotCKJgx/KfCw9xz/oUBMGkAqOsPfec7Way3cia903kLxkTZh/m2+tLuOdeeER9njdfLyPhJRPiN9+Gi4ugcp8V9O2LtyeLzYxVEHjjaA/XKthzTwmc/4qWBK+MwLygx1Xsm9gNuuFtZ90c5IQ7R46Sc+9LsVwKwsjyREmElRAhe970URDl7dCqKKqy2+7Ha5Y0su9Z7VCZZlNpO7QCRNWou6Kg1Coodi+MWq2WYR6msEuTVjDdAlUybxc7GSshwveC3uCdTdHnVLmn6Zo4fu5SSxZk26onRCf7eW8EafqcJvf0J33ZCN9Y3rqETRyD23vlZDzgapaWnPcUdHe3XTDt1yFucI947qGz4PSHvLmnp61d8DLWcvhDcojHd6dbU7IdNDeXhwJBrCoKQCz1FLm0hBjfljV4irDUEzzgHXWjcOSuAaCM3Z8O55Ja7IySNGcFBc7ooCdYQl4+xdjHsnvPOVgres5MqvOeQmXTZfEKxQ3xmdy4DyNlqYovB2E3CJ13qzzQSo4ZsX86UbCi855AngZBuG8LiyTKJTSwf4Ga6Ip6VSLChI2Rs++uDCLy3F9OMkSXbv5c3umAoDS2OxbBhI2D3lUQSRIGmJA+44o+Ra/SyNL0B9F2nRtE46o32ieyH1P/qsTzD1Mf4qwqatg6cj1EkHDBQjF8CPmxe9QCDj44vdWe9zTdqnJnqm5ZTK4K4fgcUuvCNJKup+pV0fOeHlu741bjq9UR12EXIYJYx2sbY3cplYtX6Lynea2Buhve0j2+vkpUupSdfJ6j832LHjlh5l7lPe9pTis/aE4J/8soD2KWehwiAwnWw018zA8ELHim87QYyjqJ+dnqShBuHB14pCcqA81MCO5nHbythYmAKXYu9x+SNuvZ6mNDPAq3P1GvVCsu7u3RT4eg5bvlnq2e/fT4REsF0W6wbzdLODrhNyFm019g4IMJzPlRHsLMp8c/tEZOeO81Bz6Kee4kr/LAPM2MiexRdh86eRVg7syMuDXoOq/eMdtDvSJWDiG4F3tN9u6V01WeF0IwcZRIGt4P7D3Zzz1cEfPlPXdwPZQiJVDzHsRdGQ+7ARgArYtLm+aONmKf2pff+U4S/HZR8V6VibDGT0fk+16b7R8NmwmX8c/nH8HMbvxo8zI4E+4cpIzbV6stFrTqrz8ObJ7DQThm3C6Px2esTBOBr3FM+E5WvJwGe9QenHx+qOJUS+tV+Qg1Xldonl+cdCj1MAaA8fh86z8EUFLwxCNMGMEfj1J29uvclB62jcr1ZlMgLDIeHo5s0cZHN0it45871KXWHu5xhiUqBOHfR+RgzjrSI7EFP/Np87iVVFzqxqTsP/tz57fWMuSe0rfqpqbx6spvYXh6OLrdAQ5Ri9esYV5lQCYnAbEBL3vzdm5/Hm6E4TdenqqZMADSFONls0sL+RZzWvleEOOawS9qEIElEJze/Pp5IlP3oVwv+dQnJz8/3JwG4W4YqcHvtUu/17Jnf+781sy5pzSt6oJWgHq4fXz84fh4+xCAzb+zWtaLXkzlDI95rcDQcEzfoihavKSu7OfOUllTfK5DMIm1FHOIVhVNzN6qjruRKHKwyVb23JUi5M6W6MC7lSCsbrSsq/UF4d/f+oLw72+dpepk2rpa/z2Ez3OklT5KK7EP19U6S2W5Lc+ntXT/8Hm1bhTMPT331uK5p+ffWqsmmvisWv81hM9zpD2XqP7zbH1B+Dx6+YLwBeELwvX38gXhP4/w/wEBQ0d+tjhIJAAAAABJRU5ErkJggg==" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                                                            <td style="word-break: break-word;vertical-align: top;padding-bottom: 5px;padding-right: 8px;padding-left: 8px;line-height: inherit;border-collapse: collapse;" valign="top"><a href="{{ $facebook->value }}" target="_blank" style="line-height: inherit;background-color: transparent;"><img alt="facebook" height="32" src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1564284280/facebook_2x.png" style="text-decoration: none;-ms-interpolation-mode: bicubic;height: auto;border: none;display: block;line-height: inherit;border-style: none;" title="Instagram" width="32"></a></td>
                                                                        </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div style="color:#555555;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                <div style="font-size: 12px; line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #555555;">
                                                  <div style="font-size: 12px; line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #787878;">
                                                      <p style="font-size: 14px; line-height: 27px; text-align: center; margin: 0;">
                                                        <span style="font-size: 13px;">PT ASTech<strong></span><br />
                                                        <span style="font-size: 13px;">3rd Floor WISMA SSK BUILDING,<strong></span><br />
                                                        <span style="font-size: 13px;">Daan Mogot Rd No.Km. 11, RT.5/RW.4,<strong></span><br />
                                                        <span style="font-size: 13px;">Kedaung Kali Angke, Cengkareng, West Jakarta City,<strong></span><br />
                                                        <span style="font-size: 13px;">Jakarta 11710<strong></span><br />
                                                  </div>
                                                </div>
                                            </div>
                                            <table border="0" cellpadding="0" cellspacing="0" class="divider" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;" valign="top" width="100%">
                                                <tbody>
                                                    <tr style="vertical-align: top;" valign="top">
                                                        <td class="divider_inner" style="word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                                                            <table align="center" border="0" cellpadding="0" cellspacing="0" class="divider_content" height="0" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 60%; border-top: 1px dotted #C4C4C4; height: 0px;" valign="top" width="60%">
                                                                <tbody>
                                                                    <tr style="vertical-align: top;" valign="top">
                                                                        <td height="0" style="word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #787878;" valign="top"><span></span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
