@extends('admin.home')
@section('content')
<form id="form-user-create" style="display: contents;">
<div class="col-md-4">
    <div class="card">
        <div class="card-header-tab card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                User Infoasdasd
            </div>
        </div>
        <div class="card-body">
            <label class="label-control">Name</label>
            <div class="form-group">
                <input name="name" placeholder="Name" type="text" class="form-control" required/>
            </div>

            <label class="label-control">Email</label>
            <div class="form-group">
                <input name="email" placeholder="email" type="email" class="form-control" />
            </div>

            <label class="label-control">Phone</label>
            <div class="form-group">
                <input name="phone" placeholder="628" type="text" class="form-control phone" />
            </div>

            <label class="label-control">Password</label>
            <div class="form-group">
                <input name="password" placeholder="password" type="password" class="form-control" required/>
            </div>

            <label class="label-control">Password Confirmation</label>
            <div class="form-group">
                <input name="password_confirmation" placeholder="password confirmation" type="password" class="form-control" required/>
            </div>

            <label class="label-control">Access</label>
            <div class="form-group">
                <select class="form-control" id="role" name="role_id[]" style="width:100%" multiple required>
                    <option value="">Choice Role</option> 
                </select>
            </div>
        </div>
    </div>
</div>
<div class="col-md-8">
    <div class="card">
        <div class="card-header-tab card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                User Address
            </div>
        </div>
        <div class="card-body">
            <label class="label-control">Type</label>
            <div class="form-group">
                <select class="form-control" id="address_type-select" name="ms_address_type_id" style="width:100%" required>
                    <option value="">Select Type</option>
                </select>
            </div>

            <div style="display: none;">
            <label class="label-control">Village Name</label>
            <div class="form-group">
                <select class="form-control" id="village-select" name="ms_village_id" style="width:100%">
                    <option value="">Select village</option>
                </select>
            </div>
            </div>

            <div class="form-group">
                <input type="text" name="searchMap" style="margin-top: 7px; width: 70%; border:2px solid #4DB6AC;" id="searchMap" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="control-label">Maps</label>
                <div id="map" style="height:400px"></div>
            </div>
            <div class="row" style="display: none;">
                <div class="form-group col-md-5 col-xs-5 mr-4">
                    <label class="control-label">Latitude</label>
                    <input size="20" type="text" id="latbox" class="form-control"  name="lat" onchange="inputLatlng('new')" required>
                </div>
                <div class="form-group col-md-5 col-xs-5 mr-4">
                    <label class="control-label">Longitude</label>
                    <input size="20" type="text" id="lngbox"  class="form-control"  name="long" onchange="inputLatlng('new')" required>
                </div>
            </div>

            <div class="position-relative row form-group">                
                <div class="col-sm-10">
                    <div class="col-md-10" style="display: none;">
                        <div class="row">
                            <div class="form-group col-md-5 col-xs-5 mr-4">
                                <label class="control-label">City / Regency</label>
                                <input size="20" type="text" id="cities" class="form-control"  name="cities" readonly>
                            </div>
                            <div class="form-group col-md-5 col-xs-5 mr-4">
                                <label class="control-label">Province</label>
                                <input size="20" type="text" id="province" class="form-control"  name="prv" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <label class="label-control">Address</label>
            <div class="form-group">
                <textarea name="address" class="form-control" id="detail-address-input" rows="3" required></textarea>
            </div>
            
            <div class="position-relative">
                <button class="btn btn-success" id="submit" style="float: right">Save Data</button>
            </div>
        </div>
    </div>
</div>
</form> 
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
@include('admin.template._mapsScript')
    <script>
        $('.phone').mask('628 0000 0000 00');

        globalCRUD
            .select2("#role", '/role/select2')
            .select2("#status", '/user_status/select2')
            .select2('#address_type-select', '/address_type/select2')
            .select2("#village-select", '/village/select2', function(item){
                return {
                    text: item.name + ' ' + item.district.name + ' ' + item.district.city.name + ' ' + item.district.city.province.name + ', ' + item.district.city.province.country.name,
                    id: item.id
                }
            })

            $('#form-user-create').submit(function(e) {
                globalCRUD.handleSubmit($(this))
                    .storeTo('/user')
                    .backTo(function(resp) {
                        console.log(resp)
                        return "/admin/user/detail/" + resp.data.data.user.id;
                    })
                e.preventDefault();
            });
    </script>
@endsection
