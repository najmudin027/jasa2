@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form action="#" method="post" id="form-user-info">
                <input type="hidden" value="{{ $user->id }}" name="user_id" id="user_id_val">
                <input type="hidden" value="{{ ($info == null) ? 0 : $info->id }}" name="id">
                <label>First Name</label>
                <div class="form-group">
                    <input name="first_name" placeholder="first name" type="text" class="form-control" value="{{ ($info == null) ? '' : $info->first_name }}" />
                </div>

                <label>Last Name</label>
                <div class="form-group">
                    <input name="last_name" placeholder="last name" type="text" class="form-control" value="{{ ($info == null) ? '' : $info->last_name }}" />
                </div>

                <label>Place Of Birth</label>
                <div class="form-group">
                    <input name="place_of_birth" placeholder="place of birth" type="text" class="form-control" value="{{ ($info == null) ? '' : $info->place_of_birth }}" />
                </div>

                <label>Date Of Birth</label>
                <div class="form-group row">
                   <div class="col-md-3">
                        <input name="date_of_birth" placeholder="date of birth" type="date" class="form-control" value="{{ ($info == null) ? '' : ( $info->date_of_birth != null ? $info->date_of_birth->format('Y-m-d') : '' ) }}"/>
                   </div>
                </div>

                <label>Marital Status</label>
                <div class="form-group">
                    <select class="form-control" id="marital-select" name="ms_marital_id" style="width:100%">
                        <option value="">Select Marital Status</option>
                        @if ($info != null)
                            @if ($info->marital != null)
                                <option value="{{ $info->marital->id }}" selected>{{ $info->marital->status }}</option>
                            @endif
                        @endif                        
                    </select>
                </div>

                <label>Religion</label>
                <div class="form-group">
                    <select class="form-control" id="religion-select" name="ms_religion_id" style="width:100%">
                        <option value="">Select Religion</option>
                        @if ($info != null)
                            @if ($info->religion != null)
                                <option value="{{ $info->religion->id }}" selected>{{ $info->religion->name }}</option>
                            @endif
                        @endif                        
                    </select>
                </div>

                <label>Gender</label>
                <div class="form-group">
                    <select class="form-control" name="gender" style="width:100%">
                        <option value="">Select Gender</option>
                        @if ($user->info == null)
                            <option value="0" >Male</option>
                            <option value="1" >Woman</option>
                            <option value="2" >another one</option>
                        @else
                            <option value="0" {{ ($info->gender == 0) ? 'selected' : '' }}>Male</option>
                            <option value="1" {{ ($info->gender == 1) ? 'selected' : '' }}>Woman</option>
                            <option value="2" {{ ($info->gender == 2) ? 'selected' : '' }}>another one</option>
                        @endif
                                              
                    </select>
                </div>
                
                <div class="position-relative">
                    <button class="btn btn-success btn-sm" id="submit"><i class="fa fa-plus"></i> Save</button>
                </div>

            </form>        
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        globalCRUD
            .select2("#religion-select", '/religion/select2')
            .select2("#marital-select", '/marital/select2', function(item){
                return {
                    text: item.status,
                    id: item.id
                };
            })

        // submit event
        $('#form-user-info').submit(function(e) {

            globalCRUD.handleSubmit($(this))
                .storeTo('/user/info')
                .backTo(function(resp) {
                    return '/admin/user/detail/' + $('#user_id_val').val();
                })

            e.preventDefault();
        });
    </script>
@endsection
