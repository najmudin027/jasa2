@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add-dev-plant">Add Development Program</button>
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-dev-plant" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>User Name</th>
                        <th>Curr Sequence</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
            <button class="btn btn-primary btn-sm add-dev-plant">Add Development Program</button>
        </div>
    </div>
</div>
@include('admin.setting.educations.developmentPlant.modalDevPlant')
@endsection

@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $('.user_id').select2();
    });

    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    globalCRUD
        // .select2("#user_id")
        .select2("#curr_sequence_id", '/sequence/select2',  function(item){
            return {
                id:item.id,
                text : item.sequence_name,
            }
        })

    globalCRUD.datatables({
        url: '/development_plant/datatables',
        selector: '#table-dev-plant',
        columnsField: ['id', 'status', 'date_time', 'user.name', 'curriculum.sequence_name'],
        modalSelector: "#modal-dev-plant",
        modalButtonSelector: ".add-dev-plant",
        modalFormSelector: "#form-dev-plant",
        actionLink: {
            store: function() {
                return "/development_plant";
            },
            update: function(row) {
                return "/development_plant/" + row.id;
            },
            delete: function(row) {
                return "/development_plant/" + row.id;
            },
        }
    })
</script>
@endsection
