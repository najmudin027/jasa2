{{-- preview image --}}
<script>
    function imageLogos(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#imageLogo').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#imageIcon').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image_logo").change(function() {
        imageLogos(this);
    });

    $("#image_icon").change(function() {
        readURL(this);
    });
</script>

<script>
    var webSettingObj = {
        //insert
        insert: function(data){
            $.ajax({
                url : Helper.apiUrl('/web_setting'),
                data : data,
                type : 'post',
                contentType: false,
                processData: false,
                success: function(resp) {
                    webSettingObj.successHandle(resp);
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            });
        },

        // hadle ketika response sukses
        successHandle: function(resp) {
            // send notif
            Helper.successNotif(resp.msg);
        },
    }

    // submit data
    $('#save').click(function(e){
        // alert('asd')
        var data = {};
        var fd = new FormData();
        var id = $('input[name="id"]').val();
        var logo = $('#image_logo')[0].files[0];
        var icon = $('#image_icon')[0].files[0];

        if(logo){fd.append('image_logo', logo);}
        if(icon){fd.append('image_icon', icon);}

        fd.append('id', id);
        fd.append('app_name', $('input[name="app_name"]').val());

        if (id == '') {
            webSettingObj.insert(fd);
        }
        // } else {
        //     webSettingObj.update(data);
        // }

        e.preventDefault();
    })
</script>
