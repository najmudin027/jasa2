@extends('admin.home')
@section('content')
<style>
    .btn_login { 
        background-color: #26C6DA;
        border: none;
        padding: 10px;
        width: 200px;
        border-radius:3px;
        box-shadow: 1px 5px 20px -5px rgba(0,0,0,0.4);
        color: #fff;
        margin-top: 10px;
        cursor: pointer;
    }
</style>

<form id="form-web-setting">
    <div class="col-md-12 card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Web Setting Management
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                {{-- <a href="{{ url('/create-vendors') }}" class="mb-2 mr-2 btn btn-primary btn-sm" style="float:right">Add Vendor</a> --}}
            </div>
        </div>
    
        <div class="card-body">
            <div class="card-title">
                <h3 style="color:#00b1b1;" align="center"><strong>   
            </div><br>
    <input type="hidden" name="id" id="id">
            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Aplication Name</label>
                <div class="col-sm-9">
                    <input name="app_name" id="examplename" placeholder="Distric" type="text" class="form-control">
                </div>
            </div>
    
            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Image</label>
                <div class="col-sm-6">
                    <input name="image_logo" id="image_logo" placeholder="Distric" type="file" class="form-control">
                </div>
                <div class="col-sm-3">
                    <img id="imageLogo" src="http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png" alt="your image" style="width:100px;height:100px"/>
                </div>
            </div>
    
            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Icon</label>
                <div class="col-sm-6">
                    <input name="image_icon" id="image_icon" placeholder="Icons" type="file" class="form-control">
                </div>
                <div class="col-sm-3">
                    <img id="imageIcon" src="http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png" alt="your image" style="width:100px;height:100px"/>
                </div>
            </div>
    
            <button type="submit" class="btn btn-primary waves-effect" id="save">Save</button>
        </div>
    </div>
</form>

@include('admin.template._mainScript')
@include('admin.setting.webSetting.js')
@endsection
