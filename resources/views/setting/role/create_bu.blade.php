@extends('admin.home')
@section('content')
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2020.1.219/styles/kendo.default-v2.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
<div class="app-main__inner">
    <div class="row">
        @include('admin.template._minisidebad')
        <div class="col-md-9">
            <div class="main-card mb-3 card">
                <div class="card-header-tab card-header">
                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                        {{ $title }}
                    </div>
                    <div class="btn-actions-pane-right text-capitalize">
                        
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="container">
                        <div class="card-body">
                            <form action="{{ route('admin.role.store') }}" method="post" id="multi">
                                @csrf
                                <div class="position-relative row form-group">
                                    <label class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input name="name" placeholder="enter name" type="text" value="{{ old('email') }}" class="form-control @error('name') is-invalid @enderror">
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="examplepermission" class="col-sm-2 col-form-label">permission</label>
                                    <div class="col-sm-5">
                                        
                                        <select name="" id="select-from" multiple="multiple" size="15" class="form-control">
                                            @foreach ($permissions as $nameGrup => $group)
                                                <option selected disabled style="font-size:16px" align="center">{{ $nameGrup }}</option>
                                                @foreach ($group as $parent)
                                                    <option value="{{ $parent->id }}">{{ $parent->name }}</option>
                                                @endforeach
                                            @endforeach
                                        </select><br>

                                        <a href="javascript:void(0);" id="btn-add" name="btn-add" class="btn btn-success btn-block">
                                            Move<span class="fa fa-chevron-right pull-right" aria-hidden="true"></span>
                                        </a>

                                    </div>
                                    <div class="col-md-5">
                                        
                                        <select id="select-to" multiple="multiple" name="permissions[]" size="15" class="form-control"></select><br>
                                        <a href="javascript:void(0);" id="btn-remove" name="btn-remove" class="btn btn-danger btn-block">
                                            Back<span class="fa fa-chevron-left pull-left" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="position-relative row form-check">
                                    <div class="col-sm-10 offset-sm-2">
                                        <button class="btn btn-primary" style="float:right">Save</button>
                                    </div><br><br>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@include('admin.setting.role.js')
@section('script')
<script src="https://kendo.cdn.telerik.com/2020.1.219/js/kendo.all.min.js"></script>
<script>
    $(document).ready(function() {
        $('#btn-add').click(function(){
            $('#select-from option:selected').each( function() {
                $('#select-to').append("<option value='"+$(this).val()+"' selected>"+$(this).text()+"</option>");
                $(this).remove();
            });
        });

        $('#btn-remove').click(function(){
            $('#select-to option:selected').each( function() {
                $('#select-from').append("<option value='"+$(this).val()+"' >"+$(this).text()+"</option>");
                $(this).remove();
            });
        });
    
        $('#multi').submit( function () {
            var selectalla = $('#select-to').val();
            if(!selectalla) {
                $('#select-to option').attr('selected','selected');
            }
            var selectallb = $('#select-from').val();
            if(!selectallb) {
                $('#select-from option').attr('selected','selected');
            }

        });
    });
</script>
@endsection