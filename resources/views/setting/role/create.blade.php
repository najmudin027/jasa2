@extends('admin.home')
@section('content')
<link rel="stylesheet" href="https://kendo.cdn.telerik.com/2020.1.219/styles/kendo.default-v2.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form action="{{ route('admin.role.store') }}" method="post" id="multi">
                @csrf
                <label>Name</label>
                <div class="form-group">
                    <input name="name" placeholder="enter name" type="text" value="{{ old('email') }}" class="form-control @error('name') is-invalid @enderror">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <label>Permission</label>
                <br />
                <input id="" type="checkbox" class="permission-checkbox-all"> Check All
                <hr />
                <div class="row">
                    @foreach ($permissions as $nameGrup => $group)
                    <div class="col-md-3" style="margin-bottom: 10px;">
                        <div class="card">
                            <div class="card-body">
                                <input id="" type="checkbox" class="permission-checkbox-parent" data-id="{{ $nameGrup }}">
                                <label>{{ $no++ }}. {{ str_replace('_', ' ', ucfirst($nameGrup)) }}</label>
                                @foreach ($group as $parent)
                                <div class="checkbox">
                                    <input id="checkbox-{{ $parent->id }}" type="checkbox" name="permissions[]" value="{{ $parent->id }}" class="permission-checkbox p-c-{{ $nameGrup }}">
                                    <label for="checkbox-{{ $parent->id }}">{{ $parent->name }}</label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="position-relative">
                    <div class="col-sm-10 offset-sm-2">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@include('admin.setting.role.js')
@section('script')
<script src="https://kendo.cdn.telerik.com/2020.1.219/js/kendo.all.min.js"></script>
<script>
    $('.permission-checkbox-all').change(function() {
        if (this.checked) {
            $('.permission-checkbox').prop('checked', true);
        } else {
            $('.permission-checkbox').prop('checked', false);
        }
    })

    $('.permission-checkbox-parent').change(function() {
        id = $(this).attr('data-id');
        if (this.checked) {
            $('.p-c-' + id).prop('checked', true);
        } else {
            $('.p-c-' + id).prop('checked', false);
        }
    })
</script>
@endsection