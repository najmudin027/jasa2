@extends('admin.home')
@section('content')
<div class="col-md-12 card">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            {{ $title }}
        </div>
        <div class="btn-actions-pane-right text-capitalize">
            <!-- <a href="{{ url('/admin/educations/create') }}" class="mb-2 mr-2 btn btn-primary btn-sm" style="float:right">Add Education</a> -->
        </div>
    </div>
    <div class="card-body">
        <table style="width: 100%;" id="example" class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <!-- <th>No</th> -->
                    <th>Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Date</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($showUserTeknisi as $user)
                    <tr>
                        <!-- <td>{{ $no++ }}</td> -->
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        @if ($user->status == 7)
                            <td><span class="badge badge-warning badge-pill btn-sm"> Pending</span></td>
                        @elseif($user->status == 8)
                            <td><span class="badge badge-danger badge-pill btn-sm"> Rejected</span></td>
                        @elseif($user->status == 1)
                            <td><span class="badge badge-success badge-pill btn-sm"> Approved</span></td>
                        @endif
                        <td>{{ date('d M Y H:i', strtotime($user->updated_at)) }}</td>
                        <td>
                            <!-- <button class="updatestatus btn btn-success btn-sm btn-white" data-id="{{ $user->id }}"><i class='fa fa-check'></i>&nbsp; Approve</button> -->
                            <a href="{{ url('/admin/teknisi/detail/'.$user->id) }}" class="btn btn-primary btn-sm btn-white"><i class='fa fa-eye'></i></a>
                        </td>
                    </tr>
                @endforeach
                @foreach($showTeknisi as $teknisi)
                    @if (!empty($teknisi->info))
                        <tr>
                            <!-- <td>{{ $no++ }}</td> -->
                            <td>{{ $teknisi->name }}</td>
                            <td>{{ $teknisi->email }}</td>
                            @if ($teknisi->status == 7)
                                <td><span class="badge badge-warning badge-pill btn-sm"> Pending</span></td>
                            @elseif($teknisi->status == 8)
                                <td><span class="badge badge-danger badge-pill btn-sm"> Rejected</span></td>
                            @elseif($teknisi->status == 1)
                                <td><span class="badge badge-success badge-pill btn-sm"> Approved</span></td>
                            @endif
                            <td>{{ date('d M Y H:i', strtotime($teknisi->updated_at)) }}</td>
                            <td>
                                <!-- <button class="updatestatus btn btn-success btn-sm btn-white" data-id="{{ $teknisi->id }}"><i class='fa fa-check'></i>&nbsp; Approve</button> -->
                                <a href="{{ url('/admin/teknisi/detail/'.$teknisi->id) }}" class="btn btn-primary btn-sm btn-white"><i class='fa fa-eye'></i></a>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
@section('script')
   <script>
       var table = $('#example').DataTable({
            ordering:'true',
            order: [3, 'asc'],
       });
   </script>

@endsection