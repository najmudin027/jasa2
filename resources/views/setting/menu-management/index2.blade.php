@extends('admin.home')

@section('content')
    <div class="col-md-12 card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <span id="nestable-menu">
                    <button type="button" class="mb-2 mr-1 btn btn-success pull-right pad-l" data-action="expand-all"><i class="fa fa-expand"></i></button>
                    <button type="button" class="mb-2 mr-1 btn btn-info pull-right pad-r" data-action="collapse-all"><i class="fa fa-compress"></i></button>
                </span>
            </div>
        </div>
        <div class="card-body">
            <div class="col-md-8">
                <div class="well">
                    <p class="lead">
                        <a href="#newModal" class="mb-2 mr-2 btn btn-success pull-right pad-l" data-toggle="modal" data-backdrop="false"><i class="fa fa-plus-square"></i>
                        </a>
                            Menu Management</p>
                    <div class="dd" id="nestable">
                        {!! $menu !!}
                        <input type="hidden" id="nestable-output">
                    </div>
                    <br>
                    <div id="success-indicator" style="display:none; margin-right: 10px;">
                        <div class="alert alert-success fade show" role="alert" >Menu move has been saved !</div>
                    </div>
                </div>
            </div>
            <br>
            <div class="d-block text-center card-footer">
                <div class="col-md-4">
                    <div class="well">
                        <br>
                        <p>Drag Menu to move them in a different order</p>
                    </div>
                </div>
            </div>
            @include('admin.setting.menu-management.createModalMenu')
            @include('admin.setting.menu-management.updateModalMenu')
        </div>
    </div>
    @endsection
    @section('script')
    {{-- <link href="{{  asset('adminAssets/css/simple-iconpicker.min.css') }}" rel="stylesheet" />
    <script type="text/javascript" src="{{ asset('adminAssets/js/simple-iconpicker.min.js') }}"></script> --}}
<script type="text/javascript">



function convertToSlugAdd(str) {
    elem = 'to_slug_add';
    Helper.toSlug(str,elem);
}

function convertToSlugEdit(str) {
    elem = 'to_slug_edit';
    Helper.toSlug(str,elem);
}

function convertToIcon(str) {
    elem = 'icon_add';
    Helper.toSlug(str,elem);
}


$(document).ready(function() {

    $('#to_slug_add').prop('readonly',true);
    $('#to_slug_edit').prop('readonly',true);
    var updateOutput = function(e)
    {
        var list   = e.length ? e: $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 1
    $('#nestable').nestable({
        group: 1
    }).on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));
    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));
    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });

    $('.dd').on('change', function() {

        var dataString = {
            data : $("#nestable-output").val(),
        };
        console.log(dataString);
        $.ajax({
            type: "POST",
            url:Helper.apiUrl('/menu/move'),
            data: dataString,
            cache : false,
            success: function(data){
                $("#success-indicator").fadeIn(100).delay(1000).fadeOut();
                Helper.successNotif('menu has been moved !');
            },
            error: function(xhr, status, error) {
              alert(error);
            },
        });
    });

    $("#add").click(function() {
        $.ajax({
            url:Helper.apiUrl('/menu'),
            type: 'POST',
            data: {
                name : $('input[name="name_add"]').val(),
                url : $('input[name="url_name_add"]').val(),
                module : $('input[name="module"]').val(),
                icon : $('input[name="icon_add"]').val(),
            },
            success: function(response) {
                if (response != 0) {

                    window.location.href = Helper.redirectUrl('/admin/menu/show');
                }
            },
        });
    });

    $("#edit").click(function() {
        id = $('#id_menu').val();
        $.ajax({
            url:Helper.apiUrl('/menu/' + id ),
            type:'PUT',
            data: {
                name : $('input[name="name_edit"]').val(),
                url : $('input[name="url_name_edit"]').val(),
                module : $('input[name="module_edit"]').val(),
                icon : $('input[name="icon_edit"]').val(),
            },
            success: function(response) {
                if (response != 0) {
                    Helper.successNotif('Menu has been udpated !')
                    window.location.href = Helper.redirectUrl('/admin/menu/show');
                }
            },
        });
    });

    $(document).on('click', '.update_toggle', function(e) {
        id = $(this).attr('rel');
        editMenu(id)
        e.preventDefault()
    })

    $(document).on('click', '.delete_toggle', function(e) {
        id = $(this).attr('rel');
        deleteMenu(id)
        e.preventDefault()
    })

    function editMenu(id) {
        console.log('id', id)
        $.ajax({
            url:Helper.apiUrl('/menu/' + id ),
            type: 'get',
            data: id,
            dataType: 'JSON',
            contentType: 'application/json',
            success: function(res) {
                console.log(res.data)
                $('#id_menu').val(id),
                $('#name_edit').val(res.data.name),
                $('#url_name_edit').val(res.data.url),
                $('#module_edit').val(res.data.module),
                $('#icon_edit').val(res.data.icon)
            },
            error: function(res) {
                alert("Something went wrong");
                console.log(res);
            }
        })
    }

    function deleteMenu(id) {
        console.log('id', id)
        if(confirm("Are you sure to delete this Menu ? Your action can not be reversed")) {
            $.ajax({
                url:Helper.apiUrl('/menu/' + id ),
                type: 'delete',
                dataType: 'JSON',
                contentType: 'application/json',
                success: function(res) {
                    Helper.successNotif('Menu has been deleted!');
                    window.location.href = Helper.redirectUrl('/admin/menu/show');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }

});


</script>

@endsection
