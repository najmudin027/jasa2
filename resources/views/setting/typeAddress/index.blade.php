@extends('admin.home')
@section('content')
<div class="col-md-12 ">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Master Type Address
            </div>
        </div>
        <div class="card-body">
            <table id="table-address" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Address Type</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-type">Add Addres Type</button>
        </div>
    </div>
</div>
@include('admin.setting.typeAddress.modalCreate')
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        url: '/address_type/datatables',
        selector: '#table-address',
        columnsField: ['DT_RowIndex', 'name'],
        modalSelector: "#createType",
        modalButtonSelector: ".add-type",
        modalFormSelector: "#form-address-type",
        actionLink: {
            store: function() {
                return "/address_type";
            },
            update: function(row) {
                return "/address_type/" + row.id;
            },
            delete: function(row) {
                return "/address_type/" + row.id;
            },
        }
    })
</script>

@endsection