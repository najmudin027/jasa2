@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                List Address
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                @if(Request::segment(1) == 'customer' && Auth::user()->hasRole('Customer'))
                    <a href="{{ url('/customer/address/create') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add New Address
                    </a>
                @endif

                @if(Request::segment(1) == 'teknisi' && Auth::user()->hasRole('Technician'))
                    <a href="{{ url('/admin/address/create') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add New Address
                    </a>
                @endif

            </div>
        </div>
        <div class="card-body">
            <table id="form-list-address-table" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr align="center">
                        <th>No</th>
                        <th>Phone</th>
                        <th>City</th>
                        <th>Distric</th>
                        <th>Village</th>
                        <th>Zipcode</th>
                        <th>Adress Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    globalCRUD.datatables({
        url: '/user_address/datatables',
        selector: '#form-list-address-table',
        columnsField: ['DT_RowIndex', 'phone1', 'city.name', 'district.name', 'village.name', 'ms_zipcode_id', 'types.name'],
        scrollX: true,
        actionLink: {
            update: function(row) {
                return '/customer/address/update/' + row.id;
            },
            delete: function(row) {
                return '/user_address/' + row.id;
            }
        }
    })
</script>
@endsection
