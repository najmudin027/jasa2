@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Update List Address
            </div>

            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('/customer/address/show') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                    <i class="" aria-hidden="true"></i>&nbsp;&nbsp;Back
                </a>
            </div>
        </div>
        <div class="card-body">
            <form action="" method="post" id="form-address-update">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Phone Number 1</label>
                    <div class="col-sm-10">
                        <input name="phone1" value=" {{ $updateAddress->phone1 }} " id="examplename" placeholder="+628xxx" type="text" class="form-control">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Phone Number 2</label>
                    <div class="col-sm-10">
                        <input name="phone2" value=" {{ $updateAddress->phone2 }} " id="examplename" placeholder="+628xxx" type="text" class="form-control">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Type</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="address_type" name="ms_address_type_id" style="width:100%">
                            @if (empty($updateAddress->types->id))
                                <option value="" selected></option>
                            @else
                                <option value="{{ $updateAddress->types->id }}" selected>{{ $updateAddress->types->name }}</option>
                            @endif

                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Country Name</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="countries" name="ms_countries_id" style="width:100%">
                            <option value="{{ $updateAddress->city->province->country->id }}" selected>{{ $updateAddress->city->province->country->name }}</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Province Name</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="province_id" name="ms_province_id" style="width:100%">
                            <option value="{{ $updateAddress->city->province->id }}" selected>{{ $updateAddress->city->province->name }}</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">City Name</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="city_id" name="ms_city_id" style="width:100%">
                            <option value="{{ $updateAddress->city->id }}" selected>{{ $updateAddress->city->name }}</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Distric Name</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="distric" name="ms_district_id" style="width:100%">
                            <option value="{{ $updateAddress->district->id }}" selected>{{ $updateAddress->district->name }}</option>
                        </select>
                    </div>
                </div>

                <input name="url" value="{{ \Request::segment(1) }}" type="hidden" class="form-control" readonly>
                <input name="user_id" value="{{ Auth::user()->id }}" type="hidden" class="form-control" readonly>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Village Name</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="vilage" name="ms_village_id" style="width:100%">
                            <option value="{{ $updateAddress->village->id }}" selected>{{ $updateAddress->village->name }}</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Zip-Code</label>
                    <div class="col-sm-10">
                        <input name="ms_zipcode_id" value="{{ $updateAddress->ms_zipcode_id }}" placeholder="06XXX" id="zipcode-text" type="text" class="form-control" readonly>
                    </div>
                </div>

                <input name="ms_zipcode_id" value="{{ $updateAddress->ms_zipcode_id }}" id="zipcode-id" placeholder="06XXX" type="hidden" class="form-control" readonly>
                <input name="id" value="{{ $updateAddress->id }}" type="hidden" class="form-control" readonly>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                        <textarea name="address" class="form-control" id="exampleFormControlTextarea1" rows="3">{{ $updateAddress->address }}</textarea>
                    </div>
                </div>

                <div class="position-relative row form-group">

                    <label for="exampleEmail" class="col-sm-2 col-form-label">Maps</label>
                    <div class="col-sm-10">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <input type="hidden" name="id"/>
                                    <input type="text" name="searchMapEdit" style="margin-top: 7px; width: 70%; border:2px solid #4DB6AC;" id="searchMapEdit" class="form-control" value="{{ $updateAddress->location_google }}" required>
                                </div>
                                <div class="form-group">
                                    <div id="mapEdit" style="width: 700px; height: 500px;"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-5 col-xs-5 mr-4">
                                        <label class="control-label">Latitude</label>
                                        <input size="20" type="text" id="latboxEdit" class="form-control"  name="latEdit" onchange="inputLatlng('edit')" value="{{ $updateAddress->latitude }}" required>
                                    </div>
                                    <div class="form-group col-md-5 col-xs-5 mr-4">
                                        <label class="control-label">Longitude</label>
                                        <input size="20" type="text" id="lngboxEdit"  class="form-control"  name="longEdit" onchange="inputLatlng('edit')" value="{{ $updateAddress->longitude }}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="form-group col-md-5 col-xs-5 mr-4">
                                        <label class="control-label">City / Regency</label>
                                        <input size="20" type="text" id="cityEdit" class="form-control"  name="cityEdit" value="{{ $updateAddress->cities }}" readonly>
                                    </div>
                                    <div class="form-group col-md-5 col-xs-5 mr-4">
                                        <label class="control-label">Province</label>
                                        <input size="20" type="text" id="provinceEdit" class="form-control"  name="prvEdit" value="{{ $updateAddress->province }}" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="position-relative row form-check" style="margin-bottom:50px;">
                    <div class="col-sm-10 offset-sm-2">
                        <button class="btn btn-success" autocomplete="off" id="update" ><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
@include('admin.script.profile._addressScript')

@include('admin.template._mapsScript')
<script>
    // $('#form-address-update').submit(function(e) {
    //     globalCRUD.handleSubmit($(this))
    //         .updateTo(function(formData) {
    //             return '/user_address/' + formData.id;
    //         })
    //         .redirectTo(function(resp) {
    //             return '/admin/address/show';
    //         })
    // })
    initMapEdit(parseFloat($('#latboxEdit').val()), parseFloat($('#lngboxEdit').val()));
    $("#form-address-update").submit(function(e) {

                    // return resp.data.data.redirect;
        var redirect = $('input[name  = "url"]').val();
        var id = $('input[name  = "id"]').val();
        var fd = new FormData();

            fd.append('id', $('input[name="id"]').val());
            fd.append('phone1', $('input[name="phone1"]').val());
            fd.append('phone2', $('input[name="phone2"]').val());
            fd.append('user_id', $('input[name="user_id"]').val());
            fd.append('ms_address_type_id', $('select[name="ms_address_type_id"]').val());
            fd.append('ms_countries_id', $('select[name="ms_countries_id"]').val());
            fd.append('ms_province_id', $('select[name="ms_province_id"]').val());
            fd.append('ms_city_id', $('select[name="ms_city_id"]').val());
            fd.append('ms_district_id', $('select[name="ms_district_id"]').val());
            fd.append('ms_village_id', $('select[name="ms_village_id"]').val());
            fd.append('ms_zipcode_id', $('input[name="ms_zipcode_id"]').val());
            fd.append('ms_curriculum_id', $('select[name="ms_curriculum_id"]').val());
            fd.append('no_identity', $('input[name="no_identity"]').val());
            fd.append('address', $('textarea[name="address"]').val());
            fd.append('lat', $("#latboxEdit").val());
            fd.append('long', $("#lngboxEdit").val());
            fd.append('search_map', $("#searchMapEdit").val());
            fd.append('province', $("#provinceEdit").val());
            fd.append('city', $("#cityEdit").val());
            console.log(fd)
            $.ajax({
                url:Helper.apiUrl('/user_address/' + id),
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {

                    if (response != 0) {
                        iziToast.success({
                            title: 'OK',
                            position: 'topRight',
                            message: 'Data Has Been Saved',
                        });
                        if(redirect == 'teknisi'){
                            window.location = Helper.url('/teknisi/address/show');
                        }
                        if(redirect == 'customer'){
                            window.location = Helper.url('/customer/address/show');
                        }
                        // window.location.href = Helper.redirectUrl('/admin/address/show');
                    } else {
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message: 'Something Went Wrong '
                        });
                    }
                },
            });
            e.preventDefault();
    });
</script>
@endsection
