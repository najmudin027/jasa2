@extends('admin.home')
@section('content')
@include('admin.setting.profile.profileCss')
<div class="col-md-12 card">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            {{ $title }}
        </div>
        <div class="btn-actions-pane-right text-capitalize">
            <!-- <a href="{{ url('/show-profile') }}" class="mb-2 mr-2 btn btn-primary btn-sm" style="float:right">Back</a> -->
        </div>
    </div>
    <div class="card-body">
        <div class="card-title">User Profile</div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6"><br>
                    <div  align="center">
                        @if (Auth::user()->info->picture == null)
                            <div class="circle-2">
                                <img src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" id="profile-image1" />
                            </div>
                            <div class="p-image">
                                <input id="profile-image-upload" class="form-group" name="picture" type="file">
                            </div>
                            <!-- <img alt="User Pic" src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" id="profile-image1" class="circle">  -->
                            <br><br>
                        @else
                            <div class="circle-2">
                            
                                <img src="{{ asset('/customer/storage/image/profile/' . Auth::user()->info->picture) }}" id="profile-image1" />
                            </div>
                            <!-- <img alt="User Pic" src="{{ asset('/admin/storage/image/profile/' . Auth::user()->info->picture) }}" id="profile-image1" class="circle"> -->
                            <div class="p-image">
                                <input id="profile-image-upload" class="form-group" name="picture" type="file">
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <h3 style="color:#00b1b1;" align="center"><strong>{{ Auth::user()->info->first_name }} &nbsp; {{ Auth::user()->info->last_name }}</strong></h3><hr>
                    <table>
                        <tr>
                            <td>
                                <h5 style="font-family:'Times New Roman', Times, serif">First Name</h5>
                            </td>
                            <td style="color:white">&nbsp;&nbsp;____</td>
                            <td>
                                <div class="col-sm-12">
                                    <input name="first_name" value="{{ Auth::user()->info->first_name }}" id="examplename" placeholder="First Name" type="text" class="form-control">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h5 style="font-family:'Times New Roman', Times, serif">Last Name</h5>
                            </td>
                            <td style="color:white">&nbsp;&nbsp;____</td>
                            <td>
                                <div class="col-sm-12">
                                    <input name="last_name" value="{{ Auth::user()->info->last_name }}" id="examplename" placeholder="Last Name" type="text" class="form-control">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h5 style="font-family:'Times New Roman', Times, serif">Date Of Birth</h5>
                            </td>
                            <td style="color:white">&nbsp;&nbsp;____</td>
                            <td>
                                <!-- <div class="col-sm-12">
                                    <div class="custom-file>
                                        <input class="form-control" name="date_of_birth" type="text" value="{{ Auth::user()->info->date_of_birth }}" readonly />
                                        <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                                    </div>
                                    
                                </div> -->
                                <div class="col-sm-12 input-group date" id="datepicker" data-date-format="yyyy-mm-dd">
                                    <div class="input-group">
                                        <input class="form-control" name="date_of_birth" type="text" value="{{ Auth::user()->info->date_of_birth }}" readonly />
                                        <span class="input-group-text input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h5 style="font-family:'Times New Roman', Times, serif">Place Of Birth</h5>
                            </td>
                            <td style="color:white">&nbsp;&nbsp;____</td>
                            <td>
                                <div class="col-sm-12">
                                    <input name="place_of_birth" value="{{ Auth::user()->info->place_of_birth }}" id="examplename" placeholder="Place Of Birth" type="text" class="form-control">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><h5 style="font-family:'Times New Roman', Times, serif">Marital</h5></td>
                            <td style="color:white">&nbsp;&nbsp;____</td>
                            <td>
                                <div class="col-md-12">
                                    <select class="js-example-basic-single" id="marital" name="ms_marital_id" style="width:100%">
                                        <option value="{{ Auth::user()->info->marital->id }}" selected>{{ Auth::user()->info->marital->status }}</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><h5 style="font-family:'Times New Roman', Times, serif">Religion</h5></td>
                            <td style="color:white">&nbsp;&nbsp;____</td>
                            <td>
                                <div class="col-md-12">
                                    <select class="js-example-basic-single" id="religion" name="ms_religion_id" style="width:100%">
                                        <option value="{{ Auth::user()->info->religion->id }}" selected>{{ Auth::user()->info->religion->name }}</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h5 style="font-family:'Times New Roman', Times, serif">Gender</h5>
                            </td>
                            <td style="color:white">&nbsp;&nbsp;____</td>
                            <td>
                                <div class="col-md-12">
                                    <select class="js-example-basic-single" id="gender" name="gender" style="width:100%">
                                        <option value="0">Male</option>
                                        <option value="1">Woman</option>
                                        <option value="2">another one</option>
                                    </select>
                                    <!-- <strong><span id="error-gender" style="color:red"></span></strong> -->

                                </div>
                                <input name="url" value="{{ \Request::segment(1) }}" type="hidden" class="form-control" readonly>

                            </td>
                        </tr>
                    </table>
                    <hr>
                    <button class="btn_login" type="submit" id="save">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="d-block text-center card-footer"></div>

@endsection

@section('script')
@include('admin.script.profile._profileScript');


<script>
    $("#marital").select2({
        ajax: {
            type: "GET",
            url: Helper.apiUrl('/marital/select2'),
            data: function(params) {
                return {
                    q: params.term
                };
            },
            processResults: function(data) {
                var res = $.map(data.data, function(item) {
                    return {
                        text: item.status,
                        id: item.id
                    }
                });

                return {
                    results: res
                };
            }
        }
    });

    globalCRUD
        .select2("#religion", '/religion/select2')


        $(document).ready(function() {
            $('#gender').select2();
        });
</script>

@endsection