@extends('admin.home')
@section('content')

<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                HITORY TRANSACTIONS
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="/teknisi/dashboard" class="mb-2 mr-2 btn btn-primary btn-sm" style="float:right">Back</a>
            </div>
        </div>
        

        <div class="card-body">
            <div class="tab-pane show active" id="tab-1" role="tabpanel">
                <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Orders Code</th>
                            <th>Transfer Date</th>
                            <th>Status</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    // var ajaxTable = init.tableall;

    // filter klik button
    // $("#myButton").on('click', function(e) {
    //     var type = [];
    //     $.each($("#type option:selected"), function(){            
    //         type.push($(this).val());
    //     });
    //     if(type != ''){
    //         init(type);
    //     }else{
    //         init($(this).val());
    //     }
    // });
    // $('input[name="schedule"]').daterangepicker();
    // $('#myButton').trigger('click');


    // data table

    // function init(type_id){
        var tableAll = $('#table-list-orders').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            scrollX: true,
            select: true,
            dom: 'Bflrtip',
            ordering:'true',
            order: [1, 'desc'],
            responsive: true,
            language: {
                search: '',
                searchPlaceholder: "Search..."
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/teknisi/balance/detail/datatables'),
                type: 'get',
                // data : {
                //     type_id : type_id
                // }
            },
            columns: [
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        if(full.order == null){
                            return "-"
                        }else{
                            return "<a href=''># "+full.order.code+"</a>";
                        }
                    }
                },
                {
                    data: "updated_at",
                    name: "updated_at",
                    render: function(data, type, full){
                        return moment(full.updated_at).format("DD MMMM YYYY HH:mm");
                    }
                },
                {
                    data: "transfer_status",
                    name: "transfer_status",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        if (full.transfer_status == 1) {
                            return '<span class="badge badge-success">Transfer Success</span>';
                        } else if (full.transfer_status == 0) {
                            return '<span class="badge badge-warning">Pending</span>';
                        }
                    }
                },
                {
                    data: "total",
                    name: "total",
                    render: $.fn.dataTable.render.number( ',', '.,', 0, 'Rp. ', '.00' )
                },
            ]
        });
    // }
</script>
@endsection
