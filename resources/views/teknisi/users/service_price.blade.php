@extends('admin.home')
@section('content')
<style>
    /* .modal{
        display: block !important; /* I added this to see the modal, you don't need this */
    } */

    /* Important part */
    .modal-dialog{
        overflow-y: initial !important
    }
    .modal-body{
        height: 350px;
        overflow-y: auto;
    }
</style>

<div class="col-md-12" style="margin-top: 20px;">
    <div class="card">
        <div class="card-header text-white header-bg">
            TECHNICIAN PRICE SERVICE
        </div>
        <div class="card-body">
            @include('admin.teknisi.technician._price_service_table_html')
        </div>
        <div class="card-footer">
            <button class="btn btn-primary add-price_service" type="button">
                <i class="fa fa-plus"></i> ADD PRICE SERVICE
            </button>
        </div>
    </div>
</div>
@include('admin.teknisi.technician._price_service_modal_html')
@endsection

@section('script')
@include('admin.teknisi.technician._price_service_table_script')
@include('admin.teknisi.technician._price_service_commission_script')
{{-- jquery autocomplate --}}
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js">
</script>
<script>
    //script modal


    globalCRUD.select2(".category-select", '/teknisi/category/select2');
    $('.symptom-select-control').hide();
    $('.service_type-select-control').hide();

    // agar user profile bisa diclik
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    // table table_price_service
    var table_price_service = PriceService.table();

    // add price
    $(document)
        .on('click', '.add-price_service', function(){
            $('.product_group-select-control').hide();
            $('.symptom-select-control').hide();
            $('.service_type-select-control').hide();
            $('.category-select').val('').change();
            $('.symptom-select').val('').change();
            $('.service_type-select').val('').change();
            $('.price-id').val('');
            $('.price-value').val('');

            $('#commission_display').hide();
            $('#after_commission_display').hide();
            $('#modal-price_service').modal('show');
        })

    // chenge
    $(document)
        .on('change', '.category-select', function(){
            category_id = $('.category-select').val();
            if (category_id) {
                $('.symptom-select').val('').change();
                $('.service_type-select').val('').change();

                $('.symptom-select-control').show();
                $('.service_type-select-control').hide();

                globalCRUD.select2(".symptom-select", '/teknisi/symptom/select2/' + category_id);
            }
        })

    // chenge
    $(document)
        .on('change', '.symptom-select', function(){
            val = $(this).val();
            if (val) {
                $('.service_type-select-control').show();
                $('.service_type-select').val('').change();
                globalCRUD.select2(".service_type-select", '/symptom/' + val + '/service_type');
            }
        })

    // change product_group
    $(document).on('change', '.service_type-select', function() {
        asu = $(this).val();
        if (asu) {
            $('.product_group-select-control').show();
            $('#select-product_group').val('').change();
            globalCRUD.select2("#select-product_group", '/product_groups/select2/' + asu);
        }
    });

    // Edit price
    $(document)
        .on('click', '.edit-price', function(){
            $('#commission_display').show();
            $('#after_commission_display').show();

            var row = table_price_service.row($(this).parents('tr')).data();

            var categorySelect = new Option(row.service_type.symptom.services.name, row.service_type.symptom.services.id, false, false);
            $('.category-select').empty().append(categorySelect).trigger('change');
            var symptomSelect = new Option(row.service_type.symptom.name, row.service_type.symptom.id, false, false);
            $('.symptom-select').empty().append(symptomSelect).trigger('change');
            var serviceSelect = new Option(row.service_type.name, row.service_type.id, false, false);
            $('.service_type-select').empty().append(serviceSelect).trigger('change');
            var prosel = new Option(row.product_group.name, row.product_group.id, false, false);
            $('#select-product_group').empty().append(prosel).trigger('change');
            
            $('.price-id').val(row.id);
            $('.price-value').val(row.value);
            $('#commission').val(row.commission);
            commission(row.value);
            $('#modal-price_service').modal('show');
        })

    // delete price
    $(document)
        .on('click', '.delete-price', function(){
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.delete('/technician_profile/'+id+'/price_service/')
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('ok');
                        // reload table
                        table_price_service.ajax.reload();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })


    // save price_service
    $('#form-price_service')
        .submit(function(e){
            data = Helper.serializeForm($(this));

            Helper.loadingStart();
            if (data.id == '') {
                Axios.post('/technician_profile/price_service_store', data)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif(response.data.msg);
                        // reload table
                        table_price_service.ajax.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                        if (error.response.data.msg == 'Please complete technician bank account first') {
                            alert('Please complete technician bank account first')
                            window.location.href = Helper.redirectUrl('/teknisi/profile?bank');
                        }
                        if (error.response.status == 500) {
                            // window.location.href = Helper.redirectUrl('/teknisi/profile/');
                        }
                    });
            }

            if (data.id != '') {
                Axios.put('/technician_profile/price_service_update/' + data.id, data)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif(response.data.msg);
                        // reload table
                        table_price_service.ajax.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            }


            $('#modal-price_service').modal('hide');
            e.preventDefault();
        })
</script>
@endsection
