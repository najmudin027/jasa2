@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                
            </div>
        </div>
        <div class="card-body">
            <table id="table-job_title" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th style="width: 20px;">No</th>
                        <th>Description</th>
                        <th>User</th>
                        <th>Job Title Category</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-job_title"><i class="fa fa-plus"></i> Add Job Title Category</button>
        </div>
    </div>
</div>

<form id="form-job_title">
    <div class="modal fade" id="modal-job_title" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Job Title Category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Job Catgeory</label>
                    <div class="form-group">
                        <select class="form-control job-category-select" id="job_title_category_id" name="job_title_category_id" style="width:100%" required>
                            <option value="" selected>Pilih Job Category</option>
                        </select>
                    </div>
                    <label>Description</label>
                    <div class="form-group">
                        <input name="description" placeholder="description" id="description" type="text" class="form-control" required>
                    </div>
                    <label>User</label>
                    <div class="form-group">
                        <select class="form-control user-select" id="user_id" name="user_id" style="width:100%" required>
                            <option value="" selected>Pilih User</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">Save</button>
                    <button type="button" class="btn waves-effect btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    globalCRUD
        .select2Static(".job-category-select", '/job_title_category/select2')
        .select2Static(".user-select", '/user/select2');

    globalCRUD.datatables({
        url: '/job_title/datatables',
        selector: '#table-job_title',
        columnsField: ['DT_RowIndex', 'description', 'user.name', 'job_title_category.name'],
        modalSelector: "#modal-job_title",
        modalButtonSelector: ".add-job_title",
        modalFormSelector: "#form-job_title",
        actionLink: {
            store: function() {
                return "/job_title";
            },
            update: function(row) {
                return "/job_title/" + row.id;
            },
            delete: function(row) {
                return "/job_title/" + row.id;
            },
        },
    })
</script>
@endsection
