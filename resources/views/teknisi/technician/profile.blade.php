@extends('admin.home')
@section('content')
@include('admin.setting.profile.profileCss')
<div class="col-md-4">
    <div class="card">
        <div class="card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                <h4>My Profile</h4>
            </div>
        </div>
        <div class="card-body">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic text-center">                   
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                            <label for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview">                            
                            @if($user->info == null)
                                <div id="imagePreview" style="background-image: url(https://image.freepik.com/free-vector/man-avatar-profile-round-icon_24640-14046.jpg);"></div>
                            @else
                                @if ($user->info->picture == null)
                                <div id="imagePreview" style="background-image: url(https://image.freepik.com/free-vector/man-avatar-profile-round-icon_24640-14046.jpg);"></div>
                                @else 
                                <div id="imagePreview" style="background-image: url({{ asset('/storage/user/profile/avatar/' . $user->info->picture) }});"></div>
                                @endif                        
                            @endif
                        </div>
                    </div>
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{ $user->name }}
                    </div>
                    <div class="profile-usertitle-job">
                        {{ $user->email }}
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <div class="profile-userbuttons">
                    <a href="{{ url('/customer/show/profiles') }}" class="btn btn-success btn-sm" >Edit Info</a>
                    <!-- <button class="btn btn-success btn-sm" type="button">
                        EDIT INFO
                    </button> -->
                </div>
                <!-- END SIDEBAR BUTTONS -->
            </div>
        </div>
    </div>
</div>
<div class="col-md-8">
    <div class="card">
        <div class="card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                <h4>Mechanic Info</h4>
            </div>
        </div>
        <div class="card-body">
            <!-- <table class="table table-user-information">
                <tbody>
                    <tr>
                        <th>
                            Phone
                        </th>
                        <td>
                            {{  $user->phone }}
                        </td>                        
                    </tr>
                   <tr>
                        <td>-</td>
                        <td>
                            @if ( !is_null(Auth::user()->otp_code) )
                                @if ( !is_null(Auth::user()->otp_verified_at) )
                                    @if ( !is_null( Auth::user()->otp_url ) )
                                        nah disini kondisi final karena semuanya udah ada otp code , otp verify , otp url
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        {{ $user->phone }}   
                                                    </div>
                                                    <div class="col-md-6">
                                                    <button type="button" class="btn btn-danger  btn-sm btn-white" data-toggle="modal" data-target="#exampleModal">
                                                        <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Update Phone Number
                                                    </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>      
                                    @endif
                                @else
                                    verify
                                    <div class="row">
                                        <form action="{{ route('otp.verification') }}" method="post">
                                        @csrf
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control" id="credit-card" name="otp" placeholder="X - X - X - X " data-inputmask="'mask': '9  9  9  9'">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button type="submit"  class="btn btn-success btn-sm btn-white" >Varification</button>
                                                    </div>&nbsp;&nbsp;&nbsp;
                                        </form>
                                        
                                        <form action="{{ route('otp.resend.verification') }}" method="post">
                                            @csrf
                                                    <div class="col-md-2">
                                                        <button type="submit"  class="btn btn-danger btn-sm btn-white" >Resend</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                @endif        
                            @else
                                alert notif harus confirmasi otp code
                                <form action="" method="post">
                                    <p align="center">Your number has not been verified in our system, click the button next to verify</p>
                                    <div class="text-center">
                                        <button class="sendOTP btn btn-success btn-sm btn-white"><i class='fa fa-check'></i>&nbsp; Send Code OTP</button>
                                    </div>
                                </form>
                            @endif
                        </td>
                        
                    </tr> 
                </tbody>
            </table> -->
            <table class="table table-user-information">
                <tbody>
                    <tr>
                        <th>
                            Phone Number
                        </th>
                        <td>
                        {{  $user->phone }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            First Name
                        </th>
                        <td>
                            {{ $user->info == null ? '' : $user->info->first_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Last Name
                        </th>
                        <td>
                            {{ $user->info == null ? '' : $user->info->last_name }}
                        </td>
                    </tr>                    
                    <tr>
                        <th>
                            Date Of Birth
                        </th>
                        <td>
                             {{ $user->info == null ? '' : $user->info->place_of_birth }}
                             @if($user->info != null)
                                @if($user->info->date_of_birth != null)
                                    {{ $user->info->date_of_birth->format('d F Y') }}
                                @endif
                             @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Gender
                        </th>
                        <td>
                            {{ $user->info == null ? '' : $user->info->sex }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Religion
                        </th>
                        <td>
                            @if ($user->info != null)
                                @if ($user->info->religion != null)
                                    {{ $user->info->religion->name }}
                                @endif
                            @endif                            
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Marital
                        </th>
                        <td>
                            @if ($user->info != null)
                                @if ($user->info->marital != null)
                                    {{ $user->info->marital->status }}
                                @endif
                            @endif                            
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- ====================modal Show========================================= -->
<div class="modal fade" id="exampleModal" tabindex="-1"  data-backdrop="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Phone Number</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Phone Number</label>
                    <div class="col-sm-10">
                        <input type="number" name="phone" placeholder="Your Number" class="form-control" id=""></input>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm btn-white" data-dismiss="modal">Close</button>
                <button class="update btn btn-danger btn-sm btn-white" data-id="{{ Auth::user()->id }}">
                     Update
                </button>
            </div>
        </div>
    </div>
</div>
<!-- ====================modal End========================================= -->


<div class="col-md-6" style="margin-top: 20px;">
    <div class="card">
        <div class="card-header header-border">
            TECHNICIAN JOB INFORMATION
        </div>
        <div class="card-body">
            <form id="teknisi-job-form">
                <label>Job Title Category</label>
                <div class="form-group">
                    @if($user->technician == null)
                        <input 
                            class="form-control autocomplete-job-category" 
                            id="job_title_category_name" 
                            name="job_title_category_name" 
                            placeholder="category" 
                            type="text" 
                            value="" />
                    @elseif($user->technician->job_title != null)
                        <input 
                            class="form-control autocomplete-job-category" 
                            id="job_title_category_name" 
                            name="job_title_category_name" 
                            placeholder="category" 
                            type="text" 
                            value="{{ $user->technician->job_title->job_title_category->name }}" />
                    @endif
                </div>

                <label>Job Title Description</label>
                <div class="form-group">
                    @if($user->technician == null)
                        <input 
                            class="form-control" 
                            id="job_title_description" 
                            name="job_title_description" 
                            placeholder="description" 
                            type="text" 
                            value="" />
                    @elseif($user->technician->job_title != null)
                        <input 
                            class="form-control" 
                            id="job_title_description" 
                            name="job_title_description" 
                            placeholder="description" 
                            type="text" 
                            value="{{$user->technician->job_title->description }}" />
                    @endif
                </div>

                <label>Skill</label>
                <div class="form-group">
                    @if($user->technician == null)
                        <input 
                            class="form-control" 
                            id="skil" 
                            name="skil" 
                            placeholder="skill" 
                            type="text" 
                            value="" />
                    @elseif($user->technician->skill != null)
                        <input 
                            class="form-control" 
                            id="skil" 
                            name="skil" 
                            placeholder="skill" 
                            type="text" 
                            value="{{ $user->technician->skill }}" />
                    @endif
                </div>
                

                <button class="btn btn-white bg-drak btn-sm waves-effect" type="submit"><i class="fa fa-pencil"></i> Save</button>
            </form>
        </div>
    </div>
</div>
<div class="col-md-6" style="margin-top: 20px;" id="bank_account_area">
    <div class="card">
        <div class="card-header header-border header-bank">
            TECHNICIAN REKENING
        </div>
        <div class="card-body">
            <form id="teknisi-rek-form">
            @if($user->technician == null)
            <label>Bank Name</label>
                <div class="form-group">
                    <input 
                        class="form-control" 
                        id="rekening_bank_name" 
                        name="rekening_bank_name" 
                        placeholder="bank name" 
                        type="text" 
                        value="" />
                </div>

                <label>Rekening Number</label>
                <div class="form-group">
                    <input 
                        class="form-control" 
                        id="rekening_number" 
                        name="rekening_number" 
                        placeholder="Rekening number" 
                        type="text" 
                        value="" />
                </div>

                <label>Bank Account Under the Name</label>
                <div class="form-group">
                    <input 
                    class="form-control" 
                    id="rekening_name" 
                    name="rekening_name" 
                    placeholder="rekening name" 
                    type="text" 
                    value="" />
                </div>
            @elseif($user->technician->skill != null)
                <label>Bank Name</label>
                <div class="form-group">
                    <input 
                        class="form-control" 
                        id="rekening_bank_name" 
                        name="rekening_bank_name" 
                        placeholder="bank name" 
                        type="text" 
                        value="{{ $user->technician->rekening_bank_name }}" />
                </div>

                <label>Rekening Number</label>
                <div class="form-group">
                    <input 
                        class="form-control" 
                        id="rekening_number" 
                        name="rekening_number" 
                        placeholder="Rekening number" 
                        type="text" 
                        value="{{ $user->technician->rekening_number }}" />
                </div>

                <label>Bank Account Under the Name</label>
                <div class="form-group">
                    <input 
                    class="form-control" 
                    id="rekening_name" 
                    name="rekening_name" 
                    placeholder="rekening name" 
                    type="text" 
                    value="{{ $user->technician->rekening_name }}" />
                </div>
            @endif
                

                <button class="btn btn-white bg-drak btn-sm waves-effect" type="submit"><i class="fa fa-pencil"></i> Save</button>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 20px;">
    <div class="card">
        <div class="card-header header-border">
            TECHNICIAN JOB EXPERIENCE
        </div>
        <div class="card-body">
            @include('admin.teknisi.technician._job_experience_table_html')
        </div>
        <div class="card-footer">
            <button class="btn btn-primary add-job_experience" type="button">
                ADD JOB EXPERIENCE
            </button>
        </div>
    </div>
</div>
@include('admin.teknisi.technician._job_experience_modal_html')
@endsection

@section('script')
@include('admin.teknisi.technician._job_experience_table_script')
{{-- jquery autocomplate --}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
<script>
    
    //untuk send OTP
    $(document)
        .on('click', '.btn-main-address', function(){
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.put('/customer/profile/address/main/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif(response.data.msg);
                        // reload table
                        dt.table.reloadTable();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })

    function readURL(input) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
    
    $("#imageUpload").change(function() {
        input = this;

        if (input.files && input.files[0]) {
            Helper.loadingStart();
            user_id = $("#hidden-user-id").val();
            var formData = new FormData();
            formData.append("image", input.files[0]);

            console.log(formData);
            Axios
                .post(Helper.apiUrl('/customer/profile/image'), formData, {
                    headers: {
                      'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    Helper.successNotif(response.data.msg);
                    readURL(input);
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });   
        }
    });

    // agar seperti format otp
    $(":input").inputmask();

    // update OTP
    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/user/update/number/'),
                type: 'post',
                data : {
                    phone : $('input[name="phone"]').val(),
                },

                success: function(res) {
                    Helper.successNotif('data berhasil di Update');
                    Helper.loadingStop();
                    Helper.redirectTo('/admin/technician/profile');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })



    globalCRUD.select2(".service_type-select", '/service_type/select2');

  	// autocomplate
  	$( ".autocomplete-job-category" ).autocomplete({
      	source: function( request, response ) {
	        $.ajax({
	          url: Helper.apiUrl('/job_title_category/select2'),
	          // dataType: "jsonp",
	          data: {
	            q: request.term
	          },
	          success: function( resp ) {
	            response( 
	              _.map(resp.data, function(row){
	                return row.name;
	              })              
	            );
	          }
	        });
      	},
      	minLength: 3,
      	select: function( event, ui ) {
        	console.log('event', event)
        	console.log('ui', ui)
      	},
    });

    $('#teknisi-rek-form').submit(function(e){
        data = Helper.serializeForm($(this));
        Helper.loadingStart();
        Axios.post('/technician_profile/rek_info/', data)
            .then(function(response) {
                // send notif
                Helper.successNotif(response.data.msg).redirectTo();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
        e.preventDefault()
    })

  	// teknisi-job-form
 	$('#teknisi-job-form').submit(function(e){
	    data = Helper.serializeForm($(this));
	    Helper.loadingStart();
	    Axios.post('/technician_profile/job_info', data)
	        .then(function(response) {
	            // send notif
	            Helper.successNotif(response.data.msg).redirectTo();
	        })
	        .catch(function(error) {
	            Helper.handleErrorResponse(error)
	        });
	    e.preventDefault();
  	})

  	// table job_experience
    var table_job_experience = JobExperience.table({
        selector: '#table-job_experience',
        url: '/technician_profile/job_experience'
    });

  	// add job experience
    $(document)
        .on('click', '.add-job_experience', function(){
            $('#modal-job_experience').modal('show');
        })

    // save job experience
    $('#form-job_experience')
        .submit(function(e){
            data = Helper.serializeForm($(this));

            Helper.loadingStart();
            Axios.post('/technician_profile/job_experience', data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg);
                    // reload table
                    table_job_experience.ajax.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });

            $('#modal-job_experience').modal('hide');
            e.preventDefault();
        })

    // delete job experience
    $(document)
        .on('click', '.delete-experience', function(){
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.delete('/technician_profile/'+id+'/job_experience/')
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('ok');
                        // reload table
                        table_job_experience.ajax.reload();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })


</script>

@if(isset($_GET['bank']))
<script>
    $("html, body").animate({ scrollTop: $('#bank_account_area').offset().top }, 1000);
    animateCSS('#bank_account_area', 'shakeY');
</script>
@endif

<style>
/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #555;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}

.avatar-upload {
  position: relative;
  max-width: 205px;
  margin: 0px auto;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  content: "\f040";
  font-family: 'FontAwesome';
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}

</style>
@endsection