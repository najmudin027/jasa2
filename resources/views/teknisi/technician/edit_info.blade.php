@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form action="#" method="post" id="form-teknisi-info">
                <input type="hidden" value="{{ $user_id }}" name="user_id" id="user_id_val">
                <input type="hidden" value="{{ ($teknisiInfo == null) ? 0 : $teknisiInfo->id }}" name="id">
                <label>No Identity</label>
                <div class="form-group">
                    <input name="no_identity" placeholder="no_identity" type="text" class="form-control no_identity" value="{{ ($teknisiInfo == null) ? '' : $teknisiInfo->no_identity }}" />
                </div>

                <label>Attachment</label>
                <div class="form-group">
                    <input name="attachment" placeholder="attachment" type="file" class="form-control attachment-ktp" />
                    @if ($teknisiInfo != null)
                        @if ($teknisiInfo->attachment != null)
                            <div class="form-group">
                                <center>
                                    <img id="imageIcon" src="{{asset('admin/storage/attachment/' . $teknisiInfo->attachment)}}" alt="your image" style="width:800px;height:350px" />
                                </center>
                            </div>
                        @endif
                    @endif
                </div>

                <div class="position-relative">
                    <button class="btn btn-success btn-sm" id="submit"><i class="fa fa-plus"></i> Save</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    // submit event
    $('#form-teknisi-info').submit(function(e) {
        Helper.loadingStart();

        ktp = $('.attachment-ktp')[0];
        ktp_file = (ktp.files && ktp.files[0]) ? ktp.files[0] : null;

        user_id = $('#user_id_val').val();
        var formData = new FormData();
        formData.append("attachment", ktp_file);
        formData.append("user_id", user_id);
        formData.append("no_identity", $('.no_identity').val());
        Axios
            .post(Helper.apiUrl('/technician/' + $('#user_id_val').val() + '/store_info'), formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                Helper.successNotif('Success, ok');
                Helper.redirectTo('/admin/user/detail/' + $('#user_id_val').val())
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    });
</script>
@endsection