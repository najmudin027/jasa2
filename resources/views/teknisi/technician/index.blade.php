@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table id="table-teknisi" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th style="width: 20px;">No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Total Service</th>
                        <th>Rating</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        url: '/technician/datatables',
        selector: '#table-teknisi',
        columnsField: [
            'DT_RowIndex',
            'user.full_name',
            'user.email',
            'user.phone',
            {
                data: 'id',
                name: 'id',
                orderable: false,
                searchable: false,
                className: "text-center",
                render: function(data, type, row) {
                    return row.price_services.length;
                }
            },
            {
                data: 'id',
                name: 'id',
                orderable: false,
                searchable: false,
                visible: true,
                render: function(data, type, row) {
                    return Helper.ratingStar(row.avg_rating);
                }
            }
        ],
        actionLink: {
            store: function() {
                return "/job_title";
            },
            update: function(row) {
                return "/admin/technician/edit/" + row.id;
            },
            delete: function(row) {
                return "/technician/" + row.id;
            },
            render: function(data, type, full, btn) {
                return btn.edit_btn;
            }
        },
    })
</script>
@endsection
