<form id="form-price_service">
    <div class="modal fade" data-backdrop="false" id="modal-price_service" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Add PRICE SERVICE
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body scroll-area-sm" style="height: 400px;">
                    <input name="id" type="hidden" class="price-id" />
                    <label>
                        Service
                    </label>
                    <div class="form-group">
                        <select class="form-control category-select" required style="width:100%">
                            <option selected="" value="">
                                Select Service
                            </option>
                        </select>
                    </div>
                    <div class="symptom-select-control">
                        <label>
                            Service Category
                        </label>
                        <div class="form-group">
                            <select class="form-control symptom-select" required style="width:100%">
                                <option selected="" value="">
                                    Select Service Category
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="service_type-select-control">
                        <label>
                            Service Tyoe
                        </label>
                        <div class="form-group">
                            <select class="form-control service_type-select" name="ms_services_types_id" required="" style="width:100%">
                                <option selected value="">
                                    Pilih Service Type
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="product_group-select-control">
                        <label>
                            Product Group
                        </label>
                        <div class="form-group">
                            <select id="select-product_group" name="product_group_id" class="form-control" style="width:100%;" required>
                                <option value="" selected="selected">--Select--</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <label>Price</label>
                        <div class="form-group">
                            <input class="form-control price-value" id="value" name="value" placeholder="value" type="text" required />
                        </div>
                        
                    </div>
                    <div id="commission_display" style="display:none">
                        <label>Commission</label>
                        <div class="form-group input-group col-md-6" style="margin-left:-15px">
                            <input name="commission" id="commission" value={{ isset($settings[0]) ? $settings[0]->value : 2 }} type="text" class="form-control" readonly>
                            <input name="commission_true" id="commission_true" value={{ isset($settings[0]) ? $settings[0]->value : 2 }} type="hidden">
                            <div class="input-group-append">
                                <span class="input-group-text">% percent</span>
                            </div>
                        </div>
                    </div>
                    <div id="after_commission_display" style="display:none">
                        <label>Commission Deduction</label>
                        <div class="form-group">
                            <input name="after_commission" id="after_commission" type="text" class="form-control" readonly>
                            <input name="commission_value" id="commission_value" type="hidden">
                        </div>
                    </div>
                    </input>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        Save
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>