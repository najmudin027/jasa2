<script>
    var JobExperience = {
		staticTable: function(selector){
			return $(selector).DataTable({
	            dom: 'Bflrtip',
	            responsive: true,
	            language: {
	                search: '',
	                searchPlaceholder: "Search..."
	            },
	            oLanguage: {
	                sLengthMenu: "_MENU_",
	            },
	            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
	                "<'row'<'col-sm-12'tr>>" +
	                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
	            columns: [
	            	{
	                    data: "job_title_category_name",
	                    name: "job_title_category_name",
	                },
	                {
	                    data: "job_title_description",
	                    name: "job_title_description",
	                },
	                {
	                    data: "position",
	                    name: "position",
	                },
	                {
	                    data: "company_name",
	                    name: "company_name",
	                },
	                {
	                    data: "period_start",
	                    name: "period_start",
	                    render: function(data, type, full){
		                    return moment(full.period_end).format("DD MMMM YYYY");
		                }
	                },
	                {
	                    data: "period_end",
	                    name: "period_end",
	                    render: function(data, type, full){
		                    return moment(full.period_end).format("DD MMMM YYYY");
		                }
	                },
	                {
	                    data: "type",
	                    name: "type",
	                    render: function(data, type, full){
		                    return full.type == 0 ? 'Pengalaman Internal' : 'Pengalaman External';
		                }
	                },
	                {
	                	render: function(data, type, full) {
		                    return "<button type='button' class='delete-experience btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>";
		                }
	                }
	            ],
	        });
		},
		table: function(config){
			return $(config.selector).DataTable({
	            processing: true,
                serverSide: true,
                select: true,
                dom: 'Bflrtip',
                ordering:'true',
                order: [4, 'desc'],
                responsive: true,
                language: {
                    search: '',
                    searchPlaceholder: "Search..."
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                },
                dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                ajax: {
                    url: Helper.apiUrl(config.url),
                    "type": "get"
                },
	            columns: [
	            	{
	                    data: "job_title.job_title_category.name",
	                    name: "job_title.job_title_category.name",
	                },
	                {
	                    data: "job_title.description",
	                    name: "job_title.description",
	                },
	                {
	                    data: "position",
	                    name: "position",
	                },
	                {
	                    data: "company_name",
	                    name: "company_name",
	                },
	                {
	                    data: "period_start",
	                    name: "period_start",
	                    render: function(data, type, full){
		                    return moment(full.period_end).format("DD MMMM YYYY");
		                }
	                },
	                {
	                    data: "period_end",
	                    name: "period_end",
	                    render: function(data, type, full){
		                    return moment(full.period_end).format("DD MMMM YYYY");
		                }
	                },
	                {
	                    data: "type",
	                    name: "type",
	                    render: function(data, type, full){
		                    return full.type == 0 ? 'Pengalaman Internal' : 'Pengalaman External';
		                }
	                },
	                {
	                	render: function(data, type, full) {
		                    return "<button data-id='"+full.id+"' type='button' class='delete-experience btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>";
		                }
	                }
	            ],
	        });
		}
	};
</script>