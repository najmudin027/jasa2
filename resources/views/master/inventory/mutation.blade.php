@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/inventory/show') }}" class="btn btn-primary btn-sm">Back</a>
            </div>
        </div>
        <div class="card-body">
            <form action="" method="post" id="form-inventory-mutation">
                <input name="id" value="{{ $inventory->id }}" type="hidden" class="form-control">
                <input name="ms_batch_item_id" value=" {{ $inventory->ms_batch_item_id }} " type="hidden" class="form-control">
                <input id="warehouse_id" value="{{  $inventory->warehouse->id }}" type="hidden" class="form-control">
                <input id="quantity_format" value="{{ $inventory->stock }}" type="hidden" class="form-control">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6  col-xl-5">
                            <div class="position-relative form-group">
                                <label for="" class="">Batch No: </label>
                                <b>{{ $inventory->batch_item->batch->batch_no }}</b>
                            </div>
                            <div class="position-relative form-group">
                                <label for="" class="">Warehouse: </label>
                                <b>{{ $inventory->warehouse->name }}</b>
                            </div>
                            <div class="position-relative form-group">
                                <label for="" class="">Stock: </label>
                                <b>{{ $inventory->stock }}</b>
                            </div>
                            <div class="position-relative form-group">
                                <label for="" class="">Product: </label>
                                <b>{{ $inventory->batch_item->product->name }}</b>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-5">
                            <div class="position-relative form-group">
                                <label for="" class="">Werehouse Inventory</label>
                                <select name="ms_warehouse_id" id="warehouse-select" class="form-control" required>
                                    <option value="">--Pilih--</option>
                                </select>
                            </div>

                            <div class="position-relative form-group">
                                <label for="" class="">Stock</label>
                                <input name="stock" id="stock" type="number" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <button class="btn btn-primary btn-sm btn-add-new-item" type="submit"><i class="fa fa-save"></i> Save</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
@include('admin.master.inventory.select2')
<script>
    // select2
    globalCRUD
        .select2('#warehouse-select', '/warehouses/select2p/'+$('#warehouse_id').val()+'')

    var max = parseInt($('#quantity_format').val());
    console.log($('#quantity_format').val());
    $('#stock').keyup(function(e) {
        if ($(this).val() > max) {
        e.preventDefault();
        $(this).val(max);
        } else {
            $(this).val();
        }
    });
    $('#form-inventory-mutation').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        Helper.confirm( ()=> {
            $.ajax({
                url: Helper.apiUrl('/inventory/mutation'),
                type: 'post',
                data: data,
                processData: false,
                contentType: false,
                success: function(response) {
                    Helper.successNotif('History shipment success updated !')
                    window.location.href = Helper.redirectUrl('/admin/inventory/show');
                }
            });
        });
    })
</script>

@endsection
