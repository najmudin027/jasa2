@extends('admin.home')
@section('content')
<style>
.legend {
    font-size:8pt;
    margin-bottom:7px;
}


.ck-editor__editable_inline {
    min-height: 350px;
}
</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/email-other/show') }}" class="mb-2 mr-2 btn btn-primary add-bank-transfer" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
        <form id="form-email-other">
            <div class="card-body">
                <div class="card-header">
                    Template Email Other - {{ $getData[0]->description }}
                    <div class="btn-actions-pane-right">
                        <div class="nav">
                            @php
                                $name = '';
                            @endphp
                            @foreach($types as $key => $type)
                            @php
                                if($type === 1) {
                                    $name = 'Template Customer';
                                } else if($type === 2) {
                                    $name = 'Template Teknisi';
                                } else {
                                    $name = 'Template Admin';
                                }
                            @endphp
                                <a data-toggle="tab" href="#tab-eg2-{{ $key }}" class="btn-tab btn-wide <?php if($key==0) {echo 'active';}?> btn btn-outline-success btn-md" data-desc="customer">{{ $name }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <input type="hidden" id="id_email" name="id_email" value="{{ $id_email }}">
                        <input type="hidden" id="module_name" name="module_name" value="{{ $getData[0]->name_module }}">
                        @foreach($getData as $key => $data)
                            <div class="tab-pane <?php if($key==0) {echo 'active';}?>" id="tab-eg2-{{ $key }}" role="tabpanel">
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">Subject</label>
                                    <div class="col-sm-10">
                                        <input name="subject{{ $key }}" value="{{ $data->subject }}" placeholder="Title Name" type="text" class="form-control" required>
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">Content</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="content{{ $key }}" id="content{{ $key }}" maxlength="300" placeholder="Start Typin..." >{{ $data->content }}</textarea>
                                    </div>
                                    <div class="col-sm-2">
                                        <h6>Legend : </h6>
                                        @if($data->name_module == 'rejected_registration' || $data->name_module == 'verify_registration' || $data->name_module == 'forgot_password' || $data->name_module == 'approved_regristration')
                                            <p class="legend">[{NOTE}]</p>
                                            <p class="legend">[{NAME_CUSTOMER}]</p>
                                        @else
                                            <p class="legend">[{NAME_CUSTOMER}]</p>
                                            <p class="legend">[{EMAIL_CUSTOMER}]</p>
                                            <p class="legend">[{NAME_TECHNICIAN}]</p>
                                            <p class="legend">[{EMAIL_TECHNICIAN}]</p>
                                            <p class="legend">[{ORDER_CODE}]</p>
                                            @if($data->name_module == 'ticket')
                                                <p class="legend">[{TICKET_NO}]</p>
                                                <p class="legend">[{SUBJECT_TICKET}]</p>
                                                <p class="legend">[{DESCRIPTION_TICKET}]</p>
                                                <p class="legend">[{STATUS_TICKET}]</p>
                                            @elseif($data->name_module =='review' && $data->type =='2')
                                                <p class="legend">[{REVIEW_CUSTOMER}]</p>
                                                <p class="legend">[{RATING_REVIEW}]</p>
                                            @else
                                                <p class="legend">[{STATUS_ORDER}]</p>
                                                <p class="legend">[{SCHEDULE}]</p>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer ">
                    <div class="btn-actions-pane-right">
                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Save Change</button>
                    </div>
                </div>

            </div>
        </form>
        </div>

    </div>
</div>
@endsection

@section('script')

<script>
    Helper.wysiwygEditor('#content0');
    Helper.wysiwygEditor('#content1');
    $('#form-email-other').submit(function(e){
        console.log('asd');
        globalCRUD.handleSubmit($(this))
            .updateTo(function(formData){
                return '/email-other/' + formData.module_name;
            })
            .redirectTo(function(resp){
                return '/admin/email-other/show'
            })
        e.preventDefault();
    })

</script>


@endsection
