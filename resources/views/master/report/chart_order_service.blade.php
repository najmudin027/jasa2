@extends('admin.home')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>

        <div class="col-md-12" style="padding-top: 10px">
            <div class="card header-border">
                <div class ="card-body">
                    <div class="wr_inner">
                        {{-- <div class="toolbar_chart">

                            <button id="one_week1">
                            1 week
                            </button>

                            <button id="one_month1">
                                1 month
                            </button>

                            <button id="three_months1">
                                3 months
                            </button>

                            <button id="six_months1">
                                6 months
                            </button>

                            <button id="one_year1">
                                1 year
                            </button>
                        </div> --}}
                        <div id="chart-timeline1"></div>
                        {{-- <div id="chart-bar1"></div> --}}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12" style="padding-top: 10px">
            <div class="card header-border">
                <div class ="card-body">
                    <div class="wr_inner">
                        {{-- <div class="toolbar_chart">
                            <button id="one_week2">
                            1 week
                            </button>

                            <button id="one_month2">
                                1 month
                            </button>

                            <button id="three_months2">
                                3 months
                            </button>

                            <button id="six_months2">
                                6 months
                            </button>

                            <button id="one_year2">
                                1 year
                            </button>
                        </div> --}}
                        <div id="chart-timeline2"></div>
                        {{-- <div id="chart-bar2"></div> --}}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



@endsection

@section('script')
<style>
    .block-chart{
        padding-top: 24px;
    }
    .toolbar_chart button{
        font-size: 24px;
        line-height: 1.42;
        color: #a0a0a0;
        display: inline-block;
        margin-right: 36px;
        cursor: pointer;
        background: none;
        border: none;
        box-shadow: none;
    }
    .change-type-currency{
        display: flex;
        align-items: center;
        padding-bottom: 24px;
    }
    .change-type-currency > .bootstrap-select{
        max-width: 280px;
    }
    .arrow-currency{
        margin: 0 16px;
    }
    .change-type-currency .bootstrap-select > .dropdown-toggle{
        padding-left: 22px;
    }
    .change-type-currency .bootstrap-select.btn-group .dropdown-toggle .filter-option{
        font-size: 17px;
    }
    .change-type-currency .caret {
        border-top: 5px solid;
        border-right: 5px solid transparent;
        border-left: 5px solid transparent;
    }
    .toolbar_chart .active{
        color: #75af26;
    }
</style>


<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/apexcharts/3.17.0/apexcharts.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
    var today = moment();
    var from_date = today.startOf('week');
    var to_date = today.endOf('week');
     bu_total_order ='{{ $bu_total_order }}';
     dataBuTotalOrder = JSON.parse(bu_total_order.replace(/&quot;/g,'"'));
     bu_sparepart_sold ='{{ $bu_sparepart_sold }}';
     dataBuSparepartSold = JSON.parse(bu_sparepart_sold.replace(/&quot;/g,'"'));
     bu_completed_order ='{{ $bu_completed_order }}';
     dataBuCompletedOrder = JSON.parse(bu_completed_order.replace(/&quot;/g,'"'));
     bu_pending_order ='{{ $bu_pending_order }}';
     dataBuPendingOrder = JSON.parse(bu_pending_order.replace(/&quot;/g,'"'));
     bu_cancel_order ='{{ $bu_cancel_order }}';
     dataBuCancelOrder = JSON.parse(bu_cancel_order.replace(/&quot;/g,'"'));


    let chart1,
        colorBU = "#269cb5",
        time1,
        options1 = {
            chart: {
                id: "chartbu2",
                height: 365,
                type: "line",

                // stacked: true,
                fontFamily: '"Noto Sans", sans-serif',
                animations: {
                    enabled: false,
                }
            },
            series: [
                {
                    name: "Unit Total Order",
                    data: dataBuTotalOrder
                },
                {
                    name: "Unit Sparepart Sold",
                    data: dataBuSparepartSold
                },
                {
                    name: "Unit Completed Order",
                    data: dataBuCompletedOrder
                },
                {
                    name: "Unit Pending Order",
                    data: dataBuPendingOrder
                },
                {
                    name: "Unit Cancel Order",
                    data: dataBuCancelOrder
                }
            ],
            colors: ["#269cb5","#80b048","#ffb400","#9762a2","#aaaaaa"],
            stroke: {
                width: [3, 3],
                curve: 'smooth'
            },
            markers: {
                size: 0,
                strokeWidth: 20,
                shape: "circle",
                strokeOpacity: 0.3,
                strokeColors: '#a0a0a0',
                hover: {
                    size: 5,
                }
            },
            grid: {
                borderColor: '#d8d8d8',
                xaxis: {
                    lines: {
                        show: true
                    }
                },
                yaxis: {
                    lines: {
                        show: true
                    }
                }
            },

            dataLabels: {
                enabled: false
            },
            xaxis: {
                tooltip: {
                    enabled: true
                },
                min: from_date,
                max: to_date,
                type: 'datetime',
            },
            yaxis: {
                // min: 24.400,
                // max: 25.355,
                tickAmount: 5,
                tooltip: {
                    enabled: false
                },

            },
            // tooltip: {
            //     followCursor: true,
            //     custom: function({series, seriesIndex, dataPointIndex, w}) {
            //         time = w.config.series[seriesIndex].data[dataPointIndex][0];
            //         return '<div class="box-tooltip">' +
            //                     '<div class="time-tooltip">' +
            //                         '<p class="full-date"> ' + moment(time).locale('ua').format("hh:mm:ss - DD MMM YYYY") + ' </p>'
            //                     + '</div>' +
            //
            //                     '<div class="sale-info">' +
            //                         '<p class="text-sale">Sale</p>' + '<p class="marker">: </p>' + '<p class="sale-amount">'+ series[0][dataPointIndex] +' $</p>'
            //                     + '</div>' +
            //
            //                     '<div class="buy-info">' +
            //                         '<p class="text-buy">Buy</p>' + '<p class="marker">: </p>' + '<p class="buy-amount"> '+ series[1][dataPointIndex] +' $</p>'
            //                     + '</div>' +
            //                 '</div>'
            //     }
            // }
        };

    chart1 = new ApexCharts(document.querySelector("#chart-timeline1"), options1);
    chart1.render();


    let resetCssClasses1 = function (activeEl) {
        let els = document.querySelectorAll("button");
        Array.prototype.forEach.call(els, function (el) {
            el.classList.remove('active');
        });

        activeEl.target.classList.add('active')
    }



    var optionsLine1 = {
        series: [
            {
                data: dataBuTotalOrder
            },
            {
                data: dataBuSparepartSold
            },
            {
                data: dataBuCompletedOrder
            },
            {
                data: dataBuPendingOrder
            },
            {
                data: dataBuCancelOrder
            }
        ],
        chart: {
            id: 'chartbu1',
            height: 150,
            type: 'line',
            // stacked: true,
            brush: {
                target: "chartbu2",
                enabled: true
            },
            selection: {
                enabled: true,
                xaxis: {
                    min: from_date,
                    max: to_date
                }
            },
        },
        colors: ["#269cb5","#80b048","#ffb400","#9762a2","#aaaaaa"],
        stroke: {
            width: [2, 2],
            curve: 'smooth'
        },
        xaxis: {
            type: 'datetime',
            tooltip: {
                enabled: true
            }
        },
        yaxis: {
            tickAmount: 2,
            formatter: function (value) {
                return value + "Rp.";
            }
            // min: 24.41,
            // max: 25.355
        }
    };

    var chartLine1 = new ApexCharts(document.querySelector("#chart-bar2"), optionsLine1);


    document.querySelector("#one_week1").addEventListener('click', function (e) {
        resetCssClasses1(e)
        chartLine1.updateOptions({
            xaxis: {
                min: from_date,
                max: to_date
            }
        });
        chart1.updateOptions({
            xaxis: {
                min: from_date,
                max: to_date
            }
        });

    })

    document.querySelector("#one_month1").addEventListener('click', function (e) {
        resetCssClasses(e)
        chartLine1.updateOptions({
            xaxis: {
                min: new Date('24 Feb 2020').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });
        chart1.updateOptions({
            xaxis: {
                min: new Date('24 Feb 2020').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }});

    })

    document.querySelector("#three_months1").addEventListener('click', function (e) {
        resetCssClasses1(e)
        chartLine1.updateOptions({
            xaxis: {
                min: new Date('24 Dec 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });
        chart1.updateOptions({
            xaxis: {
                min: new Date('24 Dec 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });

    })

    document.querySelector("#six_months1").addEventListener('click', function (e) {
        resetCssClasses1(e)
        chartLine1.updateOptions({
            xaxis: {
                min: new Date('24 Sep 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });
        chart1.updateOptions({
            xaxis: {
                min: new Date('24 Sep 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });

    })

    document.querySelector("#one_year1").addEventListener('click', function (e) {
        resetCssClasses1(e)
        chartLine1.updateOptions({
            xaxis: {
                min: new Date('25 Mar 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });
        chart1.updateOptions({
            xaxis: {
                min: new Date('25 Mar 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });

    })

    chartLine1.render();

</script>


<script>

     bc_total_order ='{{ $bc_total_order }}';
     dataBcTotalOrder = JSON.parse(bc_total_order.replace(/&quot;/g,'"'));
     bc_sparepart_sold ='{{ $bc_sparepart_sold }}';
     dataBcSparepartSold = JSON.parse(bc_sparepart_sold.replace(/&quot;/g,'"'));
     bc_completed_order ='{{ $bc_completed_order }}';
     dataBcCompletedOrder = JSON.parse(bc_completed_order.replace(/&quot;/g,'"'));
     bc_pending_order ='{{ $bc_pending_order }}';
     dataBcPendingOrder = JSON.parse(bc_pending_order.replace(/&quot;/g,'"'));
     bc_cancel_order ='{{ $bc_cancel_order }}';
     dataBcCancelOrder = JSON.parse(bc_cancel_order.replace(/&quot;/g,'"'));

    let chart2,

        colorBC = "#75af26",
        time2,
        options2 = {
            chart: {
                id: "chartbc2",
                height: 365,
                type: "line",

                // stacked: true,
                fontFamily: '"Noto Sans", sans-serif',
                animations: {
                    enabled: false,
                }
            },
            series: [

                {
                    name: "Money Total Order",
                    data: dataBcTotalOrder
                },
                {
                    name: "Money Sparepart Sold",
                    data: dataBcSparepartSold
                },
                {
                    name: "Money Completed Order",
                    data: dataBcCompletedOrder
                },
                {
                    name: "Money Pending Order",
                    data: dataBcPendingOrder
                },
                {
                    name: "Money Cancel Order",
                    data: dataBcCancelOrder
                }
            ],
            colors: ["#269cb5","#80b048","#ffb400","#9762a2","#aaaaaa"],
            stroke: {
                width: [3, 3],
                curve: 'smooth'
            },
            markers: {
                size: 0,
                strokeWidth: 20,
                shape: "circle",
                strokeOpacity: 0.3,
                strokeColors: '#a0a0a0',
                hover: {
                    size: 5,
                }
            },
            grid: {
                borderColor: '#d8d8d8',
                xaxis: {
                    lines: {
                        show: true
                    }
                },
                yaxis: {
                    lines: {
                        show: true
                    }
                }
            },

            dataLabels: {
                enabled: false
            },
            xaxis: {
                tooltip: {
                    enabled: true
                },
                min: from_date,
                max: to_date,
                type: 'datetime',
            },
            yaxis: {
                // min: 24.400,
                // max: 25.355,
                tickAmount: 6,
                tooltip: {
                    enabled: false
                },

            },
            // tooltip: {
            //     followCursor: true,
            //     custom: function({series, seriesIndex, dataPointIndex, w}) {
            //         time = w.config.series[seriesIndex].data[dataPointIndex][0];
            //         return '<div class="box-tooltip">' +
            //                     '<div class="time-tooltip">' +
            //                         '<p class="full-date"> ' + moment(time).locale('ua').format("hh:mm:ss - DD MMM YYYY") + ' </p>'
            //                     + '</div>' +
            //
            //                     '<div class="sale-info">' +
            //                         '<p class="text-sale">Sale</p>' + '<p class="marker">: </p>' + '<p class="sale-amount">'+ series[0][dataPointIndex] +' $</p>'
            //                     + '</div>' +
            //
            //                     '<div class="buy-info">' +
            //                         '<p class="text-buy">Buy</p>' + '<p class="marker">: </p>' + '<p class="buy-amount"> '+ series[1][dataPointIndex] +' $</p>'
            //                     + '</div>' +
            //                 '</div>'
            //     }
            // }
        };

    chart2 = new ApexCharts(document.querySelector("#chart-timeline2"), options2);
    chart2.render();


    let resetCssClasses2 = function (activeEl) {
        let els = document.querySelectorAll("button");
        Array.prototype.forEach.call(els, function (el) {
            el.classList.remove('active');
        });

        activeEl.target.classList.add('active')
    }



    var optionsLine2 = {
        series: [

            {
                data: dataBcTotalOrder
            },
            {
                data: dataBcSparepartSold
            },
            {
                data: dataBcCompletedOrder
            },
            {
                data: dataBcPendingOrder
            },
            {
                data: dataBcCancelOrder
            }
        ],
        chart: {
            id: 'chartbc1',
            height: 150,
            type: 'line',
            // stacked: true,
            brush: {
                target: "chartbc2",
                enabled: true
            },
            selection: {
                enabled: true,
                xaxis: {
                    min: from_date,
                    max: to_date
                }
            },
        },
        colors: ["#269cb5","#80b048","#ffb400","#9762a2","#aaaaaa"],
        stroke: {
            width: [2, 2],
            curve: 'smooth'
        },
        xaxis: {
            type: 'datetime',
            tooltip: {
                enabled: true
            }
        },
        yaxis: {
            tickAmount: 2,
            formatter: function (value) {
                return value + "Rp.";
            }
            // min: 24.41,
            // max: 25.355
        }
    };

    var chartLine2 = new ApexCharts(document.querySelector("#chart-bar2"), optionsLine2);


    document.querySelector("#one_week2").addEventListener('click', function (e) {
        resetCssClasses2(e)
        chartLine2.updateOptions({
            xaxis: {
                min: from_date,
                max: to_date
            }
        });
        chart2.updateOptions({
            xaxis: {
                min: from_date,
                max: to_date
            }
        });

    })

    document.querySelector("#one_month2").addEventListener('click', function (e) {
        resetCssClasses2(e)
        chartLine2.updateOptions({
            xaxis: {
                min: new Date('24 Feb 2020').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });
        chart2.updateOptions({
            xaxis: {
                min: new Date('24 Feb 2020').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }});

    })

    document.querySelector("#three_months2").addEventListener('click', function (e) {
        resetCssClasses2(e)
        chartLine2.updateOptions({
            xaxis: {
                min: new Date('24 Dec 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });
        chart2.updateOptions({
            xaxis: {
                min: new Date('24 Dec 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });

    })

    document.querySelector("#six_months2").addEventListener('click', function (e) {
        resetCssClasses2(e)
        chartLine2.updateOptions({
            xaxis: {
                min: new Date('24 Sep 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });
        chart2.updateOptions({
            xaxis: {
                min: new Date('24 Sep 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });

    })

    document.querySelector("#one_year2").addEventListener('click', function (e) {
        resetCssClasses2(e)
        chartLine2.updateOptions({
            xaxis: {
                min: new Date('25 Mar 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });
        chart2.updateOptions({
            xaxis: {
                min: new Date('25 Mar 2019').getTime(),
                max: new Date('25 Mar 2020').getTime()
            }
        });

    })

    chartLine2.render();

</script>
@endsection
