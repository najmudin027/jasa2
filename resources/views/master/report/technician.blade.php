@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Technicians</label>
                            <select name="technician_id" id="technician_id" class="form-control" style="width:100%;" required> </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Date</label>
                            <input name="order_date_range" id="order_date_range" type="text" class="form-control"/>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12" style="padding-top: 10px">
    <div class="card header-border">
        <div class="card-body table-responsive">
            <table id="table-te" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Full Name</th>
                        <th>Email</th>

                        <th>Total Services</th>
                        <th>Total Order Success</th>
                        <th>Total Order Pending</th>
                        <th>Total Order Transactions</th>
                        <th>Total Orders Revenue</th>
                        <th>Total Orders Pending Revenue</th>
                        <th>Sparepart Sold</th>
                        <th>Sparepart Revenue</th>
                    </tr>
                </thead>
            </table>
        </div>

    </div>
</div>


@endsection

@section('script')
<style>
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
    var tbl_history = null;
    globalCRUD.select2("#technician_id", '/technician/select2');
    $('input[name="order_date_range"]').daterangepicker();
    // table
    var table = globalCRUD.datatables({
            url: '/admin/report/datatables_technician',
            selector: '#table-te',
            columnsField: [{
                    name: 'name',
                    data: 'name',
                    render: function(data,type,full) {
                        return '<a href="'+Helper.url('/admin/user/detail/'+full.user.id+'')+'">'+data+'</a>';
                    }
                }, 'email', 'total_services', 'total_pending_orders', 'total_success_orders', 'total_transactions', 'total_orders_revenue', 'total_orders_revenue_pending', 'total_spareparts_sold', 'total_spareparts_sold_revenue'],
            actionLink: '',
            export: true
        })


    // search data
    $("#form-search")
        .submit(function(e) {
            input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/admin/report/datatables_technician/search' + playload);
            table.table.reloadTable(url);
            e.preventDefault();
        })
</script>
@endsection
