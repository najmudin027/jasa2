@extends('admin.home')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Status Order</label>
                            <select name="order_statuses_id" id="order_statuses_id" class="form-control" style="width:100%;" multiple="multiple" > </select>
                            <label for="" class="">Customer</label>
                            <select name="customer_id" id="customer_id" class="form-control" style="width:100%;" multiple="multiple" > </select>
                            <label for="" class="">Technician</label>
                            <select name="technician_id" id="technician_id" class="form-control" style="width:100%;" multiple="multiple" > </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Payment Type</label>
                            <select name="payment_type" id="payment_type" class="mutiple-select2 form-control" style="width:100%;" multiple="multiple" >
                                <option value=1>Online</option>
                                <option value=0>Offline</option>
                            </select>
                            <label for="" class="">Date</label>
                            <input name="order_date_range" id="order_date_range" type="text" class="form-control"/>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12" style="padding-top: 10px">
    <div class="card header-border">
        <div class="card-body">
            <table id="table-te" class="display table table-hover table-bordered nowrap" style="width:100%">
                <thead>
                    <tr>

                        <th>Order Code</th>
                        <th>Customer Name</th>
                        <th>Payment Type</th>
                        <th>Order Status</th>
                        <th>Grand Total</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection

@section('script')

<style>
    .select2-container {
    min-width: 400px;
    }

    .select2-results__option {
    padding-right: 20px;
    vertical-align: middle;
    }
    .select2-results__option:before {
    content: "";
    display: inline-block;
    position: relative;
    height: 20px;
    width: 20px;
    border: 2px solid #e9e9e9;
    border-radius: 4px;
    background-color: #fff;
    margin-right: 20px;
    vertical-align: middle;
    }
    .select2-results__option[aria-selected=true]:before {
    font-family:fontAwesome;
    content: "\f00c";
    color: #fff;
    background-color: #f77750;
    border: 0;
    display: inline-block;
    padding-left: 3px;
    }
    .select2-container--default .select2-results__option[aria-selected=true] {
        background-color: #fff;
    }
    .select2-container--default .select2-results__option--highlighted[aria-selected] {
        background-color: #eaeaeb;
        color: #272727;
    }
    .select2-container--default .select2-selection--multiple {
        margin-bottom: 10px;
    }
    .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
        border-radius: 4px;
    }
    .select2-container--default.select2-container--focus .select2-selection--multiple {
        border-color: #f77750;
        border-width: 2px;
    }
    .select2-container--default .select2-selection--multiple {
        border-width: 2px;
    }
    .select2-container--open .select2-dropdown--below {

        border-radius: 6px;
        box-shadow: 0 0 10px rgba(0,0,0,0.5);

    }
    .select2-selection .select2-selection--multiple:after {
        content: 'hhghgh';
    }
</style>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    var tbl_history = null;

    globalCRUD.select2Tags("#order_statuses_id", '/order-status/select2');
    globalCRUD.select2Tags("#payment_type");
    globalCRUD.select2Tags("#customer_id", '/user/teknisi-select2');
    globalCRUD.select2Tags("#technician_id", '/technician/select2');
    $('input[name="order_date_range"]').daterangepicker();
    // table
    var table = globalCRUD.datatables({
            url: '/order/datatables',
            selector: '#table-te',
            columnsField: ['code', 'user.name', 'pay_type', 'order_status.name', 'grand_total'],
            actionLink: '',

            export:true
        })


    // search data
    $("#form-search")
        .submit(function(e) {
            // input = Helper.serializeForm($(this));
            var data = {};
            data.order_statuses_id = $('#order_statuses_id').val();
            data.payment_type = $('#payment_type').val();
            data.customer_id = $('#customer_id').val();
            data.technician_id = $('#technician_id').val();
            data.order_date_range = $('#order_date_range').val();

            playload = '?';
            _.each(data, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/admin/report/datatables_order_service/search' + playload);
            table.table.reloadTable(url);
            e.preventDefault();
        })
</script>
@endsection
