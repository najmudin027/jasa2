@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table id="table-ticket" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>TICKET NO</th>
                        <th>SUBJECT</th>
                        <th>USER</th>
                        <th>ORDER</th>
                        <th>STATUS</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    var table = $('#table-ticket')
        .DataTable({
            processing: true,
            serverSide: true,
            select: true,
            dom: 'Bflrtip',
            ordering:'true',
            order: [1, 'desc'],
            responsive: true,
            language: {
                search: '',
                searchPlaceholder: "Search..."
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/tickets/datatables'),
                "type": "get"
            },
            columns: [{
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                    orderable: false,
                    searchable: false,
                    width: "10%"
                },
                {
                    data: "ticket_no",
                    name: "ticket_no",
                    render: function(data, type, full) {
                        return "<a href='/admin/tickets/"+ full.id + "'>"+full.ticket_no+"</a>";
                    }
                },
                {
                    data: "subject",
                    name: "subject",
                },
                {
                    data: "user.name",
                    name: "user.name",
                    orderable: false,
                    searchable: false,
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row){
                        if (row.order_id == null) {
                            return "<button type='button' class='btn btn-drak'><i style='font-size: 24px;' class='fa fa-square'></i></button>";
                        }else{
                            return "<button type='button' class='btn btn-drak'><i class='fa fa-check-square' style='font-size: 24px;'></i></button>";
                        }
                    }
                },
                {
                    data: "status",
                    name: "status",
                    render: function(data, type, full) {
                        switch (full.status) {
                            case '1':
                                return '<span class="btn btn-sm btn-primary"><i class="fa fa-envelope-open"></i> Open</span>';
                                break;

                            case '2':
                                return '<span class="btn btn-sm btn-info">on progress</span>';
                                break;

                            case '3':
                                return '<span class="btn btn-sm btn-dark">on resolving</span>';
                                break;

                            case '4':
                                return '<span class="btn btn-sm btn-success">done</span> ';
                                break;

                            case '5':
                                return '<span class="btn btn-sm btn-danger"><i class="fa fa-close"></i> Close</span> ';
                                break;

                            default:
                                return full.status;
                                break;
                        }
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        detail_link = "<a href='/admin/tickets/"+ full.id + "' class='btn btn-search btn-sm btn-info'><i class='fa fa-arrow-right'></i></a>";
                        order_link = '';
                        if(full.order_id == null){
                            order_link = "<a href='/admin/transactions/create/"+ full.id + "' class='btn btn-search btn-sm btn-dark'><i class='fa fa-plus'></i> </a>";
                        }

                        delete_btn = "<button class='btn btn-delete btn-sm btn-danger' data-id='"+full.id+"'><i class='fa fa-trash'></i> </button>";

                        return detail_link +' '+ order_link +' '+ delete_btn;
                    }
                }
            ]
        });

    $(document)
        .on('click', '.btn-delete', function(e){
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.delete('/tickets/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('deleted');
                        // reload table
                        table.ajax.reload();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
            e.preventDefault();
        })
</script>
@endsection
