@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table id="example" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name Warehouse</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ route('admin.warehouse.create.web') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> ADD WAREHOUSE</a>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    globalCRUD.datatables({
        url: '/warehouses/datatables',
        selector: '#example',
        columnsField: ['DT_RowIndex', 'name'],
        actionLink: {
            update: function(row) {
                return '/admin/warehouse/' + row.id + '/edit';
            },
            delete: function(row) {
                return '/warehouses/' + row.id;
            }
        }
    })
</script>
@endsection