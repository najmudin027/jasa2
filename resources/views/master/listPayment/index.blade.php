@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add-list-payment">Add Brand</button>
                <!-- <a href="{{ url('/create-product-brand') }}" class="mb-2 mr-2 btn btn-primary btn-sm" style="float:right">Add Brands</a> -->
            </div>
        </div>
        <div class="card-body">
            <table id="table-list-payment" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Payment Method</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@include('admin.master.listPayment.modalListPayment')
@endsection

@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
    // agar user profile bisa diclik
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    globalCRUD.datatables({
        url: '/list_payment/datatables',
        selector: '#table-list-payment',
        columnsField: ['id', 'payment_method', 'desc',],
        modalSelector: "#modal-list-payment",
        modalButtonSelector: ".add-list-payment",
        modalFormSelector: "#form-list-payment",
        actionLink: {
            store: function() {
                return "/bank_transfer/";
            },
            update: function(row) {
                return "/list_payment/" + row.id;
            },
            delete: function(row) {
                return "/list_payment/" + row.id;
            },
            deleteBatch: function(selectedid) {
                return "list_payment/update_status/"; // API
            },
        }
    })
</script>
@endsection
