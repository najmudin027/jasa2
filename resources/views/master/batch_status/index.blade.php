@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add-batch_status">ADD BATCH STATUS</button>
                <!-- <a href="{{ url('/create-additional') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">Add Additional</a>  -->
            </div>
        </div>
        <div class="card-body">
            <table id="table-batch_status" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th style="width: 20px;"></th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<form id="form-batch_status">
    <div class="modal fade" id="modal-batch_status" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Status</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Name</label>
                    <div class="form-group">
                        <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        url: '/batch-status/datatables',
        selector: '#table-batch_status',
        columnsField: ['id', 'name'],
        modalSelector: "#modal-batch_status",
        modalButtonSelector: ".add-batch_status",
        modalFormSelector: "#form-batch_status",
        actionLink: {
            store: function() {
                return "/batch-status";
            },
            update: function(row) {
                return "/batch-status/" + row.id;
            },
            delete: function(row) {
                return "/batch-status/" + row.id;
            },
        },
    })
</script>
@endsection
