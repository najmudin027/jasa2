<form id="form-unit-type">
    <div class="modal fade" id="modal-unit-type" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create Unit Types</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label class="label-control">Name</label>
                    <div class="form-group">
                        <input name="unit_name" id="name" placeholder="Unit Name" id="unit_name" type="text" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
                    <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close </button>
                </div>
            </div>
        </div>
    </div>
</form>