@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/services/create') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Services</a>
            </div>

        </div>

        <div class="card-body">
        <table style="width: 100%;" id="table-services" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Service Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
        globalCRUD.datatables({
            url: '/services/datatables',
            selector: '#table-services',
            columnsField: ['id', 'name'],
            actionLink: {
                update: function(row) {
                    return "/admin/services/edit/" + row.id;
                },
                delete: function(row) {
                    return "/services/" + row.id;
                }
            }
        })
    </script>
@endsection
