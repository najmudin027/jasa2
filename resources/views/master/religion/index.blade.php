@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-religion" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
            <button class="btn btn-primary btn-sm add-religion"><i class="fa fa-plus"></i> ADD RELIGION</button>
        </div>
    </div>
</div>
@include('admin.master.religion.religionModal')
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        url: '/religion/datatables',
        selector: '#table-religion',
        columnsField: ['DT_RowIndex', 'name'],
        modalSelector: "#modal-religion",
        modalButtonSelector: ".add-religion",
        modalFormSelector: "#form-religion",
        actionLink: {
            store: function() {
                return "/religion";
            },
            update: function(row) {
                return "/religion/" + row.id;
            },
            delete: function(row) {
                return "/religion/" + row.id;
            },
        }
    })
</script>
@endsection
