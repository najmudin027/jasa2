@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table id="table" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>No.</th>
                        <th>Province Name</th>
                        <th>Country Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ route('admin.province.create.web') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> ADD PROVINCE</a>
            <button type="button" class="mb-2 mr-2 btn btn-sm btn-danger delete-selected"><i class="fa fa-remove"></i> DELETE PROVINCE</button>
        </div>
    </div>
</div>

@include('admin.master.modalImportExcel')
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        orderBy: [2, 'asc'],
        url: '/province/datatables',
        columnsField: ['', 'DT_RowIndex', 'name', 'country.name'],
        actionLink: {
            update: function(row) {
                return "/admin/province/" + row.id + '/edit';
            },
            delete: function(row) {
                return "province/" + row.id; // API
            },
            deleteBatch: function(selectedid) {
                return "province/destroy_batch/"; // API
            },
            import: function(row) {
                return "province/importExcel"; //API
            }
        },
        selectable: true,
        import: true
    })
</script>
@endsection