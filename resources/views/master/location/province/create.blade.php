@extends('admin.home')
@section('content')
<div class="col-md-12 card">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            {{ $title }}
        </div>
        <div class="btn-actions-pane-right text-capitalize">
            <a href="{{ route('admin.province.index.web') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
        </div>
    </div>
    <div class="card-body">
        <form action="" method="post" id="form">
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Country</label>
                <div class="col-sm-10">
                    <select id="country" name="ms_country_id" style="width:100%">
                    </select>
                </div>
            </div>

            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input name="name" id="examplename" placeholder="Province" type="text" class="form-control">
                    <strong><span id="error-name" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-check">
                <div class="col-sm-10 offset-sm-2">
                    <button class="btn btn-success" id="save"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('script')
    {{-- @include('admin.script.master._provinceScript') --}}
    <script>
         $('#save').click(function(e){
        $.ajax({
            type:'post',
            data:{
                name : $('input[name="name"]').val(),
                ms_country_id : $('select[name="ms_country_id"]').val(),
            },

            url:Helper.apiUrl('/province'),
            success:function(html){
                console.log(html)
                iziToast.success({
                    title: 'Success',
                    position: 'topRight',
                    message:  'Province Has Been Saved'
                });
                window.location.href = Helper.redirectUrl('/admin/province');
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });
                    
                    })
                }
            },
        });
        e.preventDefault();
    })

        globalCRUD.select2("#country", '/country/select2');

        // $('#form').submit(function(e) {
        // globalCRUD.handleSubmit($(this))
        //     .storeTo('/province') // API
        //     .redirectTo(function(resp) {
        //         return '/admin/province/show';
        //     })

        // e.preventDefault();
        // });
    </script>
@endsection
