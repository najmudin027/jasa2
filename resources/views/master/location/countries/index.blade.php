@extends('admin.home')
@section('content')
<div class="col-md-12 card">
    <div class="header-bg card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            {{ $title }}
        </div>
    </div>
    <div class="card-body">
        <table id="table" class="display table table-hover table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th>No</th>
                    <th>Name</th>
                    <th>Code</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="card-footer">
        <a href="{{ route('admin.country.create.web') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> ADD COUNTRIES</a>
        <button type="button" class="mb-2 mr-2 btn btn-sm btn-danger delete-selected"><i class="fa fa-remove"></i> DELETE COUNTRIES</button>
    </div>
</div>
@include('admin.master.modalImportExcel')
@endsection

@section('script')

<script>
    globalCRUD.datatables({
        url: '/country/datatables',
        orderBy: [2, 'asc'],
        columnsField: ['', 'DT_RowIndex', 'name', 'code'],
        actionLink: {
            update: function(row) {
                return "/admin/country/" + row.id + '/edit';
            },
            delete: function(row) {
                return "country/" + row.id; // API
            },
            deleteBatch: function(selectedid) {
                return "country/destroy_batch/"; // API
            },
            import: function(row) {
                return "country/importExcel"; //API
            }
        },
        selectable: true,
        import: true
    })
</script>
@endsection