@extends('admin.home')
@section('content')
<div class="col-md-12 card">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            Edit Master ZipCode
        </div>
        <div class="btn-actions-pane-right text-capitalize">
            <a href="{{ route('admin.zipcode.index.web') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
        </div>
    </div>
    <div class="card-body">
        <form action="" method="post" id="form">
            <input type="hidden" value="{{ $editZipcode->id }}" name="id" >
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Country Name</label>
                <div class="col-sm-10">
                    <select id="country" name="ms_countries_id" style="width:100%">
                        <option value="{{ $editZipcode->district->city->province->country->id }}" selected>{{ $editZipcode->district->city->province->country->name }}</option>
                    </select>
                </div>
            </div>
            <div class="position-relative row form-group" id="provinceShow">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Province Name</label>
                <div class="col-sm-10">
                    <select id="province" name="ms_province_id" style="width:100%">
                        <option value="{{ $editZipcode->district->city->province->id }}" selected>{{ $editZipcode->district->city->province->name }}</option>
                    </select>
                </div>
            </div>
            <div class="position-relative row form-group" id="cityShow">
                <label for="exampleEmail" class="col-sm-2 col-form-label">City Name</label>
                <div class="col-sm-10">
                    <select id="city" name="ms_city_id" style="width:100%">
                        <option value="{{ $editZipcode->district->city->id }}" selected>{{ $editZipcode->district->city->name }}</option>
                    </select>
                    
                </div>
            </div>
            <div class="position-relative row form-group" id="districtShow">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Distric Name</label>
                <div class="col-sm-10">
                    <select id="district" name="ms_district_id" style="width:100%">
                        <option value="{{ $editZipcode->district->id }}" selected>{{ $editZipcode->district->name }}</option>
                    </select>
                    <strong><span id="error-ms_district_id" style="color:red"></span></strong>
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Zip-Code</label>
                <div class="col-sm-10">
                    <input name="zip_no" value="{{ $editZipcode->zip_no }}" id="examplename" placeholder="Distric" type="number" class="form-control">
                    <strong><span id="error-zip_no" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-check">
                <div class="col-sm-10 offset-sm-2">
                    <button class="btn btn-success" id="update"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script>
    $('#update').click(function(e) {
        id = $('input[name="id"]').val()
        $.ajax({
            type: 'put',
            data: {
                id: $('input[name="id"]').val(),
                zip_no: $('input[name="zip_no"]').val(),
                ms_district_id: $('select[name="ms_district_id"]').val(),
            },
            url: Helper.apiUrl('/zipcode/' + id),
            success: function(html) {
                iziToast.success({
                    title: 'OK',
                    position: 'topRight',
                    message: 'zipcode Has Been Saved',
                });
                console.log(html)
                window.location.href = Helper.redirectUrl('/admin/zipcode');
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });
                    
                    })
                }
            },
        });
        e.preventDefault();
    })
    // $('#form').submit(function(e) {
    //     globalCRUD.handleSubmit($(this))
    //         .updateTo(function(formData) { //API
    //             return '/zipcode/' + formData.id;
    //         })
    //         .redirectTo(function(resp) {
    //             return '/admin/zipcode/show';
    //         })

    //     e.preventDefault();
    // });
</script>
    @include('admin.script.master._zipcodeScript')
@endsection
