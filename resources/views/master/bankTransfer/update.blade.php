@extends('admin.home')
@section('content')
<style>
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 100%;
}
</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/bank/show') }}" class="mb-2 mr-2 btn btn-primary add-bank-transfer" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>

        <div class="card-body">
            <input type="hidden" id="bank_id" value="{{ $getDataBank->id }}">
            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Bank Name</label>
                <div class="col-sm-9">
                    <input name="bank_name" value="{{ $getDataBank->bank_name }}" placeholder="Bank Name" type="text" class="form-control">
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Account Name</label>
                <div class="col-sm-9">
                    <input name="name" value="{{ $getDataBank->name }}" placeholder="Account Name" type="text" class="form-control">
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Account Number</label>
                <div class="col-sm-9">
                    <input name="virtual_code" value="{{ $getDataBank->virtual_code }}" placeholder="Account Number" type="text" class="form-control">
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Description</label>
                <div class="col-sm-9">
                    <!-- <input name="desc" placeholder="desc" type="text" class="form-control"> -->
                    <textarea class="form-control" name="desc" id="the-textarea" maxlength="300" placeholder="Start Typin..."autofocus>{{ $getDataBank->desc }}</textarea>
                    <div id="the-count">
                        <span id="current">{{ strlen($getDataBank->desc) }}</span>
                        <span id="maximum">/ 300</span>
                    </div>
                </div>
            </div>


            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Icon</label>
                <div class="col-sm-6">
                    <input name="icon" placeholder="Virtual Code" type="file" class="form-control" id="imgInp">
                </div>
                <div class="col-sm-3">
                    @if($getDataBank->icon == "" )
                        <img id="img-upload" src="http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png" alt="your image" style="width:100px;height:100px"/>
                    @else
                        <img id='img-upload' src="{{asset('admin/storage/icon/' . $getDataBank->icon)}}">
                    @endif

                </div>
            </div>
            <div class="position-relative row form-check">
            <div class="col-sm-10 offset-sm-2">
                <button class="btn btn-success" id="save" style="float:right"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
            </div>
        </div><br>
        </div>

    </div>
</div>
@endsection

@section('script')
@include('admin.master.bankTransfer.countChar')
<script>
    $("#save").click(function() {
        var fd = new FormData();
        var files = $('#imgInp')[0].files[0];
        if(files){
            fd.append('icon', files);
        }
        fd.append('bank_name', $('input[name="bank_name"]').val());
        fd.append('name', $('input[name="name"]').val());
        fd.append('virtual_code', $('input[name="virtual_code"]').val());
        fd.append('desc', $('textarea[name="desc"]').val());
        $.ajax({
            url:Helper.apiUrl('/bank_transfer/'+$('#bank_id').val()+''),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Bank Transfer Has Been Saved',
                    });
                    window.location.href = Helper.redirectUrl('/admin/bank/show');
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });

                    })
                }

            },
        });
    });
</script>


<script>
    $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		});
	});
</script>

@endsection
