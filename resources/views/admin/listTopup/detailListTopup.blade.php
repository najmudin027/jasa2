@extends('admin.home')
@section('content')
<style>
    div.hr-or {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    div.hr-or:before {
        content: 'OR';
        background-color: #fff;
    }

    div.hr {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    .hr-title {
        background-color: #fff;
    }
</style>

<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header">
            Order Received
            <div class="btn-actions-pane-right">
                <a href="{{ url('/customer/topup/show-list') }}" class="btn btn-primary btn-sm add-bank-transfer">Back</a>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    {{-- <input type="hidden" name="ids" value="{{ $getDataTopupWallet->ms_wallet_id }}"> --}}
                    <table class="table table-striped">
                        @if(!empty($getDataTopupWallet->bank))
                            <thead>
                                <th>Bank</th>
                                <th>Rekening</th>
                                <th>A.N</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><img src="{{ asset('/customer/storage/bankIcon/' . $getDataTopupWallet->bank->icon) }}" alt="asd" width="50"></td>
                                    <td>{{ $getDataTopupWallet->bank->virtual_code }}</td>
                                    <td>{{ $getDataTopupWallet->bank->name }}</td>
                                    @if( $getDataTopupWallet->transfer_status_id == 0)
                                        <td><span class="badge badge-danger btn-sm">PENDING TRANSFER</span></td>
                                    @else
                                        <td><span class="badge badge-primary btn-sm">SUCCESS TRANSFER</span><br>AT : {{ date('d-M-Y', strtotime($getDataTopupWallet->updated_at)) }}</td>
                                    @endif
                                </tr>
                            </tbody>
                        @else
                            <thead>
                                <th>Payment Type</th>
                                <th>Payment Metod</th>
                                <th>Payment Status</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ $getDataTopupWallet->payment_metod_id }}</td>
                                    <td>{{ $getDataTopupWallet->payment_metod_type }}</td>
                                    @if( $getDataTopupWallet->transfer_status_id == 0)
                                        <td><span class="badge badge-danger btn-sm">PENDING TRANSFER</span></td>
                                    @else
                                        <td><span class="badge badge-primary btn-sm">SUCCESS TRANSFER</span><br>AT : {{ date('d-M-Y', strtotime($getDataTopupWallet->updated_at)) }}</td>
                                    @endif
                                </tr>
                            </tbody>
                        @endif
                    </table>
                    <div class="container">
                        <input type="hidden" id="id" class="form-control" name="id" value="{{ $getDataTopupWallet->id }}">
                        <div class="row">
                            @if(!empty($getDataTopupWallet->bank))
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="">Email</label>
                                                <input type="text" id="" class="form-control" value="{{ $getDataTopupWallet->user->email }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="" class="">Tanggal Transfer</label>
                                                <input name="transfer_date" id="datetimepicker" placeholder="" type="text" class="form-control" value="{{ date('d-M-Y', strtotime($getDataTopupWallet->transfer_date)) }}" readonly>
                                                <strong><span id="error-transfer_date" style="color:red"></span></strong>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="">A.N Rekening</label>
                                                <input type="text" id="" class="form-control" name="nama_rekening" value="{{ $getDataTopupWallet->nama_rekening }}" readonly>
                                                <strong><span id="error-a_n_bank" style="color:red"></span></strong>
                                            </div>

                                            <div class="form-group">
                                                <label for="" class="">Nominal</label>
                                                <input type="text" id="" class="form-control" name="nominal" value="Rp.{{ number_format($getDataTopupWallet->nominal) }}" readonly>
                                                <strong><span id="error-nominal" style="color:red"></span></strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                            <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="">Email</label>
                                                <input type="text" id="" class="form-control" value="{{ $getDataTopupWallet->user->email }}" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="" class="">Nominal</label>
                                                <input type="text" id="" class="form-control" name="nominal" value="Rp.{{ number_format($getDataTopupWallet->nominal) }}" readonly>
                                                <strong><span id="error-nominal" style="color:red"></span></strong>
                                            </div>
                                        </div>
                                        @if(!empty($getDataTopupWallet->voucher_id))
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="">Voucher Type</label>
                                                    <input type="text" id="" class="form-control" value="{{ $getDataTopupWallet->voucher_id == 1 ? 'Cashback' : 'Discount' }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="" class="">Amount</label>
                                                    <input type="text" id="" class="form-control" name="nominal" value="Rp.{{ number_format($getDataTopupWallet->number_of_pieces) }}" readonly>
                                                    <strong><span id="error-nominal" style="color:red"></span></strong>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                        @if(!empty($getDataTopupWallet->bank))
                            <div class="form-group">
                                @if(!empty($getDataTopupWallet->attachment))
                                    <a href="{{asset('/storage/buktiTransfer/' . $getDataTopupWallet->attachment)}}" target="_blank">
                                        <span class="badge badge-success badge-pill"><strong>View Attachment</strong></span>
                                    </a>
                                @else
                                    <span class="badge badge-warning badge-pill"><strong>Image Not Found</strong></span>
                                @endif
                                <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal">Show Attachment</button> -->
                            </div>
                            @if ($getDataTopupWallet->transfer_status_id == 1)
                                <button type="submit" class="btn btn-primary btn-lg btn-block" disabled>Approve This Top up</button>
                            @else
                                <button type="submit" class="btn btn-primary btn-lg btn-block" data-id="{{ $getDataTopupWallet->id }}" id="approve_topup">Approve This Top up</button>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<script>
    $(document).on('click', '#approve_topup', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/approved-topup/' + id ),
                type: 'post',

                success: function(res) {
                    Helper.successNotif('This Top up Has Approvd');
                    Helper.loadingStop();
                    Helper.redirectTo('/admin/topup/show-list');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        },
        {
            title: "Are You Sure",
            message: "<strong>Are you sure </strong></br> have you checked the customer top up data, if sure, press the confirmation button, if you haven't pressed the cancel button and double check",
        })
        
        e.preventDefault()
    })
</script>
@endsection
