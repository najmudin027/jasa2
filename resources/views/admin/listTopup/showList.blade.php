@extends('admin.home')
@section('content')

<div class="col-md-12" style="margin-top: 20px">

    <div class="card">
        <div class="card-header">
                WALLET TOP UP
            <div class="btn-actions-pane-right">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    TOP UP
                </button>
            </div>
        </div>
        
        <div class="card-body">
            
            
            <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>Success Top up</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($countCustomerTopup as $b)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $b->user->name }}</td>
                            <td>{{ $b->user->email }}</td>
                            <td>{{ $b->total_topup }}</td>
                            <td>
                                <a href="{{ url('/admin/topup/show-list-detail/' . $b->id) }}" class="badge badge-primary btn-sm" data-toggle="tooltip" data-html="true" title="<b>Detail</b>"><i class="fa fa-eye"></i></a></td>
                        </tr>
                    @endforeach
                </tbody> 
            </table>
        </div>
    </div>
</div>
{{-- modal --}}
<form id="form-topup">
    <div class="modal fade" id="exampleModal" data-backdrop="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Wallet Topups</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="inputs">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="10000" /> Rp.10,000</div>
                                    <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="50000" /> Rp.50,000</div>
                                    <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="200000" /> Rp.200,000</div>
                                    <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="500000" /> Rp.500,000</div>
                                </div>
                                
                            </div>
                        </div><br>
                      </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nominal Top up</label>
                        <input type="text" class="form-control topups"  name="nominal" placeholder="10000" id="amount" />
                        <input type="hidden" class="form-control"  name="note" placeholder="10000">
                        <small><strong>Note : </strong> Minimal Topup Rp 10.000.00</small><br><br>

                        <label for="exampleFormControlInput1">User Name</label>
                        <select class="js-example-basic-single" name="user_id" id="get_user" style="width:100%"></select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary waves-effect" type="submit">
                        Top up
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal fade bd-example-modal-lg" id="myModal" data-backdrop="false" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nominal Top up</label>
                        <input type="number" class="form-control" name="nominal" placeholder="10000" id="" />
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Transfer Date</label>
                        <input type="number" class="form-control" name="nominal" placeholder="2019-Mei-01" id="" />
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Attachment</label>
                        <input type="number" class="form-control" name="nominal" placeholder="10000" id="" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary waves-effect" type="submit">
                        Approve
                    </button>
                </div>
            </div>
    </div>
</div>

@endsection


@section('script')
@include('admin.master.batch.batch_js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
Helper.onlyNumberInput('.topups');
    $("#get_user").select2({
        ajax: {
            type: "GET",
            url: Helper.apiUrl('/user/select2'),
            data: function(params) {
                return {
                    q: params.term
                };
            },
            processResults: function(data) {
                var res = $.map(data.data, function(item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                });

                return {
                    results: res
                };
            }
        }
    });

    $('.radiogroup').change(function(e){
        var selectedValue = $(this).val();
        $('#amount').val(selectedValue)
    });

    // var table = $('#table-list-orders').DataTable({
    //     processing: true,
    //     serverSide: true,
    //     destroy: true,
    //     select: true,
    //     dom: 'Bflrtip',
    //     ordering:'true',
    //     order: [2, 'desc'],
    //     responsive: true,
    //     language: {
    //         search: '',
    //         searchPlaceholder: "Search..."
    //     },
    //     oLanguage: {
    //         sLengthMenu: "_MENU_",
    //     },
    //     dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
    //         "<'row'<'col-sm-12'tr>>" +
    //         "<'row'<'col-sm-6'i><'col-sm-6'p>>",
    //     ajax: {
    //         url: Helper.apiUrl('/admin/topup-wallet/datatables'),
    //         type: "get",

    //     },
    //     columns: [
    //         {
    //             data: "DT_RowIndex",
    //             name: "DT_RowIndex",
    //             orderable: false,
    //             searchable: false,
    //             width: "10%"
    //         },
    //         {
    //             data: "nominal",
    //             name: "nominal",
    //             render: $.fn.dataTable.render.number( ',', '.,', 0, 'Rp. ', '.00' )
    //         },
    //         // {
    //         //     data: "note",
    //         //     name: "note",
    //         // },
    //         {
    //             data: "topup_date",
    //             name: "topup_date",
    //             render: function(data, type, full){
    //                 return moment(full.topup_date).format("DD MMMM YYYY");
    //             }
    //         },
    //         {
    //             data: "id",
    //             name: "id",
    //             orderable: false,
    //             searchable: false,
    //             render: function(data, type, full) {
    //                 if (full.status_transfer_id === null) {
    //                     return "<span class='badge badge-danger btn-sm ' style='color:white'><i class='fa fa-spinner'></i> Pending</span>";
    //                 } else if (full.status_transfer_id === 0) {
    //                     return "<span class='badge badge-primary btn-sm ' style='color:white'><i class='fa fa-spinner'></i> Processing</span>";
    //                 }else if (full.status_transfer_id === '1') {
    //                     return "<span class='badge badge-success btn-sm ' style='color:white'><i class='fa fa-check'></i> Success</span> ";
    //                 }
    //             }
    //         },
    //         {
    //             data: "id",
    //             name: "id",
    //             orderable: false,
    //             searchable: false,
    //             render: function(data, type, full) {
    //                 if (full.status_transfer_id === null) {
    //                     return "<span class='badge badge-danger btn-sm ' style='color:white'><i class='fa fa-spinner'></i> Waiting Checkout Customer</span>"
    //                 } else if (full.status_transfer_id === 0) {
    //                     return "<a href='/admin/topup/detail/"+full.id+"' type='button' class='badge badge-primary btn-sm'  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>&nbsp;<a href='' class='badge badge-success btn-sm' data-toggle='tooltip' data-html='true' title='Topup Success'><i class='fa fa-check'></i></a>"
    //                 }else if (full.status_transfer_id === '1') {
    //                     return "<a href='/admin/topup/detail/"+full.id+"' type='button' class='badge badge-primary btn-sm'  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>";
    //                 }
                    
    //             }
    //         }
    //     ]
    // });

    $('#form-topup').submit(function(e){
        data = Helper.serializeForm($(this));

        Helper.loadingStart();
         // post data
        Axios.post(Helper.apiUrl('/admin/topup-wallet/save'), data)
            .then(function(response) {
                Helper.successNotif('Success, Top up Success, Pleace Confirmation your Payment');
                window.location.href = Helper.redirectUrl('/admin/topup/show-list');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

      e.preventDefault();
    })
</script>

@endsection
