@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                <h4>#{{ $tiket->ticket_no }}</h4>
            </div>
        </div>
        <div class="card-body">
            <p>{{ $tiket->created_at->format('d F Y H:i:s') }}</p>
            <h4>{{ $tiket->subject }}</h4>
            <br />
            <div class="user-profile">
                <img class="avatar" src="{{ $tiket->user->avatar }}" alt="Ash" />
                <div class="username">{{ $tiket->user->full_name }}</div>
                <div class="bio">
                    <p>{{ $tiket->user->email }}</p>                                 
                    <p>{{ $tiket->user->phone }}</p>                                 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-8" style="margin-bottom: 20px">
    <div class="card header-border">
        <div class="card-body">
            <div class="messaging">
                <div class="inbox_msg">
                    <div class="mesgs">
                        <div class="msg_history scroll-area-sm">
                            <?php $before = null; ?>
                            @foreach ($tiket->ticket_details as $detail)
                                @if ($auth->id == $detail->user_id)
                                <?php $before = $detail->user_id; ?>
                                <div class="outgoing_msg">
                                    <div class="sent_msg">                                        
                                        @if ($detail->attachment != null)
                                        <img alt="" class="img-responsive" src="{{ asset('/storage/attachment/' . $detail->attachment) }}" />
                                        @endif
                                        <p>{{ $detail->message }}</p>
                                        @if ($detail->order != null)
                                            <div class="card widget-content">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">{{ $detail->order->code }}</div>
                                                        <div class="widget-subheading">{{ $detail->order->symptom->name }}</div>
                                                        <div class="widget-subheading">{{ $detail->order->schedule->format('d F Y H:i:s') }}</div>
                                                    </div>
                                                    <div class="widget-content-right">
                                                        <div class="widget-numbers text-success"><a href="{{ $detail->order->url_detail }}"><span><i class="fa fa-arrow-right"></i></span></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <span class="time_date">{{ $detail->created_at->format('H:i | M j') }}</span>
                                    </div>
                                </div>
                                @endif

                                @if ($auth->id != $detail->user_id)
                                <div class="incoming_msg">
                                    <div class="incoming_msg_img">
                                        @if ($before != $detail->user_id)
                                        <?php $before = $detail->user_id; ?>
                                            <img alt="img-profile" class="rounded-circle" src="{{ $detail->user->avatar }}" />
                                        @endif
                                    </div>
                                    <div class="received_msg">
                                        <div class="received_withd_msg">                                            
                                            @if ($detail->attachment != null)
                                            <img alt="" class="img-responsive" src="{{ asset('/storage/attachment/' . $detail->attachment) }}" />
                                            @endif
                                            <p>{{ $detail->message }}</p>
                                            @if ($detail->order != null)
                                                <div class="card widget-content">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">{{ $detail->order->code }}</div>
                                                            <div class="widget-subheading">{{ $detail->order->symptom->name }}</div>
                                                            <div class="widget-subheading">{{ $detail->order->schedule->format('d F Y H:i:s') }}</div>
                                                        </div>
                                                        <div class="widget-content-right">
                                                            <div class="widget-numbers text-success"><a href="{{ $detail->order->url_detail }}"><span><i class="fa fa-arrow-right"></i></span></a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            <span class="time_date">{{ $detail->created_at->format('H:i | M j') }}</span>
                                        </div>
                                    </div>
                                </div>
                                @endif                               

                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-4" style="margin-bottom: 20px">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 20px">
            <div class="card">
                <div class="header-border card-header">
                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                        <h4>Ticket Status</h4>
                    </div>
                    <div class="btn-actions-pane-right text-capitalize">
                        {!! $tiket->status_html !!}
                    </div>
                </div>
                <div class="card-body">
                    <div class="tiket-status-option">
                        @if($tiket->status != 1)
                            <span class="status-opt btn btn-sm btn-primary" sid="{{ $tiket->id }}" status="1"><i class="fa fa-envelope-open"></i> Open Ticket</span>
                        @else
                            <span class="status-opt btn btn-sm btn-danger" sid="{{ $tiket->id }}" status="5"><i class="fa fa-close"></i> Close Ticket</span>
                        @endif                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom: 20px">
            <div class="card header-border">
                <div class="card-body">
                    @if ($tiket->status != '5')
                    <form id="form-tiket">
                        @if (Auth::user()->hasRole('admin') && $tiket->order == null)                            
                            <a href='/admin/transactions/create/{{ $tiket->id }}' class='btn btn-search btn-sm btn-dark'>Create Order <i class='fa fa-arrow-right'></i></a>
                            <hr />
                        @endif
                        <input type="hidden" name="ms_ticket_id" value="{{ $tiket->id }}" />
                        <div class="position-relative form-group">
                            <textarea id="message" class="form-control" cols="5" name="message" placeholder="Message" rows="5" type="text" required></textarea>
                        </div>
                        <div class="position-relative form-group">
                            <label for="">Attachment</label>
                            <br>
                            <input type="file" name="attachment" id="attachment" />
                        </div>
                        <br>
                        
                        <button class="btn btn-success btn-sm" type="submit">
                            REPLY
                        </button>
                        </br>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .img-responsive{
        width: 100%;
    }
    .scroll-area-sm {
        height: 500px;
        margin-top: 10px;
        margin-bottom: 10px;
        padding: 10px;
    }

    .incoming_msg_img {
        display: inline-block;
        width: 6%;
    }

    .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 92%;
    }

    .received_withd_msg p {
        box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 3px;
        color: #646464;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 12px;
        width: 100%;
    }

    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 4px 0 0;
    }

    .received_withd_msg {
        width: 57%;
    }

    .mesgs {
        float: left;
        padding: 30px 15px 0 25px;
        width: 100%;
    }

    .sent_msg p {
        background: #05728f none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        margin: 0;
        color: #fff;
        padding: 5px 10px 5px 12px;
        width: 100%;
    }

    .outgoing_msg {
        overflow: hidden;
        margin: 26px 0 26px;
    }

    .sent_msg {
        float: right;
        width: 46%;
        border: 1px solid #e4e7e8;
        padding: 3px;
    }

    .input_msg_write input {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 48px;
        width: 100%;
    }

    .messaging {
        padding: 0 0 50px 0;
    }

    .msg_history {}
</style>
<style>
    .user-profile {
        box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
        margin: auto;
        height: 8em;
        background: #fff;
        border-radius: .3em;
        margin-bottom: 20px;
    }

    .user-profile .username {
        margin: auto;
        margin-top: -4.40em;
        margin-left: 5.80em;
        color: #658585;
        font-size: 1.53em;
        font-family: "Coda", sans-serif;
        font-weight: bold;
    }

    .user-profile .bio {
        margin: auto;
        display: inline-block;
        margin-left: 10.43em;
        margin-bottom: 10px;
        color: #e76043;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }

    .user-profile>.description {
        margin: auto;
        margin-top: 1.35em;
        margin-right: 4.43em;
        width: 14em;
        color: #c0c5c5;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }
     .user-profile>img.avatar {
        padding: .7em;
        margin-left: .3em;
        margin-top: .3em;
        height: 6.23em;
        width: 6.23em;
        border-radius: 18em;
    }
</style>
@endsection

@section('script')
<script>
    $(".msg_history").animate({ scrollTop: $('.msg_history')[0].scrollHeight }, 1000);

    $('#form-tiket').submit(function(e) {
        data = Helper.serializeForm($(this));

        var formData = new FormData();
        var attachment = document.querySelector('#attachment');
        if (attachment.files[0]) {
            formData.append("attachment", attachment.files[0]);
        }
        formData.append("message", data.message);
        formData.append("ms_ticket_id", data.ms_ticket_id);

        Helper.loadingStart();
        // post data
        Axios.post(Helper.apiUrl('/customer/tickets/reply'), formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                Helper.loadingStop();
                // console.log(response.data)
                // Helper.successNotif('Success, ok');
                $('#message').val('');
                $('.msg_history').append(response.data.data);
                $(".msg_history").animate({ scrollTop: $('.msg_history')[0].scrollHeight }, 1000);
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })

    $('.status-opt').click(function() {
        status = $(this).attr('status');
        id = $(this).attr('sid');

        Helper.loadingStart();
        // post data
        Axios.put(Helper.apiUrl('/tickets/' + id + '/status'), {
                status
            })
            .then(function(response) {
                Helper.successNotif('Success, ok');
                $('.tiket-status').html(response.data.data);
                location.reload();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
@endsection