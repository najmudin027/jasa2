@extends('admin.home')
@section('content')
<style>
    div.hr-or {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    div.hr-or:before {
        content: 'OR';
        background-color: #fff;
    }

    div.hr {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    .hr-title {
        background-color: #fff;
    }
</style>

<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header">
            {{ $title }}
            <div class="btn-actions-pane-right">

            </div>
        </div>
        <div class="card-body">
            <br><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="firstName" class="">First Name</label>
                                            <input type="text" id="firstName" class="form-control" name="" value="{{ $getDataUser->info->first_name }}" readonly >
                                        </div>
                                        <div class="col-md-6">
                                            <label for="firstName" class="">Last Name</label>
                                            <input type="text" id="lastname" class="form-control" name="" value="{{ $getDataUser->info->last_name }}" readonly >
                                        </div>
                                    </div>

                                    <label for="firstName" class="">Email</label>
                                    <input type="email" id="lastname" class="form-control" name="" value="{{ $getDataUser->email }}" readonly >

                                    <label for="address" class="">Address</label>
                                    <textarea class="form-control" id="address" rows="3" name="" readonly>asdad{{ $getDataUser->info->address }}</textarea>

                                    <div class="row">

                                        <div class="col-md-6">
                                            <label for="firstName" class="">City</label>
                                            <input type="text" id="lastname" class="form-control" name="" readonly value="{{ $getDataUser->address->city->name }}">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="firstName" class="">Village</label>
                                            <input type="text" id="lastname" class="form-control" name="" readonly value="{{ $getDataUser->address->village->name }}">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="firstName" class="">Distric</label>
                                            <input type="text" id="firstName" class="form-control" name="" readonly value="{{ $getDataUser->address->district->name }}">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="firstName" class="">Zipp Code</label>
                                            @if (@empty($getDataUser->address->zipcode->zip_no))
                                                <input type="text" id="lastname" class="form-control" name="" readonly value="Not Found">
                                            @else
                                                <input type="text" id="lastname" class="form-control" name="" readonly value="{{ $getDataUser->address->zipcode->zip_no }}">
                                            @endif
                                            
                                        </div>
                                    </div><br>
                                    <div class='hr'>
                                        <span class='hr-title'><strong>Payment</strong></span>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="firstName" class="">Bank Name</label>
                                            <select name="ms_payment_methods_id" class="form-control js-example-basic-single" id="exampleFormControlSelect1">
                                                @foreach ($getBankName as $nameBank)
                                                        <option value="{{ $nameBank->id }}">{{ $nameBank->bank_name }} - {{ $nameBank->name }} - ({{ $nameBank->virtual_code }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {{-- <div class="col-md-6">
                                            <label for="firstName" class="">Virtual Code</label>
                                            <select id="city" name="ms_city_id" style="width:100%">

                                            </select>
                                        </div> --}}
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <h4 class="d-flex justify-content-between align-items-center mb-3">
                                <span class="text-muted">Your cart</span>
                                <span class="badge badge-secondary badge-pill"></span>
                            </h4>
                            <ul class="list-group mb-3 z-depth-1">
                                @php
                                    $total = 0
                                @endphp
                                @foreach($order->service_detail as $key => $detail)
                                    @php
                                        $harga = isset($detail->price) ? $detail->price  : 0;
                                        $total_harga = $harga * $detail->unit;
                                    @endphp
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">{{ $detail->services_type->name }}</h6>
                                            <small class="text-muted">qty : x{{ $detail->unit }}</small>
                                        </div>
                                        <span class="text-muted">Rp. {{ number_format($harga) }}</span>
                                    </li>
                                    @php $total += $total_harga @endphp
                                @endforeach
                                @foreach ($sparepart_details as $key => $items)
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">{{ $items->name_product }}</h6>
                                            <small class="text-muted">qty : x <b>{{ $items->quantity }}</b></small>
                                        </div>
                                        <span class="text-muted">Rp. {{ number_format($items->price) }}</span>
                                    </li>
                                    @php $total += $items->price @endphp
                                @endforeach
                                @foreach ($item_details as $key => $items)
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">{{ $items->name_product }}</h6>
                                            <small class="text-muted">qty : x <b>{{ $items->quantity }}</b></small>
                                        </div>
                                        <span class="text-muted">Rp. {{ number_format($items->price) }}</span>
                                    </li>
                                    @php $total += $items->price @endphp
                                @endforeach

                                <li class="list-group-item d-flex justify-content-between bg-light">
                                    <strong>Grand Total</strong>
                                    
                                    <strong>Rp. {{ number_format($total) }}</strong>
                                </li>
                                <li class="list-group-item d-flex justify-content-between bg-light">
                                    <strong>Discount (%)</strong>
                                    <strong>0%</strong>
                                </li>
                                {{-- <li class="list-group-item d-flex justify-content-between bg-light">
                                    <strong>PPn</strong>
                                    <strong>10% ({{ 10 / 100 * $harga}})</strong>
                                </li> --}}
                                <li class="list-group-item d-flex justify-content-between bg-light">
                                    <strong>Total</strong>
                                    <strong>Rp. {{ number_format($total) }}</strong>
                                </li>
                            </ul>
                            <div class="card p-2 bg-light">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="" id="" placeholder="Reedem Your Voucher">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-secondary">Reedem</button>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="grand_total" value="{{ $total }}">
                            <input type="hidden" name="ppn" value="0">
                            <input type="hidden" name="discount" value="0">
                            <input type="hidden" name="ppn_nominal" value="0">
                            <input type="hidden" name="discount_nominal" value="0">
                            <input type="hidden" name="id" value="{{ $order->id }}">
                        </div>
                        <div class="col-md-7">
                            <button type="submit" class="btn btn-primary btn-lg btn-block" id="save">Continue To Checkout</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')

<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });

    //untuk save all data
    $("#save").click(function() {
            var fd = new FormData();
            var id = $('input[name="id"]').val();
            // alert(id);
            fd.append('ms_payment_methods_id', $('select[name="ms_payment_methods_id"]').val());
            fd.append('grand_total', $('input[name="grand_total"]').val());
            fd.append('ppn', $('input[name="ppn"]').val());
            fd.append('discount', $('input[name="discount"]').val());
            fd.append('ppn_nominal', $('input[name="ppn_nominal"]').val());
            fd.append('discount_nominal', $('input[name="discount_nominal"]').val());

            console.log(fd)
            $.ajax({
                url:Helper.apiUrl('/customer/checkout/' + id),
                data: fd,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        iziToast.success({
                            title: 'OK',
                            position: 'topRight',
                            message: 'Data Has Been Saved',
                        });
                        window.location = Helper.url('/customer/order_detail/' + id);
                    }
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                        })
                    }
                },
            });
        });
</script>

@endsection
