<style>
  @import url(http://fonts.googleapis.com/css?family=Calibri:400,300,700);

  body {
      background-color: blue;
      font-family: 'Calibri', sans-serif !important
  }

  .mt-100 {
      margin-top: 50px
  }

  .mb-100 {
      margin-bottom: 50px
  }

  .card {
      border-radius: 1px !important
  }

  .card-header {
      background-color: #fff
  }

  .card-header:first-child {
      border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0
  }

  .btn-sm,
  .btn-group-sm>.btn {
      padding: .25rem .5rem;
      font-size: .765625rem;
      line-height: 1.5;
      border-radius: .2rem
  }
</style>
<title>Invoice #{{ $order->code }}</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<div class="container-fluid mt-100 mb-100">
    <input type="button" onclick="printDiv('printableArea')" class="btn btn-success" value="Print Invoice" />
</div>
<div class="container-fluid mt-100 mb-100" id="printableArea">
    <div id="ui-view">
        <div>
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                <img alt="I'm an image" border="0" class="left autowidth" src="http://astechindo.com/an-component/media/upload-gambar-pendukung/AstLogo.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 150px; display: block;" title="I'm an image" width="150"/>
                                </div>
                                <div class="col-md-6" align="right">
                                    <h4>Invoice<strong>&nbsp; #{{ $order->code }}</strong></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-sm-4">
                            <h6 class="mb-3">From:</h6>
                            <div><strong>PT ASTECH</strong></div>
                            <div>Email: ASTech@email.com</div>
                            <div>Phone: (021) 54367231</div>
                            <div>3rd Floor WISMA SSK BUILDING, <br> Daan Mogot Rd No.Km. 11, RT.5/RW.4,<br> Kedaung Kali Angke, Cengkareng, <br> West Jakarta City, Jakarta 11710</div>
                        </div>

                        <div class="col-sm-4" >
                            <h6 class="mb-3">To:</h6>
                            <div><strong>{{ $order->user->name }}</strong></div>
                            <div>Email: {{ $order->user->email }}</div>
                            <div>Phone: {{ $order->user->phone }}</div>
                            <div>{{ $order->address }}</div>
                        </div>

                        <div class="col-sm-4">
                        <div class="mb-2 ml-auto"> <span class="text-muted">Payment Details:</span>
                            <div class="d-flex flex-wrap wmin-md-400">
                                <ul class="list list-unstyled mb-0 text-left">
                                    <li>
                                        <h5 class="my-2">total bill:</h5>
                                    </li>
                                    <li>Payment Method :</li>
                                    <li>Description :</li>
                                </ul>
                                <ul class="list list-unstyled text-right mb-0 ml-auto">
                                    <li>
                                        <h5 class="font-weight-semibold my-2">Rp.{{ number_format($order->grand_total) }}</h5>
                                    </li>
                                    @if (empty($order->payment_method->name) || empty($order->payment_method->description))
                                        <li><span class="font-weight-semibold">-</span></li>
                                        <li><span class="font-weight-semibold">-</span></li>
                                    @else
                                        <li><span class="font-weight-semibold">{{ $order->payment_method->name }}</span></li>
                                        <li><span class="font-weight-semibold">{{ $order->payment_method->description }}</span></li>
                                    @endif

                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="30%">Name Service</th>
                                    <th width="30%">Symptom Name</th>
                                    <th width="10%" class="center">UNIT</th>
                                    <th width="15%" class="right">COST</th>
                                    <th width="15%" class="right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $total_price_service = 0
                                @endphp
                                @foreach($order->service_detail as $key => $service_detail)
                                    @php
                                        $harga_service = isset($service_detail->price) ? $service_detail->price * $service_detail->unit : 0
                                    @endphp
                                    <tr>
                                        @if ($service_detail->symptom_name !== null)
                                            <td class="left">{{ $service_detail->symptom_name }}</td>
                                            <td class="left">{{ $service_detail->service_type_name }}</td>
                                        @else
                                            <td class="left">{{ $service_detail->symptom->name }}</td>
                                            <td class="left">{{ $service_detail->services_type->name }}</td>
                                        @endif

                                        <td class="center"><b>{{ $service_detail->unit }}</b></td>
                                        <td class="right">{{ number_format($service_detail->price) }}</td>
                                        <td class="right">{{ number_format($harga_service) }}</td>
                                        @php $total_price_service += $harga_service @endphp
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="30%">Item</th>
                                    <th width="30%">Description</th>
                                    <th width="10%" class="center">QTY</th>
                                    <th width="15%" class="right">COST</th>
                                    <th width="15%" class="right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $total_price = 0
                                @endphp
                                    @foreach($order->sparepart_detail as $key => $sparepart)
                                        @php
                                            $harga_sparepart = isset($sparepart->price) ? $sparepart->price * $sparepart->quantity : 0
                                        @endphp
                                        <tr>
                                            <td class="left">{{ $sparepart->name_sparepart }}</td>
                                            @if(empty($sparepart->technician_sparepart->note))
                                                <td class="left">-</td>
                                            @else
                                                <td class="left">{{ $sparepart->technician_sparepart->note }}</td>
                                            @endif
                                            <td class="center"><b>{{ $sparepart->quantity }}</b></td>
                                            <td class="right">{{ number_format($sparepart->price) }}</td>
                                            <td class="right">{{ number_format($harga_sparepart) }}</td>
                                            @php $total_price += $harga_sparepart @endphp
                                        </tr>
                                    @endforeach
                                @foreach($item_details as $key => $item) <!-- part form product inventory -->
                                    @php
                                        $harga_item = isset($item->price) ? $item->price * $item->quantity : 0
                                    @endphp
                                    <tr>
                                        <td class="left">{{ $item->name_product }}</td>
                                        @if(empty($item->technician_sparepart->description))
                                            <td class="left">-</td>
                                        @else
                                            <td class="left">{{ $item->technician_sparepart->description }}</td>
                                        @endif
                                        <td class="center"><b>{{ $item->quantity }}</b></td>
                                        <td class="right">{{ number_format($item->price) }}</td>
                                        <td class="right">{{ number_format($harga_item) }}</td>
                                    </tr>
                                    @php $total_price += $harga_item; $totalPart = $total_price + $order->grand_total; @endphp
                                @endforeach
                                <!-- @if($total_price == 0)
                                    <h5>No Extra Part Needed.</h5>
                                @endif -->

                            </tbody>
                        </table>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-sm-5 mb-1">
                            <table>
                                <tr>
                                    <td>Note :</td>
                                </tr>
                                <tr>
                                    <td>{{ $order->note }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-4 col-sm-5 ml-auto">
                            <table class="table table-clear">
                                <tbody>
                                    <tr>
                                        <td class="left"><strong>Total</strong></td>
                                            <td class="right">{{ number_format($order->grand_total) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="left"><strong>Discount (%)</strong></td>
                                        <td class="right">0%</td>
                                    </tr>
                                    <tr>
                                        <td class="left"><strong>PPN (10%)</strong></td>
                                        <td class="right">0</td>
                                    </tr>
                                    <tr>
                                        <td class="left"><strong>Grand Total</strong></td>
                                            <td class="right">{{ number_format($order->grand_total) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- <div class="pull-right"> <a class="btn btn-sm btn-success" href="#" data-abc="true"><i class="fa fa-paper-plane mr-1"></i> Proceed to payment</a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <script>
     function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
 </script>
</body>
</html>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
