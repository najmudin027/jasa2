@extends('admin.home')
@section('content')
<style>
    div.hr-or {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    div.hr-or:before {
        content: 'OR';
        background-color: #fff;
    }

    div.hr {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    .hr-title {
        background-color: #fff;
    }
</style>

<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header">
            Order Received
            <div class="btn-actions-pane-right">
                <a href="{{ url('/customer/request_list') }}" class="btn btn-primary btn-sm add-bank-transfer">Back</a>
            </div>
        </div>
        <div class="card-body">
            <div class="row">

                <div class="col-md-12">
                    <p>thank you, your Order Has been Received</p>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="">Order Number</label>
                            <input type="text" class="form-control" value="{{ $order->code }}" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="">Order Date</label>
                            <input type="text" class="form-control" value="{{ date('M d Y H:i:s') }}" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="">Email</label>
                            <input type="text" class="form-control" value="{{ $getDataUser->email }}" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="">Total</label>
                            <input type="text" class="form-control" value="Rp. {{ $order->grand_total + $order->ppn_nominal }}" readonly>
                        </div>
                    </div> <br>
                    <table class="table table-striped">
                        <thead>
                            <th>Bank</th>
                            <th>Rekening</th>
                            <th>A.N</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td><img src="{{ asset('/customer/storage/bankIcon/' . $order->bank->icon) }}" alt="asd" width="50"></td>
                                <td>{{ $order->bank->virtual_code }}</td>
                                <td>{{ $order->bank->name }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <br>
                    <div class="container" style="background-color: #dfe6e9; height:40px; line-height: 2.8">
                        <p align="center">Setelah pembayaran berhasil, harap melakukan konfirmasi pembayaran melalui link berikut. <a href="{{ url('/customer/payment/confirmation/' . $order->id) }}" style="text-decoration: underline"><strong> Konfirmasi Bayar </strong></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')

<script>
</script>

@endsection
