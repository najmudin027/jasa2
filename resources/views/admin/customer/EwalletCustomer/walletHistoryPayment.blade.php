@extends('admin.home')
@section('content')

<div class="col-md-12">
    <!-- <div class="mb-12 card">
        <div class="card-header">
                
            <div class="btn-actions-pane-right">
                
            </div>
        </div>
    </div><br> -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="header-bg card-header">
                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                        History Transactions
                    </div>
                </div>
                <div class="card-body">
                    <table id="table-transaction-history" class="table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>#</th>
                                <th>Order Code</th>
                                <th>Created At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    var table = $('#table-transaction-history').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        select: true,
        dom: 'Bflrtip',
        ordering:'true',
        order: [1, 'desc'],
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search..."
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/customer/transaction-history/datatables'),
            type: "get",

        },
        columns: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                sortable: false,
            },
            {
                data: "id",
                name: "id",
                "targets": [ 2 ],
                "visible": false
            },
            {
                data: "code",
                name: "code",
                render: function(data, type, full) {
                    return "<a href='" + Helper.url('/customer/detail-history-transactions/' + full.id) + "'># " + full.code + "</a>";
                }
            },
            {
                data: "created_at",
                name: "created_at",
                render: function(data, type, full) {
                    return full.created_at == null ? '-' : moment(full.created_at).format("DD MMMM YYYY hh:mm");
                }
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    detail = "<a href='/customer/detail-history-transactions/" + full.id + "' class='btn btn-search btn-sm btn-success'  data-toggle='tooltip' data-html='true' title='<b>Detail</b>'><i class='fa fa-eye'></i></a>";
                    return detail;
                }
            },
        ]
    });
</script>
@endsection