<div class="col-md-12">
    <!-- <div class="mb-12 card">
        <div class="card-header">
                
            <div class="btn-actions-pane-right">
                
            </div>
        </div>
    </div><br> -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5>History Payment</h5>
                    <hr>
                    <div id="accordion">
                        @foreach($countCustomerTopup as $show)
                            @if (!empty($show->getproduct))
                                @foreach ($show->getproduct->service_detail as $service)
                                    @php
                                        $total_harga = $service->price * $service->unit;
                                    @endphp
                                @endforeach
                            @endif
                            <div class="card">
                                <div class="card-header" id="heading{{ $show->id }}">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{ $show->id }}" aria-expanded="true" aria-controls="collapse{{ $show->id }}">
                                        @if ($show->orderhistory->type_transaction_id == 1)
                                            <strong>History Order {{ $show->getproduct->order_status->name }} &nbsp;&nbsp; - {{ date('d-M-Y H:i:s', strtotime($show->created_at)) }}</strong>
                                        @elseif ($show->orderhistory->type_transaction_id == 3)
                                            @php 
                                            if (!empty($show->getproduct->penalty_order_value)) {
                                                $count_penalty_percent = $show->getproduct->penalty_order_value / 100 *  $total_harga;
                                                $grand_total_penalty =  $count_penalty_percent;
                                            }else{
                                                $count_penalty_percent = 0 *  $total_harga;
                                                $grand_total_penalty =  $count_penalty_percent;
                                            }
                                                
                                            @endphp
                                            <strong style="color: red">
                                                ORDER CANCEL &nbsp;-&nbsp;  
                                                {{ date('d-M-Y H:i:s', strtotime($show->created_at)) }} &nbsp; - &nbsp;
                                                @if($show->getproduct->penalty_order_type == 1)
                                                    (-) Rp.{{ number_format($grand_total_penalty) }}
                                                @elseif($show->getproduct->penalty_order_type == 2)
                                                    (-) Rp.{{ number_format($show->getproduct->penalty_order_value) }}
                                                @else
                                                    (-) Rp.0.00
                                                @endif
                                            </strong>
                                        @endif
                                        
                                    </button>
                                    </h5>
                                </div>
                            
                                @if (!empty($show->getproduct))
                                <div id="collapse{{ $show->id }}" class="collapse" aria-labelledby="heading{{ $show->id }}" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleFormControlInput1">Order Code</label>
                                                            <input type="text" class="form-control" value="{{ $show->getproduct->code }}" readonly>
                                                            <input type="hidden" name="id" value="{{ $show->getproduct->id }}">
                                                        </div> 
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="exampleFormControlInput1">Order Status</label>
                                                            @if ($show->getproduct->orders_statuses_id == 2)
                                                                <input type="text" class="form-control" value="ANALYZING" readonly>
                                                            @elseif ($show->getproduct->orders_statuses_id == 3)
                                                                <input type="text" class="form-control" value="REQUEST APPROVED" readonly>
                                                            @elseif ($show->getproduct->orders_statuses_id == 4)
                                                                <input type="text" class="form-control" value="ON WORKING" readonly>
                                                            @elseif ($show->getproduct->orders_statuses_id == 5)
                                                                <input type="text" class="form-control" value="CANCEL" readonly>
                                                            @elseif ($show->getproduct->orders_statuses_id == 6)
                                                                <input type="text" class="form-control" value="RESCHEDULE" readonly>
                                                            @elseif ($show->getproduct->orders_statuses_id == 7)
                                                                <input type="text" class="form-control" value="JOB DONE" readonly>
                                                            @elseif ($show->getproduct->orders_statuses_id == 9)
                                                                <input type="text" class="form-control" value="PROCESSING" readonly>
                                                            @elseif ($show->getproduct->orders_statuses_id == 10)
                                                                <input type="text" class="form-control" value="JOBS COMPLETED" readonly>
                                                            @elseif ($show->getproduct->orders_statuses_id == 11)
                                                                <input type="text" class="form-control" value="COMPLAINT" readonly>
                                                            @endif
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        @if ($show->getproduct->is_reschedule == 0)
                                                        <div class="form-group">
                                                                <label for="exampleFormControlInput1">Schedule At</label>
                                                                <input type="text" class="form-control" value="{{ date('D-M-Y', strtotime($show->getproduct->schedule)) }}" readonly>
                                                            </div> 
                                                        @endif
                                                    </div>
                                                </div>
                                                <strong>SERVICE DETAIL</strong><hr>
                                                <table id="service-detail" class="display table table-hover table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Service Type</th>
                                                            <th>Qty</th>
                                                            <th>Price</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($show->getproduct->service_detail as $service)
                                                            @php
                                                                $total_harga = $service->price * $service->unit;
                                                                $penalty = $total_harga + $wallet->nominal - $show->orderhistory->nominal;
                                                            @endphp

                                                            <tr>
                                                                <td>{{ $service->services_type->name }}</td>
                                                                <td>{{ $service->unit }}</td>
                                                                <td>Rp.{{ number_format($total_harga)  }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody><hr>

                                                    @if(count($show->getproduct->sparepart_detail) > 0)
                                                        <tbody>
                                                            <th>Sparepart Name</th>
                                                            <th>Qty</th>
                                                            <th>Price</th>
                                                        </tbody>
                                                        <tbody>
                                                            @if(count($show->getproduct->sparepart_detail) > 0)
                                                                @php $total = 0 @endphp
                                                                @foreach($show->getproduct->sparepart_detail as $sparepart_detail)
                                                                    <tr>
                                                                        <td>{{ $sparepart_detail->name_sparepart }}</td>
                                                                        <td>{{ $sparepart_detail->quantity }}</td>
                                                                        <td>Rp. {{ $sparepart_detail->priceThousandSeparator() }}</td>
                                                                    </tr>
                                                                    @php $total += $sparepart_detail->price @endphp
                                                                @endforeach
                                                            @endif

                                                            @if(count($show->getproduct->item_detail) > 0)
                                                                @foreach($show->getproduct->item_detail as $item)
                                                                    <tr>
                                                                        <td>{{ $item->inventory->batch_item->product_name }}</td>
                                                                        <td>{{ $item->quantity }}</td>
                                                                        <td>Rp. {{ number_format($item->price) }}</td>
                                                                    </tr>
                                                                    @php $total += $item->price @endphp
                                                                @endforeach
                                                            @endif
                                                        </tbody>
                                                    @endif
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="2" align="right">Price Service</td>
                                                            <td>Rp.{{ number_format($show->getproduct->grand_total) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="right">Penalty</td>
                                                            @if($show->getproduct->penalty_order_type == 1)
                                                                @php 
                                                                    $count_penalty_percent = $show->getproduct->penalty_order_value / 100 *  $total_harga;
                                                                @endphp
                                                                <td>(-) <strong>{{$show->getproduct->penalty_order_value}}% (Rp. {{number_format($count_penalty_percent)}})</strong></td>
                                                            @elseif($show->getproduct->penalty_order_type == 2)
                                                                <td>(-) Rp.<strong>{{number_format($show->getproduct->penalty_order_value)}}</strong></td>
                                                            @else
                                                                <td>Rp.<strong>0.00</strong></td>
                                                            @endif
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="right">Grand Total received by the customer</td>
                                                            @if($show->getproduct->penalty_order_type == 1)
                                                                @php 
                                                                    $count_penalty_percent = $show->getproduct->penalty_order_value / 100 *  $total_harga;
                                                                    $grand_total_penalty = $total_harga - $count_penalty_percent ;
                                                                    $grand_total_penalty_fix = $total_harga - $show->getproduct->penalty_order_value;
                                                                @endphp
                                                                <td>Rp.{{number_format($grand_total_penalty)}}</td>
                                                            @elseif($show->getproduct->penalty_order_type == 2)
                                                                @php 
                                                                    $grand_total_penalty = $total_harga - $show->getproduct->penalty_order_value;
                                                                    if (!empty($total)) {
                                                                        $totalSparepart = $total + $grand_total_penalty;
                                                                    }else{
                                                                        $totalSparepart = 0 + $grand_total_penalty;
                                                                    }
                                                                @endphp
                                                                <td>Rp.{{number_format($totalSparepart)}}</td>
                                                            @else
                                                                <td>Rp.<strong>{{number_format($show->getproduct->grand_total)}}</strong></td>
                                                            @endif
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-1">Note :</div>
                                                            @if($show->getproduct->penalty_order_type == 1)
                                                                <div class="col-md-11">
                                                                    Saldo Awal anda sebesar <strong>Rp. {{ number_format($show->getproduct->wallet_customer) }}</strong>, 
                                                                    di potong penalty sebanyak <strong>{{ $show->getproduct->penalty_order_value }}%</strong> 
                                                                    atau sebesar <strong>Rp. {{ number_format($count_penalty_percent) }}</strong> dari besar nya
                                                                    nominal service, di karnakan anda sudah membatalkan service disaat teknisi 
                                                                    kami sudah menerima permintaan service anda.
                                                                </div>
                                                            @elseif($show->getproduct->penalty_order_type == 2)
                                                                <div class="col-md-11">
                                                                    Saldo Awal anda sebesar <strong>Rp. {{ number_format($show->getproduct->wallet_customer) }}</strong>,
                                                                     di potong penalty sebesar <strong>Rp.{{ number_format($show->getproduct->penalty_order_value) }}</strong> dari besar nya
                                                                    nominal service, di karnakan anda sudah membatalkan service disaat teknisi 
                                                                    kami sudah menerima permintaan service anda.
                                                                </div>
                                                            @else
                                                            <div class="col-md-11">
                                                                    Terimakasih Semoga anda puas dengan layanan kami.
                                                                </div>
                                                            @endif
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        @endforeach
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>