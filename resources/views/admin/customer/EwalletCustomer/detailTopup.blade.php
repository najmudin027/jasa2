@extends('admin.home')
@section('content')
<style>
    div.hr-or {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    div.hr-or:before {
        content: 'OR';
        background-color: #fff;
    }

    div.hr {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    .hr-title {
        background-color: #fff;
    }
</style>

<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header">
            {{ $title }}
            <div class="btn-actions-pane-right">

            </div>
        </div>
        <div class="card-body">
            <br><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="firstName" class="">First Name</label>
                                            <input type="text" id="firstName" class="form-control" name="" value="{{ !empty($getDataUser->info) ? $getDataUser->info->first_name :Auth::user()->name }}" readonly >
                                        </div>
                                        <div class="col-md-6">
                                            <label for="firstName" class="">Last Name</label>
                                            <input type="text" id="lastname" class="form-control" name="" value="{{ !empty($getDataUser->info) ?  $getDataUser->info->last_name : '-' }}" readonly >
                                        </div>
                                    </div><br>

                                    <label for="firstName" class="">Email</label>
                                    <input type="email" id="lastname" class="form-control" name="" value="{{ $getDataUser->email }}" readonly ><br>

                                    {{-- <label for="address" class="">Address</label>
                                    <textarea class="form-control" id="address" rows="3" name="" readonly>asdad{{ !empty($getDataUser->info) ?  $getDataUser->info->address : '' }}</textarea><br> --}}
{{-- 
                                    <div class="row">

                                        <div class="col-md-6">
                                            <label for="firstName" class="">City</label>
                                            <input type="text" id="lastname" class="form-control" name="" readonly value="{{ $getDataUser->address->city->name != null ? $getDataUser->address->city->name : 'Not Found' }}">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="firstName" class="">Village</label>
                                            @if (@empty($getDataUser->address->village->name))
                                                <input type="text" id="lastname" class="form-control" name="" readonly value="Not Found">
                                            @else
                                                <input type="text" id="lastname" class="form-control" name="" readonly value="{{ $getDataUser->address->village->name }}">
                                            @endif
                                        </div>
                                    </div><br> --}}

                                    {{-- <div class="row">
                                        <div class="col-md-6">
                                            <label for="firstName" class="">District</label>
                                            <input type="text" id="firstName" class="form-control" name="" readonly value="{{ $getDataUser->address->district->name != null ? $getDataUser->address->district->name : 'Not Found' }}">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="firstName" class="">Zip Code</label>
                                            @if (@empty($getDataUser->address->zipcode->zip_no))
                                                <input type="text" id="lastname" class="form-control" name="" readonly value="Not Found">
                                            @else
                                                <input type="text" id="lastname" class="form-control" name="" readonly value="{{ $getDataUser->address->zipcode->zip_no }}">
                                            @endif
                                            
                                        </div>
                                    </div><br> --}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5" id="charts">
                            <h4 class="d-flex justify-content-between align-items-center mb-3">
                                <span class="text-muted">Your cart</span>
                                <span class="badge badge-secondary badge-pill"></span>
                            </h4>
                            <ul class="list-group mb-3 z-depth-1">
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Top up E-wallet</h6>
                                            <!-- <small class="text-muted">qty : x2</small> -->
                                        </div>
                                        <span class="text-muted">Rp. {{ number_format($wallet->nominal) }}</span>
                                    </li>

                                <li class="list-group-item d-flex justify-content-between bg-light">
                                    <strong>Sub Total</strong>
                                    @php
                                        $total = 0;
                                        if(!empty($wallet->kode_uniq_bank)){
                                            $total = $wallet->nominal + $wallet->kode_uniq_bank;
                                        }else{
                                            $total = $wallet->nominal;
                                        }
                                        
                                    @endphp
                                    <strong>Rp.{{ number_format($total) }}</strong>
                                </li>
                                <li class="list-group-item d-flex justify-content-between bg-light">
                                    <strong>Discount</strong>
                                    @if(!empty($wallet->voucer))
                                        @if(!empty($wallet->voucer->id))
                                            @if($wallet->voucer->type == 1)
                                                <strong>- Rp.{{number_format($wallet->voucer->max_nominal)}}</strong>
                                            @else
                                                <strong>- Rp. {{number_format($wallet->voucer->value)}}</strong>
                                            @endif
                                        @else
                                            <strong>Rp.0-</strong>
                                        @endif
                                    @else
                                        <strong>Rp.0-</strong>
                                    @endif
                                    
                                </li>
                                <li class="list-group-item d-flex justify-content-between bg-light">
                                    <strong>Total Pay</strong>
                                    @if(!empty($wallet->voucer))
                                        @if(!empty($wallet->voucer->id))
                                            @if($wallet->voucer->type == 1)
                                                @php
                                                    $percent = $wallet->nominal - $wallet->voucer->max_nominal;
                                                @endphp
                                                <strong>Rp.{{number_format($percent)}}</strong>
                                            @else
                                                @php
                                                    $fix = $wallet->nominal - $wallet->voucer->value;
                                                @endphp
                                                <strong>Rp. {{number_format($fix)}}</strong>
                                            @endif
                                        @else
                                             <strong>Rp.{{ number_format($total) }}</strong>
                                        @endif
                                    @else
                                         <strong>Rp.{{ number_format($total) }}</strong>
                                    @endif
                                </li>
                            </ul>
                            <input type="hidden" name="id" id="id" value="{{ $wallet->id }}">
                            {{-- <div class="card p-2 bg-light">
                                <div class="input-group">
                                    @if (!empty($wallet->voucer))
                                        @if (!empty($wallet->voucer->id))
                                            <input type="text" class="form-control reedem_voucers" value="{{ $wallet->voucer->code }}" name="reedem_voucer" id="" placeholder="Reedem Your Voucher">
                                        @else
                                            <input type="text" class="form-control reedem_voucers" value="" name="reedem_voucer" id="" placeholder="Reedem Your Voucher">
                                        @endif
                                    @else
                                        <input type="text" class="form-control reedem_voucers" value="" name="reedem_voucer" id="" placeholder="Reedem Your Voucher">
                                    @endif
                                    <div class="input-group-append">
                                        <button type="submit" id="btn_clime_voucher" class="btn btn-secondary">Reedem</button>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        {{-- <div class="col-md-5">
                            <h4 class="d-flex justify-content-between align-items-center mb-3">
                                <span class="text-muted">Your cart</span>
                                <span class="badge badge-secondary badge-pill"></span>
                            </h4>
                            <ul class="list-group mb-3 z-depth-1">
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Top up E-wallet</h6>
                                            <small class="text-muted">qty : x2</small>
                                        </div>
                                        <span class="text-muted">Rp. {{ number_format($wallet->nominal) }}</span>
                                    </li>

                                <li class="list-group-item d-flex justify-content-between bg-light">
                                    <strong>Grand Total</strong>
                                    
                                    <strong>Rp.{{ number_format($wallet->nominal) }}</strong>
                                </li>
                                <li class="list-group-item d-flex justify-content-between bg-light">
                                    <strong>Discount (%)</strong>
                                    <strong>0%</strong>
                                </li>
                                <li class="list-group-item d-flex justify-content-between bg-light">
                                    <strong>Total</strong>
                                    <strong>Rp.{{ number_format($wallet->nominal) }}</strong>
                                </li>
                            </ul>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')

@endsection
