<div class="outgoing_msg">
    <div class="sent_msg">
        <p>{{ $reply_complaint->message }}</p>
        @if ($reply_complaint->attachment != null)
            <img alt="" class="img-responsive" src="{{ asset('customer/storage/attachment/complaint/' . $reply_complaint->attachment) }}" />
        @endif
       
        <span class="time_date">{{ $reply_complaint->created_at->format('H:i | M j') }} 
    </div>
</div>