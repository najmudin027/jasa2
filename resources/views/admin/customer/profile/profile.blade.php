@extends('admin.home')
@section('content')
<input type="hidden" id="hidden-user-id" value="{{ $user->id }}">
<div class="col-md-3 profile-nav">
    <div class="panel">
          <div class="user-heading round">
            <div class="profile-userpic text-center">
                <div class="avatar-upload">
                    <div class="avatar-edit">
                        <input type='file' id="imageUpload"/>
                        <label for="imageUpload"></label>
                    </div>
                    <div class="avatar-preview">
                        <div id="imagePreview" style="background-image: url( {{ Auth::user()->avatar }} )"></div>
                    </div>
                </div>
            </div>
                <br>
              <h1>{{ $user->full_name }}</h1>
              <p>{{ $user->email }}</p>
          </div>
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="#"><i class="fa fa-key"></i> Role</a>
            </li>
            @foreach ($user->roles as $role)                
                <li class="nav-item">
                    <a class="nav-link" href="#"> <label class="label-danger">{{ $role->name }} </label></a>
                </li>
            @endforeach            
        </ul>
      </div>
</div>
<div class="col-md-9 profile-info">
        <div class="panel">
          <div class="bio-graph-heading">
            Bio Graph
          </div>
          <div class="panel-body bio-graph-info">
              <div class="row">
                  <div class="bio-row">
                      <p><span>First Name </span>: {{ $user->info == null ? '' : $user->info->first_name }}</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Last Name </span>: {{ $user->info == null ? '' : $user->info->last_name }}</p>
                  </div>
                  <div class="bio-row">
                  <p><span>Gender </span>: {{ $user->info == null ? '' : $user->info->sex }}</p>
                  </div>
                  <div class="bio-row">
                        <p><span>Birthday</span>: 
                            {{ $user->info == null ? '' : $user->info->place_of_birth }}
                             @if($user->info != null)
                                @if($user->info->date_of_birth != null)
                                    {{ $user->info->date_of_birth->format('d F Y') }}
                                @endif
                             @endif
                        </p>
                  </div>
                  <div class="bio-row">
                      <p><span>Religion </span>: @if ($user->info != null)
                                @if ($user->info->religion != null)
                                    {{ $user->info->religion->name }}
                                @endif
                            @endif</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Email </span>: {{ $user->email }}</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Marital </span>: @if ($user->info != null)
                                @if ($user->info->marital != null)
                                    {{ $user->info->marital->status }}
                                @endif
                            @endif</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Phone </span>:  {{ $user->phone }}</p>
                  </div>
              </div>
              <a href="{{ url('/customer/profile/info/edit') }}" class="btn btn-success btn-sm" ><i class="fa fa-pencil"></i> Edit Info</a>
          </div>
      </div>
</div>

<div class="col-md-12 profile-info">
        <div class="panel">
          <div class="bio-graph-heading">
          ADDRESS USER
          </div>
          <div class="panel-body bio-graph-info">
            <table id="address-table" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th style="width:70%">Full Address</th>
                            <th>Primary</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>

                <a href="{{ url('/customer/profile/address/create') }}" class="mb-2 mr-2 btn btn-primary">
                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add New Address
                </a>
          </div>
      </div>
</div>


{{-- modal phone number --}}
<form id="form-phone-number">
    <div class="modal fade" data-backdrop="false" id="modal-phone-number" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        ADD PHONE NUMBER
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="position-relative form-group" id="phone_number_modal_input_area">
                        <input name="phone_number" id="phone_number" type="text" value="{{ $user->phone }}" placeholder="phone number" class="form-control" required />
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SUBMIT
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        CLOSE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

{{-- modal otp --}}
<form id="form-phone-number" action="{{ route('otp.verification') }}" method="post">
    @csrf
    <div class="modal fade" data-backdrop="false" id="modal-otp-token" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #343a40;">
                    <h4 class="modal-title">
                        VERTIVIKASI CODE
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="position-relative form-group" id="token_modal_input_area">
                        <input name="token_otp" id="token_otp_input" type="text" placeholder="token" class="form-control"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SUBMIT
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        CLOSE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection


@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
    // agar user profile bisa diclik
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    // Helper.hideSidebar();

    var dt = globalCRUD.datatables({
        url: '/customer/profile/address_list/datatables',
        selector: '#address-table',
        columnsField: [
          'DT_RowIndex',
          {
              data:'id',
              name:'id',
              render: function(data, type, row){
                  return row.address;
                  // return row.village.name + ' ' + row.district.name + ' ' + row.city.name;
              }
          },
          {
              data:'id',
              name:'id',
              render: function(data, type, row){
                  if (row.is_main == 0) {
                    return "<button type='button' class='btn btn-main-address btn-drak' data-id='"+row.id+"'><i style='font-size: 24px;' class='fa fa-square'></i></button>";
                  }else{
                    return "<button type='button' class='btn btn-drak' data-id='"+row.id+"'><i class='fa fa-check-square' style='font-size: 24px;'></i></button>";
                  }
              }
          }
        ],
        scrollX: true,
        actionLink: {
            only_icon: false,
            update: function(row) {
                return '/customer/profile/address/'+row.id+'/edit';
            },
            delete: function(row) {
                return '/customer/profile/address/'+row.id;
            }
        }
    })

    // set main address
    $(document)
        .on('click', '.btn-main-address', function(){
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.put('/customer/profile/address/main/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif(response.data.msg);
                        // reload table
                        dt.table.reloadTable();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })

    function readURL(input) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }

    $("#imageUpload").change(function() {
        input = this;

        if (input.files && input.files[0]) {
            Helper.loadingStart();
            user_id = $("#hidden-user-id").val();
            var formData = new FormData();
            formData.append("image", input.files[0]);

            console.log(formData);
            Axios
                .post(Helper.apiUrl('/customer/profile/image'), formData, {
                    headers: {
                      'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    Helper.successNotif(response.data.msg);
                    readURL(input);
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        }
    });

    $(document)
        .on('click', '.add-phone-number', function(){
            $('#modal-phone-number').modal('show')
        })

    $(document)
        .on('click', '.add-verify-phone', function(){
            $('#modal-otp-token').modal('show')
        })

    // update OTP
    $('#form-phone-number').submit(function(e){
        Helper.confirm(function(){
            Helper.loadingStart();
            Axios
                .post(Helper.apiUrl('/user/otp/send'), {
                    phone : $('#phone_number').val(),
                })
                .then(function(response) {
                    Helper.successNotif(response.data.msg);
                    $('#modal-phone-number').modal('hide')
                    $('#modal-otp-token').modal('show')
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        })

        e.preventDefault()
    });

</script>
@endsection



<style>
    .panel {
        margin-bottom: 20px;
        background-color: #fff;
        border: 1px solid transparent;
        border-radius: 4px;
        -webkit-box-shadow: 0 1px 1px rgb(0 0 0 / 5%);
        box-shadow: 0 1px 1px rgb(0 0 0 / 5%);
    }

    .panel-body {
        padding: 15px;
    }

    .profile-nav, .profile-info{
        margin-top:30px;   
    }

    .profile-nav .user-heading {
        background: #444054;
        color: #fff;
        border-radius: 4px 4px 0 0;
        -webkit-border-radius: 4px 4px 0 0;
        padding: 30px;
        text-align: center;
    }

    .profile-nav .user-heading.round a  {
        border-radius: 50%;
        -webkit-border-radius: 50%;
        border: 10px solid rgba(255,255,255,0.3);
        display: inline-block;
    }

    .profile-nav .user-heading a img {
        width: 112px;
        height: 112px;
        border-radius: 50%;
        -webkit-border-radius: 50%;
    }

    .profile-nav .user-heading h1 {
        font-size: 22px;
        font-weight: 300;
        margin-bottom: 5px;
    }

    .profile-nav .user-heading p {
        font-size: 12px;
    }

    .profile-nav ul {
        margin-top: 1px;
    }

    .profile-nav ul > li {
        border-bottom: 1px solid #ebeae6;
        margin-top: 0;
        line-height: 30px;
    }

    .profile-nav ul > li:last-child {
        border-bottom: none;
    }

    .profile-nav ul > li > a {
        border-radius: 0;
        -webkit-border-radius: 0;
        color: #89817f;
        border-left: 5px solid #fff;
    }

    .profile-nav ul > li > a:hover, .profile-nav ul > li > a:focus, .profile-nav ul li.active  a {
        background: #f8f7f5 !important;
        border-left: 5px solid #fbc02d;
        color: #89817f !important;
    }

    .profile-nav ul > li:last-child > a:last-child {
        border-radius: 0 0 4px 4px;
        -webkit-border-radius: 0 0 4px 4px;
    }

    .profile-nav ul > li > a > i{
        font-size: 16px;
        padding-right: 10px;
        color: #bcb3aa;
    }

    .r-activity {
        margin: 6px 0 0;
        font-size: 12px;
    }


    .p-text-area, .p-text-area:focus {
        border: none;
        font-weight: 300;
        box-shadow: none;
        color: #c3c3c3;
        font-size: 16px;
    }

    .profile-info .panel-footer {
        background-color:#f8f7f5 ;
        border-top: 1px solid #e7ebee;
    }

    .profile-info .panel-footer ul li a {
        color: #7a7a7a;
    }

    .bio-graph-heading {
        background: #444054;
        color: #fff;
        text-align: center;
        font-style: italic;
        padding: 40px 110px;
        border-radius: 4px 4px 0 0;
        -webkit-border-radius: 4px 4px 0 0;
        font-size: 16px;
        font-weight: 300;
    }

    .bio-graph-info {
        color: #89817e;
    }

    .bio-graph-info h1 {
        font-size: 22px;
        font-weight: 300;
        margin: 0 0 20px;
    }

    .bio-row {
        width: 50%;
        float: left;
        margin-bottom: 10px;
        padding:0 15px;
    }

    .bio-row p span {
        width: 100px;
        display: inline-block;
    }

    .bio-chart, .bio-desk {
        float: left;
    }

    .bio-chart {
        width: 40%;
    }

    .bio-desk {
        width: 60%;
    }

    .bio-desk h4 {
        font-size: 15px;
        font-weight:400;
    }

    .bio-desk h4.terques {
        color: #4CC5CD;
    }

    .bio-desk h4.red {
        color: #e26b7f;
    }

    .bio-desk h4.green {
        color: #97be4b;
    }

    .bio-desk h4.purple {
        color: #caa3da;
    }

    .file-pos {
        margin: 6px 0 10px 0;
    }

    .profile-activity h5 {
        font-weight: 300;
        margin-top: 0;
        color: #c3c3c3;
    }

    .summary-head {
        background: #ee7272;
        color: #fff;
        text-align: center;
        border-bottom: 1px solid #ee7272;
    }

    .summary-head h4 {
        font-weight: 300;
        text-transform: uppercase;
        margin-bottom: 5px;
    }

    .summary-head p {
        color: rgba(255,255,255,0.6);
    }

    ul.summary-list {
        display: inline-block;
        padding-left:0 ;
        width: 100%;
        margin-bottom: 0;
    }

    ul.summary-list > li {
        display: inline-block;
        width: 19.5%;
        text-align: center;
    }

    ul.summary-list > li > a > i {
        display:block;
        font-size: 18px;
        padding-bottom: 5px;
    }

    ul.summary-list > li > a {
        padding: 10px 0;
        display: inline-block;
        color: #818181;
    }

    ul.summary-list > li  {
        border-right: 1px solid #eaeaea;
    }

    ul.summary-list > li:last-child  {
        border-right: none;
    }

    .activity {
        width: 100%;
        float: left;
        margin-bottom: 10px;
    }

    .activity.alt {
        width: 100%;
        float: right;
        margin-bottom: 10px;
    }

    .activity span {
        float: left;
    }

    .activity.alt span {
        float: right;
    }
    .activity span, .activity.alt span {
        width: 45px;
        height: 45px;
        line-height: 45px;
        border-radius: 50%;
        -webkit-border-radius: 50%;
        background: #eee;
        text-align: center;
        color: #fff;
        font-size: 16px;
    }

    .activity.terques span {
        background: #8dd7d6;
    }

    .activity.terques h4 {
        color: #8dd7d6;
    }
    .activity.purple span {
        background: #b984dc;
    }

    .activity.purple h4 {
        color: #b984dc;
    }
    .activity.blue span {
        background: #90b4e6;
    }

    .activity.blue h4 {
        color: #90b4e6;
    }
    .activity.green span {
        background: #aec785;
    }

    .activity.green h4 {
        color: #aec785;
    }

    .activity h4 {
        margin-top:0 ;
        font-size: 16px;
    }

    .activity p {
        margin-bottom: 0;
        font-size: 13px;
    }

    .activity .activity-desk i, .activity.alt .activity-desk i {
        float: left;
        font-size: 18px;
        margin-right: 10px;
        color: #bebebe;
    }

    .activity .activity-desk {
        margin-left: 70px;
        position: relative;
    }

    .activity.alt .activity-desk {
        margin-right: 70px;
        position: relative;
    }

    .activity.alt .activity-desk .panel {
        float: right;
        position: relative;
    }

    .activity-desk .panel {
        background: #F4F4F4 ;
        display: inline-block;
    }


    .activity .activity-desk .arrow {
        border-right: 8px solid #F4F4F4 !important;
    }
    .activity .activity-desk .arrow {
        border-bottom: 8px solid transparent;
        border-top: 8px solid transparent;
        display: block;
        height: 0;
        left: -7px;
        position: absolute;
        top: 13px;
        width: 0;
    }

    .activity-desk .arrow-alt {
        border-left: 8px solid #F4F4F4 !important;
    }

    .activity-desk .arrow-alt {
        border-bottom: 8px solid transparent;
        border-top: 8px solid transparent;
        display: block;
        height: 0;
        right: -7px;
        position: absolute;
        top: 13px;
        width: 0;
    }

    .activity-desk .album {
        display: inline-block;
        margin-top: 10px;
    }

    .activity-desk .album a{
        margin-right: 10px;
    }

    .activity-desk .album a:last-child{
        margin-right: 0px;
    }

    .avatar-upload {
  position: relative;
  max-width: 205px;
  margin: 0px auto;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  content: "\f040";
  font-family: 'FontAwesome';
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 10px solid rgba(255,255,255,0.3);
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}
</style>
