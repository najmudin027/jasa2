@extends('admin.home')
@section('content')
<form action="  " method="post" id="form-user-address" style="display: contents;">
    <div class="col-md-12">
        <div class="card">
            <div class="header-bg card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    {{ $title }}
                </div>
            </div>
            <div class="card-body">
                <input type="hidden" value="{{ $user->id }}" name="user_id" id="user_id_val">
                <label>Phone Number</label>
                <div class="form-group">
                    <input name="phone1" placeholder="phone number" type="text" class="form-control" />
                </div>

                <label>Type</label>
                <div class="form-group">
                    <select class="form-control" id="address_type-select" name="ms_address_type_id" style="width:100%">
                        <option value="">Select Type</option>
                    </select>
                </div>

                <div>
                    <label>City Name</label>
                    <div class="form-group">
                        <select class="form-control" id="city-select" name="ms_cities_id" style="width:100%">
                            <option value="">Select City</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <label for="exampleEmail" class="col-sm-2 col-form-label">Maps</label>
    <div class="col-md-12" style="margin-top: 10px">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <input type="text" name="searchMap" style="margin-top: 7px; width: 70%; border:2px solid #4DB6AC;" id="searchMap" class="form-control" required>
                </div>
                <div class="form-group">

                    <div id="map" style="height:400px"></div>
                </div>
                <div class="row" style="display: none;">
                    <div class="form-group col-md-5 col-xs-5 mr-4">
                        <label class="control-label">Latitude</label>
                        <input size="20" type="text" id="latbox" class="form-control" name="lat" onchange="inputLatlng('new')" required>
                    </div>
                    <div class="form-group col-md-5 col-xs-5 mr-4">
                        <label class="control-label">Longitude</label>
                        <input size="20" type="text" id="lngbox" class="form-control" name="long" onchange="inputLatlng('new')" required>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <div class="col-sm-10">
                        <div class="col-md-10" style="display: none;">
                            <div class="row">
                                <div class="form-group col-md-5 col-xs-5 mr-4">
                                    <label class="control-label">City / Regency</label>
                                    <input size="20" type="text" id="cities" class="form-control" name="cities" readonly>
                                </div>
                                <div class="form-group col-md-5 col-xs-5 mr-4">
                                    <label class="control-label">Province</label>
                                    <input size="20" type="text" id="province" class="form-control" name="prv" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <label>Detail Address</label>
                <div class="form-group">
                    <textarea name="address" class="form-control" id="detail-address-input" rows="3"></textarea>
                </div>


                <div class="position-relative">
                    <button class="btn btn-success" id="submit">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@section('script')
@include('admin.template._mapsScript')
<script>
    globalCRUD
        .select2('#address_type-select', '/address_type/select2')
        .select2("#city-select", '/city/select2')

    $('#form-user-address').submit(function(e) {
        globalCRUD.handleSubmit($(this))
            .storeTo('/customer/profile/address', function(data) {
                data.lat = $("#latbox").val();
                data.long = $("#lngbox").val();
                data.province = $("#province").val();
                data.cities = $("#cities").val();
                data.search_map = $("#searchMap").val();
                return data;
            })
            .backTo(function(resp) {
                return '/customer/profile';
            })
        e.preventDefault();
    });
</script>
@endsection
