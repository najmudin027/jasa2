@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form action="  " method="post" id="form-user-address">
                <input type="hidden" value="{{ $user->id }}" name="user_id" id="user_id_val">
                <input type="hidden" value="{{ $addres->id }}" name="id" id="addres-id">
                <label>Phone Number</label>
                <div class="form-group">
                    <input name="phone1" placeholder="phone number" type="text" class="form-control" value="{{ $addres->phone1 }}" />
                </div>

                <label>Type</label>
                <div class="form-group">
                    <select class="form-control" id="address_type-select" name="ms_address_type_id" style="width:100%">
                        <option value="">Select Type</option>
                        <option value="{{ $addres->types->id }}" selected>{{ $addres->types->name }}</option>
                    </select>
                </div>

                @if($addres->ms_city_id != null)
                <div>
                    <label>City Name</label>
                    <div class="form-group">
                        <select class="form-control" id="city-select" name="ms_cities_id" style="width:100%">
                            <option value="">Select City</option>
                            <option value="{{ $addres->ms_city_id }}" selected>{{ $addres->city->name }}</option>
                        </select>
                    </div>
                </div>

                @endif

                <label class="control-label">Maps</label>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="hidden" name="id" />
                        <input type="text" name="searchMapEdit" style="margin-top: 7px; width: 70%; border:2px solid #4DB6AC;" id="searchMapEdit" class="form-control" value="{{ $addres->location_google }}" required>
                    </div>
                    <div class="form-group">
                        <div id="mapEdit" style="width: 700px; height: 500px;"></div>
                    </div>
                    <div class="row" style="display: none;">
                        <div class="form-group col-md-5 col-xs-5 mr-4">
                            <label class="control-label">Latitude</label>
                            <input size="20" type="text" id="latboxEdit" class="form-control" name="latEdit" onchange="inputLatlng('edit')" value="{{ $addres->latitude }}" required>
                        </div>
                        <div class="form-group col-md-5 col-xs-5 mr-4">
                            <label class="control-label">Longitude</label>
                            <input size="20" type="text" id="lngboxEdit" class="form-control" name="longEdit" onchange="inputLatlng('edit')" value="{{ $addres->longitude }}" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-10" style="display: none;">
                    <div class="row">
                        <div class="form-group col-md-5 col-xs-5 mr-4">
                            <label class="control-label">City / Regency</label>
                            <input size="20" type="text" id="cityEdit" class="form-control" name="cityEdit" value="{{ $addres->cities }}" readonly>
                        </div>
                        <div class="form-group col-md-5 col-xs-5 mr-4">
                            <label class="control-label">Province</label>
                            <input size="20" type="text" id="provinceEdit" class="form-control" name="prvEdit" value="{{ $addres->province }}" readonly>
                        </div>
                    </div>
                </div>


                <label>Detail Address</label>
                <div class="form-group">
                    <textarea name="address" class="form-control" id="detail-address-input" rows="3">{{ $addres->address }}</textarea>
                </div>


                <div class="position-relative">
                    <button class="btn btn-success" id="submit">Save</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
@include('admin.template._mapsScript')
<script>
    initMapEdit(parseFloat($('#latboxEdit').val()), parseFloat($('#lngboxEdit').val()));
    globalCRUD
        .select2('#address_type-select', '/address_type/select2')
        .select2("#city-select", '/city/select2')

    $('#form-user-address').submit(function(e) {
        globalCRUD.handleSubmit($(this))
            .updateTo('/customer/profile/address/' + $('#addres-id').val(), function(data) {
                data.id = $("#addres-id").val();
                data.lat = $("#latboxEdit").val();
                data.long = $("#lngboxEdit").val();
                data.province = $("#provinceEdit").val();
                data.cities = $("#citiesEdit").val();
                data.search_map = $("#searchMapEdit").val();
                return data;
            })
            .backTo(function(resp) {
                return '/customer/profile';
            })
        e.preventDefault();
    });
</script>
@endsection
