@extends('admin.home')
@section('content')
<div class="col-md-4 col-xl-4" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-body">
            <div class="users-container">
                <div class="chat-search-box">
                    <div class="input-group">
                        <input class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <ul class="users">
                    @foreach($chats as $chat)
                    <li class="person" data-name="{{ $chat->members_group->first()->user->full_name }}" data-user="{{ $chat->members_group->first()->user->id }}" data-room="{{ $chat->id }}">
                        <div class="user">
                            <img src="{{ $chat->members_group->first()->user->avatar }}" alt="{{ $chat->members_group->first()->user->full_name }}" />
                            <span class="status {{ ($chat->last_message->read_at == null && Auth::id() != $chat->last_message->user_id) ? 'busy' : '' }}"></span>
                        </div>
                        <p class="name-time">
                            <span class="name">{{ $chat->members_group->first()->user->full_name }}</span>
                            <br />
                            <span class="time">{{ $chat->last_message->message }}</span>
                        </p>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-md-8 col-xl-8" style="margin-bottom: 20px">
    <div class="card chat-content">
        <div class="card-body">
            <div class="selected-user">
                <span>To: <span class="name-chat">{{ $member == null ? ($to == null ? '' : $to->full_name) : $member->user->full_name }}</span></span>
            </div>
            <div class="chat-container">
                <ul class="chat-box chatContainerScroll">

                </ul>
                <form id="chat-form">
                    <input type="hidden" name="user_id_to" class="user_id_to" value="{{ $member == null ? ($to == null ? '' : $to->id) : $member->user->id }}" />
                    <input type="hidden" name="chat_room_id" class="chat_room_id" value="{{ $member == null ? '' : $member->chat_id }}" />
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-image"></i></button>
                            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-file"></i></button>
                        </div>
                        <input type="text" name="message" class="form-control" placeholder="Type your message here..." />
                        <div class="input-group-append">
                            <button class="btn btn-outline-secondary" type="submit"><i class="fa fa-paper-plane"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .pilih {
        background: #ececf0;
    }
</style>
@include('admin.customer.chats.style')
@endsection

@section('script')
<script>
    $('.chat-content').hide();
    $('.person').click(function() {
        $('.chat-content').show();
        $('.name-chat').text($(this).attr('data-name'));
        $(this).toggleClass('pilih')
        $('.user_id_to').val($(this).attr('data-user'));
        $('.chat_room_id').val($(this).attr('data-room'));

        Helper.loadingStart();
        // post data
        Axios.get(Helper.apiUrl('/customer/chats/' + $(this).attr('data-room')))
            .then(function(response) {
                $('.chat-box').html('').append(response.data.data);
                $(".chat-box").animate({
                    scrollTop: $('.chat-box')[0].scrollHeight
                }, 1000);
                Helper.loadingStop();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
    })

    $("#chat-form")
        .submit(function(e) {
            data = Helper.serializeForm($(this));

            var formData = new FormData();
            formData.append("message", data.message);
            formData.append("user_id", data.user_id_to);
            formData.append("chat_id", data.chat_room_id);

            Helper.loadingStart();
            // post data
            Axios.post(Helper.apiUrl('/customer/chats/reply'), formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    $('[name="message"]').val('');
                    $('.chat-box').append(response.data.data);
                    $(".chat-box").animate({
                        scrollTop: $('.chat-box')[0].scrollHeight
                    }, 1000);
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });

            e.preventDefault();
        })
</script>

@if($member != null)
<script>
    $(function() {
        $('.chat-content').show();

        Helper.loadingStart();
        // post data
        Axios.get(Helper.apiUrl('/customer/chats/{{ $member->chat_id }}'))
            .then(function(response) {
                $('.chat-box').html('').append(response.data.data);
                $(".chat-box").animate({
                    scrollTop: $('.chat-box')[0].scrollHeight
                }, 1000);
                Helper.loadingStop();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
    })
</script>
@endif

@if($to != null)
<script>
    $(function() {
        $('.chat-content').show();
    })
</script>
@endif
@endsection