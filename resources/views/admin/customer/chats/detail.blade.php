@foreach($chats as $date => $dates)
<div class="date">
    <hr>
    <span>{{ $date == date('d/m/Y') ? 'Today' : $date }}</span>
    <hr>
</div>
@foreach($dates as $chat)

@if($auth->id != $chat->user->id)
<div class="message">
    <img class="avatar-md" src="{{ $chat->user->avatar }}" data-toggle="tooltip" data-placement="top" title="{{ $chat->user->name }}" alt="avatar">
    <div class="text-main">
        <div class="text-group">
            <div class="text">
                @if($chat->filename != null)
                    <img src="{{ asset('storage/user/chat/' . $chat->filename) }}" alt="" style="width: 100%; height: auto;">
                    @endif
                    @if($chat->order_obj != null)
                    @include('admin.customer.chats._components._action_order', [
                    'order' => $chat->order,
                    'url_order' => $auth->hasRole('Technician') ? url('/teknisi/request-job-accept/' . $chat->order->id) : url('/customer/service_detail/' . $chat->order->id)
                    ])
                    <hr />
                @endif
                @if($chat->is_html == 1)
                    {!! $chat->message !!}
                @else
                    @if($chat->address_obj != null)
                    <?php $ad = json_decode($chat->address_obj); ?>
                    <label>Address</label>
                    <p>{{ $ad->address }}</p>
                    <p><a href="#" class="open_map_chat" data-lat='{{ $ad->latitude ?? '' }}' data-lng='{{ $ad->longitude ?? '' }}' style="text-decoration: underline"><i class="fa fa-map-marker"></i> View Location</a></p>
                    @endif
                    <hr/> 
                    <p>{{ $chat->message == 'share_map' ? '' : $chat->message }}</p>
                @endif
            </div>
        </div>
        <span>{{ $chat->created_at->format('H:i') }}</span>
    </div>
</div>
@endif
@if($auth->id == $chat->user->id)
<div class="message me">
    <div class="text-main">
        <div class="text-group me">
            <div class="text me">
                @if($chat->filename != null)
                    <img src="{{ asset('storage/user/chat/' . $chat->filename) }}" alt="" style="width: 100%; height: auto;">
                @endif
                    @if($chat->order_obj != null)
                        @include('admin.customer.chats._components._action_order', [
                            'order' => $chat->order,
                            'url_order' => $auth->hasRole('Technician') ? url('/teknisi/request-job-accept/' . $chat->order->id) : url('/customer/service_detail/' . $chat->order->id)
                        ])
                    <hr />
                @endif
                @if($chat->is_html == 1)
                    {!! $chat->message !!}
                @else
                    @if($chat->address_obj != null)
                    <?php $ad = json_decode($chat->address_obj); ?>
                    <label>Address</label>
                    <p>{{ $ad->address }}</p>
                    <p><a href="#" class="open_map_chat" data-lat='{{ $ad->latitude ?? '' }}' data-lng='{{ $ad->longitude ?? '' }}' style="text-decoration: underline"><i class="fa fa-map-marker"></i> View Location</a></p>
                    @endif
                    <p>{{ $chat->message == 'share_map' ? '' : $chat->message }}</p>
                @endif
            </div>
        </div>
        <span>{{ $chat->created_at->format('H:i') }}</span>
    </div>
</div>
@endif

@endforeach
@endforeach