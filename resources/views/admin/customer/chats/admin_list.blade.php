@extends('admin.home')
@section('content')
<input type="hidden" id="teknisi_id" value="{{ $techician->user_id }}">
<input type="hidden" class="chat_room_id" value="0">
<div class="col-md-12">
    <main>
        <div class="layout">
            <div class="sidebar" id="sidebar">
                <div class="container">
                    <div class="col-md-12">
                        <!-- Start of Discussions -->
                        <div id="discussions" class="tab-pane fade in active show">
                            <figure class="setting"><img class="avatar-xl" src="{{ $techician->user->avatar }}" alt="avatar"></figure>
                            <span class="logo">{{  $techician->user->full_name }}</span>
                            <div class="search">
                                <form class="form-inline position-relative">
                                    <input type="search" class="form-control" id="conversations" placeholder="Search for conversations...">
                                    <button type="button" class="btn btn-link loop"><i class="fa fa-search"></i></button>
                                </form>
                                <button class="btn create" data-toggle="modal" data-target="#startnewchat"><i class="ti-pencil"></i></button>
                            </div>
                            <div class="discussions" id="scroller">
                                <div class="list-group" id="chats" role="tablist" style="overflow: scroll;">
                                    @foreach($chats as $chat)
                                    <?php $first_group = $chat->members_group->first(); ?>
                                    <a data-room="{{ $chat->id }}" data-user="{{ $first_group->user->id ?? 0 }}" data-name="{{ $first_group->user->full_name ?? '-' }}" href="#" class="chat-room filterDiscussions all unread single" data-toggle="list" role="tab">
                                        <img class="avatar-md" src="{{ $first_group->user->avatar ?? '' }}" data-toggle="tooltip" data-placement="top" title="{{ $first_group->user->full_name ?? '' }}" alt="avatar" />
                                        <div class="data">
                                            <h5>{{ $first_group->user->full_name ?? $chat->room_no }}</h5>
                                            @if($chat->unread_message_count > 0)
                                            <div class="new bg-red chat-list-count-{{ $chat->id }}">
                                                <span>{{ $chat->unread_message_count }}+</span>
                                            </div>
                                            @endif
                                            <span>{{ $chat->last_message->created_at->format('H:i') }}</span>
                                            <p class="chat-list-msg-{{ $chat->id }}">{{ Str::limit($chat->last_message->message, 20) }}</p>
                                            @if($chat->is_has_order == 1)
                                            <span class="badge chat-order-no-{{ $chat->id }}" style="background-color: #444054; color: #fff;">#{{ $chat->room_no }}</span>
                                            @endif
                                        </div>
                                    </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- End of Discussions -->
                    </div>
                </div>
            </div><!-- Sidebar -->

            <div class="main bg" id="chat-dialog">
                <div class="bg-image" style="background-image: url(https://wpkixx.com/html/talkshak/dist/img/avatars/pattern2.jpg)"></div>
                <!-- Start of Babble -->
                <div class="babble tab-pane fade active show" id="list-chat" role="tabpanel" aria-labelledby="list-chat-list">
                    <!-- Start of Chat -->
                    <div class="chat" id="chat1">

                        <div class="top">
                            <div class="container">
                                <div class="col-md-12">
                                    <div class="inside">
                                        <div class="data">
                                            <h5><a href="#" class="chat-user-name">-</a></h5>
                                            <span class="badge badge-order-code" style="background-color: #444054; color: #fff;"></span>
                                        </div>
                                        <button class="btn d-md-block d-none" title="More Info">
                                            <i class="fa fa-info"></i>
                                        </button>
                                        <button class="btn back-to-mesg" title="Back">
                                            <i class="fa fa-arrow-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content" id="content">
                            <div class="container">
                                <div class="col-md-12" id="chat-content">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Chat -->
                </div>
                <!-- End of Babble -->
            </div>
        </div>
    </main>
</div>

@include('admin.customer.chats.style_uji')
<style>
    .main .chat .content{
        height: calc(100vh - 218px);
    }
</style>
@endsection

@section('script')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.5.0/css/perfect-scrollbar.min.css" integrity="sha512-n+g8P11K/4RFlXnx2/RW1EZK25iYgolW6Qn7I0F96KxJibwATH3OoVCQPh/hzlc4dWAwplglKX8IVNVMWUUdsw==" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.5.0/perfect-scrollbar.min.js" integrity="sha512-yUNtg0k40IvRQNR20bJ4oH6QeQ/mgs9Lsa6V+3qxTj58u2r+JiAYOhOW0o+ijuMmqCtCEg7LZRA+T4t84/ayVA==" crossorigin="anonymous"></script>

<script>
    var page = 1;
    var lastPage = 1;

    function loadMore(page) {
        Helper.loadingStart();
        room_id = $('.chat_room_id').val();
        teknisi_id = $('#teknisi_id').val();
        
        // post data
        Axios.get(Helper.apiUrl('/chats/teknisi/' + teknisi_id + '/'+ room_id), {
                params: {
                    page: page
                }
            })
            .then(function(response) {
                Helper.loadingStop();
                lastPage = response.data.data.data.last_page;
                var firstMsg = $('#content .date:first');
                $('#content').scrollTop(firstMsg[0].scrollHeight);
                $('#chat-content').prepend(response.data.data.content);
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

    }

    $('.chat-room').click(function(e) {
        page = 1;
        $('.form-chat-area').show();

        room_id = $(this).attr('data-room');
        user_id = $(this).attr('data-user');
        name = $(this).attr('data-name');
        teknisi_id = $('#teknisi_id').val();
        room_no = $('.chat-order-no-' + room_id).text();

        $('.badge-order-code').text(room_no);
        $('.chat-user-name').text(name);
        $('.user_id_to').val(user_id);
        $('.chat_room_id').val(room_id);

        Helper.loadingStart();
        // post data
        Axios.get(Helper.apiUrl('/chats/teknisi/' + teknisi_id + '/'+ room_id))
            .then(function(response) {
                lastPage = response.data.data.data.last_page;
                $('.chat-list-count-' + room_id).remove();
                $('#chat-content').html('').prepend(response.data.data.content);
                $('#content').scrollTop($('#content')[0].scrollHeight + 100);
                Helper.loadingStop();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })


    $('#content').scroll(function() {
        if ($('#content').scrollTop() == ($('#content').height() - $('#content').height()) && lastPage > page) {
            console.log('ajax call get data from server and append to the div')
            page++;
            loadMore(page)
        }
    });
</script>
@endsection