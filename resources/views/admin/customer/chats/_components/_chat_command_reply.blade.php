@if($auth->id != $chat->user->id)
<div class="message">
    <img class="avatar-md" src="{{ $chat->user->avatar }}" data-toggle="tooltip" data-placement="top" title="{{ $chat->user->name }}" alt="avatar">
    <div class="text-main">
        <div class="text-group">
            <div class="text">
                {!! $chat->message !!}                
            </div>
        </div>
        <span>{{ $chat->created_at->format('H:i') }}</span>
    </div>
</div>
@endif
@if($auth->id == $chat->user->id)
<div class="message me">
    <div class="text-main">
        <div class="text-group me">
            <div class="text me">
                @if($chat->filename != null)
                <img src="{{ asset('storage/user/chat/' . $chat->filename) }}" alt="" style="width: 100%; height: auto;">
                @endif
                {!! $chat->message !!}
            </div>
        </div>
        <span>{{ $chat->created_at->format('H:i') }}</span>
    </div>
</div>
@endif