@extends('admin.home')
@section('content')
<div class="col-md-12">
    <main>
        <div class="layout">
            <div class="sidebar" id="sidebar">
                <div class="container">
                    <div class="col-md-12">
                        <!-- Start of Discussions -->
                        <div id="discussions" class="tab-pane fade in active show">
                            <figure class="setting"><img class="avatar-xl" src="{{ $auth->avatar }}" alt="avatar"></figure>
                            <span class="logo"><img src="https://astechindo.com/an-component/media/upload-gambar-pendukung/AstLogo.png" alt=""></span>
                            <div class="search">
                                <form class="form-inline position-relative">
                                    <input type="search" class="form-control" id="conversations" placeholder="Search for User...">
                                    <button type="button" class="btn btn-link loop"><i class="fa fa-search"></i></button>
                                </form>
                                <button class="btn create" data-toggle="modal" data-target="#startnewchat"><i class="ti-pencil"></i></button>
                            </div>
                            <div class="discussions" id="scroller">
                                <div class="list-group" id="chats" role="tablist" style="overflow: scroll;">
                                    @if($member == null && $to != null)
                                    <a href="#" class="filterDiscussions all unread single active" data-toggle="list" role="tab">
                                        <img class="avatar-md" src="{{ $to->avatar }}" data-toggle="tooltip" data-placement="top" title="" alt="avatar" />
                                        <div class="status offline"></div>

                                        <div class="data">
                                            <h5>{{ $to->full_name }}</h5>
                                        </div>
                                    </a>
                                    @endif
                                    @foreach($chats as $chat)
                                    <?php
                                    $first_group = $chat->members_group->first();
                                    if ($first_group == null) {
                                        $aktif = '';
                                    } else {
                                        $aktif = $first_group->user->id == $req->to ? 'active' : '';
                                    }

                                    ?>
                                    <a data-room="{{ $chat->id }}" data-user="{{ $first_group->user->id ?? '0' }}" data-name="{{ $first_group->user->full_name ?? '-' }}" href="#" class="chat-room filterDiscussions all unread single {{ $aktif }}" data-toggle="list" role="tab">
                                        <img class="avatar-md" src="{{ $first_group->user->avatar ?? 'https://cdn2.vectorstock.com/i/1000x1000/85/86/man-icon-delete-male-user-account-person-profile-vector-21208586.jpg' }}" data-toggle="tooltip" data-placement="top" title="{{ $first_group->user->full_name ?? '-' }}" alt="avatar" />
                                        <div class="data">
                                            <h5>{{ $first_group->user->full_name ?? '--' }}</h5>
                                            @if($chat->unread_message_count > 0)
                                            <div class="new bg-red chat-list-count-{{ $chat->id }}">
                                                <span>{{ $chat->unread_message_count }}+</span>
                                            </div>
                                            @endif
                                            @if($chat->last_message != null)
                                            <span>{{ $chat->last_message->created_at->format('H:i') }}</span>
                                            <p class="chat-list-msg-{{ $chat->id }}">{{ $chat->last_message->is_html == 1 ? '-' : Str::limit($chat->last_message->message, 20) }}</p>
                                            @endif
                                            @if($chat->is_has_order == 1)
                                            <span class="badge chat-order-no-{{ $chat->id }}" style="background-color: #444054; color: #fff;">#{{ $chat->room_no }}</span>
                                            @endif
                                        </div>
                                    </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!-- End of Discussions -->
                    </div>
                </div>
            </div><!-- Sidebar -->

            <div class="main bg" id="chat-dialog">
                <div class="bg-image" style="background-image: url(https://wpkixx.com/html/talkshak/dist/img/avatars/pattern2.jpg)"></div>
                <!-- Start of Babble -->
                <div class="babble tab-pane fade active show" id="list-chat" role="tabpanel" aria-labelledby="list-chat-list">
                    <!-- Start of Chat -->
                    <div class="chat" id="chat1">

                        <div class="top">
                            <div class="container">
                                <div class="col-md-12">
                                    <div class="inside">
                                        <div class="chat-user-status-icon" data-user="0"></div>
                                        <div class="data">
                                            <h5><a href="#" class="chat-user-name">{{ $member == null ? ($to == null ? '' : $to->full_name) : $member->user->full_name }}</a></h5>
                                            <span class="badge badge-order-code" style="background-color: #444054; color: #fff;"></span>
                                            <span class="chat-user-status-text" data-user="0"></span>
                                        </div>
                                        <button class="btn back-to-mesg" title="Back">
                                            <i class="fa fa-arrow-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content" id="content">
                            <div class="container">
                                <div class="col-md-12" id="chat-content" data-chat='0'>
                                    @if($to == null)
                                    <div class="no-messages">
                                        <i class="fa fa-comments"></i>
                                        <p>Seems people are shy to start the chat. Break the ice send the first message.</p>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="col-md-12">
                                <div class="bottom form-chat-area">
                                    <form class="text-area" id="chat-form">
                                        <input type="hidden" name="user_id_to" class="user_id_to" value="{{ $member == null ? ($to == null ? '' : $to->id) : $member->user->id }}" />
                                        <input type="hidden" name="chat_room_id" class="chat_room_id" value="{{ $member == null ? '' : $member->chat_id }}" />
                                        <textarea class="form-control text-message" name="message" placeholder="Start typing for reply..." rows="1"></textarea>
                                        <button type="submit" class="btn send"><i class="fa fa-send"></i></button>
                                    </form>
                                    <label>
                                        <input type="file" id="file-chat">
                                        <span class="btn attach"><i class="fa fa-paperclip"></i></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Chat -->
                </div>
                <!-- End of Babble -->
            </div>
        </div>
    </main>
</div>

<div class="modal fade" data-backdrop="false" id="modal-media" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;">
                <img class="media-img" src="" alt="" style="width: 50%; height: auto;">
                <br>
                <div class="form-group">
                    <textarea class="form-control media-message" name="message" placeholder="Start typing for reply..."></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn send-media"><i class="fa fa-send"></i> Send</button>
                <button class="btn waves-effect" data-dismiss="modal" type="button"><i class="fa fa-close"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<form id="form-done-job">
    <div class="modal fade" data-backdrop="false" id="modal-done-job" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Done Job
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="done_job_order_id" id="done_job_order_id" value="">
                    <input type="hidden" name="chat_message_id" id="chat_message_id">
                    <label for="">Symptom Code</label>
                    <div class="form-group">
                        <select class="form-control" id="ms_symptom_code" name="ms_symptom_code_id" style="width:100%" required>
                            <option value="" selected="selected">--Select--</option>
                        </select>
                    </div>
                    <label for="">Repair Code</label>
                    <div class="form-group">
                        <select class="form-control" id="ms_repair_code" name="ms_repair_code_id" style="width:100%" required>
                            <option value="" selected="selected">--Select--</option>
                        </select>
                    </div>
                    <label for="">Desc</label>
                    <div class="form-group">
                        <textarea class="form-control" id="repair_desc" name="repair_desc" rows="3" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SAVE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@include('admin.customer.chats.style_uji')
@endsection

@section('script')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.5.0/css/perfect-scrollbar.min.css" integrity="sha512-n+g8P11K/4RFlXnx2/RW1EZK25iYgolW6Qn7I0F96KxJibwATH3OoVCQPh/hzlc4dWAwplglKX8IVNVMWUUdsw==" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.5.0/perfect-scrollbar.min.js" integrity="sha512-yUNtg0k40IvRQNR20bJ4oH6QeQ/mgs9Lsa6V+3qxTj58u2r+JiAYOhOW0o+ijuMmqCtCEg7LZRA+T4t84/ayVA==" crossorigin="anonymous"></script>

<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.1.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.23.0/firebase-messaging.js"></script>
<script>
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyB4aK174OnhGjSqWv2kedc_97y4mWaZfZE",
        authDomain: "astech-469b6.firebaseapp.com",
        databaseURL: "https://astech-469b6.firebaseio.com",
        projectId: "astech-469b6",
        storageBucket: "astech-469b6.appspot.com",
        messagingSenderId: "665215279643",
        appId: "1:665215279643:web:bb7c4b60fd3219a9710ebe"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    const messaging = firebase.messaging();

    messaging
        .requestPermission()
        .then(function() {
            alert('Token')
            return messaging.getToken()
        })
        .then(function(token) {
            alert(token)
            $.ajax({
                url: Helper.apiUrl('/customer/chats/firebase_save_token'),
                type: 'POST',
                data: {
                    fcm_token: token
                },
                success: function(response) {
                    console.log(response)
                },
                error: function(err) {
                    console.log(" Can't do because: " + err);
                },
            });
        })
        .catch(function(err) {
            console.log("Unable to get permission to notify.", err);
        });

    messaging.onMessage(function(payload) {
        console.log(payload)
        const noteTitle = payload.notification.title;
        const noteOptions = {
            body: payload.notification.body,
            icon: payload.notification.icon,
        };
        new Notification(noteTitle, noteOptions);
    });

    $("#chat-form")
        .submit(function(e) {
            e.preventDefault();
        })

    function sendMsg() {

    }
</script>
@endsection