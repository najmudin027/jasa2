@extends('admin.home')
@section('content')
<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header card-header-tab-animation">
            <ul class="nav nav-justified">
                <li class="nav-item"><a data-toggle="tab" href="#tab-eg115-0" class="active nav-link">Available ({{ count($orders) }})</a></li>
                <li class="nav-item"><a data-toggle="tab" href="#tab-eg115-1" class="nav-link">Prosses ({{ count($claims) }})</a></li>
                <li class="nav-item"><a data-toggle="tab" href="#tab-eg115-2" class="nav-link">Reject ({{ count($claimsReject) }})</a></li>
                <li class="nav-item"><a data-toggle="tab" href="#tab-eg115-3" class="nav-link">Success ({{ count($claimsDone) }})</a></li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane active" id="tab-eg115-0" role="tabpanel">
                    <div class="row">
                        @foreach($orders as $order)
                        <div class="col-md-4">
                            <div class="mb-3 profile-responsive card">
                                <div class="dropdown-menu-header">
                                    <div class="dropdown-menu-header-inner bg-dark">
                                        <div class="menu-header-image opacity-2"></div>
                                        <div class="menu-header-content btn-pane-right">
                                            <div class="avatar-icon-wrapper mr-3 avatar-icon-xl btn-hover-shine">
                                                <div class="avatar-icon rounded">
                                                    <img src="{{ $order->service_detail[0]->technician->user->avatar }}" alt="Avatar 5">
                                                </div>
                                            </div>
                                            <div>
                                                <h5 class="menu-header-title">{{ $order->service_detail[0]->technician->user->name }}</h5>
                                                <h6 class="menu-header-subtitle">#{{ $order->code }}</h6>
                                            </div>
                                            <div class="menu-header-btn-pane">
                                                <a href="{{ url('customer/service_detail/' . $order->id) }}" class="btn btn-primary btn-sm" data-order="{{ $order->id }}"><i class="fa fa-arrow-right"></i> Detail</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <label>Schedule:</label>
                                        <p>{{ $order->schedule->format('d F Y G:i') }}</p>
                                        <label>Due Date:</label>
                                        <p>{{ now()->diff($order->due_date)->format('%d days, %h hours and %i minutes') }}</p>

                                        <label for="ms_symptom_code">Symptom Code</label>
                                        <p>{{ $order->symptom_code == null ? '' : $order->symptom_code->name }}</p>
                                        <label for="ms_symptom_code">Repair Code</label>
                                        <p>{{ $order->repair_code == null ? '' : $order->repair_code->name }}</p>
                                        <label for="repair_desc">Repair Description</label>
                                        <p>{{ $order->repair_desc }}</p>
                                    </li>
                                    <li class="p-0 list-group-item">
                                        <div class="grid-menu grid-menu-2col">
                                            <div class="no-gutters row">
                                                <div class="col-sm-12">
                                                    <button class="btn-claim-garansi btn-icon-vertical btn-square btn-transition br-bl btn btn-outline-link" data-order="{{ $order->id }}">
                                                        <i class="lnr-gift text-focus opacity-7 btn-icon-wrapper mb-2"> </i> Claim
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="tab-pane" id="tab-eg115-1" role="tabpanel">
                    <div class="table-responsive">
                        <table class="text-nowrap table table-hover">
                            <thead>
                                <tr>
                                    <th>Claim Code </th>
                                    <th>Order No </th>
                                    <th>Service Name </th>
                                    <th>Grand Total </th>
                                    <th>Status </th>
                                    <th>Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($claims as $claim)
                                <tr>
                                    <td>{{ $claim->garansi_no }}</td>
                                    <td><a href="{{ url('customer/service_detail/' . $claim->order->id) }}">{{ $claim->order->code }}</a></td>
                                    <td>{{ $claim->order->service_name }}</td>
                                    <td>Rp. {{ \App\Helpers\thousanSparator($claim->order->grand_total) }}</td>
                                    <td>{{ $claim->status }}</td>
                                    <td><a href='{{ url("customer/garansi/" . $claim->id . "/detail") }}' class='btn btn-search btn-sm btn-danger'><i class='fa fa-arrow-right'></i> Detail Claim</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="tab-eg115-2" role="tabpanel">
                    <table class="text-nowrap table table-hover">
                        <thead>
                            <tr>
                                <th>Claim Code </th>
                                <th>Order No </th>
                                <th>Service Name </th>
                                <th>Grand Total </th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($claimsReject as $claim)
                            <tr>
                                <td>{{ $claim->garansi_no }}</td>
                                <td><a href="{{ url('customer/service_detail/' . $claim->order->id) }}">{{ $claim->order->code }}</a></td>
                                <td>{{ $claim->order->service_name }}</td>
                                <td>Rp. {{ \App\Helpers\thousanSparator($claim->order->grand_total) }}</td>
                                <td><a href='{{ url("customer/garansi/" . $claim->id . "/detail") }}' class='btn btn-search btn-sm btn-danger'><i class='fa fa-arrow-right'></i> Detail Claim</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab-eg115-3" role="tabpanel">
                    <table class="text-nowrap table table-hover">
                        <thead>
                            <tr>
                                <th>Claim Code </th>
                                <th>Order No </th>
                                <th>Service Name </th>
                                <th>Grand Total </th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($claimsDone as $claim)
                            <tr>
                                <td>{{ $claim->garansi_no }}</td>
                                <td><a href="{{ url('customer/service_detail/' . $claim->order->id) }}">{{ $claim->order->code }}</a></td>
                                <td>{{ $claim->order->service_name }}</td>
                                <td>Rp. {{ \App\Helpers\thousanSparator($claim->order->grand_total) }}</td>
                                <td><a href='{{ url("customer/garansi/" . $claim->id . "/detail") }}' class='btn btn-search btn-sm btn-danger'><i class='fa fa-arrow-right'></i> Detail Claim</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<form id="form-garansi">
    <div class="modal fade" data-backdrop="false" id="modal-garansi" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Claim Garansi
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id-order" name="order_id">
                    <div class="position-relative form-group">
                        <label class="" for="">Subject</label>
                        <input name="subject" class="form-control" />
                    </div>
                    <div class="position-relative form-group">
                        <label class="" for="">Message</label>
                        <textarea name="desc" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SUBMIT
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        CLOSE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    $(document).on('click', '.btn-claim-garansi', function() {
        id_order = $(this).attr('data-order');
        $('#id-order').val(id_order);
        $('#modal-garansi').modal('show');
    })

    $('#form-garansi').submit(function(e) {
        data = Helper.serializeForm($(this));

        Helper.loadingStart();
        // post data
        Axios.post(Helper.apiUrl('/customer/garansi'), data)
            .then(function(response) {
                Helper.successNotif('Success, Successfully!');
                window.location.href = Helper.redirectUrl('/customer/garansi/' + response.data.data.id + '/detail');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
@endsection