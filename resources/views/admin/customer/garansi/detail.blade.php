@extends('admin.home')
@section('content')
<div class="col-md-12">
    <main>
        <div class="layout">
            <div class="sidebar" id="sidebar">
                <div class="container">
                    <div class="col-md-12">
                        <!-- Start of Discussions -->
                        <div id="discussions" class="tab-pane fade in active show">
                            <figure class="setting"><img class="avatar-xl" src="{{ $auth->avatar }}" alt="avatar"></figure>
                            <span class="logo"><b>#{{ $garansi->garansi_no }}</b> ({{ $garansi->status }})</span>
                            <div class="discussions" id="scroller">
                                <div class="list-group" id="chats" role="tablist">
                                    <div class="data" style="margin-top: 20px; overflow: scroll; max-height: 400px; overflow-y: overlay;">
                                        @if(Auth::user()->hasRole('admin'))
                                        <div class="card">
                                            <div class="card-header">
                                                <ul class="nav nav-justified">
                                                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-0" class="active nav-link">Order</a></li>
                                                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-1" class="nav-link">Customer</a></li>
                                                    <li class="nav-item"><a data-toggle="tab" href="#tab-eg7-2" class="nav-link">Technician</a></li>
                                                </ul>
                                            </div>
                                            <div class="card-body">
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab-eg7-0" role="tabpanel">
                                                        
                                                        @if($garansi->status == 'pending')
                                                            @if($garansi->customer_can_reply == 0)
                                                            <button data-id="{{ $garansi->id }}" class="btn-sm btn-hover-shine customer-reply btn btn-primary">
                                                                <i class="fa fa-square-o"></i>
                                                                Accept Claim
                                                            </button>
                                                            <button data-id="{{ $garansi->id }}" class="btn-hover-shine btn-sm garansi-reject btn btn-danger">
                                                                <i class="fa fa-square-o"></i>
                                                                Reject
                                                            </button>
                                                            @endif
                                                        @endif

                                                        @if($garansi->status == 'reject')
                                                            <button data-id="{{ $garansi->id }}" class="btn-sm btn-hover-shine customer-reply btn btn-primary">
                                                                <i class="fa fa-square-o"></i>
                                                                Reaccept Claim
                                                            </button>
                                                        @endif

                                                        

                                                        @if($garansi->customer_can_reply == 1 && $garansi->order->order_status->id == 10 && $garansi->status == 'pending' && Auth::user()->hasRole('admin'))
                                                            <button class="garansi-job btn-sm btn btn-success"><i class="fa fa-refresh"></i> Revisit</button>
                                                        @endif

                                                        <hr>

                                                        <label>Schedule:</label>
                                                        <p id="schedule_display">{{ $garansi->order->schedule->format('d F Y G:i') }}</p>
                                                        <hr />

                                                        <label>Symptom detail:</label>
                                                        <p>{{ $garansi->order->symptom_detail }}</p>
                                                        <hr />

                                                        <label for="ms_symptom_code">Symptom Code</label>
                                                        <p>{{ $garansi->order->symptom_code == null ? '' : $garansi->order->symptom_code->name }}</p>
                                                        <hr />

                                                        <label for="ms_symptom_code">Repair Code</label>
                                                        <p>{{ $garansi->order->repair_code == null ? '' : $garansi->order->repair_code->name }}</p>
                                                        <hr />

                                                        <label for="repair_desc">Repair Description</label>
                                                        <p>{{ $garansi->order->repair_desc }}</p>
                                                        <hr />

                                                        <label>Payment Type:</label>
                                                        <br />
                                                        {!! $garansi->order->payment_type == 0 ? '<div class="badge badge-danger">Offline</div>' : '<div class="badge badge-info">Online</div>' !!}
                                                    </div>
                                                    <div class="tab-pane" id="tab-eg7-1" role="tabpanel">
                                                        <div class="profile-responsive">
                                                            <div class="dropdown-menu-header">
                                                                <div class="dropdown-menu-header-inner">
                                                                    <div class="menu-header-image opacity-2"></div>
                                                                    <div class="menu-header-content">
                                                                        <div class="avatar-icon-wrapper mr-3 avatar-icon-xl btn-hover-shine">
                                                                            <div class="avatar-icon rounded">
                                                                                <img class="rounded-circle" src="{{ $garansi->order->user->avatar }}" alt="Avatar 5">
                                                                            </div>
                                                                        </div>
                                                                        <div style="color: black;">
                                                                            <h5 class="menu-header-title">{{ $garansi->order->user->full_name }}</h5>
                                                                            <h6 class="menu-header-subtitle">{{ $garansi->order->user->email }}</h6>
                                                                            <h6 class="menu-header-subtitle">{{ $garansi->order->user->phone }}</h6>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <ul class="list-group list-group-flush">
                                                                <li class="list-group-item">
                                                                    <label>Address:</label>
                                                                    <p>{{ $garansi->order->address_type_name }}</p>
                                                                    <p>{{ $garansi->order->address }}</p>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    <label>Bill Address:</label>
                                                                    <p>{{ $garansi->order->bill_to }}</p>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab-eg7-2" role="tabpanel">
                                                        <div class="profile-responsive text-center">
                                                            <div class="dropdown-menu-header">
                                                                <div class="dropdown-menu-header-inner">
                                                                    <div class="menu-header-image opacity-2"></div>
                                                                    <div class="avatar-icon-wrapper mr-3 avatar-icon-xl btn-hover-shine">
                                                                        <div class="avatar-icon rounded">
                                                                            <img class="rounded-circle" src="{{ $garansi->order->detail->technician->user->avatar }}" alt="Avatar 5">
                                                                        </div>
                                                                    </div>
                                                                    <div class="menu-header-content">                                                                        
                                                                        <div style="color: black;">
                                                                            <h5 class="menu-header-title">{{ $garansi->order->detail->technician->user->full_name }}</h5>
                                                                            <h6 class="menu-header-subtitle">{{ $garansi->order->detail->technician->user->email }}</h6>
                                                                            <h6 class="menu-header-subtitle">{{ $garansi->order->detail->technician->user->phone }}</h6>
                                                                            <h6 class="menu-header-subtitle">
                                                                                <a href="{{ url('/admin/chats?room_no=Claim-waranty-' . $garansi->order->code) }}" class="btn btn-success btn-sm"><i class="fa fa-envelope"></i> Chat</a>
                                                                            </h6>
                                                                            @if($garansi->customer_can_reply == 0)
                                                                            <br>
                                                                            <div class="alert alert-danger fade show" role="alert">Please Accept Claim to start chat with technician</div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                        <label>Schedule:</label>
                                        <p id="schedule_display">{{ $garansi->order->schedule->format('d F Y G:i') }}</p>
                                        <hr />

                                        <label>Symptom detail:</label>
                                        <p>{{ $garansi->order->symptom_detail }}</p>
                                        <hr />

                                        <label for="ms_symptom_code">Symptom Code</label>
                                        <p>{{ $garansi->order->symptom_code == null ? '' : $garansi->order->symptom_code->name }}</p>
                                        <hr />

                                        <label for="ms_symptom_code">Repair Code</label>
                                        <p>{{ $garansi->order->repair_code == null ? '' : $garansi->order->repair_code->name }}</p>
                                        <hr />

                                        <label for="repair_desc">Repair Description</label>
                                        <p>{{ $garansi->order->repair_desc }}</p>
                                        <hr />

                                        <label>Payment Type:</label>
                                        <br />
                                        {!! $garansi->order->payment_type == 0 ? '<div class="badge badge-danger">Offline</div>' : '<div class="badge badge-info">Online</div>' !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End of Discussions -->
                    </div>
                </div>
            </div><!-- Sidebar -->

            <div class="main bg" id="chat-dialog">
                <div class="bg-image" style="background-image: url(https://wpkixx.com/html/talkshak/dist/img/avatars/pattern2.jpg)"></div>
                <!-- Start of Babble -->
                <div class="babble tab-pane fade active show" id="list-chat" role="tabpanel" aria-labelledby="list-chat-list">
                    <!-- Start of Chat -->
                    <div class="chat" id="chat1">

                        <div class="top">
                            <div class="container">
                                <div class="col-md-12">
                                    <div class="inside">
                                        <div class="status online"></div>
                                        <div class="data">
                                            <h5><a target="__blank" href="{{ Auth::user()->hasRole('admin') ? url('/admin/order/service_detail/' . $garansi->order->id) : url('/customer/service_detail/' . $garansi->order->id) }}" class="chat-user-name">#{{ $garansi->order->code }}</a></h5>
                                            <span>{{ $garansi->order->order_status->name }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content" id="content">
                            <div class="container">
                                <div class="col-md-12" id="chat-content">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="col-md-12">
                                <div class="bottom form-chat-area">
                                    <form class="text-area" id="chat-form">
                                        <input type="hidden" value="{{ $garansi->id }}" name="garansi_chat_id">
                                        <textarea class="form-control text-message" name="message" placeholder="Start typing for reply..." rows="1"></textarea>
                                        <button type="submit" class="btn send"><i class="fa fa-send"></i></button>
                                    </form>
                                    <label>
                                        <input type="file" id="file-chat">
                                        <span class="btn attach"><i class="fa fa-paperclip"></i></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Chat -->
                </div>
                <!-- End of Babble -->
            </div>
        </div>
    </main>
</div>

<div class="modal fade" data-backdrop="false" id="modal-media" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;">
                <img class="media-img" src="" alt="" style="width: 50%; height: auto;">
                <input class="form-control media-message" name="message" placeholder="Start typing for reply...">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn send-media"><i class="fa fa-send"></i> Send</button>
                <button class="btn waves-effect" data-dismiss="modal" type="button"><i class="fa fa-close"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<!-- garansi -->
@if(in_array($garansi->order->orders_statuses_id, [10]))
<form id="form-garansi-job">
    <div class="modal fade" data-backdrop="false" id="modal-garansi-job" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Accept Garansi (Revisit)
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="garansi_ms_price_services_id" id="garansi_ms_price_services_id" />
                    <input type="hidden" name="garansi_id" id="garansi_id" value="{{ $garansi->id }}" />
                    <input type="hidden" name="garansi_service_detail_id" id="garansi_service_detail_id" value="{{ $garansi->order->detail->id }}">
                    <input type="hidden" value="{{ $garansi->order->detail->ms_services_types_id }}" name="teknisi_service_type_id" id="teknisi_service_type_id">
                    <label>SELECT TECHNICIAN</label>
                    <div class="form-group">
                        <select id="select-technicians" class="form-control" name="technician_id" style="width:100%" required>
                            <option value="" selected="selected">--Select--</option>
                        </select>
                    </div>

                    <div id="available_schedule">
                        <div class="position-relative form-group">
                            <label class="label-header" for="">
                                AVAILABLE SCHEDULE
                            </label>
                            <input style="padding: .375rem .75rem;font-size: 1rem;box-shadow: none;font-weight: 400;" class="form-control datetimepicker" name="schedule" type="text" id="schedule" readonly />
                        </div>
                        <div class="row" id="display_hours_available" style="padding:20px;"></div>
                    </div>

                    <div id="jam-area">
                        <label>Start Working at </label>
                        <div class="form-group">
                            <h4 id="start_at"></h4>
                        </div>
                        <div>
                            <label>Estimated working hours</label>
                            <div class="form-group">
                                <select class="est-h" id="estimation_hours" name="estimation_hours" style="width:100%" required>
                                    <option selected="" value=0>Select Duration (Hours)</option>
                                    <option value=1>1</option>
                                    <option value=2>2</option>
                                    <option value=3>3</option>
                                    <option value=4>4</option>
                                    <option value=5>5</option>
                                </select>
                            </div>
                        </div>
                        <label>End Hours at </label>
                        <div class="form-group">
                            <h4 id="end_at"></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SAVE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endif
@include('admin.customer.chats.style_uji')
@endsection

@section('script')
@include('admin.order.service_detail_style')
<style>
    button{
        margin-bottom: 5px;
    }
    .custom-control {
        margin: 10px;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js">
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" rel="stylesheet" />
<script>
    listChat();

    function readURL(input) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.media-img').attr('src', e.target.result);
            $('#modal-media').modal('show')
        }
        reader.readAsDataURL(input.files[0]);
    }

    function listChat() {
        Axios.get(Helper.apiUrl('/customer/garansi/list_chat/' + $('[name="garansi_chat_id"]').val()))
            .then(function(response) {
                $('#chat-content').append(response.data.data);
                $("#content").animate({
                    scrollTop: $('#content')[0].scrollHeight
                }, 1000);
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
    }

    function sendMsg() {
        data = Helper.serializeForm($('#chat-form'));

        var formData = new FormData();
        var attachment = document.querySelector('#file-chat');
        if (attachment.files[0]) {
            formData.append("attachment", attachment.files[0]);
        }
        formData.append("message", data.message);
        formData.append("garansi_chat_id", data.garansi_chat_id);

        // post data
        Axios.post(Helper.apiUrl('/customer/garansi/reply'), formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                $('#file-chat').val('');
                $('[name="message"]').val('');
                $('#chat-content').append(response.data.data);
                $("#content").animate({
                    scrollTop: $('#content')[0].scrollHeight
                }, 1000);
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
    }

    $('#file-chat').change(function() {
        readURL(this);
    })

    $('.send-media').click(function() {
        $('.text-message').val($('.media-message').val());
        sendMsg();
        $('#modal-media').modal('hide')
    })

    $("#chat-form")
        .submit(function(e) {
            sendMsg();
            e.preventDefault();
        })

    $("textarea[name='message']")
        .keydown(function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                sendMsg();
                return false;
            }
        })
</script>


@if($garansi->order->order_status->id == 10 && Auth::user()->hasRole('admin'))
<script>
    globalCRUD.select2('.est-h');

    $('#available_schedule').hide();
    $('#jam-area').hide();

    var TeknisiSelect = {
        initialize: function() {
            Helper.dateScheduleJob('#schedule');

            $('#select-technicians').select2({
                // minimumInputLength: 1,
                ajax: {
                    url: Helper.apiUrl('/admin/order/find_technicians'),
                    dataType: 'json',
                    delay: 250,
                    type: 'post',
                    data: function(params) {
                        return {
                            service_type_id: [$('#teknisi_service_type_id').val()],
                            // customer_id: $('teknisi-customer').val(),
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: $.map(data, TeknisiSelect.render(data))
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateSelection: function(data) {
                    return data.name;
                }
            });

            // change teknisi
            $(document).on('change', '#select-technicians', function() {
                value = $(this).val();
                teknisi = TeknisiSelect.find(value);
                $('#available_schedule').show();
                $('#jam-area').hide();
                $('#teknisi_ms_price_services_id').val(teknisi.data.price_services[0].id);
            });

            $(document).on('change', '#schedule', function() {
                $('#jam-area').show();
            });
        },

        data: function() {
            return $('#select-technicians').select2("data");
        },

        find: function(teknisi_id) {
            return _.find(TeknisiSelect.data(), function(data) {
                return data.id == teknisi_id;
            });
        },

        render: function(data) {
            return function(data) {
                var phone = data.user.phone != null ? data.user.phone : '-';
                var images = "<img src='" + data.user.avatar + "' />";
                var price_service = '';
                service_type_id = $('#select-service-type').val();

                price = _.find(data.price_services, function(row) {
                    return row.ms_services_types_id == service_type_id;
                });

                if (price) {
                    price_service += "<div class='select2-result-repository__description'><b>" + price.service_type.name + "</b> (" + Helper.toCurrency(price.value) + ")</div>";
                }

                return {
                    id: data.id,
                    text: "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__avatar'>" + images + "</div>" +
                        "<div class='select2-result-repository__meta'>" +
                        "<div class='select2-result-repository__title'><b>" + data.user.name + "</b></div>" +
                        "<div class='select2-result-repository__description'>Phone : <b>" + phone + "</b></div>" +
                        "<div class='select2-result-repository__description'>Email : <b>" + data.user.email + "</b></div>" +
                        price_service +
                        "</div></div>",
                    name: data.user.name + ' / ' + data.user.email,
                    data: data
                };
            };
        },
    }

    var jam_teknisi = [];
    var teknisi_id = null;
    Helper.dateScheduleJob('#schedule');

    $(document).on('change', "input[name='hours']", function() {
        if (this.checked) {
            $('#estimation_hours').prop('disabled', false);
            $('#start_at').html(moment($('#schedule').val() + ' ' + $("input[name='hours']:checked").val(), 'YYYY-MM-DD HH:mm').format('DD MMMM YYYY HH:mm'));
        }
    })

    $(document).on('change', '#estimation_hours', function() {
        if ($(this).val()) {
            var returned_endate = moment($('#start_at').html()).add($(this).val(), 'hours');
            console.log(returned_endate.format('H'));
            console.log(jam_teknisi)
            $('#end_at').html(returned_endate.format('HH:00'));
            if ($.inArray(parseInt(returned_endate.format('H')), jam_teknisi) !== -1) {
                return Helper.warningNotif("Cannot pick estimate hours. Cause at " + $('#end_at').html() + " in the technician schedule !");
            }
        }
    });

    TeknisiSelect.initialize();

    $(document).on('click', '.garansi-job', function() {
        $('#modal-garansi-job').modal('show')
    })

    $('#form-garansi-job')
        .submit(function(e) {
            Helper.loadingStart();
            var data = Helper.serializeForm($(this));
            Axios.put('/order/garansi_revisit/' + data.garansi_service_detail_id, data)
                .then(function(response) {
                    Helper.successNotif('Success Updated');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
            e.preventDefault();
        });

    $(document).on('click', '.customer-reply', function() {
        Helper.loadingStart();
        Axios.put('/order/garansi_customer_reply/' + $(this).attr('data-id'))
            .then(function(response) {
                Helper.successNotif('Success Updated');
                location.reload();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
    })

    $(document).on('click', '.garansi-reject', function() {
        Helper.loadingStart();
        Axios.put('/order/garansi_reject/' + $(this).attr('data-id'))
            .then(function(response) {
                Helper.successNotif('Success Updated');
                location.reload();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
    })
</script>
@endif
@endsection