@extends('admin.home')
@section('content')
<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header">
            {{ $title }}
            <div class="btn-actions-pane-right">
                <a href="{{ url('/customer/reviews/waiting') }}" class="btn btn-default">
                    Waiting For Review
                </a>
                <a href="{{ route('customer.reviews.done') }}" class="btn btn-focus">
                    My Review ({{ $order_count }})
                </a>
            </div>
        </div>
    </div>
</div>

@foreach ($orders as $order)
<div class="col-md-6 col-xl-6" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header header-border">
            {{ $order->code }}
        </div>
        <div class="card-body">
            <p>{{ $order->schedule->format('d F Y H:i:s') }}</p>
            <div class="user-profile">
                <img class="avatar" src="{{ $order->service_detail->first()->technician->user->avatar }}" alt="Ash" />
                <div class="username">{{ $order->service_detail->first()->technician->user->name }}</div>
                <div class="bio">
                    <p>{{ $order->service_detail->first()->technician->user->email }}</p> 
                    {!! $order->service_detail->first()->technician->getRatingStarUi() !!}
                    <br /> 
                    <span class="star">Review ({{ $order->service_detail->first()->technician->total_review }} user)</span>                                   
                </div>
            </div>
            <div class="form-area-review">
                @foreach (range(1, $order->rating_and_review->rating->value) as $star)
                    <span class="star"><i class="fa fa-star"></i></span>
                @endforeach
                @if( (5 - $order->rating_and_review->rating->value) > 0 )
                    @foreach (range(1, 5 - $order->rating_and_review->rating->value) as $star)
                        <span class="star"><i class="fa fa-star-o"></i></span>
                    @endforeach
                @endif   
               <p>{{ $order->rating_and_review->review->description }}</p>
            </div>
        </div>
    </div>
</div>
@endforeach


<style>
    .user-profile {
        box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
        margin: auto;
        height: 8em;
        background: #fff;
        border-radius: .3em;
        margin-bottom: 20px;
    }

    .user-profile .username {
        margin: auto;
        margin-top: -4.40em;
        margin-left: 5.80em;
        color: #658585;
        font-size: 1.53em;
        font-family: "Coda", sans-serif;
        font-weight: bold;
    }

    .user-profile .bio {
        margin: auto;
        display: inline-block;
        margin-left: 10.43em;
        margin-bottom: 10px;
        color: #e76043;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }

    .user-profile>.description {
        margin: auto;
        margin-top: 1.35em;
        margin-right: 4.43em;
        width: 14em;
        color: #c0c5c5;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }
     .user-profile>img.avatar {
        padding: .7em;
        margin-left: .3em;
        margin-top: .3em;
        height: 6.23em;
        width: 6.23em;
        border-radius: 18em;
    }
</style>
@endsection

@section('script')

@endsection
