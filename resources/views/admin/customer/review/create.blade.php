@extends('admin.home')
@section('content')

<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header">
            {{ $title }}
        </div>
        <div class="card-body">
            <div class="user-profile">
                <img class="avatar" src="{{ $order->service_detail->first()->technician->user->info->avatar }}" alt="Ash" />
                <div class="username">{{ $order->service_detail->first()->technician->user->name }}</div>
                <div class="bio">
                    <p>{{ $order->service_detail->first()->technician->user->email }}</p>   
                    <p>
                        {{ $order->service->name }}
                        <br />
                        {{ $order->symptom->name }}
                    </p>                 
                </div>
            </div>
            <form id="form-review">
                <input id="rtng-str" name="star" type="hidden" value="{{ $order->rating_and_review_id != null ? $order->rating_and_review->rating->value : 0}}"/>
                <div class="main">
                    @if($order->rating_and_review_id == null)
                        @foreach (range(1, 5) as $star)
                            <div class="star-border">
                                <div class="star">
                                </div>
                            </div>
                        @endforeach
                    @else
                        @foreach (range(1, $order->rating_and_review->rating->value) as $star)
                            <div class="star-border">
                                <div class="star" style="background: gold">
                                </div>
                            </div>
                        @endforeach
                        @if( (5 - $order->rating_and_review->rating->value) > 0 )
                            @foreach (range(1, 5 - $order->rating_and_review->rating->value) as $star)
                                <div class="star-border">
                                    <div class="star">
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    @endif
                    <div class="debug">
                    </div>
                </div>
                <div class="position-relative form-group">
                    <input type="hidden" name="technician_id" value="{{ $order->service_detail->first()->technician->id }}" />
                    <input type="hidden" name="order_id" value="{{ $order->id }}" />
                    <textarea class="form-control" cols="5" name="note" placeholder="Message" rows="5" type="text" required>{{ $order->rating_and_review_id != null ? $order->rating_and_review->review->description : ''}}</textarea>
                </div>
                <br/>
                <button class="btn btn-success" type="submit">
                    SUBMIT
                </button>
            </form>
        </div>
    </div>
</div>

<style>
    .star{
        width:50px;
        height:50px;
        -webkit-clip-path: polygon(50% 0%, 61% 35%, 98% 35%, 68% 57%, 79% 91%, 50% 70%, 21% 91%, 32% 57%, 2% 35%, 39% 35%);
      clip-path: polygon(50% 0%, 61% 35%, 98% 35%, 68% 57%, 79% 91%, 50% 70%, 21% 91%, 32% 57%, 2% 35%, 39% 35%);
        background:white;
    }
    .star-border{
        filter: drop-shadow(0px 0px 4px black);
        display:inline-block;
    }
    .star-border:hover{
        cursor:pointer;
    }
    .rounded-circle{
        width: 60px;
        height: auto;
    }
    .card .widget-content {
        border: 1px solid #c1c3c5;
    }
    .widget-content-wrapper{
        margin: 10px;
    }
    .rating-block{
      background-color:#FAFAFA;
      border:1px solid #EFEFEF;
      padding:15px 15px 20px 15px;
      border-radius:3px;
    }
</style>
<style>
    .user-profile {
        box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
        margin: auto;
        height: 8em;
        background: #fff;
        border-radius: .3em;
        margin-bottom: 20px;
    }

    .user-profile .username {
        margin: auto;
        margin-top: -4.40em;
        margin-left: 5.80em;
        color: #658585;
        font-size: 1.53em;
        font-family: "Coda", sans-serif;
        font-weight: bold;
    }

    .user-profile .bio {
        margin: auto;
        display: inline-block;
        margin-left: 10.43em;
        color: #e76043;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }

    .user-profile>.description {
        margin: auto;
        margin-top: 1.35em;
        margin-right: 4.43em;
        width: 14em;
        color: #c0c5c5;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }
     .user-profile>img.avatar {
        padding: .7em;
        margin-left: .3em;
        margin-top: .3em;
        height: 6.23em;
        width: 6.23em;
        border-radius: 18em;
    }
</style>
@endsection

@section('script')
<script>
    $( document ).ready(function() {
        var set = 0;
        $(".star-border").click(function(){
            $(this).find(".star").css("background","gold");
            $(this).prevAll().find(".star").css("background","gold");
            $(this).nextAll().find(".star").css("background","white");
            set = $(this).prevAll().length + 1;
            $('#rtng-str').val(set);
            console.log(set);
        }); 
    });

    $('#form-review').submit(function(e){
        data = Helper.serializeForm($(this));
        Helper.loadingStart();
          Axios.post('/customer/review', data)
            .then(function(response) {
                // send notif
                Helper.successNotif(response.data.msg);
                // redirect 
                Helper.redirectTo('/customer/my-review')
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)                    
            });
        e.preventDefault();
    })
</script>
@endsection
