@if(count($sparepart_details) > 0 || count($item_details) > 0)
<div class="card header-border">
    <div class="card-body">
        <div class="table-responsive">
            <table class="text-nowrap table-lg mb-0 table table-hover">
                <tr>
                    <td>Name</td>
                    <td>Unit</td>
                    <th>Price</th>
                </tr>
                <tbody>
                    @foreach($sparepart_details as $sparepart_detail)
                    <tr>
                        <td>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">{{ $sparepart_detail->name_sparepart }}</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="text-left">{{ $sparepart_detail->priceThousandSeparator() }} x {{ $sparepart_detail->quantity }}</td>
                        <th>
                            Rp. {{ \App\Helpers\thousanSparator($sparepart_detail->quantity * $sparepart_detail->price) }}
                        </th>
                    </tr>
                    @endforeach
                    @foreach($item_details as $item)
                    <tr>
                        <td>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">{{ $item->name_product }}</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="text-left">{{ $item->price }} x {{ $item->quantity }}</td>
                        <td>
                            Rp. {{ \App\Helpers\thousanSparator($item->quantity * $item->price) }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif