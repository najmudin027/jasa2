@extends('admin.home')
@section('content')
<div class="col-md-12" hidden>
    <div class="card header-border">
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-6">
                        {{-- <div class="position-relative form-group"> --}}
                            <label for="" class="">Status Order</label>
                            <select class="form-control" name="type_id" id="type" style="width:100%" >
                                <option value="">All</option>
                                <option value="2">Awaiting Jobs</option>
                                <option value="4">On Working</option>
                                <option value="8">Success Payment</option>
                                <option value="9">Processing</option>
                                <option value="7">Job Done</option>
                                <option value="11">Job Completed</option>
                                <option value="5">Cancel</option>
                                <option value="6">Reschedule</option>
                            </select>
                        {{-- </div> --}}
                    </div>
                    <div class="col-md-12">
                        <h4 style="color: white">asd</h4>
                        {{-- <button class="btn btn-danger" data-id="9"  id="search" type="submit">Search</button> --}}
                        <button class="btn btn-success" id="myButton" type="button"><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%; white-space: nowrap;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Orders Code</th>
                            <th>Create Date</th>
                            <th>Service</th>
                            <th>Technician Name</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="card-footer">
            <a href="{{ url('/customer/create-request-job') }}" class="btn btn-primary"><i class="fa fa-search"></i> Create Request Jobs</a>
        </div>
    </div>
</div>

<!-- modal less balance -->
{{-- <form id="forms-topup">
    <div class="modal fade bd-example-modal-lg" id="lessBalanceModal" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Topup Form</h5>
                    <input type="hidden" name="id" id="data_id">
                    <input type="hidden" name="selisih" id="data_selisih">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div class="modal-body">
                        <table class="table table-hover table-bordered" style="width:100%">
                            <thead>
                                <th>Service</th>
                                <th>Symptom</th>
                                <th>Total</th>
                            </thead>
                           <tbody>
                               <tr>
                                   <td id="getService"></td>
                                   <td id="getSymptom"></td>
                                   <td id="getTotal"></td>
                               </tr>
                           </tbody>
                        </table><hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-2"><strong>Note : </strong></div>
                                    <div class="col-md-10">Your wallet is not balanced with the grand total because there are some additional parts, please top up the form below</div>
                                </div>
                            </div>
                        </div><hr>
                        <div id="inputs">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="10000" /> Rp.10,000</div>
                                        <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="50000" /> Rp.50,000</div>
                                        <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="200000" /> Rp.200,000</div>
                                        <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="500000" /> Rp.500,000</div>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Nominal Top up</label>
                            <input type="number" class="form-control" name="nominal" placeholder="10000" id="amount"/>
                            <input type="hidden" class="form-control" id="exampleFormControlInput1" name="note" placeholder="10000">
                            <small><strong>Note : </strong> You must top up by Rp. </small>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary waves-effect" type="submit">
                        Top up
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal fade bd-example-modal-lg" id="confrimChangeParts" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmation Extra parts</h5>
                <input type="hidden" name="id" id="data_id">
                <input type="hidden" name="selisih" id="data_selisih">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover table-bordered" style="width:100%">
                    <thead>
                        <th>Sparepart Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                    </thead>
                    <tbody id="sparepart"></tbody>
                    <tbody>
                        <tr>
                            <td colspan="2" align="right">Grand Total</td>
                            <td id="getTotals">Rp. </td>
                        </tr>
                    </tbody>
                </table><hr>
                <input type="hidden" name="id" id="getIds">
                <input type="hidden" id="amounts" name="getTotals">
                <div id="content_topup"></div>
            </div>
        </div>
    </div>
</div> --}}
<div class="modal fade bd-example-modal-lg" id="confrimChangeParts" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmation Extra parts</h5>
                <input type="hidden" name="id" id="data_id">
                <input type="hidden" name="selisih" id="data_selisih">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover table-bordered additional_parts" style="width:100%">
                    <thead>
                        <th>Sparepart Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                    </thead>
                    <tbody id="sparepart"></tbody>
                    <tbody>
                        <tr>
                            <td colspan="2" align="right">Grand Total</td>
                            <td id="getTotals">Rp. </td>
                        </tr>
                    </tbody>
                </table>
                <hr id="line">
                {{-- <input type="text" class="nominalssss"> --}}
                <input type="hidden" name="id" id="getIds">
                <input type="hidden" id="amounts" name="getTotals">
                <div id="content_topup"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalComplaint" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Complaint Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="hidden" name="id" id="id_modal">
            <div class="modal-body">
                <!-- <div class="alert alert-danger msg_error" role="alert" id="alert_condition">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>You must be Agree to our term and condition </strong>
                </div> -->
                <div>
                    <label for="" class="">Status Order</label>
                    <input type="text" name="orders_statuses_id" value="Complaint" class="form-control" readonly>
                </div>
                <br>
                <div>
                    <label for="" class="">Note <span class="required" style="color:red">*</span></label>
                    <textarea class="form-control" name="note_complaint" id="note_complaint" rows="3" required></textarea>
                    <span id="error-note_complaint"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-id="" class="complaint btn btn-primary">Complaint</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document" style="overflow-y: initial !important">
        <div class="modal-content">
            <input type="hidden" id="completed_jobs" name="completed_jobs">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">{{ $getTitle->value ?? '' }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height: 250px;overflow-y: auto;">
                {!! $getDesc->value ?? '' !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-sm approve"  style="border: none">Complete Jobs</button>
            </div>
        </div>
    </div>
</div>
@endsection


@section('script')
<style>
    <style>

    /* Tooltip container */
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
        /* If you want dots under the hoverable text */
    }

    /* Tooltip text */
    .tooltip .tooltiptext {
        visibility: hidden;
        width: 120px;
        background-color: grey;
        color: black;
        text-align: center;
        padding: 5px 0;
        border-radius: 6px;

        /* Position the tooltip text - see examples below! */
        position: absolute;
        z-index: 1;
    }

    /* Show the tooltip text when you mouse over the tooltip container */
    .tooltip:hover .tooltiptext {
        visibility: visible;
    }
</style>



<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{-- <script>
    $('.radiogroup').change(function(e){
        var selectedValue = $(this).val();
        $('#amount').val(selectedValue)
    });

    $('#forms-topup').submit(function(e){
        data = Helper.serializeForm($(this));
        Helper.loadingStart();
        Axios.post(Helper.apiUrl('/customer/topup-wallet/less-balance-saved'), data)
            .then(function(response) {
                Helper.successNotif('Success, Top up Success, Pleace Confirmation your Payment');
                window.location.href = Helper.redirectUrl('/customer/request_list');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
      e.preventDefault();
    })

    var $modal = $('#lessBalanceModal').modal({
        show: false
    });

    var ProductAdditionalObj = {
        // isi field input
        isiDataFormModal: function(id) {
            $.ajax({
                url: Helper.apiUrl('/customer/less-balance/show/' + id),
                type: 'get',
                success: function(resp) {
                    console.log(resp)
                    $("#getService").text(resp.data.service_type_name);
                    $("#getSymptom").text(resp.data.symptom_name);
                    $("#getTotal").text(resp.data.price);
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            })

        },
        // hadle ketika response sukses
        successHandle: function(resp) {
            // send notif
            Helper.successNotif(resp.msg);
            $modal.modal('hide');
        },
    }

    $(document)
        .on('click', '#btn_less_balance', function() {
            id = $(this).attr('data-id');
            ProductAdditionalObj.isiDataFormModal(id);
            $modal.modal('show');
        })
</script> --}}

{{-- tampilkan modal perubahan part --}}
<script>
    $('.radiogroup').change(function(e) {
        var selectedValue = $(this).val();
        $('#amount').val(selectedValue)
    });



    var $modal = $('#lessBalanceModal').modal({
        show: false
    });

    var ProductAdditionalObj = {
        // isi field input
        isiDataFormModal: function(id) {
            $.ajax({
                url: Helper.apiUrl('/customer/less-balance/show/' + id),
                type: 'get',
                success: function(resp) {
                    console.log(resp)
                    $("#getService").text(resp.data.service_type_name);
                    $("#getSymptom").text(resp.data.symptom_name);
                    $("#getTotal").text(resp.data.price);
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            })

        },
        // hadle ketika response sukses
        successHandle: function(resp) {
            // send notif
            Helper.successNotif(resp.msg);
            $modal.modal('hide');
        },
    }

    $(document)
        .on('click', '#btn_less_balance', function() {
            id = $(this).attr('data-id');
            ProductAdditionalObj.isiDataFormModal(id);
            $modal.modal('show');
        })
</script>

<script>
    // $('#topup_lessbalance').on('submit', function(e){
    //     alert('asddasdas');
    //     data = Helper.serializeForm($(this));

    //     Helper.loadingStart();
    //     Axios.post(Helper.apiUrl('/customer/topup-wallet/less-balance-saved'), data)
    //         .then(function(response) {
    //             Helper.successNotif('Success, Top up Success, Pleace Confirmation your Payment');
    //             window.location.href = Helper.redirectUrl('/customer/topup/checkout/' + response.data.data.id);
    //         })
    //         .catch(function(error) {
    //             Helper.handleErrorResponse(error)
    //         });
    //   e.preventDefault();
    // });

    $(document).on('click', '#topup_lessbalance', function() {
        Helper.loadingStart();
        $.ajax({
            url: Helper.apiUrl('/customer/topup-wallet/less-balance-saved'),
            type: 'post',
            data: {
                'nominal': $('input[name="nominal"]').val()
            },
            success: function(resp) {
                console.log(resp)
                window.location.href = Helper.redirectUrl('/customer/topup/checkout/' + resp.data.id);
            },
            error: function(res, xhr, status, error) {
                Helper.errorNotif(error);
                console.log(res);
                Helper.loadingStop();
            },
        })

    });


    var $modal = $('#confrimChangeParts').modal({
        show: false
    });



    var ProductAdditionalObj = {
        // isi field input
        isiDataFormModal: function(id) {
            $.ajax({
                url: Helper.apiUrl('/customer/change_additional_parts/show/' + id),
                type: 'get',
                success: function(resp) {
                    // alert(resp.data.orders_statuses_id)
                    console.log(total_price)
                    $("#sparepart").html('');
                    $("#content_topup").html('');
                    $("#getTotals").html('');
                    $("#amounts").html('');
                    var content = '';
                    var templateTopup = '';
                    var templateApprove = '';
                    var total_price = 0;
                    var getId = 0;
                    var get_total_topup = 0;
                    var get_nominal = 0;
                    var sparepart_total = 0;
                    var tmp_item_price = 0;
                    if(resp.data.orders_statuses_id != 4){
                        if(resp.data.sparepart_detail != ''){
                            _.each(resp.data.sparepart_detail, function(detail) {
                                total_price += parseInt(detail.price) * parseInt(detail.quantity);
                                get_nominal = parseInt(total_price) - parseInt(resp.data.user.masterwallet.nominal);
                                content += `<tr>
                                                <td>${detail.name_sparepart}</td>
                                                <td>${detail.quantity}</td>
                                                <td>Rp.${Helper.thousandsSeparators(detail.price * detail.quantity)}</td>
                                            </tr>`;
                            });
                        }else if(resp.data.item_detail != ''){
                            _.each(resp.data.item_detail, function(detail) {
                                total_price += parseInt(detail.price) * parseInt(detail.quantity);
                                get_nominal = parseInt(total_price) - parseInt(resp.data.user.masterwallet.nominal);
                                getId = detail.orders_id;
                                content += `<tr>
                                                <td>${detail.name_product}</td>
                                                <td>${detail.quantity}</td>
                                                <td>Rp.${Helper.thousandsSeparators(detail.price * detail.quantity)}</td>
                                            </tr>`;
                            });
                        }

                        if(resp.data.user.masterwallet !== null){
                            if(resp.data.user.masterwallet.nominal < total_price){
                                console.log(resp.data.user.masterwallet.nominal)
                                console.log(total_price)

                                templateTopup +=   `<div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-2"><strong>Note : </strong></div>
                                                                    <div class="col-md-10">Your wallet is not balanced with the grand total because there are some additional parts, please top up the form below</div>
                                                                </div>
                                                            </div>
                                                        </div><hr>
                                                        <div class="form-group">
                                                            <label for="exampleFormControlInput1">Nominal Top up</label>
                                                            <input type="number" class="form-control" name="nominal" placeholder="${get_nominal}" id="amounts1" value="${get_nominal}"/>
                                                            <input type="hidden" class="form-control" id="exampleFormControlInput1" name="note" placeholder="10000">
                                                            <small><strong>Note : </strong> You must top up by Rp. ${Helper.thousandsSeparators(get_nominal)}</small>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button class="btn btn-primary waves-effect" id="topup_lessbalance" type="submit">
                                                                Top up
                                                            </button>
                                                        </div>`;
                            }else if(resp.data.user.masterwallet.nominal >= total_price){

                                templateTopup +=   `<div class="row">
                                                        <div class="col-md-12">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-2"><strong>Note : </strong></div>
                                                                    <div class="col-md-10">if you confirm the addition of this part your wallet will automatically be reduced according to the price listed above</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button  class="btn btn-primary waves-effect"  type="submit" id="btn_accpet_change_part">
                                                            Confirmation Change
                                                        </button>
                                                    </div>`;
                            }
                        }

                    }else{
                        if(resp.data.tmp_sparepart != ''){
                            // alert('asd');
                            _.each(resp.data.tmp_sparepart, function(detail) {
                                total_price += parseInt(detail.price) * parseInt(detail.quantity);
                                get_total_topup = parseInt(total_price) - parseInt(resp.data.user.masterwallet.nominal);
                                getId = detail.orders_id;
                                content += `<tr>
                                                <td>${detail.name_sparepart}</td>
                                                <td>${detail.quantity}</td>
                                                <td>Rp.${Helper.thousandsSeparators(detail.price * detail.quantity)}</td>
                                            </tr>`;
                            });
                        }else if(resp.data.tmp_item_detail != ''){
                            _.each(resp.data.tmp_item_detail, function(detail) {
                                total_price += parseInt(detail.price) * parseInt(detail.quantity);
                                get_total_topup = parseInt(total_price) - parseInt(resp.data.user.masterwallet.nominal);
                                getId = detail.orders_id;
                                content += `<tr>
                                                <td>${detail.name_product}</td>
                                                <td>${detail.quantity}</td>
                                                <td>Rp.${Helper.thousandsSeparators(detail.price * detail.quantity)}</td>
                                            </tr>`;
                            });
                        }

                        if(resp.data.user.masterwallet !== null){
                            if(resp.data.user.masterwallet.nominal < total_price){
                                console.log('bawah kurang dari')
                                templateTopup +=   `<div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-2"><strong>Note : </strong></div>
                                                                    <div class="col-md-10">Your wallet is not balanced with the grand total because there are some additional parts, please top up the form below</div>
                                                                </div>
                                                            </div>
                                                        </div><hr>
                                                        <div class="form-group">
                                                            <label for="exampleFormControlInput1">Nominal Top up</label>
                                                            <input type="number" class="form-control" name="nominal" placeholder="${get_total_topup}" id="amounts" value="${get_total_topup}"/>
                                                            <input type="hidden" class="form-control" id="exampleFormControlInput1" name="note" placeholder="10000">
                                                            <small><strong>Note : </strong> You must top up by Rp. ${Helper.thousandsSeparators(get_total_topup)}</small>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button class="btn btn-primary waves-effect" id="topup_lessbalance" type="submit">
                                                                Top up
                                                            </button>
                                                        </div>`;
                            }else if(resp.data.user.masterwallet.nominal >= total_price){
                                console.log('bawah lebih dari')
                                templateTopup +=   `<div class="row">
                                                        <div class="col-md-12">
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col-md-2"><strong>Note : </strong></div>
                                                                    <div class="col-md-10">if you confirm the addition of this part your wallet will automatically be reduced according to the price listed above</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button  class="btn btn-primary waves-effect"  type="submit" id="btn_accpet_change_part">
                                                            Confirmation Change
                                                        </button>
                                                    </div>`;
                            }
                        }
                    }



                    // alert(getId)

                    $('#getTotals').append(Helper.thousandsSeparators(total_price + sparepart_total + tmp_item_price))
                    $('#amounts').val(get_total_topup)
                    $('#amounts1').val(get_nominal)
                    $("#sparepart").append(content);
                    $("#content_topup").append(templateTopup);
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            })

        },
        // hadle ketika response sukses
        successHandle: function(resp) {
            // send notif
            Helper.successNotif(resp.msg);
            $modal.modal('hide');
        },
    }

    $(document)
        .on('click', '#btn_confrim_part', function() {
            id = $(this).attr('data-id');
            $('#getIds').val(id)
            $('.additional_parts').show();
            console.log(ProductAdditionalObj.isiDataFormModal(id));
            $modal.modal('show');
        })

    $(document).on('click', '#btn_accpet_change_part', function() {
        id = $('#getIds').val();
        Helper.loadingStart();
        $.ajax({
            url: Helper.apiUrl('/order/update_changed_sparepart_on_working/' + id),
            type: 'post',
            success: function(resp) {
                console.log(resp)
                Helper.successNotif('Cancel This Order Success !');
                Helper.loadingStop();
                Helper.redirectTo('/customer/dashboard');
            },
            error: function(res, xhr, status, error) {
                Helper.errorNotif('Something Went Wrong');
                console.log(res);
                Helper.loadingStop();
            },
        })

    });
</script>

<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    $("#myButton").click(function(e) {
        var type = [];
        $.each($("#type option:selected"), function() {
            type.push($(this).val());
        });
        if (type != '') {
            init(type);
        } else {
            init($(this).val());
        }
    });
    $('#myButton').trigger('click');

    function init(type_id) {
        var tableOrder = $('#table-list-orders').DataTable({
            processing: true,
            serverSide: true,
            select: true,
            dom: 'Bflrtip',
            ordering: 'true',
            order: [2, 'desc'],
            responsive: false,
            language: {
            buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/customer/request-job/datatables'),
                type: "post",
            },
            columns: [{
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                    sortable: false,
                },
                {
                    data: "code",
                    name: "code",
                    render: function(data, type, full) {
                        link = "<a href='" + Helper.url('/customer/service_detail/' + full.id) + "'># " + full.code + "</a>";

                        if (full.customer_read_at == null) {
                            link += '</br><span class="mb-2 mr-2 badge badge-pill badge-warning">NEW</span>';
                        }

                        // if (full.countdown_autocancel != null) {
                        //     $(document).ready(function() {
                        //         $('#clock' + full.id + '').countdown(full.countdown_autocancel, function(event) {
                        //             $(this).html(event.strftime('%D Days (%H Hour : %M Min)'));
                        //         });
                        //     });
                        // }

                        // if (!$.inArray(full.orders_statuses_id, [3])) {
                        //     link += "<div class='badge badge-danger ml-2' data-toggle='tooltip' data-placement='top' title='this countdown for auto cancel order'><span id='clock" + full.id + "'></span></div>";
                        // }

                        return link;
                    }
                },
                {
                    data: "created_at",
                    name: "created_at",
                    render: function(data, type, full) {
                        return full.created_at == null ? '-' : moment(full.created_at).format("DD MMMM YYYY hh:mm");
                    }
                },
                {
                    // Service ;
                    render: function(data, type, full) {
                        return full.symptom.name + "<br> <small>" + full.symptom.name  + "&nbsp;" + full.product_group.name + "</small>";
                    }
                },
                {
                    // Technician Name ;
                    render: function(data, type, full) {
                        service_detail = full.service_detail[0];
                        return '<img style="box-shadow: 0 1px 3px #694a4a;width: 50px;height: 50px; border-radius: 50%;" src="'+service_detail.technician.user.avatar+'"> <span style="margin-left: 10px;">'+service_detail.technician.name+'</span>';
                    }
                },
                {
                    data: "grand_total",
                    name: "grand_total",
                    render: $.fn.dataTable.render.number(',', '.,', 0, 'Rp. ')
                },
                {
                    data: "orders_statuses_id",
                    name: "orders_statuses_id",
                    render: function(data, type, full) {
                        total_price = 0;

                        _.each(full.service_detail, function(detail) {
                            total_price += parseInt(detail.price)
                        })

                        if (full.user.masterwallet !== null) {
                            hasil = parseInt(full.user.masterwallet.nominal) + parseInt(total_price)
                        } else {
                            hasil = 0 + parseInt(total_price)
                        }

                            var content = '';
                            if (full.orders_statuses_id === 2) {
                                content = '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                            } else if (full.orders_statuses_id === 3) {
                                if (hasil < parseInt(full.grand_total) && full.is_less_balance !== null) {
                                    content = '<span class="badge badge-warning">' + full.order_status.name + '</span><br><span class="badge badge-danger">New Extra Parts</span>';
                                } else {
                                    content = '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                                }
                            } else if (full.orders_statuses_id === 4) {
                                if (hasil < parseInt(full.grand_total) && full.is_less_balance !== null) {
                                    content = '<span class="badge badge-primary">' + full.order_status.name + '</span><br><span class="badge badge-danger">New Extra Parts</span>';
                                } else {
                                    content = '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                                }
                            } else if (full.orders_statuses_id === 5) {
                                content = '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                            } else if (full.orders_statuses_id === 6) {
                                content = '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                            } else if (full.orders_statuses_id === 7) {
                                content = '<span class="badge badge-focus">' + full.order_status.name + '</span>';
                            } else if (full.orders_statuses_id === 8) {
                                content = '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                            } else if (full.orders_statuses_id === 9) {
                                content = '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                            } else if (full.orders_statuses_id === 10) {
                                content = '<span class="badge badge-success">' + full.order_status.name + '</span>';
                            } else if (full.orders_statuses_id === 11) {
                                content = '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                            }

                            if (full.countdown_autocancel != null) {
                                $(document).ready(function() {
                                    $('#clock' + full.id + '').countdown(full.countdown_autocancel, function(event) {
                                        $(this).html(event.strftime('%D Days (%H Hour : %M Min)'));
                                    });
                                });

                                if (!$.inArray(full.orders_statuses_id, [2,3])) {
                                    content += "<br><div class='badge badge-danger' data-toggle='tooltip' data-placement='top' title='this countdown for auto cancel order'><span id='clock" + full.id + "'></span></div>";
                                }
                            }

                            if (full.countdown_autocompleted != null) {
                                $(document).ready(function() {
                                    $('#clock' + full.id + '').countdown(full.countdown_autocompleted, function(event) {
                                        $(this).html(event.strftime('%D Days (%H Hour : %M Min)'));
                                    });
                                });

                                if (!$.inArray(full.orders_statuses_id, [7])) {
                                    content += "<br><div class='badge badge-success' data-toggle='tooltip' data-placement='top' title='this countdown for auto completed order'><span id='clock" + full.id + "'></span></div>";
                                }
                            }

                            return content;
                        // }

                        // if(full.orderpayment !== null){
                        //     if(full.is_less_balance == '1' && full.orderpayment.orderhistory.type_transaction_id !== '6'){
                        //         return full.order_status.name + '<br/><span class="badge badge-danger">Less Balance</span>';
                        //     }else{
                        //         return  full.order_status.name + '<br/><span class="badge badge-warning">Waiting Admin Confirmation</span>';
                        //     }
                        // }else{
                        //     return full.order_status.name;
                        // }
                        // return text;
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        detail = "<a href='/customer/service_detail/" + full.id + "' class='btn btn-search btn-sm btn-success'  data-toggle='tooltip' data-html='true' title='<b>Detail</b>'><i class='fa fa-eye'></i></a>";
                        cancel = '<button class="btn_cancel_jobs btn btn-sm btn-danger" data-order ="' + full.id + '" data-toggle="tooltip" data-html="true" title="<b>Cancel Jobs</b>"><i class="fa fa-close"></i></button>';
                        accept = '<button class="btn_accepted_job btn btn-sm btn-primary" data-order-accept ="' + full.id + '" data-toggle="tooltip" data-html="true" title="<b>Accepted Jobs</b>"><i class="fa fa-check"></i></button>';
                        less = '<button type="button" data-id ="' + full.id + '" class="btn btn-warning btn-sm" id="btn_less_balance" data-toggle="tooltip" data-html="true" title="<b>Accepted Jobs</b>"><i class="fa fa-check"></i></button>';
                        canceled = '<button class="btn_cancel_is_approve btn btn-sm btn-danger" data-order ="' + full.id + '" data-toggle="tooltip" data-html="true" title="<b>Cancel Jobs</b>"><i class="fa fa-close"></i></button>';
                        btnModalConfrim = '<button type="button" data-id ="' + full.id + '" class="btn btn-warning btn-sm" id="btn_confrim_part" data-toggle="tooltip" data-html="true" title="<b>Accept Changes in Parts </b>"><i class="fa fa-check"></i></button>';
                        complaintList = "<a href='/customer/transacsion_list/complaint/detail/" + full.id + "' class='btn btn-search btn-sm btn-primary'  data-toggle='tooltip' data-html='true' title='<b>Complaint</b>'><i class='fa fa-comments'></i></a> ";
                        // completed = '<button type="button" class="btn btn-primary  btn-sm approve" data-id ="' + full.id + '" style="border: none" data-toggle="tooltip" data-html="true" title="<b>Complete Jobs</b>"><i class="fa fa-check"></i></button>';
                        completedConfirmation = '<a href="" type="button" data-completed_jobs ="' + full.id + '" class="btn btn-primary btn-sm append_id" data-toggle="modal" data-target="#exampleModalLong"><i class="fa fa-check"></i></a>';
                        complaint = '<button type="button" class="btn btn-danger  btn-sm btn_complaint" data-clik_button_complaint ="' + full.id + '" data-toggle="modal" data-target="#modalComplaint" style="border: none" data-toggle="tooltip" data-html="true" title="<b>Complaint Jobs</b>"><i class="fa fa-comments"></i></button>';

                        if (full.order_status.id == 8 || full.order_status.id == 5 || full.order_status.id == 6) {
                            return detail;
                        } else if (full.order_status.id == 3) {
                            total_price = 0;
                            _.each(full.service_detail, function(detail) {
                                total_price += parseInt(detail.price)
                            })

                            if (full.user.masterwallet !== null) {
                                hasil = parseInt(full.user.masterwallet.nominal) + parseInt(total_price)
                            } else {
                                hasil = 0 + parseInt(total_price)
                            }

                            if (hasil < parseInt(full.grand_total) && full.is_less_balance !== null) {
                                // return detail + ' ' + cancel + ' ' + less;
                                return btnModalConfrim + ' ' + cancel + ' ' + detail;
                            } else {
                                return accept + ' ' + cancel + ' ' + detail;
                            }
                            // total_price = 0;
                            // _.each(full.service_detail, function(detail) {
                            //     total_price += parseInt(detail.price)
                            // })
                            // _.each(full.sparepart_detail, function(detail) {
                            //     total_price += parseInt(detail.price) * parseInt(detail.quantity)
                            // })

                            // if (full.user.masterwallet !== null) {
                            // hasil = parseInt(full.user.masterwallet.nominal) + parseInt(total_price)
                            // } else {
                            //     hasil = 0 + parseInt(total_price)
                            // }

                            // if (full.user.masterwallet.nominal < hasil) {
                            //     return btnModalConfrim + ' ' + cancel + ' ' + detail;
                            // } else {
                            //     if (full.payment_type === 0) {
                            //         return accept + ' ' + detail;
                            //     } else {
                            //         return accept + ' ' + cancel + ' ' + detail;
                            //     }
                            // }
                        } else if (full.order_status.id == 9) {
                            if (full.payment_type === 1) {
                                if (full.is_approve == 1) {
                                    if(full.garansi == null){
                                        return canceled + ' ' + detail;
                                    }else{
                                        return detail
                                    }
                                } else {
                                    return detail;
                                }
                            } else {
                                return detail;
                            }
                        } else if (full.order_status.id == 11) {
                            return complaintList;
                        } else if (full.order_status.id == 10) {
                            return detail;
                        } else if (full.order_status.id == 2) {
                            return cancel + ' ' + detail;
                        } else if (full.order_status.id == 4) {
                            if (full.tmp_sparepart != '') {
                                return btnModalConfrim + ' ' + detail;
                            } else if (full.tmp_item_detail != '') {
                                return btnModalConfrim + ' ' + detail;
                            } else {
                                return detail;
                            }
                        } else if( full.order_status.id == 7){
                            var getId = 0;
                            $('#id_modal').val(full.id)
                            if(full.garansi == null){
                                return completedConfirmation + ' ' + complaint + ' ' + detail;
                            }else{
                                return completedConfirmation + ' ' + detail;
                            }
                        } else {
                            return "-";
                        }
                    }
                }
            ]
        });
    }

    $(document).on('click', '.append_id', function(){
        var idCompleteJob = $(this).attr('data-completed_jobs')
        // alert(idCompleteJob)
        $('#completed_jobs').val(idCompleteJob)
    })

    $(document).on('click', '.click_btn_complaint', function(){
        var idCompleteJob = $(this).attr('data-clik_button_complaint')
        // alert(idCompleteJob)
        $('#id_modal').val(idCompleteJob)
    })

    // cancel order sebelum di accept customer penalty (75K)
    $(document).on('click', '.btn_cancel_jobs', function(e) {
        id = $(this).attr('data-order');
        $.ajax({
            url: Helper.apiUrl('/customer/durasi/' + id),
            type: 'post',

            success: function(res) {
                Helper.confirm(function() {
                    Helper.loadingStart();
                    $.ajax({
                        url: Helper.apiUrl('/customer/cancel-jobs/' + id),
                        type: 'post',

                        success: function(res) {

                            Helper.successNotif('Cancel This Order Success !');
                            Helper.loadingStop();
                            Helper.redirectTo('/customer/dashboard');
                        },
                        error: function(res) {
                            Helper.errorNotif('Something Went Wrong');
                            console.log(res);
                            Helper.loadingStop();
                        }
                    })
                }, {
                    title: "Are You Sure",
                    message: "<strong>Are you sure </strong></br> If this order is canceled you will be charged a deduction fee for your balance of <strong>"+ (res.data.setting2 == 1 ? res.data.setting1 + "%" : "Rp." + Helper.thousandsSeparators(res.data.setting1))+"</strong> ",
                })
            },
            error: function(res) {
                Helper.errorNotif('Something Went Wrong');
                console.log(res);
                Helper.loadingStop();
            }
        })
        // {
        //     title: "Are You Sure",
        //     message: "<strong>Are you sure </strong></br> If this order is canceled you will be charged a deduction fee for your balance of <strong> IDR 70,000.00 </strong>, from the total service fee",
        // })

        e.preventDefault()
    })

    // cancel order sesudah di accept customer penalty (50%)
    $(document).on('click', '.btn_cancel_is_approve', function(e) {
        id = $(this).attr('data-order');
        $.ajax({
            url: Helper.apiUrl('/customer/durasi/' + id),
            type: 'post',

            success: function(res) {
                // alert(res.data.jam)

                Helper.confirm(function() {
                    Helper.loadingStart();
                    $.ajax({
                        url: Helper.apiUrl('/customer/cancel-jobs-is-approve/' + id),
                        type: 'post',

                        success: function(res) {
                            Helper.successNotif('Cancel This Order Success !');
                            Helper.loadingStop();
                            Helper.redirectTo('/customer/dashboard');
                        },
                        error: function(res) {
                            Helper.errorNotif('This service cannot be canceled because the mechanic will come to your place soon');
                            console.log(res);
                            Helper.loadingStop();
                        }
                    })
                }, {
                    title: "Are You Sure",
                    message: "<strong>Are you sure </strong></br> If this order is canceled you will be charged a deduction fee for your balance of <strong>" + (res.data.getSettingType == 1 ? res.data.getSettingValue + "%" : "Rp." + res.data.getSettingValue) + "</strong> ",
                    // from the total service fee, This offer can be canceled for up to <strong>" + res.data.jam + "</strong> hours
                })
            },
            error: function(res) {
                Helper.errorNotif('This service cannot be canceled because the mechanic will come to your place soon');
                console.log(res);
                Helper.loadingStop();
            }
        })


        e.preventDefault()
    })

    $(document).on('click', '.btn_accepted_job', function(e) {
        id = $(this).attr('data-order-accept');
        Helper.confirm(function() {
            Helper.loadingStart();
            $.ajax({
                url: Helper.apiUrl('/customer/accept-order/' + id),
                type: 'post',
                success: function(res) {
                    Helper.successNotif('Accepted Job Success !');
                    Helper.loadingStop();
                    Helper.redirectTo('/customer/dashboard');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        }, {
            title: "Are You Sure",
            message: "Are You Sure, you will continue this offer?",
        })

        e.preventDefault()
    })

    $(document).on('click', '.approve', function(e) {
        id = $('#completed_jobs').val();
        $('#exampleModal').modal('hide');
        // Helper.confirm(function(){
        Helper.loadingStart();
        $.ajax({
            url:Helper.apiUrl('/customer/approve/' + id ),
            type: 'post',
            data : {
                commission_value : $('input[name="commission_value"]').val(),
                early_total : $('input[name="early_total"]').val(),
                total : $('input[name="total"]').val(),
            },

            success: function(res) {
                Helper.successNotif('Success');
                Helper.loadingStop();
                Helper.redirectTo('/customer/request_list');
            },
            error: function(res) {
                alert("Something went wrong");
                console.log(res);
                Helper.loadingStop();
            }
        })
    })

    $(document).on('click', '.complaint', function(e) {
        id = $('#id_modal').val();
        // Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/customer/complaint/' + id ),
                type: 'post',
                data : {
                    note_complaint : $('textarea[name="note_complaint"]').val(),
                },

                success: function(res) {
                    Helper.successNotif('Success');
                    Helper.redirectTo('/customer/request_list');
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])

                        })
                        Helper.handleErrorResponse(error)
                    }
                            Helper.loadingStop();

                }
            })
        // })

        e.preventDefault()
    })

        // e.preventDefault()
    // })


    // $('.nominalssss').on('change keyup', function() {
    //     var sanitized = $(this).val().replace(/[^0-9]/g, '');
    //     $(this).val(sanitized);
    // });

    // $('.nominalsssss').on('change keyup', function() {
    //     var sanitized = $(this).val().replace(/[^0-9]/g, '');
    //     $(this).val(sanitized);
    // });
</script>

@endsection
