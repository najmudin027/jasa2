<div class="row" id="load" style="position: relative;">
    @forelse ($technicians as $technician)
        <div class="col-lg-3 col-md-3 col-sm-12" style="padding-left: 25px; padding-bottom: 25px;">
        <!-- <span>{{ $technician }}</span> -->
            <input type="checkbox" class="selected-technician teknisi-card-check-{{ $technician->id }}" teknisi-id="{{ $technician->id }}" total-prices="{{ $technician->total_prices }}" id="myCheckbox{{ $technician->id }}" name="teknisi" onclick="check(this)" hidden>
            <label for="myCheckbox{{ $technician->id }}" class="teknisi-card" data-id="{{ $technician->id }}">
                <div class="our-team">

                    <div class="profile-userpic text-center">
                        <div class="avatar-upload">
                            <div class="avatar-preview">
                                <div id="imagePreview" style="background-image: url( {{ $technician->user->avatar }} )"></div>
                            </div>
                        </div>
                    </div>

                    <br><br><br>
                    <div class="team-content">
                        <h3 class="name" style="font-size: 15px;text-transform: uppercase;font-weight: 800;margin-top: -30px;">{{ $technician->user->name }}</h3>
                        @if($technician->user->badge_id != null)
                            <div class="mb-2 mr-2 badge badge-pill badge-{{ $technician->user->badge->color }}">{{ $technician->user->badge->name }}</div>
                        @endif
                        <h4 class="title">Rp. {{ number_format($technician->total_service_price) }}</h4>
                        <h6 class="title font-weight-bold" style="font-size:10px; margin-top:10px;color:rgb(4, 170, 87);"><i class="fa fa-map-marker fa-lg"></i> {{ ($technician->user->address != null && $technician->user->address->city != null) ? $technician->user->address->city->name : '-' }}</h6>
                        <hr />
                        <table align="center">
                            <tr>
                                <td>
                                    <span class="point">({{ $technician->getFormatRatingValue() }})</span>
                                    {!! $technician->getRatingStarUi(
                                        '<span class="star">
                                            <i class="fa fa-star" style="color: gold">
                                            </i>
                                        </span>',
                                        '<span class="star">
                                            <i class="fa fa-star" style="color: black">
                                            </i>
                                        </span>'
                                    ) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    @if($technician->total_review > 0)
                                        <span class="amount">
                                            ({{ $technician->total_review }}) Reviews
                                        </span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <small>
                                        <strong> Order Complete {{ $technician->order_completes_count }}</strong>
                                     </small>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
               <span class="btn btn-dark btn-block btn-next-step-{{ $technician->id }} hide-button" data-id="{{ $technician->id }}" readonly> <i class="fa fa-check"></i> </span>
            </label>
        </div>
        <input type="hidden" name="teknisi_id" value="{{ $technician->id }}">
    @empty
    <div class="col-md-12 text-center">
        {{-- <img src="https://image.freepik.com/free-vector/404-error-page-found_41910-364.jpg" width="400px" height="400px"> --}}
        <br/>
        <br/>
        <br/>
        <h3>Sorry Technicians Not Available.</h3>
    </div>

    @endforelse
    <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:20px;">
        <div style="margin:20px;">
            {!! $technicians->links() !!}
        </div>
    </div>
</div>

<style>
                        .our-team:hover{
                            background-color: #007cfb1f;
                        }
                        .profile-userpic img {
                            float: none;
                            margin: 0 auto;
                            width: 50%;
                            -webkit-border-radius: 50% !important;
                            -moz-border-radius: 50% !important;
                            border-radius: 50% !important;
                        }

                        .avatar-upload {
                            position: relative;
                            max-width: 205px;
                            margin: 0px auto;
                            }
                            .avatar-upload .avatar-edit {
                            position: absolute;
                            right: 12px;
                            z-index: 1;
                            top: 10px;
                            }
                            .avatar-upload .avatar-edit input {
                            display: none;
                            }
                            .avatar-upload .avatar-edit input + label {
                            display: inline-block;
                            width: 34px;
                            height: 34px;
                            margin-bottom: 0;
                            border-radius: 100%;
                            background: #FFFFFF;
                            border: 1px solid transparent;
                            box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
                            cursor: pointer;
                            font-weight: normal;
                            transition: all 0.2s ease-in-out;
                            }
                            .avatar-upload .avatar-edit input + label:hover {
                            background: #f1f1f1;
                            border-color: #d6d6d6;
                            }
                            .avatar-upload .avatar-edit input + label:after {
                            content: "\f040";
                            font-family: 'FontAwesome';
                            color: #757575;
                            position: absolute;
                            top: 10px;
                            left: 0;
                            right: 0;
                            text-align: center;
                            margin: auto;
                            }
                            .avatar-upload .avatar-preview {
                            width: 120px;
                            height: 120px;
                            position: relative;
                            display: block;
                            margin-left: auto;
                            margin-right: auto;
                            border-radius: 100%;
                            border: 6px solid #F8F8F8;
                            box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
                            }
                            .avatar-upload .avatar-preview > div {
                            width: 100%;
                            height: 100%;
                            border-radius: 100%;
                            background-size: cover;
                            background-repeat: no-repeat;
                            background-position: center;
                            }
                    </style>
