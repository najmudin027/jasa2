<!-- List Technician CSS Script -->
<style>
    .list-group-item {
        user-select: none;
    }

    

    .list-group input[type="checkbox"] {
        display: none;
    }

    .list-group input[type="checkbox"] + .list-group-item {
        cursor: pointer;
    }

    .list-group input[type="checkbox"] + .list-group-item:before  {
        content: "\2713";
        color: transparent;
        font-weight: bold;
        margin-right: 1em;
    }

    .list-group input[type="checkbox"]:checked + .list-group-item  {
        background-color: #0275D8;
        color: #FFF;
    }

    .list-group input[type="checkbox"]:checked + .list-group-item:before  {
        color: inherit;
    }

    .list-group input[type="radio"] {
    display: none;
    }

    .list-group input[type="radio"] + .list-group-item {
    cursor: pointer;
    }

    .list-group input[type="radio"] + .list-group-item:before {
    color: transparent;
    font-weight: bold;
    margin-right: 1em;
    }

    .list-group input[type="radio"]:checked + .list-group-item {
    background-color: #0275D8;
    color: #FFF;
    }

    .list-group input[type="radio"]:checked + .list-group-item:before {
    color: inherit;
    }
</style>
<!-- Start Style from list technicians-->
<style>
.our-team {
    box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
    border-top: 2px solid #444054;
    padding: 30px 0 30px;
    margin-bottom: 10px;
    background-color: #fff;
    text-align: center;
    overflow: hidden;
    position: relative;
}

.our-team .picture {
    display: inline-block;
    height: 130px;
    width: 130px;
    margin-bottom: 50px;
    z-index: 1;
    position: relative;
}

.our-team .picture::before {
    content: "";
    width: 100%;
    height: 0;
    border-radius: 50%;
    background-color: #444054;
    position: absolute;
    bottom: 135%;
    right: 0;
    left: 0;
    opacity: 0.9;
    transform: scale(3);
    transition: all 0.3s linear 0s;
}

.our-team:hover .picture::before {
    height: 100%;
}

.our-team .picture::after {
    content: "";
    width: 100%;
    height: 100%;
    border-radius: 50%;
    background-color: #444054;
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
}

.our-team .picture img {
    width: 100%;
    height: auto;
    border-radius: 50%;
    transform: scale(1);
    transition: all 0.9s ease 0s;
}



.our-team .title {
    display: block;
    font-size: 15px;
    color: #4e5052;
    text-transform: capitalize;
}

.our-team .social {
    width: 100%;
    padding: 0;
    margin: 0;
    background-color: #444054;
    position: absolute;
    bottom: -100px;
    left: 0;
    transition: all 0.5s ease 0s;
}

.our-team:hover .social {
    bottom: 0;
}

.our-team .social li {
    display: inline-block;
}

.our-team .social li a {
    display: block;
    padding: 10px;
    font-size: 17px;
    color: white;
    transition: all 0.3s ease 0s;
    text-decoration: none;
}

.our-team .social li a:hover {
    color: #444054;
    background-color: #f7f5ec;
}
</style>

<style>
    body {
        font-family: 'Roboto Condensed', sans-serif;
        font-size: 14px;
        line-height: 1.42857143;
        color: #47525d;
        background-color: #fff;
    }

    hr {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
    }

    .jFiler {
        font-family: inherit;
    }

    .pagination {
        justify-content: center;
    }
</style>

<style>
    input[type="checkbox"][id^="myCheckbox"] {
        display: none;
    }

    label {
        border: 1px solid rgb(255, 255, 255);
        padding: 5px;
        display: block;
        position: relative;
        margin: 5px;
        cursor: pointer;
        border-radius: 10px;

    }

    label:before {
        background-color: white;
        color: white;
        content: " ";
        display: block;
        border-radius: 50%;
        border: 1px solid grey;
        position: absolute;
        top: -5px;
        left: -5px;
        width: 25px;
        height: 25px;
        text-align: center;
        line-height: 28px;
        transition-duration: 0.4s;
        transform: scale(0);
    }

    label img {
        
        transition-duration: 0.2s;
        transform-origin: 50% 50%;
    }

    :checked + label {
        border-color: #53CFD4;
        height: auto;

    }

    

    :checked + label img {
        transform: scale(0.9);

        z-index: -1;
    }
</style>
