@foreach ($viewGalery as $item)
    <div class="col-lg-3 col-md-4 col-xs-6 thumb" i>
        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
            data-image=""
            data-target="#image-gallery">
            <img class="img-thumbnail image "
                    src="{{asset('admin/storage/image/' . $item->filename)}}"
                    alt="Another alt text"
                    data-img = "{{ $item->id }}"
                    data-src="{{asset($item->filename)}}" style="width:450px;height:126px">
        </a>
    </div>
@endforeach 