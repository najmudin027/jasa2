@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Detail Data Technician
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                @if (Request::segment(1) == 'admin')
                    <a href="{{ url('/admin/home') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                        <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back
                    </a>
                @elseif(Request::segment(1) == 'customer')
                    <a href="{{ url('/customer/dashboard') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                        <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back
                    </a>
                @endif
            </div>
        </div>
        <div class="card-body">
            <form action="" method="post" id="form-address-create">
                <div class="row">
                    <div class="col-md-12">
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">First Name </label>
                            <div class="col-sm-10">
                                @if ($updateTeknisi->teknisidetail != null)
                                    <input name="no_identity" id="examplename" placeholder="0012XXX" value="{{ $updateTeknisi->info->first_name }}" type="text" class="form-control" readonly>
                                @else
                                    <input name="no_identity" id="examplename" placeholder="First Name Empty" value="" type="text" class="form-control" readonly>
                                @endif
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Last Name</label>
                            <div class="col-sm-10">
                                @if ($updateTeknisi->teknisidetail != null)
                                    <input name="no_identity" id="examplename" placeholder="0012XXX" value="{{ $updateTeknisi->info->last_name }}" type="text" class="form-control" readonly>
                                @else
                                    <input name="no_identity" id="examplename" placeholder="Last Name Empty" value="" type="text" class="form-control" readonly>
                                @endif
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Date Of Birth </label>
                            <div class="col-sm-10">
                                @if ($updateTeknisi->teknisidetail != null)
                                    <input name="no_identity" id="examplename" placeholder="0012XXX" value="{{ date('d M Y', strtotime($updateTeknisi->info->date_of_birth)) }}" type="text" class="form-control" readonly>
                                @else
                                    <input name="no_identity" id="examplename" placeholder="Date Of Birth Empty" value="" type="text" class="form-control" readonly>
                                @endif
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Marital</label>
                            <div class="col-sm-10">
                                @if ($updateTeknisi->teknisidetail != null)
                                    <input name="no_identity" id="examplename" placeholder="0012XXX" value="{{ $updateTeknisi->info->marital->status }}" type="text" class="form-control" readonly>
                                @else
                                    <input name="no_identity" id="examplename" placeholder="Marital Empty" value="" type="text" class="form-control" readonly>
                                @endif
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Religion </label>
                            <div class="col-sm-10">
                                @if ($updateTeknisi->teknisidetail != null)
                                    <input name="no_identity" id="examplename" placeholder="0012XXX" value="{{ $updateTeknisi->info->religion->name }}" type="text" class="form-control" readonly>
                                @else
                                    <input name="no_identity" id="examplename" placeholder="Religion Empty" value="" type="text" class="form-control" readonly>
                                @endif
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Gender</label>
                            <div class="col-sm-10">
                                @if ($updateTeknisi->teknisidetail != null)
                                    <input name="no_identity" id="examplename" placeholder="0012XXX" value="{{ $updateTeknisi->info->gendet == 0 ? 'Male' : 'Vemale' }}" type="text" class="form-control" readonly>
                                @else
                                    <input name="no_identity" id="examplename" placeholder="Gender Empty" value="" type="text" class="form-control" readonly>
                                @endif
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">No KTP </label>
                            <div class="col-sm-10">
                                @if ($updateTeknisi->teknisidetail != null)
                                    <input name="no_identity" id="examplename" placeholder="0012XXX" value="{{ $updateTeknisi->teknisidetail->no_identity }}" type="text" class="form-control" readonly>
                                @else
                                    <input name="no_identity" id="examplename" placeholder="This technician has not filled in the ID card number" value="" type="text" class="form-control" readonly>
                                @endif
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Phone Number </label>
                            <div class="col-sm-10">
                                <input name="no_identity" id="examplename" placeholder="0012XXX" value="{{ $updateTeknisi->phone }} " type="text" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input name="no_identity" id="examplename" placeholder="0012XXX" value="{{ $updateTeknisi->email }} " type="text" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">KTP/SIM/Passport</label>
                            <div class="col-sm-10">
                                @if ($updateTeknisi->teknisidetail != null)
                                    <a href="{{asset('admin/storage/attachment/' . $updateTeknisi->teknisidetail->attachment)}}" target="_blank">
                                        <span class="badge badge-success badge-pill"><strong>Open Attachment</strong></span>
                                    </a>
                                @else
                                    <span class="badge badge-warning badge-pill"><strong>This technician has not uploaded a photo of the identity card</strong></span>
                                @endif
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">City</label>
                            <div class="col-sm-10">

                                <input name="city_id" id="city_id" value="{{ ($updateTeknisi->address != null && $updateTeknisi->address->city != null) ? $updateTeknisi->address->city->name : "-" }}" type="text" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Address</label>
                            <div class="col-sm-10">
                                <textarea name="address" class="form-control" id="exampleFormControlTextarea1" rows="3" disabled>{{ !empty($updateTeknisi->address) ? $updateTeknisi->address->address : '' }}</textarea>
                                <strong><span id="error-address" style="color:red"></span></strong>
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label"></label>
                            <div class="col-sm-10">
                                @if (Request::segment(1) == 'admin')
                                    @if ($updateTeknisi->status != 1 && $updateTeknisi->status != 8)
                                        <button class="updatestatus btn btn-success btn-sm btn-white" data-id="{{ $updateTeknisi->id }}">
                                            <i class='fa fa-check'></i>&nbsp; Approve
                                        </button>

                                        <button type="button" class="btn btn-danger  btn-sm btn-white" data-toggle="modal" data-target="#exampleModal">
                                            <i class="fa fa-close" aria-hidden="true"></i>&nbsp;Reject
                                        </button>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="position-relative row form-check"></div>
            </form>
        </div>
    </div>
</div>

<!-- ====================midal Show========================================= -->
<div class="modal fade" id="exampleModal" tabindex="-1"  data-backdrop="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Note Rejected</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Note</label>
                    <div class="col-sm-10">
                        <textarea name="note_reject" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm btn-white" data-dismiss="modal">Close</button>
                <button class="reject btn btn-danger btn-sm btn-white" data-id="{{ $updateTeknisi->id }}">
                    Reject
                </button>

            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    <script>

        $(document).on('click', '.updatestatus', function(e) {
            id = $(this).attr('data-id');
            Helper.confirm(function(){
                Helper.loadingStart();
                $.ajax({
                    url:Helper.apiUrl('/user/status/' + id ),
                    type: 'put',
                    dataType: 'JSON',
                    contentType: 'application/json',

                    success: function(res) {
                        Helper.loadingStop();
                        Helper.redirectTo('/admin/home');
                    },
                    error: function(res) {
                        alert("Something went wrong");
                        console.log(res);
                        Helper.loadingStop();
                    }
                })
            },
            {
                title: "Are You Sure",
                message: "Are you sure you have checked the completeness of this technician's data ?",
            })
            e.preventDefault()
        })

        $(document).on('click', '.reject', function(e) {
            id = $(this).attr('data-id');
            Helper.confirm(function(){
                Helper.loadingStart();
                $.ajax({
                    url:Helper.apiUrl('/user/reject/' + id ),
                    type: 'post',
                    data : {
                        note_reject : $('textarea[name="note_reject"]').val(),
                    },
                    // dataType: 'JSON',
                    // contentType: 'application/json',

                    success: function(res) {
                        Helper.successNotif('This Tecnician Has Been Rejected');
                        Helper.loadingStop();
                        Helper.redirectTo('/admin/home');
                    },
                    error: function(res) {
                        alert("Something went wrong");
                        console.log(res);
                        Helper.loadingStop();
                    }
                })
            },{
                    title: "Are You Sure",
                    message: "Are you sure you will reject this technician's application ?",
                })

            e.preventDefault()
        })
    </script>
@endsection

<!-- let modalId = $('#image-gallery');

$(document).ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title')
          .text($sel.data('title'));
        $('#image-gallery-image')
          .attr('src', $sel.data('image'));
        disableButtons(counter, $sel.data('image-id'));
      }

      if (setIDs == true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document).keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });




    $('.alert-regis').hide();
    $("#pop").on("click", function() {
   $('#imagepreview').attr('src', $('#imageresource').attr('src')); // here asign the image to the modal when the user click the enlarge link
   $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
}); -->
