<div class="app-header header-shadow bg-info header-text-light">
    <div class="app-header__logo">
        <div class="logo-src">
            <center>
                <img src="{{  url('/admin/get/logo/from-storage/' . $get_logo_astech) }}" alt="" style="width: 100px; height: auto">
            </center>
        </div>
        @if (Auth::user()->status != 6 && Auth::user()->status != 7 && Auth::user()->status != 8)
            <div class="header__pane ml-auto">
                <div>
                    <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        @endif
    </div>
    <div class="app-header__mobile-menu">
        @if (Auth::user()->status != 6 && Auth::user()->status != 7 && Auth::user()->status != 8)
            <div>
                <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        @endif
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
        <button class="btn back-to-mesg back-chat" title="Back">
            <i class="fa fa-arrow-right"></i>
        </button>
    </div>
    <div class="app-header__content">
        <div class="app-header-left">
            
        </div>
        <div class="app-header-right">
            @if (Auth::user()->status != 6 && Auth::user()->status != 7 && Auth::user()->status != 8)
                <div class="header-dot">
                    <div class="dropdown">
                        @if((Auth::user()->hasRole('Customer') || Auth::user()->hasRole('Technician')) && Request::segment(2) != 'chats')
                        <a href="{{ Auth::user()->hasRole('Technician') ? route('teknisi.chat') : route('customer.chat') }}" class="mb-2 mr-2 btn-icon btn-icon-only btn btn-link btn-sm link-dropdown-noty" style="color: white;">
                            <i class="fa fa-envelope btn-icon-wrapper font-size-xlg"> </i>
                            @if($chat_count_notif > 0)
                            <span class="badge badge-pill badge-danger count-notif">{{ $chat_count_notif }}</span>
                            @endif
                        </a>
                        @endif
                        <button style="color: white;" type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="mb-2 mr-2 btn-icon btn-icon-only btn btn-link btn-sm link-dropdown-noty">
                            <i class="fa fa-bell btn-icon-wrapper font-size-xlg"> </i>
                            @if($unread_count > 0)
                            <span class="badge badge-pill badge-danger count-notif">{{ $unread_count }}</span>
                            @endif
                        </button>
                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-noty dropdown-menu-xl rm-pointers dropdown-menu dropdown-menu-right">
                            <div class="scroll-area-sm" style="height: 200px;">
                                <div class="scrollbar-container">
                                    <div class="p-3">
                                        @foreach($notifications as $notification)
                                        <?php $datan = (json_decode($notification->data)) ?>
                                        <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                            <div class="vertical-timeline-item vertical-timeline-element">
                                                <div>
                                                    <span class="vertical-timeline-element-icon bounce-in">
                                                        @if($notification->read_at != null)
                                                        <i class="badge badge-dot badge-dot-xl badge-success"> </i>
                                                        @else
                                                        <i class="badge badge-dot badge-dot-xl badge-warning"> </i>
                                                        @endif
                                                    </span>
                                                    <div class="vertical-timeline-element-content bounce-in">
                                                        <h4 class="timeline-title"><a href="{{ route('noty.read', ['id' => $notification->id]) }}">{{ $datan->name }}</a></h4>
                                                        <p>{{ $datan->description }}
                                                            <a href="javascript:void(0);">{{ \Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</a>
                                                        </p>
                                                        <span class="vertical-timeline-element-date"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <ul class="nav flex-column">
                                <li class="nav-item-divider nav-item"></li>
                                <li class="nav-item-btn text-center nav-item">
                                    @if(Request::segment(1) == 'customer')
                                        <a href="{{ url('/customer/noty/list') }}" class="btn-shadow btn-wide btn-pill btn btn-sm">View All Notification</a>
                                    @elseif(Request::segment(1) == 'teknisi')
                                        <a href="{{ url('/teknisi/noty/list') }}" class="btn-shadow btn-wide btn-pill btn btn-sm">View All Notification</a>
                                    @elseif(Request::segment(1) == 'admin')
                                        <a href="{{ url('/admin/noty/list') }}" class="btn-shadow btn-wide btn-pill btn btn-sm">View All Notification</a>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endif
            <div class="header-btn-lg pr-0">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="btn-group">
                                <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn link-dropdown-avatar">
                                    <img width="42" class="rounded-circle" src="{{ Auth::user()->avatar }}" alt="" style="background-color: #fff;">
                                    <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                </a>
                                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-avatar rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-menu-header">
                                        <div class="dropdown-menu-header-inner bg-info">
                                            <div class="menu-header-image opacity-2" style="background-image: url('assets/images/dropdown-header/city3.jpg');"></div>
                                            <div class="menu-header-content text-left">
                                                <div class="widget-content p-0">
                                                    <div class="widget-content-wrapper">
                                                        <div class="widget-content-left mr-3">
                                                            <img width="42" class="rounded-circle" src="{{ Auth::user()->avatar }}" alt="">
                                                        </div>
                                                        <div class="widget-content-left">
                                                            <div class="widget-heading">{{ Auth::user()->full_name }}</div>
                                                        </div>
                                                        @if (Auth::user()->status != 6 && Auth::user()->status != 7 && Auth::user()->status != 8)
                                                            <div class="widget-content-right mr-2">
                                                                <!-- <a href="{{ url('logout') }}" class="btn-pill btn-shadow btn-shine btn btn-focus">Logout</a> -->
                                                                @if(Request::segment(1) == 'customer' && Auth::user()->hasRole('Customer') && Auth::user()->is_login_as_teknisi == null && Auth::user()->hasRole('Technician'))
                                                                <a type="button" class="btn-pill btn-shadow btn-shine btn btn-focus" href="{{ url('/teknisi/dashboard') }}"><i class="fa fa-arrow-right"></i> Switch To Mechanic</a>
                                                                @endif

                                                                @if(Request::segment(1) == 'teknisi' && Auth::user()->hasRole('Technician') && Auth::user()->is_login_as_teknisi == null && Auth::user()->hasRole('Customer'))
                                                                <a type="button" class="btn-pill btn-shadow btn-shine btn btn-focus" href="{{ url('/customer/dashboard') }}"><i class="fa fa-arrow-right"></i> Switch To Customer</a>
                                                                @endif
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="nav flex-column">
                                        <li class="nav-item-divider mb-0 nav-item"></li>
                                    </ul>
                                    <div class="grid-menu grid-menu-2col">
                                        <div class="no-gutters row">
                                            @if (Auth::user()->status != 6 && Auth::user()->status != 7 && Auth::user()->status != 8)
                                                <div class="col-sm-6">
                                                    @if(Request::segment(1) == 'customer' && Auth::user()->hasRole('Customer'))
                                                        <a class="btn btn-primary btn-sm" href="{{ url('/customer/profile') }}"><i class="fa fa-user"></i> Profile
                                                        </a>
                                                    @endif

                                                    @if(Request::segment(1) == 'teknisi' && Auth::user()->hasRole('Technician'))
                                                        <a class="btn btn-primary btn-sm" href="{{ url('/teknisi/profile') }}"><i class="fa fa-user"></i> Profile
                                                        </a>
                                                    @endif

                                                    @if(Request::segment(1) == 'admin' && Auth::user()->hasRole('admin'))
                                                        <a class="btn btn-primary btn-sm" href="{{ url('/admin/user/detail/' . Auth::id()) }}"><i class="fa fa-user"></i> Profile
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="col-sm-6">
                                                    @if(Request::segment(1) == 'customer' && Auth::user()->hasRole('Customer'))
                                                        @if(count(Auth::user()->addresses) > 0)
                                                            <a class="btn btn-success btn-sm" href="{{url('/customer/address/show')}}"><i class="fa fa-map"></i> Address</a>
                                                        @else
                                                            <a class="btn btn-success btn-sm" href="{{url('/customer/profile/address/create')}}"><i class="fa fa-map"></i> Address</a>
                                                        @endif
                                                    @endif

                                                    @if(Request::segment(1) == 'teknisi' && Auth::user()->hasRole('Technician'))
                                                        @if(count(Auth::user()->addresses) > 0)
                                                            <a class="btn btn-success btn-sm" href="{{url('/teknisi/address/show')}}"><i class="fa fa-map"></i> Address</a>
                                                        @else
                                                            <a class="btn btn-success btn-sm" href="{{url('/teknisi/address/create')}}"><i class="fa fa-map"></i> Address</a>
                                                        @endif
                                                        
                                                    @endif

                                                </div>
                                            @endif
                                            <div class="col-sm-12">
                                                <a href="{{ url('logout') }}" class="btn-shadow btn-shine btn btn-focus">Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content-left  ml-3 header-user-info">
                            <div class="widget-heading"> {{ Auth::user()->full_name }} </div>
                            @if (Auth::user()->status != 6 && Auth::user()->status != 7 && Auth::user()->status != 8)
                                <div class="widget-subheading"> {{ Auth::user()->roles->pluck('name')->join(',') }} </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>