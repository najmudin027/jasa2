<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Login - ASTech</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="ArchitectUI HTML Bootstrap 4 Dashboard Template">
    <meta name="msapplication-tap-highlight" content="no">

	<link href="https://demo.dashboardpack.com/architectui-html-pro/main.d810cf0ae7f39f28f336.css" rel="stylesheet"></head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow">
        <div class="app-container">
            <div class="h-100">
                <div class="h-100 no-gutters row">
                    <div class="d-none d-lg-block col-lg-4">
                        <div class="slider-light">
                            <div class="slick-slider">
                                <div>
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-plum-plate" tabindex="-1">
                                        <div class="slide-img-bg" style="background-image: url('{{ asset('image2.jpg')}}');"></div>
                                        <div class="slider-content">
                                            {{-- <h3>Perfect Balance</h3>
                                            <p>ArchitectUI is like a dream. Some think it's too good to be true! Extensive
                                                collection of unified React Boostrap Components and Elements.
                                            </p> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="h-100 d-flex bg-white justify-content-center align-items-center col-md-12 col-lg-8">
                        <div class="mx-auto app-login-box col-sm-12 col-md-10 col-lg-9">
                            <div class="app-logo" style="background-image: url('{{ asset('astech.png')}}'); width:150px;height:30px"></div>
                            <h4 class="mb-0">
                                <span class="d-block">Create New Password Forms,</span>
                            </h4>
								<div class="divider row"></div>
                            <div>
								@if(session()->has('error'))
									<div class="alert alert-danger" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<strong>{{ session()->get('error') }} </strong>
									</div>
                                @endif
                                @if(session()->has('success'))
									<div class="alert alert-primary" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<strong>{{ session()->get('success') }} </strong>
									</div>
								@endif
								<form class="" id="signupForm" action="{{ url('/forgot-password-update') }}" method="post">
                                    @csrf
                                    <input type="hidden" value="{{ request()->token }}" name="key">
                                    <div class="form-row">
                                        <div class="col-md-6">
											<span class="txt1 p-b-11">New Password</span>
											<div class="wrap-input100 validate-input m-b-36" data-validate = "Password is required">
												<input type="password" class="form-control" id="password" name="password" placeholder="Password">
												<span class="focus-input100"></span>
											</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
												<span class="txt1 p-b-11">Confrim Password</span>
												<span id="getToken" style="display: none;">{{ $getToken }}</span>
												<div class="wrap-input100 validate-input m-b-36" data-validate = "Confrim Password is required">
													<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Password">
													<span class="focus-input100"></span>
												</div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="divider row"></div>
                                    <div class="d-flex align-items-center">
                                        <div class="ml-auto">
                                            <button class="btn btn-primary btn-lg">Create New Password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<script type="text/javascript" src="https://demo.dashboardpack.com/architectui-html-pro/assets/scripts/main.d810cf0ae7f39f28f336.js"></script>
	<script src="{{ asset('main.js') }}"></script>
</body>

</html>