<div class="col-md-3">
    <div class="main-card mb-3 card">
        <div class="card-body"><h5 class="card-title">User Management</h5>
            <ul class="list-group list-group-flush">
                <a href="{{ url('/admin/user/show') }}" class="list-group-item">Create Users</a>
                <a href="{{ url('/admin/permission') }}" class="list-group-item">Permission</a>
                <a href="{{ url('/admin/role/show') }}" class="list-group-item">Role</a>
            </ul>
        </div>
    </div>
</div>