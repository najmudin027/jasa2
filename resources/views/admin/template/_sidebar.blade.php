<div class="app-sidebar sidebar-shadow bg-dark sidebar-text-light">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar" style="overflow: scroll;">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class='app-sidebar__heading'></li>

                @if(Auth::user()->hasRole('Customer') && $role_prefix == 'customer')
                <li class="">
                    <a href='/customer/dashboard' style='height:2.5rem;'>
                        <i class='metismenu-icon fa fa-dashboard'></i> Dashboard
                    </a>
                </li>
                <li class='app-sidebar__heading'></li>
                <li class="">
                    <a href='/customer/create-request-job' style='height:2.5rem;'>
                        <i class='metismenu-icon fa fa-search'></i>Create Request Job
                    </a>
                </li>
                <!-- <li class='app-sidebar__heading'></li>
                    <li class="">
                        <a href='/customer/category-services' style='height:2.5rem;'>
                            <i class ='metismenu-icon fa fa-stethoscope'></i>Category Services
                        </a>
                    </li> -->
                <li class='app-sidebar__heading'></li>
                @if (Request::segment(2) == 'request_list' || Request::segment(2) == 'transacsion_list' || Request::segment(2) == 'topup' || Request::segment(2) == 'topup-history')
                <li class="mm-active">
                    @else
                <li>
                    @endif
                    <a href="#">
                        <i class="metismenu-icon fa fa-sliders"></i> Transaction
                        @if( ($count_new_order + $count_garansi_customer + $count_complaint_order) > 0)
                        <span class="badge badge-pill badge-warning" style="border-radius: 0; padding: 5px;">New</span>
                        @endif
                        <i class="metismenu-state-icon fa fa-chevron-down caret-left"></i>
                    </a>
                    @if (Request::segment(2) == 'request_list' || Request::segment(2) == 'transacsion_list' || Request::segment(2) == 'topup' || Request::segment(2) == 'topup-history')
                    <ul class="mm-show">
                        @else
                        <ul>
                            @endif

                            <li>
                                @if (Request::segment(2) == 'request_list' )
                                <a href="/customer/request_list" class="mm-active">
                                    @else
                                    <a href="/customer/request_list">
                                        @endif
                                        <i class="metismenu-icon"></i> List Transaction
                                        @if($count_new_order > 0)
                                        <span class="badge badge-pill badge-warning" style="border-radius: 0; padding: 5px;">{{ $count_new_order }}</span>
                                        @endif
                                    </a>
                            </li>
                            <li>
                                @if (Request::segment(2) == 'transacsion_list' )
                                <a href="/customer/transacsion_list/complaint" class="mm-active">
                                    @else
                                    <a href="/customer/transacsion_list/complaint">
                                        @endif
                                        <i class="metismenu-icon"></i>List Complaint
                                        @if($count_complaint_order > 0)
                        <span class="badge badge-pill badge-warning" style="border-radius: 0; padding: 5px;">{{ $count_complaint_order }}</span>
                        @endif
                                    </a>
                            </li>
                            <li>
                                @if (Request::segment(2) == 'topup' )
                                <a href="/customer/topup/show-list" class="mm-active">
                                    @else
                                    <a href="/customer/topup/show-list">
                                        @endif
                                        <i class="metismenu-icon"></i>Top up Wallet
                                    </a>
                            </li>
                            <li>
                                @if (Request::segment(2) == 'topup-history' )
                                <a href="/customer/topup-history/show-history" class="mm-active">
                                    @else
                                    <a href="/customer/topup-history/show-history">
                                        @endif
                                        <i class="metismenu-icon"></i>History Payment
                                    </a>
                            </li>
                            <li>
                                <a href='/customer/garansi'>
                                    <i class='metismenu-icon fa fa-gear'></i>Garansi Order
                                    @if($count_garansi_customer > 0)
                                    <span class="badge badge-pill badge-warning" style="border-radius: 0; padding: 5px;">{{ $count_garansi_customer }}</span>
                                    @endif
                                </a>
                            </li>
                        </ul>
                </li>
                {{-- <li class="">
                        <a href='/customer/request_list' style='height:2.5rem;'>
                            <i class ='metismenu-icon fa fa-sliders'></i> Transaction List
                        </a>
                    </li> --}}
                <li class='app-sidebar__heading'></li>
                <li class="">
                    <a href='/customer/reviews/waiting' style='height:2.5rem;'>
                        <i class='metismenu-icon fa fa-star'></i>My Review
                        @if($count_waiting_review > 0)
                        <span class="badge badge-pill badge-warning" style="border-radius: 0; padding: 5px;">{{ $count_waiting_review }}</span>
                        @endif
                    </a>
                </li>
                <li class='app-sidebar__heading'></li>
                <li class="">
                    <a href='/customer/tickets' style='height:2.5rem;'>
                        <i class='metismenu-icon fa fa-envelope'></i>Ticket Support
                        @if($count_new_ticket > 0)
                        <span class="badge badge-pill badge-warning" style="border-radius: 0; padding: 5px;">{{ $count_new_ticket }}</span>
                        @endif
                    </a>
                </li>
                @endif


                @if(Auth::user()->hasRole('Technician') && $role_prefix == 'teknisi')
                <li class="">
                    <a href='/teknisi/dashboard' style='height:2.5rem;'>
                        <i class='metismenu-icon fa fa-dashboard'></i> Dashboard
                    </a>
                </li>
                <li class='app-sidebar__heading'></li>
                <!-- <li class="">
                        <a href='/teknisi/list-request-job' style='height:2.5rem;'>
                                <i class ='metismenu-icon fa fa-briefcase'></i> Job Request</a>
                    </li> -->

                @if (Request::segment(2) == 'list-request-job' || Request::segment(2) == 'transacsion_list')
                <li class="mm-active">
                    @else
                <li>
                    @endif
                    <a href="#">
                        <i class="metismenu-icon fa fa-sliders"></i> Request List
                        @if(($count_new_order + $count_complaint_order) > 0)
                        <span class="badge badge-pill badge-warning" style="border-radius: 0; padding: 5px;">New</span>
                        @endif
                        <i class="metismenu-state-icon fa fa-chevron-down caret-left"></i>
                    </a>
                    @if (Request::segment(2) == 'list-request-job' || Request::segment(2) == 'transacsion_list' || Request::segment(2) == 'list-garansi-job')
                    <ul class="mm-show">
                        @else
                        <ul>
                            @endif

                            <li>
                                @if (Request::segment(2) == 'list-request-job' )
                                <a href="/teknisi/list-request-job" class="mm-active">
                                    @else
                                    <a href="/teknisi/list-request-job">
                                        @endif
                                        <i class="metismenu-icon"></i> List Jobs
                                        @if($count_new_order > 0)
                                        <span class="badge badge-pill badge-warning" style="border-radius: 0; padding: 5px;">{{ $count_new_order }}</span>
                                        @endif
                                    </a>
                            </li>
                            <li>
                                @if (Request::segment(2) == 'complaint-jobs' )
                                <a href="/teknisi/complaint-jobs" class="mm-active">
                                    @else
                                    <a href="/teknisi/complaint-jobs">
                                        @endif
                                        <i class="metismenu-icon"></i>List Complaint
                                        @if($count_complaint_order > 0)
                        <span class="badge badge-pill badge-warning" style="border-radius: 0; padding: 5px;">{{ $count_complaint_order }}</span>
                        @endif
                                    </a>
                            </li>

                            <li>
                                @if (Request::segment(2) == 'list-garansi-job' )
                                <a href="{{ url('/teknisi/list-garansi-job') }}" class="mm-active">
                                    @else
                                    <a href="{{ url('/teknisi/list-garansi-job') }}">
                                        @endif
                                        <i class="metismenu-icon"></i>List Claim
                                        @if($count_garansi_teknisi > 0)
                                        <span class="badge badge-pill badge-warning" style="border-radius: 0; padding: 5px;">{{ $count_garansi_teknisi }}</span>
                                        @endif
                                    </a>
                            </li>
                        </ul>
                </li>

                <li class='app-sidebar__heading'></li>
                <li class="">
                    <a href='{{ url("/teknisi/review-list") }}' style='height:2.5rem;'>
                        <i class='metismenu-icon fa fa-star'></i> Review List</a>
                </li>
                <li class='app-sidebar__heading'></li>
                <li class="">
                    <a href='{{ route('teknisi.service_price') }}' style='height:2.5rem;'>
                        <i class='metismenu-icon fa fa-dollar'></i> Service Price</a>
                </li>
                <li class='app-sidebar__heading'></li>
                <li class="">
                    <a href='/teknisi/my-schedules' style='height:2.5rem;'>
                        <i class='metismenu-icon fa fa-users'></i> My Schedules</a>
                </li>
                {{-- <li class='app-sidebar__heading'></li>
                <li class="">
                    <a href='/teknisi/team/list' style='height:2.5rem;'>
                        <i class='metismenu-icon fa fa-users'></i> My Team Member</a>
                </li> --}}
                @endif


                @if(!Auth::user()->hasRole('Technician') && !Auth::user()->hasRole('Customer'))
                <li class="">
                    <a href='/admin/home' style='height:2.5rem;'>
                        <i class='metismenu-icon fa fa-dashboard'></i> Dashboard
                    </a>
                </li>
                <li class='app-sidebar__heading'></li>
                {!! $menus !!}
                @endif
            </ul>
        </div>
    </div>
</div>
