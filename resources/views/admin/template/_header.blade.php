<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title }} | {{ $title_header_global }}</title>

    {{-- @if ($setting != null)
        <link rel = "icon" type = "image/png" href = "{{ asset('public/web_setting_image/' . $setting->icon) }}">
    @else
        <link rel = "icon" type = "image/png" href = ""> --}}
    {{-- @endif --}}

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="{{ $metta_header_global }}">
    <meta name="msapplication-tap-highlight" content="no">
    {{-- ======================CSS====================== --}}
    <link href="{{ asset('adminAssets/css/main2.css') }}" rel="stylesheet">
    <link href="{{  asset('adminAssets/css/nestable.css') }}" rel="stylesheet" />
    <link href="{{  asset('adminAssets/css/dropzone.css') }}" rel="stylesheet" />


    {{-- <link href="{{  asset('adminAssets/css/font-awesome.min.css') }}" rel="stylesheet" /> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    {{-- ==================Javascript================== --}}
    <script type="text/javascript" src="{{ asset('adminAssets/js/dropzone.js') }}"></script>

    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">


    <style>
        body{
            font-family: 'Roboto', sans-serif;
        }
        .modal {
            top: 100px;
            right: 100px;
            bottom: 100px;
            left: 0px;
            z-index: 10040;
            overflow: auto;
            overflow-y: auto;
        }


        ol { list-style: none;}
        .breadcrumb { width: 100%; float: left; margin: 20px 0; padding: 7px; position: relative; display: block; background: rgba(0,0,0,.05)}
        .breadcrumb ol { list-style: none;}
        .breadcrumb li { height: 30px; line-height: 30px; float: left; padding: 0 12px;}
        .breadcrumb li a { text-decoration: none;}
        .breadcrumb li .fa { width: 30px; height: 30px; line-height: 30px;}
        .breadcrumb a:hover { text-decoration: none;}

        /* Bredcrumb Fill 0 */
        .breadcrumb-fill0 { padding: 15px; background: #ffffff; border-radius: 5px; box-shadow: 0px 0px 50px 0px rgba(0,0,0,.2);}

        /* Bredcrumb Fill 1 */
        .breadcrumb-fill1 { padding: 15px; background: #135fb4; color: #ffffff; border-radius: 5px; box-shadow: 0px 0px 50px 0px rgba(0,0,0,.2);}
        .breadcrumb-fill1, .breadcrumb-fill1 li, .breadcrumb-fill1 li.active, .breadcrumb-fill1 li a { color: #ffffff;}

        /* Bredcrumb Fill 2 - style 1 */
        .breadcrumb-fill2 { padding: 15px; background: #135fb4; color: #ffffff; border-radius: 15px; box-shadow: 0px 0px 50px 0px rgba(0,0,0,.2);}
        .breadcrumb-fill2 .fa { position: absolute; background: #135fb4; padding: 25px; border-radius: 50%; left: -10px; top: -10px; font-size: 30px; line-height: 1; box-shadow: 0px 0px 50px 0px rgba(0,0,0,.2);}
        .breadcrumb-fill2, .breadcrumb-fill2 li, .breadcrumb-fill2 li.active, .breadcrumb-fill2 li a { color: #ffffff;}
        .breadcrumb-fill2 li:first-child { margin-right: 30px;}
    </style>
</head>
