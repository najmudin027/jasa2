@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                            {{ $title }}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary btn-sm add-btb-outlet" style="float: right"><i class="fa fa-plus"></i> ADD OUTLET</button>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" id="btb-outlet-ids" name="business_to_business_transaction_id" value="{{ !empty($outlet) ? $outlet->business_to_business_transaction_id : $id }}">
        <div class="card-body">
            <table id="table-btb" class="table table-hover table-bordered" style="width:100%; white-space: nowrap;">
                <thead>
                    <tr>
                        {{-- <th>NO</th> --}}
                        <th>Nama Outlet</th>
                        <th>Jumlah Unit</th>
                        <th>Nama PIC</th>
                        <th>Nomor PIC</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-body">
            {{-- <button class="btn btn-primary btn-sm add-btb"><i class="fa fa-plus"></i> Add New</button>
            <a href="{{ url('/admin/business_to_business/log') }}" class="btn btn-success btn-sm">View Logs</a> --}}
        </div>
    </div>
</div>

<form id="form-btb-outlet">
    <div class="modal fade" id="modal-btb-outlet" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Outlet</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="btb-outlet-id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input name="name" placeholder="Name Outlet" type="text" class="form-control btb-outlet-name" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Jumlah Unit</label>
                                        <input name="jumlah_unit" placeholder="Jumlah Unit" type="number" class="btb-outlet-jumlah_unit form-control" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Type</label>
                                        <select class="form-control btb-outlet-type" name="type" aria-label="Default select example">
                                            <option value="gedung">Gedung</option>
                                            <option value="kantor">Kantor</option>
                                            <option value="rumah">Rumah</option>
                                            <option value="apartemen">Apartemen</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="">Nomor PIC</label>
                                    <input name="nomor_pic" placeholder="628" type="text" class="form-control phone btb-outlet-phone-pic" />
                                </div>
                                <div class="col-md-12">
                                    <label for="">Nama PIC</label>
                                    <input name="nama_pic" placeholder="Nama PIC" type="text" class="form-control btb-outlet-nama-pic" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea name="alamat" id="" cols="30" rows="2"  class="btb-outlet-alamat form-control"></textarea>
                                {{-- <input name="alamat" placeholder="alamat" type="text"> --}}
                            </div>
                            <div class="form-group">
                                <label for="">Maps</label>
                                <input type="text" name="searchMap" style="margin-top: 7px; width: 70%; border:2px solid #4DB6AC;" id="searchMap" class="form-control btb-outlet-map" >
                            </div>
                            <div class="form-group">
                                <div id="map" style="height:400px"></div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-5 col-xs-5 mr-4">
                                    <label class="control-label">Latitude</label>
                                    <input size="20" type="text" id="latbox" class="form-control btb-outlet-lat"  name="lat" onchange="inputLatlng('new')" >
                                </div>
                                <div class="form-group col-md-5 col-xs-5 mr-4">
                                    <label class="control-label">Longitude</label>
                                    <input size="20" type="text" id="lngbox"  class="form-control btb-outlet-long"  name="long" onchange="inputLatlng('new')" >
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
                    <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
</form>

<style>
    .fixed-sidebar .app-main .app-main__outer {
        width: 100%;
    }

    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
@include('admin.template._mapsScript')
<script>
    $('.phone').mask('628 0000 0000 00');
    $('.add-btb-outlet').click(function() {
        kosongkanFieldHeader();
        $('#modal-btb-outlet').modal('show')
    })

    const kosongkanFieldHeader = function() {
        $('.btb-outlet-alamat').val('');
        $('.btb-outlet-jumlah_unit').val('');
        $('.btb-outlet-name').val('');
        $('.btb-outlet-map').val('');
        $('.btb-outlet-lat').val('');
        $('.btb-outlet-long').val('');
        $('.btb-outlet-type').val('');
        $('.btb-outlet-id').val('');
        $('.btb-outlet-phone-pic').val('');
        $('.btb-outlet-nama-pic').val('');
    }

    const isiFieldHeader = function(row) {
        $('.btb-outlet-alamat').val(row.business_to_business_outlet_transaction.alamat);
        $('.btb-outlet-jumlah_unit').val(row.business_to_business_outlet_transaction.jumlah_unit);
        $('.btb-outlet-name').val(row.business_to_business_outlet_transaction.name);
        $('.btb-outlet-id').val(row.business_to_business_outlet_transaction.id);
        $('.btb-outlet-map').val(row.business_to_business_outlet_transaction.searchMap);
        $('.btb-outlet-lat').val(row.business_to_business_outlet_transaction.lat);
        $('.btb-outlet-long').val(row.business_to_business_outlet_transaction.long);
        $('.btb-outlet-type').val(row.business_to_business_outlet_transaction.type);
        $('.btb-outlet-phone-pic').val(row.business_to_business_outlet_transaction.nomor_pic);
        $('.btb-outlet-nama-pic').val(row.business_to_business_outlet_transaction.nama_pic);
    }

    $('#form-btb-outlet').submit(function(e) {
        e.preventDefault();
        Helper.loadingStart();

        data = Helper.serializeForm($(this));
        console.log(data)
        data.business_to_business_transaction_id = $('#btb-outlet-ids').val();
        id = $('.btb-outlet-id').val();
        if (id === '') {
            Axios.post('/business_to_business/outlet', data)
                .then(function(response) {
                    // alert('create');
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    // $('#table-btb-outlet').bootstrapTable('refresh');
                    location.reload();
                    // hide modal
                    $('#modal-btb-outlet').modal('hide')
                    // kosongkan field
                    kosongkanFieldHeader()
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        } else {
            Axios.put('/business_to_business/' + id + '/outlet', data)
                .then(function(response) {
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    $('#table-btb-outlet').bootstrapTable('refresh');
                    // hide modal
                    $('#modal-btb-outlet').modal('hide')
                    // kosongkan field
                    kosongkanFieldHeader()
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    // kosongkan field
                    kosongkanFieldHeader()
                    Helper.loadingStop();
                });
        }
    });

    $(document).on('click', '.delete-btb-outlet-header', function() {
        var id = $(this).attr('data-id');
        Helper.confirmDelete(function() {
            Helper.loadingStart();
            Axios.delete('/business_to_business/' + id + '/outlet')
                .then(function(response) {
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        })
    })

    $(document).ready(function() {
        var table = $('#table-btb').DataTable({
            processing: true,
            serverSide: true,
            select: false,
            dom: 'Bflrtip',
            responsive: true,
            order: [1, 'asc'],
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/company/datatables-outlet-by-company/'+ $('#btb-outlet-ids').val()),
                "type": "get",
            },
            // columnDefs: [
            //     {
            //         "targets": [ 1 ],
            //         "visible": false,
            //         "searchable": false
            //     },
            // ]
            columns: [
                // {
                //     data: "DT_RowIndex",
                //     name: "DT_RowIndex",
                //     sortable: false,
                //     searchable: false,
                //     width: "10%"
                // },
                {
                    data: "name",
                    name: "name",
                    render: function(data, type, row) {
                        return row.name
                    }
                },
                {
                    data: "jumlah_unit",
                    name: "jumlah_unit",
                    render: function(data, type, row) {
                        return row.jumlah_unit
                    }
                },
                {
                    data: "nama_pic",
                    name: "nama_pic",
                    render: function(data, type, row) {
                        return row.nama_pic 
                    }
                },
                {
                    data: "nomor_pic",
                    name: "nomor_pic",
                    render: function(data, type, row) {
                        return row.nomor_pic 
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        btn_edit_item = '<a href="/admin/company/list_outlet-by-company/update/' + row.id + '" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
                        btn_delete_item = '<button data-toggle="tooltip" title="delete" type="button" data-id="' + row.id + '" class="btn btn-danger btn-xs delete-btb-outlet-header"><i class="fa fa-trash"></i></button>';
                        // view_link = "<a class='btn btn-primary btn-sm' href='" + Helper.url('/admin/business-to-business-user-request-order/detail/' + row.request_code) + "'><i class='fa fa-eye'></i></a>";
                        // btn_delete_item = '<button type="button" data-id="' + row.request_code + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
                        return btn_edit_item + " " + btn_delete_item;
                    }
                },
            ],
        });

    });
</script>
@endsection