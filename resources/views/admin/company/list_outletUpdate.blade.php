@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-10">
                        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                            {{ $title }}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary btn-sm add-btb-outlet" style="float: right"><i class="fa fa-plus"></i> ADD OUTLET</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="card-body">
            <form id="form-btb-outlet">
                <input type="text" id="ids" value="{{ $outlet->id }}">
                <input type="text" class="btb-outlet-id"  value="{{ $outlet->business_to_business_transaction_id }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name="name" placeholder="Name Outlet" type="text" class="form-control btb-outlet-name" value="{{ $outlet->name }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jumlah Unit</label>
                                    <input name="jumlah_unit" placeholder="Jumlah Unit" type="number" class="btb-outlet-jumlah_unit form-control" value="{{ $outlet->jumlah_unit }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Type</label>
                                    <select class="form-control btb-outlet-type" name="type" aria-label="Default select example">
                                        <option value="gedung" {{ $outlet->type == "Gedung" ? 'selected' : false }} >Gedung</option>
                                        <option value="kantor" {{ $outlet->type == "Kantor" ? 'selected' : false }} >Kantor</option>
                                        <option value="rumah" {{ $outlet->type == "Rumah" ? 'selected' : false }} >Rumah</option>
                                        <option value="apartemen" {{ $outlet->type == "Apartemen" ? 'selected' : false }} >Apartemen</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="">Nomor PIC</label>
                                <input name="nomor_pic" placeholder="628" type="text" class="form-control phone btb-outlet-phone-pic" value="{{ $outlet->nomor_pic }}" required/>
                            </div>
                            <div class="col-md-12">
                                <label for="">Nama PIC</label>
                                <input name="nama_pic" placeholder="628" type="text" class="form-control btb-outlet-nama-pic" value="{{ $outlet->nama_pic }}" required/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" id="" cols="30" rows="2"  class="btb-outlet-alamat form-control">{{ $outlet->alamat }}</textarea>
                            {{-- <input name="alamat" placeholder="alamat" type="text"> --}}
                        </div>
                        <div class="form-group">
                            <label for="">Maps</label>
                            <input type="text" name="searchMap" style="margin-top: 7px; width: 70%; border:2px solid #4DB6AC;" id="searchMap" class="form-control btb-outlet-map" required>
                        </div>
                        <div class="form-group">
                            <div id="map" style="height:400px"></div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-5 col-xs-5 mr-4">
                                <label class="control-label">Latitude</label>
                                <input size="20" type="text" id="latbox" class="form-control btb-outlet-lat"  value="{{ $outlet->lat }}" name="lat" onchange="inputLatlng('new')" required>
                            </div>
                            <div class="form-group col-md-5 col-xs-5 mr-4">
                                <label class="control-label">Longitude</label>
                                <input size="20" type="text" id="lngbox"  class="form-control btb-outlet-long"  value="{{ $outlet->long }}" name="long" onchange="inputLatlng('new')" required>
                            </div>
                        </div>
                        {{-- <div class="row"> --}}
                            <button type="submit" class="btn btn-primary btn-block">Simpan Data</button>
                        {{-- </div> --}}
                    </div>
                </div>
            </form>
            
        </div>
    </div>
</div>

@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
@include('admin.template._mapsScript')
<script>
    $('#form-btb-outlet').submit(function(e) {
        e.preventDefault();
        Helper.loadingStart();

        data = Helper.serializeForm($(this));
        console.log(data)
        data.business_to_business_transaction_id = $('.btb-outlet-id').val();
        id = $('#ids').val();
        if (id === '') {
            Axios.post('/business_to_business/outlet', data)
                .then(function(response) {
                //     alert('asd');
                //     //send notif
                    Helper.successNotif(response.data.msg);
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        } else {
            Axios.put('/business_to_business/' + id + '/outlet', data)
                .then(function(response) {
                    Helper.successNotif(response.data.msg).redirectTo('/admin/company/list_outlet-by-company/' + $('.btb-outlet-id').val());
                    
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        }
    })
</script>
@include('admin.template._mapsScript')
@endsection