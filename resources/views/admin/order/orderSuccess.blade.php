@extends('admin.home')
@section('content')


<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                TRANSACTION PENDING TRANSFER
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                {{-- <a href="/admin/transactions/create" class="mb-2 mr-2 btn btn-primary btn-sm" style="float:right">Create Order</a> --}}
            </div>
        </div>
        

        <div class="card-body">
            <!-- <div class="tab-pane show active" id="tab-1" role="tabpanel"> -->
                <table id="list_pending_transfer" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Technician Name</th>
                            <th>Email</th>
                            <th>Total Pending</th>
                            <th>Total Success</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($countTeknisiOrder as $b)
                            @if($b->total_pending_orders > 0 || $b->total_success_orders > 0)
                                <tr>
                                    <td>{{$b->name}}</td>
                                    <td>{{$b->email}}</td>
                                    <td>{{ $b->total_pending_orders > 0 ? $b->total_pending_orders : 0  }}</td>
                                    <td>{{ $b->total_success_orders > 0 ? $b->total_success_orders : 0 }}</td>
                                    <td><a href="{{ url('/admin/transaction-success/detail/' . $b->id) }}" class="badge badge-primary btn-sm" data-toggle="tooltip" data-html="true" title="<b>Detail</b>"><i class="fa fa-eye"></i></a></td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>   
                </table>
            <!-- </div> -->
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
        $(document).ready(function() {
            $('#list_pending_transfer').DataTable({
                ordering: 'true',
                order: [2, 'desc', 3, 'desc'],
            });
        } );
</script>
@endsection
