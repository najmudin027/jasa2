@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card header-border">
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="">Schedule</label>
                            <input name="order_date_range" type="text" value="" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3" style="display: none;">
                        <div class="position-relative form-group">
                            <label for="" class="label-control">Warranty Claims</label>
                            <select class="form-control" name="klaim_garansi">
                                <option value="">All</option>
                                <option value="1" selected>Only Warranty Claims</option>
                                <option value="0">Without Warranty Claims</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="label-control">Payment Type</label>
                            <select class="form-control" name="payment_type">
                                <option value="">All</option>
                                <option value="1">Online</option>
                                <option value="0">Offline</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="label-control">Status</label>
                            <select class="form-control select-status" name="order_status_id[]" multiple style="width: 100%">
                                <option value="2">Analyzing</option>
                                <option value="3">Waiting Approval</option>
                                <option value="4">On Working</option>
                                <option value="9">Processing</option>
                                <option value="7">Job Done</option>
                                <option value="10">Job Completed</option>
                                <option value="11">Complaint</option>
                                <option value="5">Cancel</option>
                            </select>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>

        <div class="card-body">
            <div class="tab-pane show active" id="tab-1" role="tabpanel">
                <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Orders Code</th>
                            <th>Schedule</th>
                            <th>Customer</th>
                            <th>Technician</th>
                            <th>Payment Type</th>
                            <th>Total</th>
                            <th>Status Order</th>
                            <th>Status Garansi</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <a href="/admin/transactions/create" class="mb-2 mr-2 btn btn-primary btn-sm"><i class="fa fa-plus"></i> Create Order</a>
                <form class="navbar-form navbar-left" action="{{ url('/admin/transactions/excel_download') }}" method="get" role="search">
                    <button type="submit" class="btn bg-olive active btn-success btn-sm"> Export To Excel</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    $(function() {
        $('input[name="order_date_range"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="order_date_range"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });

        $('input[name="order_date_range"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    });

    globalCRUD.select2(".select-status")

    // $(document).ready(function() {
    var tableOrder = $('#table-list-orders').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        scrollX: true,
        ordering: 'true',
        order: [1, 'desc'],
        responsive: true,
        language: {
            buttons: {
                colvis: '<i class="fa fa-list-ul"></i>'
            },
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        buttons: [{
                extend: 'colvis'
            },
            {
                text: '<i class="fa fa-refresh"></i>',
                action: function(e, dt, node, config) {
                    dt.ajax.reload();
                }
            }
        ],
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/order/datatables/search'),
            type: 'get',
            data:{
                klaim_garansi: 1,
            }
        },
        columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                sortable: false,
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    link = "<a href='" + Helper.url('/admin/order/service_detail/' + full.id) + "'># " + full.code + "</a>";
                    if (full.garansi) {
                        link += '</br><span class="ml-auto badge badge-primary">Warranty Claim</span>';
                    }
                    if (full.admin_read_at == null) {
                        link += '</br><span class="ml-auto badge badge-warning">NEW</span>';
                    }
                    return link;
                }
            },
            {
                data: "schedule",
                name: "schedule",
                render: function(data, type, full) {
                    return moment(full.schedule).format("DD MMMM YYYY HH:mm");
                }
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    if (full.user == null) {
                        return "Deleted Users";
                    } else {
                        return '<img style="box-shadow: 0 1px 3px #694a4a;width: 50px;height: 50px; border-radius: 50%;" src="'+full.user.avatar+'"> <span style="margin-left: 10px;">'+full.user.name+'</span>';
                    }
                }
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    if (full.service_detail.length) {
                        service_detail = full.service_detail[0];
                        return '<img style="box-shadow: 0 1px 3px #694a4a;width: 50px;height: 50px; border-radius: 50%;" src="'+service_detail.technician.user.avatar+'"> <span style="margin-left: 10px;">'+service_detail.technician.user.full_name+'</span>';
                    } else {
                        return "Deleted Users";
                    }
                }
            },
            {
                data: "payment_type",
                name: "payment_type",
                render: function(data, type, full) {
                    status = full.payment_type == 0 ? '<div class="badge badge-danger ml-2">Offline</div>' : '<div class="badge badge-info ml-2">Online</div>';
                    return status;
                }
            },
            {
                data: "grand_total",
                name: "grand_total",
                render: $.fn.dataTable.render.number(',', '.,', 0, 'Rp. ', '.00')
            },
            {
                data: "order_status.name",
                name: "order_status.name",
                render: function(data, type, full) {
                    var text = '';
                    if (full.order_status.id === 3 && full.is_less_balance === 1) {
                        text = '<span class="badge badge-warning">' + data + '</span><br/> <span class="badge badge-danger">Less Balance</span>';
                    } else {
                        if (full.orders_statuses_id === 2) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 3) {
                            return '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 4) {
                            return '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 5) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 6) {
                            return '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 7) {
                            return '<span class="badge badge-focus">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 8) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 9) {
                            return '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 10) {
                            return '<span class="badge badge-success">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 11) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        }
                    }
                    return text;
                }
            },
            {
                data: "order_status.name",
                name: "order_status.name",
                render: function(data, type, full) {
                    if (full.garansi.status == 'pending') {
                        className = 'badge badge-primary';
                    }

                    if (full.garansi.status == 'revisit') {
                        className = 'badge badge-warning';
                    }

                    if (full.garansi.status == 'reject') {
                        className = 'badge badge-danger';
                    }

                    if (full.garansi.status == 'done') {
                        className = 'badge badge-success';
                    }

                    return '<span class="'+className+'">' + full.garansi.status + '</span>';
                }
            },
            {
                data: "id",
                name: "id",
                orderable: false,
                searchable: false,
                render: function(data, type, full) {
                    action = '';

                    garansi_action = full.garansi == null ? '' : "<a class='btn-sm btn btn-success btn-hover-shine' href='" + Helper.url('/admin/garansi/' + full.garansi.id + '/detail') + "' title='claim'><i class='fa fa-envelope'></i></a>";

                    detail_action = "<a href='/admin/order/service_detail/" + full.id + "' class='btn-hover-shine btn btn-primary btn-sm' data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>";

                    delete_action = "<button class='btn-hover-shine delete btn btn-danger btn-sm' data-id='" + full.id + "' data-toggle='tooltip' data-html='true' title='Delete'><i class='fa fa-trash'></i></button>";

                    complain_action = "<a href='/admin/transacsion_list/complaint/detail/" + full.id + "' class='btn btn-primary btn-sm' data-toggle='tooltip' data-html='true' title='Complaint'><i class='fa fa-comment'></i></a";

                    action += garansi_action + ' ';

                    if (full.orders_statuses_id != null) {
                        if (full.order_status.id == 2) {
                            action += delete_action;
                        }

                        if (full.order_status.id == 11) {
                            action += complain_action;
                        }
                    }

                    return action;
                }
            }
        ]
    });


    $("#form-search").submit(function(e) {
        input = Helper.serializeForm($(this));
        playload = '?';
        _.each(input, function(val, key) {
            playload += key + '=' + val + '&'
        });
        playload = playload.substring(0, playload.length - 1);
        console.log(playload)

        url = Helper.apiUrl('/order/datatables/search' + playload);
        tableOrder.ajax.url(url).load();
        e.preventDefault();
    })

    // })

    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function() {
            Helper.loadingStart();
            $.ajax({
                url: Helper.apiUrl('/admin/order/delete/' + id),
                type: 'get',

                success: function(res) {
                    // Helper.successNotif('data berhasil didelete');
                    Helper.loadingStop();
                    tableOrder.ajax.reload();
                    // $('#myButton').trigger('click');
                },
                error: function(res) {
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        e.preventDefault()
    })
</script>
@endsection