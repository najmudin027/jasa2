@if(count($sparepart_details) == 0 && count($item_details) == 0)
<form id="sparepart-detail-form">
    <input type="hidden" id="sparepart_detail_order_id" value="{{ $order->id }}" name="sparepart_detail_order_id">
    <!-- sparepart -->
    <div class="card">
        <div class="card-header header-border">
            Add additional sparepart from
            <div class="btn-actions-pane-right">
                <div class="nav">
                    <a data-toggle="tab" href="#tab-eg2-0" class="btn-tab btn-wide active btn btn-outline-success btn-md" data-desc="sparepaer-inventory">My Inventory</a>
                    <a data-toggle="tab" href="#tab-eg2-1" class="btn-tab btn-wide mr-1 ml-1  btn btn-outline-success btn-md" data-desc="sparepaer-company">Company </a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <input type="hidden" name="type_part" id="type_part" value="my-inventory">
            <div class="tab-content">
                <div class="tab-pane active" id="tab-eg2-0" role="tabpanel">
                    <div class="dynamic_sparepart">
                        <div class="position-relative row form-group">
                            <div class="col-sm-6">
                                Name
                            </div>
                            <div class="col-sm-2">
                                Price
                            </div>
                            <div class="col-sm-2">
                                Unit
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <div class="col-sm-6">
                                <input type="text" placeholder="name" name="teknisi_sparepart_name[]" data-uniq="xxx" class="form-control teknisi_sparepart_name_input-xxx" />
                            </div>
                            <div class="col-sm-2">
                                <input type="text" placeholder="price" name="teknisi_sparepart_price[]" data-uniq="xxx" class="form-control teknisi_sparepart_price_i teknisi_sparepart_price_input-xxx" />
                            </div>
                            <div class="col-sm-2">
                                <input type="text" placeholder="unit" name="teknisi_sparepart_qty[]" data-uniq="xxx" class="form-control teknisi_sparepart_qty_i teknisi_sparepart_qty_input-xxx" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-eg2-1" role="tabpanel">
                    <div class="dynamic_sparepart_company">
                        <div class="position-relative row form-group">
                            <div class="col-sm-3">
                                Warehouse
                            </div>
                            <div class="col-sm-4">
                                Name
                            </div>
                            <div class="col-sm-2">
                                Price
                            </div>
                            <div class="col-sm-2">
                                Unit
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <div class="col-sm-3">
                                <select class="form-control select-warehouse select-warehouse-xxx" name="warehouse_id[]" data-uniq="xxx" style="width:100%">
                                    <option value="" selected="selected">--Pilih--</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control select-inventory select-inventory-xxx" name="inventory_id[]" data-uniq="xxx" style="width:100%">
                                    <option value="" selected="selected">--Pilih--</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" data-uniq="xxx" class="form-control inventory-price-xxx inventory-price" name="inventory_price[]" disabled />
                            </div>
                            <div class="col-sm-2">
                                <input type="text" data-uniq="xxx" placeholder="unit" name="inventory_qty[]" class="form-control inventory-unit inventory-unit-xxx" value="0" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Save Change</button>
        </div>
    </div>
</form>
@endif

@if(count($sparepart_details) > 0)
<form id="sparepart-detail-form">
    <input type="hidden" id="sparepart_detail_order_id" value="{{ $order->id }}" name="sparepart_detail_order_id">
    <input type="hidden" name="type_part" id="type_part" value="my-inventory">
    <div class="card">
        <div class="card-header header-border">
            Technician Inventory
        </div>
        <div class="card-body">
            <div class="dynamic_sparepart">
                <div class="position-relative row form-group">
                    <div class="col-sm-6">
                        Name
                    </div>
                    <div class="col-sm-2">
                        Price
                    </div>
                    <div class="col-sm-2">
                        Unit
                    </div>
                </div>
                @foreach($sparepart_details as $no => $sparepart_detail)
                <?php $uniq = time(); ?>
                <div class="position-relative row form-group dynamic_sparepart_content" id="my_part_row_{{ $uniq }}">
                    <input type="hidden" value="{{ $sparepart_detail->id }}" name="sparepart_detail_id[]" />
                    <div class="col-sm-6">
                        <input type="text" placeholder="name" name="teknisi_sparepart_name[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_name_input-{{ $uniq }}" value="{{ $sparepart_detail->name_sparepart }}" />
                    </div>
                    <div class="col-sm-2">
                        <input type="text" placeholder="price" name="teknisi_sparepart_price[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_price_i teknisi_sparepart_price_input-{{ $uniq }}" value="{{ $sparepart_detail->price }}" />
                    </div>
                    <div class="col-sm-2">
                        <input type="text" placeholder="unit" name="teknisi_sparepart_qty[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_qty_i teknisi_sparepart_qty_input-{{ $uniq }}" value="{{ $sparepart_detail->quantity }}" />
                    </div>
                    <div class="col-sm-2" style="padding-left:0px;">
                        <button type="button" data-sparepart_detail_id="{{ $sparepart_detail->id }}" data-uniq="{{ $uniq }}" class="btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Save Change</button>
        </div>
    </div>
</form>
@endif

@if(count($item_details) > 0)
<form id="sparepart-detail-form">
    <input type="hidden" id="sparepart_detail_order_id" value="{{ $order->id }}" name="sparepart_detail_order_id">
    <input type="hidden" name="type_part" id="type_part" value="my-company">
    <div class="card">
        <div class="card-header header-border">
            Company Inventory
        </div>
        <div class="card-body">
            <div class="dynamic_sparepart_company">
                <div class="position-relative row form-group">
                    <div class="col-sm-3">
                        Warehouse
                    </div>
                    <div class="col-sm-4">
                        Name
                    </div>
                    <div class="col-sm-2">
                        Price
                    </div>
                    <div class="col-sm-2">
                        Unit
                    </div>
                </div>
                @foreach($item_details as $no => $item_detail)
                <?php $uniq = time(); ?>
                <div class="position-relative row dynamic_sparepart_company_content form-group" id="my_company_row_{{ $uniq }}">
                    <input type="hidden" value="{{ $item_detail->id }}" name="item_detail_id[]" />
                    <input type="hidden" class="select-inventory" value="{{ $item_detail->inventory_id }}" name="inventory_id[]" />
                    <div class="col-sm-3">
                        <label for="">{{ $item_detail->inventory->warehouse->name }}</label>
                    </div>
                    <div class="col-sm-4">
                        <label for="">{{ $item_detail->name_product }} (Available : {{ $item_detail->inventory->stock_available }})</label>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" data-uniq="{{ $uniq }}" class="form-control inventory-price-{{ $uniq }} inventory-price" value="{{ $item_detail->price }}" name="inventory_price[]" disabled />
                    </div>
                    <div class="col-sm-2">
                        <input type="number" data-uniq="{{ $uniq }}" data-available="{{ $item_detail->inventory->stock_available }}" placeholder="unit" name="inventory_qty[]" value="{{ $item_detail->quantity }}" class="form-control inventory-unit inventory-unit-{{ $uniq }}" value="0" />
                    </div>
                    <div class="col-sm-1" style="padding-left:0px;">
                        <button data-item_detail_id="{{ $item_detail->id }}" type="button" data-uniq="{{ $uniq }}" class="btn-transition btn btn-sm btn-danger btn_remove_my_company"><i class="fa fa-close"></i></button>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Save Change</button>
        </div>
    </div>
</form>
@endif
