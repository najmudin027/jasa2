@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <input type="hidden" id="id-btb" value="{{ $btb->id }}" />
            <div class="row">
                <div class="col-md-4">
                    <label>Company</label>
                    <p><strong>{{ $btb->company->name }}</strong></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                OUTLET
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Status Quotation</label>
                    <select name="status_quotation" class="status_quotation-select form-control">
                        <option value="">All</option>
                        @foreach($statuses as $status)
                        <option value="{{ $status->status_quotation }}">{{ $status->status_quotation }}</option>
                        @endforeach()
                    </select>
                </div>
                <div class="col-md-12">
                    <br />
                    <button type="button" class="btn btn-danger btn-sm filter-btb-outlet"><i class="fa fa-search"></i> SEARCH</button>
                    <button type="button" class="btn btn-primary btn-sm add-btb-outlet"><i class="fa fa-plus"></i> ADD OUTLET</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="table-btb-outlet" style="white-space: nowrap;">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<form id="form-btb-outlet">
    <div class="modal fade" id="modal-btb-outlet" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Outlet</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="btb-outlet-id">
                    <div class="form-group">
                        <label>Name</label>
                        <input name="name" placeholder="Name Outlet" type="text" class="form-control btb-outlet-name" required>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <input name="alamat" placeholder="alamat" type="text" class="btb-outlet-alamat form-control">
                    </div>
                    <div class="form-group">
                        <label>Jumlah Unit</label>
                        <input name="jumlah_unit" placeholder="Jumlah Unit" type="number" class="btb-outlet-jumlah_unit form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
                    <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="form-btb-outlet-item">
    <div class="modal fade" id="modal-btb-outlet-item" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title modal-title-btb-outlet-item"></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="btb-outlet-item-id">
                    <input type="hidden" name="business_to_business_outlet_transaction_id" id="business_to_business_outlet_transaction_id">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Unit Ke</label>
                                <select name="unit_ke" class="form-control btb-outlet-item-unit_ke" required>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Merek</label>
                                <input name="merek" placeholder="merek" type="text" class="btb-outlet-item-merek form-control" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Remark</label>
                                <input name="remark" placeholder="remark" type="text" class="btb-outlet-item-remark form-control">
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>No Quotation</label>
                                <input name="no_quotation" placeholder="No Quotation" type="text" class="btb-outlet-item-no_quotation form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>No Po</label>
                                <input name="no_po" placeholder="No PO" type="text" class="btb-outlet-item-no_po form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>No Invoice</label>
                                <input name="no_invoice" placeholder="No Invoice" type="text" class="btb-outlet-item-no_invoice form-control">
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nominal Quotation</label>
                                <input name="nominal_quot" placeholder="Nominal Quot" type="number" class="btb-outlet-item-nominal_quot form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status Quotation</label>
                                <input name="status_quotation" placeholder="Status Quotation" type="text" class="btb-outlet-item-status_quotation form-control">
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal Pembayaran</label>
                                <input autocomplete="off" name="tanggal_pembayaran" placeholder="Tanggal Pembayaran" type="text" class="datepick btb-outlet-item-tanggal_pembayaran form-control" />
                                <span>*Opsional</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal Quotation</label>
                                <input autocomplete="off" name="tanggal_quot" placeholder="Tanggal Quot" type="text" class="datepick btb-outlet-item-tanggal_quot form-control" />
                                <span>*Opsional</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal PO</label>
                                <input autocomplete="off" name="tanggal_po" placeholder="Tanggal PO" type="text" class="datepick btb-outlet-item-tanggal_po form-control" />
                                <span>*Opsional</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal Invoice</label>
                                <input autocomplete="off" name="tanggal_Invoice" placeholder="Tanggal Invoice" type="text" class="datepick btb-outlet-item-tanggal_Invoice form-control" />
                                <span>*Opsional</span>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Upload Berita acara</label>
                                <input name="file_berita_acara" type="file" class="btb-outlet-item-file_berita_acara form-control">
                                <span class="file_berita_acara-file_area">
                                    <br>
                                    <a href="" target="__blank" class="btn btn-xs btn-info file_berita_acara-link">
                                        <i class='fa fa-download'></i><span class="file_berita_acara-file_name"></span></a>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Upload File Quot</label>
                                <input name="file_quot" type="file" class="btb-outlet-item-file_quot form-control">
                                <span class="file_quot-file_area">
                                    <br>
                                    <a href="" target="__blank" class="btn btn-xs btn-info file_quot-link">
                                        <i class='fa fa-download'></i><span class="file_quot-file_name"></span></a>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Upload PO</label>
                                <input name="file_po" type="file" class="btb-outlet-item-file_po form-control">
                                <span class="file_po-file_area">
                                    <br>
                                    <a href="" target="__blank" class="btn btn-xs btn-info file_po-link">
                                        <i class='fa fa-download'></i><span class="file_po-file_name"></span></a>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Upload Invoice</label>
                                <input name="file_invoice" type="file" class="btb-outlet-item-file_invoice form-control">
                                <span class="file_invoice-file_area">
                                    <br>
                                    <a href="" target="__blank" class="btn btn-xs btn-info file_invoice-link">
                                        <i class='fa fa-download'></i><span class="file_invoice-file_name"></span></a>
                                </span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
                    <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="modal fade" id="modal-btb-log" data-backdrop="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">LOG</h4>
            </div>
            <div class="modal-body">
                <table id="table-btb"></table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-btb" data-backdrop="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detail Log</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive detail-log-content">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<style>
    .fixed-sidebar .app-main .app-main__outer {
        width: 100%;
    }

    .th-danger {
        background-color: #ff000033;
    }
</style>
@endsection

@section('script')
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-treegrid/0.2.0/css/jquery.treegrid.min.css" rel="stylesheet">
<link href="https://unpkg.com/bootstrap-table@1.18.1/dist/bootstrap-table.min.css" rel="stylesheet">
<link href="https://unpkg.com/bootstrap-table@1.18.2/dist/extensions/fixed-columns/bootstrap-table-fixed-columns.min.css" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-treegrid/0.2.0/js/jquery.treegrid.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.18.1/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.18.1/dist/extensions/treegrid/bootstrap-table-treegrid.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.18.2/dist/extensions/fixed-columns/bootstrap-table-fixed-columns.min.js"></script>
<script>
    Helper.datePick('.datepick');

    var $table = $('#table-btb-outlet');

    $table.bootstrapTable({
        uniqueId: 'id',
        showColumns: false,
        search: true,
        showRefresh: true,
        sortOrder: 'ASC',
        buttonsClass: 'primary',
        pagination: true,
        height:700,
        sidePagination: 'server',
        pageList: [10, 20, 50, 100],
        classes: 'table table-hover table-no-bordered table-nonfluid',
        url: Helper.apiUrl('/business_to_business/' + $('#id-btb').val() + '/outlet'),

        fixedColumns: true,
        fixedNumber: 2,

        idField: 'id',
        treeShowField: 'name',
        parentIdField: 'pid',
        onPostBody: function() {
            var columns = $table.bootstrapTable('getOptions').columns

            if (columns && columns[0][1].visible) {
                $table.treegrid({
                    treeColumn: 1,
                    onChange: function() {
                        $table.bootstrapTable('resetView')
                    }
                })
            }
        },
        columns: [{
                title: 'NO',
                sortable: true,
                formatter: function(value, row, index) {
                    margin = '';
                    if (row.is_parent === false) {
                        margin = '<span style="margin-left: 10px;"><span>';
                    }
                    return margin + row.number;
                }
            },
            {
                title: 'NAME',
                field: 'name',
                sortable: false,
                formatter: function(value, row, index) {
                    linkJumlahIsi = "<a target='__blank'  href='" + Helper.url('/admin/business_to_business/' + row.business_to_business_outlet_transaction.id + '/detail') + "'>" + row.name + "</a>";
                    if (row.is_parent === false) {
                        return row.business_to_business_outlet_detail_transaction.no_quotation;
                    }
                    return linkJumlahIsi;
                }
            },
            {
                title: 'ACTION',
                sortable: false,
                formatter: function(value, row, index) {
                    if (row.is_parent === false) {
                        btn_edit_item = '<button data-toggle="tooltip" title="edit" type="button" data-id="' + row.id + '" class="btn btn-success btn-xs edit-btb-outlet-item"><i class="fa fa-pencil"></i></button>';
                        btn_log_view = '<button data-toggle="tooltip" title="log" type="button" data-id="' + row.business_to_business_outlet_detail_transaction.id + '" class="btn btn-warning btn-xs log-btb-outlet-item"><i class="fa fa-history"></i></button>';
                        btn_add_item = '';
                        btn_delete_item = '<button data-toggle="tooltip" title="delete" type="button" data-id="' + row.business_to_business_outlet_detail_transaction.id + '" class="btn btn-danger btn-xs delete-btb-outlet-item"><i class="fa fa-trash"></i></button>';
                    } else {
                        btn_log_view = '<button data-toggle="tooltip" title="log" type="button" data-id="' + row.id + '" class="btn btn-warning btn-xs log-btb-outlet-header"><i class="fa fa-history"></i></button>';
                        btn_add_item = '<button data-toggle="tooltip" title="add" type="button" data-id="' + row.business_to_business_outlet_transaction.id + '" class="btn btn-primary btn-xs add-btb-outlet-item"><i class="fa fa-plus"></i> ADD</button>';
                        btn_edit_item = '<button data-toggle="tooltip" title="edit" type="button" data-id="' + row.id + '" class="btn btn-success btn-xs edit-btb-outlet-header"><i class="fa fa-pencil"></i></button>';
                        btn_delete_item = '<button data-toggle="tooltip" title="delete" type="button" data-id="' + row.business_to_business_outlet_transaction.id + '" class="btn btn-danger btn-xs delete-btb-outlet-header"><i class="fa fa-trash"></i></button>';
                    }

                    return btn_add_item + " " + btn_log_view + ' ' + btn_edit_item + " " + btn_delete_item;
                }
            },
            {
                title: 'JUMLAH UNIT',
                field: 'business_to_business_outlet_transaction.jumlah_unit',
                align: 'right',
                sortable: false
            },
            {
                title: 'UNIT KE',
                align: 'right',
                formatter: function(value, row, index) {
                    return row.business_to_business_outlet_detail_transaction === null ? '' : row.business_to_business_outlet_detail_transaction.unit_ke;
                }
            },
            {
                title: 'MEREK',
                sortable: false,
                formatter: function(value, row, index) {
                    return row.business_to_business_outlet_detail_transaction === null ? '' : row.business_to_business_outlet_detail_transaction.merek;
                }
            },
            {
                title: 'REMARK',
                sortable: false,
                formatter: function(value, row, index) {
                    return row.business_to_business_outlet_detail_transaction === null ? '' : row.business_to_business_outlet_detail_transaction.remark;
                }
            },
            {
                title: 'NO QUOTATION',
                sortable: false,
                formatter: function(value, row, index) {
                    return row.business_to_business_outlet_detail_transaction === null ? '' : row.business_to_business_outlet_detail_transaction.no_quotation;
                }
            },
            {
                title: 'NO PO',
                sortable: false,
                formatter: function(value, row, index) {
                    return row.business_to_business_outlet_detail_transaction === null ? '' : row.business_to_business_outlet_detail_transaction.no_po;
                }
            },
            {
                title: 'NO INVOICE',
                sortable: false,
                formatter: function(value, row, index) {
                    return row.business_to_business_outlet_detail_transaction === null ? '' : row.business_to_business_outlet_detail_transaction.no_invoice;
                }
            },
            {
                title: 'NOMINAL QUOTATION',
                sortable: false,
                formatter: function(value, row, index) {
                    return row.business_to_business_outlet_detail_transaction === null ? '' : row.business_to_business_outlet_detail_transaction.nominal_quot;
                }
            },
            {
                title: 'STATUS QUOTATION',
                sortable: false,
                formatter: function(value, row, index) {
                    return row.business_to_business_outlet_detail_transaction === null ? '' : row.business_to_business_outlet_detail_transaction.status_quotation;
                }
            },
            {
                title: 'TANGGAL PEMBAYARAN',
                sortable: false,
                formatter: function(value, row, index) {
                    if (row.business_to_business_outlet_detail_transaction === null) {
                        return '-';
                    }

                    if (row.business_to_business_outlet_detail_transaction.tanggal_pembayaran === null) {
                        return '-';
                    }
                    return moment(row.business_to_business_outlet_detail_transaction.tanggal_pembayaran).format("DD MMMM YYYY");
                }
            },
            {
                title: 'TANGGAL QUOTATION',
                sortable: false,
                formatter: function(value, row, index) {
                    if (row.business_to_business_outlet_detail_transaction === null) {
                        return '-';
                    }

                    if (row.business_to_business_outlet_detail_transaction.tanggal_quot === null) {
                        return '-';
                    }
                    return moment(row.business_to_business_outlet_detail_transaction.tanggal_quot).format("DD MMMM YYYY");
                }
            },
            {
                title: 'TANGGAL PO',
                sortable: false,
                formatter: function(value, row, index) {
                    if (row.business_to_business_outlet_detail_transaction === null) {
                        return '-';
                    }

                    if (row.business_to_business_outlet_detail_transaction.tanggal_po === null) {
                        return '-';
                    }
                    return moment(row.business_to_business_outlet_detail_transaction.tanggal_po).format("DD MMMM YYYY");
                }
            },
            {
                title: 'TANGGAL INVOICE',
                sortable: false,
                formatter: function(value, row, index) {
                    if (row.business_to_business_outlet_detail_transaction === null) {
                        return '-';
                    }

                    if (row.business_to_business_outlet_detail_transaction.tanggal_Invoice === null) {
                        return '-';
                    }

                    return moment(row.business_to_business_outlet_detail_transaction.tanggal_Invoice).format("DD MMMM YYYY");
                }
            },
            {
                title: 'FILE BERITA ACARA',
                sortable: false,
                align: 'center',
                formatter: function(value, row, index) {
                    if (row.business_to_business_outlet_detail_transaction === null) {
                        return '-';
                    }
                    link = "<a target='__blank' href='" + Helper.url('/admin/business_to_business/' + row.business_to_business_outlet_detail_transaction.uniq_key + '/download?type=file_berita_acara&exptime=' + new Date().getTime()) + "' class='btn btn-xs btn-info'><i class='fa fa-download'></i></a>";

                    return row.business_to_business_outlet_detail_transaction.file_berita_acara === null ? '-' : link;

                }
            },
            {
                title: 'FILE QUOTATION',
                sortable: false,
                align: 'center',
                formatter: function(value, row, index) {
                    if (row.business_to_business_outlet_detail_transaction === null) {
                        return '-';
                    }
                    link = "<a target='__blank' href='" + Helper.url('/admin/business_to_business/' + row.business_to_business_outlet_detail_transaction.uniq_key + '/download?type=file_quot&exptime=' + new Date().getTime()) + "' class='btn btn-xs btn-info'><i class='fa fa-download'></i></a>";

                    return row.business_to_business_outlet_detail_transaction.file_quot === null ? '-' : link;

                }
            },
            {
                title: 'FILE PO',
                sortable: false,
                align: 'center',
                formatter: function(value, row, index) {
                    if (row.business_to_business_outlet_detail_transaction === null) {
                        return '-';
                    }
                    link = "<a target='__blank' href='" + Helper.url('/admin/business_to_business/' + row.business_to_business_outlet_detail_transaction.uniq_key + '/download?type=file_po&exptime=' + new Date().getTime()) + "' class='btn btn-xs btn-info'><i class='fa fa-download'></i></a>";

                    return row.business_to_business_outlet_detail_transaction.file_po === null ? '-' : link;

                }
            },
            {
                title: 'FILE INVOICE',
                sortable: false,
                align: 'center',
                formatter: function(value, row, index) {
                    if (row.business_to_business_outlet_detail_transaction === null) {
                        return '-';
                    }
                    link = "<a target='__blank' href='" + Helper.url('/admin/business_to_business/' + row.business_to_business_outlet_detail_transaction.uniq_key + '/download?type=file_invoice&exptime=' + new Date().getTime()) + "' class='btn btn-xs btn-info'><i class='fa fa-download'></i></a>";

                    return row.business_to_business_outlet_detail_transaction.file_invoice === null ? '-' : link;

                }
            },
            {
                title: 'USER CREATED',
                sortable: false,
                formatter: function(value, row, index) {
                    if (row.business_to_business_outlet_detail_transaction !== null) {
                        return row.business_to_business_outlet_detail_transaction.user_create.name;
                    }
                    if (row.business_to_business_outlet_transaction !== null) {
                        return row.business_to_business_outlet_transaction.user_create.name;
                    }
                    return '-';
                }
            },
            {
                title: 'USER UPDATE',
                sortable: false,
                formatter: function(value, row, index) {
                    if (row.business_to_business_outlet_detail_transaction !== null) {
                        if (row.business_to_business_outlet_detail_transaction.user_update === null) {
                            return '-';
                        }
                        return row.business_to_business_outlet_detail_transaction.user_update.name;
                    }
                    if (row.business_to_business_outlet_transaction !== null) {
                        if (row.business_to_business_outlet_transaction.user_update === null) {
                            return '-';
                        }
                        return row.business_to_business_outlet_transaction.user_update.name;
                    }
                    return '-';
                }
            },
        ]
    });

    $('.filter-btb-outlet').click(function() {
        playload = '?status_quotation=' + $('.status_quotation-select').val();
        url = Helper.apiUrl('/business_to_business/' + $('#id-btb').val() + '/outlet') + playload
        $table.bootstrapTable('refresh', {
            url: url
        })
    })

    $('.add-btb-outlet').click(function() {
        kosongkanFieldHeader();
        $('#modal-btb-outlet').modal('show')
    })

    $(document).on('click', '.edit-btb-outlet-header', function() {
        id = $(this).attr('data-id');
        row = $table.bootstrapTable('getRowByUniqueId', id);
        isiFieldHeader(row);
        $('#modal-btb-outlet').modal('show');
    })

    $('#form-btb-outlet').submit(function(e) {
        e.preventDefault();
        Helper.loadingStart();

        data = Helper.serializeForm($(this));
        data.business_to_business_transaction_id = $('#id-btb').val();
        id = $('.btb-outlet-id').val();
        if (id === '') {
            Axios.post('/business_to_business/outlet', data)
                .then(function(response) {
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    $('#table-btb-outlet').bootstrapTable('refresh');
                    // hide modal
                    $('#modal-btb-outlet').modal('hide')
                    // kosongkan field
                    kosongkanFieldHeader()
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        } else {
            Axios.put('/business_to_business/' + id + '/outlet', data)
                .then(function(response) {
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    $('#table-btb-outlet').bootstrapTable('refresh');
                    // hide modal
                    $('#modal-btb-outlet').modal('hide')
                    // kosongkan field
                    kosongkanFieldHeader()
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    // kosongkan field
                    kosongkanFieldHeader()
                    Helper.loadingStop();
                });
        }
    })

    $(document).on('click', '.add-btb-outlet-item', function() {
        id = $(this).attr('data-id');
        row = $table.bootstrapTable('getRowByUniqueId', id);
        $('#modal-btb-outlet-item').modal('show');
        kosongkanFielDetail(row);
        $('#business_to_business_outlet_transaction_id').val(id);
        $('.modal-title-btb-outlet-item').text('ADD OUTLET ITEM')
    })

    $(document).on('click', '.edit-btb-outlet-item', function() {
        id = $(this).attr('data-id');
        rows = $table.bootstrapTable('getData');
        row = _.find(rows, function(o) {
            return o.id == id;
        });
        isiFielDetail(row);
        $('#modal-btb-outlet-item').modal('show');
        $('.modal-title-btb-outlet-item').text('EDIT OUTLET ITEM')
    })

    $(document).on('click', '.delete-btb-outlet-header', function() {
        var id = $(this).attr('data-id');

        Helper.confirmDelete(function() {
            Helper.loadingStart();
            Axios.delete('/business_to_business/' + id + '/outlet')
                .then(function(response) {
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    $('#table-btb-outlet').bootstrapTable('refresh');
                    // hide modal
                    $('#modal-btb-outlet').modal('hide')
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        })
    })

    $(document).on('click', '.delete-btb-outlet-item', function() {
        var id = $(this).attr('data-id');

        Helper.confirmDelete(function() {
            Helper.loadingStart();
            Axios.delete('/business_to_business/' + id + '/outlet_detail')
                .then(function(response) {
                    //send notif
                    Helper.successNotif('success delete');
                    // refresh
                    $('#table-btb-outlet').bootstrapTable('refresh');
                    // hide modal
                    $('#modal-btb-outlet').modal('hide')
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        })
    })

    $('#form-btb-outlet-item').submit(function(e) {
        e.preventDefault();
        Helper.loadingStart();
        data = Helper.serializeForm($(this));
        formData = generateFormDataDetail(data);

        id = $('.btb-outlet-item-id').val();
        if (id === '') {
            Axios.post('/business_to_business/outlet_detail', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    $('#table-btb-outlet').bootstrapTable('refresh');
                    // hide modal
                    $('#modal-btb-outlet-item').modal('hide')

                    kosongkanFielDetail();

                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        } else {
            Axios.post('/business_to_business/' + id + '/outlet_detail', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    $('#table-btb-outlet').bootstrapTable('refresh');
                    // hide modal
                    $('#modal-btb-outlet-item').modal('hide')
                    kosongkanFielDetail();
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        }
    })

    const kosongkanFieldHeader = function() {
        $('.btb-outlet-alamat').val('');
        $('.btb-outlet-jumlah_unit').val('');
        $('.btb-outlet-name').val('');
        $('.btb-outlet-id').val('');
    }

    const isiFieldHeader = function(row) {
        $('.btb-outlet-alamat').val(row.business_to_business_outlet_transaction.alamat);
        $('.btb-outlet-jumlah_unit').val(row.business_to_business_outlet_transaction.jumlah_unit);
        $('.btb-outlet-name').val(row.business_to_business_outlet_transaction.name);
        $('.btb-outlet-id').val(row.business_to_business_outlet_transaction.id);
    }

    const kosongkanFielDetail = function(row = null) {
        $('.btb-outlet-item-file_berita_acara').val('');
        $('.btb-outlet-item-file_quot').val('');
        $('.btb-outlet-item-file_po').val('');
        $('.btb-outlet-item-file_invoice').val('');
        $('.file_berita_acara-file_area').hide();
        $('.file_quot-file_area').hide();
        $('.file_po-file_area').hide();
        $('.file_invoice-file_area').hide();

        $('.btb-outlet-item-merek').val('');
        $('.btb-outlet-item-remark').val('');
        $('.btb-outlet-item-no_quotation').val('');
        $('.btb-outlet-item-no_po').val('');
        $('.btb-outlet-item-no_invoice').val('');
        $('.btb-outlet-item-nominal_quot').val('');
        $('.btb-outlet-item-tanggal_pembayaran').val('');
        $('.btb-outlet-item-tanggal_quot').val('');
        $('.btb-outlet-item-tanggal_po').val('');
        $('.btb-outlet-item-tanggal_Invoice').val('');
        $('.btb-outlet-item-unit_ke').val('');
        $('.btb-outlet-item-id').val('');
        $('.btb-outlet-item-status_quotation').val('');
        $('.btb-outlet-item-unit_ke').empty().append('<option value="">Select Unit Ke</option>');
        if (row !== null) {
            var i;
            var jumlah_unit = row.business_to_business_outlet_transaction.jumlah_unit;
            var $selectUnitKe = $('.btb-outlet-item-unit_ke');
            for (i = 1; i <= jumlah_unit; i++) {
                $selectUnitKe.append('<option value="' + i + '">' + i + '</option>')
            }
            console.log('kosongkanFielDetail', row.business_to_business_outlet_transaction)
        }
    }

    const isiFielDetail = function(row) {
        detail = row.business_to_business_outlet_detail_transaction;

        $('.btb-outlet-item-file_berita_acara').val('');
        $('.btb-outlet-item-file_quot').val('');
        $('.btb-outlet-item-file_po').val('');
        $('.btb-outlet-item-file_invoice').val('');

        $('.file_berita_acara-file_area').hide();
        $('.file_quot-file_area').hide();
        $('.file_po-file_area').hide();
        $('.file_invoice-file_area').hide();

        if (detail.file_berita_acara != null) {
            link = Helper.url('/admin/business_to_business/' + detail.uniq_key + '/download?type=file_berita_acara&exptime=' + new Date().getTime());
            $('.file_berita_acara-file_area').show();
            $('.file_berita_acara-file_name').text(detail.file_berita_acara);
            $('.file_berita_acara-link').attr('href', link);
        }

        if (detail.file_quot != null) {
            link = Helper.url('/admin/business_to_business/' + detail.uniq_key + '/download?type=file_quot&exptime=' + new Date().getTime());
            $('.file_quot-file_area').show();
            $('.file_quot-file_name').text(detail.file_quot);
            $('.file_quot-link').attr('href', link);
        }

        if (detail.file_po != null) {
            link = Helper.url('/admin/business_to_business/' + detail.uniq_key + '/download?type=file_po&exptime=' + new Date().getTime());
            $('.file_po-file_area').show();
            $('.file_po-file_name').text(detail.file_po);
            $('.file_po-link').attr('href', link);
        }

        if (detail.file_invoice != null) {
            link = Helper.url('/admin/business_to_business/' + detail.uniq_key + '/download?type=file_invoice&exptime=' + new Date().getTime());
            $('.file_invoice-file_area').show();
            $('.file_invoice-file_name').text(detail.file_invoice);
            $('.file_invoice-link').attr('href', link);
        }

        $('#business_to_business_outlet_transaction_id').val(row.business_to_business_outlet_detail_transaction.business_to_business_outlet_transaction_id);
        $('.btb-outlet-item-merek').val(row.business_to_business_outlet_detail_transaction.merek);
        $('.btb-outlet-item-remark').val(row.business_to_business_outlet_detail_transaction.remark);
        $('.btb-outlet-item-no_quotation').val(row.business_to_business_outlet_detail_transaction.no_quotation);
        $('.btb-outlet-item-no_po').val(row.business_to_business_outlet_detail_transaction.no_po);
        $('.btb-outlet-item-no_invoice').val(row.business_to_business_outlet_detail_transaction.no_invoice);
        $('.btb-outlet-item-nominal_quot').val(row.business_to_business_outlet_detail_transaction.nominal_quot);
        $('.btb-outlet-item-tanggal_pembayaran').val(row.business_to_business_outlet_detail_transaction.tanggal_pembayaran);
        $('.btb-outlet-item-tanggal_quot').val(row.business_to_business_outlet_detail_transaction.tanggal_quot);
        $('.btb-outlet-item-tanggal_po').val(row.business_to_business_outlet_detail_transaction.tanggal_po);
        $('.btb-outlet-item-tanggal_Invoice').val(row.business_to_business_outlet_detail_transaction.tanggal_Invoice);
        $('.btb-outlet-item-status_quotation').val(row.business_to_business_outlet_detail_transaction.status_quotation);
        $('.btb-outlet-item-id').val(row.business_to_business_outlet_detail_transaction.id);

        $('.btb-outlet-item-unit_ke').empty().append('<option value="">Select Unit Ke</option>');
        if (row !== null) {
            var i;
            var jumlah_unit = row.business_to_business_outlet_transaction.jumlah_unit;
            var $selectUnitKe = $('.btb-outlet-item-unit_ke');
            for (i = 1; i <= jumlah_unit; i++) {
                $selectUnitKe.append('<option value="' + i + '">' + i + '</option>')
            }
            console.log('isiFielDetail', row)
        }
        $('.btb-outlet-item-unit_ke').val(row.business_to_business_outlet_detail_transaction.unit_ke).change();
    }

    const generateFormDataDetail = function(input) {
        var formData = new FormData();

        _.each(input, function(val, key) {
            formData.append(key, val);
        })

        file_berita_acara = $('.btb-outlet-item-file_berita_acara').prop('files')[0];
        if (file_berita_acara) {
            formData.append("file_berita_acara", file_berita_acara);
        }

        file_quot = $('.btb-outlet-item-file_quot').prop('files')[0];
        if (file_quot) {
            formData.append("file_quot", file_quot);
        }

        file_po = $('.btb-outlet-item-file_po').prop('files')[0];
        if (file_po) {
            formData.append("file_po", file_po);
        }

        file_invoice = $('.btb-outlet-item-file_invoice').prop('files')[0];
        if (file_invoice) {
            formData.append("file_invoice", file_invoice);
        }

        return formData;
    }
</script>


<script>
    var $tableLog = $('#table-btb');

    $tableLog.bootstrapTable({
        uniqueId: 'id',
        showColumns: false,
        search: false,
        showRefresh: true,
        sortOrder: 'ASC',
        pagination: true,
        sidePagination: 'server',
        pageList: [10, 20, 50, 100],
        classes: 'table table-hover table-no-bordered table-nonfluid',
        url: Helper.apiUrl('/business_to_business/log'),
        columns: [{
                title: 'NO',
                sortable: true,
                formatter: function(value, row, index) {
                    return index + 1;
                }
            },
            {
                title: 'TYPE',
                field: 'log_type',
                formatter: function(value, row, index) {
                    switch (row.log_type) {
                        case 'create':
                            return '<span class="badge badge-primary">' + row.log_type + '</span>';
                            break;

                        case 'edit':
                            return '<span class="badge badge-success">' + row.log_type + '</span>';
                            break;

                        case 'delete':
                            return '<span class="badge badge-danger">' + row.log_type + '</span>';
                            break;

                        default:
                            return row.log_type;
                            break;
                    }
                }
            },
            {
                title: 'MODUL',
                field: 'log_modul',
            },
            {
                title: 'USER',
                field: 'user.name',
            },
            {
                title: 'DATE TIME',
                formatter: function(value, row, index) {
                    return moment(row.created_at).format("DD MMMM YYYY HH:mm:ss");
                }
            },
            {
                title: 'ACTION',
                sortable: false,
                align: 'center',
                formatter: function(value, row, index) {
                    detail_btn = '<button type="button" data-id="' + row.id + '" class="btn btn-danger btn-xs btn-log-detail"><i class="fa fa-eye"></i> Detail</button>';
                    return detail_btn;
                }
            },
        ]
    });

    $(document).on('click', '.log-btb-outlet-header', function() {
        var id = $(this).attr('data-id');

        playload = '?business_to_business_outlet_transaction_id=' + id;
        url = Helper.apiUrl('/business_to_business/log') + playload
        $tableLog.bootstrapTable('refresh', {
            url: url
        })

        $('#modal-btb-log').modal('show');
    })

    $(document).on('click', '.log-btb-outlet-item', function() {
        var id = $(this).attr('data-id');

        playload = '?business_to_business_outlet_detail_transaction_id=' + id;
        url = Helper.apiUrl('/business_to_business/log') + playload
        $tableLog.bootstrapTable('refresh', {
            url: url
        })

        $('#modal-btb-log').modal('show');
    })

    $(document).on('click', '.btn-log-detail', function() {
        var id = $(this).attr('data-id');
        row = $tableLog.bootstrapTable('getRowByUniqueId', id);
        modalContent(row, row.before, row.after);
    })

    function modalContent(row, before, after) {
        var template = `
            <table class="table">
                <thead>
                    <tr>
                        <th>-</th>
                        <th>Data baru</th>
                        <th>Data lama</th>
                    </tr>
        `;

        if (row.log_type === 'delete' && after === null) {
            if (before !== null) {
                _.each(before, function(val, key) {
                    template += '<tr>';
                    switch (key) {
                        case 'company':
                            template += generateCompanyJson(val, null);
                            break;

                        case 'business_to_business_transaction':
                            template += generateBusinessToBusinessTransactionJson(val, null);
                            break;
                        case 'created_at':
                            break;
                        case 'updated_at':
                            break;
                        case 'user_created':
                            break;
                        case 'user_updated':
                            break;
                        case 'business_to_business_outlet_transaction':
                            break;

                        case 'user_create':
                            user = val === null ? '' : val.name;
                            template += "<th>user create</th>";
                            template += "<th>DELETED</th>";
                            template += "<th>" + user + "</th>";
                            break;

                        case 'user_update':
                            user = val === null ? '' : val.name;
                            template += "<th>user update</th>";
                            template += "<th>DELETED</th>";
                            template += "<th>" + user + "</th>";
                            break;

                        default:
                            template += "<th>" + key + "</th>";
                            template += "<th>DELETED</th>";
                            template += "<th>" + val + "</th>";
                            break;
                    }
                    template += '</tr>';
                })
            }
        } else {
            if (after !== null) {
                _.each(after, function(val, key) {
                    template += '<tr>';
                    template += jsonTemplate(row, before, key, val);
                    template += '</tr>';
                })
            }
        }

        template += `</thead></table>`;
        $('.detail-log-content').html(template);

        $('#modal-btb').modal('show')
    }

    function jsonTemplate(row, playload_before, playload_current_key, playload_current_val) {
        var table = '';

        after_key = playload_current_key;
        after_val = playload_current_val;

        switch (playload_current_key) {
            case 'company':
                table += generateCompanyJson(playload_current_val, null);
                break;

            case 'business_to_business_transaction':
                table += generateBusinessToBusinessTransactionJson(playload_current_val, null);
                break;
            case 'user_created':
                break;
            case 'created_at':
                break;
            case 'updated_at':
                break;
            case 'user_updated':
                break;
            case 'business_to_business_outlet_transaction':
                break;

            case 'user_create':
                user = playload_current_val === null ? '' : playload_current_val.name;
                table += "<th>user create</th>";
                table += "<th>" + user + "</th>";
                break;

            case 'user_update':
                user = playload_current_val === null ? '' : playload_current_val.name;
                table += "<th>user update</th>";
                table += "<th>" + user + "</th>";
                break;

            case 'created_at':
                table += "<th>" + after_key + "</th>";
                table += "<th>" + (after_val === null ? '-' : moment(after_val).format("DD MMMM YYYY HH:mm:ss")) + "</th>";
                break;

            case 'updated_at':
                table += "<th>" + after_key + "</th>";
                table += "<th>" + (after_val === null ? '-' : moment(after_val).format("DD MMMM YYYY HH:mm:ss")) + "</th>";
                break;

            default:
                table += "<th>" + after_key + "</th>";
                table += "<th>" + after_val + "</th>";
                break;
        }

        if (playload_before != null) {
            valueBefore = playload_before[playload_current_key];
            before_key = playload_current_key;
            before_val = valueBefore;
            table = '';
            switch (playload_current_key) {
                case 'company':
                    table += generateCompanyJson(valueBefore);
                    break;

                case 'business_to_business_transaction':
                    table += generateBusinessToBusinessTransactionJson(playload_current_val, valueBefore);
                    break;

                case 'user_created':
                    break;
                case 'user_updated':
                    break;
                case 'business_to_business_outlet_transaction':
                    break;
                case 'created_at':
                    break;
                case 'updated_at':
                    break;
                case 'created_at':
                    user_before = playload_current_val === null ? '' : moment(playload_current_val).format("DD MMMM YYYY HH:mm:ss");
                    user_after = valueBefore === null ? '' : moment(valueBefore).format("DD MMMM YYYY HH:mm:ss");

                    className = user_before != user_after ? 'th-danger' : '';
                    table += "<th class='" + className + "'>" + before_key + "</th>";
                    table += "<th class='" + className + "'>" + user_after + "</th>";
                    table += "<th class='" + className + "'>" + user_before + "</th>";
                    break;

                case 'updated_at':
                    user_before = playload_current_val === null ? '' : moment(playload_current_val).format("DD MMMM YYYY HH:mm:ss");
                    user_after = valueBefore === null ? '' : moment(valueBefore).format("DD MMMM YYYY HH:mm:ss");

                    className = user_before != user_after ? 'th-danger' : '';
                    table += "<th class='" + className + "'>" + before_key + "</th>";
                    table += "<th class='" + className + "'>" + user_after + "</th>";
                    table += "<th class='" + className + "'>" + user_before + "</th>";
                    break;

                case 'user_create':
                    user_before = playload_current_val === null ? '' : playload_current_val.name;
                    user_after = valueBefore === null ? '' : valueBefore.name;

                    className = user_before != user_after ? 'th-danger' : '';
                    table += "<th class='" + className + "'>" + before_key + "</th>";
                    table += "<th class='" + className + "'>" + user_after + "</th>";
                    table += "<th class='" + className + "'>" + user_before + "</th>";
                    break;

                case 'user_update':
                    user_before = playload_current_val === null ? '' : playload_current_val.name;
                    user_after = valueBefore === null ? '' : valueBefore.name;

                    className = user_before != user_after ? 'th-danger' : '';
                    table += "<th class='" + className + "'>" + before_key + "</th>";
                    table += "<th class='" + className + "'>" + user_after + "</th>";
                    table += "<th class='" + className + "'>" + user_before + "</th>";
                    break;

                default:
                    className = playload_current_val != valueBefore ? 'th-danger' : '';
                    table += "<th class='" + className + "'>" + before_key + "</th>";
                    table += "<th class='" + className + "'>" + playload_current_val + "</th>";
                    table += "<th class='" + className + "'>" + valueBefore + "</th>";
                    break;
            }
        }
        return table;
    }

    function generateCompanyJson(value) {
        if (value === null) {
            return '';
        }
        var table = '';
        table += '<table class="table">';
        table += '<p>Company Detail</p>';
        _.each(value, function(val, key) {
            table += '<tr>';
            table += "<th>" + key + "</th>";
            table += "<th>" + val + "</th>";
            table += '</tr>';
        })
        table += '</table>';

        return table;
    }

    function generateBusinessToBusinessTransactionJson(playload_current_val, valueBefore) {
        if (playload_current_val === null) {
            return '';
        }

        var table = '';
        table += '<table class="table">';
        table += '<p>business to business transaction Detail</p>';
        _.each(playload_current_val, function(val, key) {
            if (key === 'company') {
                before_val = valueBefore === null ? '' : valueBefore[key].name;
                table += '<tr>';
                table += "<th>" + key + "</th>";
                table += "<th>" + val.name + "</th>";
                table += "<th>" + before_val + "</th>";
                table += '</tr>';
            } else {
                table += '<tr>';
                table += "<th>" + key + "</th>";
                table += "<th>" + val + "</th>";
                table += "<th>" + (valueBefore === null ? '-' : valueBefore) + "</th>";
                table += '</tr>';
            }
        })
        table += '</table>';

        return table;
    }
</script>
@endsection