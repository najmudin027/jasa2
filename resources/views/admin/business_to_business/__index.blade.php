@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Search
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <label>Company</label>
                    <select name="company_id" class="comapany-select form-control">
                        <option value="">Select Company</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label>Outlet Name</label>
                    <input class="outlet-name form-control" name="outlet"/>
                </div>
                <div class="col-md-12">
                    <br>
                    <button type="button" class="btn btn-primary btn-sm btn-btb-search"><i class="fa fa-search"></i> Search</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table id="table-btb" class="table table-hover table-bordered" style="width:100%; white-space: nowrap;">
                <thead>
                    <tr>
                        <th></th>
                        <th>NO</th>
                        <th>COMPANY NAME</th>
                        <th>USER CREATED</th>
                        <th>USER UPDATED</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-body">
            <button class="btn btn-primary btn-sm add-btb"><i class="fa fa-plus"></i> Add New</button>
            <a href="{{ url('/admin/business_to_business/log') }}" class="btn btn-success btn-sm">View Logs</a>
        </div>
    </div>
</div>
<form id="form-btb">
    <div class="modal fade" id="modal-btb" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Badge</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Company</label>
                        <select class="form-control comapany-select" id="company_id" name="company_id" style="width:100%" required>
                            <option value="">Select Company</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
                    <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
</form>

<style>
    .fixed-sidebar .app-main .app-main__outer {
        width: 100%;
    }

    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
@endsection

@section('script')
<script>
    globalCRUD
        .select2Static(".comapany-select", '/company/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })

    $('.add-btb').click(function() {
        $('#modal-btb').modal('show')
    })

    $('#form-btb').submit(function(e) {
        e.preventDefault();

        data = Helper.serializeForm($(this));

        Axios.post('/business_to_business', data)
            .then(function(response) {
                //send notif
                Helper.successNotif(response.data.msg);
                // redirect
                window.location.href = Helper.redirectUrl('/admin/business_to_business/' + response.data.data.id + '/show');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
    })


    $(document).ready(function() {
        var table = $('#table-btb').DataTable({
            processing: true,
            serverSide: true,
            select: false,
            dom: 'Bflrtip',
            responsive: true,
            order: [2, 'asc'],
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/business_to_business'),
                "type": "get",
            },
            columns: [{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    sortable: false,
                    searchable: false,
                    "defaultContent": ''
                },
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                    sortable: false,
                    searchable: false,
                    width: "10%"
                },
                {
                    data: "company_id",
                    name: "company_id",
                    render: function(data, type, row) {
                        link = "<a href='" + Helper.url('/admin/business_to_business/' + row.id + '/show') + "'>" + row.company.name + "</a>";
                        return link;
                    }
                },
                {
                    data: "user_created",
                    name: "user_created",
                    render: function(data, type, row) {
                        if (row.user_create !== null) {
                            return row.user_create.name;
                        }
                        return '';
                    }
                },
                {
                    data: "user_updated",
                    name: "user_updated",
                    render: function(data, type, row) {
                        if (row.user_update !== null) {
                            return row.user_update.name;
                        }
                        return '';
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        view_link = "<a class='btn btn-primary btn-sm' href='" + Helper.url('/admin/business_to_business/' + row.id + '/show') + "'><i class='fa fa-eye'></i></a>";
                        btn_delete_item = '<button type="button" data-id="' + row.id + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
                        return view_link + ' ' + btn_delete_item;
                    }
                },
            ],
        });

        $('#table-btb tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(formatx(row.data())).show();
                tr.addClass('shown');
            }
        });

        $('.btn-btb-search').click(function() {
            company_id = $('.comapany-select').val();
            playload = '?company_id=' + company_id + '&outlet_name=' + $('.outlet-name').val();

            url = Helper.apiUrl('/business_to_business') + playload;

            table.ajax.url(url).load();
        })

        $(document).on('click', '.delete-btb', function() {
            var id = $(this).attr('data-id');

            Helper.confirmDelete(function() {
                Helper.loadingStart();
                Axios.delete('/business_to_business/' + id)
                    .then(function(response) {
                        //send notif
                        Helper.successNotif(response.data.msg);
                        // refresh
                        table.ajax.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            })
        })

    });

    function formatx(d) {
        console.log(d)
        template = '';
        template += '<table class="table" style="margin: 10px;">';
        template += '<tr>';
        template += '<td></td>';
        template += '<th>Outlet Name</th>';
        template += '<th>Alamat</th>';
        template += '<th>Jumlah Unit</th>';
        template += '<th>Jumlah Unit Terisi</th>';
        template += '</tr>';
        _.each(d.business_to_business_outlet_transactions, function(o, key) {
            linkJumlahUnit = "<a target='__blank'  href='" + Helper.url('/admin/business_to_business/' + o.id + '/detail') + "'>" + o.jumlah_unit + "</a>";
            linkJumlahIsi = "<a target='__blank'  href='" + Helper.url('/admin/business_to_business/' + o.id + '/detail') + "'>" + o.business_to_business_outlet_detail_transactions.length + "</a>";
            bg = (o.jumlah_unit == o.business_to_business_outlet_detail_transactions.length) ? 'background: #a4646438;' : '';

            template += '<tr style="'+bg+'">';
            template += '<td></td>';
            template += '<td>' + o.name + '</td>';
            template += '<td>' + o.alamat + '</td>';
            template += '<td>' + linkJumlahUnit + '</td>';
            template += '<td>' + linkJumlahIsi + '</td>';
            template += '</tr>';
        })
        template += '</table>';
        return template;
    }
</script>
@endsection