@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table id="table-btb"></table>
        </div>
    </div>
</div>

<form id="form-btb">
    <div class="modal fade" id="modal-btb" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detail Log</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive detail-log-content">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<style>
    .th-danger {
        background-color: #ff000033;
    }
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-treegrid/0.2.0/css/jquery.treegrid.min.css" rel="stylesheet">
<link href="https://unpkg.com/bootstrap-table@1.18.1/dist/bootstrap-table.min.css" rel="stylesheet">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-treegrid/0.2.0/js/jquery.treegrid.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.18.1/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.18.1/dist/extensions/treegrid/bootstrap-table-treegrid.min.js"></script>
<script>
    var $table = $('#table-btb');

    $table.bootstrapTable({
        uniqueId: 'id',
        showColumns: false,
        search: true,
        showRefresh: true,
        sortOrder: 'ASC',
        pagination: true,
        sidePagination: 'server',
        pageList: [10, 20, 50, 100],
        classes: 'table table-hover table-no-bordered table-nonfluid',
        url: Helper.apiUrl('/business_to_business/log'),
        columns: [{
                title: 'NO',
                sortable: true,
                formatter: function(value, row, index) {
                    return index + 1;
                }
            },
            {
                title: 'NOMOR TRANSAKSI',
                field: 'no_transaksi',
                formatter: function(value, row, index) {
                    link = "<a target='__blank' href='" + Helper.url('/admin/business_to_business/' + row.business_to_business_transaction_id + '/show') + "'>" + row.no_transaksi + "</a>";
                    return link;
                }
            },
            {
                title: 'TYPE',
                field: 'log_type',
                formatter: function(value, row, index) {
                    switch (row.log_type) {
                        case 'create':
                            return '<span class="badge badge-primary">' + row.log_type + '</span>';
                            break;

                        case 'edit':
                            return '<span class="badge badge-success">' + row.log_type + '</span>';
                            break;

                        case 'delete':
                            return '<span class="badge badge-danger">' + row.log_type + '</span>';
                            break;

                        default:
                            return row.log_type;
                            break;
                    }
                }
            },
            {
                title: 'MODUL',
                field: 'log_modul',
            },
            {
                title: 'USER',
                field: 'user.name',
            },
            {
                title: 'DATE TIME',
                formatter: function(value, row, index) {
                    return moment(row.created_at).format("DD MMMM YYYY HH:mm:ss");
                }
            },
            {
                title: 'ACTION',
                sortable: false,
                align: 'center',
                formatter: function(value, row, index) {
                    detail_btn = '<button type="button" data-id="' + row.id + '" class="btn btn-danger btn-xs btn-log-detail"><i class="fa fa-eye"></i> Detail</button>';
                    return detail_btn;
                }
            },
        ]
    });

    $(document).on('click', '.btn-log-detail', function() {
        var id = $(this).attr('data-id');
        row = $table.bootstrapTable('getRowByUniqueId', id);
        modalContent(row, row.before, row.after);
    })

    function modalContent(row, before, after) {
        var template = `
            <table class="table">
                <thead>
                    <tr>
                        <th>-</th>
                        <th>Data baru</th>
                        <th>Data lama</th>
                    </tr>
        `;

        if (row.log_type === 'delete' && after === null) {
            if (before !== null) {
                _.each(before, function(val, key) {
                    template += '<tr>';
                    switch (key) {
                        case 'company':
                            template += generateCompanyJson(val, null);
                            break;

                        case 'business_to_business_transaction':
                            template += generateBusinessToBusinessTransactionJson(val, null);
                            break;

                        case 'user_created':
                            break;
                        case 'user_updated':
                            break;
                        case 'business_to_business_outlet_transaction':
                            break;

                        case 'user_create':
                            user = val === null ? '' : val.name;
                            template += "<th>user create</th>";
                            template += "<th>DELETED</th>";
                            template += "<th>" + user + "</th>";
                            break;

                        case 'user_update':
                            user = val === null ? '' : val.name;
                            template += "<th>user update</th>";
                            template += "<th>DELETED</th>";
                            template += "<th>" + user + "</th>";
                            break;

                        default:
                            template += "<th>" + key + "</th>";
                            template += "<th>DELETED</th>";
                            template += "<th>" + val + "</th>";
                            break;
                    }
                    template += '</tr>';
                })
            }
        } else {
            if (after !== null) {
                _.each(after, function(val, key) {
                    template += '<tr>';
                    template += jsonTemplate(row, before, key, val);
                    template += '</tr>';
                })
            }
        }

        template += `</thead></table>`;
        $('.detail-log-content').html(template);

        $('#modal-btb').modal('show')
    }

    function jsonTemplate(row, playload_before, playload_current_key, playload_current_val) {
        var table = '';

        after_key = playload_current_key;
        after_val = playload_current_val;

        switch (playload_current_key) {
            case 'company':
                table += generateCompanyJson(playload_current_val, null);
                break;

            case 'business_to_business_transaction':
                table += generateBusinessToBusinessTransactionJson(playload_current_val, null);
                break;

            case 'user_created':
                break;
            case 'user_updated':
                break;
            case 'business_to_business_outlet_transaction':
                break;

            case 'user_create':
                user = playload_current_val === null ? '' : playload_current_val.name;
                table += "<th>user create</th>";
                table += "<th>" + user + "</th>";
                break;

            case 'user_update':
                user = playload_current_val === null ? '' : playload_current_val.name;
                table += "<th>user update</th>";
                table += "<th>" + user + "</th>";
                break;

            case 'created_at':
                table += "<th>" + after_key + "</th>";
                table += "<th>" + (after_val === null ? '-' : moment(after_val).format("DD MMMM YYYY HH:mm:ss")) + "</th>";
                break;

            case 'updated_at':
                table += "<th>" + after_key + "</th>";
                table += "<th>" + (after_val === null ? '-' : moment(after_val).format("DD MMMM YYYY HH:mm:ss")) + "</th>";
                break;

            default:
                table += "<th>" + after_key + "</th>";
                table += "<th>" + after_val + "</th>";
                break;
        }

        if (playload_before != null) {
            valueBefore = playload_before[playload_current_key];
            before_key = playload_current_key;
            before_val = valueBefore;
            table = '';
            switch (playload_current_key) {
                case 'company':
                    table += generateCompanyJson(valueBefore);
                    break;

                case 'business_to_business_transaction':
                    table += generateBusinessToBusinessTransactionJson(playload_current_val, valueBefore);
                    break;

                case 'user_created':
                    break;
                case 'user_updated':
                    break;
                case 'business_to_business_outlet_transaction':
                    break;

                case 'created_at':
                    user_before = playload_current_val === null ? '' : moment(playload_current_val).format("DD MMMM YYYY HH:mm:ss");
                    user_after = valueBefore === null ? '' : moment(valueBefore).format("DD MMMM YYYY HH:mm:ss");

                    className = user_before != user_after ? 'th-danger' : '';
                    table += "<th class='" + className + "'>" + before_key + "</th>";
                    table += "<th class='" + className + "'>" + user_after + "</th>";
                    table += "<th class='" + className + "'>" + user_before + "</th>";
                    break;

                case 'updated_at':
                    user_before = playload_current_val === null ? '' : moment(playload_current_val).format("DD MMMM YYYY HH:mm:ss");
                    user_after = valueBefore === null ? '' : moment(valueBefore).format("DD MMMM YYYY HH:mm:ss");

                    className = user_before != user_after ? 'th-danger' : '';
                    table += "<th class='" + className + "'>" + before_key + "</th>";
                    table += "<th class='" + className + "'>" + user_after + "</th>";
                    table += "<th class='" + className + "'>" + user_before + "</th>";
                    break;

                case 'user_create':
                    user_before = playload_current_val === null ? '' : playload_current_val.name;
                    user_after = valueBefore === null ? '' : valueBefore.name;

                    className = user_before != user_after ? 'th-danger' : '';
                    table += "<th class='" + className + "'>" + before_key + "</th>";
                    table += "<th class='" + className + "'>" + user_after + "</th>";
                    table += "<th class='" + className + "'>" + user_before + "</th>";
                    break;

                case 'user_update':
                    user_before = playload_current_val === null ? '' : playload_current_val.name;
                    user_after = valueBefore === null ? '' : valueBefore.name;

                    className = user_before != user_after ? 'th-danger' : '';
                    table += "<th class='" + className + "'>" + before_key + "</th>";
                    table += "<th class='" + className + "'>" + user_after + "</th>";
                    table += "<th class='" + className + "'>" + user_before + "</th>";
                    break;

                default:
                    className = playload_current_val != valueBefore ? 'th-danger' : '';
                    table += "<th class='" + className + "'>" + before_key + "</th>";
                    table += "<th class='" + className + "'>" + playload_current_val + "</th>";
                    table += "<th class='" + className + "'>" + valueBefore + "</th>";
                    break;
            }
        }
        return table;
    }

    function generateCompanyJson(value) {
        if (value === null) {
            return '';
        }
        var table = '';
        table += '<table class="table">';
        table += '<p>Company Detail</p>';
        _.each(value, function(val, key) {
            table += '<tr>';
            table += "<th>" + key + "</th>";
            table += "<th>" + val + "</th>";
            table += '</tr>';
        })
        table += '</table>';

        return table;
    }

    function generateBusinessToBusinessTransactionJson(playload_current_val, valueBefore) {
        if (playload_current_val === null) {
            return '';
        }

        var table = '';
        table += '<table class="table">';
        table += '<p>business to business transaction Detail</p>';
        _.each(playload_current_val, function(val, key) {
            if (key === 'company') {
                before_val = valueBefore === null ? '' : valueBefore[key].name;
                table += '<tr>';
                table += "<th>" + key + "</th>";
                table += "<th>" + val.name + "</th>";
                table += "<th>" + before_val + "</th>";
                table += '</tr>';
            } else {
                table += '<tr>';
                table += "<th>" + key + "</th>";
                table += "<th>" + val + "</th>";
                table += "<th>" + (valueBefore === null ? '-' : valueBefore) + "</th>";
                table += '</tr>';
            }
        })
        table += '</table>';

        return table;
    }
</script>
@endsection