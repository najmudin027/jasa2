@extends('admin.home')
@section('content')

<form id="b2b-form-setting" style="display: contents;">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header header-bg">
                {{$title}}
            </div>
            <div class="card-body">
                @foreach ($getDataSettingB2b as $no => $settings)
                    <div class="form-group col-md-12">
                        @if ($settings->name == 'status_waranty')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" name="type[]" placeholder="" value="hours">
                                <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        @if ($settings->name == 'kerusakan')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" name="type[]" placeholder="" value="hours">
                                <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        @if ($settings->name == 'validity')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" name="type[]" placeholder="" value="hours">
                                <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        @if ($settings->name == 'payment')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" name="type[]" placeholder="" value="hours">
                                <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        @if ($settings->name == 'minimum_order')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" name="type[]" placeholder="" value="hours">
                                <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        @if ($settings->name == 'status_price')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" name="type[]" placeholder="" value="hours">
                                <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        @if ($settings->name == 'time_of_delivery')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" name="type[]" placeholder="" value="hours">
                                <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        @if ($settings->name == 'account_no')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" name="type[]" placeholder="" value="hours">
                                <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        @if ($settings->name == 'bank_name')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" name="type[]" placeholder="" value="hours">
                                <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        @if ($settings->name == 'bank_address')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" name="type[]" placeholder="" value="hours">
                                <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                            </div>
                        @endif
                    </div>

                    <div class="form-group col-md-12">
                        @if ($settings->name == 'beneficary')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" name="type[]" placeholder="" value="hours">
                                <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                            </div>
                        @endif
                    </div>
                @endforeach
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary" id="save_commission">Submit</button>
                </div>
            </div>
            
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    $('#b2b-form-setting').submit(function(e) {
        data = Helper.serializeForm($(this));

        Helper.loadingStart();
        // post data
        Axios.post(Helper.apiUrl('/business-to-business-settings/store'), data)
            .then(function(response) {
                Helper.successNotif('Success, General Setting Has Been saved');
                location.reload();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
@endsection