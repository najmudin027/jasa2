@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="main-card mb-3 card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                List Users
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <!-- <a href="{{ url('/admin/permission') }}" class="btn btn-sm btn-danger">Manage Permission</a>
                <a href="{{ url('/admin/role/show') }}" class="btn btn-sm btn-danger">Manage Role</a> -->

            </div>
        </div>
        <div class="card-body">
            <table id="form-user-table" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Active</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ url('/admin/user-business-to-business/create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Users</a>
        </div>
    </div>
</div>
<form id="form-change_password">
    <div class="modal fade" data-backdrop="false" id="modal-change_password" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Change Password
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="user_id" value="">
                    <label>New Password</label>
                    <div class="form-group">
                        <input name="new_password" placeholder="new password" type="password" class="form-control" required />
                    </div>
                    <label>New Password Confim</label>
                    <div class="form-group">
                        <input id="password-input" name="new_password_confirmation" placeholder="new password confirmation" type="password" class="form-control" required />
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SAVE
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        CLOSE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@section('script')
<script>
    var dt = globalCRUD.datatables({
        url: '/user-b-2-b/datatables',
        selector: '#form-user-table',
        columnsField: [
            {
                data: 'user.name',
                name: 'user.name',
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    url = Helper.url('/admin/user-business-to-business/detail/' + row.id);
                    return '<a href="' + url + '">' + row.name + '</a>';
                }
            },
            'email',
            'phone1',
            {
                data: 'id',
                name: 'id',
                orderable: false,
                render: function(data, type, row) {
                    if (row.status != 1) {
                        return "<button type='button' class='btn btn-sm btn-user-status btn-drak' data-id='" + row.id + "'><i style='font-size: 24px;' class='fa fa-square'></i></button>";
                    } else {
                        return "<button type='button' class='btn btn-sm btn-user-status btn-drak' data-id='" + row.id + "'><i class='fa fa-check-square' style='font-size: 24px;'></i></button>";
                    }
                }
            }
        ],
        actionLink: {
            only_icon: false,
            update: function(row) {
                return '/admin/user-business-to-business/update/' + row.id;
            },
            delete: function(row) {
                return '/user-business-to-business/' + row.id;
            },
            render: function(data, type, full, actionData) {
                btn_hapus = '<button class="btn btn-danger btn-sm btn-hapus-user" data-id="' + full.id + '"><i class="fa fa-trash"></i></button>';
                btn_cp = '<button class="btn btn-success btn-sm btn-change_password" data-id="' + full.id + '"><i class="fa fa-pencil"></i> Change Password</button>';
                if (full.name == 'admin') {
                    return btn_cp + " <a class='btn btn-info btn-sm' href='/admin/user-business-to-business/detail/" + full.id + "'><i class='fa fa-arrow-right'></i> Detail</a>";
                } else {
                    return  actionData.edit_btn + " " + btn_hapus;
                }
            }
        },
    })
</script>

<script>
    $(document).on('click', '.btn-change_password', function() {
        $('[name="user_id"]').val($(this).attr('data-id'))
        $('#modal-change_password').modal('show');
    })

    // ganti status
    $(document)
        .on('click', '.btn-user-status', function() {
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.put('/user-b-2-b/change_status/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('Success');
                        // reload table
                        dt.table.reloadTable();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })

        // ganti hapus
    $(document).on('click', '.btn-hapus-user', function() {
        id = $(this).attr('data-id');
        // bootbox.dialog("Warning!, are you sure you want to delete this user", function(result){ 
        bootbox.confirm("Apakah Anda yakin akan Menghapus User bussiness to Bussiness ini ?", function(result){ 
            if (result === true) {
                Axios.delete('/user-b-2-b/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('user has been deleted');
                        // reload table
                        dt.table.reloadTable();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            }
        });
    })

    $('#form-change_password').submit(function(e) {
        data = {
            id: $('[name="user_id"]').val(),
            new_password: $('[name="new_password"]').val(),
            new_password_confirmation: $('[name="new_password_confirmation"]').val(),
        };

        Helper.loadingStart();
        // post data
        Axios.put(Helper.apiUrl('/user/change_password/' + data.id), data)
            .then(function(response) {
                Helper.successNotif('Success Change Password');
                $('#modal-change_password').modal('hide');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
@endsection