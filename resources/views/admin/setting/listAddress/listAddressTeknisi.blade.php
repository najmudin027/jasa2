@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                List Address
            </div>
            <div class="btn-actions-pane-right text-capitalize" hidden>
                    <a href="{{ url('/teknisi/address/create') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add New Address
                    </a>
                
            </div>
        </div>
        <div class="card-body">
            <table id="table-list-address" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr align="center">
                        <th>Phone</th>
                        <th style="width:75%">Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>

    var table = $('#table-list-address').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        select: true,
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search..."
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/user_address/datatables/teknisi'),
            type: "get",

        },
        columns: [
            {
                data: "phone1",
                name: "phone1",
            },
            {
                data: "address",
                name: "address",
            },
            {
                data: "id",
                name: "id",
                orderable: false,
                searchable: false,
                render: function(data, type, full) {
                    return "<a href='/teknisi/address/update/"+ full.id +"' class='badge badge-success btn-sm ' class='badge badge-success btn-sm '  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-pencil'></i> </a>";
                }
            }
        ]
    });
</script>
@endsection
