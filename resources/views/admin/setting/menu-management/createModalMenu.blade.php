<!-- Create new item Modal -->
<div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        {{-- <form action="" class="form-horizontal" role="form" method="post"> --}}
        <div class="modal-header">
            <h4 class="modal-title">Add Menu</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="title" class="col-sm-12 col-form-label">Name</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" name="name_add">
                </div>
            </div>
            <div class="form-group">
                <label for="url" class="col-sm-12 col-form-label">Url</label>
                <div class="col-lg-10">
                <input type="text" class="form-control" name="url_name_add" id="url_name_add">
                </div>
            </div>
            <div class="form-group">
                <label for="url" class="col-sm-12 col-form-label">Module Name</label>
                <div class="col-lg-10">
                <input type="text" class="form-control" name="module" id="module">
                </div>
            </div>
            <div class="form-group">
                <label for="label" class="col-sm-12 col-form-label">Icon</label>
                <div class="col-lg-10">
                <input type="text" class="form-control icon-picker-add" name="icon_add" Placeholder="fa fa-address-book">
                <small>example fill text: "fa fa-address-book". here is the reference link for the icon <a href="https://fontawesome.com/v4.7.0/icons/" target="_blank">go to link</a></small>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" id="add">Save</button>
        </div>
        {{-- </form> --}}
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



