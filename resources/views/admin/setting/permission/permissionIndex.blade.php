@extends('admin.home')
@section('content')
<div class="app-main__inner">
    <div class="row">
        @include('admin.template._minisidebad')
        <div class="col-md-9">
            <div class="main-card mb-3 card">
                <div class="card-header">{{ $title }}</div>
                <div class="table-responsive"><br>
                    <div class="container">
                        <table id="example" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Guard Name</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="d-block text-center card-footer">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        // show Datatables
        var table = $('#example').DataTable( {
            processing: true,
            serverSide: true,
            select: true,
            dom: 'Bflrtip',
            responsive: true,
            language: {
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],

            columnDefs: [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            }],

            ajax: {
                "url": Helper.apiUrl("/permission/datatables"),
                "type": "get",

                "data": function ( d ) {
                    return $.extend( {}, d, {
                        "extra_search": $('#extra').val()
                    });
                }
            },
            columns: [
                { data : "DT_RowIndex", name: "DT_RowIndex" },
                { data : "name", name: "name" },
                { data : "guard_name", name: "guard_name" },
            ],
        });

        // Add Number Rows
        Helper.numberRow(table)
    </script>
@endsection
