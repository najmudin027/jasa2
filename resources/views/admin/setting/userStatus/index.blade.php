@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-user-status" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-user-status">Add User Status</button>
        </div>
    </div>
</div>
@include('admin.setting.userStatus.modalUserStatus')
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        url: '/user_status/datatables',
        selector: '#table-user-status',
        columnsField: ['id', 'name'],
        modalSelector: "#modal-user-status",
        modalButtonSelector: ".add-user-status",
        modalFormSelector: "#form-user-status",
        actionLink: {
            store: function() {
                return "/user_status";
            },
            update: function(row) {
                return "/user_status/" + row.id;
            },
            delete: function(row) {
                return "/user_status/" + row.id;
            },
        }
    })
</script>
@endsection