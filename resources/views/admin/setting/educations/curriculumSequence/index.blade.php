@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add-curriculum-sequence">Add Curriculum</button>
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-curriculum-sequence" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Sequence Name</th>
                        <th>Dev Program</th>
                        <th>Curriculum</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
            <button class="btn btn-primary btn-sm add-curriculum-sequence">Add Curriculum</button>
        </div>
    </div>
</div>
@include('admin.setting.educations.curriculumSequence.modalCurriculumSequence')
@endsection

@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    globalCRUD
        .select2("#dev_program_id", '/development_program/select2', function(item){
            return {
                id:item.id,
                text : item.program_name
            }
        })
        .select2("#curriculum_id", '/curriculum/select2', function(item){
            return {
                id:item.id,
                text : item.curriculum_name
            }
        })

    globalCRUD.datatables({
        url: '/sequence/datatables',
        selector: '#table-curriculum-sequence',
        columnsField: ['id', 'sequence_name', 'developmentprog.program_name', 'curriculum.curriculum_name'],
        modalSelector: "#modal-curriculum-sequence",
        modalButtonSelector: ".add-curriculum-sequence",
        modalFormSelector: "#form-curriculum-sequence",
        actionLink: {
            store: function() {
                return "/sequence";
            },
            update: function(row) {
                return "/sequence/" + row.id;
            },
            delete: function(row) {
                return "/sequence/" + row.id;
            },
        }
    })
</script>
@endsection
