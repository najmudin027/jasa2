<form id="form-dev-plant">
    <div class="modal fade" id="modal-dev-plant" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Dev Plant</h4>
                </div>
                <div class="modal-body">

                    <label>User Name</label>
                    <div class="form-group">
                        <select class="user_id" name="user_id" id="user_id">
                            @foreach ($getUser as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                            
                          </select>                          
                    </div>

                    <label>Curriculum Sequence</label>
                    <div class="form-group">
                        <select class="js-example-basic-single" name="curr_sequence_id" id="curr_sequence_id">
                            
                          </select>                          
                    </div>

                    <input type="hidden" id="id" name="id">
                    <label>Status</label>
                    <div class="form-group">
                        <input name="status" placeholder="Status" type="text" class="form-control">
                    </div>

                    

                    <label>Date Time</label>
                    <div class="form-group">
                        <input type="date" name="date_time" class="form-control" width="100%" value="{{ date('Y-m-d') }}" />                     
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>

