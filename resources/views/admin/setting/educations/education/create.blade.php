@extends('admin.home')
@section('content')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<div class="col-md-12 card">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            {{ $title }}
        </div>
        <div class="btn-actions-pane-right text-capitalize">
            <a href="{{ url('/show-list-education') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
        </div>
    </div>
    <div class="card-body">
        <form action="" method="post" id="form-create-education">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label align-right">Institution</label>
                                <div class="col-sm-10">
                                    <input name="institution" id="institution" placeholder="Institution" type="text" class="form-control">
                                    <strong><span id="error-institution" style="color:red"></span></strong>
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label">Start Date</label>
                                <div class="col-sm-10">
                                <input type="date" class="form-control" name="education_start_date" width="100%"/>
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label">Type</label>
                                <div class="col-sm-10">
                                    <input name="type" id="type" placeholder="Type" type="text" class="form-control">
                                    <strong><span id="error-type" style="color:red"></span></strong>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label">Place</label>
                                <div class="col-sm-10">
                                    <input name="place" id="place" placeholder="Place" type="text" class="form-control">
                                    <strong><span id="error-place" style="color:red"></span></strong>
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label">End Date</label>
                                <div class="col-sm-10">
                                <input type="date" class="form-control" name="education_end_date" width="100%" />
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label">Major</label>
                                <div class="col-sm-10">
                                    <input name="major" id="major" placeholder="Major" type="text" class="form-control">
                                    <strong><span id="error-major" style="color:red"></span></strong>
                                </div>
                            </div>

                            <div class="position-relative row form-check">
                                <div class="col-sm-10 offset-sm-2">
                                    <button class="btn btn-success" id="save"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection



@section('script')
{{-- @include('admin.script.master._productModelScript') --}}
<script>
    $('#form-create-education').submit(function(e) {
        globalCRUD.handleSubmit($(this))
            .storeTo('/education')
            .backTo(function(resp) {
                return "/admin/educations/show";
            }),
        e.preventDefault();
    });

    

</script>
@endsection