@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add-curriculum">Add Curriculum</button>
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-curriculum" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Curriculum Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
            <button class="btn btn-primary btn-sm add-curriculum">Add Curriculum</button>
        </div>
    </div>
</div>
@include('admin.setting.educations.curriculum.modalCurriculum')
@endsection

@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    globalCRUD.datatables({
        url: '/curriculum/datatables',
        selector: '#table-curriculum',
        columnsField: ['id', 'curriculum_name'],
        modalSelector: "#modal-curriculum",
        modalButtonSelector: ".add-curriculum",
        modalFormSelector: "#form-curriculum",
        actionLink: {
            store: function() {
                return "/curriculum";
            },
            update: function(row) {
                return "/curriculum/" + row.id;
            },
            delete: function(row) {
                return "/curriculum/" + row.id;
            },
        }
    })
</script>
@endsection
