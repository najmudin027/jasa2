<!doctype html>
<html lang="en">
    @include('admin.template._header')
<body>
    <style>
        td.details-control {
            background-image: url("{{ url('adminAssets/image/error.png') }}") no-repeat center center;
            cursor: pointer;
            width: 20px;
        }
        tr.shown td.details-control {
            background-image: url("{{ url('adminAssets/image/error.png') }}") no-repeat center center;
        }
    </style>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        @include('admin.template._navbar')
        <div class="app-main">
            @include('admin.template._sidebar')
            <div class="app-main__outer">
                <div class="app-main__inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="main-card mb-3 card">
                                <div class="card-body">
                                    <div class="card-header-tab card-header">
                                        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                            {{ $title }}
                                        </div>
                                        <div class="btn-actions-pane-right text-capitalize">
                                            <a href="{{ route('create.menu') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add Menus</a>
                                        </div>
                                    </div><br>
                                    <!-- Button trigger modal -->

                                    <table id="example" class="display table table-hover table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Childs</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.template._mainScript')
    @include('admin.script.menu._menuSettingScript')
    <script>
        // Helper.ceklogin()
    </script>
</body>
</html>


