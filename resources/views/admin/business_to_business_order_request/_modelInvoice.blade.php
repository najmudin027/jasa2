<div class="modal fade" id="show_invoice_modal" data-backdrop="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Berita Acara</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body invoice_area">
                <table class="table table-hover table-bordered invoice_table" style="width:100%">
                    <thead>
                        <th>Image</th>
                        <th>Action</th>
                    </thead>
                    <tbody id="append_invoice_area"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('.invoice_table').DataTable();
    });

    var $modal_invoice = $('#show_invoice_modal').modal({
        show: false
    });

    var invoiceObj = {
        // isi field input
        isiDataFormModalInvoice: function(ids, type) {
            $.ajax({
                url: Helper.apiUrl('/request-order-b-2-b/show-invoice/'+type+'/'+id),
                type: 'get',
                success: function(resp) {
                    console.log(resp.data)
                    $("#append_invoice_area").html('');
                    var content_invoice_area = '';
                    _.each(resp.data, function(invoice) {
                        content_invoice_area += `<tr>
                                                    <td><a href="/storage/media-btb/${invoice.image_invoice}" target="__blank">${invoice.image_invoice}</a></td>
                                                    <td><button type="submit" class="btn btn-danger btn-sm" id="btn_delete_invoice" data-id="${invoice.id}"><i class="fa fa-trash"></i></button></td>
                                                </tr> `;
                    });
                    $("#append_invoice_area").append(content_invoice_area);
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            })

        },
        // hadle ketika response sukses
        successHandle: function(resp) {
            // send notif
            Helper.successNotif(resp.msg);
            $modal_invoice.modal('hide');
        },
    }


    $(document)
        .on('click', '#show-invoice', function() {
            id = $(this).attr('data-ids');
            type = $(this).attr('data-type');
            console.log(invoiceObj.isiDataFormModalInvoice(id, type));
            $modal_invoice.modal('show');
        })

    $(document)
        .on('click', '#btn_delete_invoice', function() {
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.post('/request-order-b-2-b/delete-invoice/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('Data Berhasil Dihapus');
                        // reload table
                        location.reload()
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })
</script>