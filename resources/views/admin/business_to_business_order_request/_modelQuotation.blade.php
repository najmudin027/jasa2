<div class="modal fade" id="show-modal-quotation" data-backdrop="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Quotation Attachment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover table-bordered quotation" style="width:100%">
                    <thead>
                        <th>Image</th>
                        <th>Action</th>
                    </thead>
                    <tbody id="content_area_quotation"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('.quotation').DataTable();
    });

    var $modal = $('#show-modal-quotation').modal({
        show: false
    });

    var quotObj = {
        // isi field input
        isiModalQuot: function(type, id) {
            $.ajax({
                url: Helper.apiUrl('/request-order-b-2-b/show-quotation/'+type+'/'+id),
                type: 'get',
                success: function(rsp) {
                    console.log(rsp.data)
                    $("#content_area_quotation").html('');
                    var content_quot = '';
                    _.each(rsp.data, function(det) {
                        content_quot += `<tr>
                                            <td><a href="/storage/media-btb/${det.image_quot}" target="__blank">${det.image_quot}</a></td>
                                            <td><button type="submit" class="btn btn-danger btn-sm" id="btn_delete_quot" data-id="${det.id}"><i class="fa fa-trash"></i></button></td>
                                        </tr>`;
                    });
                    $("#content_area_quotation").append(content_quot);
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            })

        },
        // hadle ketika rsponse sukses
        successHandle: function(rsp) {
            // send notif
            Helper.successNotif(rsp.msg);
            $modal.modal('hide');
        },
    }


    $(document)
        .on('click', '#show-quotation', function() {
            id = $(this).attr('data-ids');
            type = $(this).attr('data-type');
            console.log(quotObj.isiModalQuot(type, id));
            $modal.modal('show');
        })

    $(document)
        .on('click', '#btn_delete_quot', function() {
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.post('/request-order-b-2-b/delete-quotation/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('Data Berhasil Dihapus');
                        // reload table
                        location.reload()
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })
</script>