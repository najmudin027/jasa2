<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $showQuotation->quotation_code }} | Quotation</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
</head>
<body class="hold-transition sidebar-mini">

    <div class="invoice p-3 mb-3">
        <!-- title row -->
        <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{ asset('astech.png') }}" alt=""> 
                </div>
                <div class="col-md-6">
                    <h4><small class="float-right">Quotation Code  : <strong>{{ $showQuotation->quotation_code }}</strong></small></h4> <br>
                    <h5><small class="float-right">Quotation Date  : <strong>{{ date('M-d-Y', strtotime($showQuotation->created_at)) }}</strong></small></h5> <br>
                </div>
            </div><hr>
        </div>
        <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-3 invoice-col">
                <!--NAME OF COMPANY / PIC-->
                <address>
                    <div><strong>PT ASTECH</strong></div>
                    <div>Email: ASTech@email.com</div>
                    <div>Phone: (021) 54367231</div>
                    <div>3rd Floor WISMA SSK BUILDING, <br> Daan Mogot Rd No.Km. 11, RT.5/RW.4,<br> Kedaung Kali Angke, Cengkareng, <br> West Jakarta City, Jakarta 11710</div>
                </address>
            </div>
            <div class="col-sm-3 invoice-col text-left">
                <!--NAME OF COMPANY / PIC-->
                <address>
                <strong>{{$showQuotation->company}}</strong><br>
                Phone : {{$showQuotation->phone}}<br>
                {{$showQuotation->address}}<br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col text-left">
                {{-- <b>Invoice #007612</b><br> --}}
                {{-- <br> --}}
                <!--Term & Condition-->
                <address>
                    <b>Validity :</b>  {{$showQuotation->validity}}<br>
                    <b>Payment :</b>  {{$showQuotation->payment}}<br>
                    <b>Minimum Order :</b>  {{$showQuotation->min_order}}<br>
                    <b>Status Proce :</b>  {{$showQuotation->status_process}}<br>
                    <b>Time Of Delivery :</b>  {{$showQuotation->deliver}}<br>
                </address>
            </div>
            
            <!-- /.col -->
            <div class="col-sm-2 invoice-col text-left">
                <!--UNIT INFORMATION-->
                <address>
                Product : {{$showQuotation->product_name}}<br>
                Type Service : {{$showQuotation->type_produk}} <br>
                Remark : {{$showQuotation->remark}} <br>
                Revisi : <strong>{{$showQuotation->revisi_ke == 0 ? '-' : $showQuotation->revisi_ke }}</strong> <br>
                </address>
            </div>
            
        </div>
        <div class="row">
            <div class="col-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        {{-- <th>Qty</th> --}}
                        <th>Product</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($showQuotation->get_part as $item)
                            <tr>
                                {{-- <td>{{ $item->qty }}</td> --}}
                                <td>{{ $item->part_description }}</td>
                                <td>Rp. {{ number_format($item->selling_price) }}</td>
                                <td>{{ $item->qty }}</td>
                                <?php $subtotal = $item->qty * $item->selling_price ?>
                                <td>Rp. {{ number_format($subtotal) }}</td>
                            </tr>
                            @php
                                if(!empty($showQuotation->nama_jasa) && !empty($showQuotation->harga_jasa)){
                                    $harga_jasa = $showQuotation->harga_jasa;
                                }else{
                                    $harga_jasa = 0;
                                }

                                if(!empty($showQuotation->get_labor)){
                                    $service = $showQuotation->get_labor->service_price;
                                }else{
                                    $service = 0;
                                }
                                $arr = 0;
                                foreach ($showQuotation->get_part as $value) {
                                    $arr += $value->selling_price * $value->qty;
                                }
                                
                                $totals = $arr + $service + $harga_jasa;
                            @endphp
                        @endforeach
                        @if (!empty($showQuotation->get_labor))
                            <tr>
                                <td>{{!empty($showQuotation->get_labor) ? $showQuotation->get_labor->name : '-'}}</td>
                                <td>Rp.{{!empty($showQuotation->get_labor) ? number_format($showQuotation->get_labor->service_price) : '0'}}</td>
                                <td>-</td>
                                <td>Rp.{{!empty($showQuotation->get_labor) ? number_format($showQuotation->get_labor->service_price) : '0'}}</td>
                            </tr>
                        @endif
                        @if (!empty($showQuotation->nama_jasa) && !empty($showQuotation->harga_jasa))
                            <tr>
                                <td>{{!empty($showQuotation->nama_jasa) ? $showQuotation->nama_jasa : '-'}}</td>
                                <td>Rp.{{!empty($showQuotation->harga_jasa) ? number_format($showQuotation->harga_jasa) : '0'}}</td>
                                <td>-</td>
                                <td>Rp.{{!empty($showQuotation->harga_jasa) ? number_format($showQuotation->harga_jasa) : '0'}}</td>
                            </tr>
                        @endif
                    </tbody>
                    <tbody>
                        <tr>
                            <td colspan="3" class="text-right">Sub Total :</td>
                            <td>Rp. {{number_format($totals)}}</td>
                        </tr>
                        <tr>
                            <td colspan="3" class="text-right">TAX :</td>
                            @php
                            if($totals >= 5000000){
                                    $materai = 10000;
                                }else{
                                    $materai = 0;
                                }
                                $percentage = 10;
                                $totalHarga = $totals;

                                $new_tax = ($percentage / 100) * $totalHarga;
                                
                            @endphp
                            <td>Rp. 10% (Rp.{{number_format($new_tax)}})</td>
                        </tr>
                        @if ($totals >= 5000000)
                            <tr>
                                <td colspan="3" class="text-right">Materai :</td>
                                <td>Rp. 10.000</td>
                            </tr>
                        @endif
                        <tr>
                            @php
                                $tax = $new_tax;
                                $hargasemua = $totals;
                                $hasilAkhir = $tax + $hargasemua + $materai;
                            @endphp
                            <td colspan="3" class="text-right">Total :</td>
                            <td>Rp. {{ number_format($hasilAkhir) }} </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-6">
                
            </div>
            <div class="col-6">
                {{-- <p class="lead">Amount Due 2/22/2014</p>

                <div class="table-responsive">
                <table class="table">
                    <tr>
                    <th style="width:50%">Subtotal:</th>
                    <td>$250.30</td>
                    </tr>
                    <tr>
                    <th>Tax (9.3%)</th>
                    <td>$10.34</td>
                    </tr>
                    <tr>
                    <th>Shipping:</th>
                    <td>$5.80</td>
                    </tr>
                    <tr>
                    <th>Total:</th>
                    <td>$265.24</td>
                    </tr>
                </table>
                </div> --}}
            </div>
        </div>

        <!-- this row will not appear when printing -->
        
    </div>

    <div class="row no-print">
        <div class="col-12">
            <button class="btn btn-primary btn-xl .no-print"  onclick="window.print();return false;"><i class="fas fa-print"></i>&nbsp; Print Quotation</button>
        </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('dist/js/demo.js') }}"></script>
</body>
</html>
