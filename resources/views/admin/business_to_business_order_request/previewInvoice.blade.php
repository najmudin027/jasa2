<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ str_replace('QUOT', 'INV', $showInv->quotation_code)}} | Invoice</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
</head>
<body class="hold-transition sidebar-mini">
    <div id="invoice">
        <table style="width: 100%">
            <tr>
                <td></td>
                <td class="text-right"><img src="{{ asset('astech.png') }}" alt="" width="10%"> </td>
            </tr>
            <tr>
                <td></td>
                <td class="text-center"><h4><strong>Invoice</strong></h4></td>
                <td></td>
            </tr>
        </table><br>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <table  style="width: 80%">
                            <tr>
                                <td><strong>From</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Name </strong></td>
                                {{-- <td>:</td> --}}
                                <td><Strong>PT ASTECH</Strong></td>
                            </tr>
                            <tr>
                                <td><strong>Email </strong></td>
                                {{-- <td>:</td> --}}
                                <td><Strong>ASTech@email.com</Strong></td>
                            </tr>
                            <tr>
                                <td><strong>Address </strong></td>
                                {{-- <td>:</td> --}}
                                <td>3rd Floor WISMA SSK BUILDING, <br> Daan Mogot Rd No.Km. 11, RT.5/RW.4,<br> Kedaung Kali Angke, Cengkareng, <br> West Jakarta City, Jakarta 11710</td>
                            </tr>
                            <tr>
                                <td><strong>City </strong></td>
                                {{-- <td>:</td> --}}
                                <td>DKI Jakarta</td>
                            </tr>
                            <tr>
                                <td><strong>Phone </strong></td>
                                {{-- <td>:</td> --}}
                                <td>(021) 54367231</td>
                            </tr>
                        </table><br>
                    </div>
                    <div class="col-md-5" class="text-right">
                        <table  class="text-right" style="width:100%">
                            <tr>
                                <td>No Invoice</td>
                                <td>{{ str_replace('QUOT', 'INV', $showInv->quotation_code)}}</td>
                            </tr>
                            <tr>
                                <td>Invoice Date</td>
                                <td>{{ date('M-D-Y H:i', strtotime($showInv->created_at)) }}</td>
                            </tr>
                            <tr>
                                <td>Term Of Payment</td>
                                <td>{{ $showInv->validity }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <table  style="width: 80%">
                            <tr>
                                <td><strong>To </strong></td>
                            </tr>
                            <tr>
                                <td><strong>Name </strong></td>
                                {{-- <td>:</td> --}}
                                <td><Strong>{{$showInv->company}}</Strong></td>
                            </tr>
                            <tr>
                                <td><strong>Phone </strong></td>
                                {{-- <td>:</td> --}}
                                <td>{{$showInv->phone}}</td>
                            </tr>
                            <tr>
                                <td><strong>Address </strong></td>
                                {{-- <td>:</td> --}}
                                <td>{{$showInv->address}}</td>
                            </tr>
                            
                        </table><br>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
        </div>
        <br>
        <table border="1" class="table" style="width:100%">
            <tr>
                <td>Product</td>
                <td>Price</td>
                <td>QTY</td>
                <td>Total</td>
            </tr>
            <tbody>
                @foreach ($showInv->get_part as $item)
    
                    <tr>
                        {{-- <td class="no">{{ $item->qty }}</td> --}}
                        <td >{{ $item->part_description }}</td>
                        <td >Rp. {{ number_format($item->selling_price) }}</td>
                        <td >{{ $item->qty }}</td>
                        @php
                            $amount =  $item->qty * $item->selling_price;
                        @endphp
                        <td >Rp. {{ number_format($amount) }}</td>
                    </tr>
                    @php
                        $arr = 0;
                        foreach ($showInv->get_part as $value) {
                            $arr += $value->qty * $value->selling_price;
                        }
                    @endphp
                @endforeach
                @if (!empty($showInv->get_labor))
                    <tr>
                        <td>{{!empty($showInv->get_labor) ? $showInv->get_labor->name : '-'}}</td>
                        <td>Rp.{{!empty($showInv->get_labor) ? number_format($showInv->get_labor->service_price) : '0'}}</td>
                        <td>-</td>
                        <td>Rp.{{!empty($showInv->get_labor) ? number_format($showInv->get_labor->service_price) : '0'}}</td>
                    </tr>
                @endif
                @if (!empty($showInv->nama_jasa) && !empty($showInv->harga_jasa))
                    <tr>
                        <td>{{!empty($showInv->nama_jasa) ? $showInv->nama_jasa : '-'}}</td>
                        <td>Rp.{{!empty($showInv->harga_jasa) ? number_format($showInv->harga_jasa) : '0'}}</td>
                        <td>-</td>
                        <td>Rp.{{!empty($showInv->harga_jasa) ? number_format($showInv->harga_jasa) : '0'}}</td>
                    </tr>
                @endif
            </tbody>
            <tbody>
                @php
                    if(!empty($showInv->nama_jasa) && !empty($showInv->harga_jasa)){
                        $harga_jasa = $showInv->harga_jasa;
                    }else{
                        $harga_jasa = 0;
                    }

                    if(!empty($showInv->get_labor)){
                        $harga_labor = $showInv->get_labor->service_price;
                    }else{
                        $harga_labor = 0;
                    }

                    if($arr >= 5000000){
                        $materai = 10000;
                    }else{
                        $materai = 0;
                    }
                    $percentage = 10;
                    $totalHarga = $arr + $harga_labor + $harga_jasa;

                    $new_tax = ($percentage / 100) * $totalHarga;
                    
                @endphp
                <tr>
                    <td colspan="3" class="text-right">Sub Total :</td>
                    <td>Rp. {{number_format($totalHarga)}}</td>
                </tr>
                <tr>
                    <td colspan="3" class="text-right">TAX :</td>
                    <td>Rp. 10% (Rp.{{number_format($new_tax)}})</td>
                </tr>
                @if ($arr >= 5000000)
                    <tr>
                        <td colspan="3" class="text-right">Materai :</td>
                        <td>Rp. 10.000</td>
                    </tr>
                @endif
                <tr>
                    @php
                        $tax = $new_tax;
                        $hargasemua = $arr;
                        $hasilAkhir = $tax + $hargasemua + $materai + $harga_jasa;
                    @endphp
                    <td colspan="3" class="text-right">Grand Total :</td>
                    <td>Rp. {{ number_format($hasilAkhir) }} </td>
                </tr>
            </tbody>
        </table>
        <table>
            <tr>
                <td>Beneficary</td>
                <td>:</td>
                <td>{{$beneficary->value}}</td>
            </tr>
            <tr>
                <td>Account No</td>
                <td>:</td>
                <td>{{$account_no->value}}</td>
            </tr>
            <tr>
                <td>Bank Name</td>
                <td>:</td>
                <td>{{$bank_name->value}}</td>
            </tr>
            <tr>
                <td>Bank Address</td>
                <td>:</td>
                <td>{{$bank_address->value}}</td>
            </tr>
        </table><br>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-7">
                    <!--<table>-->
                    <!--    <tr>-->
                    <!--        <td style="text-decoration: underline"><center>Customer</center></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td>(........................................................)</td>-->
                    <!--    </tr>-->
                    </table>
                </div>
                <div class="col-md-3">
                    <table class="text-right">
                        <tr>
                            <td style="text-decoration: underline"><center>Finance / Accounting</center></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td>(........................................................)</td>
                        </tr>
                        <tr>
                            <td><center>Signed</center></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row no-print">
        <div class="col-12">
            <button class="btn btn-primary btn-xl .no-print"  onclick="window.print();return false;"><i class="fas fa-print"></i>&nbsp; Print Quotation</button>
        </div>
    </div>
    <style>
        #invoice{
        padding: 30px;
    }
    
    .invoice {
        position: relative;
        background-color: #FFF;
        min-height: 680px;
        padding: 15px
    }
    
    .invoice header {
        padding: 10px 0;
        margin-bottom: 20px;
        border-bottom: 1px solid #3989c6
    }
    
    .invoice .company-details {
        text-align: right
    }
    
    .invoice .company-details .name {
        margin-top: 0;
        margin-bottom: 0
    }
    
    .invoice .contacts {
        margin-bottom: 20px
    }
    
    .invoice .invoice-to {
        text-align: left
    }
    
    .invoice .invoice-to .to {
        margin-top: 0;
        margin-bottom: 0
    }
    
    .invoice .invoice-details {
        text-align: right
    }
    
    .invoice .invoice-details .invoice-id {
        margin-top: 0;
        color: #3989c6
    }
    
    .invoice main {
        padding-bottom: 50px
    }
    
    .invoice main .thanks {
        margin-top: -100px;
        font-size: 2em;
        margin-bottom: 50px
    }
    
    .invoice main .notices {
        padding-left: 6px;
        border-left: 6px solid #3989c6
    }
    
    .invoice main .notices .notice {
        font-size: 1.2em
    }
    
    .invoice table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px
    }
    
    .invoice table td,.invoice table th {
        padding: 15px;
        background: #eee;
        border-bottom: 1px solid #fff
    }
    
    .invoice table th {
        white-space: nowrap;
        font-weight: 400;
        font-size: 16px
    }
    
    .invoice table td h3 {
        margin: 0;
        font-weight: 400;
        color: #3989c6;
        font-size: 1.2em
    }
    
    .invoice table .qty,.invoice table .total,.invoice table .unit {
        text-align: right;
        font-size: 1.2em
    }
    
    .invoice table .no {
        color: #fff;
        font-size: 1.6em;
        background: #3989c6
    }
    
    .invoice table .unit {
        background: #ddd
    }
    
    .invoice table .total {
        background: #3989c6;
        color: #fff
    }
    
    .invoice table tbody tr:last-child td {
        border: none
    }
    
    .invoice table tfoot td {
        background: 0 0;
        border-bottom: none;
        white-space: nowrap;
        text-align: right;
        padding: 10px 20px;
        font-size: 1.2em;
        border-top: 1px solid #aaa
    }
    
    .invoice table tfoot tr:first-child td {
        border-top: none
    }
    
    .invoice table tfoot tr:last-child td {
        color: #3989c6;
        font-size: 1.4em;
        border-top: 1px solid #3989c6
    }
    
    .invoice table tfoot tr td:first-child {
        border: none
    }
    
    .invoice footer {
        width: 100%;
        text-align: center;
        color: #777;
        border-top: 1px solid #aaa;
        padding: 8px 0
    }
    
    @media print {
        .invoice {
            font-size: 11px!important;
            overflow: hidden!important
        }
    
        .invoice footer {
            position: absolute;
            bottom: 10px;
            page-break-after: always
        }
    
        .invoice>div:last-child {
            page-break-before: always
        }
    }
    </style>
</body>
</html>