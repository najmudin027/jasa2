@extends('admin.home')
@section('content')
    <form id="outlet-form" style="display: contents;">
        <div class="col-md-12" style="margin-top: 10px;">
            <div class="card">
                <div class="header-bg card-header-tab card-header">
                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    {{ $title }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <
        <div class="col-md-12" style="margin-top: 10px;">
            <div class="card">
                <div class="card-body">
                    
                    <div class="form-group">
                        <label>User</label>
                        <select class="form-control user-select" id="user_id" name="user_id" style="width:100%" required>
                            <option value="">Select User</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 10px;" id="outlet-content"> </div>
        <div class="col-md-12" style="margin-top: 10px;">
            <div class="card">
                <div class="card-body">
                    {{-- <button type="button" class="btn btn-sm btn-danger waves-effect" id="btn-delete-outlet"><i
                            class="fa fa-trash"></i> Delete Outlet</button> --}}
                    <button type="button" class="btn btn-sm btn-primary waves-effect" id="btn-add-outlet"><i
                            class="fa fa-plus"></i> Add Outlet</button>
                    <button type="submit" class="btn btn-sm btn-success waves-effect"><i
                        class="fa fa-plus"></i> Save Outlet</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('script')
    <script>
        $('#btn-add-outlet').hide();
        $('#btn-add-outlet').hide();
        var DATA_UNIQ = 1;

        show();
        function show(){
            $('#outlet-content').append(outletBoxContentTemplate(show_delete_btn = false));
            loadUserSelect2();
            // loadOutletSelect2();
            DATA_UNIQ++;

        }

        // $('#btn-add-outlet').click(function() {
        //     show()
        // })

        $('#btn-add-outlet').click(function() {
            user_id = $('.user-select').find(':selected').val();
            $('#outlet-content').append(outletBoxContentTemplate());
            loadOutletSelect2(user_id)
            DATA_UNIQ++;
        })

        $(document).on('click', '.btn-delete-outlet', function() {
            console.log('klik')
            uniq = $(this).attr('data-uniq');
            $('.container-detail-card-' + uniq).remove();
        })

        $(document).on('change', '.user-select', function() {
            $('.outlet-select').html('<option value="">Select Outlet</option>');
            $('.unit-select').html('<option value="">Select Unit</option>');
            uniq = $('#get_id_uniq').val();
            user_id = $(this).find(':selected').val();
            console.log('uniqqq', uniq)
            console.log('user_id', user_id)
            loadOutletSelect2(user_id)
        })

        $(document).on('change', '.outlet-select', function() {
            $('.unit-select').html('<option value="">Select Unit</option>');
            uniq = $(this).attr('data-uniq');
            outlet_id = $(this).find(':selected').val();
            console.log('uniq', uniq)
            console.log('outlet_id', outlet_id)
            loadUnitSelect2(uniq, outlet_id);
            $('#btn-add-outlet').show();
        })

        $('#outlet-form').submit(function(e){
            Helper.loadingStart();
            var data = new FormData($('#media-form')[0]);
            var bodyFormData = new FormData();

            $('.user-select').each(function(){ 
                bodyFormData.append('user_id', $(this).val()); 
            });
             
            $('.outlet-select').each(function(){ 
                bodyFormData.append('ms_outlet_id[]', $(this).val() ); 
            });

            $('.unit-select').each(function(){ 
                bodyFormData.append('ms_transaction_b2b_detail[]', $(this).val() ); 
            });

            $('.outlet-remark').each(function(){ 
                bodyFormData.append('remark[]', $(this).val() ); 
            });

            $('.outlet-service').each(function(){ 
                bodyFormData.append('service_type[]', $(this).val() ); 
            });

            // $('.outlet-image').each(function(){ 
            //     var file = $(this).prop('files');
            //     fileName = null;
            //     if(file.length){
            //         fileName = file[0];
            //     }
            //     console.log(fileName)
            //     bodyFormData.append('image[]', fileName); 
            // });
            // no = 0;
            // $('.outlet-vidio').each(function(){ 
            //     var file = $(this).prop('files');
            //     fileName = null;
            //     if(file.length){
            //         fileName = file[0];
            //     }
            //     console.log(fileName)
            //     bodyFormData.append('vidio['+no+']', fileName); 
            //     no++;
            // });

            

            Axios.post('/user-b-2-b/request-order-multiple', bodyFormData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    window.location.href = Helper.redirectUrl('/admin/business-to-business-user-request-order/show');
                    Helper.successNotif('Success Updated');
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });

            e.preventDefault();
        })

        function outletBoxContentTemplate(show_delete_btn = true) {
            btn_delete_style = 'display:none';
            if(show_delete_btn){
                btn_delete_style = '';
            }
            
            var template = `
            <div class="card container-detail-card-${DATA_UNIQ}" style="margin-bottom: 20px;">
                <input type="hidden" id="get_id_uniq" value="${DATA_UNIQ}">
                <div class="card-body">
                    <div class="form-group">
                        <label>Outlet-${DATA_UNIQ}</label>
                        <select class="form-control outlet-select outlet-select-${DATA_UNIQ}" id="outlet_id" name="outlet_id" style="width:100%" data-uniq="${DATA_UNIQ}" required>
                            
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Unit-${DATA_UNIQ}</label>
                        <select class="form-control unit-select unit-select-${DATA_UNIQ}" id="unit_id" name="unit_id" style="width:100%" required>
                           
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Remark</label>
                        <textarea cols="30" rows="4" class="form-control outlet-remark"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Service</label>
                        <select class="form-control outlet-service" >
                            <option value="Repair">Repair</option>
                            <option value="Maintance">Maintance</option>
                            <option value="Checking">Checking</option>
                        </select>
                    </div>
                    <button style="${btn_delete_style}" type="button" class="btn btn-sm btn-danger waves-effect btn-delete-outlet" data-uniq="${DATA_UNIQ}"><i
                            class="fa fa-trash"></i> Delete Outlet</button>
                </div>
            </div>
        `;
            return template;
        }

        // <div class="form-group">
        //                 <label>Image</label>
        //                 <input type="file" class="form-control outlet-image" />
        //             </div>
        //             <div class="form-group">
        //                 <label>Vidio</label>
        //                 <input type="file" class="form-control outlet-vidio" />
        //             </div>

        function loadOutletSelect2(user_id) {
            globalCRUD
                .select2Static(".outlet-select", '/user-b-2-b/dropdown-get-outlet-admin/'+user_id, function(item) {
                    return {
                        id: item.id,
                        text: item.name,
                        data: item,
                    }
                })
        }

        function loadUnitSelect2(uniq, outlet_id) {
            globalCRUD
                .select2Static(".unit-select-" + uniq, '/user-b-2-b/dropdown-get-unit/' + outlet_id, function(item) {
                    type_name = '';
                    type_desc = '';
                    if(item.get_type){
                        type_name = item.get_type.name;
                        type_desc = item.get_type.desc == null ? '' : item.get_type.desc;
                    }
                    return {
                        id: item.id,
                        text: item.unit_ke + ' - (' + item.merek  + ' ' + type_name + ' ' + type_desc + ')',
                        data: item,
                    }
                })
        }

        function loadUserSelect2() {
            globalCRUD
                .select2Static(".user-select", '/user-b-2-b/dropdown-get-user-b2b', function(item) {
                    return {
                        id: item.id,
                        text: item.name,
                        data: item,
                    }
                })
        }
    </script>
@endsection
