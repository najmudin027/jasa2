
<div class="modal fade" id="show_image_order_modal" data-backdrop="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    
                </button>
            </div>
            <div class="modal-body order_image">
                <table class="table table-hover table-bordered order_image" style="width:100%">
                    <thead>
                        <th>Image</th>
                        <th>Action</th>
                    </thead>
                    <tbody id="append_order_image"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>

<script>
    // $(document).ready( function () {
        var dt = $('.order_image').DataTable();
    // });

    var $modal_order_image = $('#show_image_order_modal').modal({
        show: false
    });

    var orderImageObj = {
        // isi field input
        isiDataFormModalOrderImage: function(id) {
            $.ajax({
                url: Helper.apiUrl('/request-order-b-2-b/show-image-order/'+id),
                type: 'get',
                success: function(resp) {
                    console.log(resp.data)
                    $("#append_order_image").html('');
                    var content_order_image = '';
                    _.each(resp.data, function(orderImage) {
                        content_order_image += ` <tr>
                                                <td><a href="/storage/media-request-order/${orderImage.image}" target="__blank">${orderImage.image}</a></td>
                                                <td><button class="btn btn-danger btn-sm" id="image_order_delete" data-id="${orderImage.id}"><i class="fa fa-trash"></i></button></td>
                                            </tr> `;
                    });
                    $("#append_order_image").append(content_order_image);
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            })

        },
        // hadle ketika response sukses
        successHandle: function(resp) {
            // send notif
            Helper.successNotif(resp.msg);
            $modal_order_image.modal('hide');
        },
    }


    $(document)
        .on('click', '#show_image_order', function() {
            id = $(this).attr('data-ids');
            console.log(orderImageObj.isiDataFormModalOrderImage(id));
            $modal_order_image.modal('show');
        })

    // ganti status
    $(document)
        .on('click', '#image_order_delete', function() {
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.post('/request-order-b-2-b/delete-berita-acara/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('Data Berhasil Dihapus');
                        // reload table
                        location.reload()
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })
</script>