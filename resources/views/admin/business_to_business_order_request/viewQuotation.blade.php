@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-top: 10px;">
    <form id="sparepart-detail-form">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            Update Quotation
                        </div>
                        <div class="col-md-6 text-right">
                            Revisi : <strong>{{$data->revisi_ke == 000 ? '-' : $data->revisi_ke }}</strong>
                            {{-- <a class="btn btn-primary" href="{{ url('/admin/business-to-business-user-request-order/create') }}">Request Order</a> --}}
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id_b2b_details" id="id_b2b_details" value="{{ !empty($data->get_trans) ? $data->get_trans->ms_b2b_detail : '' }}">
            <div class="card-body">
                <input type="hidden" id="id_trans" name="id_trans" value="{{ $data->id_transaction }}">
                <input type="hidden" id="sparepart_detail_order_id" value="{{ $data->id }}" name="sparepart_detail_order_id">
                <input type="hidden" name="type_part" id="type_part" value="my-inventory">
                <input type="hidden" name="id_transaction" id="id_transaction" value="{{ $data->id }}">
                <div class="col-md-12" style="color: black">
                    <center><strong style="color:black">Data Client</strong></center><br>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">Company : </label>
                            @if (!empty($data->company))
                                <input type="text" class="form-control" readonly name="company" id="company" value="{{ !empty($data->company) ?  $data->company : '-' }}" style="width: 100%">
                            @else
                                <input type="text" class="form-control" readonly name="company" id="company" value="-" style="width: 100%">
                            @endif    
                            
                        </div>
                        <div class="col-md-4">
                            <label for="">Phone : </label>
                            @if (!empty($data->phone))
                                    <input type="text" class="form-control" readonly name="phone" id="phone" value="{{ !empty($data->phone) ? $data->phone : '-' }}">
                            @else
                                    <input type="text" class="form-control" readonly name="phone" id="phone" value="-">
                            @endif    
                        </div>
                        <div class="col-md-4">
                            <label for="">Address : </label>
                            @if (!empty($data->address))
                                    <input type="text" class="form-control" readonly name="address" id="address" value="{{ !empty($data->address) ? $data->address : '-' }}" style="width: 100%">
                            @else
                                <input type="text" class="form-control" readonly name="address" id="address" value="-" style="width: 100%">
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="color: black" hidden>
                    <center><strong style="color:black">Term And Condition</strong></center><br>
                    <div class="row">
                        <div class="col-md-4 text-left" style="margin-bottom: 20px">
                            <label for="">Validity : </label>
                            <input type="text" class="form-control" name="validity" id="validity" value="{{$validity->value}}" readonly>
                        </div>
                        <div class="col-md-4 text-left" style="margin-bottom: 20px">
                            <label for="">Minimum Order : </label>
                            <input type="text" class="form-control" name="min_order" id="min_order" value="{{$minimum_order->value}}" readonly>
                        </div>
                        <div class="col-md-4 text-left" style="margin-bottom: 20px">
                            <label for="">Status Process : </label>
                            <input type="text" class="form-control" name="status_process" id="status_process" value="{{$status_proce->value}}" readonly>
                        </div>
                        <div class="col-md-6 text-left">
                            <label for="">Payment : </label><br>
                            <input type="text" class="form-control" name="payment" id="payment" value="{{$payment->value}}" readonly style="width: 100%">
                        </div>
                        <div class="col-md-6 text-left">
                            <label for="">Time Delivery : </label><br>
                            <input type="text" class="form-control" name="deliver" id="deliver" value="{{$time_of_delivery->value}}" readonly style="width: 100%">
                        </div>
                    </div>
                </div>
                <hr>
                <div class="col-md-12" style="color: black">
                    <center><strong style="color:black">Unit Information</strong></center><br>
                    <div class="row">
                        <div class="col-md-4 text-left" style="margin-bottom: 20ox">
                            <label for="">Product</label>
                            <input type="text" name="product_name" id="product_name" value="{{ !empty($data->product_name) ? $data->product_name : '-' }}" placeholder="merek" class="btb-outlet-item-merek form-control" readonly>
                            {{-- <select name="product_name" id="product_name" placeholder="merek" class="btb-outlet-item-merek form-control" required>
                                @foreach ($brand as $merk)
                                    <option value="{{$merk->name}}" {{ $data->product_name == $merk->name ? 'selected' : null}}>{{$merk->name}}</option>
                                @endforeach
                            </select> --}}
                        </div>
                        <div class="col-md-4 text-left" style="margin-bottom: 20ox">
                            <label for="">Unit</label>
                            <input type="text" name="unit_ke" id="unit_ke" class="form-control" value="{{ !empty($data->unit_ke) ? $data->unit_ke : '-' }}" readonly>
                            {{-- <select name="type_produk" id="type_produk" placeholder="merek" class="btb-outlet-item-type form-control" required>
                                @foreach ($type as $tipe)
                                    <option value="{{$tipe->id}}" {{ $data->type_produk == $tipe->id ? 'selected' : null}}>{{$tipe->name}}  {{$tipe->desc}}</option>
                                @endforeach
                            </select> --}}
                        </div>
                        <div class="col-md-4 text-left" style="margin-bottom: 20ox">
                            <label for="">Type</label>
                            <input type="text" name="type_produk" id="type_produk" placeholder="Type" class="btb-outlet-item-type form-control" value="{{ !empty($data->type_produk) ? $data->type_produk : '-' }}" readonly>
                            {{-- <select name="type_produk" id="type_produk" placeholder="merek" class="btb-outlet-item-type form-control" required>
                                @foreach ($type as $tipe)
                                    <option value="{{$tipe->id}}" {{ $data->type_produk == $tipe->id ? 'selected' : null}}>{{$tipe->name}}  {{$tipe->desc}}</option>
                                @endforeach
                            </select> --}}
                        </div>
                        <div class="col-md-12" style="margin-top: 20px">
                            <label for="">Remark</label>
                            <textarea name="remark" id="remark" class="form-control" id="" cols="30" rows="3">{{ !empty($data->remark) ? $data->remark : '' }}</textarea>
                        </div>

                        
                    </div>
                </div><br>
                {{-- <div class="dynamic_sparepart">
                    <center><strong style="color: black">Input Product</strong></center><br>
                    <div class="position-relative row form-group">
                        <div class="col-sm-6">
                            Name
                        </div>
                        <div class="col-sm-2">
                            Price
                        </div>
                        <div class="col-sm-2">
                            Unit
                        </div>
                    </div>
                    @foreach($getSparepart->get_part as $no => $part)
                        <?php $uniq = time(); ?>
                        <div class="position-relative row form-group dynamic_sparepart_content" id="my_part_row_{{ $uniq }}">
                        <input type="hidden" value="{{ $part->id }}" name="sparepart_detail_id[]" />
                        <input type="hidden" value="0" name="tmp_sparepart_id[]" />
                        <div class="col-sm-5">
                            <input type="text" placeholder="name"  name="teknisi_sparepart_name[]" data-uniq="{{ $uniq }}" class="teknisi_sparepart_name_id form-control teknisi_sparepart_name_input-{{ $uniq }}" value="{{ $part->produk_name }}" disabled/>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" placeholder="price"  name="teknisi_sparepart_price[]" data-uniq="{{ $uniq }}" class="teknisi_sparepart_price_id form-control teknisi_sparepart_price_i teknisi_sparepart_price_input-{{ $uniq }}" value="{{ $part->price }}" disabled/>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" placeholder="unit"  name="teknisi_sparepart_qty[]" data-uniq="{{ $uniq }}" class="teknisi_sparepart_qty_id form-control teknisi_sparepart_qty_i teknisi_sparepart_qty_input-{{ $uniq }}"value="{{ $part->qty }}" disabled/>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" placeholder="Satuan"  name="teknisi_sparepart_satuan[]" data-uniq="{{ $uniq }}" class="teknisi_sparepart_satuan_id form-control teknisi_sparepart_satuan_i teknisi_sparepart_satuan_input-{{ $uniq }}" value="{{ $part->satuan }}" disabled/>
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" data-sparepart_detail_id="{{ $data->id }}" data-uniq="{{ $uniq }}" class="btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                        </div>
                        </div>
                    @endforeach
                </div><hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-7 text-right"><button type="button" class="btn btn-primary btn-sm teknisi_sparepart_add_btn"><i class="fa fa-plus"></i>&nbsp; Add Spareparts</button></div>
                                    <div class="col-md-5 text-right"><button type="submit" class="btn btn-primary btn-sm btn-block"><i class="fa fa-save"></i>&nbsp; Save changes</button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
            
        </div><br>
        <div class="card">
            <div class="card-header header-border">
                SPARE PART
            </div>
            <div class="card-body">
                {{-- <div class="col-md-12">
                    <label for="exampleEmail">Astech Service Center</label>
                    <select name="asc_id" id="asc_id" class="form-control" required style="width:100%">
                        <option value="" selected></option>
                    </select>
                </div><br> --}}
                <table class="table">
                    <thead>
                        <tr>
                            <th>Code Material</th>
                            <th>Part Description</th>
                            <th>Qty</th>
                            <th>Stok</th>
                            <th>Price</th>
                            <th></th>
                            <th>hpp</th>
                            <th>Subtotal</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="dynamic_details">
                        @foreach ($getSparepart->get_part as $detail)  
                            <?php
                                $uniq = 'xxx'.$detail->id;
                            ?>
                            <tr>
                                    <input type="hidden" class="bundle_details_id_input" value={{ $detail->id }} id="bundle_details_id[]" />
                                    <td width="20%">
                                        <select name="part_data_stock_id[]" data-uniq="{{ $uniq }}" class="form-control part_data_stock_ids part_data_stock_id-{{ $uniq }}" style="width:100%" required>
                                            <option value="{{ $detail->part_data_stock_id }}">{{ !empty($detail->get_stok) ? $detail->get_stok->code_material : 'Part Deleted' }}</option>
                                        </select>
                                    </td>
                                    <td width="30%">
                                        <input name="part_description[]" type="text"
                                            placeholder="Description Sparepart"
                                            value="{{ !empty($detail->get_stok) ? $detail->part_description : '' }}"
                                            class="form-control part_descriptions part_description-{{ $uniq }}" data-uniq="{{ $uniq }}"
                                            style="width:100%" readonly>
                                    </td>
                                    <td>
                                        <input name="qty[]" type="text" placeholder="Quantity"
                                            class="form-control qtys qty-{{ $uniq }} number_only"  value="{{ !empty($detail->get_stok) ? $detail->qty : 0 }}" data-uniq="{{ $uniq }}" required>
                                    </td>
                                    <td>
                                        <input name="stok[]" type="text" placeholder=""
                                            class="form-control stoks stok-{{ $uniq }} number_only" data-uniq="{{ $uniq }}" value="{{ !empty($detail->get_stok) ? $detail->stok : 0 }}" required readonly>
                                    </td>
                                    <td>
                                        <input name="selling_price[]" type="text" placeholder="Price"
                                            class="form-control selling_prices selling_price-{{ $uniq }} number_only" value="{{!empty($detail->get_stok) ? $detail->selling_price : 0 }}" data-uniq="{{ $uniq }}" required>
                                    </td>
                                    <td>
                                        <input name="satuan[]" type="text" placeholder="Satuan"
                                            class="form-control satuans satuan-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $detail->satuan }}" hidden>
                                    </td>
                                    <td>
                                        <input name="hpp_average[]" type="text" placeholder="Hpp Price"
                                            class="form-control hpps hpp_average-{{ $uniq }} number_only" value="{{ !empty($detail->get_stok) ? $detail->hpp : 0 }}" data-uniq="{{ $uniq }}" required readonly>
                                    </td>
                                    <td>
                                        <input name="sub_total_details[]" type="text"
                                            class="form-control sub_total_detail sub_total_details-{{ $uniq }} number_only" value="{{ $detail->sub_total_details }}" data-uniq="{{ $uniq }}"
                                            disabled>
                                    </td>
                                    <td>
                                        <div class="btn-actions-pane-left text-right">
                                            <button type="button" class="btn btn-success btn_add_details"><i class="fa fa-plus"></i></button>
                                            @if(count($getSparepart->get_part) > 1)
                                            <button type="button" bundle_details_data_id="{{ $detail->id }}" data-uniq="{{ $uniq }}" class="btn-transition btn btn-danger btn_remove_details" ><i class="fa fa-times"></i></button>
                                            @endif
                                            
                                        </div>
                                    </td>
                                </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div><br>

        <div class="card">
            <div class="card-header header-border">
                LABOR
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Service Name</th>
                            <th>Service Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            {{-- <input type="hidden" value=0 id="count_details_data" /> --}}
                            <td width="20%">
                                <select name="service_name" data-uniq="xxx" class="form-control service_names service_name" style="width:100%">
                                    <option value="{{ !empty($data->get_labor) ? $data->get_labor->name : null }}">{{ !empty($data->get_labor) ? $data->get_labor->name : null }}</option>
                                </select>
                            </td>
                            <td width="30%">
                                <input name="service_price" type="text"
                                    placeholder="Service Price"
                                    class="form-control service_prices service_price" value="{{ !empty($data->get_labor) ? $data->get_labor->service_price : null }}" data-uniq="xxx"
                                    style="width:100%" readonly>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div><br>

        <div class="card">
            <div class="card-header header-border">
                Jasa
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama Jasa</th>
                            <th>Harga Jasa</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            {{-- <input type="hidden" value=0 id="count_details_data" /> --}}
                            <td width="20%">
                                <input type="text" name="nama_jasa" class="form-control nama_jasa" value="{{ !empty($data->nama_jasa) ? $data->nama_jasa : '' }}" placeholder="Nama Jasa">
                            </td>
                            <td width="30%">
                                <input type="text" name="harga_jasa" class="form-control harga_jasa" value="{{ !empty($data->harga_jasa) ? $data->harga_jasa : '' }}" placeholder="harga Jasa">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div><br>

        <div class="card">
            <button type="submit" class="btn btn-xl btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
        </div>
    </form>
</div>
<style>
    .fixed-sidebar .app-main .app-main__outer {
        width: 100%;
    }

    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
@endsection

@section('script')
@include('admin.business_to_business_order_request._jsCreateQuotation')
@include('admin.business_to_business_order_request._jsDetailCreateQuotation')

<script>
    globalCRUD.select2('.service_name', '/jasa-excel/select2', function(item) {
        return {
            id: item.name,
            text:item.name,
            price:item.price,
        }
    });

    $('.service_name').on('select2:select', function (e) {
        console.log(e.params)
        $('.service_price').val(e.params.data.price);
    });
</script>
<script>

    $('#sparepart-detail-form').submit(function(e){
        var data = new FormData($('#media-form')[0]);
        var bodyFormData = new FormData();

        $('.id_b2b_details').each(function(){ 
            bodyFormData.append('id_b2b_details', $(this).val() ); 
        });

        $('.nama_jasa').each(function(){ 
            bodyFormData.append('nama_jasa', $(this).val() ); 
        });

        $('.harga_jasa').each(function(){ 
            bodyFormData.append('harga_jasa', $(this).val() ); 
        });

        $('#unit_ke').each(function(){ 
            bodyFormData.append('unit_ke', $(this).val() ); 
        });
        
        $('.service_names').each(function(){ 
            bodyFormData.append('service_name', $(this).val() ); 
        });

        $('.service_prices').each(function(){ 
            bodyFormData.append('service_price', $(this).val() ); 
        });

        $('.part_data_stock_ids').each(function(){ 
            bodyFormData.append('part_data_stock_id[]', $(this).val() ); 
        });

        $('.part_descriptions').each(function(){ 
            bodyFormData.append('part_description[]', $(this).val() ); 
        });

        $('.qtys').each(function(){ 
            bodyFormData.append('qty[]', $(this).val() ); 
        });

        $('.selling_prices').each(function(){ 
            bodyFormData.append('selling_price[]', $(this).val() ); 
        });

        // $('.satuans').each(function(){ 
        //     bodyFormData.append('satuan[]', $(this).val() ); 
        // });

        $('.sub_total_detail').each(function(){ 
            bodyFormData.append('sub_total_details[]', $(this).val() ); 
        });

        $('#company').each(function(){ 
            bodyFormData.append('company', $(this).val()); 
        });

        $('#phone').each(function(){ 
            bodyFormData.append('phone', $(this).val()); 
        });

        $('#address').each(function(){ 
            bodyFormData.append('address', $(this).val()); 
        });

        $('#validity').each(function(){ 
            bodyFormData.append('validity', $(this).val()); 
        });

        $('#min_order').each(function(){ 
            bodyFormData.append('min_order', $(this).val()); 
        });

        $('#status_process').each(function(){ 
            bodyFormData.append('status_process', $(this).val()); 
        });

        $('#payment').each(function(){ 
            bodyFormData.append('payment', $(this).val()); 
        });

        $('#id_transaction').each(function(){ 
            bodyFormData.append('id_transaction', $(this).val()); 
        });

        $('#deliver').each(function(){ 
            bodyFormData.append('deliver', $(this).val()); 
        });

        $('#product_name').each(function(){ 
            bodyFormData.append('product_name', $(this).val()); 
        });

        $('#type_produk').each(function(){ 
            bodyFormData.append('type_produk', $(this).val()); 
        });

        $('#remark').each(function(){ 
            bodyFormData.append('remark', $(this).val()); 
        });

        $('.stoks').each(function(){ 
            bodyFormData.append('stok[]', $(this).val()); 
        });

        $('.hpps').each(function(){ 
            bodyFormData.append('hpp_average[]', $(this).val()); 
        });

        $('#id_trans').each(function(){ 
            bodyFormData.append('id_trans', $(this).val()); 
        });

        $('.bundle_details_id_input').each(function(){ 
            bodyFormData.append('bundle_details_id', $(this).val()); 
        });

        

        Helper.confirm(function() {
            Helper.loadingStart();
            Axios.post('/request-order-b-2-b/save-and-edit-quotation-order/'+ $('#sparepart_detail_order_id').val(), bodyFormData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
            .then(function(response) {
                window.location.href = Helper.redirectUrl('/admin/business-to-business-user-request-order/detail-request-order/' + $('#id_trans').val());
                // // location.reload();
                Helper.successNotif('Success Updated');
                Helper.loadingStop();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
        });
        e.preventDefault();
    })

</script>

<script>
    $(function() {
        Helper.dateFormat('.date_format');
        Helper.currency('.currency');
        ClassApp.hitungTotal();
        // $('.btn_add_details').prop("disabled", true);
        // $('select[name="part_data_stock_id[]"]').prop("disabled", true);

        $('.part_data_stock_ids').each(function(){
            // ClassApp.select2CodeMaterial($(this).attr('data-uniq'), $('#asc_id').val());
            ClassApp.select2CodeMaterialDetail($(this).attr('data-uniq'));
            
        })
    });
    globalCRUD.select2('#asc_id', '/asc-excel/select2');
    $("#form-data").submit(function(e) {
        ClassApp.saveData($(this), e);
    });

    $('#asc_id').on('select2:select', function (e) {
        asc_id = $(this).val();
        ClassApp.select2CodeMaterial('xxx',asc_id);
        $('.btn_add_details').prop("disabled", false);
        $('select[name="part_data_stock_id[]"]').prop("disabled", false);
        $('select[name="part_data_stock_id[]"]').each(function() {
            var uniq = $(this).attr('data-uniq');
            ClassApp.resetSelect2CodeMaterial(uniq);
            ClassApp.select2CodeMaterial(uniq,asc_id);
        });
    });

    

    // $('.qty-' + xx + '').keyup(function(e) {
    //     var max = parseInt($('.stok-' + xx + '').val());
    //     console.log(max)
    //     // if (parseInt($(this).val()) > max) {
    //     //     e.preventDefault();
    //     //     $(this).val(max);
    //     // } else {
    //     //     $(this).val();
    //     // }
    // });

    $(document).on('click', '.btn_delete_details', function(){

    })
</script>
@endsection