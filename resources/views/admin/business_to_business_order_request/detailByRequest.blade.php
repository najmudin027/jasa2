
@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-top: 10px;">
    @if ($teknisiSchedule < 1)
        <div class="alert alert-danger alert-regis" role="alert">
            <center>
                <i class="fa fa-bell-o"></i><strong> Order ini Belum Memilih Teknisi</strong>
                &nbsp;&nbsp;&nbsp;
                <a class="btn btn-outline-dange" href="{{ url("/admin/business-to-business-user-request-order/assign-teknisi/".$data->id) }}" style="border-color: rgb(159 21 21)!important;border: 1px solid; text-color:black"><i class="fa fa-plus"> <strong>Assign Teknisi</strong></i></a>
            </center>
        </div>
    @endif
    <div class="header-bg card-header-tab card-header">
        <div class="col-md-12">
            <div class="row">
                <input type="hidden" id="id_order" value="{{ $data->id }}">
                <div class="col-md-9">
                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                        @if (!empty($data->get_outlet))
                            Cabang : {{ $data->get_outlet->name }} ({{ $data->get_outlet->business_to_business_transaction != null ? $data->get_outlet->business_to_business_transaction->company->name : '-' }})
                        @else
                            Cabang : Outlet Deleted
                        @endif
                        
                    </div>
                </div>
                <div class="col-md-3 text-right">
                    
                </div>
            </div>
        </div>  
    </div><br>
    <div class="row">   
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                Order Detail
                            </div>
                            <div class="col-md-4 text-right">
                                    @if ($getSchedule > 0)
                                        <button type="button" data-id="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->id : ''}}" class="btn btn-success  edit-btb-outlet-item" ><i class="fa fa-pencil"></i> Edit Detail Orders</button>
                                    @else 
                                        <button type="button" data-id="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->id : ''}}" class="btn btn-success  edit-btb-outlet-item" disabled><i class="fa fa-pencil"></i> Edit Detail Orders</button>
                                    @endif
                                        {{-- @endif
                                    @else
                                    <button type="button" data-id="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->id : ''}}" class="btn btn-success  edit-btb-outlet-item"  style="pointer-events: none;cursor: not-allowed;text-decoration: none;"><i class="fa fa-pencil"></i> Edit Detail Order</button>
                                    @endif --}}
                                {{-- @if ($teknisiSchedule > 0)
                                    <button type="button" data-id="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->id : ''}}" class="btn btn-success  edit-btb-outlet-item"><i class="fa fa-pencil"></i> Edit Detail Order</button>
                                @else
                                    <button type="button" class="btn btn-success btn-xs edit-btb-outlet-item" disabled><i class="fa fa-pencil"></i> Edit Detail Order</button>
                                @endif --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    @if (!empty($data->get_b2b_detail))
                                        <label for=""><strong>Merek</strong></label>
                                        <p>{{!empty($data->get_b2b_detail->merek) ? $data->get_b2b_detail->merek : '-'}}</p>
                                        <label for=""><strong>Remark</strong></label>
                                        <p>{{!empty($data->get_b2b_detail->remark) ? $data->get_b2b_detail->remark : '-'}}</p>
                                        <label for=""><strong>Status</strong></label><br>
                                        @if($data->get_b2b_detail->status_quotation == "Requested")
                                            <p><span class='badge badge-primary'>Requested</span></p>
                                        @elseif($data->get_b2b_detail->status_quotation == "Process")
                                            <p><span class='badge badge-success'>Process</span></p>
                                        @elseif($data->get_b2b_detail->status_quotation == "Scheduling")
                                            <p><span class='badge badge-danger'>Scheduling</span></p>
                                        @elseif($data->get_b2b_detail->status_quotation == "Finished")
                                            <p><span class='badge badge-info'>Finished</span></p>
                                        @elseif($data->get_b2b_detail->status_quotation == "Waiting Payment")
                                            <p><span class='badge badge-success'>Waiting Payment</span></p>
                                        @else
                                            <p><span class='badge badge-secondary'>-</span></p>
                                        @endif
                                        <label for=""><strong>No Quotation</strong></label>
                                        <p>{{!empty($data->get_b2b_detail->no_quotation) ? $data->get_b2b_detail->no_quotation : '-'}}</p>
                                        <label for=""><strong>No PO</strong></label>
                                        <p>{{!empty($data->get_b2b_detail->no_po) ? $data->get_b2b_detail->no_po : '-'}}</p>
                                        <label for=""><strong>No Invoice</strong></label>
                                        @if($data->get_b2b_detail->status_quotation == "Waiting Payment")
                                            <p>{{!empty($data->get_b2b_detail->no_quotation) ? str_replace('QUOT', 'INV', $data->get_b2b_detail->no_quotation) : '-'}}</p>
                                        @else
                                           <p>-</p>
                                        @endif
                                        <label for=""><strong>Type Service</strong></label>
                                        <p>{{!empty($data->service_type) != null ? $data->service_type : '-'}}</p>
                                        {{-- <label for=""><strong>Invoice</strong></label><br>
                                        <a href="{{url('/business-to-business-user-request-order/preview-invoice/')}}">Preview Invoice</a> --}}
                                    @endif
                                    
                                    
                                </div>
                                <div class="col-md-6">
                                    <label for=""><strong>Unit Ke</strong></label>
                                    @if (!empty($data->get_b2b_detail))
                                        @if (!empty($data->get_b2b_detail->unit_ke))
                                            <p>{{$data->get_b2b_detail->unit_ke}}</p>
                                        @else
                                            <p>-</p>
                                        @endif
                                        <p>-</p>

                                        {{-- <p>{{!empty($data->get_b2b_detail->unit_ke) ? $data->get_b2b_detail->unit_ke : '-'}}</p> --}}
                                        <!--<label for=""><strong>Nominal Quotation</strong></label>-->
                                        {{-- <!--<p>Rp.{{!empty($data->get_b2b_detail->nominal_quot) ? bumber_format($data->get_b2b_detail->nominal_quot)  : '-'}}</p>--> --}}
                                        <label for=""><strong>Payment Date</strong></label>
                                        <p>{{!empty($data->get_b2b_detail->tanggal_pembayaran) != null ? date('d-M-Y', strtotime($data->get_b2b_detail->tanggal_pembayaran)) : '-'}}</p>
                                        <label for=""><strong>Date Quot</strong></label>
                                        <p>{{!empty($data->get_b2b_detail->tanggal_quot) != null ? date('d-M-Y', strtotime($data->get_b2b_detail->tanggal_quot)) : '-'}}</p>
                                        <label for=""><strong>Purchase Order Date</strong></label>
                                        <p>{{!empty($data->get_b2b_detail->tanggal_po) != null ? date('d-M-Y', strtotime($data->get_b2b_detail->tanggal_po)) : '-'}}</p>
                                        <label for=""><strong>Invoice Date</strong></label>
                                        <p>{{!empty($data->get_b2b_detail->tanggal_Invoice) != null ? date('d-M-Y', strtotime($data->get_b2b_detail->tanggal_Invoice)) : '-'}}</p>
                                        <label for=""><strong>Tanggal Pengerjaan</strong></label>
                                        <p>{{!empty($data->get_b2b_detail->tanggal_pengerjaan) != null ? date('d-M-Y', strtotime($data->get_b2b_detail->tanggal_pengerjaan)) : '-'}}</p>
                                        <label for=""><strong>Create Quotation Order</strong></label>
                                        @if ($getSchedule > 0)
                                            @if ($getDataQuot > 0)
                                                <button class="btn btn-primary" disabled>Create Quotation</button>
                                            @else
                                                <a href="{{ url('/admin/business-to-business-user-request-order/create-quotation/'.$data->id) }}" class="btn btn-primary">Create Quotation</a>
                                            @endif
                                        @else
                                            <!--<a href="{{ url('/admin/business-to-business-user-request-order/create-quotation/'.$data->id) }}" class="btn btn-primary" style="pointer-events: none;cursor: no-drop;text-decoration: none;">Create Quotation</a>-->
                                            <button class="btn btn-primary" disabled>Create Quotation</button>
                                        @endif
                                    @endif
                                    
                                    
                                </div>
                            </div><hr>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            
        </div> <hr>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            @if (!empty($data->get_outlet))
                                @if (!empty($data->get_outlet->business_to_business_transaction))
                                    <h5 class="menu-header-title"> {{ $data->get_outlet->business_to_business_transaction->company->name }}</h5>
                                @else
                                    <h5 class="menu-header-title">  -</h5>
                                @endif
                            @else 
                                <h5 class="menu-header-title">  -</h5>
                            @endif
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <label for=""><Strong>Nama Pic</Strong></label>
                                @if (!empty($data->get_outlet))
                                    @if (!empty($data->get_outlet->business_to_business_transaction))
                                        <p> {{ !empty($data->get_outlet->business_to_business_transaction->company->nama_pic) ? $data->get_outlet->business_to_business_transaction->company->nama_pic : '-' }}</p>
                                    @else
                                        <p>  -</p>
                                    @endif
                                    <!--<h5 class="menu-header-title">  -</h5>-->
                                @endif
                            </div>
                            <div class="col-md-6 text-right">
                                <label for=""><Strong>Nomor Pic</Strong></label>
                                @if (!empty($data->get_outlet))
                                    @if (!empty($data->get_outlet->business_to_business_transaction))
                                        <p> {{ !empty($data->get_outlet->business_to_business_transaction->company->phone) ? $data->get_outlet->business_to_business_transaction->company->phone : '-' }}</p>
                                    @else
                                        <p>  -</p>
                                    @endif
                                    <!--<h5 class="menu-header-title">  -</h5>-->
                                @endif
                            </div>
                            <div class="col-md-6">
                                <label for=""><Strong>Email</Strong></label>
                                @if (!empty($data->get_outlet))
                                    @if (!empty($data->get_outlet->business_to_business_transaction))
                                        <p> {{ !empty($data->get_outlet->business_to_business_transaction->company->email) ? $data->get_outlet->business_to_business_transaction->company->email : '-' }}</p>
                                    @else
                                        <p>  -</p>
                                    @endif
                                <!--<h5 class="menu-header-title">  -</h5>-->
                                @endif
                            </div>
                            <div class="col-md-6 text-right">
                                <label for=""><strong>NPWP</strong></label>
                                @if (!empty($data->get_outlet))
                                    @if (!empty($data->get_outlet->business_to_business_transaction))
                                        <p> {{ !empty($data->get_outlet->business_to_business_transaction->company->npwp) ? $data->get_outlet->business_to_business_transaction->company->npwp : '-' }}</p>
                                    @else
                                        <p>  -</p>
                                    @endif
                                    <!--<h5 class="menu-header-title">  -</h5>-->
                                @endif
                            </div>
                            <div class="col-md-6">
                                <label for=""><Strong>Nomor Rekening</Strong></label>
                                @if (!empty($data->get_outlet))
                                    @if (!empty($data->get_outlet->business_to_business_transaction))
                                        <p> {{ !empty($data->get_outlet->business_to_business_transaction->company->nomor_rekening) ? $data->get_outlet->business_to_business_transaction->company->nomor_rekening : '-' }}</p>
                                    @else
                                        <p>  -</p>
                                    @endif
                                <!--<h5 class="menu-header-title">  -</h5>-->
                                @endif
                            </div>
                            <div class="col-md-6 text-right">
                                <label for=""><strong>Nama Bank</strong></label>
                                @if (!empty($data->get_outlet))
                                    @if (!empty($data->get_outlet->business_to_business_transaction))
                                        <p> {{ !empty($data->get_outlet->business_to_business_transaction->company->nama_bank) ? $data->get_outlet->business_to_business_transaction->company->nama_bank : '-' }}</p>
                                    @else
                                        <p>  -</p>
                                    @endif
                                    <!--<h5 class="menu-header-title">  -</h5>-->
                                @endif
                            </div>
                            <div class="col-md-12">
                                <label for=""><strong>Alamat</strong></label>
                                @if (!empty($data->get_outlet))
                                    @if (!empty($data->get_outlet->business_to_business_transaction))
                                        <p> {{ !empty($data->get_outlet->business_to_business_transaction->company->alamat) ? $data->get_outlet->business_to_business_transaction->company->alamat : '-' }}</p>
                                    @else
                                        <p>  -</p>
                                    @endif
                                <!--<h5 class="menu-header-title">  -</h5>-->
                                @endif
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div><hr>

            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            @if (!empty($data->get_outlet))
                                <h5 class="menu-header-title"> {{ $data->get_outlet->name }}</h5>
                            @else
                                <h5 class="menu-header-title">  -</h5>
                            @endif
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <label for=""><Strong>Jumlah Unit</Strong></label>
                                @if (!empty($data->get_outlet))
                                    <p> {{ !empty($data->get_outlet->jumlah_unit) ? $data->get_outlet->jumlah_unit : '-' }}</p>
                                @else
                                    <p>  -</p>
                                @endif
                            </div>
                            <div class="col-md-6 text-right">
                                <label for=""><Strong>Type</Strong></label>
                                @if (!empty($data->get_outlet))
                                    <p> {{ !empty($data->get_outlet->type) ? $data->get_outlet->type : '-' }}</p>
                                @else
                                    <p>  -</p>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <label for=""><Strong>Nama Pic</Strong></label>
                                @if (!empty($data->get_outlet))
                                    <p> {{ !empty($data->get_outlet->nama_pic) ? $data->get_outlet->nama_pic : '-' }}</p>
                                @else
                                    <p>  -</p>
                                @endif
                            </div>
                            <div class="col-md-6 text-right">
                                <label for=""><strong>Nomor Pic</strong></label>
                                @if (!empty($data->get_outlet))
                                    <p> {{ !empty($data->get_outlet->nomor_pic) ? $data->get_outlet->nomor_pic : '-' }}</p>
                                @else
                                    <p>  -</p>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <label for=""><strong>Alamat</strong></label>
                                @if (!empty($data->get_outlet))
                                    <p> {{ !empty($data->get_outlet->alamat) ? $data->get_outlet->alamat : '-' }}</p>
                                @else
                                    <p>  -</p>
                                @endif
                            </div>
                            <div class="col-md-12">
                                @if (!empty($data->get_outlet))
                                    <a href="" data-toggle="modal" data-target="#modal_maps" data-lat='{{ $data->get_outlet->lat ?? '' }}' data-lng='{{ $data->get_outlet->long ?? '' }}' style="text-decoration: underline"><i class="fa fa-map-marker"></i> View Location</a>
                                @else
                                    <p>  -</p>
                                @endif
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div><hr>
        </div> <hr>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <form id="ubah_status">
                        <input type="hidden" id="get_id_detail_trans" name="id_status" value="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->id : '' }}">
                        <div class="form-group">
                            <label>Status</label>
                            <select name="status_quotation" class="btb-outlet-item-status_quotation form-control" id="exampleFormControlSelect1">
                                @if(!empty($data->get_b2b_detail))
                                    @if ($getSchedule > 0)
                                        @if (!empty($data->get_b2b_detail->tanggal_pengerjaan))
                                            <option value="Requested" {{$data->get_b2b_detail->status_quotation == "Requested" ? 'selected' : null}}>Requested</option>
                                            <option value="Process" {{$data->get_b2b_detail->status_quotation == "Process" ? 'selected' : null}}>Process</option>
                                            {{-- <option value="Scheduling" {{$data->get_b2b_detail->status_quotation == "Scheduling" ? 'selected' : null}}>Scheduling</option> --}}
                                            <option value="Finished" {{$data->get_b2b_detail->status_quotation == "Finished" ? 'selected' : null}}>Finished</option>
                                            <option value="Waiting Payment" {{$data->get_b2b_detail->status_quotation == "Waiting Payment" ? 'selected' : null}}>Waiting Payment</option>
                                        @else
                                            <option value="Requested" {{$data->get_b2b_detail->status_quotation == "Requested" ? 'selected' : null}}>Requested</option>
                                            <option value="Process" {{$data->get_b2b_detail->status_quotation == "Process" ? 'selected' : null}}>Process</option>
                                            {{-- <option value="Scheduling" {{$data->get_b2b_detail->status_quotation == "Scheduling" ? 'selected' : null}}>Scheduling</option> --}}
                                            <option value="Finished" {{$data->get_b2b_detail->status_quotation == "Finished" ? 'selected' : null}} disabled>Finished</option>
                                            <option value="Waiting Payment" {{$data->get_b2b_detail->status_quotation == "Waiting Payment" ? 'selected' : null}} disabled>Waiting Payment</option>
                                        @endif
                                        
                                    @else
                                        <option value="Requested" {{$data->get_b2b_detail->status_quotation == "Requested" ? 'selected' : null}}>Requested</option>
                                        <option value="Process" {{$data->get_b2b_detail->status_quotation == "Process" ? 'selected' : null}}>Process</option>
                                        {{-- <option value="Scheduling" {{$data->get_b2b_detail->status_quotation == "Scheduling" ? 'selected' : null}} disabled>Scheduling</option> --}}
                                        <option value="Finished" {{$data->get_b2b_detail->status_quotation == "Finished" ? 'selected' : null}} disabled>Finished</option>
                                        <option value="Waiting Payment" {{$data->get_b2b_detail->status_quotation == "Waiting Payment" ? 'selected' : null}} disabled>Waiting Payment</option>
                                    @endif
                                @endif
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm btn-block">Ubah Status</button>
    
                    </form>
                </div>
            </div><br>
            <div class="card">
                <div class="card-header header-border">
                    Attachment
                </div>
                <div class="card-body">
                   <div class="form-group">
                       <div class="form-group">
                            <label>Upload Berita acara</label><br>
                            @if ($cekBeritaAcara > 0)
                                <a href="" class="text-right" id="show-berita-acara" data-toggle="modal" data-type="file_berita_acara" data-ids="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->id : ''}}" data-target="#exampleModal">View Attachment</a>
                            @else 
                                <p>-</p>
                            @endif
                        </div>
                        
                        <label>File Quotation</label><br>
                        @if ($hitungQuot > 0)
                            <a href="{{ url('/admin/business-to-business-user-request-order/file-quotation/'.$data->id) }}" target="_blank" class="text-right">View Quotation</a>
                        @else 
                            <p>-</p>
                        @endif
                    </div>
                    
                    <div class="form-group">
                        <label>Upload PO</label><br>
                        @if ($cekPo > 0)
                            <a href="" class="text-right" id="show-po" data-toggle="modal" data-type="image_po" data-ids="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->id : ''}}" data-target="#exampleModal">View Attachment</a>
                        @else 
                            <p>-</p>
                        @endif
                    </div>
                    
                    <div class="form-group">
                        <label>File Invoice</label><br>
                        @if ($hitungQuot > 0)
                            @if (!empty($data->get_b2b_detail))
                                @if ($data->get_b2b_detail->status_quotation == 'Waiting Payment')
                                    <a href="{{ url('/admin/business-to-business-user-request-order/preview-invoice/'.$data->id) }}" target="_blank" class="text-right">Preview File Invoice</a>
                                @else 
                                    <p>Belum Bisa Menampilkan File Invoice</p>
                                @endif
                            @else
                                <p>Belum Bisa Menampilkan File Invoice</p>
                            @endif
                        @else
                            <p>Belum Bisa Menampilkan File Invoice</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Order Image</label><br>
                        @if ($cekOrderImage->image)
                            <a href="" class="text-right" id="show_image_order" data-toggle="modal" data-ids="{{!empty($data->id) ? $data->id : ''}}" data-target="#exampleModal">View Attachment</a>
                        @else
                            <p>-</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Order Vidio</label><br>
                        @if (!empty($cekOrderImage->vidio))
                            <a href="{{ asset('/videos/'.$cekOrderImage->vidio) }}" target="_blank">View Video</a>
                            <!-- <a href="" class="text-right" id="show_image_order" data-toggle="modal" data-ids="{{!empty($data->id) ? $data->id : ''}}" data-target="#exampleModal">View Attachment</a> -->
                        @else
                            <p>-</p>
                        @endif
                    </div>
                </div>
            </div>
        </div> <hr>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            @if (!empty($cekStatus))
                @if ($cekStatus->status == 'Tiba Ditempat' || $cekStatus->status == null)
                    <div class="alert alert-warning" role="alert">
                        <h6><strong>Tidak Bisa Menambah Teknisi Status Berganti Selesai atau Menjadwal Ulang </strong></h6>
                    </div>
                @endif
            @endif
            <div class="card">
                <div class="card-header header-border">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">List Teknisi</div>
                            <div class="col-md-4 text-right">
                                @if (!empty($cekStatus))
                                    @if ($cekStatus->status == 'Tiba Ditempat' || $cekStatus->status == null)
                                        {{-- <a class="btn btn-primary btn-xs" disabled href="{{ url("/admin/business-to-business-user-request-order/assign-teknisi/".$data->id) }}"><i class="fa fa-plus">Assign Teknisi</i></a> --}}
                                    @else
                                        <a class="btn btn-primary btn-xs" href="{{ url("/admin/business-to-business-user-request-order/assign-teknisi/".$data->id) }}"><i class="fa fa-plus">Assign Teknisi</i></a>
                                    @endif
                                @endif
                                {{-- @if ($teknisiSchedule < 1)
                                    <a class="btn btn-primary btn-xs" href="{{ url("/admin/business-to-business-user-request-order/assign-teknisi/".$data->id) }}"><i class="fa fa-plus">Assign Teknisi</i></a>
                                @endif --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="tables-technician-expand" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Teknisi</th>
                                <th>Status</th>
                                <th>Jadwal</th>
                                <th>Action</th>
                                {{-- <th>Tanggal Mulai</th> --}}
                                {{-- <th>Checkin</th>
                                <th>Checkout</th>
                                <th>Reschedule</th> --}}
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
<br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header header-border">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-10">List Quotation</div>
                            <div class="col-md-2 text-right">
                                @if (!empty($data->get_b2b_detail))
                                    @if ($data->get_b2b_detail->status_quotation == 'Waiting Payment' && $getDataQuot > 0)
                                        <a href="{{ url('/admin/business-to-business-user-request-order/preview-invoice/'.$data->id) }}" target="_blank" class="btn btn-primary btn-block btn-sm"><i class="fa fa-file-text"></i> &nbsp; View Invoice</a>
                                    @else 
                                        <button disabled="disabled" class="btn btn-primary btn-block btn-sm"><i class="fa fa-file-text"></i> &nbsp; View Invoice</button>
                                    @endif
                                    <!--<button disabled="disabled" class="btn btn-primary btn-block btn-sm"><i class="fa fa-file-text"></i> &nbsp; View Invoice</button>-->
                                @endif
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="table-list-quotations" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Quotation Code</th>
                                <th>Company</th>
                                <th>Status</th>
                                <th>Revisi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="get_id_quotation" value="{{ $data->id }}">
</div>
@include('admin.order.service_detail_style')
<style>
    .our-team .picture::before,
    .our-team .picture::after,
    .our-team .social {
        background-color: #ed463b;
    }
</style>
<form id="form-btb-outlet-detail-item">
    <div class="modal fade" id="modal-btb-outlet-item" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title modal-title-btb-outlet-item"></h4>
                </div>
                {{-- <input type="text" name="show_status" value="{{ !empty($data->get_b2b_detail) ? $data->get_b2b_detail->status_quotation : '' }}"> --}}
                <div class="modal-body">
                    <input type="hidden" class="btb-outlet-item-id" value="{{ !empty($data->ms_b2b_detail) ? $data->ms_b2b_detail : '' }}">
                    {{-- <input type="hidden" name="business_to_business_outlet_transaction_id" id="business_to_business_outlet_transaction_id" value="{{ $outletDetail->business_to_business_outlet_transaction_id }}">
                    <input type="hidden" name="is_parent_b2b_outlet_id" id="is_parent_b2b_outlet_id" value="{{ $outletDetail->id }}"> --}}

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Unit Ke</label>
                                    {{-- <select name="unit_ke" class="form-control btb-outlet-item-unit_ke" required>
                                    </select> --}}
                                    <input type="text" name="unit_ke" value="1" class="form-control" value="{{ !empty($data->get_b2b_detail) ? $data->get_b2b_detail->unit_ke : '' }}" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Merek</label>
                                <input name="merek" placeholder="merek" type="text" class="form-control" value="{{ !empty($data->get_b2b_detail) ? $data->get_b2b_detail->merek : '' }}" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Remark</label>
                                <input name="remark" placeholder="remark" type="text" class="btb-outlet-item-remark form-control" value="{{ !empty($data->get_b2b_detail) ? $data->get_b2b_detail->remark : ''}}">
                            </div>
                        </div>
                    </div>

                    {{-- <hr /> --}}
                    <div class="row">
                        <div class="col-md-4" hidden>
                            <div class="form-group">
                                <label>No Quotation</label>
                                <input name="no_quotation" placeholder="No Quotation" type="text" class="btb-outlet-item-no_quotation form-control" value="{{ !empty($data->get_b2b_detail) ? $data->get_b2b_detail->no_quotation : ''}}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>No Po</label>
                                <input name="no_po" placeholder="No PO" type="text" class="btb-outlet-item-no_po form-control" value="{{ !empty($data->get_b2b_detail) ? $data->get_b2b_detail->no_po : ''}}">
                            </div>
                        </div>
                        <div class="col-md-4" hidden>
                            <div class="form-group">
                                <label>No Invoice</label>
                                <input name="no_invoice" placeholder="No Invoice" type="text" class="btb-outlet-item-no_invoice form-control" value="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->no_invoice : ''}}">
                            </div>
                        </div>
                    </div>
                    {{-- <hr /> --}}

                    <div class="row">
                        {{-- <div class="col-md-6">
                            <div class="form-group">
                                <label>Nominal Quotation</label>
                                <input name="nominal_quot" placeholder="Nominal Quot" type="number" class="btb-outlet-item-nominal_quot form-control">
                            </div>
                        </div> --}}
                        {{-- <div class="col-md-12">
                            <div class="form-group">
                                <label>Status Quotation</label>
                                <input name="status_quotation" placeholder="Status Quotation" type="text" class="btb-outlet-item-status_quotation form-control">
                                <select name="status_quotation" class="btb-outlet-item-status_quotation form-control" id="exampleFormControlSelect1">
                                    @if(!empty($data->get_b2b_detail))
                                        <option value="Requested" {{$data->get_b2b_detail->status_quotation == "Requested" ? 'selected' : null}}>Requested</option>
                                        <option value="Process" {{$data->get_b2b_detail->status_quotation == "Process" ? 'selected' : null}}>Process</option>
                                        <option value="Scheduling" {{$data->get_b2b_detail->status_quotation == "Scheduling" ? 'selected' : null}}>Scheduling</option>
                                        <option value="Finished" {{$data->get_b2b_detail->status_quotation == "Finished" ? 'selected' : null}}>Finished</option>
                                        <option value="Payment" {{$data->get_b2b_detail->status_quotation == "Waiting Payment" ? 'selected' : null}}>Waiting Payment</option>
                                    @endif
                                </select>
                            </div>
                        </div> --}}
                    </div>
                    {{-- <hr /> --}}
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Tanggal Quotation</label>
                                @if ($hitungQuot > 0)
                                    <input name="tanggal_quot" value="{{ date('Y-m-d', strtotime($ambil_tanggal_quot->created_at)) }}" placeholder="Tanggal Quotation" type="text" class="datepick btb-outlet-item-tanggal_quot form-control">
                                @else
                                    <input name="tanggal_quot" placeholder="Tanggal Quotation" type="text" class="datepick btb-outlet-item-tanggal_quot form-control">
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Tanggal PO</label>
                                <input name="tanggal_po" value="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->tanggal_po : ''}}" placeholder="Tanggal PO" type="text" class="datepick btb-outlet-item-tanggal_po form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Tanggal pengerjaan</label>
                                <input name="tanggal_pengerjaan" value="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->tanggal_pengerjaan : ''}}" placeholder="Tanggal Pengerjaan" type="text" class="datepick btb-outlet-item-tanggal_pengerjaan form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Pembayaran</label>
                                <input name="tanggal_pembayaran" value="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->tanggal_pembayaran : ''}}" placeholder="Tanggal Pembayaran" type="text" class="datepick btb-outlet-item-tanggal_pembayaran form-control">
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Invoice</label>
                                {{-- @if (!empty($data->get_b2b_detail))
                                    @if ($data->get_b2b_detail->status_quotation == 'Payment')
                                        @php
                                            if(!empty($ambil_tanggal_quot)){
                                                $tgl_quot = $ambil_tanggal_quot->created_at;
                                            }else{
                                                $tgl_quot = null;
                                            }
                                        @endphp
                                        <input name="tanggal_Invoice" value="{{ date('Y-m-d', strtotime($tgl_quot)) }}" placeholder="Tanggal Invoice" type="text" class="datepick btb-outlet-item-tanggal_Invoice form-control">
                                    @else
                                        <input name="tanggal_quot"  placeholder="Tanggal Quotation" type="text" class="datepick btb-outlet-item-tanggal_quot form-control">
                                    @endif
                                @else --}}
                                    <input name="tanggal_Invoice" value="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->tanggal_Invoice : ''}}"  placeholder="Tanggal Quotation" type="text" class="datepick btb-outlet-item-tanggal_quot form-control">
                                {{-- @endif --}}
                                    
                            </div>
                        </div>
                        
                    </div>
                    {{-- <hr /> --}}

                    @php
                        $mytime = Carbon\Carbon::now();
                    @endphp
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6"><label>File Quotation</label></div>
                                </div>
                                @if ($hitungQuot > 0)
                                    <a href="{{ url('/admin/business-to-business-user-request-order/file-quotation/'.$data->id) }}" target="_blank" class="text-right">Preview File Quotation</a>
                                @else 
                                    <p>Belum Bisa Menampilkan File Quotation</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6"><label>File Invoice</label></div>
                                </div>
                                {{-- <input name="file_invoice" type="file" class="btb-outlet-item-file_invoice form-control"> --}}
                                @if ($hitungQuot > 0)
                                    @if (!empty($data->get_b2b_detail))
                                        @if ($data->get_b2b_detail->status_quotation == 'Waiting Payment')
                                            <a href="{{ url('/admin/business-to-business-user-request-order/preview-invoice/'.$data->id) }}" target="_blank" class="text-right">Preview File Invoice</a>
                                        @else 
                                            <p>Belum Bisa Menampilkan File Invoice</p>
                                        @endif
                                    @else
                                        <p>Belum Bisa Menampilkan File Invoice</p>
                                    @endif
                                @else
                                    <p>Belum Bisa Menampilkan File Invoice</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6"><label>Upload Berita acara</label></div>
                                    <div class="col-md-6">
                                        @if ($cekBeritaAcara > 0)
                                            <a href="" class="text-right" id="show-berita-acara" data-toggle="modal" data-type="file_berita_acara" data-ids="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->id : ''}}" data-target="#exampleModal" style="float: right">View Attachment</a>
                                        @else 
                                            <p>-</p>
                                        @endif
                                    </div>
                                </div>
                                <input name="file_berita_acara" type="file" class="btb-outlet-item-file_berita_acara form-control">
                                {{-- @if (!empty($data->get_b2b_detail->file_quot))
                                    <span>
                                        <br>
                                        <a href="{{ url("/admin/business_to_business/" . $data->get_b2b_detail->uniq_key . "/download?type=file_quot&exptime=" ) }}" 
                                            target="__blank" 
                                            class="btn btn-xs btn-info">
                                            <i class="fa fa-download"></i><span class="badge badge-primary"></span>{{ $data->get_b2b_detail->file_quot }}</a>
                                    </span>
                                @endif --}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6"><label>Upload PO</label></div>
                                    <div class="col-md-6">
                                        @if ($cekPo > 0)
                                            <a href="" class="text-right" id="show-po" data-toggle="modal" data-type="image_po" data-ids="{{!empty($data->get_b2b_detail) ? $data->get_b2b_detail->id : ''}}" data-target="#exampleModal" style="float: right">View Attachment</a>
                                        @else 
                                            <p>-</p>
                                        @endif
                                    </div>
                                </div>
                                <input name="file_po" type="file" class="btb-outlet-item-file_po form-control">
                                {{-- @if (!empty($data->get_b2b_detail->file_po))
                                    <span>
                                        <br>
                                        <a href="{{ url("/admin/business_to_business/" . $data->get_b2b_detail->uniq_key . "/download?type=file_po&exptime=" ) }}" 
                                            target="__blank" 
                                            class="btn btn-xs btn-info">
                                            <i class="fa fa-download"></i><span class="badge badge-primary"></span>{{ $data->get_b2b_detail->file_po }}</a>
                                    </span>
                                @endif --}}
                            </div>
                        </div>
                        
                    </div>
                    
                    

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
                    <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
</form>


<style>
    td.details-control {
        background: url('https://www.datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('https://www.datatables.net/examples/resources/details_close.png') no-repeat center center;
    }

    .custom-control-label-2{
        margin: 0;
    }
</style>
@endsection

@section('script')
@include('admin.template._locationServiceDetail')
@include('admin.business_to_business_order_request._modelBeritaAcara')
@include('admin.business_to_business_order_request._modalOrderImage')
@include('admin.business_to_business_order_request._modelQuotation')
@include('admin.business_to_business_order_request._modelInvoice')
@include('admin.business_to_business_order_request._modelPo')
{{-- @include('admin.business_to_business_order_request.assignTeknisi') --}}


<script>

    function format ( d ) {
        if(d.checkout !== null){
            checkout = moment(d.checkout).format("DD MMMM YYYY HH:mm");
        }else{
            checkout = '-';
        }

        if(d.status !== null){
            status = d.status;
        }else{
            status = '-';
        }

        if(d.checkin !== null){
            checkin = moment(d.checkin).format("DD MMMM YYYY HH:mm");
            $
        }else{
            checkin = '-';
        }

        if(d.reschedule !== null){
            schedule = moment(d.reschedule).format("DD MMMM YYYY HH:mm");
        }else{
            schedule = '-';
        }

        if(d.remark !== null){
            remark = d.remark;
        }else{
            remark = '-';
        }
        // `d` is the original data object for the row
        return '<div class="col-md-12">'+
                    '<div class="row">'+
                        '<div class="col-md-6">'+
                            '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
                                '<tr>'+
                                    '<td>Check In :</td>'+
                                    '<td>'+ checkin +'</td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Checkout :</td>'+
                                    '<td>'+ checkout +'</td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<td>Remark :</td>'+
                                    '<td>'+ remark +'</td>'+
                                '</tr>'+
                                // '<tr>'+
                                //     '<td>Penjadwalan Ulang :</td>'+
                                //     '<td>'+ schedule +'.</td>'+
                                // '</tr>'+
                            '</table>'+
                        '</div>'+
                        
                    '</div>'+
                '</div>';
    }
    // '<div class="col-md-6">'+
    //                         '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
    //                             '<tr>'+
    //                                 '<td>Foto Teknisi</td>'+
    //                                 '<td><img src="/storage/image/profile-teknisi/'+d.get_teknisi.image_teknisi+'" alt="" width="40%"></td>'+
    //                             '</tr>'+
    //                         '</table>'+
    //                     '</div>'+

    $(document).ready(function() {
        var table = $('#tables-technician-expand').DataTable( {
            "ajax": Helper.apiUrl('/request-order-b-2-b/list-schedule-teknisi/'+$('#id_order').val()),
            "columns": [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": ''
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        if(full.get_teknisi !== null){
                            return full.get_teknisi.name;
                        }else{
                            return '-';
                        }
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        if(full.status !== null){
                            if(full.status == 'Tiba Ditempat'){
                                return '<span class="badge badge-primary badge-pill">Tiba Ditempat</span>';
                            }else if(full.status == 'Selesai'){
                                return '<span class="badge badge-success badge-pill">Selesai</span>';
                            }else if(full.status == 'Penjadwalan Ulang'){
                                return '<span class="badge badge-warning badge-pill" style="color:white">Penjadwalan Ulang</span>';
                            }else{
                                return '-';
                            }
                        }else{  
                            return '-';
                        }
                    }
                },
                {
                    data: "schedule_date",
                    name: "schedule_date",
                    render: function(data, type, full) {
                        return moment(full.schedule_date).format("DD MMMM YYYY");
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        if(full.checkin == null){
                            return "<a class='btn btn-primary btn-sm' target='_blank' href='" + Helper.url('/admin/business-to-business-user-request-order/update-schedule-teknisi/'+full.id) + "'><i class='fa fa-clock-o' aria-hidden='true'></i></a>";
                        }else{
                            return '-';
                        }
                        
                    }
                },
            ],
            "order": [[1, 'asc']]
        });

        $('#tables-technician-expand tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );
    
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );
    });


    Helper.datePick('.datepick');
    $(document).on('click', '.edit-btb-outlet-item', function() {
        $('#modal-btb-outlet-item').modal('show');
        $('.modal-title-btb-outlet-item').text('EDIT OUTLET ITEM')
    })

    const generateFormDataDetail = function(input) {
        var formData = new FormData();

        _.each(input, function(val, key) {
            formData.append(key, val);
        })

        file_berita_acara = $('.btb-outlet-item-file_berita_acara').prop('files')[0];
        if (file_berita_acara) {
            formData.append("file_berita_acara", file_berita_acara);
        }

        // file_quot = $('.btb-outlet-item-file_quot').prop('files')[0];
        // if (file_quot) {
        //     formData.append("file_quot", file_quot);
        // }

        file_po = $('.btb-outlet-item-file_po').prop('files')[0];
        if (file_po) {
            formData.append("file_po", file_po);
        }

        // file_invoice = $('.btb-outlet-item-file_invoice').prop('files')[0];
        // if (file_invoice) {
        //     formData.append("file_invoice", file_invoice);
        // }

        return formData;
    }

    $('#form-btb-outlet-detail-item').submit(function(e) {
        e.preventDefault();
        Helper.loadingStart();

        data = Helper.serializeForm($(this));
        formData = generateFormDataDetail(data);

        id = $('.btb-outlet-item-id').val();
        if (id === '') {
            Axios.post('/business_to_business/outlet_detail_add_parent', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    // alert('Masuk ke if');
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // // refresh
                    // tableOrder.ajax.reload();
                    location.reload();
                    // // hide modal
                    $('#modal-btb-outlet-item').modal('hide')
                    // // empty
                    kosongkanFielDetail();
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        } else {
            Axios.post('/business_to_business/' + id + '/outlet_detail', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    // alert('Masuk ke ellse');
                    // send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    // tableOrder.ajax.reload();
                    location.reload();
                    // hide modal
                    $('#modal-btb-outlet-item').modal('hide')
                    // empty
                    kosongkanFielDetail();
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        }
    })

    $('#ubah_status').submit(function(e) {
        e.preventDefault();
        Helper.loadingStart();

        data = Helper.serializeForm($(this));
        formData = generateFormDataDetail(data);

        id = $('#get_id_detail_trans').val();
        if (id !== '') {
            Axios.post('/business_to_business/' + id + '/change_status', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    // alert('Masuk ke ellse');
                    // send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    // tableOrder.ajax.reload();
                    location.reload();
                    // hide modal
                    // $('#modal-btb-outlet-item').modal('hide')
                    // empty
                    kosongkanFielDetail();
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        } else {
            Helper.handleErrorResponse('Id Tidak Ditemukan')
            Helper.loadingStop();
        }
    })
</script>

<script>
    $(document).ready(function() {
        var table = $('#table-list-quotations').DataTable({
            processing: true,
            serverSide: true,
            select: false,
            dom: 'Bflrtip',
            responsive: true,
            order: [1, 'asc'],
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/request-order-b-2-b/list-quotation-order/' + $('#get_id_quotation').val()),
                "type": "get",
            },
            // columnDefs: [
            //     {
            //         "targets": [ 1 ],
            //         "visible": false,
            //         "searchable": false
            //     },
            // ]
            columns: [
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                       return "<a target='_blank' href='" + Helper.url('/admin/business-to-business-user-request-order/preview-quotation/' + row.id) + "'>"+ row.quotation_code +"</a>";

                        // return moment(row.created_at).format("DD MMMM YYYY");
                    }
                },
                {
                    data: "company",
                    name: "company",
                    render: function(data, type, row) {
                        return row.company;
                        // return moment(row.created_at).format("DD MMMM YYYY");
                    }
                },
                {
                    data: "status",
                    name: "status",
                    render: function(data, type, row) {
                        if(row.status == 1){
                            return '<span class="badge badge-primary badge-pill">New</span>';
                        }else{
                            return '<span class="badge badge-success badge-pill">Approved</span>';
                        }
                    }
                },
                {
                    data: "revisi_ke",
                    name: "revisi_ke",
                    render: function(data, type, row) {
                        if(row.revisi_ke == 000 ){
                            return '-';
                        }else{
                            return row.revisi_ke;
                        }
                        
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        view_link = "<a class='btn btn-primary btn-sm' href='" + Helper.url('/admin/business-to-business-user-request-order/view-quotation/' + row.id) + "'><i class='fa fa-edit'></i></a>";
                        view_template = "<a class='btn btn-primary btn-sm' target='_blank' href='" + Helper.url('/admin/business-to-business-user-request-order/preview-quotation/' + row.id) + "'><i class='fa fa-eye'></i></a>";
                        view_inv = "<a class='btn btn-success btn-sm' target='_blank' href='" + Helper.url('/admin/business-to-business-user-request-order/preview-invoice/' + row.id) + "'><i class='fa fa-file-word-o'></i></a>";
                        // btn_delete_item = '<button type="button" data-id="' + row.request_code + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
                        return view_link + ' ' + view_template;
                    }
                },
            ],
        });
    });
</script>

@endsection