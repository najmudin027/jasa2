@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-top: 10px;">
    <form id="sparepart-detail-form">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            Create Quotation
                        </div>
                        <div class="col-md-6 text-right">
                            {{-- <a class="btn btn-primary" href="{{ url('/admin/business-to-business-user-request-order/create') }}">Request Order</a> --}}
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="id_b2b_order" id="id_b2b_order" value="{{ !empty($data->get_b2b_detail) ? $data->get_b2b_detail->id : '' }}">
            <div class="card-body">
                <input type="hidden" id="sparepart_detail_order_id" value="{{ $data->id }}" name="sparepart_detail_order_id">
                <input type="hidden" name="type_part" id="type_part" value="my-inventory">
                <input type="hidden" name="id_transaction" id="id_transaction" value="{{ $data->id }}">

                <div class="col-md-12" style="color: black">
                    <center><strong style="color:black">Data Client</strong></center><br>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">Company : </label>
                            @if (!empty($data->get_outlet->business_to_business_transaction))
                                    <input type="text" class="form-control" readonly name="company" id="company" value="{{ !empty($data->get_outlet->business_to_business_transaction->company->name) ?  $data->get_outlet->business_to_business_transaction->company->name : '-' }}" style="width: 100%">
                            @else
                                    <input type="text" class="form-control" readonly name="company" id="company" value="-" style="width: 100%">
                            @endif    
                            
                        </div>
                        <div class="col-md-4">
                            <label for="">Phone : </label>
                            @if (!empty($data->get_outlet->business_to_business_transaction))
                                    <input type="text" class="form-control" readonly name="phone" id="phone" value="{{ !empty($data->get_outlet->business_to_business_transaction->company->phone) ? $data->get_outlet->business_to_business_transaction->company->phone : '-' }}">
                            @else
                                    <input type="text" class="form-control" readonly name="phone" id="phone" value="-">
                            @endif    
                        </div>
                        <div class="col-md-4">
                            <label for="">Address : </label>
                            @if (!empty($data->get_outlet->business_to_business_transaction))
                                    <input type="text" class="form-control" readonly name="address" id="address" value="{{ !empty($data->get_outlet->business_to_business_transaction->company->alamat) ? $data->get_outlet->business_to_business_transaction->company->alamat : '-' }}" style="width: 100%">
                            @else
                                    <input type="text" class="form-control" readonly name="address" id="address" value="-" style="width: 100%">
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="color: black" hidden>
                    <center><strong style="color:black">Term And Condition</strong></center><br>
                    <div class="row">
                        <div class="col-md-4 text-left" style="margin-bottom: 20px">
                            <label for="">Validity : </label>
                            <input type="text" class="form-control" name="validity" id="validity" value="{{$validity->value}}" readonly>
                        </div>
                        <div class="col-md-4 text-left" style="margin-bottom: 20px">
                            <label for="">Minimum Order : </label>
                            <input type="text" class="form-control" name="min_order" id="min_order" value="{{$minimum_order->value}}" readonly>
                        </div>
                        <div class="col-md-4 text-left" style="margin-bottom: 20px">
                            <label for="">Status Process : </label>
                            <input type="text" class="form-control" name="status_process" id="status_process" value="{{$status_proce->value}}" readonly>
                        </div>
                        <div class="col-md-6 text-left">
                            <label for="">Payment : </label><br>
                            <input type="text" class="form-control" name="payment" id="payment" value="{{$payment->value}}" readonly style="width: 100%">
                        </div>
                        <div class="col-md-6 text-left">
                            <label for="">Time Delivery : </label><br>
                            <input type="text" class="form-control" name="deliver" id="deliver" value="{{$time_of_delivery->value}}" readonly style="width: 100%">
                        </div>
                    </div>
                </div>
                <hr>

                <div class="col-md-12" style="color: black">
                    <center><strong style="color:black">Unit Information</strong></center><br>
                    <div class="row">
                        <div class="col-md-4 text-left" style="margin-bottom: 20px">
                            <label for="">Product</label>
                            @php
                                if(!empty($data->get_b2b_order_detail->get_type)){
                                    $name = $data->get_b2b_order_detail->get_type->name;
                                    $desc = $data->get_b2b_order_detail->get_type->desc;
                                }else{
                                    $name = '-';
                                    $desc = '-';
                                }
                            @endphp
                            <input type="text" name="product_name" id="product_name" value="{{ !empty($data->get_b2b_order_detail) ? $data->get_b2b_order_detail->merek . '  ' . $name . ' ' . $desc : '-' }}" placeholder="merek" class="btb-outlet-item-merek form-control" readonly>
                            {{-- <label for="">Product</label>
                            <select name="product_name" id="product_name" placeholder="merek" class="btb-outlet-item-merek form-control" required>
                                @foreach ($brand as $merk)
                                    @if (!empty($data->get_b2b_order_detail))
                                        <option value="{{$merk->name}}" {{ $data->get_b2b_order_detail->merek == $merk->name ? 'selected' : ''}}>{{$merk->name}}</option>
                                    @else
                                        <option value="{{$merk->name}}">{{$merk->name}}</option>
                                    @endif
                                    
                                @endforeach
                            </select> --}}
                        </div>
                        <div class="col-md-4" style="margin-bottom: 20px">
                            <label for="">Unit</label>
                            <input type="text" name="unit_ke" id="unit_ke" class="form-control" value="{{ !empty($data->get_b2b_order_detail) ? $data->get_b2b_order_detail->unit_ke : '-' }}" readonly>
                        </div>
                        <div class="col-md-4 text-left" style="margin-bottom: 20px">
                            <label for="">Type Service</label>
                            <input type="text" name="type_produk" id="type_produk" placeholder="Type" class="btb-outlet-item-type form-control" value="{{ !empty($data->service_type) ? $data->service_type  : '-' }}" readonly>
                            
                            {{-- <select name="type_produk" id="type_produk" placeholder="merek" class="btb-outlet-item-type form-control" required>
                                @foreach ($type as $tipe)
                                    <option value="{{$tipe->id}}">{{$tipe->name}} {{$tipe->desc}}</option>
                                @endforeach
                            </select> --}}
                        </div>

                        
                        <div class="col-md-6" style="margin-bottom: 20px"></div>
                        <div class="col-md-12 text-left" style="margin-bottom: 20px">
                            <label for="">Remark</label>
                            <textarea name="remark" id="remark" class="form-control" id="" cols="30" rows="3">{{ $data->remark }}</textarea>
                            {{-- <input type="text" name="remark" class="form-control"> --}}
                        </div>
                    </div>
                </div>

                
                <br>
            </div>
        </div><br>

        {{-- <div class="card">
            <div class="card-header header-border">
                Details Bundle
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <label for="exampleEmail">Astech Service Center</label>
                    <select name="asc_id" id="asc_id" class="form-control" required style="width:100%">
                    </select>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Code Material</th>
                            <th>Part Description</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Satuan</th>
                            <th>Subtotal</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="dynamic_details">
                        <tr>
                            <input type="hidden" value=0 id="count_details_data" />
                            <td width="20%">
                                <select name="part_data_stock_id[]" data-uniq="xxx" class="form-control part_data_stock_ids part_data_stock_id-xxx" style="width:100%">
                                </select>
                            </td>
                            <td width="30%">
                                <input name="part_description[]" type="text"
                                    placeholder="Description Sparepart"
                                    class="form-control part_descriptions part_description-xxx" data-uniq="xxx"
                                    style="width:100%" readonly>
                            </td>
                            <td>
                                <input name="qty[]" type="text" placeholder="Quantity"
                                    class="form-control qtys qty-xxx number_only" data-uniq="xxx" required>
                            </td>
                            <td>
                                <input name="selling_price[]" type="text" placeholder="Price"
                                    class="form-control selling_prices selling_price-xxx number_only" data-uniq="xxx" required>
                            </td>
                            <td>
                                <input name="satuan[]" type="text" placeholder="Satuan"
                                    class="form-control satuans satuan-xxx" data-uniq="xxx" required>
                            </td>
                            <td>
                                <input name="sub_total_details[]" type="text"
                                    class="form-control sub_total_detail sub_total_details-xxx number_only" data-uniq="xxx"
                                    readonly>
                            </td>
                            <td>
                                <div class="btn-actions-pane-left text-right">
                                    <button type="button" class="btn btn-success btn_add_details"><i class="fa fa-plus"></i></button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div><br> --}}

        <div class="card">
            <div class="card-header header-border">
                SPARE PART
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Code Material</th>
                            <th>Part Description</th>
                            <th>Qty</th>
                            <th>Stok</th>
                            <th>Price</th>
                            <th></th>
                            <th>hpp</th>
                            <th>Subtotal</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="dynamic_details">
                        <tr>
                            <input type="hidden" value=0 id="count_details_data" />
                            <td width="20%">
                                <select name="part_data_stock_id[]" data-uniq="xxx" class="form-control part_data_stock_ids part_data_stock_id-xxx" style="width:100%" required>
                                </select>
                            </td>
                            <td width="30%">
                                <input name="part_description[]" type="text"
                                    placeholder="Description Sparepart"
                                    class="form-control part_descriptions part_description-xxx" data-uniq="xxx"
                                    style="width:100%" readonly>
                            </td>
                            <td>
                                <input name="qty[]" type="text" placeholder="Quantity"
                                    class="form-control qtys qty-xxx number_only" data-uniq="xxx" required>
                            </td>
                            <td>
                                <input name="stok[]" type="text" placeholder="Stok"
                                    class="form-control stoks stok-xxx number_only" data-uniq="xxx" required readonly>
                            </td>
                            <td>
                                <input name="selling_price[]" type="text" placeholder="Price"
                                    class="form-control selling_prices selling_price-xxx number_only" data-uniq="xxx" required>
                            </td>
                            <td>
                                <input name="satuan[]" type="text" placeholder="Satuan"
                                    class="form-control satuans satuan-xxx" data-uniq="xxx" hidden>
                            </td>
                            <td>
                                <input name="hpp_average[]" type="text" placeholder="Hpp Price"
                                    class="form-control hpps hpp_average-xxx number_only" data-uniq="xxx" required readonly>
                            </td>
                            <td>
                                <input name="sub_total_details[]" type="text"
                                    class="form-control sub_total_detail sub_total_details-xxx number_only" data-uniq="xxx"
                                    disabled>
                            </td>
                            <td>
                                <div class="btn-actions-pane-left text-right">
                                    <button type="button" class="btn btn-success btn_add_details"><i class="fa fa-plus"></i></button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div><br>

        <div class="card">
            <div class="card-header header-border">
                LABOR
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Service Name</th>
                            <th>Service Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            {{-- <input type="hidden" value=0 id="count_details_data" /> --}}
                            <td width="20%">
                                <select name="service_name" data-uniq="xxx" class="form-control service_names service_name" style="width:100%">
                                </select>
                            </td>
                            <td width="30%">
                                <input name="service_price" type="text"
                                    placeholder="Service Price"
                                    class="form-control service_prices service_price" data-uniq="xxx"
                                    style="width:100%" readonly>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div><br>

        <div class="card">
            <div class="card-header header-border">
                Jasa
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama Jasa</th>
                            <th>Harga Jasa</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            {{-- <input type="hidden" value=0 id="count_details_data" /> --}}
                            <td width="20%">
                                <input type="text" name="nama_jasa" class="form-control nama_jasa" placeholder="Nama Jasa">
                            </td>
                            <td width="30%">
                                <input type="text" name="harga_jasa" class="form-control harga_jasa" placeholder="harga Jasa">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div><br>

        <div class="card">
            <button type="submit" class="btn btn-xl btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
        </div>
    </form>
</div>

<style>
    .fixed-sidebar .app-main .app-main__outer {
        width: 100%;
    }

    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
@endsection

@section('script')
@include('admin.business_to_business_order_request._jsCreateQuotation')
@include('admin.business_to_business_order_request._jsDetailCreateQuotation')

<script>
    Helper.onlyNumberInput('.harga_jasa');
    $('#sparepart-detail-form').submit(function(e){
        Helper.loadingStart();
        var data = new FormData($('#media-form')[0]);
        var bodyFormData = new FormData();

        $('#id_b2b_order').each(function(){ 
            bodyFormData.append('id_b2b_order', $(this).val() ); 
        });

        $('.nama_jasa').each(function(){ 
            bodyFormData.append('nama_jasa', $(this).val() ); 
        });

        $('.harga_jasa').each(function(){ 
            bodyFormData.append('harga_jasa', $(this).val() ); 
        });

        $('#unit_ke').each(function(){ 
            bodyFormData.append('unit_ke', $(this).val() ); 
        });

        $('.service_names').each(function(){ 
            bodyFormData.append('service_name', $(this).val() ); 
        });

        $('.service_prices').each(function(){ 
            bodyFormData.append('service_price', $(this).val() ); 
        });

        $('.part_data_stock_ids').each(function(){ 
            bodyFormData.append('part_data_stock_id[]', $(this).val() ); 
        });

        $('.part_descriptions').each(function(){ 
            bodyFormData.append('part_description[]', $(this).val() ); 
        });

        $('.qtys').each(function(){ 
            bodyFormData.append('qty[]', $(this).val() ); 
        });

        $('.selling_prices').each(function(){ 
            bodyFormData.append('selling_price[]', $(this).val() ); 
        });

        // $('.satuans').each(function(){ 
        //     bodyFormData.append('satuan[]', $(this).val() ); 
        // });

        $('.sub_total_detail').each(function(){ 
            bodyFormData.append('sub_total_details[]', $(this).val() ); 
        });

        $('#company').each(function(){ 
            bodyFormData.append('company', $(this).val()); 
        });

        $('#phone').each(function(){ 
            bodyFormData.append('phone', $(this).val()); 
        });

        $('#address').each(function(){ 
            bodyFormData.append('address', $(this).val()); 
        });

        $('#validity').each(function(){ 
            bodyFormData.append('validity', $(this).val()); 
        });

        $('#min_order').each(function(){ 
            bodyFormData.append('min_order', $(this).val()); 
        });

        $('#status_process').each(function(){ 
            bodyFormData.append('status_process', $(this).val()); 
        });

        $('#payment').each(function(){ 
            bodyFormData.append('payment', $(this).val()); 
        });

        $('#id_transaction').each(function(){ 
            bodyFormData.append('id_transaction', $(this).val()); 
        });

        $('#deliver').each(function(){ 
            bodyFormData.append('deliver', $(this).val()); 
        });

        $('#product_name').each(function(){ 
            bodyFormData.append('product_name', $(this).val()); 
        });

        $('#type_produk').each(function(){ 
            bodyFormData.append('type_produk', $(this).val()); 
        });

        $('#remark').each(function(){ 
            bodyFormData.append('remark', $(this).val()); 
        });

        $('.stoks').each(function(){ 
            bodyFormData.append('stok[]', $(this).val()); 
        });

        $('.hpps').each(function(){ 
            bodyFormData.append('hpp_average[]', $(this).val()); 
        });
        

        Axios.post('/request-order-b-2-b/save-quotation-order', bodyFormData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                
                // console.log(('.qtys').val());
                Helper.loadingStop();
                window.location.href = Helper.redirectUrl('/admin/business-to-business-user-request-order/detail-request-order/'+ $('#sparepart_detail_order_id').val());
                // // location.reload();
                Helper.successNotif('Success Updated');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>

<script>
    globalCRUD.select2('.service_name', '/jasa-excel/select2', function(item) {
        return {
            id: item.name,
            text:item.name,
            price:item.price,
        }
    });

    $('.service_name').on('select2:select', function (e) {
        console.log(e.params)
        $('.service_price').val(e.params.data.price);
    });
</script>

<script>
    $(function() {
        var uniq = $(this).attr('data-uniq');
        Helper.dateFormat('.date_format');
        Helper.currency('.currency');
        ClassApp.hitungTotal();
        ClassApp.select2CodeMaterialDetail('xxx');
    });
</script>
@endsection