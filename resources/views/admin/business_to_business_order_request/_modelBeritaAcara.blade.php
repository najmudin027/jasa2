<div class="modal fade" id="show_berita_acara" data-backdrop="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Berita Acara</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    
                </button>
            </div>
            <div class="modal-body berita_acara">
                <table class="table table-hover table-bordered berita_acara" style="width:100%">
                    <thead>
                        <th>Image</th>
                        <th>Action</th>
                    </thead>
                    <tbody id="append_berita_acara"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>

<script>
    // $(document).ready( function () {
        var dt = $('.berita_acara').DataTable();
    // });

    var $modal_berita_acara = $('#show_berita_acara').modal({
        show: false
    });

    var beritaAcaraObj = {
        // isi field input
        isiDataFormModalBerita: function(type, id) {
            $.ajax({
                url: Helper.apiUrl('/request-order-b-2-b/show-berita-acara/'+type+'/'+id),
                type: 'get',
                success: function(resp) {
                    console.log(resp.data)
                    $("#append_berita_acara").html('');
                    var content_berita = '';
                    _.each(resp.data, function(getDetails) {
                        content_berita += ` <tr>
                                                <td><a href="/storage/media-btb/${getDetails.image_berita_acara}" target="__blank">${getDetails.image_berita_acara}</a></td>
                                                <td><button class="btn btn-danger btn-sm" id="berita_acara_delete" data-id="${getDetails.id}"><i class="fa fa-trash"></i></button></td>
                                            </tr> `;
                    });
                    $("#append_berita_acara").append(content_berita);
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            })

        },
        // hadle ketika response sukses
        successHandle: function(resp) {
            // send notif
            Helper.successNotif(resp.msg);
            $modal_berita_acara.modal('hide');
        },
    }


    $(document)
        .on('click', '#show-berita-acara', function() {
            type = $(this).attr('data-type');
            id = $(this).attr('data-ids');
            console.log(beritaAcaraObj.isiDataFormModalBerita(type, id));
            $modal_berita_acara.modal('show');
        })

    // ganti status
    $(document)
        .on('click', '#berita_acara_delete', function() {
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.post('/request-order-b-2-b/delete-berita-acara/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('Data Berhasil Dihapus');
                        // reload table
                        location.reload()
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })
</script>