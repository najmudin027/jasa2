@extends('admin.home')
@section('content')
@if(Auth::user()->status == 6 || Auth::user()->status == 7 || Auth::user()->status == 8)
    @include('users.teknisiInfoUpdate')
@else
    @if (Auth::user()->status == 6 || Auth::user()->status == 7 || Auth::user()->status == 8)
        @include('users.teknisiInfo')
    @else
        @if($garansi_teknisi != null)
            <div class="col-md-12" style="margin-bottom: 20px;">
                <div class="card">
                    <div class="card-header header-bg" style="background-color: #F44336;">
                        <a href="{{ url('/teknisi/list-garansi-job') }}" style="color: #fff;">
                            <i class="fa fa-envelope" style="font-size: 17px;"></i> View Warranty Claim !
                        </a>
                        <div class="btn-actions-pane-right text-capitalize">
                            <a href="{{ url('/teknisi/list-garansi-job') }}" class="btn btn-success btn-sm">
                                <i class="fa fa-arrow-right"></i> Warranty Claim
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="col-md-4" style="margin-bottom: 20px;">
            <div class="pt-0 pb-0 card">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-outer">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Earnings This Month ({{ date('M') }} {{ date('Y') }})</div>
                                        @php
                                            if(!empty($totalPart)){
                                                $allTotal = $totalPart + $getTotalService;
                                            }else{
                                                $allTotal = $getTotalService;
                                            }
                                        @endphp
                                        <div class="widget-subheading"><strong style="color: #1600a2; font-size:20px"> Rp.{{ number_format($allTotal) }}</strong></div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="widget-numbers text-success"></div><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-4" style="margin-bottom: 20px;">
            <div class="pt-0 pb-0 card">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-outer">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Order Complete Last Mounth ({{ date("F",strtotime("-1 month")) }} {{ date('Y') }})</div>
                                        @php
                                            if(!empty($totalPart)){
                                                $allTotal = $totalPart + $completeOrder;
                                            }else{
                                                $allTotal = $completeOrder;
                                            }
                                        @endphp
                                        <div class="widget-subheading"><strong style="color: #009297; font-size:20px">Rp.{{ number_format($completeOrder) }}</strong></div>
                                    </div>
                                    <div class="widget-content-right">
                                        <div class="widget-numbers text-danger"></div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        @php
            $sumTotalBalance = 0;
        @endphp

        @if(!empty($balance))
            @foreach ($balance as $pendingBalance)
                @php
                    $total_harga = $pendingBalance->total;
                    $sumTotalBalance += $total_harga;
                @endphp

            @endforeach
        @endif

        <div class="col-md-4" style="margin-bottom: 20px;">
            <div class="pt-0 pb-0 card">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-outer">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">Pending Balance</div>
                                        <div class="widget-subheading"><a href="{{ url('/teknisi/balance/detail') }}">View History</a> :<strong style="color: #970000; font-size:20px"> Rp. {{ number_format($sumTotalBalance) }}</strong></div>
                                    </div>
                                    <div class="widget-content-right">

                                        <div class="widget-numbers text-success"> </div>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
            <div class="divider mt-0" style="margin-bottom: 30px;"></div>
                @if(Auth::user()->status == 7 )
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header-tab card-header">
                                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                    Technician Register
                                </div>
                            </div>
                            <div class="card-body">
                                <h3 align="center">Your account is being verified by admin</h3>
                                <p align="center">please attention to the login notification to your email account</p>
                            </div>
                        </div>
                    </div>
                @else
                <div class="card">
                    <div class="header-border card-header">
                        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                            JOB REQUEST
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-pane show active" id="tab-1" role="tabpanel">
                            <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Orders Code</th>
                                        <th>Service</th>
                                        <th>Customer Name</th>
                                        <th>Create Date</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="d-block card-footer">
                        <!-- <button class="btn-wide btn btn-success"><i aria-hidden="true" class="fa fa-arrow-circle-right" title="go"></i> VIEW MORE</button> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" data-backdrop="false" id="modal-job-request" role="dialog" tabindex="-1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">
                            Detail Job
                        </h4>
                        <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                            <span aria-hidden="true">
                                ×
                            </span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button class="btn waves-effect" data-dismiss="modal" type="button">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" data-backdrop="false" id="modal-job-done" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Jobs Done</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="get_id" id="done">

                        <label for="">Symptom Code</label>
                        <select class="js-example-basic-single" id="ms_symptom_code" name="ms_symptom_code" style="width:100%"></select>
                        <strong><span id="error-ms_symptom_code" style="color:red"></span></strong><br>
                        <label for="">Repair Code</label>
                        <select class="js-example-basic-single" id="ms_repair_code" name="ms_repair_code" style="width:100%"></select>
                        <strong><span id="error-ms_repair_code" style="color:red"></span></strong><br>
                        <label for="">Desc</label>
                        <textarea class="form-control" id="repair_desc" name="repair_desc" rows="3"></textarea>
                        <strong><span id="error-repair_desc" style="color:red"></span></strong><br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" id="jobs_done" data-id="" class="btn btn-primary">Success Jobs</button>
                        <!-- <button type="submit"  class="mb-2 mr-2 btn btn-sm btn-primary delete-selected" style="float:right">Job Done</button> -->
                    </div>
                </div>
            </div>
        </div>

        @endif
    @endif
@endif
@endsection

@section('script')
@include('admin.template._firebase_config')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js">
</script>
<script>

    $('.detail-job').click(function(e){
        $('#modal-job-request').modal('show');
    });

    var table = $('#table-list-orders').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            select: true,
            dom: 'Bflrtip',
            ordering:'true',
            order: [3, 'desc'],
            responsive: true,
            language: {
                search: '',
                searchPlaceholder: "Search..."
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/teknisi/request-job/datatables/all'),
                type: 'get',
            },
            columns: [
                {
                    data: "code",
                    name: "code",
                    render: function(data, type, full) {
                        return "<a href='/teknisi/request-job-accept/"+ full.id + "'># "+full.code+"</a>";
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        return full.symptom.name + "<br> <small>" + full.symptom.name  + "&nbsp;" + full.product_group.name + "</small>";
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        return full.customer.name;
                    }
                },
                {
                    data: "created_at",
                    name: "created_at",
                    render: function(data, type, full) {
                        return full.created_at == null ? '-' : moment(full.created_at).format("DD MMMM YYYY hh:mm");
                    }
                },
                {
                    data: "grand_total",
                    name: "grand_total",
                    render: $.fn.dataTable.render.number( ',', '.,', 0, 'Rp. ' )
                },
                {
                    data: "order_status.name",
                    name: "order_status.name",
                    render: function(data, type, full) {
                        var content = '';
                        if (full.orders_statuses_id === 2) {
                            content = '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 3) {
                            content = '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 4) {
                            content = '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 5) {
                            content = '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 6) {
                            content = '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 7) {
                            content = '<span class="badge badge-focus">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 8) {
                            content = '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 9) {
                            content = '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 10) {
                            content = '<span class="badge badge-success">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 11) {
                            content = '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        }

                        if (full.countdown_autocancel != null) {
                            $(document).ready(function() {
                                $('#clock' + full.id + '').countdown(full.countdown_autocancel, function(event) {
                                    $(this).html(event.strftime('%D Days (%H Hour : %M Min)'));
                                });
                            });

                            if (!$.inArray(full.orders_statuses_id, [2,3])) {
                                content += "<br><div class='badge badge-danger' data-toggle='tooltip' data-placement='top' title='this countdown for auto cancel order'><span id='clock" + full.id + "'></span></div>";
                            }

                        }

                        if (full.countdown_autocompleted != null) {
                            $(document).ready(function() {
                                $('#clock' + full.id + '').countdown(full.countdown_autocompleted, function(event) {
                                    $(this).html(event.strftime('%D Days (%H Hour : %M Min)'));
                                });
                            });

                            if (!$.inArray(full.orders_statuses_id, [7])) {
                                content += "<br><div class='badge badge-success' data-toggle='tooltip' data-placement='top' title='this countdown for auto completed order'><span id='clock" + full.id + "'></span></div>";
                            }
                        }

                        return content;
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        detail = "<a href='/teknisi/request-job-accept/" + full.id + "' class='badge badge-primary btn-sm'  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>";
                        acceptJobs = '<a href="" type="button" class="accept-modal badge badge-sm badge-primary" data-accept-id='+full.id+' data-toggle="tooltip" data-html="true" title="Accept Order"><i class="fa fa-check"></i></a>'
                        cancel = '<a href="" type="button" class="badge badge-sm badge-danger delete-selected" data-cancel-id='+full.id+' id="reject_job" data-toggle="tooltip" data-html="true" title="Reject Order"><i class="fa fa-times"></i></a>'
                        complaint = "<a href='/teknisi/transacsion_list/complaint/detail/" + full.id + "' class='badge badge-success btn-sm '  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-comment'></i></a>";
                        jobDone = '<a href="" type="button" class="badge badge-primary btn-sm click_btn_job_done" data-id-order='+ full.id +' data-toggle="modal" data-target="#modal-job-done" ><i class="fa fa-check" style="color:white"></i></a>'
                        onWorking = '<a href="" type="submit" id="on_working" data-id-processing='+full.id+' class="mb-2 mr-2 badge badge-sm badge-success delete-selected" data-toggle="tooltip" data-html="true" title="On Working"><i class="fa fa-check" style="color:white"></i></a>'
                        if (full.order_status.id == 3 || full.order_status.id == 5 || full.order_status.id == 7 || full.order_status.id == 8 || full.order_status.id == 10) {
                            return detail;
                        } else if (full.order_status.id == 11) {
                            return complaint;
                        }else if (full.order_status.id == 4) {
                            return jobDone + ' ' + detail;
                        } else if (full.order_status.id == 2) {
                            return detail;
                        } else if(full.order_status.id == 9){
                            return onWorking + '' + detail;
                        } else {
                            return "-"
                        }

                    }
                }
            ]
        });

        $(document).on('click', '#on_working', function(e) {
            id = $(this).attr('data-id-processing');
            Helper.confirm(function(){
                Helper.loadingStart();
                $.ajax({
                    url:Helper.apiUrl('/teknisi/on-working/' + id ),
                    type: 'post',

                    success: function(res) {
                        Helper.successNotif('Success');
                        Helper.loadingStop();
                        Helper.redirectTo('/teknisi/dashboard');
                    },
                    error: function(res) {
                        console.log(res);
                        Helper.loadingStop();
                    }
                })
            })

            e.preventDefault()
        })

        $(document).on('click', '.click_btn_job_done', function(){
            var id_job_done = $(this).attr('data-id-order');
            // alert(id_job_done)
            $('#done').val(id_job_done)
        })

        // $(document).on('click', '#reject_job', function(){
        //     Helper.confirm( ()=> {
        //         $.ajax({
        //             url: Helper.apiUrl('/teknisi/reject-job/' + $(this).attr('data-cancel-id')),
        //             type: 'post',
        //             data: {
        //                 order_id: $('#order_id').val()
        //             },
        //             success: function(response){
        //                 location.reload()
        //                 Helper.successNotif('Cancel Jobs Has Been Success');
        //                 window.location.href = Helper.redirectUrl('/teknisi/list-request-job');
        //             }
        //         });
        //     });
        // })
</script>
<script>
     $(document).on('click', '#jobs_done', function(e) {
        id = $('#done').val();
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/teknisi/jobs-done/' + id ),
                type: 'post',
                data: {
                    ms_symptom_code: $('#ms_symptom_code').val(),
                    ms_repair_code: $('#ms_repair_code').val(),
                    repair_desc: $('#repair_desc').val(),
                },
                success: function(res) {
                    Helper.successNotif('Success');
                    Helper.loadingStop();
                    Helper.redirectTo('/teknisi/list-request-job');
                },
                error: function(xhr, status, error) {
                    Helper.loadingStop();
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                        })
                    }
                }
            })
        },
        {
            title: "Are You Sure",
            message: "Are you really sure this work has been completed ?",
        })

        e.preventDefault()
    })

    globalCRUD
        .select2Static("#ms_symptom_code", '/symptom-code/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })
        .select2Static("#ms_repair_code", '/repair-code/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })
</script>
@endsection
