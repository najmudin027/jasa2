@extends('admin.home')
@section('content')
<div class="col-md-4">
  <div class="card">
    <div class="header-border card-header">
      <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
        {{ $technician->user->name }}
      </div>
    </div>
    <div class="card-body">
      <div class="profile-sidebar">
        <!-- SIDEBAR USERPIC -->
        <div class="profile-userpic text-center">
          <div class="avatar-upload">
            <div class="avatar-preview">
              <div id="imagePreview" style="background-image: url({{ asset($technician->user->avatar) }});"></div>
            </div>
          </div>
        </div>
        <!-- END SIDEBAR USERPIC -->
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
          <div class="profile-usertitle-name">
            {{ $technician->user->full_name }}
            <br />
            <button type="button" class="btn btn-sm btn-primary chat-teknisi"><i class="fa fa-envelope"></i> Chat</button>
          </div>
        </div>
        <!-- END SIDEBAR USER TITLE -->
      </div>
      <input type="hidden" name="teknisi_id" value="{{ $technician->id }}">
    </div>
  </div>
</div>


<div class="col-md-8">
  <div class="card">
    <div class="header-border card-header">
      TECHNICIAN INFORMATION
      <div class="btn-actions-pane-right">
        <div class="btn-group-sm nav btn-group" role="group">
          <a class="btn-pill pl-3 active btn btn-focus" data-toggle="tab" href="#tab-eg3-0">
            INFORMATION DETAIL
          </a>
          <a class="btn btn-focus" data-toggle="tab" href="#tab-eg3-1">
            JOB INFO
          </a>
        </div>
      </div>
    </div>
    <div class="card-body">
      <div class="tab-content">
        <div class="tab-pane active" id="tab-eg3-0" role="tabpanel">
          <table class="table table-user-information">
            <tbody>
              <tr>
                <th>
                  First Name
                </th>
                <td>
                  {{ $technician->user->info == null ? '' : $technician->user->info->first_name }}
                </td>
              </tr>
              <tr>
                <th>
                  Last Name
                </th>
                <td>
                  {{ $technician->user->info == null ? '' : $technician->user->info->last_name }}
                </td>
              </tr>
              <tr>
                <th>
                  Gender
                </th>
                <td>
                  {{ $technician->user->info == null ? '' : $technician->user->info->sex }}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="tab-pane" id="tab-eg3-1" role="tabpanel">
          <table class="table table-user-information">
            <tbody>
              <tr>
                <th>
                  Job Title Category
                </th>
                <td>
                  {{ $technician->job_title != null ? $technician->job_title->job_title_category->name : ''}}
                </td>
              </tr>
              <tr>
                <th>
                  Job Title Description
                </th>
                <td>
                  {{ $technician->job_title != null ? $technician->job_title->description : ''}}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="col-md-12" style="margin-top: 20px">
  <div class="card">
    <div class="card-header header-border">REVIEW</div>
    <div class="card-body">
      <div class="row">
        <div class="col-sm-3">
          <div class="rating-block">
            <h4>Average user rating</h4>
            <h2 class="bold padding-bottom-7">{{ $technician->avg_rating }} <small>/ 5</small></h2>
            {!!
            $technician->getRatingStarUi(
            '<button type="button" class="btn btn-warning btn-sm" style="color: #fff;" aria-label="Left Align">
              <span class="fa fa-star" aria-hidden="true"></span>
            </button>',
            '<button type="button" class="btn btn-default btn-dark btn-sm" aria-label="Left Align">
              <span class="fa fa-star" aria-hidden="true"></span>
            </button>'
            )
            !!}
          </div>
        </div>

        <div class="col-sm-3">
          <h4>Rating breakdown</h4>
          @foreach($rr as $rating)
          <div class="pull-left">
            <div class="pull-left" style="width:35px; line-height:1;">
              <div style="height:9px; margin:5px 0;">{{ $rating->value }} <span class="fa fa-star"></span></div>
            </div>
            <div class="pull-left" style="width:180px;">
              <div class="progress" style="height:9px; margin:8px 0;">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: {{ $rating->total * 100 / $technician->total_review }}%">
                  <span class="sr-only">80% Complete (danger)</span>
                </div>
              </div>
            </div>
            <div class="pull-right" style="margin-left:10px;">{{ $rating->total }}</div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>


<div class="col-md-12" style="margin-top: 20px">
  <div class="card">
    <div class="card-header header-border">
      REVIEW
    </div>
    <div class="card-body">
      <ul class="rm-list-borders rm-list-borders-scroll list-group list-group-flush" id="comment-area">
        @foreach ($reviews as $review)
        <li class="list-group-item">
          <div class="widget-content p-0">
            <div class="widget-content-wrapper">
              <div class="widget-content-left">
                <div class="widget-heading">
                  <img alt="" class="rounded-circle" src="https://demo.dashboardpack.com/architectui-html-free/assets/images/avatars/9.jpg" />
                  {{ $review->user->name }}
                </div>
                <div class="widget-heading">
                  {!! $review->order->rating_and_review->rating->getRatingStarUi() !!}
                </div>
                <div class="widget-subheading">
                  {{ $review->description }}
                </div>
              </div>
            </div>
          </div>
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>
@endsection

@section('script')
@endsection


















<style>
  .star {
    width: 50px;
    height: 50px;
    -webkit-clip-path: polygon(50% 0%, 61% 35%, 98% 35%, 68% 57%, 79% 91%, 50% 70%, 21% 91%, 32% 57%, 2% 35%, 39% 35%);
    clip-path: polygon(50% 0%, 61% 35%, 98% 35%, 68% 57%, 79% 91%, 50% 70%, 21% 91%, 32% 57%, 2% 35%, 39% 35%);
    background: white;
  }

  .star-border {
    filter: drop-shadow(0px 0px 4px black);
    display: inline-block;
  }

  .star-border:hover {
    cursor: pointer;
  }

  .rounded-circle {
    width: 60px;
    height: auto;
  }

  .card .widget-content {
    border: 1px solid #c1c3c5;
  }

  .widget-content-wrapper {
    margin: 10px;
  }

  .rating-block {
    background-color: #FAFAFA;
    border: 1px solid #EFEFEF;
    padding: 15px 15px 20px 15px;
    border-radius: 3px;
  }
</style>
<style>
  /* Profile container */
  .profile {
    margin: 20px 0;
  }

  /* Profile sidebar */
  .profile-sidebar {
    background: #fff;
  }

  .profile-userpic img {
    float: none;
    margin: 0 auto;
    width: 50%;
    -webkit-border-radius: 50% !important;
    -moz-border-radius: 50% !important;
    border-radius: 50% !important;
  }

  .profile-usertitle {
    text-align: center;
    margin-top: 20px;
  }

  .profile-usertitle-name {
    color: #5a7391;
    font-size: 16px;
    font-weight: 600;
    margin-bottom: 7px;
  }

  .profile-usertitle-job {
    text-transform: uppercase;
    color: #555;
    font-size: 12px;
    font-weight: 600;
    margin-bottom: 15px;
  }

  .profile-userbuttons {
    text-align: center;
    margin-top: 10px;
  }

  .profile-userbuttons .btn {
    text-transform: uppercase;
    font-size: 11px;
    font-weight: 600;
    padding: 6px 15px;
    margin-right: 5px;
  }

  .profile-userbuttons .btn:last-child {
    margin-right: 0px;
  }

  .profile-usermenu {
    margin-top: 30px;
  }

  /* Profile Content */
  .profile-content {
    padding: 20px;
    background: #fff;
    min-height: 460px;
  }

  .avatar-upload {
    position: relative;
    max-width: 205px;
    margin: 0px auto;
  }

  .avatar-upload .avatar-edit {
    position: absolute;
    right: 12px;
    z-index: 1;
    top: 10px;
  }

  .avatar-upload .avatar-edit input {
    display: none;
  }

  .avatar-upload .avatar-edit input+label {
    display: inline-block;
    width: 34px;
    height: 34px;
    margin-bottom: 0;
    border-radius: 100%;
    background: #FFFFFF;
    border: 1px solid transparent;
    box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
    cursor: pointer;
    font-weight: normal;
    transition: all 0.2s ease-in-out;
  }

  .avatar-upload .avatar-edit input+label:hover {
    background: #f1f1f1;
    border-color: #d6d6d6;
  }

  .avatar-upload .avatar-edit input+label:after {
    content: "\f040";
    font-family: 'FontAwesome';
    color: #757575;
    position: absolute;
    top: 10px;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto;
  }

  .avatar-upload .avatar-preview {
    width: 192px;
    height: 192px;
    position: relative;
    border-radius: 100%;
    border: 6px solid #F8F8F8;
    box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
  }

  .avatar-upload .avatar-preview>div {
    width: 100%;
    height: 100%;
    border-radius: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
  }
</style>