<form id="form-job_experience">
    <div class="modal fade" data-backdrop="false" id="modal-job_experience" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="height: 500px;">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Add Job Title Category
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body" style="max-height: calc(100% - 120px);overflow-y: scroll;">
                    <input id="id" name="id" type="hidden" />
                    <label>Job Title Category</label>
                    <div class="form-group">
                        <input 
                            class="form-control autocomplete-job-category" 
                            name="job_title_category_name" 
                            placeholder="category" 
                            type="text" 
                            value=""
                            required />
                    </div>
                    <label>Job Title Description</label>
                    <div class="form-group">
                        <input 
                            class="form-control" 
                            name="job_title_description_ex" 
                            placeholder="description" 
                            type="text" 
                            required />
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Company Name</label>
                            <div class="form-group">
                                <input 
                                    class="form-control" 
                                    id="company_name" 
                                    name="company_name" 
                                    placeholder="company name" 
                                    type="text" 
                                    required />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Position</label>
                            <div class="form-group">
                                <input 
                                    class="form-control" 
                                    id="position" 
                                    name="position" 
                                    placeholder="position" 
                                    type="text" 
                                    required />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Period Start</label>
                            <div class="form-group">
                                <input 
                                    class="form-control" 
                                    id="datetimepicker1" 
                                    name="period_start" 
                                    placeholder="period start" 
                                    type="text"
                                    required />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Period End</label>
                            <div class="form-group">
                                <input 
                                    class="form-control" 
                                    id="datetimepicker2" 
                                    name="period_end" 
                                    placeholder="period end" 
                                    type="text"
                                    required />
                            </div>
                        </div>
                    </div>
                    <label>type</label>
                    <div class="form-group">
                        <select class="form-control" id="type" name="type" required="" style="width:100%">
                            <option selected="" value="0">
                                Pengalaman Internal
                            </option>
                            <option value="1">
                                Pengalaman External
                            </option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        Save
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>