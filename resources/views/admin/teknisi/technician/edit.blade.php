@extends('admin.home')
@section('content')
<input type="hidden" id="hidden-user-id" value="{{ $technician->user->id }}">
<div class="col-md-4">
    <div class="card">
        <div class="card-header-tab card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic text-center">
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                            <label for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview" style="background-image: url({{ $technician->user->avatar }});"></div>
                        </div>
                    </div>
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{ $technician->user->name }}
                    </div>
                    <div class="profile-usertitle-job">
                        @if($technician->getAvgRating() > 0)
                        {!! $technician->getRatingStarUi() !!}
                        @endif
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <div class="profile-userbuttons">
                    <a href="{{ url('/admin/user/detail/'. $technician->user->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-info"></i> USER INFO</a>
                </div>
                <!-- END SIDEBAR BUTTONS -->
            </div>
            <input type="hidden" name="teknisi_id" value="{{ $technician->id }}">
        </div>
    </div>
</div>
<div class="col-md-8">
    <div class="card">
        <div class="card-header-tab card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table class="table table-user-information">
                <tbody>
                    <tr>
                        <th>
                            First Name
                        </th>
                        <td>
                            {{ $technician->user->info == null ? '' : $technician->user->info->first_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Last Name
                        </th>
                        <td>
                            {{ $technician->user->info == null ? '' : $technician->user->info->last_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Date Of Birth
                        </th>
                        <td>
                            {{ $technician->user->info == null ? '' : $technician->user->info->place_of_birth }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Date Of Birth
                        </th>
                        <td>
                            @if($technician->user->info != null)
                            @if($technician->user->info->date_of_birth != null)
                            {{ $technician->user->info->date_of_birth->format('d F Y') }}
                            @endif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Religion
                        </th>
                        <td>
                            @if ($technician->user->info != null)
                            @if ($technician->user->info->religion != null)
                            {{ $technician->user->info->religion->name }}
                            @endif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Marital
                        </th>
                        <td>
                            @if ($technician->user->info != null)
                            @if ($technician->user->info->marital != null)
                            {{ $technician->user->info->marital->status }}
                            @endif
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <a href="{{ url('/admin/user/info/'. $technician->user->id) }}" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Edit Info</a>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 20px;">
    <div class="card header-border">
        <div class="card-header">
            TECHNICIAN INFORMATION
        </div>
        <div class="card-body">
            <form id="">
                <input type="hidden" name="id" value="{{ $technician->id }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Identity Card Number</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" value="{{ ($technician->user->teknisidetail == null) ? '' : $technician->user->teknisidetail->no_identity }}" readonly>
                                </div>
                            </div>
                        </div>
                        <label>Identity Card</label>
                        @if (isset($technician->user->teknisidetail->attachment))
                        <div class="form-group">
                            <a href="{{ asset('admin/storage/attachment/' . $technician->user->teknisidetail->attachment) }}" alt="your_image" target="_blank">
                                <span class="badge badge-success badge-pill"><strong>Open Attachment</strong></span>
                            </a>
                        </div>

                        @else
                        <div class="form-group">
                            <span class="badge badge-warning badge-pill"><strong>This technician has not uploaded a photo of the identity card</strong></span>
                        </div>
                        @endif
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer">
            <a href="{{ route('technician.edit_info.web', $technician->user->id) }}" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> EDIT TEKNISI INFO</a>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 20px;">
    <div class="card">
        <div class="card-header">
            TECHNICIAN JOB INFORMATION
            <div class="btn-actions-pane-right">
                <div class="btn-group-sm nav btn-group" role="group">
                    <a class="btn-pill pl-3 active btn btn-focus" data-toggle="tab" href="#tab-eg3-0">
                        JOB INFO
                    </a>
                    <a class="btn btn-focus" data-toggle="tab" href="#tab-eg3-1">
                        JOB EXPERIENCE
                    </a>
                    <a class="btn btn-focus" data-toggle="tab" href="#tab-eg3-2">
                        EDUCATION
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 20px;">
    <div class="card header-border">
        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane active" id="tab-eg3-0" role="tabpanel">
                    <div class="row">
                        <div class="col-md-12 card" style="margin: 20px;">
                            <form id="teknisi-rek-form">
                                <input type="hidden" name="id" value="{{ $technician->id }}" />
                                <label>Bank Name</label>
                                <div class="form-group">
                                    <input class="form-control" id="rekening_bank_name" name="rekening_bank_name" placeholder="bank name" type="text" value="{{ $technician->rekening_bank_name }}" required />
                                </div>

                                <label>Rekening Number</label>
                                <div class="form-group">
                                    <input class="form-control" id="rekening_number" name="rekening_number" placeholder="Rekening number" type="text" value="{{ $technician->rekening_number }}" required />
                                </div>

                                <label>Bank Account Under the Name</label>
                                <div class="form-group">
                                    <input class="form-control" id="rekening_name" name="rekening_name" placeholder="Account name" type="text" value="{{ $technician->rekening_name }}" required />
                                </div>

                                <button class="btn btn-white bg-drak btn-sm waves-effect" type="submit"><i class="fa fa-pencil"></i> Save Rekening Info</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-eg3-1" role="tabpanel">
                    <button class="btn btn-primary btn-sm add-job_experience" type="button">
                        Add Job Expperience
                    </button>
                    <hr>
                    @include('admin.teknisi.technician._job_experience_table_html')
                </div>
                <div class="tab-pane" id="tab-eg3-2" role="tabpanel">
                    <button class="btn btn-primary btn-sm add-curriculum" type="button">
                        Add Curriculum
                    </button>
                    <hr>
                    @include('admin.teknisi.technician._curriculum_table_html')
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 20px;">
    <div class="card">
        <div class="card-header header-bg">
            TECHNICIAN PRICE SERVICE
            <div class="btn-actions-pane-right text-capitalize">

            </div>
        </div>
        <div class="card-body">
            @include('admin.teknisi.technician._price_service_table_html')
        </div>
        <div class="card-footer">
            <button class="btn btn-sm btn-primary add-price_service" type="button">
                <i class="fa fa-plus"></i> ADD PRICE SERVICE
            </button>
        </div>
    </div>
</div>

@include('admin.teknisi.technician._price_service_modal_html')
@include('admin.teknisi.technician._job_experience_modal_html')
@include('admin.teknisi.technician._curriculum_modal_html')
@endsection

@section('script')
@include('admin.teknisi.technician._job_experience_table_script')
@include('admin.teknisi.technician._curriculum_table_script')
@include('admin.teknisi.technician._price_service_table_script')
@include('admin.teknisi.technician._price_service_commission_script')
{{-- jquery autocomplate --}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('/datePicker/jquery.datetimepicker.css') }}">
<script src="{{ asset('/datePicker/jquery.datetimepicker.full.min.js') }}"></script>
<script>
    $(document).ready(function() {
        jQuery('#datetimepicker1').datetimepicker({
            timepicker: false,
            format: 'Y-m-d',
            mask: true
        });
        jQuery('#datetimepicker2').datetimepicker({
            timepicker: false,
            format: 'Y-m-d',
            mask: true
        });
    });
</script>
<script>
    // autocomplate
    $(".autocomplete-job-category").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: Helper.apiUrl('/job_title_category/select2'),
                // dataType: "jsonp",
                data: {
                    q: request.term
                },
                success: function(resp) {
                    response(
                        _.map(resp.data, function(row) {
                            return row.name;
                        })
                    );
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            console.log('event', event)
            console.log('ui', ui)
        },
    });

    $(document)
        .ready(function() {
            // load select2
            globalCRUD
                .select2Static(".job-title-select", '/job_title/select2', function(item) {
                    return {
                        id: item.id,
                        text: item.description
                    }
                })
                .select2(".job-category-select", '/job_title_category/select2')
                .select2(".category-select", '/admin/category/select2')
                .select2(".user-select", '/user/select2');

            globalCRUD
                .select2(".curriculum-select", '/curriculum/select2', function(item) {
                    return {
                        id: item.id,
                        text: item.curriculum_name
                    }
                });

        })

    $('#teknisi-job-form').submit(function(e) {
        data = Helper.serializeForm($(this));
        Helper.loadingStart();
        Axios.put('/technician/' + data.id + '/job_info/', data)
            .then(function(response) {
                // send notif
                Helper.successNotif(response.data.msg).redirectTo();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
        e.preventDefault()
    })

    $('#teknisi-rek-form').submit(function(e) {
        data = Helper.serializeForm($(this));
        Helper.loadingStart();
        Axios.put('/technician/' + data.id + '/rek_info/', data)
            .then(function(response) {
                // send notif
                Helper.successNotif(response.data.msg).redirectTo();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
        e.preventDefault()
    })

    // table
    var table = JobExperience.table({
        selector: '#table-job_experience',
        url: '/technician/' + $('input[name="teknisi_id"]').val() + '/experience_datatables'
    });

    // add job experience
    $(document)
        .on('click', '.add-job_experience', function() {
            $('#modal-job_experience').modal('show');
        })

    // add job experience
    $(document)
        .on('click', '.delete-experience', function() {
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.delete('/job_experience/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('ok');
                        // reload table
                        table.ajax.reload();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })

    // save job experience
    $('#form-job_experience')
        .submit(function(e) {
            data = Helper.serializeForm($(this));
            data.id = $('input[name="teknisi_id"]').val();

            Helper.loadingStart();
            Axios.post('/technician/' + data.id + '/store_experience', data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg);
                    // reload table
                    table.ajax.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });

            $('#modal-job_experience').modal('hide');
            e.preventDefault(); // table
            var table = JobExperience.table({
                selector: '#table-job_experience',
                url: '/technician/' + $('input[name="teknisi_id"]').val() + '/experience_datatables'
            });

            // add job experience
            $(document)
                .on('click', '.add-job_experience', function() {
                    $('#modal-job_experience').modal('show');
                })

            // add job experience
            $(document)
                .on('click', '.delete-experience', function() {
                    id = $(this).attr('data-id');
                    Helper.confirm(function() {
                        Helper.loadingStart();
                        Axios.delete('/job_experience/' + id)
                            .then(function(response) {
                                // send notif
                                Helper.successNotif('ok');
                                // reload table
                                table.ajax.reload();
                            })
                            .catch(function(error) {
                                console.log(error)
                                Helper.handleErrorResponse(error)
                            });
                    })
                })

            // save job experience
            $('#form-job_experience')
                .submit(function(e) {
                    data = Helper.serializeForm($(this));
                    data.id = $('input[name="teknisi_id"]').val();

                    Helper.loadingStart();
                    Axios.post('/technician/' + data.id + '/store_experience', data)
                        .then(function(response) {
                            // send notif
                            Helper.successNotif(response.data.msg);
                            // reload table
                            table.ajax.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });

                    $('#modal-job_experience').modal('hide');
                    e.preventDefault();
                })
        })

    // table curriculum
    var table = Curriculum.table({
        selector: '#table-curriculum',
        url: '/technician/' + $('input[name="teknisi_id"]').val() + '/curriculum_datatables'
    });

    // add curriculum
    $(document)
        .on('click', '.add-curriculum', function() {
            $('.curriculum-id').val('');
            $('.curriculum-select').val('');
            $('#modal-curriculum').modal('show');
        })

    // delete curriculum
    $(document)
        .on('click', '.delete-curriculum', function() {
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.delete('/technician/' + id + '/destroy_curriculum')
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('ok');
                        // reload table
                        table.ajax.reload();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })

    // save curriculum
    $('#form-curriculum')
        .submit(function(e) {
            data = Helper.serializeForm($(this));
            data.id = $('input[name="teknisi_id"]').val();

            Helper.loadingStart();
            Axios.post('/technician/' + data.id + '/store_curriculum', data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg);
                    // reload table
                    table.ajax.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });

            $('#modal-curriculum').modal('hide');
            e.preventDefault();
        })

    // table table_price_service
    var table_price_service = PriceService.table(
        '/technician/' + $('input[name="teknisi_id"]').val() + '/price_list_datatables'
    );

    $('.symptom-select-control').hide();
    $('.service_type-select-control').hide();

    // add price
    $(document)
        .on('click', '.add-price_service', function() {
            $('.product_group-select-control').hide();
            $('.symptom-select-control').hide();
            $('.service_type-select-control').hide();
            $('.category-select').val('').change();
            $('.symptom-select').val('').change();
            $('.service_type-select').val('').change();
            $('.price-id').val('');
            $('.price-value').val('');

            $('#commission_display').hide();
            $('#after_commission_display').hide();
            $('#modal-price_service').modal('show');

        })

    // edit price
    $(document)
        .on('click', '.edit-price', function() {
            $('#commission_display').show();
            $('#after_commission_display').show();

            var row = table_price_service.row($(this).parents('tr')).data();
            var categorySelect = new Option(row.service_type.symptom.services.name, row.service_type.symptom.services.id, false, false);
            $('.category-select').empty().append(categorySelect).trigger('change');
            var symptomSelect = new Option(row.service_type.symptom.name, row.service_type.symptom.id, false, false);
            $('.symptom-select').empty().append(symptomSelect).trigger('change');
            var serviceSelect = new Option(row.service_type.name, row.service_type.id, false, false);
            $('.service_type-select').empty().append(serviceSelect).trigger('change');
            var prosel = new Option(row.product_group.name, row.product_group.id, false, false);
            $('#select-product_group').empty().append(prosel).trigger('change');

            $('.price-id').val(row.id);
            $('.price-value').val(row.value);
            $('#modal-price_service').modal('show');
            $('#commission').val(row.commission_now.value);
            commission(row.value);
            $('#modal-price_service').modal('show');
        })

    // chenge
    $(document)
        .on('change', '.category-select', function() {
            category_id = $('.category-select').val();
            if (category_id) {
                $('.symptom-select').val('').change();
                $('.service_type-select').val('').change();

                $('.symptom-select-control').show();
                $('.service_type-select-control').hide();

                globalCRUD.select2(".symptom-select", '/admin/symptom/select2/' + category_id);
            }
        })

    // chenge
    $(document)
        .on('change', '.symptom-select', function() {
            val = $(this).val();
            if (val) {
                $('.service_type-select-control').show();
                $('.service_type-select').val('').change();
                globalCRUD.select2(".service_type-select", '/symptom/' + val + '/service_type');
            }
        })

    // change product_group
    $(document).on('change', '.service_type-select', function() {
        asu = $(this).val();
        if (asu) {
            $('.product_group-select-control').show();
            $('#select-product_group').val('').change();
            globalCRUD.select2("#select-product_group", '/product_groups/select2/' + asu);
        }
    });

    // delete price
    $(document)
        .on('click', '.delete-price', function() {
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.delete('/technician/' + id + '/price_service/')
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('ok');
                        // reload table
                        table_price_service.ajax.reload();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })

    // save price
    $('#form-price_service')
        .submit(function(e) {
            data = Helper.serializeForm($(this));
            data.id = $('input[name="teknisi_id"]').val();
            if(parseInt($('.price-value').val()) < parseInt($('#commission').val())) {
                Helper.warningNotif('Nominal must not be less than the commission !')
                return false;
            } else {
                Helper.loadingStart();
                Axios.post('/technician/' + data.id + '/store_price', data)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif(response.data.msg);
                        // reload table
                        table_price_service.ajax.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });

                $('#modal-price_service').modal('hide');
                e.preventDefault();
            }
        })

    function readURL(input) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }

    $("#imageUpload").change(function() {
        input = this;

        if (input.files && input.files[0]) {
            Helper.loadingStart();
            user_id = $("#hidden-user-id").val();
            var formData = new FormData();
            formData.append("image", input.files[0]);
            formData.append("user_id", user_id);

            console.log(formData);
            Axios
                .post(Helper.apiUrl('/user/profile/image/' + user_id), formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    Helper.successNotif('Success, ok');
                    readURL(input);
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        }
    });
</script>
@endsection
<style>
    /* Profile container */
    .profile {
        margin: 20px 0;
    }

    /* Profile sidebar */
    .profile-sidebar {
        background: #fff;
    }

    .profile-userpic img {
        float: none;
        margin: 0 auto;
        width: 50%;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important;
    }

    .profile-usertitle {
        text-align: center;
        margin-top: 20px;
    }

    .profile-usertitle-name {
        color: #5a7391;
        font-size: 16px;
        font-weight: 600;
        margin-bottom: 7px;
    }

    .profile-usertitle-job {
        text-transform: uppercase;
        color: #555;
        font-size: 12px;
        font-weight: 600;
        margin-bottom: 15px;
    }

    .profile-userbuttons {
        text-align: center;
        margin-top: 10px;
    }

    .profile-userbuttons .btn {
        text-transform: uppercase;
        font-size: 11px;
        font-weight: 600;
        padding: 6px 15px;
        margin-right: 5px;
    }

    .profile-userbuttons .btn:last-child {
        margin-right: 0px;
    }

    .profile-usermenu {
        margin-top: 30px;
    }

    /* Profile Content */
    .profile-content {
        padding: 20px;
        background: #fff;
        min-height: 460px;
    }

    .avatar-upload {
        position: relative;
        max-width: 205px;
        margin: 0px auto;
    }

    .avatar-upload .avatar-edit {
        position: absolute;
        right: 12px;
        z-index: 1;
        top: 10px;
    }

    .avatar-upload .avatar-edit input {
        display: none;
    }

    .avatar-upload .avatar-edit input+label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
    }

    .avatar-upload .avatar-edit input+label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
    }

    .avatar-upload .avatar-edit input+label:after {
        content: "\f040";
        font-family: 'FontAwesome';
        color: #757575;
        position: absolute;
        top: 10px;
        left: 0;
        right: 0;
        text-align: center;
        margin: auto;
    }

    .avatar-upload .avatar-preview {
        width: 192px;
        height: 192px;
        position: relative;
        border-radius: 100%;
        border: 6px solid #F8F8F8;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }

    .avatar-upload .avatar-preview>div {
        width: 100%;
        height: 100%;
        border-radius: 100%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
    }
</style>
