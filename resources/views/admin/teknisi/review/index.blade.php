@extends('admin.home')

@section('content')
<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>

        <div class="card-body">
            <div class="tab-pane show active" id="tab-1" role="tabpanel">
                <table id="table-list-orders" class="table table-hover table-bordered" style="width:100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th width="20%">Orders Code</th>
                            <th width="20%">Rating</th>
                            <th width="60%">Review</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    var table = $('#table-list-orders').DataTable({
        processing: true,
        serverSide: true,
        ordering: 'true',
        autoWidth: false,
        responsive: true,
        scrollX: true,
        lengthChange: true,
        // order: [3, 'desc'],
        responsive: true,
        language: {
            buttons: {
                colvis: '<i class="fa fa-list-ul"></i>'
            },
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        buttons: [{
                extend: 'colvis'
            },
            {
                text: '<i class="fa fa-refresh"></i>',
                action: function(e, dt, node, config) {
                    dt.ajax.reload();
                }
            }
        ],
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/teknisi/review-list'),
            type: 'get',
        },
        columnDefs: [{
            targets: [3],
            createdCell: function(cell) {

                var $cell = $(cell);

                $(cell).contents().wrapAll("<div class='content'></div>");
                var $content = $cell.find(".content");

                if ($content.text().length > 20) {
                    $(cell).append($("<a href='#'>Read more</a>"));
                    $btn = $(cell).find("a");

                    $content.css({
                        "height": "20px",
                        "overflow": "hidden"
                    })
                    $cell.data("isLess", true);

                    $btn.click(function() {
                        var isLess = $cell.data("isLess");
                        $content.css("height", isLess ? "auto" : "20px")
                        $(this).text(isLess ? "Read less" : "Read more")
                        $cell.data("isLess", !isLess)
                    })
                }
            }
        }],
        columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                sortable: false,
            },
            {
                data: "code",
                name: "code",
                render: function(data, type, full) {
                    link = "<a href='" + Helper.url('/teknisi/request-job-accept/' + full.id) + "'># " + full.code + "</a>";
                    if (full.garansi) {
                        link += '</br><span class="mb-2 mr-2 badge badge-pill badge-primary">Warranty Claim</span>';
                    }
                    return link;
                }
            },
            {
                data: 'id',
                name: 'id',
                orderable: false,
                searchable: false,
                visible: true,
                render: function(data, type, row) {
                    return Helper.ratingStar(row.rating_and_review.rating.value);
                }
            },
            {
                data: 'id',
                name: 'id',
                orderable: false,
                searchable: false,
                visible: true,
                render: function(data, type, row) {
                    return row.rating_and_review.review.description;
                }
            }
        ]
    });
</script>
@endsection