@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                COMPLAINT LIST
            </div>
        </div>
        
        <div class="card-body">
            <table id="table-list-complaint" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Orders Code</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection


@section('script')

<script>
    var table = $('#table-list-complaint').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        select: true,
        dom: 'Bflrtip',
        ordering:'true',
        order: [3, 'desc'],
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search..."
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/teknisi/complaint-list/datatables'),
            type: "get",

        },
        columns: [
            {
                data: "code",
                name: "code",
                render: function(data, type, full) {
                    link = "<a href='" + Helper.url('/teknisi/request-job-accept/' + full.id) + "'># " + full.code + "</a>";
                    if (full.garansi) {
                        link += '</br><span class="ml-auto badge badge-primary">Warranty Claim</span>';
                    }
                    if (full.unread_complaint != null) {
                        link += '</br><span class="ml-auto badge badge-warning">NEW</span>';
                    }
                    return link;
                }
            },
            {
                data: "updated_at",
                name: "updated_at",
                render: function(data, type, full){
                    return moment(full.updated_at).format("DD MMMM YYYY");
                }
            },
            {
                data: "order_status.name",
                name: "order_status.name",
            },
            {
                data: "id",
                name: "id",
                orderable: false,
                searchable: false,
                render: function(data, type, full) {
                    if (full.order_status.id == 11) {
                        return "<a href='/teknisi/complaint-jobs/detail/"+ full.id +"' class='badge badge-success btn-sm ' class='badge badge-success btn-sm '  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i> </a>";
                    } else if (full.order_status.id == 10) {
                        return "<a href='/teknisi/complaint-jobs/detail/"+ full.id +"' class='badge badge-success btn-sm ' class='badge badge-success btn-sm '  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i> </a> ";
                    } else{
                        return "-"
                    }
                }
            }
        ]
    });
</script>

@endsection
