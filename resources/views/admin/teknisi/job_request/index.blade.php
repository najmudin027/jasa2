@extends('admin.home')

@section('content')
<!-- <div class="col-md-12">
    <div class="card header-border">
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="">Schedule</label>
                            <input name="order_date_range" type="text" value="" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="label-control">Warranty Claims</label>
                            <select class="form-control" name="klaim_garansi">
                                <option value="">All</option>
                                <option value="1">Only Warranty Claims</option>
                                <option value="0">Without Warranty Claims</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="label-control">Payment Type</label>
                            <select class="form-control" name="payment_type">
                                <option value="">All</option>
                                <option value="1">Online</option>
                                <option value="0">Offline</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="label-control">Status</label>
                            <select class="form-control select-status" name="order_status_id[]" multiple style="width: 100%">
                                <option value="2">Analyzing</option>
                                <option value="3">Waiting Approval</option>
                                <option value="4">On Working</option>
                                <option value="9">Processing</option>
                                <option value="7">Job Done</option>
                                <option value="10">Job Completed</option>
                                <option value="11">Complaint</option>
                                <option value="5">Cancel</option>
                            </select>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
            </form>
        </div>
    </div>
</div> -->

<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>

        <div class="card-body">
            <div class="tab-pane show active" id="tab-1" role="tabpanel">
                <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Orders Code</th>
                            <th>Service</th>
                            <th>Customer Name</th>
                            <th>Create Date</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" data-backdrop="false" id="modal-job-done" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Jobs Done</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="get_id" id="done">

                <label for="">Symptom Code</label>
                <select class="js-example-basic-single" id="ms_symptom_code" name="ms_symptom_code" style="width:100%"></select>
                <strong><span id="error-ms_symptom_code" style="color:red"></span></strong><br>
                <label for="">Repair Code</label>
                <select class="js-example-basic-single" id="ms_repair_code" name="ms_repair_code" style="width:100%"></select>
                <strong><span id="error-ms_repair_code" style="color:red"></span></strong><br>
                <label for="">Desc</label>
                <textarea class="form-control" id="repair_desc" name="repair_desc" rows="3"></textarea>
                <strong><span id="error-repair_desc" style="color:red"></span></strong><br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" id="jobs_done" data-id="" class="btn btn-primary">Success Jobs</button>
                <!-- <button type="submit"  class="mb-2 mr-2 btn btn-sm btn-primary delete-selected" style="float:right">Job Done</button> -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    globalCRUD
        .select2Static("#ms_symptom_code", '/symptom-code/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })
        .select2Static("#ms_repair_code", '/repair-code/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })
</script>
<script>
    $(function() {
        $('input[name="order_date_range"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="order_date_range"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });

        $('input[name="order_date_range"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        globalCRUD.select2(".select-status")
    });

    var table = $('#table-list-orders').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        select: true,
        dom: 'Bflrtip',
        ordering: 'true',
        order: [3, 'desc'],
        responsive: true,
        language: {
            buttons: {
                colvis: '<i class="fa fa-list-ul"></i>'
            },
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        buttons: [{
                extend: 'colvis'
            },
            {
                text: '<i class="fa fa-refresh"></i>',
                action: function(e, dt, node, config) {
                    dt.ajax.reload();
                }
            }
        ],
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/teknisi/request-job/datatables/all'),
            type: 'get',
        },
        columns: [
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                sortable: false,
            },
            {
                data: "code",
                name: "code",
                render: function(data, type, full) {
                    link = "<a href='" + Helper.url('/teknisi/request-job-accept/' + full.id) + "'># " + full.code + "</a>";
                    if (full.garansi) {
                        link += '</br><span class="mb-2 mr-2 badge badge-pill badge-primary">Warranty Claim</span>';
                    }
                    if (full.teknisi_read_at == null) {
                        link += '</br><span class="mb-2 mr-2 badge badge-pill badge-warning">NEW</span>';
                    }
                    return link;
                }
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    return full.symptom.name + "<br> <small>" + full.symptom.name + "&nbsp;" + full.product_group.name + "</small>";
                }
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    return full.customer.full_name;
                }
            },
            {
                data: "created_at",
                name: "created_at",
                render: function(data, type, full) {
                    return full.created_at == null ? '-' : moment(full.created_at).format("DD MMMM YYYY hh:mm");
                }
            },
            {
                data: "grand_total",
                name: "grand_total",
                render: $.fn.dataTable.render.number(',', '.,', 0, 'Rp. ')
            },
            {
                data: "order_status.name",
                name: "order_status.name",
                render: function(data, type, full) {

                    var content = '';
                    if (full.orders_statuses_id === 2) {
                        content = '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                    } else if (full.orders_statuses_id === 3) {
                        content = '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                    } else if (full.orders_statuses_id === 4) {
                        content = '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                    } else if (full.orders_statuses_id === 5) {
                        content = '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                    } else if (full.orders_statuses_id === 6) {
                        content = '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                    } else if (full.orders_statuses_id === 7) {
                        content = '<span class="badge badge-focus">' + full.order_status.name + '</span>';
                    } else if (full.orders_statuses_id === 8) {
                        content = '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                    } else if (full.orders_statuses_id === 9) {
                        content = '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                    } else if (full.orders_statuses_id === 10) {
                        content = '<span class="badge badge-success">' + full.order_status.name + '</span>';
                    } else if (full.orders_statuses_id === 11) {
                        content = '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                    }

                    if (full.countdown_autocancel != null) {
                        $(document).ready(function() {
                            $('#clock' + full.id + '').countdown(full.countdown_autocancel, function(event) {
                                $(this).html(event.strftime('%D Days (%H Hour : %M Min)'));
                            });
                        });

                        if (!$.inArray(full.orders_statuses_id, [2,3])) {
                            content += "<br><div class='badge badge-danger' data-toggle='tooltip' data-placement='top' title='this countdown for auto cancel order'><span id='clock" + full.id + "'></span></div>";
                        }
                    }

                    if (full.countdown_autocompleted != null) {
                        $(document).ready(function() {
                            $('#clock' + full.id + '').countdown(full.countdown_autocompleted, function(event) {
                                $(this).html(event.strftime('%D Days (%H Hour : %M Min)'));
                            });
                        });

                        if (!$.inArray(full.orders_statuses_id, [7])) {
                            content += "<br><div class='badge badge-success' data-toggle='tooltip' data-placement='top' title='this countdown for auto completed order'><span id='clock" + full.id + "'></span></div>";
                        }
                    }

                    return content;
                }
            },
            {
                data: "id",
                name: "id",
                orderable: false,
                searchable: false,
                render: function(data, type, full) {
                    detail = "<a href='/teknisi/request-job-accept/" + full.id + "' class='badge badge-primary btn-sm'  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>";
                    complaint = "<a href='/teknisi/transacsion_list/complaint/detail/" + full.id + "' class='badge badge-success btn-sm '  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-comment'></i></a>";
                    jobDone = '<a href="" class="badge badge-primary btn-sm click_btn_job_done" data-id-order='+ full.id +' data-toggle="modal" data-target="#modal-job-done" ><i class="fa fa-check" style="color:white"></i></a>'
                    onWorking = '<a href="" type="submit" id="on_working" data-id-processing='+full.id+' class="mb-2 mr-2 badge badge-sm badge-success delete-selected" data-toggle="tooltip" data-html="true" title="On Working"><i class="fa fa-check" style="color:white"></i></a>'
                    if (full.order_status.id == 2 || full.order_status.id == 3 || full.order_status.id == 5 || full.order_status.id == 7 || full.order_status.id == 8 || full.order_status.id == 10) {
                        return detail;
                    } else if (full.order_status.id == 11) {
                        return complaint;
                    }else if (full.order_status.id == 4) {
                        return jobDone + ' ' + detail;
                    } else if(full.order_status.id == 9){
                        return onWorking + '' + detail;
                    } else {
                        return "-"
                    }

                }
            }
        ]
    });

    $(document).on('click', '#on_working', function(e) {
            id = $(this).attr('data-id-processing');
            Helper.confirm(function(){
                Helper.loadingStart();
                $.ajax({
                    url:Helper.apiUrl('/teknisi/on-working/' + id ),
                    type: 'post',

                    success: function(res) {
                        Helper.successNotif('Success');
                        Helper.loadingStop();
                        Helper.redirectTo('/teknisi/dashboard');
                    },
                    error: function(res) {
                        console.log(res);
                        Helper.loadingStop();
                    }
                })
            })

            e.preventDefault()
        })

    $(document).on('click', '.click_btn_job_done', function(){
        var id_job_done = $(this).attr('data-id-order');
        // alert(id_job_done)
        $('#done').val(id_job_done)
    })

    $(document).ready(function() {


        $("#form-search").submit(function(e) {
            input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/teknisi/request-job/datatables' + playload);
            tableOrder.ajax.url(url).load();
            e.preventDefault();
        })

    })

    $(document).on('click', '#jobs_done', function(e) {
        id = $('#done').val();
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/teknisi/jobs-done/' + id ),
                type: 'post',
                data: {
                    ms_symptom_code: $('#ms_symptom_code').val(),
                    ms_repair_code: $('#ms_repair_code').val(),
                    repair_desc: $('#repair_desc').val(),
                },
                success: function(res) {
                    Helper.successNotif('Success');
                    Helper.loadingStop();
                    Helper.redirectTo('/teknisi/list-request-job');
                },
                error: function(xhr, status, error) {
                    Helper.loadingStop();
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                        })
                    }
                }
            })
        },
        {
            title: "Are You Sure",
            message: "Are you really sure this work has been completed ?",
        })

        e.preventDefault()
    })
</script>
@endsection
