@extends('admin.home')
@section('content')

<style>
.rounded-profile {
    border-radius: 100% !important;
    overflow: hidden;
    width: 100px;
    height: 100px;
    border: 8px solid rgba(255, 255, 255, 0.7);
}
p {
    margin-bottom:0px;
}
.wrap{width: 500px;margin: 2em auto;}

.clearfix:before, .clearfix:after { content: " "; display: table; }
.clearfix:after { clear: both; }

.select2-result-repository { padding-top: 4px; padding-bottom: 3px; }
.select2-result-repository__avatar { float: left; width: 60px; margin-right: 10px; }
.select2-result-repository__avatar img { width: 100%; height: auto; border-radius: 2px; }
.select2-result-repository__meta { margin-left: 70px; }
.select2-result-repository__title { color: black; font-weight: bold; word-wrap: break-word; line-height: 1.1; margin-bottom: 4px; }
.select2-result-repository__price, .select2-result-repository__stargazers { margin-right: 1em; }
.select2-result-repository__price, .select2-result-repository__stargazers, .select2-result-repository__watchers { display: inline-block; color: rgb(68, 68, 68); font-size: 13px; }
.select2-result-repository__description { font-size: 13px; color: #777; margin-top: 4px; }
.select2-results__option--highlighted .select2-result-repository__title { color: white; }
.select2-results__option--highlighted .select2-result-repository__price, .select2-results__option--highlighted .select2-result-repository__stargazers, .select2-results__option--highlighted .select2-result-repository__description, .select2-results__option--highlighted .select2-result-repository__watchers { color: #c6dcef; }

.select2-container--adwitt .select2-results > .select2-results__options {
  max-height: 300px;
}
.select2-results__options {overflow-y: auto;}
</style>

    <div class="col-md-12">
        @if($order->orders_statuses_id == 2)
            <div class="alert alert-danger">
                ANALYZING
                <button type="button" class="mb-2 mr-2 btn btn-sm btn-danger delete-selected" id="reject_job" style="float: right;"><i class="fa fa-times"></i> REJECT JOBS</button>
                <button type="button" class="accept-modal mb-2 mr-2 btn btn-sm btn-primary" style="float: right;"><i class="fa fa-check"></i> ACCEPTED JOBS</button>
            </div>
        @elseif($order->orders_statuses_id == 3)
            <div class="alert alert-success">
                APPROVED JOBS
            </div>
        @elseif($order->orders_statuses_id == 4)
            <div class="alert alert-success">
                ON WORKING
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-job-done" style="float: right;">
                    <i class="fa fa-check"></i> JOBS DONE
                </button>
            </div>
        @elseif($order->orders_statuses_id == 5)
            <div class="alert alert-danger">
                CANCELED
            </div>
        @elseif($order->orders_statuses_id == 6)
            <div class="alert alert-danger">
                RESCHEDULE
            </div>
        @elseif($order->orders_statuses_id == 7)
            <div class="alert alert-success">
                JOBS DONE
            </div>
        @elseif($order->orders_statuses_id == 8)
            <div class="alert alert-warning">
                ON WORKING
            </div>
        @elseif($order->orders_statuses_id == 9)
            <div class="alert alert-primary">
                PROCESSING
                <button type="submit" id="on_working" data-id="{{ $order->id }}" class="mb-2 mr-2 btn btn-sm btn-success delete-selected" style="float: right;">
                    <i class="fa fa-spinner"></i> ON WORKING
                </button>
            </div>
        @elseif($order->orders_statuses_id == 10)
            <div class="alert alert-success">
                JOBS COMPLETED
            </div>
        @elseif($order->orders_statuses_id == 11)
            <div class="alert alert-success">
                JOBS COMPLAINT
            </div>
        @endif
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header header-bg">
                Order #{{ $order->code }}
            </div>
        </div>
    </div>

    <div class="col-md-4" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Order Detail

                <div class="btn-actions-pane-right text-capitalize">
                    @if($order->order_status->id == 2)
                        <button class="accept-modal btn btn-success btn-sm accept-job"><i class="fa fa-arrow-right"></i> Accept Job</button>
                    @endif

                    @if($order->order_status->id == 4)
                        <button class="btn btn-success btn-sm done-job" data-toggle="modal" data-target="#modal-job-done"><i class="fa fa-check"></i> Job Done</button>
                    @endif

                    @if($order->order_status->id == 9)
                        <button type="button" id="on_working" data-id="{{ $order->id }}"  class="btn btn-success btn-sm on_working-job"><i class="fa fa-spinner"></i> On Working</button>
                    @endif
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Order No:</label>
                                <p>{{ $order->code }}</p>
                            </div>
                            <div class="col-md-6">
                                <label>Order No:</label>
                                <p><a href="" data-toggle="modal" data-target="#productDetail" style="text-decoration: underline">Product Detail</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
            <div class="modal fade" id="productDetail" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Product Detail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <label>Product Name</label>
                        <div class="form-group">
                            <input type="text" class="form-control" value="{{ $order->product_name_detail }}" readonly>
                        </div>

                        <label>Brand Name</label>
                        <div class="form-group">
                            <input type="text" class="form-control"  value="{{ $order->brand_name_detail }}" readonly>
                        </div>

                        <label>Model Name</label>
                        <div class="form-group">
                            <input type="text" class="form-control"  value="{{ $order->model_name_detail }}" readonly>
                        </div>

                        <label>Serial Number</label>
                        <div class="form-group">
                            <input type="text" class="form-control" value="{{ $order->serial_number_detail }}" readonly>
                        </div>

                        <label>Remark</label>
                        <div class="form-group">
                            <textarea name="" class="form-control" id="" readonly>{{ $order->remark_detail }}</textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </div>

                <label>Schedule:</label>
                <p id="schedule_display">
                    <span class="mb-2 mr-2 badge badge-pill badge-primary">
                        {{ $order->schedule->format('d F Y G:i') }}
                    </span>
                </p>

                <label>Order Status:</label>
                <p><span class="mb-2 mr-2 badge badge-pill badge-dark">{{ $order->order_status->name }}</span></p>

                <label>Order Note:</label>
                <p>{{ $order->note }}</p>

                <label>Symptom detail:</label>
                <div class="alert alert-info" role="alert">
                    {{ $order->symptom_detail !== null ? $order->symptom_detail : "-" }}
                </div>
                <label for="ms_symptom_code">Symptom Code</label>
                <p>{{ $order->symptom_code == null ? '-' : $order->symptom_code->name }}</p>
                <label for="ms_symptom_code">Repair Code</label>
                <p>{{ $order->repair_code == null ? '-' : $order->repair_code->name }}</p>
                <label for="repair_desc">Repair Description</label>
                <p>{{ $order->repair_desc == null ? '-' : $order->repair_desc }}</p>

                <label>Payment Type:</label>
                <br />
                {!! $order->payment_type == 0 ? '<div class="badge badge-danger">Offline</div>' : '<div class="badge badge-info">Online</div>' !!}

                <div class="text-right" style="margin-top: 10px;">
                    @if($order->orders_statuses_id == 9)
                    <label>Delivery Note : &nbsp;&nbsp;</label>
                    <a href="{{ url('/admin/order/workmanship_report_pdf/'. $order_id ) }}" class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-sm btn-outline-warning"><i class="fa fa-file-pdf"></i></a>
                    <small>.pdf</small>
                    @endif
                    <br />
                    @if ($order->orders_statuses_id == 1 || $order->orders_statuses_id == 2 || $order->orders_statuses_id == 5 || $order->orders_statuses_id == 6)
                    <label>Invoice: &nbsp; - </label>
                    @else
                    <label>Invoice: &nbsp;&nbsp;</label>
                    <a href="{{ url('/admin/invoice/'. $order_id ) }}" class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-sm btn-outline-danger" target="_blank"><i class="fa fa-file-pdf"></i></a>
                    <small>.pdf</small>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Billing Detail
                <div class="btn-actions-pane-right text-capitalize">
                    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_maps" data-lat='{{ $order->user->address->latitude }}' data-lng='{{ $order->user->address->longitude }}'><i class="fa fa-map-marker"></i> View Location</button> --}}
                </div>
            </div>
            <div class="card-body">
                <label>Name:</label>
                <p>{{ $order->user->name }}</p>

                <label>Email:</label>
                <p>{{ $order->user->email }}</p>

                <label>Phone Number:</label>
                <p>{{ $order->user->phone }}</p>

                <label>Address:</label>
                <p>{{ $order->address_type_name }}</p>
                <p>{{ $order->address }}</p>
            </div>
        </div>
    </div>

    <div class="col-md-4" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Billing Amount
            </div>
            <div class="card-body bill-amount-area">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Services {{ $order->service_name }}</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @foreach($order->service_detail as $key => $detail)
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    @if ($detail->symptom_name !== null && $detail->service_type_name !== null && $order->product_group_name !== null)
                                        <div class="widget-heading">{{ $detail->symptom_name }}</div>
                                        <div class="widget-subheading">{{ $detail->service_type_name }} {{ $order->product_group_name }}</div>
                                    @else
                                        <div class="widget-heading">{{ $detail->symptom->name }}</div>
                                        <div class="widget-subheading">{{ $detail->services_type->name }} {{ $order->product_group->name }}</div>
                                        <div class="widget-subheading">Qty : {{ $detail->unit }} x</div>
                                        <input type="hidden" value="{{ $detail->unit }}" class="unit_services">
                                    @endif

                                </div>
                                <div class="widget-content-right">
                                    <div class="text-primary"><span> <strong>Rp. {{ number_format($detail->price) }}</strong></span></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Sparepart</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @foreach($sparepart_details as $sparepart_detail)
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">{{ $sparepart_detail->name_sparepart }}</div>
                                    <div class="widget-subheading">{{ $sparepart_detail->priceThousandSeparator() }} x {{ $sparepart_detail->quantity }}</div>
                                </div>
                                <div class="widget-content-right">

                                    <div class="text-primary"><span> <strong>Rp. {{ \App\Helpers\thousanSparator($sparepart_detail->quantity * $sparepart_detail->price) }}</strong></span></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                    @foreach($item_details as $item)
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">{{ $item->name_product }}</div>
                                    <div class="widget-subheading">{{ $item->price }} x {{ $item->quantity }}</div>
                                </div>
                                <div class="widget-content-right">
                                    <div class="text-primary"><span><strong>Rp. {{ \App\Helpers\thousanSparator($item->quantity * $item->price) }}</strong></span></div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                    @foreach($tmp_spareparts as $tmp_sparepart)
                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">{{ $tmp_sparepart->name_sparepart }}</div>
                                    <div class="widget-subheading">{{ $tmp_sparepart->price }} x {{ $tmp_sparepart->quantity }}</div>
                                </div>
                                <div class="widget-content-right">
                                    <div class="text-primary"><span><strong>Rp. {{ \App\Helpers\thousanSparator($tmp_sparepart->quantity * $tmp_sparepart->price) }}</strong></span></div>
                                </div>
                            </div>
                            <!-- <center>
                                <span class="badge badge-warning badge-pill">Waiting Customer Confirmation</span>
                            </center> -->
                        </div>
                    </li>
                    @endforeach

                    <li class="list-group-item">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Grand Total</div>
                                </div>
                                <div class="widget-content-right">
                                    <div class="text-primary"><span id="grand_total_order"><strong>Rp. {{ \App\Helpers\thousanSparator($order->grand_total) }}</strong></span></div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- sparepart -->
    <div class="col-md-8" style="margin-top: 10px;">
        <!-- <div class="card">
            <div class="card-header header-border">
                Detail sparepart
            </div>
        </div> -->

        @if(!in_array($order->orders_statuses_id, [3, 5, 7, 9, 10]))
            @if(count($sparepart_details) == 0 && count($item_details) == 0 && count($tmp_spareparts) == 0 && count($tmp_item_detail) == 0)
                <form id="sparepart-detail-form">
                    <input type="hidden" id="sparepart_detail_order_id" value="{{ $order->id }}" name="sparepart_detail_order_id">
                    <!-- sparepart -->
                    <div class="card">
                        <div class="card-header header-border">
                            Add additional sparepart from
                            <div class="btn-actions-pane-right">
                                <div class="nav">
                                    <a data-toggle="tab" href="#tab-eg2-0" class="btn-tab btn-wide active btn btn-outline-success btn-md" data-desc="sparepaer-inventory">My Inventory</a>
                                    <a data-toggle="tab" href="#tab-eg2-1" class="btn-tab btn-wide mr-1 ml-1  btn btn-outline-success btn-md" data-desc="sparepaer-company">Company </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="type_part" id="type_part" value="my-inventory">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-eg2-0" role="tabpanel">
                                    <div class="dynamic_sparepart">
                                        <div class="position-relative row form-group">
                                            <div class="col-sm-6">
                                                Name
                                            </div>
                                            <div class="col-sm-2">
                                                Price
                                            </div>
                                            <div class="col-sm-2">
                                                Unit
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <div class="col-sm-6">
                                                <input type="text" placeholder="name" name="teknisi_sparepart_name[]" data-uniq="xxx" class="form-control teknisi_sparepart_name_input-xxx" />
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" placeholder="price" name="teknisi_sparepart_price[]" data-uniq="xxx" class="form-control teknisi_sparepart_price_i teknisi_sparepart_price_input-xxx" />
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" placeholder="unit" name="teknisi_sparepart_qty[]" data-uniq="xxx" class="form-control teknisi_sparepart_qty_i teknisi_sparepart_qty_input-xxx" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-eg2-1" role="tabpanel">
                                    <div class="dynamic_sparepart_company">
                                        <div class="position-relative row form-group">
                                            <div class="col-sm-3">
                                                Warehouse
                                            </div>
                                            <div class="col-sm-5">
                                                Name
                                            </div>
                                            <div class="col-sm-2">
                                                Price
                                            </div>
                                            <div class="col-sm-1">
                                                Unit
                                            </div>
                                        </div>
                                        <div class="position-relative row form-group">
                                            <div class="col-sm-3">
                                                <select class="form-control select-warehouse select-warehouse-xxx" name="warehouse_id[]" data-uniq="xxx" style="width:100%">
                                                    <option value="" selected="selected">--Pilih--</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <select class="form-control select-inventory select-inventory-xxx" name="inventory_id[]" data-uniq="xxx" style="width:100%">
                                                    <option value="" selected="selected">--Pilih--</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" data-uniq="xxx" class="form-control inventory-price-xxx inventory-price" name="inventory_price[]" disabled />
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" data-uniq="xxx" placeholder="unit" name="inventory_qty[]" class="form-control inventory-unit inventory-unit-xxx" value="0" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Save Change</button>&nbsp;
                        </div>
                    </div>
                </form>
            @endif

            @if(count($sparepart_details) > 0 || count($tmp_spareparts) > 0)
                <form id="sparepart-detail-form">
                    <input type="hidden" id="sparepart_detail_order_id" value="{{ $order->id }}" name="sparepart_detail_order_id">
                    <input type="hidden" name="type_part" id="type_part" value="my-inventory">
                    <div class="card">
                        <div class="card-header header-border">
                            Technician Inventory
                        </div>
                        <div class="card-body">
                            <div class="dynamic_sparepart">
                                <div class="position-relative row form-group">
                                    <div class="col-sm-6">
                                        Name
                                    </div>
                                    <div class="col-sm-2">
                                        Price
                                    </div>
                                    <div class="col-sm-2">
                                        Unit
                                    </div>
                                </div>
                                @foreach($sparepart_details as $no => $sparepart_detail)
                                    <?php $uniq = time(); ?>
                                    <div class="position-relative row form-group dynamic_sparepart_content" id="my_part_row_{{ $uniq }}">
                                    <input type="hidden" value="{{ $sparepart_detail->id }}" name="sparepart_detail_id[]" />
                                    <input type="hidden" value="0" name="tmp_sparepart_id[]" />
                                        <div class="col-sm-6">
                                            <input type="text" placeholder="name" name="teknisi_sparepart_name[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_name_input-{{ $uniq }}" value="{{ $sparepart_detail->name_sparepart }}" disabled/>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" placeholder="price" name="teknisi_sparepart_price[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_price_i teknisi_sparepart_price_input-{{ $uniq }}" value="{{ $sparepart_detail->price }}" disabled/>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" placeholder="unit" name="teknisi_sparepart_qty[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_qty_i teknisi_sparepart_qty_input-{{ $uniq }}" value="{{ $sparepart_detail->quantity }}" disabled/>
                                        </div>
                                        <div class="col-sm-2" style="padding-left:0px;">
                                            <button type="button" data-sparepart_detail_id="{{ $sparepart_detail->id }}" data-uniq="{{ $uniq }}" class="btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                                        </div>
                                    </div>
                                @endforeach
                                @if (count($tmp_spareparts) > 0)
                                    @foreach($tmp_spareparts as $no => $tmp_sparepart)
                                        <?php $uniq = time(); ?>
                                        <div class="position-relative row form-group dynamic_sparepart_content" id="my_part_row_{{ $uniq }}">
                                            <input type="hidden" value="{{ $tmp_sparepart->id }}" name="tmp_sparepart_id[]" />
                                            <input type="hidden" value="0" name="sparepart_detail_id[]" />
                                            <div class="col-sm-6">
                                                <input type="text" placeholder="name" name="teknisi_sparepart_name[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_name_input-{{ $uniq }}" value="{{ $tmp_sparepart->name_sparepart }}" />
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" placeholder="price" name="teknisi_sparepart_price[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_price_i teknisi_sparepart_price_input-{{ $uniq }}" value="{{ $tmp_sparepart->price }}" />
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" placeholder="unit" name="teknisi_sparepart_qty[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_qty_i teknisi_sparepart_qty_input-{{ $uniq }}" value="{{ $tmp_sparepart->quantity }}" />
                                            </div>
                                            <div class="col-sm-2" style="padding-left:0px;">
                                                <button type="button" data-tmp_sparepart_id="{{ $tmp_sparepart->id }}" data-uniq="{{ $uniq }}" class="btn btn-danger btn_remove_my_tmp_part"><i class="fa fa-close"></i></button>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></i>&nbsp; Save Change</button>&nbsp;
                            @if(count($sparepart_details) > 0)
                                <button type="button" class="btn btn-primary btn-sm teknisi_sparepart_add_btn"><i class="fa fa-plus"></i>&nbsp; Add Spareparts</button>
                            @else
                                <button type="button" class="btn btn-primary btn-sm teknisi_sparepart_add_btn"><i class="fa fa-plus"></i>&nbsp; Add Spareparts</button>
                            @endif
                        </div>
                    </div>
                </form>
            @endif

           <!--  @if(count($tmp_spareparts) > 0)
                <form id="sparepart-detail-form">
                    <input type="hidden" id="sparepart_detail_order_id" value="{{ $order->id }}" name="sparepart_detail_order_id">
                    <input type="hidden" name="type_part" id="type_part" value="my-inventory">
                    <div class="card">
                        <div class="card-header header-border">
                            Technician Inventory
                        </div>
                        <div class="card-body">
                            <div class="dynamic_sparepart">
                                <div class="position-relative row form-group">
                                    <div class="col-sm-6">
                                        Name
                                    </div>
                                    <div class="col-sm-2">
                                        Price
                                    </div>
                                    <div class="col-sm-2">
                                        Unit
                                    </div>
                                </div>
                                @foreach($tmp_spareparts as $no => $tmp_sparepart)
                                    <?php $uniq = time(); ?>
                                    <div class="position-relative row form-group dynamic_sparepart_content" id="my_part_row_{{ $uniq }}">
                                        <input type="hidden" value="{{ $tmp_sparepart->id }}" name="tmp_sparepart_id[]" />
                                        <input type="hidden" value="0" name="sparepart_detail_id[]" />
                                        <div class="col-sm-6">
                                            <input type="text" placeholder="name" name="teknisi_sparepart_name[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_name_input-{{ $uniq }}" value="{{ $tmp_sparepart->name_sparepart }}" />
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" placeholder="price" name="teknisi_sparepart_price[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_price_i teknisi_sparepart_price_input-{{ $uniq }}" value="{{ $tmp_sparepart->price }}" />
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" placeholder="unit" name="teknisi_sparepart_qty[]" data-uniq="{{ $uniq }}" class="form-control teknisi_sparepart_qty_i teknisi_sparepart_qty_input-{{ $uniq }}" value="{{ $tmp_sparepart->quantity }}" />
                                        </div>
                                        <div class="col-sm-2" style="padding-left:0px;">
                                            <button type="button" data-tmp_sparepart_id="{{ $tmp_sparepart->id }}" data-uniq="{{ $uniq }}" class="btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Save Change</button>&nbsp;
                            @if(count($sparepart_details) > 0)
                                <button type="button" class="btn btn-primary btn-sm teknisi_sparepart_add_btn"><i class="fa fa-pencil"></i>Add Parts</button>
                            @else
                                <button type="button" class="btn btn-primary btn-sm teknisi_sparepart_add_btn"><i class="fa fa-pencil"></i>item detail</button>
                            @endif
                        </div>
                    </div>
                </form>
            @endif -->

            @if(count($item_details) > 0 || count($tmp_item_detail) > 0)
                <form id="sparepart-detail-form">
                    <input type="hidden" id="sparepart_detail_order_id" value="{{ $order->id }}" name="sparepart_detail_order_id">
                    <input type="hidden" name="type_part" id="type_part" value="my-company">
                    <div class="card">
                        <div class="card-header header-border">
                            Company Inventory
                        </div>
                        <div class="card-body">
                            <div class="dynamic_sparepart_company">
                                <div class="position-relative row form-group">
                                    <div class="col-sm-3">
                                        Warehouse
                                    </div>
                                    <div class="col-sm-4">
                                        Name
                                    </div>
                                    <div class="col-sm-2">
                                        Price
                                    </div>
                                    <div class="col-sm-2">
                                        Unit
                                    </div>
                                </div>
                                @foreach($item_details as $no => $item_detail)
                                    <?php $uniq = time(); ?>
                                    <div class="position-relative row dynamic_sparepart_company_content form-group" id="my_company_row_{{ $uniq }}">
                                        <input type="hidden" value="{{ $item_detail->id }}" name="item_detail_id[]" />
                                        <input type="hidden" class="select-inventory" value="{{ $item_detail->inventory_id }}" name="inventory_id[]" />
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" value="{{ $item_detail->inventory->warehouse->name }}" disabled>
                                            {{-- <label for="">{{ $item_detail->inventory->warehouse->name }}</label> --}}
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="">{{ $item_detail->name_product }} (Available : {{ $item_detail->inventory->stock_available }})</label>
                                            {{-- <textarea name="" id="" cols="38" rows="2" disabled></textarea> --}}
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" data-uniq="{{ $uniq }}" class="form-control inventory-price-{{ $uniq }} inventory-price" value="{{ $item_detail->price }}" name="inventory_price[]" disabled />
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" data-uniq="{{ $uniq }}" data-available="{{ $item_detail->inventory->stock_available }}" placeholder="unit" name="inventory_qty[]" value="{{ $item_detail->quantity }}" class="form-control inventory-unit inventory-unit-{{ $uniq }}" value="0" readonly />
                                        </div>
                                        <div class="col-sm-1" style="padding-left:0px;">
                                            <button data-item_detail_id="{{ $item_detail->id }}" type="button" data-uniq="{{ $uniq }}" class="btn-transition btn btn-sm btn-danger btn_remove_my_company"><i class="fa fa-close"></i></button>
                                        </div>
                                    </div>
                                @endforeach

                                @if(count($tmp_item_detail) > 0)
                                    @foreach($tmp_item_detail as $no => $tmp_item_detail)
                                        <?php $uniq = time(); ?>
                                        <div class="position-relative row dynamic_sparepart_company_content form-group" id="my_company_row_{{ $uniq }}">
                                            <input type="hidden" value="{{ $tmp_item_detail->id }}" name="tmp_item_detail_id[]" />
                                            <input type="hidden" class="select-inventory" value="{{ $tmp_item_detail->inventory_id }}" name="inventory_id[]" />
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" value="{{ $tmp_item_detail->inventory->warehouse->name }}" disabled>
                                                {{-- <label for="">{{ $tmp_item_detail->inventory->warehouse->name }}</label> --}}
                                            </div>
                                            <div class="col-sm-4">
                                                <label for="">{{ $tmp_item_detail->name_product }} (Available : {{ $tmp_item_detail->inventory->stock_available }})</label>
                                                {{-- <textarea name="" id="" cols="38" rows="2" disabled></textarea> --}}
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" data-uniq="{{ $uniq }}" class="form-control inventory-price-{{ $uniq }} inventory-price" value="{{ $tmp_item_detail->price }}" name="inventory_price[]" disabled />
                                            </div>
                                            <div class="col-sm-2">
                                                <input type="text" data-uniq="{{ $uniq }}" data-available="{{ $tmp_item_detail->inventory->stock_available }}" placeholder="unit" name="inventory_qty[]" value="{{ $tmp_item_detail->quantity }}" class="form-control inventory-unit inventory-unit-{{ $uniq }}" value="0" readonly />
                                            </div>
                                            <div class="col-sm-1" style="padding-left:0px;">
                                                <button data-tmp_item_detail_id="{{ $tmp_item_detail->id }}" type="button" data-uniq="{{ $uniq }}" class="btn-transition btn btn-sm btn-danger btn_remove_tmp_my_company"><i class="fa fa-close"></i></button>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Save Change</button>&nbsp;
                            @if(count($item_details) > 0)
                                <button type="button" class="btn btn-primary btn-sm teknisi_inventory_add_btn"><i class="fa fa-pencil"></i>Add Parts</button>
                            @else
                                <button type="button" class="btn btn-primary btn-sm teknisi_inventory_add_btn"><i class="fa fa-pencil"></i>sparepart detail</button>
                            @endif
                        </div>
                    </div>
                </form>
            @endif
        @else
            @if(count($sparepart_details) > 0 || count($item_details) > 0)
                <div class="card header-border">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="text-nowrap table-lg mb-0 table table-hover">
                                <tr>
                                    <td>Name</td>
                                    <td>Unit</td>
                                    <th>Price</th>
                                </tr>
                                <tbody>
                                    @foreach($sparepart_details as $sparepart_detail)
                                    <tr>
                                        <td>
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">{{ $sparepart_detail->name_sparepart }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-left">{{ $sparepart_detail->priceThousandSeparator() }} x {{ $sparepart_detail->quantity }}</td>
                                        <th>
                                            Rp. {{ \App\Helpers\thousanSparator($sparepart_detail->quantity * $sparepart_detail->price) }}
                                        </th>
                                    </tr>
                                    @endforeach
                                    @foreach($item_details as $item)
                                    <tr>
                                        <td>
                                            <div class="widget-content p-0">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">{{ $item->name_product }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-left">{{ $item->price }} x {{ $item->quantity }}</td>
                                        <td>
                                            Rp. {{ \App\Helpers\thousanSparator($item->quantity * $item->price) }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        <!-- media -->
        @include('admin.order._media')
    </div>

    <div class="col-md-4" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Customer
                <div class="btn-actions-pane-right text-capitalize">
                    @if($order->orders_statuses_id == 2)
                        <button type="button" id="reject_job" class="mb-2 mr-2 btn btn-sm btn-danger delete-selected"><i class="fa fa-times"></i> Reject Job</button>
                    @endif
                </div>
            </div>
        </div>
        <div class="our-team">
            <div class="picture">
                <img class="img-fluid" src="{{ $order->user->avatar  }}">
                <!--<img class="img-fluid" src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg">-->
            </div>
            <div class="team-content">
                <h3 class="name">{{ $order->user->full_name  }}</h3>
                <p class="name">{{ $order->user->email }}</p>
                <hr />
                <span class="amount">
                    {{  $order->order_status->name }}
                </span>
                <br />
            </div>
            <!-- <ul class="social">
                {{-- <li><a href="{{ url('/customer/chats?to=' . $teknisi->user->id) }}" target="__blank" class="fa fa-envelope" aria-hidden="true"></a></li> --}}
            </ul> -->
        </div>
        @if($order->rating_and_review != null)
        <div class="card">
            <div class="card-header header-border">
                Review
            </div>
            <div class="card-body">
                @foreach (range(1, $order->rating_and_review->rating->value) as $star)
                    <span class="star"><i class="fa fa-star"></i></span>
                @endforeach
                @if( (5 - $order->rating_and_review->rating->value) > 0 )
                    @foreach (range(1, 5 - $order->rating_and_review->rating->value) as $star)
                        <span class="star"><i class="fa fa-star-o"></i></span>
                    @endforeach
                @endif
               <p>{{ $order->rating_and_review->review->description }}</p>
            </div>
        </div>
        @endif
    </div>



    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-shadow-alternate border mb-3 card card-body border-alternate">
            <h5 class="card-title">History Transaction</h5><br>
            <div class="price scroll-area-sm" style="margin-top:25px; margin-bottom: 30px; height:200px">
                <div class="scrollbar-container ps--active-y ps">
                    <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                        @foreach($order->history_order->reverse() as $key => $history_order)
                        <?php
                             $color = '';
                            $text = '';
                            if($history_order->orders_statuses_id == 1) {
                                $color = 'primary';
                                $text = 'New order has created - ';
                            }
                            elseif($history_order->orders_statuses_id == 2) {
                                $color = 'warning';
                                $text = 'Job has pending, need waiting confirmation at - ';
                            }
                            elseif ($history_order->orders_statuses_id == 3) {
                                $color = 'success';
                                $text = 'Job has approved by '.$order->service_detail[0]->technician->user->name.' at - ';
                            }
                            elseif ($history_order->orders_statuses_id == 4) {
                                $color = 'success';
                                $text = 'Payment successfully paid at - ';
                            }
                            elseif ($history_order->orders_statuses_id == 5) {
                                $color = 'danger';
                                $text = 'Order has canceled at - ';
                            }
                            elseif ($history_order->orders_statuses_id == 6) {
                                $color = 'warning';
                                $text = 'Order has rescheduled at - ';
                            }
                            elseif ($history_order->orders_statuses_id == 7) {
                                $color = 'success';
                                $text = 'Job has done ! at - ';
                            }
                            elseif ($history_order->orders_statuses_id == 9) {
                                $color = 'warning';
                                $text = 'Job Has Been Processing ! at - ';
                            }
                            elseif ($history_order->orders_statuses_id == 10) {
                                $color = 'primary';
                                $text = 'Job Completed ! at - ';
                            }
                        ?>
                        {{-- <div class="vertical-timeline-item vertical-timeline-element">
                            <div><span class="vertical-timeline-element-icon bounce-in"><i class="badge badge-dot badge-dot-xl badge-{{ $color }}"> </i></span>
                                <div class="vertical-timeline-element-content bounce-in">
                                    <h2 class="timeline-title">{{ $history_order->order_status->name }}</h2>
                                    <p>{{ $text }}<b class="text-{{ $color }}">{{ $history_order->created_at }}</b></p>
                                </div>
                            </div>
                        </div> --}}
                        <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                            <div class="vertical-timeline-item vertical-timeline-element">
                                <div>
                                    <span class="vertical-timeline-element-icon bounce-in">
                                        <i class="badge badge-dot badge-dot-xl badge-success"> </i>
                                    </span>
                                    <div class="vertical-timeline-element-content bounce-in">
                                        <h4 class="timeline-title">{{ $history_order->order_status->name }}</h4>
                                        <p>{{ $text }}
                                            <b class="text-{{ $color }}">{{ $history_order->created_at }}</b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-shadow-alternate border mb-3 card card-body border-alternate">
            <h5 class="card-title">MEDIA PROBLEM</h5>
            <br>
            <div class="col-md-12" style="margin-bottom: 15px;">
                <ul>
                    @foreach($order_media_problem as $omp)
                        <li>
                            <a href="{{ url('storage/order_problem/'.$omp->filename.'') }}" target="_blank">
                            @if($omp->type != 'video')
                                <img href="{{ url('storage/order_problem/'.$omp->filename.'') }}" src="{{ url('storage/order_problem/'.$omp->filename.'') }}" width="300" height="250"/>
                            @else
                                <video width="400" controls>
                                    <source src="{{ url('storage/order_problem/'.$omp->filename.'') }}" type="video/{{ $omp->extension }}">
                                    Your browser does not support HTML video.
                                </video>
                            @endif
                            </a>
                        </li>
                    @endforeach
                    </ul>

                <div class="jFiler-items jFiler-row">
                    <ul class="jFiler-items-list jFiler-items-grid">
                        @foreach($order_media_problem as $omp)
                            <li class="jFiler-item jFiler-no-thumbnail" data-jfiler-index="3" style="">
                                <a href="{{ url('storage/order_problem/'.$omp->filename.'') }}" target="_blank">
                                    <div class="jFiler-item-container">
                                        <div class="jFiler-item-inner">
                                            <div class="jFiler-item-thumb">
                                                <div class="jFiler-item-status"></div>
                                                <div class="jFiler-item-thumb-overlay">
                                                    <div class="jFiler-item-info">
                                                        <div style="display:table-cell;vertical-align: middle;">
                                                            <span class="jFiler-item-title">
                                                                <b title="{{ $omp->filename }}"><?php if($omp->type == 'video') { echo '<i class="fa fa-play"></i> <br>'.$omp->type.'.'.$omp->extension.''; } else { echo $omp->filename; } ?></b>
                                                            </span>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="jFiler-item-thumb-image">
                                                    @if($omp->type != 'video')
                                                    <img src="{{ url('storage/order_problem/'.$omp->filename.'') }}" draggable="false">
                                                    @else
                                                        <video width="400" controls>
                                                            <source src="{{ url('storage/order_problem/'.$omp->filename.'') }}" type="video/{{ $omp->extension }}">
                                                            Your browser does not support HTML video.
                                                        </video>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="jFiler-item-assets jFiler-row">
                                                <ul class="list-inline pull-left">
                                                    <li></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div> --}}

{{-- input hidden --}}
<input type="hidden" id="technician_id" value="{{ $technicians_id }}" name="technician_id">
<input type="hidden" id="order_id" value="{{ $order->id }}" name="order_id">
<input type="hidden" id="customer_ewallet" value="{{ $order->user->ewallet == null ? 0 : $order->user->ewallet->nominal }}" name="order_id">
<input type="hidden" id="is_less_balance" value=0 name="is_less_balance">
<input type="hidden" id="wallet_history_id" value='{{ $order->user->ewallet == null ? 0 : $order->user->ewallet->id }}' name="wallet_history_id">
<input type="hidden" name="saldo" id="saldo" value="{{ $walletBalanceCustomer->Total }}">
<input type="hidden" name="get_user" id="get_user" value="{{ $order->user->id }}">

<!-- modal Reshecule -->
<div class="modal fade" data-backdrop="false" id="modal-reschedule" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Reschedule
                </h4>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">

            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <label class="" for="">
                            Schedule
                        </label>
                        <input class="form-control datetimepicker" id="reschedule_input" required="" type="text"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary waves-effect" id="reschedule_submit">
                    RESCHEDULE JOB
                </button>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" data-backdrop="false" id="modal-accepted-job" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Approve Confirmation
                </h4>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="border">
                    <div class="row">
                        <div class="col-md-6" style="margin:15px">
                            <label>Grand Total :</label>
                            <div class="form-group">
                                <h4 id="grand_total_approve"></h4>
                                <h5>Rp. {{ number_format($order->grand_total) }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div style="padding:10px">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Start Working at </label>
                            <div class="form-group">
                                <input type="hidden" value="{{ $order->schedule }}">
                                <h4 id="start_at" hidden>{{ $order->schedule->format('Y m d H:i') }}</h4>
                                <h6>{{ $order->schedule->format('d F Y G:i') }}</h6>
                                <!-- format('d F Y G:i') -->
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label>End Hours at </label>
                            <div class="form-group">
                                <h6 id="end_at"></h6>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label>What is the estimated work time?</label>
                            <div class="row">
                                <div class="input-group col-md-6">
                                    <select class="form-control" name="estimation_hours" id="estimation_hours" required style="width:100%">
                                        <option selected="" value=0>Select Duration</option>
                                        <option value=1>1</option>
                                        <option value=2>2</option>
                                        <option value=3>3</option>
                                        <option value=4>4</option>
                                        <option value=5>5</option>
                                    </select>
                                </div>
                                <br/>
                                <div class="input-group-append col-md-6">
                                    <span class="input-group-text">Hours</span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button class="btn btn-primary waves-effect accept-btn" type="submit">
                    Save
                </button>
                <button class="btn waves-effect" data-dismiss="modal" type="button">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" data-backdrop="false" id="modal-job-done" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Jobs Done</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="">Symptom Code</label>
                <select class="js-example-basic-single" id="ms_symptom_code" name="ms_symptom_code" style="width:100%"></select>
                <strong><span id="error-ms_symptom_code" style="color:red"></span></strong><br>
                <label for="">Repair Code</label>
                <select class="js-example-basic-single" id="ms_repair_code" name="ms_repair_code" style="width:100%"></select>
                <strong><span id="error-ms_repair_code" style="color:red"></span></strong><br>
                <label for="">Desc</label>
                <textarea class="form-control" id="repair_desc" name="repair_desc" rows="3"></textarea>
                <strong><span id="error-repair_desc" style="color:red"></span></strong><br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" id="jobs_done" data-id="{{ $order->id }}" class="btn btn-primary">Success Jobs</button>
                <!-- <button type="submit"  class="mb-2 mr-2 btn btn-sm btn-primary delete-selected" style="float:right">Job Done</button> -->
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
@include('admin.order.service_detail_style')
@include('admin.template._locationServiceDetail')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js">
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" rel="stylesheet" />
{{-- <link rel="stylesheet" href="{{ asset('adminAssets/css/lightslider.min.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/lightgallery.css') }}"> --}}

{{-- <link type="text/css" rel="stylesheet" href="css/lightGallery.css" />
<script src="js/lightGallery.js"></script>

<script type="text/javascript" src="{{ asset('adminAssets/js/lightslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminAssets/js/lightgallery.js') }}"></script> --}}
<link rel="stylesheet" href="{{ asset('adminAssets/css/jquery.filer.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/themes/jquery.filer-dragdropbox-theme.css') }}">
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/jquery.filer.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>

<!-- select2 job done -->
<script>
    globalCRUD
        .select2Static("#ms_symptom_code", '/symptom-code/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })
        .select2Static("#ms_repair_code", '/repair-code/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })
</script>
<script>

var jam_teknisi = '{{$jam_teknisi}}';

$(document).on('change', '#estimation_hours', function(){
    var returned_endate = moment($('#start_at').html()).add($(this).val(), 'hours');
    console.log(returned_endate.format('H'));
    $('#end_at').html(returned_endate.format('HH:00'));
    $('.accept-btn').attr('disabled', false);
    if($.inArray(parseInt(returned_endate.format('H')), JSON.parse(jam_teknisi)) !== -1) {
        $('.accept-btn').attr('disabled', true);
        return Helper.warningNotif("Cannot pick estimate hours. Cause at "+$('#end_at').html()+" in the technician schedule !");
    }
});



$(document).on('click', '#jobs_done', function(e) {
    id = $(this).attr('data-id');
    Helper.confirm(function(){
        Helper.loadingStart();
        $.ajax({
            url:Helper.apiUrl('/teknisi/jobs-done/' + id ),
            type: 'post',
            data: {
                ms_symptom_code: $('#ms_symptom_code').val(),
                ms_repair_code: $('#ms_repair_code').val(),
                repair_desc: $('#repair_desc').val(),
            },
            success: function(res) {
                Helper.successNotif('Success');
                Helper.loadingStop();
                Helper.redirectTo('/teknisi/list-request-job');
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                    })
                }
            }
        })
    },
    {
        title: "Are You Sure",
        message: "Are you really sure this work has been completed ?",
    })

    e.preventDefault()
})

$(document).on('click', '#on_working', function(e) {
    id = $(this).attr('data-id');
    Helper.confirm(function(){
        Helper.loadingStart();
        $.ajax({
            url:Helper.apiUrl('/teknisi/on-working/' + id ),
            type: 'post',

            success: function(res) {
                Helper.successNotif('Success');
                Helper.loadingStop();
                Helper.redirectTo('/teknisi/list-request-job');
            },
            error: function(res) {
                console.log(res);
                Helper.loadingStop();
            }
        })
    })

    e.preventDefault()
})

var order_id = $('#order_id').val();


Helper.date('.datetimepicker',1);
Helper.noNegative();

$(document).on('click', '#reschedule', function(){
    $('#modal-reschedule').modal('show');
})

$(document).on('click', '#reschedule_submit', function(){
    var reschedule = $('#reschedule_input').val();
    $('#reschedule_display').show();
    $('#reschedule_text').html(reschedule).show();
    $('#modal-reschedule').modal('hide');
})

$(document).on('click', '#reject_job', function(){
    Helper.confirm( ()=> {
        $.ajax({
            url: Helper.apiUrl('/teknisi/reject-job/' + $('#order_id').val()),
            type: 'post',
            data: {
                order_id: $('#order_id').val()
            },
            success: function(response){
                location.reload()
                Helper.successNotif('Accepted Job Berhasil di Submit !');
                window.location.href = Helper.redirectUrl('/teknisi/list-request-job');
            }
        });
    });
})
var selected = [{}];
    $(document).ready(function(){
        var i=1;
        $('#add').click(function(){
            i++;
            $('.dynamic_sparepart').append((
                `<div class="position-relative row form-group" id="row_${i}">
                    <div class="col-sm-6">
                        <input type="text" min=1 id="name_sparepart_${i}" data-uniq="${i}" placeholder="Name Sparepart" name="name_sparepart[]" class="form-control-sm form-control mb-3 part-name part-name-${i}">
                    </div>
                    <div class="col-sm-3">
                        <input type="number" min=1 id="price_sparepart_${i}" data-uniq="${i}" placeholder="Price" name="price_sparepart[]" class="form-control-sm form-control mb-3 part-price part-price-${i}">
                    </div>
                    <div class="col-sm-2">
                        <input type="number" min=1 id="qty_sparepart_${i}" data-uniq="${i}" placeholder="unit" name="qty_sparepart[]" value="1" class="form-control-sm form-control mb-3 part-unit part-unit-${i}">
                    </div>
                    <div class="col-sm-1" style="padding-left:0px;">
                        <button name="remove" data-uniq="${i}" class="btn-transition btn btn-sm btn-danger btn_remove"><i class="fa fa-minus"></i></button>
                    </div>
                </div>`)
            );
            Helper.noNegative();
        });
    });

    $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("data-uniq");
        $('#row_'+button_id+'').remove();

        // reload extra cost
        pushCost();
    });

    var _MY_INVENTORY = [];
    $('#grand-total-text').text(Helper.toCurrency($('#total_service').val()));
    $('#grand_total').val($('#total_service').val());
    // event link tab klik
    $('.btn-tab').click(function(){

        $('#extra-cost-area').html('');
        tab = $(this).attr('data-desc');
        if (tab == 'sparepaer-inventory') {
            $('#type_part').val('my-inventory');
            pushCost();
        }else{
            $('#type_part').val('my-company');
            pushCostInventoryCompany();
        }
    })
    // even accept-btn di klik
    $('.accept-btn').click(function(){
        Helper.loadingStart();
        data = {};
        data.inventory_id = $('.select-inventory').map(function(){return $(this).val();}).get();
        data.type = $('#type_part').val();
        data.name = $('.part-name').map(function(){return $(this).val();}).get();
        data.price = $('.part-price').map(function(){return $(this).val();}).get();
        data.unit = $('.part-unit').map(function(){return $(this).val();}).get();
        data.name_inventory = $('.name_sparepart_inventory').map(function(){return $(this).val();}).get();
        data.price_inventory = $('.price_sparepart_inventory').map(function(){return $(this).val();}).get();
        data.qty_inventory = $('.inventory-unit').map(function(){return $(this).val();}).get();
        data.reschedule = $('#reschedule_input').val();
        data.grand_total = $('#grand_total').val();
        data.total_part = $('#total_part').val();
        data.unit_services = $('.unit_services').map(function(){return $(this).val();}).get();
        data.technician_id = $('#technician_id').val();
        data.order_id = $('#order_id').val();
        data.estimation_hours = $('#estimation_hours').val();
        data.end_at = $('#end_at').html();
        data.is_less_balance = $("#is_less_balance").val();
        data.wallet_history_id = $("#wallet_history_id").val();
        data.saldo = $("#saldo").val();
        data.get_user = $("#get_user").val();
        // console.log(data);

        $.ajax({
            url : Helper.apiUrl('/teknisi/accepted-job/'+order_id),
            type: 'post',
            data: data,
            success: function(response){
                console.log(response);
                Helper.successNotif('Accepted Job Berhasil di Submit !');
                Helper.loadingStop();
                window.location.href = Helper.redirectUrl('/teknisi/list-request-job');
            }
        });
    })

    // event input unit di ganti
    $(document).on('keyup', '.part-price', function(){
        uniq = $(this).attr('data-uniq');
        sparepart = $('.part-name-' + uniq).val();
        if (sparepart != '') {
            pushCost();
        }
    });

    // event input harga di ganti
    $(document).on('keyup', '.part-unit', function(){
        uniq = $(this).attr('data-uniq');
        sparepart = $('.part-name-' + uniq).val();
        if (sparepart != '') {
            pushCost();
        }
    });

    // even select part di ganti
    $(document).on('keyup', '.part-name', function(){
        pushCost();
    });

    function pushCost(){
        template = '';
        grand_total = 0;
        total_part = 0 ;
        var part = {};
        $('.part-name').each(function(){
            var value = $(this).val();
            var uniq = $(this).attr('data-uniq');

            if (value != '') {
                part.name = value;
                part.price = ($('.part-price-' + uniq).val() != '') ? $('.part-price-' + uniq).val() : 0;
                part.unit = ($('.part-unit-' + uniq).val() != '') ? $('.part-unit-' + uniq).val() : 0;
                total_part += parseInt(part.unit) * parseInt(part.price);
                grand_total += parseInt($('#total_service').val()) + parseInt(part.unit) * parseInt(part.price);
                template += templateExtraCost(part);
            }
        })

        $('#grand-total-text').text(Helper.toCurrency(grand_total));
        $('#grand_total').val(grand_total);
        $('#total_part').val(total_part);

        $('#extra-cost-area').html(template);
    }

    function templateExtraCost(sparepart){
        var template = '';

        template += '<tr>';
            template += '<th>'+sparepart.name+'</th>';
        template += '</tr>';

        template += '<tr>';
            template += '<th>'+sparepart.unit+'</th>';
            template += '<th class="text-right">'+Helper.toCurrency(sparepart.price)+'</th>';
        template += '</tr>';
        return template;
    }
</script>


<script>
    // add
    $(document).ready(function(){
        var xx = 1;
        $('#add-inventory').click(function(){
            xx++;
            $('.dynamic_sparepart_company').append((
                `<div class="position-relative row form-group" id="inventory_row_${xx}">
                    <div class="col-sm-7">
                        <select class="select-inventory select-inventory-${xx}" id="inventory_id_${xx}" data-uniq="${xx}" name="inventory_id[]" style="width:100%">
                            <option value="" selected="selected">--Pilih--</option>'+
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" min=1 id="qty_inventory_${xx}" data-uniq="${xx}" placeholder="unit" name="qty_inventory[]" value="1" class="form-control inventory-unit inventory-unit-${xx}">
                    </div>
                    <div class="col-sm-2" style="padding-left:0px;">
                        <button name="remove" data-uniq="${xx}" class="btn-transition btn btn-sm btn-danger btn_remove_inventory"><i class="fa fa-minus"></i></button>
                    </div>
                </div>`)
            );
            Helper.noNegative();
            var newSelect = $(".dynamic_sparepart_company").find(".select-inventory").last();
            // initializeSelect2InventoryCompany(newSelect);
        });
    });

    // delete
    $(document).on('click', '.btn_remove_inventory', function(){
        var button_id = $(this).attr("data-uniq");
        $('#inventory_row_'+button_id+'').remove();
    });

    // even input unit di ganti
    $(document).on('change', '.inventory-unit', function(){
        uniq = $(this).attr('data-uniq');
        unit_val = $(this).val();

        item = $('.select-inventory-' + uniq).val();
        if (item != '') {

            inventories = $('.select-inventory-' + uniq).select2("data");
            inventory = _.find(inventories, function(inv) { return inv.id == value; });

            if (parseInt(unit_val) <= parseInt(inventory.data.stock)) {
                pushCostInventoryCompany();
            }else{
                $(this).val(1);
            }
        }
    });

    // even select part di ganti
    $(document).on('change', '.select-inventory', function(){
        id = $(this).val();

        valid = validasiSelectInventoryCompany(id);
        if (valid == true) {
            pushCostInventoryCompany();
        }else{
            $(this).val('').change();
        }
    });


    $(".select-inventory").each(function() {
        // initializeSelect2InventoryCompany($(this));
    });

    // function initializeSelect2InventoryCompany(selectElementObj) {
    //     selectElementObj.select2({
    //         ajax: {
    //             url: Helper.apiUrl('/teknisi/inventory-company/select2'),
    //             // url: Helper.apiUrl('/technician_sparepart/select2'),
    //             dataType: 'json',
    //             delay: 250,
    //             data: function (params) {
    //                 return {
    //                     q: params.term,
    //                     page: params.page
    //                 };
    //             },
    //             processResults: function (data, page) {
    //                 return {
    //                     results: $.map(data.data, displayItemInventory)
    //                 };
    //             },
    //             cache: true
    //         },
    //         data: $.map(selected, displayItemInventory),
    //         escapeMarkup: function (markup) { return markup; },
    //         minimumInputLength: 1,
    //         templateSelection: formatRepoSelection
    //     });
    // }

    function formatRepoSelection(data) {
      return data.name || data.text;
    }

    function displayItemInventory(data) {
        console.log('length', data.length);
        console.log('data', data);
        if (data.batch_item) {
            console.log('data', data);
            var images = "<img src='"+Helper.url('/adminAssets/image/no-image.png')+"' />";
            return {
                id : data.id,
                text :  "<div class='select2-result-repository clearfix'>" +
                        // "<div class='select2-result-repository__avatar'>"+images+"</div>" +
                        "<div class='select2-result-repository__meta'>" +
                        "<div class='select2-result-repository__title'>" + data.batch_item.product_name + "</div>"+
                        "<div class='select2-result-repository__description'>" + data.warehouse.name + "</div>"+
                        "<div class='select2-result-repository__description'>" + data.stock + " Stock</div>"+
                        "<div class='select2-result-repository__statistics'>" +
                        "<div class='select2-result-repository__forks'><i class='fa fa-fa-money'></i><b> Rp. " + Helper.toCurrency(data.cogs_value) + "</b></div>" +
                        "</div></div>",
                name: data.batch_item.product_name + "(Qty: " + data.stock + ")",
                data: data
            };
        }

        return {
            id : '',
            text :  "",
            name: '',
            data: []
        };
    }


    function validasiSelectInventoryCompany(id){
        var jumlah = 0;
        $('.select-inventory').each(function(){
            value = $(this).val();

            if (id == value) {
                jumlah++;
            }

            if (jumlah == 2) {
                return;
            }
        })

        valid = jumlah >= 2 ? false : true;

        return valid;
    }

    function pushCostInventoryCompany(){
        template = '';
        grand_total = 0;
        $('.select-inventory').each(function(){
            value = $(this).val();
            uniq = $(this).attr('data-uniq');

            if (value != '') {
                inventories = $('.select-inventory-' + uniq).select2("data");
                inventory = _.find(inventories, function(inv) { return inv.id == value; });
                if (inventory) {
                    unit = $('.inventory-unit-' + uniq).val();
                    inventory.unit = unit;
                    grand_total += parseInt($('#total_service').val()) + parseInt(unit) * parseInt(inventory.data.cogs_value);
                    template += templateExtraCostInventoryCompany(inventory);
                }
            }
        })

        $('#grand-total-text').text(Helper.toCurrency(grand_total));
        $('#grand_total').val(grand_total);
        $('#extra-cost-area').html(template);
    }

    function templateExtraCostInventoryCompany(inventory){

        var template = '';

        template += '<tr>';
            template += '<th>'+inventory.data.item_name+'</th>';
        template += '</tr>';

        template += '<tr>';
            template += '<th>'+inventory.unit+ ' ' + inventory.data.unit_type.unit_name +'</th>';
            template += '<th class="text-right">'+Helper.toCurrency(inventory.data.cogs_value)+'</th>';
        template += '</tr>';
        template += "<input type='text' name='name_sparepart_inventory[]' class='name_sparepart_inventory' value="+inventory.data.item_name+">";
        template += "<input type='text' name='price_sparepart_inventory[]' class='price_sparepart_inventory' value="+inventory.data.cogs_value+">";
        return template;
    }

    $(document).on('click', '.accept-modal', function(){
        $('#modal-accepted-job').modal('show');
        $('#start_at').html($('#working_at').html());
        console.log($('#customer_ewallet').val()+' < '+$('#grand_total').val())
        if(parseInt($('#customer_ewallet').val()) < parseInt($('#grand_total').val())) {
            console.log('asdasd');
            $('#grand_total_approve').html(Helper.toCurrency($('#grand_total').val()));
            $('#is_less_balance').val(1);
        }

    });

</script>

<!-- sparepart -->
<script>
    SparepartClass = {
        create: function(xx) {
            $('.dynamic_sparepart').append((`
                <input type="hidden" value="0" name="sparepart_detail_id[]">
                <input type="hidden" value="0" name="tmp_sparepart_id[]" />
                <div class="position-relative row form-group" id="my_part_row_${xx}">
                    <div class="col-sm-6">
                        <input type="text" placeholder="name" name="teknisi_sparepart_name[]" data-uniq="${xx}" class="form-control teknisi_sparepart_name_input-${xx}" />
                    </div>
                    <div class="col-sm-2">
                        <input type="text" placeholder="price" name="teknisi_sparepart_price[]" data-uniq="${xx}" class="teknisi_sparepart_price_i teknisi_sparepart_price_input-${xx} form-control" />
                    </div>
                    <div class="col-sm-2">
                        <input type="text" placeholder="unit" name="teknisi_sparepart_qty[]" data-uniq="${xx}" class="teknisi_sparepart_qty_i teknisi_sparepart_qty_input-${xx} form-control" />
                    </div>
                    <div class="col-sm-2" style="padding-left:0px;">
                        <button type="button" data-sparepart_detail_id="0" data-uniq="${xx}" class="btn-transition btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            `));

            Helper.onlyNumberInput('.teknisi_sparepart_qty_i')
                .onlyNumberInput('.teknisi_sparepart_price_i');
        },
        delete: function($el) {
            var uniq = $el.attr("data-uniq");
            var sparepart_detail_id = parseInt($el.attr("data-sparepart_detail_id"));

            if (sparepart_detail_id != 0) {
                Helper.confirm(function() {
                    Axios.delete('/teknisi/destroy_sparepart_detail/' + sparepart_detail_id)
                        .then(function(response) {
                            Helper.successNotif('Success Delete');
                            $('#my_part_row_' + uniq + '').remove();
                            $('#grand_total_order').text(response.data.grand_total);
                            if ($('.dynamic_sparepart_content').length == 0) {
                                SparepartClass.create(response.data.uniq);
                            }
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
            } else {
                $('#my_part_row_' + uniq + '').remove();
            }
        },
        tmp_delete: function($el) {
            var uniq = $el.attr("data-uniq");
            var sparepart_detail_id = parseInt($el.attr("data-tmp_sparepart_id"));

            if (sparepart_detail_id != 0) {
                Helper.confirm(function() {
                    Axios.delete('/teknisi/destroy_tmp_sparepart_detail/' + sparepart_detail_id)
                        .then(function(response) {
                            Helper.successNotif('Success Delete');
                            $('#my_part_row_' + uniq + '').remove();
                            if ($('.dynamic_sparepart_content').length == 0) {
                                SparepartClass.create(response.data.uniq);
                            }
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
            } else {
                $('#my_part_row_' + uniq + '').remove();
            }
        },
        update: function() {

        }
    };

    // event link tab klik
    $('.btn-tab').click(function() {
        $('#extra-cost-area').html('');
        tab = $(this).attr('data-desc');
        if (tab == 'sparepaer-inventory') {
            $('#type_part').val('my-inventory');
        } else {
            $('#type_part').val('my-company');
        }
    });

    $(function() {
        var xx = 1;

        Helper.onlyNumberInput('.teknisi_sparepart_qty_i')
            .onlyNumberInput('.teknisi_sparepart_price_i');

        $('#sparepart-detail-form')
            .submit(function(e) {
                var data = Helper.serializeForm($(this));
                Axios.put('/teknisi/update_sparepart_detail_before_accept/' + data.sparepart_detail_order_id, data)
                    .then(function(response) {
                        Helper.successNotif('Success Updated');
                        location.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
                e.preventDefault();
            });

        // add row part my inv
        $(document)
            .on('focus', 'input[name="teknisi_sparepart_name[]"], input[name="teknisi_sparepart_qty[]"]', function(e) {
                if ($(this).is('input[name="teknisi_sparepart_name[]"]:last')) {
                    SparepartClass.create(new Date().getTime() + xx)
                    xx++;
                }

                if ($(this).is('input[name="teknisi_sparepart_qty[]"]:last')) {
                    SparepartClass.create(new Date().getTime() + xx)
                    xx++;
                }
            })

        $(document).on('click', '.teknisi_sparepart_add_btn', function(e) {
                    SparepartClass.create(new Date().getTime() + xx)
                    xx++;
            })

        // delete part my inv
        $(document)
            .on('click', '.btn_remove_my_part', function() {
                SparepartClass.delete($(this));
            });

        $(document)
            .on('click', '.btn_remove_my_tmp_part', function() {
                SparepartClass.tmp_delete($(this));
            });
    })
</script>

<!-- part company -->
<script>
    var inventoryCompanySelect = {
        initialize: function() {
            Helper.onlyNumberInput('.inventory-unit');
            $('select[name="inventory_id[]"]').prop('disabled', true);
            $('.inventory-unit').prop('disabled', false);
            globalCRUD.select2('.select-warehouse', '/warehouses/select2');

            $(document).on('change', '.select-inventory', function() {
                inventoryCompanySelect.handleOnChange($(this));
            });

            $(document).on('change', '.select-warehouse', function() {
                uniq = $(this).attr('data-uniq')
                inventoryCompanySelect.make('.select-inventory-' + uniq, $(this).val());
                $('.select-inventory-' + uniq).prop('disabled', false);
            });
        },

        make: function(selector, warehouse_id) {
            inventory_id = [];
            $('.select-inventory').each(function() {
                if ($(this).val() != '') {
                    inventory_id.push($(this).val());
                }
            }).get();

            $(selector).select2({
                ajax: {
                    url: Helper.apiUrl('/teknisi/inventory-company/select2'),
                    dataType: 'json',
                    delay: 250,
                    type: 'post',
                    data: function(params) {
                        return {
                            inventory_id: inventory_id,
                            warehouse_id: warehouse_id,
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: $.map(data.data, inventoryCompanySelect.render(data))
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateSelection: function(data) {
                    return data.name || data.text;
                }
            });
        },

        data: function(selector = null) {
            selector = selector == null ? '.select-inventory' : selector;
            return $(selector).select2("data");
        },

        find: function(inv_id, selector = null) {
            return _.find(inventoryCompanySelect.data(selector), function(data) {
                return data.id == inv_id;
            });
        },

        render: function(data) {
            return function(data) {
                console.log(data);
                if (data.product) {
                    var images = "<img src='" + data.product.main_image + "' />";
                    return {
                        id: data.id,
                        text: "<div class='select2-result-repository clearfix'>" +
                            // "<div class='select2-result-repository__avatar'>" + images + "</div>" +
                            "<div class='select2-result-repository__meta'>" +
                            "<div class='select2-result-repository__title'>" + data.item_name + "</div>" +
                            "<div class='select2-result-repository__description'>" + data.warehouse.name + "</div>" +
                            "<div class='select2-result-repository__description'>" + Helper.thousandsSeparators(data.stock_available) + " Stock Available</div>" +
                            "<div class='select2-result-repository__statistics'>" +
                            "<div class='select2-result-repository__forks'><i class='fa fa-fa-money'></i><b> Rp. " + Helper.thousandsSeparators(data.cogs_value) + "</b></div>" +
                            "</div></div>",
                        name: data.item_name + "(Qty: " + Helper.thousandsSeparators(data.stock_available) + ")",
                        data: data
                    };
                }

                return {
                    id: '',
                    text: "",
                    name: '',
                    data: []
                };
            };
        },

        handleOnChange: function($el) {
            uniq = $el.attr('data-uniq');
            inventory = this.find($el.val(), $('.select-inventory-' + uniq));
            $('.inventory-price-' + uniq).val(Helper.thousandsSeparators(inventory.data.cogs_value));
            $('.inventory-unit-' + uniq).prop('disabled', false);
        },

        templateNewInputPartMyCompany: function(xx) {
            $('.dynamic_sparepart_company').append((`
                <div class="position-relative row form-group" id="my_company_row_${xx}">
                    <input type="hidden" value="0" name="item_detail_id[]">
                    <div class="col-sm-3">
                        <select class="form-control select-warehouse select-warehouse-${xx}" name="warehouse_id[]" data-uniq="${xx}" style="width:100%">
                            <option value="" selected="selected">--Pilih--</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <select class="form-control select-inventory select-inventory-${xx}" id="inventory_id_${xx}" data-uniq="${xx}" name="inventory_id[]" style="width:100%">
                            <option value="" selected="selected">--Pilih--</option>'+
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" data-uniq="${xx}" class="form-control inventory-price-${xx}" name="inventory_price[]" disabled/>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" id="qty_inventory_${xx}" data-uniq="${xx}" placeholder="unit" name="inventory_qty[]" value="0" class="form-control inventory-unit inventory-unit-${xx}">
                    </div>
                    <div class="col-sm-1" style="padding-left:0px;">
                        <button data-item_detail_id="0" type="button" data-uniq="${xx}" class="btn-transition btn btn-sm btn-danger btn_remove_my_company"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            `));

            Helper.onlyNumberInput('.inventory-unit');
        },

        delete: function($el) {
            var uniq = $el.attr("data-uniq");
            var item_detail = parseInt($el.attr("data-item_detail_id"));

            if (item_detail != 0) {
                Helper.confirm(function() {
                    Axios.delete('/order/destroy_item_detail/' + item_detail)
                        .then(function(response) {
                            Helper.successNotif('Success Delete');
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
            } else {
                $('#my_company_row_' + uniq + '').remove();
            }
        },

        deleteTmpItemDetail: function($el) {
            var uniq = $el.attr("data-uniq");
            var item_detail = parseInt($el.attr("data-tmp_item_detail_id"));

            if (item_detail != 0) {
                Helper.confirm(function() {
                    Axios.delete('/teknisi/destroy_tmp_item_detail/' + item_detail)
                        .then(function(response) {
                            Helper.successNotif('Success Delete');
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
            } else {
                $('#my_company_row_' + uniq + '').remove();
            }
        },
    }

    inventoryCompanySelect.initialize();

    $(function() {
        var yy = 1;

        // add row part my inv
        $(document)
            .on('focus', 'input[name="inventory_qty[]"]', function(e) {
                console.log(($(this).is('input[name="inventory_qty[]"]:last')));
                if ($(this).is('input[name="inventory_qty[]"]:last')) {
                    uniq = new Date().getTime() + yy;
                    inventoryCompanySelect.templateNewInputPartMyCompany(uniq)
                    globalCRUD.select2('.select-warehouse-' + uniq, '/warehouses/select2');
                    $('.select-inventory-' + uniq).prop('disabled', true);
                    $('.inventory-unit-' + uniq).prop('disabled', true);
                    yy++;
                }
            })

            $(document).on('click', '.teknisi_inventory_add_btn', function(e) {
                    uniq = new Date().getTime() + yy;
                    inventoryCompanySelect.templateNewInputPartMyCompany(uniq)
                    globalCRUD.select2('.select-warehouse-' + uniq, '/warehouses/select2');
                    $('.select-inventory-' + uniq).prop('disabled', true);
                    $('.inventory-unit-' + uniq).prop('disabled', true);
                    yy++;
            })

        // delete part my inv
        $(document)
            .on('click', '.btn_remove_my_company', function() {
                inventoryCompanySelect.delete($(this))
            });

        // event input unit di ganti
        $(document)
            .on('keyup', 'input[name="inventory_qty[]"]', function() {
                stock_available = $(this).attr('data-available');
                if (stock_available) {
                    stock_available = parseInt(stock_available);
                } else {
                    uniq = $(this).attr("data-uniq");
                    inventory = inventoryCompanySelect.find($('.select-inventory-' + uniq).val(), $('.select-inventory-' + uniq));
                    stock_available = parseInt(inventory.data.stock_available);
                }

                qty = parseInt($(this).val());
                console.log('cek stock', stock_available, qty, (qty >= stock_available))
                if (qty > stock_available) {
                    Helper.errorNotif('Melebihi Qty yang tersedia');
                    $(this).val(0);
                }
            });

        //delete parts di tmp item detail
        $(document)
            .on('click', '.btn_remove_tmp_my_company', function() {
                inventoryCompanySelect.deleteTmpItemDetail($(this));
            });
    })
</script>
@endsection
