<!doctype html>
<html lang="en">
@include('admin.template._header')

<body>
    <style>
        .ui-theme-settings .btn-open-opt {
            border-radius: 50px;
            position: absolute;
            left: -114px;
            bottom: 80px;
            padding: 0;
            height: 54px;
            line-height: 54px;
            width: 54px;
            text-align: center;
            display: block;
            box-shadow: 0 0.46875rem 2.1875rem rgba(4,9,20,0.03), 0 0.9375rem 1.40625rem rgba(4,9,20,0.03), 0 0.25rem 0.53125rem rgba(4,9,20,0.05), 0 0.125rem 0.1875rem rgba(4,9,20,0.03);
            margin-top: -27px;
        }

        .alert-danger .btn-danger {
            float: right;
        }

        .alert-danger span {
            line-height: 34px;
        }

        .alert-danger > div:after {
            clear: both;
            content: '';
            display: table;
        }
    </style>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        @include('admin.template._navbar')
        
        @if((Auth::user()->hasRole('Customer') || Auth::user()->hasRole('Technician')) && Request::segment(2) != 'chats')
            <div class="ui-theme-settings" style="right: 450px; bottom: 10px;">
                <a href="{{ Auth::user()->hasRole('Technician') ? route('teknisi.chat') : route('customer.chat') }}" class="btn-open-opt btn btn-warning btn-icon btn-icon-only btn btn-link btn-sm">
                    <i style="padding: 15px;" class="fa fa-envelope btn-icon-wrapper font-size-xlg"> </i>
                    <span class="badge badge-pill badge-success count-notif">{{ $chat_count_notif }}</span>
                </a>
            </div>
        @endif
        <div class="app-main">
            @if (Auth::user()->status != 6 && Auth::user()->status != 7 && Auth::user()->status != 8)
                @include('admin.template._sidebar')
            @endif


            @if (Auth::user()->status != 6 && Auth::user()->status != 7 && Auth::user()->status != 8)
                <div class="app-main__outer">
            @endif
                    <div class="app-main__inner">
                        <div class="row">
                            <div class="col-md-12">
                                @if (Auth::user()->status == 6)
                                    <div class="alert alert-warning alert-regis" role="alert">
                                        <center>
                                            <i class="fa fa-bell-o"></i> <strong>One more step! Complete your registration</strong>
                                        </center>
                                    </div>
                                @endif

                                @if (Auth::user()->status == 7)
                                    <div class="alert alert-success alert-regis" role="alert">
                                        <center>
                                            <i class="fa fa-bell-o"></i> 
                                            <strong>your application is in the process of verifying our admin</strong>
                                        </center>
                                    </div>
                                @endif

                                @if (Auth::user()->status == 8)
                                    <div class="alert alert-danger alert-regis" role="alert">
                                        <center>
                                            <i class="fa fa-bell-o"></i> 
                                            <strong>Your application does not meet the requirements</strong><br>
                                            <small>Dengan Alasan : {{ Auth::user()->note_reject }}</small>
                                        </center>
                                    </div>
                                @endif
                                
                                {{-- <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        @foreach ($breadcrumbs as $item)
                                        <li class="breadcrumb-item">
                                            <a href="{{ $item['link'] }}"> {{ $item['name'] }} </a>
                                        </li>
                                        @endforeach
                                    </ol>
                                </nav> --}}
                            </div>
                        </div>
                        <div class="row">
                            @yield('content')
                        </div>
                    </div>
            @if (Auth::user()->status != 6 && Auth::user()->status != 7 && Auth::user()->status != 8)
                </div>
            @endif
        </div>
    </div>
    @include('admin.template._mainScript')
    @yield('script')
</body>

</html>