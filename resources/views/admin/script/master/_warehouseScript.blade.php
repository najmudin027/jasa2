<script>
    $(document).ready(function() {
        if($("#city").val() == null) {
            $("#cityShow").hide('');
        }
        if ($("#distric").val() == null) {
            $("#districtShow").hide('');
        }
        if ($("#vilage").val() == null) {
            $("#villageShow").hide('');
        }
        if ($("#zipcode").val() == null) {
            $("#zipcodeShow").hide('');
        }

        $("#city").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/city/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });

        $("#city").change(function() {
            $("#distric").html('')
            $("#districtShow").show('')
            $("#villageShow").hide('')
            $("#zipcodeShow").hide('')
            districSelect($(this).val())
        })
        districSelect($("#city").val())

        $("#distric").change(function() {
            $("#vilage").html('')
            $("#villageShow").show('')
            $("#zipcodeShow").hide('')
            vilageSelect($(this).val());
            zipcodeSelect($(this).val());
        })
        vilageSelect($("#distric").val());

        $("#vilage").change(function() {
            $("#zipcode").html('');
            $("#zipcodeShow").show('')

        })
        zipcodeSelect($("#distric").val());
    });

    function districSelect(id) {
        $("#distric").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/address_search/find_district?city_id=' + id),
                // url: 'http://localhost:8001/api/address_search/find_district?city_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }

    function vilageSelect(id) {
        $("#vilage").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/address_search/find_village?district_id=' + id),
                // url: 'http://localhost:8001/api/address_search/find_village?district_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }

    function zipcodeSelect(id) {
        $("#zipcode").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/address_search/find_zipcode?district_id=' + id),
                // url: 'http://localhost:8001/api/address_search/find_village?district_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.zip_no,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }
</script>
