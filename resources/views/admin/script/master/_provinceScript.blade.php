<script>
     $('#save').click(function(e){
        $.ajax({
            type:'post',
            data:{
                name : $('input[name="name"]').val(),
                ms_country_id : $('select[name="ms_country_id"]').val(),
            },

            url:Helper.apiUrl('/provinces'),
            success:function(html){
                console.log(html)
                // simpan ke
                // redirect
                swal.fire({
                    type: 'success',
                    title: 'Province Has Been Saved',
                    icon: "success",
                });
                window.location.href = Helper.redirectUrl('/show-province');
            },
            error:function(xhr, status, error){
                pesan = xhr.responseJSON.msg;
                Helper.msgNotif(pesan, 'error');
            },
        });
        e.preventDefault();
    })

    //import excel
    $("#import").click(function() {
        var fd = new FormData();
        var files = $('#file')[0].files[0];
        if(files){
            fd.append('file', files);
        }
        $.ajax({
            url:Helper.apiUrl('/provinces/importExcel'),
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    swal.fire({
                        type: 'success',
                        title: 'File Has Been Imported',
                        icon: "success",
                    });
                    window.location.href = Helper.redirectUrl('/show-province');
                } else {
                    swal.fire({
                        type: 'error',
                        title: 'File Not Uploaded',
                        icon: "error",
                    });
                }
            },
        });
    });

    // update
    $('#update').click(function(e){
        id = $('input[name="id"]').val()
        $.ajax({
            type:'put',
            data:{
                id : $('input[name="id"]').val(),
                name : $('input[name="name"]').val(),
                ms_country_id : $('select[name="ms_country_id"]').val(),
            },

            url:Helper.apiUrl('/provinces/' + id),
            success:function(html){
                swal.fire({
                    type: 'success',
                    title: 'Provinces Has Been Updated',
                    icon: "success",
                });
                console.log(html)
                window.location.href = Helper.redirectUrl('/show-province');
            },
            error:function(xhr, status, error){
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+field).text(pesan[0])
                    })
                }
            },
        });
        e.preventDefault();
    })

    $(".js-example-basic-single").select2({
        ajax: {
            type: "GET",
            url:Helper.apiUrl('/countries/select2'),
            data: function (params) {
                return {
                    q: params.term
                };
            },
            processResults: function (data) {
                var res = $.map(data.data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                });
                return {
                    results: res
                };
            }
        }
    });

    $('.delete-selected-prov').click(function() {
        var ids = $.map(table.rows('.selected').data(), function(item) {
            return item.id;
        });
        deleteSelectedProv(ids);
        console.log(ids)
    });

    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: {
            style: 'multi',
        },
        columnDefs: [{
            orderable: false,
            defaultContent: '',
            className: 'select-checkbox',
            targets: 0,
            searchable: false,
        }],
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url:Helper.apiUrl('/provinces/datatables'),
            "type": "get",
            "data": function ( d ) {
                return $.extend( {}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },

        columns: [{
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    return '';
                }
            },
            {
                data : "name",
                name: "name",
            },
            {
                data : "country.name",
                name: "ms_country_id",
            },
            {
                data : "id",
                name : "id",
                render : function(data, type, full) {
                    return "<a href=''  data-id='"+full.id+"' class='update btn btn-warning btn-white btn-sm'><i class='fa fa-pencil'></i> Update</a> <a href='#' data-id='"+full.id+"'  class='delete btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>"
                }
            },
        ],
    });

    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deleteCategories(id)
        e.preventDefault()
    })

    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        viewCategories(id)
        e.preventDefault()
    })

    function viewCategories(id) {
        window.location.href = Helper.redirectUrl('/update-province/' + id )
    }

    function deleteCategories(id) {
        console.log('id', id)
        if(confirm("Are you sure to delete this Province ? Your action can not be reversed")) {
            $.ajax({
                url:Helper.apiUrl('/provinces/' + id ),
                type: 'delete',
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    swal.fire({
                    type: 'success',
                    title: 'Province Has Been Deleted',
                    icon: "success",
                });
                    table.ajax.reload();

                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }

    function deleteSelectedProv(ids) {
        if(confirm("Are you sure to delete this Province ? Your action can not be reversed")) {
            $.ajax({
                url: Helper.apiUrl('/provinces/destroy_multiple/'),
                type: 'delete',
                data: {
                    id: ids
                },
                success: function(res) {
                    table.ajax.reload();
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }
</script>
