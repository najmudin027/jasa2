<script>

    $(document).ready(function() {
        $('#import').attr('disabled', true);
        $('.msg').hide();
		$('.progress').show();
        $('#file').on('change',function(){
            var size = 10; //in MB
            var maxSize = 1024 * (1024*size);
            if(this.files[0].size > maxSize){
                $('#import').attr('disabled', true);
                Helper.errorNotif('Error. File size may not exceed '+ size +' MB');

            }
            else{
                $('#import').attr('disabled', false);
            }
            var ext = this.value.match(/\.(.+)$/)[1];
            switch (ext) {
                case 'xlsx':
                    $('#import').attr('disabled', false);
                    break;
                default:
                $('#import').attr('disabled', true);
                Helper.errorNotif('Extension must xlsx !.');

            }

        });
    });

     $("#import").click(function() {
        Helper.loadingStart();
        var fd = new FormData();
        var files = $('#file')[0].files[0];
        var type_import = $('#type_import').val();
        if(files){
            fd.append('file', files);
            fd.append('type_import', type_import);
        }
        $.ajax({
            xhr : function() {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener('progress', function(e){
					if(e.lengthComputable){
						var percent = Math.round((e.loaded / e.total) * 100);
						$('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
					}
				});
				return xhr;
			},
            url:Helper.apiUrl('/import-excel'),
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            success: function(data) {
                console.log(data)

                Helper.hideSidebar();
                data = JSON.parse(data);
                myData = data.preview;
                if(data.error == 0) {
                    Helper.loadingStop();
                    $('.note p').html('');
                    $('.note span').html('');
                    $('.note').css('display','none');
                    if(type_import == 'batchinventory') {
                        $('#preview').dataTable({
                            destroy: true,
                            bAutoWidth: false,
                            "aaData": data.preview,
                            "aoColumns": [
                                { "mDataProp": "batch_date", width: '200px'},
                                { "mDataProp": "shipping_cost", width: '200px' },
                                { "mDataProp": "extra_cost", width: '100px' },
                                { "mDataProp": "product_code", width: '200px' },
                                { "mDataProp": "value", width: '200px' },
                                { "mDataProp": "quantity", width: '100px' },
                                { "mDataProp": "status", width: '200px' },
                                { "mDataProp": "cogs_value", width: '100px'},
                                { "mDataProp": "unit_types", width: '300px' },
                                { "mDataProp": "warehouse_name", width: '300px' },
                                { "mDataProp": "warehouse_address", width: '200px' },
                            ]
                        });
                    } else {

                        function templatex(d) {
                            console.log(d)
                            template = '';
                            template += '<div class="col-md-4">';
                            template += '<h5>Part Data</h5>';
                            template += '<table class="table">';
                            template += '<tr>';
                            template += '<td>No</td>';
                            template += '<td>Code Material</td>';
                            template += '<td>Parts Description</td>';
                            template += '<td>Quantity</td>';
                            template += '</tr>';

                            _.each(d.part_data_excel, function(i, key) {
                                template += '<tr>';
                                template += '<td>' + (key + 1) + '</td>';
                                template += '<td>' + i.code_material + '</td>';
                                template += '<td>' + i.part_description + '</td>';
                                template += '<td>' + i.quantity + '</td>';
                                template += '</tr>';
                            })
                            template += '</table>';
                            template += '</div>';

                            return template;
                        }
                        var table = $('#preview').DataTable({
                            destroy: true,
                            bAutoWidth: false,
                            processing: true,
                            "aaData": data.preview,
                            "aoColumnDefs": [{
                                targets: [5, 15],
                                createdCell: function(cell) {
                                    var $cell = $(cell);
                                    $(cell).contents().wrapAll("<div class='content'></div>");
                                    var $content = $cell.find(".content");

                                    $(cell).append($("<button class='btn btn-sm btn-outline-primary'>Read more</button>"));
                                    $btn = $(cell).find("button");

                                    $content.css({
                                    "height": "50px",
                                    "overflow": "hidden"
                                    })
                                    $cell.data("isLess", true);

                                    $btn.click(function() {
                                    var isLess = $cell.data("isLess");
                                    $content.css("height", isLess ? "auto" : "50px")
                                    $(this).text(isLess ? "Read less" : "Read more")
                                    $cell.data("isLess", !isLess)
                                    })
                                }
                            }],

                            "aoColumns": [
                                {
                                    "className": 'details-control',
                                    "orderable": false,
                                    "mDataProp": null,
                                    "defaultContent": ''
                                },
                                { "mDataProp": "service_order_no", width: '200px'},
                                { "mDataProp": "asc_name", width: '200px' },
                                { "mDataProp": "customer_name", width: '100px' },
                                { "mDataProp": "type_job", width: '200px' },
                                { "mDataProp": "defect_type", width: '200px' },
                                { "mDataProp": "engineer_code", width: '100px' },
                                { "mDataProp": "engineer_name", width: '200px' },
                                { "mDataProp": "assist_engineer_name", width: '100px'},
                                { "mDataProp": "merk_brand", width: '300px' },
                                { "mDataProp": "model_product", width: '300px' },
                                { "mDataProp": "status", width: '200px' },
                                { "mDataProp": "reason", width: '200px'},
                                { "mDataProp": "remark_reason", width: '200px' },
                                { "mDataProp": "pending_aging_days", width: '100px' },
                                { "mDataProp": "street", width: '200px' },
                                { "mDataProp": "city", width: '200px' },
                                { "mDataProp": "phone_no_mobile", width: '100px' },
                                { "mDataProp": "phone_no_home", width: '200px' },
                                { "mDataProp": "phone_no_office", width: '100px'},
                                { "mDataProp": "month", width: '300px' },
                                { "mDataProp": "date", width: '300px' },
                                { "mDataProp": "request_time", width: '200px' },
                                { "mDataProp": "created_times", width: '200px'},
                                { "mDataProp": "engineer_picked_order_date", width: '200px' },
                                { "mDataProp": "engineer_picked_order_time", width: '100px' },
                                { "mDataProp": "admin_assigned_to_engineer_date", width: '200px' },
                                { "mDataProp": "admin_assigned_to_engineer_time", width: '200px' },
                                { "mDataProp": "engineer_assigned_date", width: '100px' },
                                { "mDataProp": "engineer_assigned_time", width: '200px' },
                                { "mDataProp": "tech_1st_appointment_date", width: '100px'},
                                { "mDataProp": "tech_1st_appointment_time", width: '300px' },
                                { "mDataProp": "1st_visit_date", width: '300px' },
                                { "mDataProp": "1st_visit_time", width: '200px' },
                                { "mDataProp": "repair_completed_date", width: '200px'},
                                { "mDataProp": "repair_completed_time", width: '200px' },
                                { "mDataProp": "closed_date", width: '100px' },
                                { "mDataProp": "closed_time", width: '200px' },
                                { "mDataProp": "rating", width: '200px' },

                            ]
                        });
                        $('#preview tbody').on('click', 'td.details-control', function() {
                            var tr = $(this).closest('tr');
                            var row = table.row(tr);

                            if (row.child.isShown()) {
                                // This row is already open - close it
                                row.child.hide();
                                tr.removeClass('shown');
                            } else {
                                // Open this row
                                row.child(templatex(row.data())).show();
                                tr.addClass('shown');
                            }
                        });
                    }

                    $('#modal_preview').modal('show');
                }
                else if(data.error == 1){
                    Helper.loadingStop();
                    $('.note p').html('');
                    $('.note span').html('');
                    $('.note').css('display','none');
                    $('#import').attr('disabled', true);
                    Helper.errorNotif(data.msg);
                }
                else if(data.error == 2) {
                    Helper.loadingStop();
                    $('.note p').html('');
                    $('.note span').html('');
                    $('.note').css('display','block');
                    $('#import').attr('disabled', true);
                    $('.note span').append('<p style="font-style: italic;font-weight:800; color:#FF6347;">'+data.data.length+" error has found</p>");
                    for(i=0;i<data.data.length;i++){
                        $('.note #note-content').append((i+1)+". "+data.data[i]+"<br/>");
                    }
                }

            },
            complete:function(){
                Helper.loadingStop();
                $('.preview').removeClass('hidden');
            },
            error:function(data){
                Helper.loadingStop();
                $('.loader').css('display','none');
                $('#import').attr('disabled', false);
                Helper.errorNotif('Error : '+data.responseJSON.msg);

            }
        });
    });


    var myData;

    function saveData() {

        console.log(myData);
        Helper.loadingStart();
        $('#import').attr('disabled', true);
        $.ajax({
            url:Helper.apiUrl('/batchinventory/save-import-excel'),
            type: "POST",
            data: {data:myData},
            success: function (data) {
                Helper.loadingStop();
                $('#import').attr('disabled', true);
                Helper.successNotif('Batch Inventory Import has been Success !');
            },error:function(){
                Helper.loadingStop();
                $('#import').attr('disabled', true);
                Helper.errorNotif(data.msg);
            }
        });
    }

    function saveDataOrdersDataExcel() {
        console.log(JSON.stringify(myData));
        var aa = JSON.stringify(myData);

        Helper.loadingStart();
        $('#import').attr('disabled', true);
        $.ajax({
            url:Helper.apiUrl('/ordersdataexcel/save-import-excel'),
            type: "POST",
            data: {data:aa},
            success: function (data) {
                Helper.loadingStop();
                $('#import').attr('disabled', true);
                Helper.successNotif('Orders Data Excel Import has been Success !');
            },error:function(){
                Helper.loadingStop();
                $('#import').attr('disabled', true);
                Helper.errorNotif(data.msg);
            }
        });
    }

</script>
