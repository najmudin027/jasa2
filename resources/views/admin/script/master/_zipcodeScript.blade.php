<script>
    // select 2 where location id
    $(document).ready(function() {
        if ($("#province").val() == null) {
            $("#provinceShow").hide('');
        }
        if($("#city").val() == null) {
            $("#cityShow").hide('');
        }
        if ($("#district").val() == null) {
            $("#districtShow").hide('');
        }
        $("#country").select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/country/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });

        $("#country").change(function() {
            $("#province").html('')
            $("#provinceShow").show('')
            $("#cityShow").hide('')
            $("#districtShow").hide('')
            provinceSelect($(this).val())
        });
        provinceSelect($("#country").val())

        $("#province").change(function() {
            $('#city').val('');
            $("#cityShow").show('')
            $("#districtShow").hide('')
            citySelect($(this).val());
        })
        citySelect($("#province").val());
        $("#city").change(function() {
            $("#district").val('')
            $("#districtShow").show('')
            districtSelect($(this).val());
        });
        districtSelect($("#city").val());
    });

    function provinceSelect(id) {
        $("#province").select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/address_search/find_province?country_id=' + id),
                // url: 'http://localhost:8001/api/address_search/find_district?city_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }

    function citySelect(id) {
        $("#city").select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/address_search/find_city?province_id=' + id),
                // url: 'http://localhost:8001/api/address_search/find_village?district_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }

    function districtSelect(id) {
        $("#district").select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/address_search/find_district?city_id=' + id),
                // url: 'http://localhost:8001/api/address_search/find_village?district_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    _distric = data.data
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }

</script>
