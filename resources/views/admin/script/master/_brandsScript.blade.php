<script>
    //select 2 part category
    $("#product_part").select2({
        ajax: {
            type: "GET",
            url:Helper.apiUrl('/product_part_category/select2'),
            // url: 'http://localhost:8001/api/product_part_category/select2',
            data: function(params) {
                return {
                    q: params.term
                };
            },
            processResults: function(data) {
                var res = $.map(data.data, function(item) {
                    return {
                        text: item.part_category,
                        id: item.id
                    }
                });

                return {
                    results: res
                };
            }
        }
    })

    $('#save').click(function(e){
        alert('asd');
        $.ajax({
            type:'post',
            data:{
                name : $('input[name="name"]').val(),
                code : $('input[name="code"]').val(),
                product_part_category_id : $('select[name="product_part_category_id"]').val(),
            },
           
            url:Helper.apiUrl('/product_brand'),
            success:function(html){
                swal.fire({
                    type: 'success',
                    title: 'City Has Been Saved',
                    icon: "success",
                });
                window.location.href = Helper.redirectUrl('/show-list-product-brand');
            },
            error:function(xhr, status, error){
                swal.fire({
                    type: 'error',
                    title: 'distrik yg dipilih tidak ditemukan',
                    icon: "error",
                });
            },
        });
        e.preventDefault();
    })

    

    $('#update').click(function(e){
        id : $('input[name="id"]')
        $.ajax({
            type : 'put',
            data : {
                id : $('input[name="id"]'),
                name : $('input[name="name"]'),
                code : $('input[name="code"]'),
                product_part_category_id : $('input[name="product_part_category_id"]').val(),
            },
            url : Helper.apiUrl('/product_brand/' + id),
            success:function(html){
                swal.fire({
                    type: 'success',
                    title: 'Brands Has Been Updated',
                    icon: "success",
                });
            }, 
            error:function(xhr, status, error){
                swal.fire({
                    type: 'error',
                    title: 'Something went worng',
                    icon: "error",
                });
            },
        });
        e.preventDefault();
    })

    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: true,
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },

        ajax : {
            url : Helper.apiUrl('/product_brand/datatables'),
            type : 'get',
            data : function(d){
                return $.extend( {}, d,  {
                    "extra_search": $('#extra').val()
                });
            }
        },

            columns : [
                {
                    name : 'id', 
                    data : 'id'
                },
                {
                    name: 'name',
                    data: 'name'
                },
                {
                    name: 'code',
                    data: 'code'
                },
                {
                    name: 'id',
                    data: 'id',
                    render : function(data, type, full){
                        return "<a href='#' data-id='"+full.id+"'  class='delete btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>" +
                                "&nbsp <a href=''  data-id='"+full.id+"' class='update btn btn-primary btn-sm'><i class='fa fa-pencil'></i></a>"
                    }
                },
            ],
    });

    Helper.numberRow(table)

    //view Update
    $(document).on('click', '.update', function(e){
        id = $(this).attr('data-id');
        updateBrands(id)
        e.preventDefault()
    })

    function updateBrands(id){
        window.location.href = Helper.redirectUrl('/update-product-brand/' + id) 
    }

    // delete
    $(document).on('click', '.delete', function(e){
        id = $(this).attr('data-id')
        deleteBrands(id)
        e.preventDefault()
    })

    function deleteBrands(id){
        if(confirm("Are you sure to delete this Brands ? Your action can not be reversed")){
            $.ajax ({
                type : 'delete',
                url : Helper.apiUrl('/product_brand/' + id),
                dataType : 'JSON',
                contentType : 'application/json',
                success: function(res) {
                    swal.fire({
                    type: 'success',
                    title: 'Color Has Been Deleted',
                    icon: "success",
                });
                    table.ajax.reload();
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }
</script>