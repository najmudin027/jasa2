<script>
    $('#save').click(function(e){
        $.ajax({
            type : 'post',
            data : {
                status : $('input[name="status"]').val(),
            },
            url : Helper.apiUrl('/maritals'),
            success:function(html){
                swal.fire({
                    type: 'success',
                    title: 'Marital Has Been Saved',
                    icon: "success",
                });
                window.location.href = Helper.redirectUrl('/show-list-marital');
            },
            error:function(){
                swal.fire({
                    type: 'error',
                    title: 'something went wrong',
                    icon: "error",
                });
            },
        });
        e.preventDefault();
    })

    // update
    $('#update').click(function(e){
        id = $('input[name="id"]').val()
        $.ajax({
            type:'put',
            data:{
                id : $('input[name="id"]').val(),
                status : $('input[name="status"]').val(),
            },
            
            url:Helper.apiUrl('/maritals/' + id),
            success:function(html){
                swal.fire({
                    type: 'success',
                    title: 'Maritals Has Been Updated',
                    icon: "success",
                });
                console.log(html)
                window.location.href = Helper.redirectUrl('/show-list-marital');
            },
            error:function(xhr, status, error){
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+field).text(pesan[0])
                    })
                }
            },
        });
        e.preventDefault();
    })

    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: true,
        dom: 'Bflrtip',
        
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },

        ajax: {
            url:Helper.apiUrl('/maritals/datatables'),
            "type": "get",
            "data": function ( d ) {
                return $.extend( {}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },

        columns: [
            { 
                data : "id", 
                name: "id" 
            },
            { 
                data : "status", 
                name: "status",
            },
            { 
                data : "id", 
                name : "id", 
                render : function(data, type, full) {
                    return "<a href='#' data-id='"+full.id+"'  class='delete btn btn-danger btn-xs'><i class='fa fa-trash'></i></a>" +
                            "&nbsp<a href='' data-id='"+full.id+"'  class='update btn btn-primary btn-xs'><i class='fa fa-pencil'></i></a>" 
                
                }
            },
        ],
    });

    Helper.numberRow(table)

     // button update
     $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        updateMarital(id)
        e.preventDefault()
    })

    function updateMarital(id) {
        window.location.href = Helper.redirectUrl('/update-marital/' + id )
    }

    //button delete
    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deleteMarital(id)
        e.preventDefault()
    })

    function deleteMarital(id) {
        if(confirm("Are you sure to delete this MArital ? Your action can not be reversed")) {
            $.ajax({
                url:Helper.apiUrl('/maritals/' + id ),
                type: 'delete',
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    swal.fire({
                    type: 'success',
                    title: 'MArital Has Been Deleted',
                    icon: "success",
                });
                    table.ajax.reload();

                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }
</script>