@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>

    var $modal = $('#createType').modal({
        show: false
    });

    var table = $('#table-address').DataTable({
        processing: true,
        serverSide: true,
        select: true,
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            url: Helper.apiUrl('/address_type/datatables'),
            "type": "get",
            "data": function(d) {
                return $.extend({}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },
        columnDefs: [{
            targets: 0,
            className: 'text-center',
            searchable: false,
            sortable: false,
            width: "5%"
            // render: $.fn.dataTable.render.number('.', ',', 2 )
        }],
        columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
            },
            {
                data: "name",
                name: "name",
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    region = full;
                    btn_delete = '<button data-id="' + full.id + '" class="btn-wide btn-danger btn-sm delete-address-type"><i class="fa fa-trash"></i></button>';
                    edit_delete = '<button data-id="' + full.id + '" class="btn-wide btn-primary btn-sm edit-address-type"><i class="fa fa-pencil"></i></button>';
                    action = edit_delete + ' ' + btn_delete;
                    return action;
                }
            },
        ],
    });

    $('.add-type').on('click', function() {
        emptyForm();
        $modal.modal('show')
    });

    $('#form-address-type').submit(function(e) {
        var data = {};
        $.each($(this).serializeArray(), function(i, field) {
            data[field.name] = field.value;
        });
        if (data.id == '') {
            console.log('inser')
            insertAddressType(data);
        } else {
            console.log('update')
            updateAddressType(data);
        }

        e.preventDefault();
    })

    // delete data
    $(document).on('click', ".delete-address-type", function() {
        id = $(this).attr('data-id');
        deleteAddressType(id);
    })

    // edit data
    $(document).on('click', ".edit-address-type", function() {
        id = $(this).attr('data-id');
        var row = table.row( $(this).parents('tr') ).data();
        addDataModal(row);
        $modal.modal('show');
    })

    function addDataModal(region) {
        $("#name").val(region.name);
        $("#id").val(region.id);
    }

    function deleteAddressType(id) {
        $.ajax({
            url: Helper.apiUrl('/address_type/') + id,
            type: 'delete',
            dataType: 'JSON',
            contentType: 'application/json',
            success: function(res) {
                // reload table
                table.ajax.reload();
                Helper.successNotif('success')
            },
            error: function(res) {
                Helper.errorHandle(res)
                console.log(res);
            }
        })
    }

    function emptyForm() {
        $("#name").val('');
        $("#id").val('');
    }

    function insertAddressType(data) {
        $.ajax({
            url: Helper.apiUrl('/address_type'),
            data: data,
            type: 'post',
            success: function(res) {
                table.ajax.reload();
                successHandle(res);
            },
            error: function(res) {
                Helper.errorHandle(res)
                console.log(res);
            }
        })
    }

    function updateAddressType(data) {
        $.ajax({
            url: Helper.apiUrl('/address_type/' + data.id),
            data: data,
            type: 'put',
            success: function(res) {
                // reload table
                table.ajax.reload();

                successHandle(res);
            },
            error: function(res) {
                Helper.errorHandle(res)
                console.log(res);
            }
        })
    }

    function successHandle(resp) {
        Helper.successNotif(resp.msg);
        $modal.modal('hide');
    }








</script>
@endsection
