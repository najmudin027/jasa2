<script>
   // saveData
   $('#save').click(function(e){
        $.ajax({
            type:'post',
            data:{
                part_category : $('input[name="part_category"]').val(),
                part_code : $('input[name="part_code"]').val(),
            },
           
            url:Helper.apiUrl('/product_part_category'),
            success:function(html){
                console.log(html)
                // simpan ke
                // redirect
                swal.fire({
                    type: 'success',
                    title: 'City Has Been Saved',
                    icon: "success",
                });
                window.location.href = Helper.redirectUrl('/show-list-part-category');
            },
            error:function(xhr, status, error){
                swal.fire({
                    type: 'error',
                    title: 'Part yg dipilih tidak ditemukan',
                    icon: "error",
                });
            },
        });
        e.preventDefault();
    })

    $('#update').click(function(e) {
        id = $('input[name="id"]').val()
        $.ajax({
            type : 'put',
            data:{
                id : $('input[name="id"]').val(),
                part_category : $('input[name="part_category"]').val(),
                part_code : $('input[name="part_code"]').val(),
            },
            url : Helper.apiUrl('/product_part_category/' + id),
            success:function(html){
                swal.fire({
                    type: 'success',
                    title: 'Part Has Been Updated',
                    icon: "success",
                });
                window.location.href = Helper.redirectUrl('/show-list-part-category');
            },
            error:function(xhr, status, error){
                swal.fire({
                    type: 'error',
                    title: 'Part yg dipilih tidak ditemukan',
                    icon: "error",
                });
            },
        });
        e.preventDefault();
    })

    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: true,
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            url:Helper.apiUrl('/product_part_category/datatables'),
            "type": "get"
        },

        columns: [
            { 
                data : "id", 
                name: "id" 
            },
            { 
                data : "part_category", 
                name: "part_category" 
            },
            { 
                data : "part_code", 
                name: "part_code" 
            },
            { 
                data : "id", 
                name : "id", 
                render : function(data, type, full) {
                    return "<a href='' data-id='"+full.id+"' class='update btn btn-success btn-sm'><i class='fa fa-pencil'></i></a>"+
                            "&nbsp <a href='#'  data-id='"+full.id+"' class='delete btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>" 
                            
                
                }
            },
        ],
    });

    Helper.numberRow(table)

    //btn update
    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deletePcategory(id)
        e.preventDefault()
    })

    function viewPcategory(id) {
        window.location.href = Helper.redirectUrl('/update-part-category/' + id )
    }

    //btn delete
    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        viewPcategory(id)
        e.preventDefault()
    })

    function deletePcategory(id) {
        console.log('id', id)
        if(confirm("Are you sure to delete this Distric ? Your action can not be reversed")) {
            $.ajax({
                url:Helper.apiUrl('/product_part_category/' + id ),
                type: 'delete',
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    swal.fire({
                    type: 'success',
                    title: 'Part Category Has Been Deleted',
                    icon: "success",
                });
                    table.ajax.reload();

                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }

</script>