<script>
    $(document).ready(function() {
        if ($("#province").val() == null) {
            $("#provinceShow").hide('');
        }
        if($("#city").val() == null) {
            $("#cityShow").hide('');
        }
        $("#country").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/country/select2'),
                // url: 'http://localhost:8001/api/cities/select2',
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });

        $("#country").change(function() {
            $("#province").val('')
            $("#provinceShow").show('');
            $("#cityShow").hide('');
            provinceSelect($(this).val());
        })
        provinceSelect($("#country").val());

        $("#province").change(function() {
            $("#cityShow").show('')
            $("#city").val('')
            citySelect($(this).val());
        })
        citySelect($("#province").val());
    });

    function provinceSelect(id) {
        $("#province").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/address_search/find_province?country_id=' + id),
                // url: 'http://localhost:8001/api/address_search/find_district?city_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }

    function citySelect(id) {
        $("#city").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/address_search/find_city?province_id=' + id),
                // url: 'http://localhost:8001/api/address_search/find_village?district_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }

</script>
