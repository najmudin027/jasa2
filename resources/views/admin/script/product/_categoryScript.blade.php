{{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> --}}
<script>
    // view datatable
    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: true,
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            url:Helper.apiUrl('/categories/datatables'),
            "type": "get",
            "data": function ( d ) {
                return $.extend( {}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },

        columns: [
            { 
                data : "id", 
                name: "id" 
            },
            { 
                data : "name", 
                name: "name", 
                render: function(data, type, full){
                    return '<span  class="category-name-span">'+full.name+'</span>';
                } 
            },
            { 
                data : "child", 
                name: "sub_category", 
                render: function(data, type, full){
                    return full.childs.length;
                } 
            },
            { 
                data : "id", 
                name : "id", 
                render : function(data, type, full) {
                    return "<a href='' data-id='"+full.id+"' class='update btn btn-success btn-xs'><i class='fa fa-pencil'></i></a>"+
                            "&nbsp <a href='#'  data-id='"+full.id+"' class='delete btn btn-danger btn-xs'><i class='fa fa-trash'></i></a>" 
                            
                
                }
            },
        ],
    });

    // Add Number Rows
    Helper.numberRow(table)

    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deleteCategories(id)
        e.preventDefault()
    })

    // btn update
    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        updateCategories(id)
        e.preventDefault()
    })

    function updateCategories(id) {
        window.location.href = Helper.redirectUrl('/update-category/' + id )
    }

    function deleteCategories(id) {
        console.log('id', id)
        if(confirm("Are you sure to delete this Categories ? Your action can not be reversed")) {
            $.ajax({
                url:Helper.apiUrl('/categories/' + id ),
                type: 'delete',
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    swal.fire({
                    type: 'success',
                    title: 'Category Has Been Deleted',
                    icon: "success",
                });
                    table.ajax.reload();

                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }

    // $(document).ready(function() {
    //     var max_fields      = 100; //maximum input boxes allowed
    //     var wrapper   		= $(".form-groups1"); //Fields wrapper
    //     var add_button      = $(".add_button"); //Add button ID
        
    //     var x = 1; //initlal text box count
    //     $(add_button).click(function(e){ //on add input button click
    //         e.preventDefault();
    //         if(x < max_fields){ //max input box allowed
    //             x++; //text box increment
    //             $(wrapper).append('<div class="form-groups1">'+
    //                                     '<div class="position-relative row form-group">'+
    //                                         '<label for="exampleEmail" class="col-sm-2 col-form-label">Child Category Name</label>'+
    //                                         '<div class="input-group col-md-10">'+
    //                                             '<input type="text" name="name" class="form-control" placeholder="Category">'+
    //                                             '<div class="input-group-append">'+
    //                                                 // '<button class="add_button btn btn-outline-secondary" type="button" id="button-addon2">'+
    //                                                 //     '<i class="fa fa-minus"></i>'+
    //                                                 // '</button>'+
    //                                                 '<a href="#"  class="remove_fields btn btn-outline-secondary" type="button" id="button-addon2">'+
    //                                                     '<i class="fa fa-minus"></i>'+
    //                                                 '</a>'+
    //                                             '</div>'+
    //                                         '</div>'+
    //                                     '</div>'+
    //                                 '</div>'); //add input box
    //         }
    //     });
        
    //     $(wrapper).on("click",".remove_fields", function(e){ //user click on remove text
    //         e.preventDefault(); $(this).parent().parent().parent().remove(); x--;
    //     })
    // });

    //store data
    $('#save').click(function(e){
        $.ajax({
            type:'post',
            data:{
                name : $('input[name="name"]').val(),
                parent_id : $('input[name="parent_id"]').val(),
            },
           
            url:Helper.apiUrl('/categories'),
            success:function(html){
                console.log(html)
                // simpan ke
                // redirect
                swal.fire({
                    type: 'success',
                    title: 'Category Has Been Saved',
                    icon: "success",
                });
                window.location.href = Helper.redirectUrl('/list-category');
            },
            error:function(xhr, status, error){
                pesan = xhr.responseJSON.msg;
                Helper.msgNotif(pesan, 'error');
            },
        });
        e.preventDefault();
    })

    // update
    $('#update').click(function(e){
        alert('asdas')
        id = $('input[name="id"]').val()
        alert(id)
        $.ajax({
            type:'put',
            data:{
                name : $('input[name="name"]').val(),
            },
            
            url:Helper.apiUrl('/categories/' + id),
            success:function(html){
                swal.fire({
                    type: 'success',
                    title: 'Category Has Been Updated',
                    icon: "success",
                });
                console.log(html)
                window.location.href = Helper.redirectUrl('/list-category');
            },
            error:function(xhr, status, error){
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+field).text(pesan[0])
                    })
                }
            },
        });
        e.preventDefault();
    })


    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls form:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
		$(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
</script>