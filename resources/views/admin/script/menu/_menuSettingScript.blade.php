
<script>

    //store data
    $('#save').click(function(e){
        $.ajax({
            type:'post',
            data:{
                name         : $('input[name  = "name"]').val(),
                url          : $('input[name  = "url"]').val(),
                icon         : $('input[name  = "icon"]').val(),
                display_order: $('input[name  = "display_order"]').val(),
                permission_id: '17',
                parent_id    : $('select[name = "parent_id"]').val(),
            },

            url:Helper.apiUrl('/menu'),
            success:function(html){
                console.log(html)
                swal.fire({
                    type: 'success',
                    title: 'Menus Has Been Saved',
                    icon: "success",
                });
                window.location.href = Helper.redirectUrl('admin/menu/show');
            },
            error:function(xhr, status, error){
                pesan = xhr.responseJSON.msg;
                Helper.msgNotif(pesan, 'error');
            },
        });
        e.preventDefault();
    })

    //update data
    $('#update').click(function(e){
        menu = $('input[name="id"]').val()
        alert(menu)
        $.ajax({
            type:'put',
            data:{
                id           : $('input[name  = "id"]').val(),
                name         : $('input[name  = "name"]').val(),
                url          : $('input[name  = "url"]').val(),
                icon         : $('input[name  = "icon"]').val(),
                display_order: $('input[name  = "display_order"]').val(),
                parent_id    : $('select[name = "parent_id"]').val(),
            },

            url:Helper.apiUrl('/menu/' + menu),
            success:function(html){
                console.log(html)
                window.location.href = Helper.redirectUrl('admin/menu/show');
            },
            error:function(html){
                alert('gagal');
            },
        });
        e.preventDefault();
    })



    // view datatable
    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: true,
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            url:Helper.apiUrl('/menu/datatables'),
            "type": "get",

            "data": function ( d ) {
                return $.extend( {}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },
        columns: [
            {
                className:      'details-control',
                orderable:      false,
                data:           null,
                defaultContent: ''
            },
            {
                data : "name",
                name: "name"
            },
            {
                data : "child",
                name: "sub_category",
                render: function(data, type, full){
                    return full.childs.length;
                }
            },
            { data : "id", name : "id", render : function(data, type, full) {
                return "<a href='#' data-id='"+full.id+"' class='delete btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>" +
                        "&nbsp;<a href='' data-id='"+full.id+"' class='update btn btn-primary btn-sm'><i class='fa fa-pencil'></i></a>"

            }},
        ],
    });

    $('#example tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );

   // Add Number Rows
    // Helper.numberRow(table)

    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deleteMenus(id)
        e.preventDefault()
    })

    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        viewMenus(id)
        e.preventDefault()
    })



    $(".js-example-basic-single").select2({
        ajax: {
            type: "GET",
            url:Helper.apiUrl('/menu/select2'),
            data: function (params) {
                return {
                    q: params.term
                };
            },
            processResults: function (data) {
                var res = $.map(data.data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                });
                return {
                    results: res
                };
            }
        }
    });

    function viewMenus(id) {
        window.location.href = Helper.redirectUrl('/admin/menu/update/' + id )
    }

    function deleteMenus(id) {
        console.log('id', id)
        if(confirm("Are you sure to delete this Menus ? Your action can not be reversed")) {
            $.ajax({
                url:Helper.apiUrl('/menu/' + id ),
                type: 'delete',
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    table.ajax.reload();
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }

    function format ( d ) {
        console.log(d)
        var childs = d.childs
        var listChild = ''
        var template = ''

        _.each(childs, function(child){
            listChild += '<tr>';
                listChild += '<td style="width:20px"></td>';
                listChild += '<td style="width:400px">'+ child.name +'</td>';
                listChild += '<td style="width:100px">';
                    listChild += '<a href="#" data-id='+child.id+' class="delete btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
                    listChild += '&nbsp;<a href="" data-id='+child.id+' class="update btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>';
                listChild += '</td>';
                listChild += '<td style="width:520px">';
                listChild += '</td>';

            listChild += '</tr>';
        })
        // `d` is the original data object for the row
        template += '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';

            template += listChild;


            template += '</table>';
            return template;
    }
</script>
