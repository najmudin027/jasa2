<script>
    $('#resend').click(function(e){
        $.ajax({
            type:'post',
            data: {},
            url:Helper.apiUrl('/email/resend'),
            success:function(html){
                console.log(html)
                Swal.fire({
                    icon: 'success',
                    title: 'CONGRATULATION',
                    text: 'Email Has Ben Resend',
                })
            },
            error:function(html){
                Swal.fire({
                    icon: 'Error',
                    title: 'Oops...',
                    text: 'Something went wrong!'
                })
            },
        });
        e.preventDefault();
    })
</script>