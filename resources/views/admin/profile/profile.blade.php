@extends('admin.home')
@section('content')
<style>
    input.hidden {
        position: absolute;
        left: -9999px;
    }

    #profile-image1 {
        cursor: pointer;
        width: 80%;
        border-radius: 80%;
    }

    .btn_login { 
        background-color: #26C6DA;
        border: none;
        padding: 10px;
        width: 200px;
        border-radius:3px;
        box-shadow: 1px 5px 20px -5px rgba(0,0,0,0.4);
        color: #fff;
        margin-top: 10px;
        cursor: pointer;
    }
</style>

    <div class="app-main__inner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            asdasdas
                            <div class="card-title">User Profileasdasd</div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6"><br><br>
                                        <div  align="center">
                                            @if ($detailUser->info->picture == null)
                                                <img alt="User Pic" src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" id="profile-image1" class="img-circle img-responsive"> 
                                                <br><br>
                                            @else
                                                <img alt="User Pic" src="{{ asset('/admin/storage/image/profile/' . $detailUser->info->picture) }}" id="profile-image1" class="img-circle img-responsive">     
                                                <br><br>
                                            @endif
                                            
                                            {{-- <br><br><input id="profile-image-upload" class="hidden" type="file">
                                            <div style="color:#999;" >click here to change profile image</div> --}}
                                        </div>
                                    </div>
                                    <div class="col-md-6"><br><br><br>
                                        <h3 style="color:#00b1b1;" align="center"><strong>{{ $detailUser->info->first_name }} {{ $detailUser->info->last_name }}</strong></h3><hr>
                                        <table>
                                            <tr>
                                                <td>
                                                    <h5 style="font-family:'Times New Roman', Times, serif">First Name</h5>
                                                </td>
                                                <td style="color:white">&nbsp;&nbsp;____</td>
                                                <td>
                                                    <h6 style="font-family:'Times New Roman', Times, serif">{{ $detailUser->info->first_name }}</h6>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h5 style="font-family:'Times New Roman', Times, serif">Last Name</h5>
                                                </td>
                                                <td style="color:white">&nbsp;&nbsp;____</td>
                                                <td>
                                                    <h6 style="font-family:'Times New Roman', Times, serif">{{ $detailUser->info->last_name }}</h6>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h5 style="font-family:'Times New Roman', Times, serif">Date Of Birth</h5>
                                                </td>
                                                <td style="color:white">&nbsp;&nbsp;____</td>
                                                <td>
                                                    <h6 style="font-family:'Times New Roman', Times, serif">{{ $detailUser->info->date_of_birth }}</h6>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h5 style="font-family:'Times New Roman', Times, serif">Place Of Birth</h5>
                                                </td>
                                                <td style="color:white">&nbsp;&nbsp;____</td>
                                                <td>
                                                    <h6 style="font-family:'Times New Roman', Times, serif">{{ $detailUser->info->place_of_birth }}</h6>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h5 style="font-family:'Times New Roman', Times, serif">Gender</h5>
                                                </td>
                                                <td style="color:white">&nbsp;&nbsp;____</td>
                                                <td>
                                                    <h6 style="font-family:'Times New Roman', Times, serif">{{ $detailUser->info->place_of_birth == 0 ? 'Pria' : 'Wanita' }}</h6>
                                                </td>
                                            </tr>
                                        </table>
                                        <hr>
                                        {{-- <button class="btn_login" type="submit">Update Profile</button> --}}
                                        <a href="{{ url('/update-profile/'. $detailUser->info->id ) }}" class="btn_login">Update Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('admin.template._mainScript')
@include('admin.script.profile._profileScript')
@endsection
