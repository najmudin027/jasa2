@extends('admin.home')
@section('content')
<div class="col-md-12">
    @if(session()->has('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>	
            <strong>{{ session()->get('success') }}</strong>
        </div>
    @endif
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <!-- <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New Grade</button> -->
            </div>
        </div>
        <div class="card-body">
            <table id="table-teknisi" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Telepon</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ url('/admin/business-to-business-master-teknisi/create') }}" class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New</a>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
   $(document).ready(function() {
            var table = $('#table-teknisi').DataTable({
                processing: true,
                serverSide: true,
                select: false,
                dom: 'Bflrtip',
                responsive: true,
                order: [1, 'asc'],
                language: {
                    buttons: {
                        colvis: '<i class="fa fa-list-ul"></i>'
                    },
                    search: '',
                    searchPlaceholder: "Search...",
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                },
                buttons: [{
                        extend: 'colvis'
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload();
                        }
                    }
                ],
                dom: "<'row'<'col-sm-6'Bl><'col-sm-6'>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                ajax: {
                    url: Helper.apiUrl('/business-to-business-master-teknisi/datatables'),
                    "type": "get",
                },
                columns: [
                    {
                        data: "DT_RowIndex",
                        name: "DT_RowIndex",
                        sortable: false,
                        searchable: false,
                        width: "10%"
                    },
                    {
                        data: "name",
                        name: "name",
                        render: function(data, type, row) {
                            return row.name
                        }
                    },
                    {
                        data: "email",
                        name: "email",
                        render: function(data, type, row) {
                            return row.email
                        }
                    },
                    {
                        data: "phone",
                        name: "phone",
                        render: function(data, type, row) {
                            return row.phone 
                        }
                    },
                    // {
                    //     data: "status",
                    //     name: "status",
                    //     render: function(data, type, row) {
                    //         return row.status 
                    //     }
                    // },
                    {
                        data: "id",
                        name: "id",
                        render: function(data, type, row) {
                            btn_edit_item = '<a href="/admin/business-to-business-master-teknisi/update/' + row.id + '" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>';
                            btn_delete_item = '<button data-toggle="tooltip" title="delete" type="button" data-id="' + row.id + '" class="btn btn-danger btn-xs delete-teknisi"><i class="fa fa-trash"></i></button>';
                            // view_link = "<a class='btn btn-primary btn-sm' href='" + Helper.url('/admin/business-to-business-user-request-order/detail/' + row.request_code) + "'><i class='fa fa-eye'></i></a>";
                            // btn_delete_item = '<button type="button" data-id="' + row.request_code + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
                            return btn_edit_item + " " + btn_delete_item;
                        }
                    },
                ],
            });
        });

    $(document).on('click', '.delete-teknisi', function() {
            id = $(this).attr('data-id');
            // bootbox.dialog("Warning!, are you sure you want to delete this user", function(result){ 
            bootbox.confirm("Apakah Anda yakin akan Menghapus Teknisi bussiness to Bussiness ini ?", function(result){ 
                if (result === true) {
                    Helper.loadingStart();
                    Axios.delete('/business-to-business-master-teknisi/delete/' + id)
                        .then(function(response) {
                            // send notif
                            Helper.successNotif('Teknisi has been deleted');
                            // reload table
                            Helper.loadingStop();
                            location.reload();
                        })
                        .catch(function(error) {
                            console.log(error)
                            Helper.handleErrorResponse(error)
                        });
                }
            });
        })
</script>
@endsection
