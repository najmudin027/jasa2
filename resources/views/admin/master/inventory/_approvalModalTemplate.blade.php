<div class="no-gutters row">
    <div class="col-md-6">
        <div class="pt-0 pb-0 card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-outer">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">BATCH NO</div>
                                    <div class="widget-subheading">{{ $inventory->batch_item->batch->batch_no }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-6">
        <div class="pt-0 pb-0 card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-outer">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">WAREHOUSE</div>
                                    <div class="widget-subheading">{{ $inventory->warehouse->name }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-12">
        <div class="pt-0 pb-0 card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-outer">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">PRODUCT</div>
                                    <div class="widget-subheading">{{ $inventory->batch_item->product_name }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-12">
        <div class="pt-0 pb-0 card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-outer">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">QTY SHIPPING</div>
                                    <div class="widget-subheading">{{ $inventory->batch_item->value_format }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-outer">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">QTY INVENTORY</div>
                                    <div class="widget-subheading">{{ $inventory->stock }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-outer">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left" style="color: red">
                                    <div class="widget-heading">QTY INVENTORY REQUEST</div>
                                    <div class="widget-subheading">
                                        <h4>{{ $inventory->stock_temp }}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>