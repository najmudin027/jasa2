@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-bottom:15px;">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/inventory/show') }}" class="btn btn-primary btn-sm">Back</a>
            </div>
        </div>
        <div class="card-body">
            <form action="" method="post" id="form-inventory-mutation">
                <input name="id" id="inventory_id" value="{{ $inventory->id }}" type="hidden" class="form-control">
                <input name="ms_batch_item_id" value=" {{ $inventory->ms_batch_item_id }} " type="hidden" class="form-control">
                <input id="warehouse_id" value="{{  $inventory->warehouse->id }}" type="hidden" class="form-control">
                <input id="quantity_format" value="{{ $inventory->stock_now }}" type="hidden" class="form-control">

                <input name="this_warehouse_name" value="{{ $inventory->warehouse_name }}" type="hidden" class="form-control">
                <input name="this_batch_code" value="{{ $inventory->batch_code }}" type="hidden" class="form-control">

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6  col-xl-5">
                            <div class="position-relative form-group">
                                <label for="" class="">Batch No: </label>
                                <b>{{ $inventory->batch_code }}</b>
                            </div>
                            <div class="position-relative form-group">
                                <label for="" class="">Warehouse: </label>
                                <b>{{ $inventory->warehouse_name }}</b>
                            </div>
                            <div class="position-relative form-group">
                                <label for="" class="">Stock: </label>
                                <b><span id="this_stock">{{ $inventory->stock_now }}</span></b>
                            </div>
                            <div class="position-relative form-group">
                                <label for="" class="">Product: </label>
                                <b>{{ $inventory->item_name }}</b>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-5">
                            <div class="position-relative form-group">
                                <label for="" class="">Werehouse Inventory</label>
                                <select name="ms_warehouse_id" id="warehouse-select" class="form-control" required>
                                    <option value="">--Pilih--</option>
                                </select>
                            </div>

                            <div class="position-relative form-group">
                                <label for="" class="">Stock</label>
                                <input name="stock" id="stock" type="number" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <button class="btn btn-primary btn-sm btn-add-new-item" type="submit"><i class="fa fa-save"></i> Save</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Inventory Mutation Histories
            </div>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <table id="table" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Batch Code</th>
                                    <th>Product Name</th>
                                    <th>Warehouse</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th>Stock Before</th>
                                    <th>Stock After</th>
                                    <th>Logs</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<style>

</style>
@include('admin.master.inventory.select2')
<script>

    var tbl = "";
    tbl = $('#table').DataTable({
        ajax: {
            url: Helper.apiUrl('/inventory/mutation-histories/'+$('#inventory_id').val()),
            type: "get",
            error: function () {
            },
            complete: function() {
            },
        },
        selector: '#table',
        dom: 't',
        columns: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',

            },
            {
                data: 'inventory.batch_item.batch.batch_no',
                name: 'inventory.batch_item.batch.batch_no',
            },
            {
                data: 'inventory.item_name',
                name: 'inventory.item_name',
            },
            {
                data: 'warehouse_name',
                name: 'warehouse_name',
            },
            {
                data: 'amount',
                name: 'amount',
            },
            {
                data: 'type_amount',
                name: 'type_amount',
                render: function(data, type, row){
                    if (data == 1) {
                        return '<span class="badge badge-success">IN</span>'
                    } else {
                        return '<span class="badge badge-danger">OUT</span>';
                    }

                }
            },
            {
                data: 'stock_before',
                name: 'stock_before',
            },
            {
                data: 'stock_after',
                name: 'stock_after',
            },
            {
                render: function(data, type, row){
                    if(row.order_id != null) {
                        if (row.type_amount == 1) {
                            return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit dari Cancel Order  #<b>'+row.order.code+'</b>'
                        } else {
                            return '<span class="badge badge-danger">Stok Keluar</span> <b>'+row.amount+'</b> unit ke Order #<b>'+row.order.code+'</b>'
                        }
                    } else {
                        if(row.type_amount == 0) {
                            return '<span class="badge badge-danger">Stok Keluar</span> <b>'+row.amount+'</b> unit ke Inventory <b>'+row.mutation_join.history_in.warehouse_name+'</b>'
                        } else {
                            return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit dari Inventory <b>'+row.mutation_join.history_out.warehouse_name+'</b>'
                        }

                    }

                }
            },

            {
                data: 'created_at',
                name: 'created_at',
                render: function(data, type, row) {
                    return moment(data).format("DD MMMM YYYY hh:mm:ss");
                }
            },

        ]
    });

</script>
<script>

    if($('#this_stock').html() == 0) {
        $('#stock').val(0);
        $('#stock').prop('disabled',true);
    }
    // select2
    globalCRUD
        .select2('#warehouse-select', '/warehouses/select2p/'+$('#warehouse_id').val()+'')

    var max = parseInt($('#quantity_format').val());
    console.log($('#quantity_format').val());
    $('#stock').keyup(function(e) {
        if ($(this).val() > max) {
        e.preventDefault();
        $(this).val(max);
        } else {
            $(this).val();
        }
    });
    $('#form-inventory-mutation').submit(function (e) {
        e.preventDefault();
        if($('#stock').val() == 0 ) {
            Helper.warningNotif('This value has 0, cannot mutation !')
        } else if($('#stock').val() > parseInt($('#quantity_format').val())) {
            Helper.warningNotif('Value is greater than the existing stock')
        } else{
            var data = new FormData(this);
            Helper.confirm( ()=> {
                $.ajax({
                    url: Helper.apiUrl('/inventory/mutation'),
                    type: 'post',
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        Helper.successNotif('Mutation Inventory success updated !')
                        // window.location.href = Helper.redirectUrl('/admin/inventory/show');
                        $('#this_stock').html(parseInt($('#quantity_format').val()) - parseInt($('#stock').val()))
                        tbl.ajax.reload();
                    }
                });
            });
        }

    })
</script>



@endsection
