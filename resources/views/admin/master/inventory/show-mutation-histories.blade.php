@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <table id="table" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Batch Code</th>
                                    <th>Product Name</th>
                                    <th>Warehouse</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th>Stock Before</th>
                                    <th>Stock After</th>
                                    <th>Logs</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>

    var tbl = "";

    tbl = $('#table').DataTable({
        ajax: {
            url: Helper.apiUrl('/inventory/mutation-histories/'),
            type: "get",
            error: function () {
            },
            complete: function() {
            },
        },
        selector: '#table',
        dom: 't',
        columns: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',

            },
            {
                data: 'inventory.batch_item.batch.batch_no',
                name: 'inventory.batch_item.batch.batch_no',
            },
            {
                data: 'inventory.item_name',
                name: 'inventory.item_name',
            },
            {
                data: 'warehouse_name',
                name: 'warehouse_name',
            },
            {
                data: 'amount',
                name: 'amount',
            },
            {
                data: 'type_amount',
                name: 'type_amount',
                render: function(data, type, row){
                    if (data == 1) {
                        return '<span class="badge badge-success">IN</span>'
                    } else {
                        return '<span class="badge badge-danger">OUT</span>';
                    }

                }
            },
            {
                data: 'stock_before',
                name: 'stock_before',
            },
            {
                data: 'stock_after',
                name: 'stock_after',
            },
            {
                render: function(data, type, row){
                    if(row.order_id != null) {
                        if (row.type_amount == 1) {
                            return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit dari Cancel Order  #<b>'+row.order.code+'</b>'
                        } else {
                            return '<span class="badge badge-danger">Stok Keluar</span> <b>'+row.amount+'</b> unit ke Order #<b>'+row.order.code+'</b>'
                        }
                    } else {
                        if(row.type_amount == 0) {
                            return '<span class="badge badge-danger">Stok Keluar</span> <b>'+row.amount+'</b> unit ke Inventory <b>'+row.mutation_join.history_in.warehouse_name+'</b>'
                        } else {
                            return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit dari Inventory <b>'+row.mutation_join.history_out.warehouse_name+'</b>'
                        }

                    }
                }
            },

            {
                data: 'created_at',
                name: 'created_at',
                render: function(data, type, row) {
                    return moment(data).format("DD MMMM YYYY hh:mm:ss");
                }
            },

        ]
    });


</script>
@endsection
