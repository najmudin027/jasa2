@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add">ADD BATCH STATUS SHIPMENTS</button>
                <!-- <a href="{{ url('/create-additional') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">Add Additional</a>  -->
            </div>
        </div>
        <div class="card-body">
            <table id="table" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<form id="form">
    <div class="modal fade" id="modal" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Status Shipment</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Name</label>
                    <div class="form-group">
                        <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    var tbl = globalCRUD.datatables({
        url: '/batch-status-shipment/datatables',
        selector: '#table',
        // columnsField: ['id', 'name', 'is_accepted'],
        modalSelector: "#modal",
        modalButtonSelector: ".add",
        modalFormSelector: "#form",
        columns: [
        {
            data: 'name',
            name: 'name',
            width: "50%",
            sortable: false,
            render: function(data, type, full) {
                if(full.is_accepted) {
                    return data+'&nbsp;&nbsp;<i class="fa fa-check"></i>';
                } else {
                    return data;
                }
            }
        },
        {
            data: 'id',
            name: 'id',
            render: function(data, type, full) {
                var id = full.id;
                var id_cant_delete = ['1','2','3'];
                edit_btn = "<button data-id='" + id + "' class='table-edit btn btn-warning btn-sm btn-white'><i class='fa fa-pencil'></i> Edit</button>";
                delete_btn = "<button data-id='" + id + "' class='table-delete btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</button>";
                if(id == '1' || id == '2' || id == '3') {
                    return edit_btn;
                } else {
                    return edit_btn + " " + delete_btn;
                }


            }
        }
       ],
        actionLink: {
            store: function() {
                return "/batch-status-shipment";
            },
            update: function(row) {
                return "/batch-status-shipment/" + row.id;
            },
            delete: function(row) {
                return "/batch-status-shipment/" + row.id;
            },
        },
    });

    //edit data
    $(document)
        .on('click', ".table-edit", function() {

            var row = DataTablesHelper.table.row($(this).parents('tr')).data();

            if(row.is_accepted) {
                $('#is_accepted').prop('checked',true);
            } else {
                $('#is_accepted').prop('checked',false);
            }
            if (config.modalSelector !== null) {
                ModalHelper.modalShow('Edit Table', row);
            } else {
                Helper.redirectTo(config.actionLink.update(row));
            }

        })

    // delete data
    $(document).on('click', ".table-delete", function() {
        var row = DataTablesHelper.table.row($(this).parents('tr')).data();
        globalCRUD.delete(config.actionLink.delete(row))

    })

    // //is_accepted
    // $(document)
    //     .on('click', ".table-accepted", function() {
    //         var row = DataTablesHelper.table.row($(this).parents('tr')).data();
    //         Helper.confirm( ()=> {
    //             $.ajax({
    //                 url: Helper.apiUrl('/batch-status-shipment/set_accepted'),
    //                 type: 'post',
    //                 data: {
    //                     id: row.id
    //                 },
    //                 success: function(response) {
    //                     Helper.successNotif('Status shipment success updated !')
    //                     tbl.table.reloadTable()

    //                 }
    //             })
    //         })
    // })

</script>
@endsection
