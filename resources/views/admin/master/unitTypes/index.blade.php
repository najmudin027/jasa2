@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">

            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-unit-type" class="display table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Unit Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-unit-type"><i class="fa fa-plus"></i> Add Unit Type</button>
        </div>
    </div>
</div>
@include('admin.master.unitTypes.modalUnitType')
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        orderBy: [1, 'asc'],
        url: '/unit-type/datatables',
        selector: '#table-unit-type',
        columnsField: ['DT_RowIndex', 'unit_name'],
        modalSelector: "#modal-unit-type",
        modalButtonSelector: ".add-unit-type",
        modalFormSelector: "#form-unit-type",
        actionLink: {
            store: function() {
                return "/unit-type";
            },
            update: function(row) {
                return "/unit-type/" + row.id;
            },
            delete: function(row) {
                return "/unit-type/" + row.id;
            },
        }
    })
</script>
@endsection