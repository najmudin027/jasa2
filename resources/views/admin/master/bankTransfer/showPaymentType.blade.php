@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
            </div>

        </div>

        <div class="card-body">
            <table style="width: 100%;" id="table-bank-transfer" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Payment Name</th>
                        <th>Action</th>
                        <th>Active/In Active Payment</th>
                    </tr>
                </thead>
                @foreach ($showListPayment as $item)
                    <tbody>
                        <td>{{ $no++ }}</td>
                        <td>{{ $item->payment_name }}</td>
                        <td>
                            @if ($item->id == 1)
                                <a href="{{ url('/admin/bank/show') }}" class="btn-hover-shine btn btn-primary btn-sm" data-toggle="tooltip" data-html="true" title="Detail"><i class='fa fa-pencil'></i></a>
                            @else
                                <a href="{{ url('/admin/bank/midtrans-settings') }}" class="btn btn-success btn-sm" data-toggle="tooltip" data-html="true" title="Midtrans Detail"><i class="fa fa-pencil"></i></a>
                            @endif
                        </td>
                        <td>
                            @if ($item->is_active == 1)
                                <input name="status" class="tgl tgl-skewed switch-btn click_btn" data-id="{{ $item->id }}" id="cb3-{{ $item->id }}" type="checkbox" />
                                <label class="tgl-btn" data-tg-off="Inactive" data-tg-on="Active" for="cb3-{{ $item->id }}"></label>
                            @else
                                <input name="status" class="tgl tgl-skewed switch-btn click_btn" data-id="{{ $item->id }}" id="cb3-{{ $item->id }}" type="checkbox" checked />
                                <label class="tgl-btn" data-tg-off="Inactive" data-tg-on="Active" for="cb3-{{ $item->id }}"></label>
                            @endif
                        </td>
                    </tbody>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function() { $('#table-bank-transfer').DataTable(); } );

        $('.switch-btn').click(function() {
            id = $(this).attr('data-id');
            // alert($('.click_btn').is(":checked") ? 1 : 0);
            // if(this.checked){
                $.ajax({
                    type: "POST",
                    url: Helper.apiUrl('/general-setting/switch_btn/' + id ),
                    data: {
                        status : $('.click_btn').is(":checked") ? 1 : 0,
                    },
                    success: function(data) {
                            iziToast.success({
                                title: 'OK',
                                position: 'topRight',
                                message: 'Data Has Been Saved',
                            });
                    },
                    error: function() {
                       
                            iziToast.error({
                                title: 'OK',
                                position: 'topRight',
                                message: 'something went wrong',
                            });
                        
                    },
                });
            // }
        });
    </script>
@endsection

<style>
    .tgl {
        display: none;
    }

    .tgl,
    .tgl:after,
    .tgl:before,
    .tgl *,
    .tgl *:after,
    .tgl *:before,
    .tgl+.tgl-btn {
        box-sizing: border-box;
    }

    .tgl::-moz-selection,
    .tgl:after::-moz-selection,
    .tgl:before::-moz-selection,
    .tgl *::-moz-selection,
    .tgl *:after::-moz-selection,
    .tgl *:before::-moz-selection,
    .tgl+.tgl-btn::-moz-selection {
        background: none;
    }

    .tgl::selection,
    .tgl:after::selection,
    .tgl:before::selection,
    .tgl *::selection,
    .tgl *:after::selection,
    .tgl *:before::selection,
    .tgl+.tgl-btn::selection {
        background: none;
    }

    .tgl+.tgl-btn {
        outline: 0;
        display: block;
        width: 7em;
        height: 2em;
        position: relative;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .tgl+.tgl-btn:after,
    .tgl+.tgl-btn:before {
        position: relative;
        display: block;
        content: "";
        width: 50%;
        height: 100%;
    }

    .tgl+.tgl-btn:after {
        left: 0;
    }

    .tgl+.tgl-btn:before {
        display: none;
    }

    .tgl:checked+.tgl-btn:after {
        left: 50%;
    }


    .tgl-skewed+.tgl-btn {
        overflow: hidden;
        -webkit-transform: skew(-10deg);
        transform: skew(-10deg);
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        -webkit-transition: all .2s ease;
        transition: all .2s ease;
        font-family: sans-serif;
        background: #888;
    }

    .tgl-skewed+.tgl-btn:after,
    .tgl-skewed+.tgl-btn:before {
        -webkit-transform: skew(10deg);
        transform: skew(10deg);
        display: inline-block;
        -webkit-transition: all .2s ease;
        transition: all .2s ease;
        width: 100%;
        text-align: center;
        position: absolute;
        line-height: 2em;
        font-weight: bold;
        color: #fff;
        text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);
    }

    .tgl-skewed+.tgl-btn:after {
        left: 100%;
        content: attr(data-tg-on);
    }

    .tgl-skewed+.tgl-btn:before {
        left: 0;
        content: attr(data-tg-off);
    }

    .tgl-skewed+.tgl-btn:active {
        background: #888;
    }

    .tgl-skewed+.tgl-btn:active:before {
        left: -10%;
    }

    .tgl-skewed:checked+.tgl-btn {
        background: #1976d2;
    }

    .tgl-skewed:checked+.tgl-btn:before {
        left: -100%;
    }

    .tgl-skewed:checked+.tgl-btn:after {
        left: 0;
    }

    .tgl-skewed:checked+.tgl-btn:active:after {
        left: 10%;
    }
</style>
