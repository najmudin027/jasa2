@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                
            </div>
        </div>
        <div class="card-body">
            <table id="table-price-item" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Product</th>
                        <th>Unit Type</th>
                        <th>City</th>
                        <th>Distric</th>
                        <th>Village</th>
                        <th>Value</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-unit-type"><i class="fa fa-plus"></i> Add Unit Type</button>
        </div>
    </div>
</div>
@include('admin.master.priceItems.create')
@endsection

@section('script')
<script>
    Helper.currency('#value');
    
    globalCRUD
        .select2Static("#unit_type", '/unit-type/select2', function(item) {
            return {
                id: item.id,
                text: item.unit_name
            }
        })
        .select2("#product", '/product/select2')
        .select2("#city", '/city/select2')

    datatablesCrud = globalCRUD.datatables({
        url: '/price-item/datatables',
        selector: '#table-price-item',
        modalSelector: '#modal-price-item',

        columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex"
            },
            {
                data: "product.name",
                name: "ms_product_id",
            },
            {
                data: "unit.unit_name",
                name: "ms_unit_type_id",
            },
            {
                data: "city.name",
                name: "ms_city_id",
            },
            {
                data: "district.name",
                name: "ms_district_id",
            },
            {
                data: "village.name",
                name: "ms_village_id",
            },
            {
                data: "value",
                name: "value",
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    priceItems = full;
                    btn_delete = '<button data-id="' + full.id + '" class="btn-wide btn-danger btn-sm btn delete-price-item"><i class="fa fa-trash"></i></button>';
                    edit_delete = '<button data-id="' + full.id + '" class="btn-wide btn-primary btn btn-sm edit-price-item"><i class="fa fa-pencil"></i></button>';
                    action = edit_delete + ' ' + btn_delete;
                    return action;
                }
            },
        ]
    })

    var $modal = $('#modal-price-item').modal({
        show: false
    });

    // add data
    $('.add-unit-type')
        .on('click', function() {
            emptyFormModal();
            $modal.modal('show');
        });

    // on change city
    $("#city")
        .change(function() {
            city_id = $('#city').val();

            $("#distric").empty();
            $("#vilage").empty();

            globalCRUD.select2(
                selector = '#distric',
                url = '/address_search/find_district?city_id=' + city_id
            )
        })

    // on change distric
    $("#distric")
        .change(function() {
            district_id = $('#distric').val();

            $("#vilage").empty();

            globalCRUD.select2(
                selector = '#vilage',
                url = '/address_search/find_village?district_id=' + district_id
            )
        })

    // edit data
    $(document)
        .on('click', ".edit-price-item", function() {
            var row = DataTablesHelper.table.row($(this).parents('tr')).data();

            console.log(row);
            isiFormModal(row);
            $modal.modal('show');
        })

    // delete data
    $(document)
        .on('click', ".delete-price-item", function() {
            id = $(this).attr('data-id');
            globalCRUD.delete('/price-item/' + id)
        })

    // submit data
    $('#form-price-item')
        .submit(function(e) {
            Helper.unMask('#value')
            var data = Helper.serializeForm($(this));

            if (data.id == '') {
                globalCRUD.store('/price-item/', data)
            } else {
                globalCRUD.update('/price-item/' + data.id, data)
            }

            e.preventDefault();
        })

    function emptyFormModal() {
        $("#value").val('');
        $("#city").empty();
        $("#distric").empty();
        $("#vilage").empty();
        $("#product").empty();
        $("#unit_type").val('');
        $("#id").val('');
    }

    function isiFormModal(row) {
        var productOption = new Option(row.product.name, row.product.id, false, false);
        $('#product').empty().append(productOption).trigger('change');

        var cityOption = new Option(row.city.name, row.city.id, false, false);
        $('#city').empty().append(cityOption).trigger('change');

        var districOption = new Option(row.district.name, row.district.id, false, false);
        $('#distric').empty().append(districOption).trigger('change');

        var vilageOption = new Option(row.village.name, row.village.id, false, false);
        $('#vilage').empty().append(vilageOption).trigger('change');

        $("#value").val(row.value);
        $("#unit_type").val(row.ms_unit_type_id).change();
        $("#id").val(row.id);
        Helper.currency('#value')
    }
</script>
@endsection
