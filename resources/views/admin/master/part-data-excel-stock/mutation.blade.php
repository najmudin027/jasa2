@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-bottom:15px;">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/part-data-excel-stock/show') }}" class="btn btn-primary btn-sm">Back</a>
            </div>
        </div>
        <div class="card-body">
            <form action="" method="post" id="form-part-data-excel-stock-mutation">
                <input name="id" id="pdes_id" value="{{ $pdes->id }}" type="hidden" class="form-control">
                <input name="code_material" value=" {{ $pdes->code_material }} " type="hidden" class="form-control">
                <input name="part_description" value=" {{ $pdes->part_description }} " type="hidden" class="form-control">
                <input id="quantity_format" value="{{ $pdes->stock }}" type="hidden" class="form-control">
                <input name="asc_id" value="{{ $pdes->asc_id }}" type="hidden" class="form-control">
                <input name="asc_name" value="{{ $pdes->asc_name }}" type="hidden" class="form-control">

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6  col-xl-5">
                            <div class="position-relative form-group">
                                <label for="" class="">Code Material: </label>
                                <b>{{ $pdes->code_material }}</b>
                            </div>
                            <div class="position-relative form-group">
                                <label for="" class="">Part Description: </label>
                                <b>{{ $pdes->part_description }}</b>
                            </div>
                            <div class="position-relative form-group">
                                <label for="" class="">ASC Name: </label>
                                <b>{{ $pdes->asc_name }}</b>
                            </div>
                            <div class="position-relative form-group">
                                <label for="" class="">Stock: </label>
                                <b><span id="this_stock">{{ $pdes->stock }}</span></b>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-5">
                            <div class="position-relative form-group">
                                <label for="" class="">ASC Name</label>
                                <select name="asc_id" id="asc-select" class="form-control" required>
                                    <option value="">--Pilih--</option>
                                </select>
                            </div>

                            <div class="position-relative form-group">
                                <label for="" class="">Stock</label>
                                <input name="stock" id="stock" type="number" class="form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <button class="btn btn-primary btn-sm btn-add-new-item" type="submit"><i class="fa fa-save"></i> Save</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Part Data Stock Histories
            </div>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <table id="table" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Part Description</th>
                                    <th>ASC Name</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th>Stock Before</th>
                                    <th>Stock After</th>
                                    <th>Logs</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<style>

</style>
<script>

    var tbl = "";
    tbl = $('#table').DataTable({
        ajax: {
            url: Helper.apiUrl('/part-data-excel-stock/mutation-histories/'+$('#pdes_id').val()),
            type: "get",
            error: function () {
            },
            complete: function() {
            },
        },
        selector: '#table',
        dom: 't',
        columns: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',

            },

            {
                data: 'part_data_stock_name',
                name: 'part_data_stock_name',
            },
            {
                data: 'asc_name',
                name: 'asc_name',
            },
            {
                data: 'amount',
                name: 'amount',
            },
            {
                data: 'type_amount',
                name: 'type_amount',
                render: function(data, type, row){
                    if (data == 1) {
                        return '<span class="badge badge-success">IN</span>'
                    } else {
                        return '<span class="badge badge-danger">OUT</span>';
                    }

                }
            },
            {
                data: 'stock_before',
                name: 'stock_before',
            },
            {
                data: 'stock_after',
                name: 'stock_after',
            },
            {
                render: function(data, type, row){

                    // 0 = add_general_journal;
                    // 1 = mutasi_masuk_part_stock;
                    // 2 = mutasi_keluar_part_stock;
                    // 3 = add_order_data_excel;
                    // 4 = edit_general_journal;
                    // 5 = edit_order_data_excel;
                    // 6 = delete_general_journal;
                    // 7 = delete_order_data_excel;
                    // 8 = create_new_part_stock;

                    if (row.type_mutation == 0) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit dari Petty Part'
                    } else if (row.type_mutation == 1) {
                        return '<span class="badge badge-warning">Mutasi <span class="text-success">In</span></span> <b>'+row.amount+'</b> unit dari ASC Name = '+row.mutation_join.history_out.asc_name+'</b>'
                    } else if (row.type_mutation == 2) {
                        return '<span class="badge badge-warning">Mutasi <span class="text-danger">Out</span></span> <b>'+row.amount+'</b> unit dari ASC Name = '+row.mutation_join.history_in.asc_name+'</b>'
                    } else if (row.type_mutation == 3) {
                        return '<span class="badge badge-danger">Stok Keluar</span> <b>'+row.amount+'</b> unit ke Order Data Excel #<b>'+(row.part_data_excel != null ? row.part_data_excel.order_data_excel.service_order_no : '(deleted)')+'</b>'
                    } else if (row.type_mutation == 4 && row.type_amount == 0) {
                        return '<span class="badge badge-success">Stok Keluar</span> <b>'+row.amount+'</b> unit edit Petty Part'
                    } else if (row.type_mutation == 4 && row.type_amount == 1) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit edit Petty Part'
                    } else if (row.type_mutation == 5 && row.type_amount == 0) {
                        return '<span class="badge badge-success">Stok Keluar</span> <b>'+row.amount+'</b> unit edit Order Data Excel #<b>'+(row.part_data_excel != null ? row.part_data_excel.order_data_excel.service_order_no : '(deleted)')+'</b>'
                    } else if (row.type_mutation == 5 && row.type_amount == 1) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit edit Order Data Excel #<b>'+(row.part_data_excel != null ? row.part_data_excel.order_data_excel.service_order_no : '(deleted)')+'</b>'
                    } else if (row.type_mutation == 6) {
                        return '<span class="badge badge-danger">Stok Keluar</span> <b>'+row.amount+'</b> unit (deleted) Petty'
                    } else if (row.type_mutation == 7) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit delete Order Data Excel #<b>(deleted)</b>'
                    } else if (row.type_mutation == 8) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit, Create New Part Stock</b>'
                    }

                }
            },

            {
                data: 'created_at',
                name: 'created_at',
                render: function(data, type, row) {
                    return moment(data).format("DD MMMM YYYY hh:mm:ss");
                }
            },

        ]
    });

</script>
<script>


    if($('#this_stock').html() == 0) {
        $('#stock').val(0);
        $('#stock').prop('disabled',true);
    }

    // select2
    globalCRUD
        .select2('#asc-select', '/asc-excel/select2p/'+ $('input[name="asc_id"]').val()+'')

    var max = parseInt($('#quantity_format').val());
    console.log($('#quantity_format').val());
    $('#stock').keyup(function(e) {
        if ($(this).val() > max) {
        e.preventDefault();
        $(this).val(max);
        } else {
            $(this).val();
        }
    });
    $('#form-part-data-excel-stock-mutation').submit(function (e) {
        e.preventDefault();
        if($('#stock').val() == 0 ) {
            Helper.warningNotif('This value has 0, cannot mutation !')
        } else if ($('#stock').val() > parseInt($('#quantity_format').val())) {
            Helper.warningNotif('Value is greater than the existing stock')
        } else{
            var data = new FormData(this);
            Helper.confirm( ()=> {
                $.ajax({
                    url: Helper.apiUrl('/part-data-excel-stock/mutation'),
                    type: 'post',
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        Helper.successNotif('Mutation Part Data Excel Stock success updated !')
                        // window.location.href = Helper.redirectUrl('/admin/inventory/show');
                        $('#this_stock').html(parseInt($('#quantity_format').val()) - parseInt($('#stock').val()))
                        tbl.ajax.reload();
                    }
                });
            });
        }

    })
</script>



@endsection
