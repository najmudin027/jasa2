@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                assaf
            </div>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <table id="table" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Part Description</th>
                                    <th>ASC Name</th>
                                    <th>Amount</th>
                                    <th>Type</th>
                                    <th>Stock Before</th>
                                    <th>Stock After</th>
                                    <th>Logs</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>

var tbl = "";
    tbl = $('#table').DataTable({
        ajax: {
            url: Helper.apiUrl('/part-data-excel-stock/mutation-histories'),
            type: "get",
            error: function () {
            },
            complete: function() {
            },
        },
        selector: '#table',
        dom: 't',
        columns: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',

            },
            {
                data: 'part_data_stock_name',
                name: 'part_data_stock_name',
            },
            {
                data: 'asc_name',
                name: 'asc_name',
            },
            {
                data: 'amount',
                name: 'amount',
            },
            {
                data: 'type_amount',
                name: 'type_amount',
                render: function(data, type, row){
                    if (data == 1) {
                        return '<span class="badge badge-success">IN</span>'
                    } else {
                        return '<span class="badge badge-danger">OUT</span>';
                    }

                }
            },
            {
                data: 'stock_before',
                name: 'stock_before',
            },
            {
                data: 'stock_after',
                name: 'stock_after',
            },
            {
                render: function(data, type, row){

                    // 0 = add_general_journal;
                    // 1 = mutasi_masuk_part_stock;
                    // 2 = mutasi_keluar_part_stock;
                    // 3 = add_order_data_excel;
                    // 4 = edit_general_journal;
                    // 5 = edit_order_data_excel;
                    // 6 = delete_general_journal;
                    // 7 = delete_order_data_excel;
                    // 8 = create_new_part_stock;

                    if (row.type_mutation == 0) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit dari Petty Part'
                    } else if (row.type_mutation == 1) {
                        return '<span class="badge badge-warning">Mutasi <span class="text-success">In</span></span></span> <b>'+row.amount+'</b> unit dari ASC Name = '+row.mutation_join.history_out.asc_name+'</b>'
                    } else if (row.type_mutation == 2) {
                        return '<span class="badge badge-warning">Mutasi <span class="text-danger">Out</span></span></span> <b>'+row.amount+'</b> unit dari ASC Name = '+row.mutation_join.history_in.asc_name+'</b>'
                    } else if (row.type_mutation == 3) {
                        return '<span class="badge badge-danger">Stok Keluar</span> <b>'+row.amount+'</b> unit ke Order Data Excel #<b>'+(row.part_data_excel != null ? row.part_data_excel.order_data_excel.service_order_no : '(deleted)')+'</b>'
                    } else if (row.type_mutation == 4 && row.type_amount == 0) {
                        return '<span class="badge badge-success">Stok Keluar</span> <b>'+row.amount+'</b> unit edit Petty Part'
                    } else if (row.type_mutation == 4 && row.type_amount == 1) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit edit Petty Part'
                    } else if (row.type_mutation == 5 && row.type_amount == 0) {
                        return '<span class="badge badge-success">Stok Keluar</span> <b>'+row.amount+'</b> unit edit Order Data Excel #<b>'+(row.part_data_excel != null ? row.part_data_excel.order_data_excel.service_order_no : '(deleted)')+'</b>'
                    } else if (row.type_mutation == 5 && row.type_amount == 1) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit edit Order Data Excel #<b>'+(row.part_data_excel != null ? row.part_data_excel.order_data_excel.service_order_no : '(deleted)')+'</b>'
                    } else if (row.type_mutation == 6) {
                        return '<span class="badge badge-danger">Stok Keluar</span> <b>'+row.amount+'</b> unit (deleted) Petty Part'
                    } else if (row.type_mutation == 7) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit delete Order Data Excel #<b>(deleted)</b>'
                    } else if (row.type_mutation == 8) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit, Create New Part Stock</b>'
                    }

                }
            },

            {
                data: 'created_at',
                name: 'created_at',
                render: function(data, type, row) {
                    return moment(data).format("DD MMMM YYYY hh:mm:ss");
                }
            },

        ]
    });

</script>
@endsection
