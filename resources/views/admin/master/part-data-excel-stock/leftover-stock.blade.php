@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-bottom:15px;">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="">Months</label>
                            <select class="form-control" name="month" id="month">
                                <option value="" selected>- All -</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="">Years</label>
                            <select class="form-control" name="year" id="year">
                                <option value="" selected>- All -</option>
                                @for($year=$max_year; $year >= $min_year; $year--)
                                    <option value="{{$year}}">{{$year}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm mt-2" type="submit">Search</button>
                <button class="btn btn-primary btn-sm mt-2" type="button" id="export_data" style="display:none">Export Data</button>
            </form>
        </div>

    </div>
</div>

<div class="col-md-12" id="table_content">
    <div class="card">
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <table id="table-leftover-stock" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Code Material</th>
                                    <th>ASC Name</th>
                                    <th>Part Description</th>
                                    <th>Month</th>
                                    <th>Year</th>
                                    <th>Hpp Average</th>
                                    <th>Leftover Stock</th>
                                    <th>Detail In Out</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- Modal Detail Out -->
 <div class="modal fade" id="detail_inout_modal" data-backdrop="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail In Out <span id="month_year"></span></h5>
            </div>
            <div class="modal-body">
                <table id="table-detail-inout" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <tr>
                                <th>No.</th>
                                <th>Part Description </th>
                                <th>ASC Name</th>
                                <th>Recent Stock</th>
                                <th>Amount</th>
                                <th>Type IN/OUT</th>
                                <th>Created By</th>
                            </tr>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="close_detail_inout_modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<style>
    .table-edit {
        visibility: hidden;
    }
</style>
<script>
    $(document).ready(function() {
        $('#table_content').hide();
        $('#export_data').prop('disabled', true);
    });

    $(document).on('change', '#month, #year', function() {
        $('#export_data').prop('disabled', true);
    })
     // var option_remove = [];
     var data = [
        {id: '1', text: 'January', title: 'January'},
        {id: '2', text: 'February', title: 'February'},
        {id: '3', text: 'March', title: 'March'},
        {id: '4', text: 'April', title: 'April'},
        {id: '5', text: 'May', title: 'May'},
        {id: '6', text: 'June', title: 'June'},
        {id: '7', text: 'July', title: 'July'},
        {id: '8', text: 'August', title: 'August'},
        {id: '9', text: 'September', title: 'September'},
        {id: '10', text: 'October', title: 'October'},
        {id: '11', text: 'November', title: 'November'},
        {id: '12', text: 'December', title: 'December'},
    ];

    $("#month").select2({
        data: data,
        escapeMarkup: function(markup) {
            return markup;
        }
    })

    $("#form-search").submit(function(e) {
        e.preventDefault();
        var input = Helper.serializeForm($(this));
        playload = '?';
        _.each(input, function(val, key) {
            playload += key + '=' + val + '&'
        });
        playload = playload.substring(0, playload.length - 1);
        console.log(playload)
        url = '/part-data-excel-stock/leftover-stock' + playload;
        // tbl_leftover_stock.table.reloadTable(url);
        initTableLeftoverStock(url)
        $('#table_content').show();
        $('#export_data').prop('disabled', false);
    });

    function initTableLeftoverStock(url) {
        var tbl_leftover_stock = globalCRUD.datatables({
            url: url,
            selector: '#table-leftover-stock',
            export: true,
            column_export: [0,1,2,3,4,5,6,7],
            columnsField: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                },

                {
                    data: 'code_material',
                    name: 'code_material',
                },
                {
                    data: 'asc_name',
                    name: 'asc_name',
                },
                {
                    data: 'part_description',
                    name: 'part_description',
                },
                {
                    data: 'month',
                    name: 'month',
                },
                {
                    data: 'year',
                    name: 'year',
                },
                {
                    data: 'hpp_average_month',
                    name: 'hpp_average_month',
                },
                {
                    data: 'leftover_stock',
                    name: 'leftover_stock',
                },
                {
                    render: function(data, type, row){

                        return "<button class='btn btn-primary btn-sm open_detail_inout' month_number="+row.month_number+" year="+row.year+" part_id="+ row.part_data_stock_id+" title='Detail Out'><i class='fa fa-bars'></i></button>";

                    }
                },

            ],
            actionLink:""
        });
        return tbl_leftover_stock.table.reloadTable(Helper.apiUrl(url));
    }

    $(document).on('click', '#close_detail_inout_modal', function() {
        $('#detail_inout_modal').modal('hide');
    });

    $(document).on('click', '.open_detail_inout', function() {
        console.log('asd')
        var id = $(this).attr('part_id');
        var month_number = $(this).attr('month_number')
        var year = $(this).attr('year');
        $('#month_year').html(moment().month(month_number).format('MMMM')+' - '+year);
        console.log([id,month_number,year])
        var url = '/part-data-excel-stock/histories-in-out/'+id+'/'+month_number+'/'+year+'';
        initTableInOut(url);
        $('#detail_inout_modal').modal('show');


    });

    function initTableInOut(url) {
        var tbl = globalCRUD.datatables({
            url: url,
            selector: '#table-detail-inout',
            columnsField: [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                },
                {
                    data: 'part_data_stock_name',
                    name: 'part_data_stock_name',
                },
                {
                    data: 'asc_name',
                    name: 'asc_name',
                },
                {
                    data: 'stock_after',
                    name: 'stock_after',
                },
                {
                    data: 'amount',
                    name: 'amount',
                },
                {
                    data: 'type_mutation',
                    name: 'type_mutation',
                    render: function(data, type, row){
                        // 0 = add_general_journal;
                        // 3 = order_data_excel;

                        if (data == 0) {
                            return '<span class="badge badge-success">IN</span> General Journal'
                        } else if (data == 8) {
                            return '<span class="badge badge-success">IN</span> Create New Part Stock';
                        } else if (data == 3) {
                            return '<span class="badge badge-danger">OUT</span> Orders Excel #<b>'+((row.part_data_excel != null && row.part_data_excel.order_data_excel != null) ? row.part_data_excel.order_data_excel.service_order_no : '(deleted)')+'</b>';
                        }

                    }
                },

                {
                    data: 'created_at',
                    name: 'created_at',
                    render: function(data, type, row) {
                        return '<span class="badge badge-primary">'+row.user.name+' </span><br>'+moment(data).format("DD MMMM YYYY hh:mm:ss");
                    }
                }
            ],
            actionLink: ""
        });

        tbl.table.reloadTable(Helper.apiUrl(url));

    }


</script>




@endsection
