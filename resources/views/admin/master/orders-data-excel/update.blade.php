@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/orders-data-excel/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off">
    <input type="hidden" name="id" value="{{ $getData->id }}">
    <div class="col-md-6" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                #1 Services
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Service Order No</label>
                    <div class="col-sm-12">
                        <input name="service_order_no" placeholder="Service Order No" type="text" class="form-control" value="{{ $getData->service_order_no }}" required>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">ASC Name</label>
                    <div class="col-sm-12">
                        <select name="asc_id" id="asc_id" style="width:100%">
                            <option value="{{ $getData->asc_id }}" >{{ $getData->asc_name }}</option>
                        </select>
                        <input name="asc_name" id="asc_name" type="hidden" value="{{ $getData->asc_name }}" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Customer Name</label>
                    <div class="col-sm-12">
                        <input name="customer_name" placeholder="Customer Name" type="text" value="{{ $getData->customer_name }}" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Type Job</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="type_job" name="type_job" style="width:100%">
                            <option value="{{ $getData->type_job }}" selected>{{ $getData->type_job }}</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Defect Type Description</label>
                    <div class="col-sm-12">
                        <textarea name="defect_type" id="defect_type" class="form-control" rows="3" >{{ $getData->defect_type }}</textarea>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Code</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="engineer_code" name="engineer_code" style="width:100%">
                            <option value="{{ $getData->engineer_code }}" selected>{{ $getData->engineer_code }}</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Name</label>
                    <div class="col-sm-12">
                        <input name="engineer_name" id="engineer_name" placeholder="Engineer Name" type="text" value="{{ $getData->engineer_name }}" class="form-control" readonly>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Assist Engineer Name</label>
                    <div class="col-sm-12">
                        <input name="assist_engineer_name" placeholder="Assist Engineer Name" type="text" class="form-control" value="{{ $getData->assist_engineer_name }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Merk Brand</label>
                    <div class="col-sm-12">
                        <input name="merk_brand" placeholder="Merk Brand" type="text" class="form-control" value="{{ $getData->merk_brand }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Model Product</label>
                    <div class="col-sm-12">
                        <input name="model_product" placeholder="Model Product" type="text" class="form-control" value="{{ $getData->model_product }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Status</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="status" name="status" style="width:100%">
                            <option value="{{ $getData->status }}" selected>{{ $getData->status }}</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Reason</label>
                    <div class="col-sm-12">
                        <input name="reason" placeholder="Reason" type="text" class="form-control" value="{{ $getData->reason }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Remark Reason</label>
                    <div class="col-sm-12">
                        <input name="remark_reason" placeholder="Remark Reason" type="text" class="form-control" value="{{ $getData->remark_reason }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Pending Aging Days</label>
                    <div class="col-sm-12">
                        <input name="pending_aging_days" placeholder="Pending Aging Days" type="text" class="form-control" value="{{ $getData->pending_aging_days }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                #4 Completed Service Time
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Repair Completed Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="repair_completed_date" type="text" class="form-control date_format" value="{{ $getData->repair_completed_date->format('d-m-Y') }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Repair Completed Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="repair_completed_time" type="text" class="form-control time" value="{{ $getData->repair_completed_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Closed Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="closed_date" id="closed_date" type="text" class="form-control" value="{{ $getData->closed_date->format('d-m-Y') }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Closed Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="closed_time" type="text" class="form-control time" value="{{ $getData->closed_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Rating</label>
                    <div class="col-sm-12">
                        <input name="rating" placeholder="Rating" type="text" class="form-control" value="{{ $getData->rating }}">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                #2 Address
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Street</label>
                    <div class="col-sm-12">
                        <textarea name="street" placeholder="Street" class="form-control" rows="3">{{ $getData->street }}</textarea>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">City</label>
                    <div class="col-sm-12">
                        <input name="city" placeholder="City" type="text" class="form-control" value="{{ $getData->city }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Mobile)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_mobile" placeholder="Phone No (Mobile)" type="text" class="form-control number_only" value="{{ $getData->phone_no_mobile }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Home)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_home" placeholder="Phone No (Home)" type="text" class="form-control number_only" value="{{ $getData->phone_no_home }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Office)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_office" placeholder="Phone No (Office)" type="text" class="form-control number_only" value="{{ $getData->phone_no_office }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                3# Services Time
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Month</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="month" name="month" style="width:100%">
                            <option value="{{ $getData->month }}" selected>{{ $getData->month }}</option>
                        </select>
                        <small class="form-text text-muted">
                            Tahun <?php echo date("Y") ?>
                        </small>
                    </div>

                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Request Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="date" id="request_date" type="text" class="form-control date_format" value="{{ $getData->date->format('d-m-Y') }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Request Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="request_time" type="text" class="form-control time" value="{{ $getData->request_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Picked Order Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="engineer_picked_order_date" type="text" class="form-control date_format" value="{{ $getData->engineer_picked_order_date->format('d-m-Y') }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Picked Order Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="engineer_picked_order_time" type="text" class="form-control time" value="{{ $getData->engineer_picked_order_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Admin Assigned to Engineer Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="admin_assigned_to_engineer_date" type="text" class="form-control date_format" value="{{ $getData->admin_assigned_to_engineer_date->format('d-m-Y') }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Admin Assignned to Engineer Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="admin_assigned_to_engineer_time" type="text" class="form-control time" value="{{ $getData->admin_assigned_to_engineer_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Assigned Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="engineer_assigned_date" type="text" class="form-control date_format" value="{{ $getData->engineer_assigned_date->format('d-m-Y') }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Assigned Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="engineer_assigned_time" type="text" class="form-control time" value="{{ $getData->engineer_assigned_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Tech 1st Appointment Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="tech_1st_appointment_date" type="text" class="form-control date_format" value="{{ $getData->tech_1st_appointment_date->format('d-m-Y') }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Tech 1st Appointment Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="tech_1st_appointment_time" type="text" class="form-control time" value="{{ $getData->tech_1st_appointment_time }}">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">1st Visit Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="1st_visit_date" type="text" class="form-control date_format" value="{{ $getData->{'1st_visit_date'}->format('d-m-Y') }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">1st Visit Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="1st_visit_time" type="text" class="form-control time" value="{{ $getData->{'1st_visit_time'} }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> SAVE ORDERS DATA EXCEL</button>
        </div>
    </div>
</form>
@if(count($bundleData) > 0)
<div class="col-md-12" style="margin-top: 10px;">
    <form id="bundle-data-form">
        <input type="hidden" id="orders_data_excel_id" value="{{ $getData->id }}" name="orders_data_excel_id">
        <div class="card">
            <div class="card-header header-border">
                Parts Data Bundle
            </div>
            <div class="card-body">
                <div class="dynamic_bundle">
                    <div class="position-relative row form-group">
                        <div class="col-sm-7">
                            Bundle Name
                        </div>
                        <div class="col-sm-3">
                            Main Price
                        </div>
                        <div class="col-sm-1">
                            View Bundle Details
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            {{-- <button type="button" class="btn btn-success btn_add_bundle"><i class="fa fa-plus"></i></button> --}}
                        </div>
                    </div>

                    @foreach($bundleData as $key=> $bundle)
                        <?php $uniq = $key+1; ?>
                        <div class="position-relative row form-group dynamic_bundle_content" id="my_bundle_row_{{ $uniq }}">
                            <input type="hidden" value="{{ $bundle->id }}" name="bundle_data_id[]" />
                            <div class="col-sm-7">
                                <select name="part_data_stock_bundle_id[]" data-uniq="{{ $uniq }}" class="form-control part_data_stock_bundle_id_input-{{ $uniq }}" style="width:100%">
                                    @if($bundle->part_data_stock_bundle_id != null)
                                        <option value="{{ $bundle->part_data_stock_bundle_id }}" selected>{{ $bundle->bundle_name }}</option>
                                    @else
                                        <option value="" selected>--Pilih--</option>
                                    @endif
                                    <input type="hidden" name="bundle_name[]" data-uniq="{{ $uniq }}" class="form-control bundle_name_input-{{ $uniq }}" value="{{ $bundle->bundle_name }}"/>
                                    <input type="hidden" placeholder="name" name="part_data_stock_bundle_id_before[]" data-uniq="{{ $uniq }}" class="form-control part_data_stock_bundle_id_before_input-{{ $uniq }}" value="{{ $bundle->part_data_stock_bundle_id }}" />
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="main_price[]" data-uniq="{{ $uniq }}" class="form-control number_only main_price_input-{{ $uniq }}" value="{{ $bundle->main_price }}" readonly/>
                            </div>
                            <div class="col-sm-1 text-center">
                                <button type="button" data-uniq="{{ $uniq }}" class="btn btn-success bundle_details_data bundle_details_data-{{ $uniq }}" data-part_data_stock_bundle_id="{{ $bundle->part_data_stock_bundle_id }}" data-bundle_name="{{ $bundle->bundle_name }}"><i class="fa fa-bars"></i></button>
                            </div>
                            <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" data-uniq="{{ $uniq }}" data-part_data_excel_bundle_id="{{ $bundle->id }}" class="btn-transition btn btn-danger btn_remove_my_bundle remove_my_bundle-{{ $uniq }}"><i class="fa fa-close"></i></button>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="card-footer ">
                <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> SAVE BUNDLE DATA</button>
            </div>
        </div>
    </form>
</div>
@else
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Parts Data Bundle (OPTIONAL)
        </div>
        <form id="bundle-add-form">
            <input type="hidden" id="orders_data_excel_id" value="{{ $getData->id }}" name="orders_data_excel_id">
            <div class="card-body">
                <div class="dynamic_bundle">
                    <div class="position-relative row form-group">
                        <div class="col-sm-7">
                            Bundle Name
                        </div>
                        <div class="col-sm-3">
                            Main Price
                        </div>
                        <div class="col-sm-1">
                            View Bundle Details
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" class="btn btn-success btn_add_bundle"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="position-relative row form-group dynamic_bundle_content" id="my_bundle_row_xxx">
                        <div class="col-sm-7">
                            <select name="part_data_stock_bundle_id[]" data-uniq="xxx" class="form-control part_data_stock_bundle_id_input-xxx" style="width:100%">
                            </select>
                            <input type="hidden" name="bundle_name[]" data-uniq="xxx" class="form-control bundle_name_input-xxx"/>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="main_price[]" data-uniq="xxx" class="form-control main_price_input-xxx" readonly/>
                        </div>
                        <div class="col-sm-1 text-center">
                            <button type="button" data-uniq="xxx" class="btn btn-success bundle_details_data bundle_details_data-xxx" data-part_data_stock_bundle_id="" data-bundle_name=""><i class="fa fa-bars"></i></button>
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" data-uniq="xxx" data-part_data_excel_bundle_id="0" class="btn-transition btn btn-danger remove_my_bundle"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer ">
                <button type="submit" class="accept-modal btn btn-sm btn-primary" ><i class="fa fa-plus"></i> SAVE BUNDLE</button>
            </div>
        </form>
    </div>
</div>
@endif
<div class="col-md-12" style="margin-top: 10px;">
    <form id="part-data-form">
        <input type="hidden" id="orders_data_excel_id" value="{{ $getData->id }}" name="orders_data_excel_id">
        <div class="card">
            <div class="card-header header-border">
                Parts Data
            </div>
            <div class="card-body">
                <div class="dynamic_part">
                    <div class="position-relative row form-group">
                        <div class="col-sm-2">
                            Code Material
                        </div>
                        <div class="col-sm-3">
                            Part Description
                        </div>
                        <div class="col-sm-2">
                            Quantity
                        </div>
                        <div class="col-sm-2">
                            Stock Available
                        </div>
                        <div class="col-sm-2 ">
                            Order By Customer (Qty x Price)
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" class="btn btn-success btn_add_my_part"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                    @foreach($partData as $key=> $part)
                        <?php $uniq = $key+1; ?>
                        <div class="position-relative row form-group dynamic_sparepart_content" id="my_part_row_{{ $uniq }}">
                            <input type="hidden" value="{{ $part->id }}" name="part_data_id[]" />
                            <div class="col-sm-2">
                                <select name="part_data_stock_inventories_id[]" data-uniq="{{ $uniq }}" class="cd_disabled form-control part_data_stock_inventories_id_input-{{ $uniq }}" required style="width:100%" readonly>
                                    @if($part->part_data_stock_inventories_id != null)
                                        <option value="{{ $part->part_data_stock_inventories_id }}" selected>{{ $part->part_data_stock_inventory->part_data_stock->code_material }}</option>
                                    @else
                                        <option value="" selected>--Pilih--</option>
                                    @endif
                                </select>
                                <input type="hidden" placeholder="name" name="part_data_stock_inventories_id_before[]" data-uniq="{{ $uniq }}" class="form-control part_data_stock_inventories_id_before-{{ $uniq }}" value="{{ $part->part_data_stock_inventories_id }}" />
                                <input type="hidden" placeholder="name" name="code_material[]" data-uniq="{{ $uniq }}" class="form-control code_material_input-{{ $uniq }}" value="{{ $part->part_data_stock_inventory->part_data_stock->code_material }}" />
                                <input type="hidden" name="pdesi_id[]" data-uniq="{{ $uniq }}" class="form-control pdesi_id_input-{{ $uniq }}" value="{{ $part->part_data_stock_inventories_id }}" />
                            </div>
                            <div class="col-sm-3">
                                <input type="text" placeholder="Part Description" name="part_description[]" data-uniq="{{ $uniq }}" class="form-control part_description_input-{{ $uniq }}" value="{{ $part->part_data_stock_inventory->part_data_stock->part_description }}" readonly/>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" placeholder="unit" name="quantity[]" data-uniq="{{ $uniq }}" class="form-control number_only quantity_input-{{ $uniq }}" value="{{ $part->quantity }}" required/>
                            </div>
                            <div class="col-sm-2 text-center">
                                <input type="text" name="stock_part[]" data-uniq="{{ $uniq }}" class="stock_part_input-{{ $uniq }} form-control" value="{{ $part->part_data_stock_inventories_id != null ? $part->part_data_stock_inventory->stock : 'kosong' }}" readonly/>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-check">

                                    <input name="is_order_by_customer_check[]" class="form-check-input big-checkbox is_order_by_customer_check-{{ $uniq }}" type="checkbox" data-uniq="{{ $uniq }}">
                                    <input type="hidden" name="is_order_by_customer[]" class="is_order_by_customer-{{ $uniq }}" value={{ $part->is_order_by_customer }} data-uniq="{{ $uniq }}">
                                    <label class="form-check-label" for="flexCheckDefault">
                                        <input type="text" name="selling_price[]" data-part_data_stock_inventories_id="{{ $part->part_data_stock_inventories_id }}" data-uniq="{{ $uniq }}" class="selling_price-{{ $uniq }} form-control" value="{{ $part->selling_price }}" readonly/>
                                        <input type="hidden" name="price_hpp_average[]" data-part_data_stock_inventories_id="{{ $part->part_data_stock_inventories_id }}" data-uniq="{{ $uniq }}" class="price_hpp_average-{{ $uniq }} form-control" value="{{ $part->price_hpp_average }}" readonly/>
                                        <input type="hidden" name="selling_price_pcs[]" data-part_data_stock_inventories_id="{{ $part->part_data_stock_inventories_id }}" data-uniq="{{ $uniq }}" class="selling_price_pcs-{{ $uniq }}" value="{{ $part->selling_price_pcs }}" readonly/>
                                    </label>
                                </div>
                            </div>

                            <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" data-part_data_id="{{ $part->id }}" data-uniq="{{ $uniq }}" class="btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="card-footer ">
                <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> SAVE PART DATA</button>
            </div>
        </div>
    </form>
</div>

<div class="col-md-12" style="margin-top: 10px;">
    <form id="jasa-data-form">
        <input type="hidden" id="orders_data_excel_id" value="{{ $getData->id }}" name="orders_data_excel_id">
        <div class="card">
            <div class="card-header header-border">
                Jasa Data
            </div>
            <div class="card-body">
                <div class="dynamic_jasa">
                    <div class="position-relative row form-group">
                        <div class="col-sm-6">
                            Jasa/Labor
                        </div>
                        <div class="col-sm-5">
                            Price
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" class="btn btn-success btn_add_my_jasa"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                    @foreach($jasaData as $key=> $jasa)
                        <?php $uniq = $key+1; ?>
                        <div class="position-relative row form-group dynamic_jasa_content" id="my_jasa_row_{{ $uniq }}">
                            <input type="hidden" value="{{ $jasa->id }}" name="jasa_data_id[]" />
                            <div class="col-sm-6">
                                <select name="jasa_id[]" data-uniq="{{ $uniq }}" class="cd_disabled form-control jasa_id_input-{{ $uniq }}" required style="width:100%" >
                                    @if($jasa->jasa_id != null)
                                        <option value="{{ $jasa->jasa_id }}" selected>{{ $jasa->jasa_name }}</option>
                                    @else
                                        <option value="" selected>--Pilih--</option>
                                    @endif
                                </select>
                                <input type="hidden" placeholder="name" name="jasa_name[]" data-uniq="{{ $uniq }}" class="form-control jasa_name_input-{{ $uniq }}" value="{{ $jasa->jasa_name }}" />
                            </div>
                            <div class="col-sm-5">
                                <input type="text" placeholder="unit" name="price_j[]" data-uniq="{{ $uniq }}" class="form-control number_only price_j_input-{{ $uniq }}" value="{{ $jasa->price }}" readonly/>
                            </div>
                            <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" data-jasa_data_id="{{ $jasa->id }}" data-uniq="{{ $uniq }}" class="btn btn-danger btn_remove_my_jasa"><i class="fa fa-close"></i></button>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="card-footer ">
                <button type="submit" class="accept-modal btn btn-sm btn-primary" ><i class="fa fa-plus"></i> SAVE PART DATA</button>
            </div>
        </div>
    </form>
</div>

<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Orders Data Excel Logs
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-orders-data-excel-logs" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Type Modified</th>
                        <th>By User (Name)</th>
                        <th>Created At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal Bundle Details -->
<div class="modal fade" id="bundle_details_modal" data-backdrop="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Bundle Details for <span id="bundle_name"></span></h5>
            </div>
            <div class="modal-body">
                <table id="table-bundle-details" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <tr>
                                <th>No.</th>
                                <th>Code Material</th>
                                <th>Part Description</th>
                                <th>Quantity</th>
                                <th>Hpp Average</th>
                                <th>Selling Price</th>
                            </tr>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="close_bundle_details_modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
@include('admin.master.orders-data-excel._js')
<style>
    elect[readonly].select2-hidden-accessible + .select2-container {
        pointer-events: none;
        touch-action: none;
    }
    select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
    background: #eee;
    box-shadow: none;
    }
    select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow,
    select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
        display: none;
    }
</style>
<script>
    // $('.cd_disabled').attr("readonly", "readonly");
    var option_remove =[];
    var id = $('input[name="id"]').val();
    var data = [
        {id: 'Januari', text: 'Januari', title: 'Januari'},
        {id: 'Februari', text: 'Februari', title: 'Februari'},
        {id: 'Maret', text: 'Maret', title: 'Maret'},
        {id: 'April', text: 'April', title: 'April'},
        {id: 'Mei', text: 'Mei', title: 'Mei'},
        {id: 'Juni', text: 'Juni', title: 'Juni'},
        {id: 'Juli', text: 'Juli', title: 'Juli'},
        {id: 'Agustus', text: 'Agustus', title: 'Agustus'},
        {id: 'September', text: 'September', title: 'September'},
        {id: 'Oktober', text: 'Oktober', title: 'Oktober'},
        {id: 'November', text: 'November', title: 'November'},
        {id: 'Desember', text: 'Desember', title: 'Desember'},
    ];

    $("#month").select2({
        data: data,
        escapeMarkup: function(markup) {
            return markup;
        }
    })


    $(function() {
        ClassApp.select2CodeMaterialLoad();
        ClassApp.select2JasaLoad();
        ClassApp.select2BundleLoad();

        $('.time').inputmask("hh:mm:ss", {
            placeholder: "hh:mm:ss",
            insertMode: false,
            showMaskOnHover: false,
            hourFormat: "24",
            clearIncomplete: true
        });
        $('.date').inputmask("dd-mm-yyyy", {
            placeholder: "dd-mm-yyyy",
            clearIncomplete: true
        });
        Helper.dateFormat('.date_format');

        $('input[name="selling_price[]"]').hide();
        $('input[name="is_order_by_customer[]"]').each(function() {
            var xx = $(this).attr('data-uniq');
            var el_check = '.is_order_by_customer-'+xx+'';
            if($(el_check).val() == 1) {
                $('.is_order_by_customer_check-'+xx+'').attr('checked', true);
                $('.selling_price-'+xx+'').show();
            }
        });

        //order by customer hiden -> selling_price
        $(document).on('click', 'input[name="is_order_by_customer_check[]"]', function() {
            var xx = $(this).attr('data-uniq');
            var checked = 0;
            if($('.quantity_input-'+xx+'').val() != "") {
                var checked = 0;
                if($('.is_order_by_customer_check-'+xx+'').is(':checked')) {
                    checked = 1;
                    console.log(checked);
                }
                $('.is_order_by_customer-'+xx+'').val(checked);
                ClassApp.checkIsOrderByCustomer(xx);

                if($('.selling_price-'+xx+'').val() === 0) {
                    Helper.errorNotif('Unable to select, Selling Price is Rp. 0')
                    $('.is_order_by_customer_check-'+xx+'').prop('checked',false).trigger("change");
                    $('.selling_price-'+xx+'').hide();
                } else if ($('.selling_price-'+xx+'').val() == "") {
                    ClassApp.getSellingPriceAll($('.pdesi_id_input-'+xx+'').val(),xx);
                }
                ClassApp.hitungSellingPriceTotalOrderByCustomer(xx);
            } else {
                $('.is_order_by_customer_check-'+xx+'').prop('checked',false).trigger("change");
                $('.quantity_input-'+xx+'').focus();
                Helper.warningNotif('Isi terlebih Dahulu Quantity !');
            }
        });

         //QUantity * Price hpp
         $(document).on('keyup', 'input[name="quantity[]"]', function() {
            var xx = $(this).attr('data-uniq');
            ClassApp.hitungSellingPriceTotalOrderByCustomer(xx);
        });


    });

    Helper.onlyNumberInput('.number_only');

    $("#form-data").submit(function(e) {

        var form = Helper.serializeForm($(this));
        var fd = new FormData($(this)[0]);
        console.log(form, data);

        $.ajax({
            url:Helper.apiUrl('/orders-data-excel/'+id+''),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Orders Data Excel Has Been Saved',
                    });
                    // window.location.href = Helper.redirectUrl('/admin/orders-data-excel/show');
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });

                    })
                }

            },
        });
        e.preventDefault();
    });

    SparepartClass = {
        create: function(xx) {
            $('.btn_add_my_part').prop("disabled", true);
            $('.dynamic_part').append((`

                <div class="position-relative row form-group" id="my_part_row_${xx}">
                    <input type="hidden" value="0" name="part_data_id[]">
                    <div class="col-sm-2">
                        <select name="part_data_stock_inventories_id[]" data-uniq="${xx}" class="form-control part_data_stock_inventories_id_input-${xx}" required style="width:100%">
                        </select>
                        <input type="hidden" name="code_material[]" data-uniq="${xx}" class="code_material_input-${xx} form-control" readonly/>
                        <input type="hidden" name="part_data_stock_inventories_id_before[]" data-uniq="${xx}" class="form-control part_data_stock_inventories_id_before-${xx}" value="" />
                    </div>
                    <div class="col-sm-3">
                        <input type="text" placeholder="Part Description" name="part_description[]" data-uniq="${xx}" class="part_description_input-${xx} form-control" readonly/>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" placeholder="unit" name="quantity[]" data-uniq="${xx}" class=" quantity_input-${xx} number_only form-control" required/>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" name="stock_part[]" data-uniq="${xx}" class="stock_part_input-${xx} form-control" readonly/>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-check">
                            <input name="is_order_by_customer_check[]" data-uniq="${xx}" class="form-check-input big-checkbox is_order_by_customer_check-${xx}" type="checkbox">
                            <input type="hidden" name="is_order_by_customer[]" class="is_order_by_customer-${xx}" data-uniq="${xx}" value=0>
                            <label class="form-check-label" for="flexCheckDefault">
                                <input type="text" name="selling_price[]" data-uniq="${xx}" data-part_data_stock_inventories_id="" class="selling_price-${xx} form-control" readonly/>
                                <input type="hidden" name="price_hpp_average[]" data-uniq="${xx}" data-part_data_stock_inventories_id="" class="price_hpp_average-${xx} form-control" readonly/>
                                <input type="hidden" name="selling_price_pcs[]" data-part_data_stock_inventories_id="" data-uniq="${xx}" class="selling_price_pcs-${xx}"/>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-1" style="padding-left:0px;">
                        <button type="button" data-part_data_id="0" data-uniq="${xx}" class="btn-transition btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                    </div>

                </div>
            `));
            if($('#asc_name').val() != "") {
                $('select[name="part_data_stock_inventories_id[]"]').prop("disabled", false);
            } else {
                $('select[name="part_data_stock_inventories_id[]"]').prop("disabled", true);
            }
            Helper.onlyNumberInput('.number_only');
            ClassApp.select2CodeMaterial(xx,$('#asc_id').val());
            ClassApp.checkStock(xx);
            $('.is_order_by_customer_check-'+xx+'').prop("disabled", true);
            $('.selling_price-'+xx+'').hide();
        },
        delete: function($el) {
            var uniq = $el.attr("data-uniq");
            var part_data_id = parseInt($el.attr("data-part_data_id"));

            if (part_data_id != 0) {
                Helper.confirm(function() {
                    Axios.delete('/orders-data-excel/destroy_part_data/' + part_data_id)
                        .then(function(response) {
                            event.preventDefault();
                            //do something
                            $(this).prop('disabled', true);
                            Helper.successNotif('Success Delete');
                            $('#my_part_row_' + uniq + '').remove();
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
            } else {
                $('#my_part_row_' + uniq + '').remove();
            }
        },
        createJasa: function(xx) {
            $('.btn_add_my_jasa').prop("disabled", true);
            $('.dynamic_jasa').append((`
                <div class="position-relative row form-group" id="my_jasa_row_${xx}">
                    <input type="hidden" value="0" name="jasa_data_id[]">
                    <div class="col-sm-6">
                        <select name="jasa_id[]" data-uniq="${xx}" class="form-control jasa_id_input-${xx}" required style="width:100%">
                        </select>
                        <input type="hidden" name="jasa_name[]" data-uniq="${xx}" class="jasa_name_input-${xx} form-control"/>
                    </div>
                    <div class="col-sm-5">
                        <input type="text" placeholder="Price" name="price_j[]" data-uniq="${xx}" class=" price_j_input-${xx} number_only form-control" readonly/>
                    </div>
                    <div class="col-sm-1" style="padding-left:0px;">
                        <button type="button" data-jasa_data_id="0" data-uniq="${xx}" class="btn-transition btn btn-danger btn_remove_my_jasa"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            `));
            Helper.onlyNumberInput('.number_only');
            ClassApp.select2Jasa(xx);
            $('#btn_save_jasa').prop('disabled', false);
        },
        deleteJasa: function($el) {
            var uniq = $el.attr("data-uniq");
            var jasa_data_id = parseInt($el.attr("data-jasa_data_id"));
            if (jasa_data_id != 0) {
                Helper.confirm(function() {
                    Axios.delete('/orders-data-excel/destroy_jasa_data/' + jasa_data_id)
                        .then(function(response) {
                            Helper.successNotif('Success Delete');
                            $('#my_jasa_row_' + uniq + '').remove();
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
            } else {
                $('#my_jasa_row_' + uniq + '').remove();
            }
        },
        createBundle: function (xx) {

            $('.dynamic_bundle').append((`
                <div class="position-relative row form-group" id="my_bundle_row_${xx}">
                    <div class="col-sm-7">
                        <select name="part_data_stock_bundle_id[]" data-uniq="${xx}" class="form-control part_data_stock_bundle_id_input-${xx}" required style="width:100%">
                        </select>
                        <input type="hidden" name="bundle_name[]" data-uniq="${xx}" class="form-control bundle_name_input-${xx}"/>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" name="main_price[]" data-uniq="${xx}" class="main_price_input-${xx} form-control" readonly/>
                    </div>
                    <div class="col-sm-1  text-center">
                        <button type="button" data-uniq="${xx}" class="btn btn-success bundle_details_data bundle_details_data-${xx}" data-part_data_stock_bundle_id="" data-bundle_name=""><i class="fa fa-bars"></i></button>
                    </div>
                    <div class="col-sm-1" style="padding-left:0px;">
                        <button type="button" data-uniq="${xx}" data-part_data_excel_bundle_id="0" class="btn-transition btn btn-danger remove_my_bundle remove_my_bundle_${xx}"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            `));
            if($('#asc_name').val() != "") {
                $('select[name="part_data_stock_bundle_id[]"]').prop("disabled", false);
            } else {
                $('select[name="part_data_stock_bundle_id[]"]').prop("disabled", true);
            }
            Helper.onlyNumberInput('.number_only');
            ClassApp.select2Bundle(xx,$('#asc_id').val());
            $('.bundle_details_data-'+xx+'').prop("disabled", true);

        },
        deleteBundle: function($el) {
            var uniq = $el.attr("data-uniq");
            var bundle_id = parseInt($el.attr('data-part_data_excel_bundle_id'));
            if (bundle_id != 0) {
                Helper.confirm(function() {
                    Axios.delete('/orders-data-excel/destroy_bundle_data/' + bundle_id)
                        .then(function(response) {
                            Helper.successNotif('Success Delete');
                            $('#my_bundle_row_' + uniq + '').remove();
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
            } else {
                $('#my_bundle_row_' + uniq + '').remove();
            }
        },
    };

    $(function() {
        var xx = 1;
        Helper.onlyNumberInput('.quantity_i');
        $('#part-data-form')
            .submit(function(e) {
                var data = Helper.serializeForm($(this));
                var fd = new FormData($(this)[0]);
                fd.append('repair_completed_date', $('input[name="repair_completed_date"]').val());
                fd.append('service_order_no', $('input[name="service_order_no"]').val());
                fd.append('defect_type', $('#defect_type').val());
                fd.append('asc_name', $('input[name="asc_name"]').val());
                Axios.post('/orders-data-excel/update_part_data/' + data.orders_data_excel_id, fd)
                    .then(function(response) {
                        if (response.status == 204) {
                            Helper.successNotif(Helper.deleteMsg());
                        } else {
                            Helper.successNotif(response.data.msg);
                        }

                        location.reload();

                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
                e.preventDefault();
            });

        ClassApp.preventDuplicatePartSelect();
        $(document)
            .on('click', '.btn_add_my_part', function() {
                console.log('asd')
                SparepartClass.create(new Date().getTime() + xx)
                xx++;
                ClassApp.preventDuplicatePartSelect();
            })

        // add row part
        $(document)
            .on('focus', 'input[name="code_material[]"], input[name="part_description[]"]', function(e) {
                if ($(this).is('input[name="code_material[]"]:last')) {
                    SparepartClass.create(new Date().getTime() + xx)
                    xx++;
                }

                if ($(this).is('input[name="quantity[]"]:last')) {
                    SparepartClass.create(new Date().getTime() + xx)
                    xx++;
                }
            })

        // delete part my inv
        $(document)
            .on('click', '.btn_remove_my_part', function() {
                $('.btn_add_my_part').prop("disabled", false);
                SparepartClass.delete($(this));
            });

        //Jasa
        $('#jasa-data-form')
            .submit(function(e) {
                var data = Helper.serializeForm($(this));
                var fd = new FormData($(this)[0]);
                Axios.post('/orders-data-excel/update_jasa_data/' + data.orders_data_excel_id, fd)
                    .then(function(response) {
                        if (response.status == 204) {
                            Helper.successNotif(Helper.deleteMsg());
                        } else {
                            Helper.successNotif(response.data.msg);
                        }
                        location.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
                e.preventDefault();
            });

        ClassApp.preventDuplicateJasaSelect();
        $(document)
            .on('click', '.btn_add_my_jasa', function() {
                console.log('asd')
                SparepartClass.createJasa(new Date().getTime() + xx)
                xx++;
                ClassApp.preventDuplicateJasaSelect();
            })

        // delete part my inv
        $(document)
            .on('click', '.btn_remove_my_jasa', function() {
                $('.btn_add_my_jasa').prop("disabled", false);
                SparepartClass.deleteJasa($(this));
            });


        //Bundle
        $('#bundle-data-form')
            .submit(function(e) {
                var data = Helper.serializeForm($(this));
                var fd = new FormData($(this)[0]);
                Axios.post('/orders-data-excel/update_bundle_data/' + data.orders_data_excel_id, fd)
                    .then(function(response) {
                        if (response.status == 204) {
                            Helper.successNotif(Helper.deleteMsg());
                        } else {
                            Helper.successNotif(response.data.msg);
                        }
                        location.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
                e.preventDefault();
            });

            //Bundle
        $('#bundle-add-form')
            .submit(function(e) {
                var data = Helper.serializeForm($(this));
                var fd = new FormData($(this)[0]);
                Axios.post('/orders-data-excel/add_bundle_data', fd)
                    .then(function(response) {
                        if (response.status == 204) {
                            Helper.successNotif(Helper.deleteMsg());
                        } else {
                            Helper.successNotif(response.data.msg);
                        }
                        location.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
                e.preventDefault();
            });

        $(document)
            .on('click', '.btn_add_bundle', function() {
                console.log('asd')
                SparepartClass.createBundle(new Date().getTime() + xx)
                xx++;
            })

         // delete my bundle
         $(document)
            .on('click', '.remove_my_bundle', function() {
                SparepartClass.deleteBundle($(this));
            });


        // Table Bundle Details
        $(document)
            .on('click', '.bundle_details_data', function() {
                var id = $(this).attr("data-part_data_stock_bundle_id");
                var bundle_name = $(this).attr("data-bundle_name");
                $('#bundle_name').html(bundle_name);
                var url = '/part-data-excel-stock-bundle/details-data/'+id+'';
                ClassApp.initTablesBundleDetails(url);
                $('#bundle_details_modal').modal('show');
            });
    })


</script>

@endsection
