<link rel="stylesheet" href="{{ asset('adminAssets/css/easy-autocomplete.min.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/easy-autocomplete.themes.min.css') }}">
{{-- Select2 with description --}}
<style>
    .rounded-profile {
        border-radius: 100% !important;
        overflow: hidden;
        width: 100px;
        height: 100px;
        border: 8px solid rgba(255, 255, 255, 0.7);
    }

    wrap {
        width: 500px;
        margin: 2em auto;
    }

    .clearfix:before,
    .clearfix:after {
        content: " ";
        display: table;
    }

    .clearfix:after {
        clear: both;
    }

    .select2-result-repository {
        padding-top: 4px;
        padding-bottom: 3px;
    }

    .select2-result-repository__avatar {
        float: left;
        width: 60px;
        height: auto;
        margin-right: 10px;
    }

    .select2-result-repository__avatar img {
        width: 100%;
        height: auto;
        border-radius: 50%;
    }

    .select2-result-repository__meta {
        /* margin-left: 70px; */
        margin-left: 0px;
    }

    .select2-result-repository__title {
        color: black;
        font-weight: bold;
        word-wrap: break-word;
        line-height: 1.1;
        margin-bottom: 4px;
    }

    .select2-result-repository__price,
    .select2-result-repository__stargazers {
        margin-right: 1em;
    }

    .select2-result-repository__price,
    .select2-result-repository__stargazers,
    .select2-result-repository__watchers {
        display: inline-block;
        color: rgb(68, 68, 68);
        font-size: 13px;
    }

    .select2-result-repository__description {
        font-size: 13px;
        color: #777;
        margin-top: 4px;
    }

    .select2-results__option--highlighted .select2-result-repository__title {
        color: white;
    }

    .select2-results__option--highlighted .select2-result-repository__price,
    .select2-results__option--highlighted .select2-result-repository__stargazers,
    .select2-results__option--highlighted .select2-result-repository__description,
    .select2-results__option--highlighted .select2-result-repository__watchers {
        color: #c6dcef;
    }
</style>
<script type="text/javascript" src="{{ asset('adminAssets/js/jquery.easy-autocomplete.min.js') }}"></script>
<script>

    globalCRUD.select2('#asc_id', '/asc-excel/select2');
    $('#asc_id').on('select2:select', function (e) {
        $('#asc_name').val(e.params.data.text);
    });

    $(function() {
        $('#closed_date').prop('readonly', true);
    })

    $(document).on('change', '#asc_id', function() {
        if($(this).val() != "") {
            $('select[name="part_data_stock_inventories_id[]"]').prop("disabled", false);
            $('select[name="part_data_stock_bundle_id[]"]').prop("disabled", false);
            $('.notice_asc_name').hide();
            $('.btn_add_my_part').prop("disabled", false);
            $('.btn_add_bundle').prop("disabled", false);
            ClassApp.select2CodeMaterial('xxx',$(this).val());
            ClassApp.select2Bundle('xxx',$(this).val());

        }
    })

    $(document).on('change','#closed_date', function(e) {
        if($('input[name="date"]').val() != "") {
            var startDate = moment($('#request_date').val(), "DD.MM.YYYY");
            var endDate = moment($('#closed_date').val(), "DD.MM.YYYY");
            var result = endDate.diff(startDate, 'days');
            if(result >= 0) {
                $('input[name="pending_aging_days"]').val(result);
            } else {
                Helper.warningNotif('Closed Date cant less than Request Date')
                $(this).val('');
            }
        }

    });
    $(document).on('change', '#request_date', function(e) {
        if($(this).val() != "") {
            $('#closed_date').prop('disabled', false);
            $('#closed_date').val('');
            $('.warning_closed_date').hide();
            $('#closed_date').datetimepicker({
                minDate: $('#request_date').val(),
                format: 'd-m-Y',
                formatDate: 'd-m-Y',
                timepicker: false,
                scrollInput: false
                // inline:true,
            });
        }
    });

    globalCRUD.select2('#engineer_code', '/engineer-code-excel/select2', function(item) {
                return {
                    id: item.engineer_code,
                    text:item.engineer_code,
                    engineer_name:item.engineer_name,
                }
            });

    globalCRUD.select2('#type_job', '/type-job-excel/select2', function(item) {
                return {
                    id: item.name,
                    text:item.name,
                }
            });
    globalCRUD.select2('#status', '/status-excel/select2', function(item) {
                return {
                    id: item.status,
                    text:item.status,
                }
            });

    $('#engineer_code').on('select2:select', function (e) {
        $('#engineer_name').val(e.params.data.engineer_name);
    });

    // var data_stock_id = [];
    var ClassApp = {
        select2CodeMaterialLoad : function() {
            $('select[name="part_data_stock_inventories_id[]"]').each(function(){
                var uniq = $(this).attr('data-uniq');
                ClassApp.select2CodeMaterial(uniq,$('#asc_id').val())
                ClassApp.checkStock(uniq)
                $(document).on('change', '#asc_id', function() {
                    if($(this).val() != "") {
                        $('select[name="part_data_stock_inventories_id[]"]').prop("disabled", false);
                        $('.notice_asc_name').hide();
                        $('.btn_add_my_part').prop("disabled", false);
                        ClassApp.select2CodeMaterial(uniq,$(this).val());
                    }
                })
            });
        },

        select2JasaLoad : function() {
            $('select[name="jasa_id[]"]').each(function(){
                var uniq = $(this).attr('data-uniq');
                ClassApp.select2Jasa(uniq)
            });
        },

        select2BundleLoad : function() {
            $('select[name="part_data_stock_bundle_id[]"]').each(function(){
                var uniq = $(this).attr('data-uniq');
                ClassApp.select2Bundle(uniq,$('#asc_id').val());
                $(document).on('change', '#asc_id', function() {
                    if($(this).val() != "") {
                        $('select[name="part_data_stock_bundle_id[]"]').prop("disabled", false);
                        $('.notice_asc_name').hide();
                        $('.btn_add_bundle').prop("disabled", false);
                        ClassApp.select2Bundle(uniq,$(this).val());
                    }
                })
            });
        },

        select2Bundle : function(xx, asc_id=null) {
            globalCRUD.select2('.part_data_stock_bundle_id_input-'+xx+'', '/part-data-excel-stock-bundle/select2/'+asc_id+'', function(item) {
                return {
                    id: item.id,
                    text:item.name,
                    main_price:item.main_price,
                    available:item.available
                }
            });
            $('.part_data_stock_bundle_id_input-'+xx+'').on('select2:select', function (e) {
                if(e.params.data.available !== 0) {
                    $('.bundle_name_input-'+xx+'').val(e.params.data.text);
                    $('.main_price_input-'+xx+'').val(e.params.data.main_price);
                    $('.remove_bundle_'+xx+'').attr('data-part_data_stock_bundle_id',e.params.data.id);
                    $('.bundle_details_data-'+xx+'').attr('data-part_data_stock_bundle_id',e.params.data.id);
                    $('.bundle_details_data-'+xx+'').attr('data-bundle_name',e.params.data.text);
                    $('.btn_add_bundle').prop("disabled", false);
                    $('.bundle_details_data-'+xx+'').prop("disabled", false);
                } else {
                    Helper.errorNotif("One of the stock parts in the bundle out of stock !");
                    $('.part_data_stock_bundle_id_input-'+xx+'').val('').change();
                    $('.main_price_input-'+xx+'').val('').change();
                }

            });
        },

        select2CodeMaterial : function(xx, asc_id=null) {
            $('.part_data_stock_inventories_id_input-'+xx+'').select2({
                // minimumInputLength: 1,
                ajax: {
                    url: Helper.apiUrl('/part-data-excel-stock-inventory/select2/code_material/'+asc_id+'/filter_order'),
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: $.map(data.data, ClassApp.renderSelect2CodeMaterial(data.data))
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateSelection: function(data) {
                    return data.name || data.text;
                }
            });

            $('.part_data_stock_inventories_id_input-'+xx+'').on('select2:select', function (e) {
                $('.selling_price-'+xx+'').hide();
                $('input[name="is_order_by_customer_check[]"]').prop("disabled", false);
                $('.is_order_by_customer_check-'+xx+'').prop("checked", false);
                $('.remove_my_part_'+xx+'').attr('data-part_data_stock_inventories_id',e.params.data.id);
                $('.part_description_input-'+xx+'').val(e.params.data.part_description);
                $('.code_material_input-'+xx+'').val(e.params.data.name);
                $('.stock_part_input-'+xx+'').val(e.params.data.stock);
                $('.selling_price_pcs-'+xx+'').val(e.params.data.selling_price);
                $('.price_hpp_average-'+xx+'').val(e.params.data.hpp_average);
                $('.btn_add_my_part').prop("disabled", false);

            });
        },

        renderSelect2CodeMaterial: function(data) {
            return function(data) {
                return {
                    id: data.id,
                    text: "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__meta'>" +
                        "<div class='select2-result-repository__title'><b>" + data.part_data_stock.code_material + "</b></div>" +
                        "<div class='select2-result-repository__description'>Part Desription : <b>" + data.part_data_stock.part_description + "</b></div>" +
                        "</div></div>",
                    data: data,
                    part_description:data.part_data_stock.part_description,
                    selling_price:data.selling_price,
                    stock:data.stock,
                    name:data.part_data_stock.code_material,
                    hpp_average:data.hpp_average
                };
            };
        },

        select2Jasa : function(xx) {
            globalCRUD.select2('.jasa_id_input-'+xx+'', '/jasa-excel/select2', function(item) {
                return {
                    id: item.id,
                    text:item.name,
                    price:item.price,
                    // desc:item.desc
                }
            });
            $('.jasa_id_input-'+xx+'').on('select2:select', function (e) {
                $('.remove_my_jasa_'+xx+'').attr('data-jasa_id',e.params.data.id);
                $('.jasa_name_input-'+xx+'').val(e.params.data.text);
                $('.price_j_input-'+xx+'').val(e.params.data.price);
                $('.btn_add_my_jasa').prop("disabled", false);
            });
        },

        checkStock: function(xx) {
            $('.quantity_input-' + xx + '').keyup(function(e) {
                var max = parseInt($('.stock_part_input-' + xx + '').val());
                if (parseInt($(this).val()) > max) {
                    e.preventDefault();
                    $(this).val(max);
                } else {
                    $(this).val();
                }
            });
        },

        checkIsOrderByCustomerLoad : function() {
            $('input[name="is_order_by_customer_check[]"]').each(function(){
                var uniq = $(this).attr('data-uniq');
                ClassApp.checkIsOrderByCustomer(uniq);
            });

        },

        checkIsOrderByCustomer: function(xx) {
            $('.selling_price-'+xx+'').toggle(this.checked);
        },

        getHppAverageAll: function(pdes_id, out_stock, xx) {
            $.ajax({
                url:Helper.apiUrl('/general-journal/part/price_hpp_average_all/'+pdes_id+'/'+out_stock+''),
                type: 'get',
                success: function(response) {
                    var nilai = 0;
                    if(response.data!= null) {
                        nilai = response.data;
                    }
                    $('.price_hpp_average-'+xx+'').val(nilai * parseInt($('.quantity_input-'+xx+'').val()));
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });
        },

        getSellingPriceAll: function(pdesi_id, xx) {
            $.ajax({
                url:Helper.apiUrl('/part-data-excel-stock-inventory/get-selling-price/'+pdesi_id+''),
                type: 'get',
                success: function(response) {
                    var nilai = 0;
                    if(response.data!= null) {
                        nilai = response.data;
                    }
                    $('.selling_price-'+xx+'').val(nilai * parseInt($('.quantity_input-'+xx+'').val()));
                    $('.selling_price_pcs-'+xx+'').val(nilai);
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });
        },

        hitungTotalOrderByCustomer: function(xx) {
            var nilai = parseInt($('.quantity_input-'+xx+'').val()) * parseInt($('.price_hpp_average_pcs-'+xx+'').val());
            console.log(nilai)
            $('.price_hpp_average-'+xx+'').val(nilai);
        },

        hitungSellingPriceTotalOrderByCustomer: function(xx) {
            var nilai = parseInt($('.quantity_input-'+xx+'').val()) * parseInt($('.selling_price_pcs-'+xx+'').val());
            $('.selling_price-'+xx+'').val(nilai);
        },


        preventDuplicatePartSelect: function() {
            $('select[name="part_data_stock_inventories_id[]"]').change(function () {
                var xx = $(this).attr('data-uniq');
                if ($('select[name="part_data_stock_inventories_id[]"] option[value="' + $(this).val() + '"]:selected').length > 1) {
                    $(this).val('').change();
                    $('.part_description_input-'+xx+'').val('').change();
                    $('.quantity_input-'+xx+'').val('').change();
                    $('.stock_part_input-'+xx+'').val('').change();
                    Helper.warningNotif('You have already selected this Part previously, please choose another!');
                }
            });
        },
        preventDuplicateJasaSelect: function() {
            $('select[name="jasa_id[]"]').change(function () {
                var xx = $(this).attr('data-uniq');
                if ($('select[name="jasa_id[]"] option[value="' + $(this).val() + '"]:selected').length > 1) {
                    $(this).val('').change();
                    $('.price_j_input-'+xx+'').val('').change();
                    Helper.warningNotif('You have already selected this Jasa previously, please choose another!');
                }
            });
        },
        preventDuplicateBundleSelect: function() {
            $('select[name="part_data_stock_bundle_id[]"]').change(function () {
                var xx = $(this).attr('data-uniq');
                if ($('select[name="part_data_stock_bundle_id[]"] option[value="' + $(this).val() + '"]:selected').length > 1) {
                    $(this).val('').change();
                    $('.main_price_input-'+xx+'').val('').change();
                    Helper.warningNotif('You have already selected this Bundle previously, please choose another!');
                }
            });
        },

        initTablesBundleDetails: function(url) {
            var tbl = globalCRUD.datatables({
                url: url,
                selector: '#table-bundle-details',
                columnsField: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                    },
                    {
                        data: 'code_material',
                        name: 'code_material',
                    },
                    {
                        data: 'part_description',
                        name: 'part_description',
                    },
                    {
                        data: 'quantity',
                        name: 'quantity',
                    },
                    {
                        name: 'hpp_average',
                        data: 'hpp_average',
                        render: function(data, type, row) {
                            return 'Rp. '+Helper.toCurrency(data);
                        }
                    },
                    {
                        name: 'selling_price',
                        data: 'selling_price',
                        render: function(data, type, row) {
                            return 'Rp. '+Helper.toCurrency(data);
                        }
                    },
                ],
                actionLink: ""
            });
            tbl.table.reloadTable(Helper.apiUrl(url));
        }

    };
</script>
