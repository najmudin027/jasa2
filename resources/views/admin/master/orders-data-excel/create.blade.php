@extends('admin.home')
@section('content')
<style>
     .big-checkbox {width: 15px; height: 15px; vertical-align:bottom;}
</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/orders-data-excel/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off">
    <div class="col-md-6" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                #1 Services
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Service Order No</label>
                    <div class="col-sm-12">
                        <input name="service_order_no" placeholder="Service Order No" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">ASC Name</label>
                    <div class="col-sm-12">
                        <select name="asc_id" id="asc_id" class="form-control" required style="width:100%">
                        </select>
                        <input name="asc_name" id="asc_name" type="hidden" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Customer Name</label>
                    <div class="col-sm-12">
                        <input name="customer_name" placeholder="Customer Name" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Type Job</label>
                    <div class="col-sm-12">
                        <select name="type_job" id="type_job" class="form-control" required style="width:100%">
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Defect Type Description</label>
                    <div class="col-sm-12">
                        <textarea name="defect_type" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Code</label>
                    <div class="col-sm-12">
                        <select name="engineer_code" id="engineer_code" class="form-control" required style="width:100%">
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Name</label>
                    <div class="col-sm-12">
                        <input name="engineer_name" id="engineer_name" placeholder="Engineer Name" type="text" class="form-control" readonly>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Assist Engineer Name</label>
                    <div class="col-sm-12">
                        <input name="assist_engineer_name" placeholder="Assist Engineer Name" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Merk Brand</label>
                    <div class="col-sm-12">
                        <input name="merk_brand" placeholder="Merk Brand" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Model Product</label>
                    <div class="col-sm-12">
                        <input name="model_product" placeholder="Model Product" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Status</label>
                    <div class="col-sm-12">
                        <select name="status" id="status" class="form-control" required style="width:100%">
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Reason</label>
                    <div class="col-sm-12">
                        <input name="reason" placeholder="Reason" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Remark Reason</label>
                    <div class="col-sm-12">
                        <input name="remark_reason" placeholder="Remark Reason" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Pending Aging Days</label>
                    <div class="col-sm-12">
                        <input name="pending_aging_days" placeholder="Pending Aging Days" type="text" class="form-control" readonly>
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                #4 Completed Service Time
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Repair Completed Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="repair_completed_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Repair Completed Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="repair_completed_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Closed Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="closed_date" id="closed_date" type="text" class="form-control"><br>
                    </div>
                    <div class="col-sm-12 input-group">
                        <small class="form-text text-muted warning_closed_date">Select Request Date First!</small>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Closed Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="closed_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Rating</label>
                    <div class="col-sm-12">
                        <input name="rating" placeholder="Rating" type="text" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                #2 Address
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Street</label>
                    <div class="col-sm-12">
                        <textarea name="street" placeholder="Street" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">City</label>
                    <div class="col-sm-12">
                        <input name="city" placeholder="City" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Mobile)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_mobile" placeholder="Phone No (Mobile)" type="text" class="form-control number_only">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Home)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_home" placeholder="Phone No (Home)" type="text" class="form-control number_only">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Office)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_office" placeholder="Phone No (Office)" type="text" class="form-control number_only">
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                3# Services Time
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Month</label>
                    <div class="col-sm-12">
                        <select class="form-control js-example-basic-single" name="month" id="month">
                        </select>
                        <small class="form-text text-muted">
                            Tahun <?php echo date("Y") ?>
                        </small>
                    </div>

                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Request Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="date" id="request_date" type="text" class="form-control date_format" required>
                    </div>

                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Request Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="request_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Picked Order Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="engineer_picked_order_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Picked Order Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="engineer_picked_order_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Admin Assigned to Engineer Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="admin_assigned_to_engineer_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Admin Assignned to Engineer Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="admin_assigned_to_engineer_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Assigned Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="engineer_assigned_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Assigned Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="engineer_assigned_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Tech 1st Appointment Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="tech_1st_appointment_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Tech 1st Appointment Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="tech_1st_appointment_time" type="text" class="form-control time">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">1st Visit Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="1st_visit_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">1st Visit Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="1st_visit_time" type="text" class="form-control time">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Parts Data Bundle (OPTIONAL)
            </div>
            <div class="card-body">
                <div class="dynamic_bundle">
                    <div class="position-relative row form-group">
                        <div class="col-sm-7">
                            Bundle Name
                            <small class="form-text text-danger notice_asc_name">select asc name fisrt !</small>
                        </div>
                        <div class="col-sm-3">
                            Main Price
                        </div>
                        <div class="col-sm-1">
                            View Bundle Details
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" class="btn btn-success btn_add_bundle"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="position-relative row form-group" id="bundle_row_xxx">
                        <div class="col-sm-7">
                            <select name="part_data_stock_bundle_id[]" data-uniq="xxx" class="form-control part_data_stock_bundle_id_input-xxx" style="width:100%">
                            </select>
                            <input type="hidden" name="bundle_name[]" data-uniq="xxx" class="form-control bundle_name_input-xxx"/>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="main_price[]" data-uniq="xxx" class="form-control main_price_input-xxx" readonly/>
                        </div>
                        <div class="col-sm-1 text-center">
                            <button type="button" data-uniq="xxx" class="btn btn-success bundle_details_data bundle_details_data-xxx" data-part_data_stock_bundle_id="" data-bundle_name=""><i class="fa fa-bars"></i></button>
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" data-uniq="xxx" data-part_data_stock_bundle_id="" class="btn-transition btn btn-danger btn_remove_bundle remove_bundle_xxx"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Parts Data
            </div>
            <div class="card-body">
                <div class="dynamic_part">
                    <div class="position-relative row form-group">
                        <div class="col-sm-2">
                            Code Material
                            <small class="form-text text-danger notice_asc_name">select asc name fisrt !</small>
                        </div>
                        <div class="col-sm-3">
                            Part Description
                        </div>
                        <div class="col-sm-2">
                            Quantity
                        </div>
                        <div class="col-sm-2">
                            Stock Available
                        </div>
                        <div class="col-sm-2 ">
                            Order By Customer (Qty x Price)
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" class="btn btn-success btn_add_my_part"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="position-relative row form-group" id="my_part_row_xxx">
                        <div class="col-sm-2">
                            <select name="part_data_stock_inventories_id[]" data-uniq="xxx" class="form-control part_data_stock_inventories_id_input-xxx" style="width:100%">
                            </select>
                            <input type="hidden" name="code_material[]" data-uniq="xxx" class="code_material_input-xxx form-control"/>

                        </div>
                        <div class="col-sm-3">
                            <input type="text" placeholder="Part Description" name="part_description[]" data-uniq="xxx" class="form-control part_description_input-xxx" readonly/>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" placeholder="Quantity" name="quantity[]" data-uniq="xxx" class="form-control number_only quantity_input-xxx" required/>
                        </div>
                        <div class="col-sm-2 text-center">
                            <input type="text" name="stock_part[]" data-uniq="xxx" class="stock_part_input-xxx form-control" readonly/>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-check">

                                <input name="is_order_by_customer_check[]" class="form-check-input big-checkbox is_order_by_customer_check-xxx" type="checkbox" data-uniq="xxx" >
                                <input type="hidden" name="is_order_by_customer[]" data-uniq="xxx" class="is_order_by_customer-xxx">
                                <label class="form-check-label" for="flexCheckDefault">
                                    <input type="text" name="selling_price[]" data-part_data_stock_inventories_id="" data-uniq="xxx" class="selling_price-xxx form-control" readonly/>
                                    <input type="hidden" name="price_hpp_average[]" data-part_data_stock_inventories_id="" data-uniq="xxx" class="price_hpp_average-xxx form-control" readonly/>
                                    <input type="hidden" name="selling_price_pcs[]" data-part_data_stock_inventories_id="" data-uniq="xxx" class="selling_price_pcs-xxx"/>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" data-uniq="xxx" data-part_data_stock_inventories_id="" class="btn-transition btn btn-danger btn_remove_my_part remove_my_part_xxx"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Jasa (Labor)
            </div>
            <div class="card-body">
                <div class="dynamic_jasa">
                    <div class="position-relative row form-group">
                        <div class="col-sm-6">
                            Nama Jasa
                        </div>
                        <div class="col-sm-5">
                            Price
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" class="btn btn-success btn_add_my_jasa"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="position-relative row form-group" id="my_jasa_row_xxx">
                        <div class="col-sm-6">
                            <select name="jasa_id[]" data-uniq="xxx" class="form-control jasa_id_input-xxx" style="width:100%">
                            </select>
                            <input type="hidden" name="jasa_name[]" data-uniq="xxx" class="jasa_name_input-xxx form-control"/>
                        </div>
                        <div class="col-sm-5">
                            <input type="text" placeholder="Price" name="price_j[]" data-uniq="xxx" class="form-control number_only price_j_input-xxx" readonly/>
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" data-uniq="xxx" data-jasa_id="" class="btn-transition btn btn-danger btn_remove_my_jasa remove_my_jasa_xxx"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> CREATE </button>
        </div>
    </div>
</form>
<!-- Modal Bundle Details -->
<div class="modal fade" id="bundle_details_modal" data-backdrop="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Bundle Details for <span id="bundle_name"></span></h5>
            </div>
            <div class="modal-body">
                <table id="table-bundle-details" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <tr>
                                <th>No.</th>
                                <th>Code Material</th>
                                <th>Part Description</th>
                                <th>Quantity</th>
                                <th>Hpp Average</th>
                                <th>Selling Price</th>
                            </tr>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="close_bundle_details_modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
@include('admin.master.orders-data-excel._js')
<script>

    // var option_remove = [];
    var data = [
        {id: 'Januari', text: 'Januari', title: 'Januari'},
        {id: 'Februari', text: 'Februari', title: 'Februari'},
        {id: 'Maret', text: 'Maret', title: 'Maret'},
        {id: 'April', text: 'April', title: 'April'},
        {id: 'Mei', text: 'Mei', title: 'Mei'},
        {id: 'Juni', text: 'Juni', title: 'Juni'},
        {id: 'Juli', text: 'Juli', title: 'Juli'},
        {id: 'Agustus', text: 'Agustus', title: 'Agustus'},
        {id: 'September', text: 'September', title: 'September'},
        {id: 'Oktober', text: 'Oktober', title: 'Oktober'},
        {id: 'November', text: 'November', title: 'November'},
        {id: 'Desember', text: 'Desember', title: 'Desember'},
    ];

    $("#month").select2({
        data: data,
        escapeMarkup: function(markup) {
            return markup;
        }
    })


    $(function() {
        $('.btn_add_my_part').prop("disabled", true);
        $('select[name="part_data_stock_inventories_id[]"]').prop("disabled", true);
        $('.btn_add_bundle').prop("disabled", true);
        $('select[name="part_data_stock_bundle_id[]"]').prop("disabled", true);

        $('.bundle_details_data').prop("disabled", true);

        ClassApp.checkStock('xxx');
        $('.time').inputmask("hh:mm:ss", {
            placeholder: "hh:mm:ss",
            insertMode: false,
            showMaskOnHover: false,
            hourFormat: "24",
            clearIncomplete: true
        });
        // $('.date').inputmask("dd-mm-yyyy", {
        //     placeholder: "dd-mm-yyyy",
        //     clearIncomplete: true
        // });
        Helper.dateFormat('.date_format');
        //order by customer hiden -> selling_price
        $('input[name="is_order_by_customer_check[]"]').prop("disabled", true);
        $('input[name="selling_price[]"]').hide();
        $(document).on('click', 'input[name="is_order_by_customer_check[]"]', function() {
            var xx = $(this).attr('data-uniq');

            if($('.quantity_input-'+xx+'').val() != "") {
                var checked = 0;
                if($('.is_order_by_customer_check-'+xx+'').is(':checked')) {
                    checked = 1;
                    console.log(checked);
                }
                $('.is_order_by_customer-'+xx+'').val(checked);
                ClassApp.checkIsOrderByCustomer(xx);
                if($('.selling_price-'+xx+'').val() == 0) {
                    Helper.errorNotif('Unable to select, price is Rp. 0')
                    $('.is_order_by_customer_check-'+xx+'').prop('checked',false).trigger("change");
                    $('.selling_price-'+xx+'').hide();
                }
                ClassApp.hitungSellingPriceTotalOrderByCustomer(xx);
            } else {
                $('.is_order_by_customer_check-'+xx+'').prop('checked',false).trigger("change");
                $('.quantity_input-'+xx+'').focus();
                Helper.warningNotif('Isi terlebih Dahulu Quantity !');
            }

        })
        //QUantity * Price hpp
        $(document).on('keyup', 'input[name="quantity[]"]', function() {
            var xx = $(this).attr('data-uniq');
            ClassApp.hitungSellingPriceTotalOrderByCustomer(xx);
        });

    });

    Helper.onlyNumberInput('.number_only');

    $(function() {
        var xx = 1;

        // add row part data
        $(document)
            .on('focus', 'select[name="part_data_stock_inventories_id[]"]', 'input[name="quantity[]"]', function(e) {
                if ($(this).is('input[name="quantity[]"]:last')) {
                    templateNewInputPartData(xx)
                    xx++;
                }
            })

        ClassApp.preventDuplicatePartSelect();
        $(document)
            .on('click', '.btn_add_my_part', function() {
                templateNewInputPartData(xx)
                xx++;
                ClassApp.preventDuplicatePartSelect();
            })
        // delete part my inv
        $(document)
            .on('click', '.btn_remove_my_part', function() {
                $('.btn_add_my_part').prop("disabled", false);
                var button_id = $(this).attr("data-uniq");
                // Helper.removeArrayValue(option_remove, parseInt($(this).attr("data-part_data_stock_inventories_id")));
                $('#my_part_row_' + button_id + '').remove();
                // console.log(option_remove);
            });
        ClassApp.select2Jasa('xxx');
        ClassApp.preventDuplicateJasaSelect();
        $(document)
            .on('click', '.btn_add_my_jasa', function() {
                templateNewInputJasaData(xx)
                xx++;
                ClassApp.preventDuplicateJasaSelect();
            })
        // delete Jasa/Labor
        $(document)
            .on('click', '.btn_remove_my_jasa', function() {
                $('.btn_add_my_jasa').prop("disabled", false);
                var button_id = $(this).attr("data-uniq");
                $('#my_jasa_row_' + button_id + '').remove();
            });

        ClassApp.preventDuplicateBundleSelect();
        $(document)
            .on('click', '.btn_add_bundle', function() {
                templateNewInputBundleData(xx)
                xx++;
                ClassApp.preventDuplicateBundleSelect();
            })
        // delete Bundle
        $(document)
            .on('click', '.btn_remove_bundle', function() {
                $('.btn_add_bundle').prop("disabled", false);
                var button_id = $(this).attr("data-uniq");
                $('#bundle_row_' + button_id + '').remove();
            });

        // Table Bundle Details
        $(document)
            .on('click', '.bundle_details_data', function() {
                var id = $(this).attr("data-part_data_stock_bundle_id");
                var bundle_name = $(this).attr("data-bundle_name");
                $('#bundle_name').html(bundle_name);
                var url = '/part-data-excel-stock-bundle/details-data/'+id+'';
                ClassApp.initTablesBundleDetails(url);
                $('#bundle_details_modal').modal('show');
            });

    });


    function templateNewInputBundleData(xx) {
        $('.btn_add_bundle').prop("disabled", true);
        $('.dynamic_bundle').append((`
            <div class="position-relative row form-group" id="bundle_row_${xx}">
                <div class="col-sm-7">
                    <select name="part_data_stock_bundle_id[]" data-uniq="${xx}" class="form-control part_data_stock_bundle_id_input-${xx}" required style="width:100%">
                    </select>
                    <input type="hidden" name="bundle_name[]" data-uniq="${xx}" class="form-control bundle_name_input-${xx}"/>
                </div>
                <div class="col-sm-3">
                    <input type="text" name="main_price[]" data-uniq="${xx}" class="main_price_input-${xx} form-control" readonly/>
                </div>
                <div class="col-sm-1  text-center">
                    <button type="button" data-uniq="${xx}" class="btn btn-success bundle_details_data bundle_details_data-${xx}" data-part_data_stock_bundle_id="" data-bundle_name=""><i class="fa fa-bars"></i></button>
                </div>
                <div class="col-sm-1" style="padding-left:0px;">
                    <button type="button" data-uniq="${xx}" data-part_data_stock_bundle_id="" class="btn-transition btn btn-danger btn_remove_bundle remove_bundle_${xx}"><i class="fa fa-close"></i></button>
                </div>
            </div>
        `));
        if($('#asc_name').val() != "") {
            $('select[name="part_data_stock_bundle_id[]"]').prop("disabled", false);
        } else {
            $('select[name="part_data_stock_bundle_id[]"]').prop("disabled", true);
        }
        Helper.onlyNumberInput('.number_only');
        ClassApp.select2Bundle(xx,$('#asc_id').val());
        $('.bundle_details_data-'+xx+'').prop("disabled", true);
    }

    function templateNewInputPartData(xx) {
        $('.btn_add_my_part').prop("disabled", true);
        $('.dynamic_part').append((`
            <div class="position-relative row form-group" id="my_part_row_${xx}">
                <div class="col-sm-2">
                    <select name="part_data_stock_inventories_id[]" data-uniq="${xx}" class="form-control part_data_stock_inventories_id_input-${xx}" required style="width:100%">
                    </select>
                    <input type="hidden" name="code_material[]" data-uniq="${xx}" class="code_material_input-${xx} form-control"/>

                </div>
                <div class="col-sm-3">
                    <input type="text" placeholder="Part Description" name="part_description[]" data-uniq="${xx}" class="part_description_input-${xx} form-control" readonly/>
                </div>
                <div class="col-sm-2">
                    <input type="text" placeholder="Quantity" name="quantity[]" data-uniq="${xx}" class=" quantity_input-${xx} number_only form-control" required/>
                </div>
                <div class="col-sm-2">
                    <input type="text" name="stock_part[]" data-uniq="${xx}" class="stock_part_input-${xx} form-control" readonly/>
                </div>
                <div class="col-sm-2">
                    <div class="form-check">

                        <input name="is_order_by_customer_check[]" data-uniq="${xx}" class="form-check-input big-checkbox is_order_by_customer_check-${xx}" type="checkbox" value=0>
                        <input type="hidden" name="is_order_by_customer[]" class="is_order_by_customer-${xx}" data-uniq="${xx}" value=0>
                        <label class="form-check-label" for="flexCheckDefault">
                            <input type="text" name="selling_price[]" data-uniq="${xx}" data-part_data_stock_inventories_id="" class="selling_price-${xx} form-control" readonly/>
                            <input type="hidden" name="price_hpp_average[]" data-uniq="${xx}" data-part_data_stock_inventories_id="" class="price_hpp_average-${xx} form-control" readonly/>
                            <input type="hidden" name="selling_price_pcs[]" data-part_data_stock_inventories_id="" data-uniq="${xx}" class="selling_price_pcs-${xx}"/>
                        </label>
                    </div>
                </div>
                <div class="col-sm-1" style="padding-left:0px;">
                    <button type="button" data-uniq="${xx}" data-part_data_stock_inventories_id="" class="btn-transition btn btn-danger btn_remove_my_part remove_my_part_${xx}"><i class="fa fa-close"></i></button>
                </div>
            </div>
        `));
        if($('#asc_name').val() != "") {
            $('select[name="part_data_stock_inventories_id[]"]').prop("disabled", false);
        } else {
            $('select[name="part_data_stock_inventories_id[]"]').prop("disabled", true);
        }

        Helper.onlyNumberInput('.number_only');
        ClassApp.select2CodeMaterial(xx,$('#asc_id').val());
        ClassApp.checkStock(xx);
        $('.is_order_by_customer_check-'+xx+'').prop("disabled", true);
        $('.selling_price-'+xx+'').hide();
    }
    function templateNewInputJasaData(xx) {
        $('.btn_add_my_jasa').prop("disabled", true);
        $('.dynamic_jasa').append((`
            <div class="position-relative row form-group" id="my_jasa_row_${xx}">
                <div class="col-sm-6">
                    <select name="jasa_id[]" data-uniq="${xx}" class="form-control jasa_id_input-${xx}" required style="width:100%">
                    </select>
                    <input type="hidden" name="jasa_name[]" data-uniq="${xx}" class="jasa_name_input-${xx} form-control"/>
                </div>
                <div class="col-sm-5">
                    <input type="text" placeholder="Price" name="price_j[]" data-uniq="${xx}" class=" price_j_input-${xx} number_only form-control" readonly/>
                </div>

                <div class="col-sm-1" style="padding-left:0px;">
                    <button type="button" data-uniq="${xx}" data-jasa_id="" class="btn-transition btn btn-danger btn_remove_my_part remove_my_jasa_${xx}"><i class="fa fa-close"></i></button>
                </div>
            </div>
        `));

        Helper.onlyNumberInput('.number_only');
        ClassApp.select2Jasa(xx);
    }

    $("#form-data").submit(function(e) {
        var data = Helper.serializeForm($(this));
        Axios.post('/orders-data-excel', data)
            .then(function(response) {
                event.preventDefault();
                //do something
                $(this).prop('disabled', true);
                Helper.successNotif(response.data.msg);
                // location.reload();
                window.location.href = Helper.redirectUrl('/admin/orders-data-excel/show');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
        e.preventDefault();

    });



</script>


@endsection
