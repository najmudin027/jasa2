<form id="form-engineer-code-excel">
    <div class="modal fade" id="modal-engineer-code-excel" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">ADD ENGINEER CODE EXCEL STOCK</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Engineer Code</label>
                    <div class="form-group">
                        <input name="engineer_code" placeholder="Engineer Code" id="engineer_code" type="text" class="form-control">
                    </div>
                    <label>Engineer Name</label>
                    <div class="form-group">
                        <input name="engineer_name" placeholder="Engineer Name" id="engineer_name" type="text" class="form-control">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm waves-effect btn-clus" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-sm btn-oke waves-effect"><i class="fa fa-plus"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
