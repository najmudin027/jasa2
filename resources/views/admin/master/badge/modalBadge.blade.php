
<form id="form-badge">
    <div class="modal fade" id="modal-badges" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Badge</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id" class="id">
                    <label class="label-control">Name Badge</label>
                    <div class="form-group">
                        <input name="name" placeholder="Name Badge" id="name" type="text" class="form-control name" required>
                    </div>
                    <span id="error-name" style="color:red"></span>
                    <label class="label-control">Color</label>
                    <div class="form-group">
                        <select name="color" id="color" class="form-control color">
                        </select>
                    </div>
                    <label class="label-control">Users</label>
                    <div class="form-group">
                        <select name="user_emails[]" id="user_emails" class="form-control user_emails" multiple="multiple">

                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
                    <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
</form>

