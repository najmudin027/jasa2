@extends('admin.home')
@section('content')
<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }


</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="col-md-6">
                <h3 class="card-title">
                    {{ $title }}
                </h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ url('admin/part-data-excel-stock-inventory/create') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add </a>
            </div>
        </div>

        <div class="card-body">
        <table style="width: 100%;" id="table-pdesi" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Code Material</th>
                        <th>ASC Name</th>
                        <th>Part Description</th>
                        <th>Stock</th>
                        <th>History</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>

    Helper.selectMonth();
    $(function() {
        $('#code_material').prop('disabled', true);

        $('.date_format').datetimepicker({
            timepicker:false,
            format:'d-m-Y',
        });

    });


    var tableOrder = globalCRUD.datatables({
        url: '/part-data-excel-stock-inventory/datatables',
        selector: '#table-pdesi',
        columnsField: [
            'DT_RowIndex',
            {
                data: "part_data_stock.code_material",
                name: "part_data_stock.code_material",
            },
            {
                data: "part_data_stock.asc.name",
                name: "asc_id",
            },
            {
                data: "part_data_stock.part_description",
                name: "part_description",
            },
            'stock',
            {
                data: "id",
                name: "id",
                searchable: false,
                orderable: false,
                render: function(data, type, row){
                    if (row.quantity == 0) {
                        return '-';
                    } else {
                        return "<a href='/admin/part-data-excel-stock-inventory/history-in-out/"+ row.id + "' class='btn btn-primary btn-sm' title='History'><i class='fa fa-history'></i></a>";
                    }

                }
            },
        ],
        actionLink: {
            store: function() {
                return "/admin/part-data-excel-stock-inventory";
            },
            update: function(row) {
                return "/admin/part-data-excel-stock-inventory/edit/" + row.id;
            },
            delete: function(row) {
                return "/part-data-excel-stock-inventory/" + row.id;
            },
        }
    })


    </script>


@endsection
