<form id="form-asc-excel">
    <div class="modal fade" id="modal-asc-excel" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">ADD ASC EXCEL</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Name</label>
                    <div class="form-group">
                        <input name="name" placeholder="Name" id="name" type="text" class="form-control">
                    </div>
                    <label>Address</label>
                    <div class="form-group">
                        <textarea row="4" name="address" placeholder="Address" id="address" class="form-control"></textarea>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm waves-effect btn-clus" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-sm btn-oke waves-effect"><i class="fa fa-plus"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
