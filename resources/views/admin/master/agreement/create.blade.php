@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
            </div>
        </div>
        <div class="card-body">
            <form id="agreement-save">
                @foreach ($termCondition as $term)
                    <div class="form-group">
                        @if ( $term->name == 'agreement_title')
                            <label for="exampleFormControlSelect2">{{ $term->desc }}</label>
                            @if ($term)
                                <input type="text" name="value[]" class="form-control" value="{{ $term->value }}">
                                <input type="hidden" value="{{ $term->id }}" name="id[]">
                            @else
                                <input type="hidden" value="agreement_title" name="name[]">
                                <input type="text" name="value[]" class="form-control" value="" placeholder="Title Term Condition">
                            @endif
                           
                        @else
                            <label for="exampleFormControlSelect2">{{ $term->desc }}</label>
                            @if ($term)
                                <input type="hidden" value="{{ $term->id }}" name="id[]">    
                                <textarea class="form-control" name="value[]" id="contents" maxlength="300" placeholder="Start Typin..." >{{ $term->value }}</textarea>
                            @else
                                <input type="hidden" value="aggrement_title" name="name[]">
                                <textarea class="form-control" name="value[]" id="contents" maxlength="300" placeholder="Start Typin..." ></textarea>
                            @endif
                        @endif
                        
                    </div>
                @endforeach
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg" id="save_agreement">Save Requirements</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    Helper.wysiwygEditor('#contents');
    $('#agreement-save').submit(function(e){
        globalCRUD.handleSubmit($(this))
            .storeTo('/general-setting/aggrement/save')
            .redirectTo(function(resp){
                return '/admin/agreement/create';
            })
        e.preventDefault();
    });




    // $(document).on('click', '#save_agreement', function(e) {
    //     // Helper.confirm(function(){
    //     var content = $('#contents').val();
    //     alert(content)
    //     Helper.loadingStart();
    //     // alert($("#konten").serialize());
    //     $.ajax({
    //         url:Helper.apiUrl('/general-setting/aggrement/save'),
    //         type: 'post',
    //         data : {
    //             desc : $("#konten").serialize()
    //         },

    //         success: function(res) {
    //             // Helper.successNotif('Success');
    //             // Helper.redirectTo('/admin/agreement/create');
    //         },
    //         error: function(xhr, status, error) {
    //             if(xhr.status == 422){
    //                 error = xhr.responseJSON.data;
    //                 _.each(error, function(pesan, field){
    //                     $('#error-'+ field).text(pesan[0])
    //                 })
    //             }
    //             Helper.loadingStop();
    //         }
    //     })
    //     // })
    //     e.preventDefault()
    // })
</script>
@endsection
