@extends('admin.home')
@section('content')

<form id="form-social-media-save" style="display: contents;">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header header-bg">
                {{ $title }}
            </div>
            <div class="card-body">
                @foreach ($settings as $setting)
                <input type="hidden" name="id[]" value="{{ $setting->id }}">
                <label class="label-control">{{ $setting->desc }}</label>
                <div class="form-group">
                    <select name="value[]" class="form-control" required>
                        <option value="1" {{ $setting->value == 1 ? 'selected' : '' }} >Show Login Button</option>
                        <option value="0" {{ $setting->value == 0 ? 'selected' : '' }}>Hide Login Button</option>
                    </select>
                </div>
                @endforeach
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary" id="save_commission">Submit</button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    $('#form-social-media-save').submit(function(e) {
        data = Helper.serializeForm($(this));
        
        Helper.loadingStart();
        // post data
        Axios.post(Helper.apiUrl('/general-setting/social-login-setting-save'), data)
            .then(function(response) {
                Helper.successNotif('Success');
                location.reload();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
@endsection