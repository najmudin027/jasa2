@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
            </div>
        </div>
        <div class="card-body">
            <form id="form">
                <div class="form-group">
                    <label for="exampleFormControlSelect2">{{ $data->desc }}</label>
                    <input type="text" id="value" name="value" id_setting="{{ $data->id }}" class="form-control" value="{{ $data->value }}">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg" id="save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('#form').submit(function(e){
        var id = $('#value').attr('id_setting');
        console.log(id);
        globalCRUD.handleSubmit($(this))
            .updateTo('/general-setting/'+id+'')
            .redirectTo(function(resp){
                Helper.successNotif('Success, Google Maps Key updated!');
                return '/admin/home';
            })
        e.preventDefault();
    });
</script>
@endsection
