@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add-general-setting">ADD NEW</button>
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="general-setting" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
            <button class="btn btn-primary btn-sm add-general-setting">ADD NEW</button>
        </div>
    </div>
</div>
@include('admin.master.general-setting._generalSettingModal')
@endsection

@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    globalCRUD.datatables({
        url: '/general-setting/datatables',
        selector: '#general-setting',
        columnsField: ['DT_RowIndex', 'name', 'value'],
        modalSelector: "#modal-general-setting",
        modalButtonSelector: ".add-general-setting",
        modalFormSelector: "#form-general-setting",
        actionLink: {
            store: function() {
                return "/general-setting";
            },
            update: function(row) {
                return "/general-setting/" + row.id;
            },
            delete: function(row) {
                return "/general-setting/" + row.id;
            },
        }
    })
</script>
@endsection
