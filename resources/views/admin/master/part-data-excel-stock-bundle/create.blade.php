@extends('admin.home')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header-tab card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    {{ $title }}
                </div>
                <div class="btn-actions-pane-right text-capitalize">
                    <a href="{{ url('admin/part-data-excel-stock-bundle/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
                </div>
            </div>
        </div>
    </div>
    <form id="form-data" style="display: contents;" autocomplete="off">
        <div class="col-md-12" style="margin-top: 10px;">
            <div class="card">
                <div class="card-header header-border">
                    Main Bundle
                </div>
                <div class="card-body">
                    <div class="position-relative row form-group">
                        <label for="exampleEmail" class="col-sm-12 col-form-label">Name</label>
                        <div class="col-sm-12">
                            <input name="name" placeholder="Name Bundle" type="text" class="form-control" required>
                        </div>
                    </div>
                    <div class="position-relative row form-group">
                        <label for="exampleEmail" class="col-sm-12 col-form-label">Astech Service Center</label>
                        <div class="col-sm-12">
                            <select name="asc_id" id="asc_id" class="form-control" required style="width:100%">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 10px;">
            <div class="card">
                <div class="card-header header-border">
                    Details Bundle
                </div>
                <div class="card-body">
                    <div class="btn-actions-pane-left text-right">
                        <button type="button" class="btn btn-success btn_add_details"><i class="fa fa-plus"></i></button>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Code Material</th>
                                <th>Part Description</th>
                                <th>Qty</th>
                                <th>Stock Available</th>
                                <th>Hpp Avg Price</th>
                                <th>Selling Price</th>
                                <th>Subtotal</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody class="dynamic_details">
                            <tr>
                                <input type="hidden" value=0 id="count_details_data" />
                                <td width="20%">
                                    <select name="part_data_stock_inventories_id[]" data-uniq="xxx" class="form-control part_data_stock_inventories_id-xxx" style="width:100%">
                                    </select>
                                </td>
                                <td width="30%">
                                    <input name="part_description[]" type="text"
                                        placeholder="Description Sparepart"
                                        class="form-control part_description-xxx" data-uniq="xxx"
                                        style="width:100%" readonly>
                                </td>
                                <td>
                                    <input name="qty[]" type="text" placeholder="Quantity"
                                        class="form-control qty-xxx number_only" data-uniq="xxx" required>
                                </td>
                                <td>
                                    <input name="stock_available[]" type="text" placeholder=""
                                        class="form-control stock_available-xxx number_only" data-uniq="xxx" required readonly>
                                </td>
                                <td>
                                    <input name="hpp_average[]" type="text" placeholder="Hpp Price"
                                        class="form-control hpp_average-xxx number_only" data-uniq="xxx" readonly>
                                </td>
                                <td>
                                    <input name="selling_price[]" type="text" placeholder="Selling Price"
                                        class="form-control selling_price-xxx number_only" data-uniq="xxx" readonly required>
                                </td>
                                <td>
                                    <input name="sub_total_details[]" type="text"
                                        class="form-control sub_total_details-xxx currency" data-uniq="xxx"
                                        readonly>
                                </td>
                                <td>
                                </td>
                            </tr>
                        <tfoot>
                            <tr>
                                <td colspan="1"></td>
                                <td><b>TOTAL Details</b></td>
                                <td><input name="total_qty_details" type="text" class="form-control"
                                        id="total_qty_details" readonly></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tfoot>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12" style="margin-top: 10px;">
            <div class="card">
                <div class="card-header header-border">
                    Price Bundle
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-7">

                        </div>
                        <div class="col-lg-5">
                            <div class="text-right form-group row justify-content-end">
                                <label class="col-sm-3 control-label">Main Price Suggestion:</label>
                                <div class="col-sm-9">
                                    <input type="text" id="total_details" name="total_details"
                                        class="form-control text-right currency" readonly>
                                </div>
                            </div>
                            <div class="text-right form-group row justify-content-end">
                                <label class="col-sm-3 control-label">Main price:</label>
                                <div class="col-sm-9">
                                    <input type="text" id="main_price" name="main_price"
                                        class="form-control text-right number_only">
                                </div>
                            </div>
                            <div class="text-right form-group row justify-content-end">
                                <label class="col-sm-3 control-label">Labor:</label>
                                <div class="col-sm-9">
                                    <input type="text" id="labor_price" name="labor_price"
                                        class="form-control text-right" readonly>
                                </div>
                            </div>
                            <div class="text-right form-group row justify-content-end" style="margin-top: 50px;">
                                <button type="submit" class="btn btn-primary mr-2" id="save_data"><i
                                    class="fa fa-save"></i> Save Data</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('script')
    <style>
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {

            -webkit-appearance: none;
            margin: 0;
        }

        input[type=number] {
            -moz-appearance: textfield;
            /* Firefox */
        }

    </style>

    @include('admin.master.part-data-excel-stock-bundle._main_js')
    @include('admin.master.part-data-excel-stock-bundle._details_js')
    <script>
        $(function() {
            Helper.dateFormat('.date_format');
            Helper.currency('.currency');
            ClassApp.hitungTotal();
            $('.btn_add_details').prop("disabled", true);
            $('select[name="part_data_stock_inventories_id[]"]').prop("disabled", true);
        });
        globalCRUD.select2('#asc_id', '/asc-excel/select2');
        $("#form-data").submit(function(e) {
            ClassApp.saveData($(this), e);
        });

        $('#asc_id').on('select2:select', function (e) {
            asc_id = $(this).val();
            ClassApp.select2CodeMaterial('xxx',asc_id);
            $('.btn_add_details').prop("disabled", false);
            $('select[name="part_data_stock_inventories_id[]"]').prop("disabled", false);
            $('select[name="part_data_stock_inventories_id[]"]').each(function() {
                var uniq = $(this).attr('data-uniq');
                ClassApp.resetSelect2CodeMaterial(uniq);
                ClassApp.select2CodeMaterial(uniq,asc_id);
            });
        });
    </script>
@endsection
