<script>

Helper.onlyNumberInput('.number_only');





var ClassApp = {
    setDetailsQty: function (uniq) {
        return $('.qty-' + uniq).val();
    },
    setDetailsSellingPrice: function (uniq) {
        return $('.selling_price-' + uniq).val();
    },
    checkStock: function(xx) {
        $('.qty-' + xx + '').keyup(function(e) {
            var max = parseInt($('.stock_available-' + xx + '').val());
            if (parseInt($(this).val()) > max) {
                e.preventDefault();
                $(this).val(max);
            } else {
                $(this).val();
            }
        });
    },

    hitungTotal: function() {
        Helper.unMask('input[name="sub_total_details[]"]');
        Helper.unMask('input[name="total_details[]"]');
        // Helper.unMask('input[name="sub_total_service[]"]');
        // Helper.unMask('input[name="total_service[]"]');

        var total_details_qty = 0;
        var sub_total_details = 0;
        var total_details = 0;
        var total = 0;
        var grand_total = 0;

        $('input[name="qty[]"]').each(function() {
            var uniq = $(this).attr('data-uniq');
            var qty = $('.qty-' + uniq).val() == '' ? 0 : parseInt($('.qty-' + uniq).val());
            var selling_price = $('.selling_price-' + uniq).val() == '' ? 0 : parseInt($('.selling_price-' + uniq).val());
            var sub_total_details = qty * selling_price;

            total_details_qty += qty;
            $('.sub_total_details-' + uniq).val(Helper.toCurrency(sub_total_details));
            total_details += sub_total_details;
            $('#total_qty_details').val(total_details_qty);
            $('#total_details').val(Helper.toCurrency(total_details));
        });
        var labor_price = parseInt($('#main_price').val()) - total_details;
        $('#labor_price').val(Helper.toCurrency(ClassApp.preventNan(labor_price)));
    },

    templateNewInputDetails: function(xx) {
        $('.dynamic_details').append((`
            <tr id="details_row-${xx}">
                <input type="hidden" value="0" name="bundle_details_id[]">
                <td>
                    <select name="part_data_stock_inventories_id[]" data-uniq="${xx}" class="form-control part_data_stock_inventories_id-${xx}" style="width:100%">
                    </select>
                </td>
                <td width="30%">
                    <input name="part_description[]" type="text"
                        placeholder="Description Sparepart"
                        class="form-control part_description-${xx}" data-uniq="${xx}"
                        style="width:100%" readonly>
                </td>
                <td>
                    <input name="qty[]" type="text" placeholder="Quantity"
                        class="form-control qty-${xx} number_only" data-uniq="${xx}" required>
                </td>
                <td>
                    <input name="stock_available[]" type="text" placeholder=""
                        class="form-control stock_available-${xx} number_only" data-uniq="${xx}" required readonly>
                </td>
                <td>
                    <input name="hpp_average[]" type="text" placeholder="Hpp Price"
                        class="form-control hpp_average-${xx} number_only" data-uniq="${xx}" readonly>
                </td>
                <td>
                    <input name="selling_price[]" type="text" placeholder="Selling Price"
                        class="form-control selling_price-${xx} number_only" data-uniq="${xx}" required readonly>
                </td>
                <td>
                    <input name="sub_total_details[]" type="text"
                        class="form-control sub_total_details-${xx} number_only currency" data-uniq="${xx}"
                        readonly>
                </td>
                <td>
                    <div role="group" class="mb-2 btn-group-sm btn-group">
                        <button type="button" bundle_details_data_id="0" data-uniq="${xx}" class="btn-transition btn btn-sm btn-danger btn_remove_details" ><i class="fa fa-times"></i></button>
                    </div>
                </td>
            </tr>
        `));
    },

    deleteDetails: function($el) {
        var uniq = $el.attr("data-uniq");
        var details_id = parseInt($el.attr("bundle_details_data_id"));

        if (details_id != 0) {
            Helper.confirm(function() {
                Axios.delete('/part-data-excel-stock-bundle/destroy_details_data/' + details_id)
                    .then(function(response) {
                        Helper.successNotif('Success Delete');
                        $('#details_row-' + uniq + '').remove();
                        location.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            })
            return false;
        } else {
            $('#details_row-' + uniq + '').remove();
        }
    },

    select2CodeMaterialLoad : function() {
        $('select[name="part_data_stock_inventories_id[]"]').each(function(){
            var uniq = $(this).attr('data-uniq');
            ClassApp.select2CodeMaterial(uniq,$('#asc_id').val())
            $(document).on('change', '#asc_id', function() {
                if($(this).val() != "") {
                    ClassApp.select2CodeMaterial(uniq,$(this).val());
                }
            })
        });

    },

    select2CodeMaterial : function(xx, asc_id=null) {
        globalCRUD.select2('.part_data_stock_inventories_id-'+xx+'', '/part-data-excel-stock-inventory/select2/code_material/'+asc_id+'/filter_stock', function(item) {
            return {
                id: item.id,
                text:item.part_data_stock.code_material,
                part_description:item.part_data_stock.part_description,
                selling_price:item.selling_price,
                stock:item.stock,
                out_stock: item.out_stock,
                hpp_average: item.hpp_average
            }
        });

        $('.part_data_stock_inventories_id-'+xx+'').on('select2:select', function (e) {
            if(e.params.stock == 0) {
                Helper.warningNotif('Stock Available 0, Silahkan Buat Stock Inventory Terlebih dahulu! / Pilih Part Lain');
                $(this).val('').change();
            }
            $('.remove_details_'+xx+'').attr('data-part_data_stock_inventories_id',e.params.data.id);
            $('.part_description-'+xx+'').val(e.params.data.part_description);
            $('.selling_price-'+xx+'').val(e.params.data.selling_price);
            $('.stock_available-'+xx+'').val(e.params.data.stock);
            $('.hpp_average-'+xx+'').val(e.params.data.hpp_average);
            $('.btn_add_my_part').prop("disabled", false);
        });
    },

    resetSelect2CodeMaterial: function(xx) {
        $('.part_data_stock_inventories_id-'+xx+'').val('').trigger('change');
    },

    saveData: function(el, e, id) {
        Helper.unMask('.currency');
        var form = Helper.serializeForm(el);
        var fd = new FormData(el[0]);
        $.ajax({
            url:Helper.apiUrl('/part-data-excel-stock-bundle'),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                console.log(response);
                if (response != 0) {
                    Helper.successNotif(response.msg);
                    window.location.href = Helper.redirectUrl('/admin/part-data-excel-stock-bundle/show');
                }
            },
            error: function(xhr, status, error) {
                handleErrorResponse(error);
            },
        });

        e.preventDefault();
    },

    updateData: function(el, e, id) {
        Helper.unMask('.currency');
        var form = Helper.serializeForm(el);
        var fd = new FormData(el[0]);

        $.ajax({
            url:Helper.apiUrl('/part-data-excel-stock-bundle/'+id+''),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Part Data Excel Stock Bundle Has Been Saved',
                    });
                    window.location.href = Helper.redirectUrl('/admin/part-data-excel-stock-bundle/edit/'+id+'');
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });

                    })
                }

            },
        });
        e.preventDefault();
    },

    preventDuplicatePartSelect: function() {
        $('select[name="part_data_stock_inventories_id[]"]').change(function () {
            var xx = $(this).attr('data-uniq');
            if ($('select[name="part_data_stock_inventories_id[]"] option[value="' + $(this).val() + '"]:selected').length > 1) {
                $(this).val('').change();
                $('.part_description-'+xx+'').val('').change();
                $('.qty-'+xx+'').val('').change();
                Helper.warningNotif('You have already selected this Part previously, please choose another!');
            }
        });
    },
    preventNan: function(val) {
        if (isNaN(val)) {
            return 0;
        }
        return val;
    }
}

</script>
