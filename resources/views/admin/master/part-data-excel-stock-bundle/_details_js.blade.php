<script>
    // add details btn
    // var _uniq_counter_details = 1;

    var _uniq_counter_details = parseInt($('#count_details_data').val())+1;
    ClassApp.preventDuplicatePartSelect();
    $(document).on('click', ".btn_add_details", function(e) {
        uniq_details = _uniq_counter_details;
        ClassApp.templateNewInputDetails(uniq_details);
        if($('#asc_id').val() != null) {
            $('select[name="part_data_stock_inventories_id[]"]').prop("disabled", false);
        } else {
            $('select[name="part_data_stock_inventories_id[]"]').prop("disabled", true);
        }
        Helper.onlyNumberInput('.number_only');
        ClassApp.select2CodeMaterial(uniq_details,$('#asc_id').val());
        ClassApp.checkStock(uniq_details);
        _uniq_counter_details++;
        ClassApp.preventDuplicatePartSelect();
    });


    // delete details btn
    $(document).on('click', '.btn_remove_details', function() {
        _uniq_counter_details--;
        ClassApp.hitungTotal();
        ClassApp.deleteDetails($(this));
    });

    $(document).on('keyup', 'input[name="qty[]"]', function() {
        ClassApp.setDetailsQty($(this).attr('data-uniq'));
        ClassApp.hitungTotal();
    });

    $(document).on('keyup', 'input[name="selling_price[]"]', function() {
        ClassApp.setDetailsSellingPrice($(this).attr('data-uniq'));
        ClassApp.hitungTotal();
    });

    $(document).on('keyup', '#main_price', function() {
        ClassApp.hitungTotal();
    });


</script>
