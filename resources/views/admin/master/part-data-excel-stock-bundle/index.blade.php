@extends('admin.home')
@section('content')
<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }


</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="col-md-6">
                <h3 class="card-title">
                    {{ $title }}
                </h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ url('admin/part-data-excel-stock-bundle/create') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add </a>
            </div>
        </div>
        <div class="card-body">
        <table style="width: 100%;" id="table-bundle" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th>Bundle Name</th>
                        <th>Main Price</th>
                        <th>Created By</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(function() {

    });

    var tableOrder = globalCRUD.datatables({
        url: '/part-data-excel-stock-bundle/datatables',
        selector: '#table-bundle',
        columnsField: [
            {
                "className": 'details-control',
                "defaultContent": ''
            }
            ,'name',
            {
                name: 'main_price',
                data: 'main_price',
                render: function(data, type, row) {
                    return 'Rp. '+Helper.toCurrency(data);
                }
            },
            'user.name',
            {
                name: 'created_at',
                data: 'created_at',
                render: function(data, type, row) {
                    return moment(data).format("DD MMMM YYYY - H:m:s");
                }
            },
            {
                render: function(data, type, row){
                    var button_edit = "<button title='Edit' data-id='" + row.id + "' class='part-bundle-edit btn-hover-shine btn btn-warning btn-sm btn-white'><i class='fa fa-pencil'></i></button>";
                    var button_delete = "<button title='Delete' data-id='" + row.id + "' class='part-bundle-delete btn-hover-shine btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                    return button_edit+" "+button_delete;
                }
            }],
        actionLink : ""
    })

    // edit data
    $(document).on('click', '.part-bundle-edit', function() {
        var id = $(this).attr('data-id');
        Helper.redirectTo('/admin/part-data-excel-stock-bundle/edit/'+id);
    });

    $(document).on('click', '.part-bundle-delete', function() {
        var id = $(this).attr('data-id');
        tableOrder.delete('/part-data-excel-stock-bundle/'+id);
    });


    function templatex(d) {
        console.log(d)
        template = '';
        template += '<div class="col-md-6">';
        template += '<h5>Bundle Details</h5>';
        template += '<table class="table" id="example">';
        template += '<tr>';
        template += '<td>No</td>';
        template += '<td>Code Material</td>';
        template += '<td>Parts Description</td>';
        template += '<td>Quantity</td>';
        template += '<td>Hpp Price</td>';
        template += '<td>Selling Price</td>';
        template += '<td>Sub Total</td>';
        template += '</tr>';

        _.each(d.part_data_stock_bundle_detail, function(i, key) {
            template += '<tr>';
            template += '<td>' + (key + 1) + '</td>';
            template += '<td>' + i.code_material + '</td>';
            template += '<td>' + i.part_description + '</td>';
            template += '<td>' + i.quantity + '</td>';
            template += '<td>' + 'Rp. '+Helper.toCurrency(i.hpp_average != null ? i.hpp_average: 0) + '</td>';
            template += '<td>' + 'Rp. '+Helper.toCurrency(i.selling_price != null ? i.selling_price: 0) + '</td>';
            template += '<td>' + 'Rp. '+(i.selling_price != null && i.quantity != null ? Helper.toCurrency(parseInt(i.quantity) * parseInt(i.selling_price)) : 0) + '</td>';
            template += '</tr>';
        })
        template += '</table>';
        template += '</div>';

        return template;
    }
    var table = $('#table-bundle').DataTable();
    $('#table-bundle tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(templatex(row.data())).show();
            $(row.child()).addClass('smalltable');
            tr.addClass('shown');
        }
    });
    </script>
@endsection
