@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/orders-data-excel/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off">
    <div class="col-md-6" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                #1 Services
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Service Order No</label>
                    <div class="col-sm-12">
                        <input name="service_order_no" placeholder="Service Order No" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">ASC Name</label>
                    <div class="col-sm-12">
                        <input name="asc_name" placeholder="ASC Name" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Customer Name</label>
                    <div class="col-sm-12">
                        <input name="customer_name" placeholder="Customer Name" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Type Job</label>
                    <div class="col-sm-12">
                        <input name="type_job" placeholder="Type Job" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Defect Type Description</label>
                    <div class="col-sm-12">
                        <textarea name="defect_type" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Code</label>
                    <div class="col-sm-12">
                        <select name="engineer_code" id="engineer_code" class="form-control" required style="width:100%">
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Name</label>
                    <div class="col-sm-12">
                        <input name="engineer_name" id="engineer_name" placeholder="Engineer Name" type="text" class="form-control" readonly>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Assist Engineer Name</label>
                    <div class="col-sm-12">
                        <input name="assist_engineer_name" placeholder="Assist Engineer Name" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Merk Brand</label>
                    <div class="col-sm-12">
                        <input name="merk_brand" placeholder="Merk Brand" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Model Product</label>
                    <div class="col-sm-12">
                        <input name="model_product" placeholder="Model Product" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Status</label>
                    <div class="col-sm-12">
                        <select name="status" id="status" class="form-control" required style="width:100%">
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Reason</label>
                    <div class="col-sm-12">
                        <input name="reason" placeholder="Reason" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Remark Reason</label>
                    <div class="col-sm-12">
                        <input name="remark_reason" placeholder="Remark Reason" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Pending Aging Days</label>
                    <div class="col-sm-12">
                        <input name="pending_aging_days" placeholder="Pending Aging Days" type="text" class="form-control" readonly>
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                #4 Completed Service Time
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Repair Completed Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="repair_completed_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Repair Completed Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="repair_completed_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Closed Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="closed_date" id="closed_date" type="text" class="form-control"><br>
                    </div>
                    <div class="col-sm-12 input-group">
                        <small class="form-text text-muted warning_closed_date">Select Request Date First!</small>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Closed Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="closed_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Rating</label>
                    <div class="col-sm-12">
                        <input name="rating" placeholder="Rating" type="text" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                #2 Address
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Street</label>
                    <div class="col-sm-12">
                        <textarea name="street" placeholder="Street" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">City</label>
                    <div class="col-sm-12">
                        <input name="city" placeholder="City" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Mobile)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_mobile" placeholder="Phone No (Mobile)" type="text" class="form-control number_only">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Home)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_home" placeholder="Phone No (Home)" type="text" class="form-control number_only">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Office)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_office" placeholder="Phone No (Office)" type="text" class="form-control number_only">
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                3# Services Time
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Month</label>
                    <div class="col-sm-12">
                        <select class="form-control js-example-basic-single" name="month" id="month">
                        </select>
                        <small class="form-text text-muted">
                            Tahun <?php echo date("Y") ?>
                        </small>
                    </div>

                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Request Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="date" id="request_date" type="text" class="form-control date_format" required>
                    </div>

                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Request Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="request_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Picked Order Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="engineer_picked_order_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Picked Order Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="engineer_picked_order_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Admin Assigned to Engineer Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="admin_assigned_to_engineer_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Admin Assignned to Engineer Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="admin_assigned_to_engineer_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Assigned Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="engineer_assigned_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Assigned Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="engineer_assigned_time" type="text" class="form-control time">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Tech 1st Appointment Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="tech_1st_appointment_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Tech 1st Appointment Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="tech_1st_appointment_time" type="text" class="form-control time">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">1st Visit Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="1st_visit_date" type="text" class="form-control date_format">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">1st Visit Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="1st_visit_time" type="text" class="form-control time">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Parts Data
            </div>
            <div class="card-body">
                <div class="dynamic_part">
                    <div class="position-relative row form-group">
                        <div class="col-sm-3">
                            Code Material
                        </div>
                        <div class="col-sm-5">
                            Part Description
                        </div>
                        <div class="col-sm-2">
                            Quantity
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" class="btn btn-success btn_add_my_part"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <div class="position-relative row form-group">
                        <div class="col-sm-3">
                            <input type="text" placeholder="Code Material" name="code_material[]" data-uniq="xxx" class="form-control code_material_input-xxx" />
                        </div>
                        <div class="col-sm-5">
                            <input type="text" placeholder="Part Description" name="part_description[]" data-uniq="xxx" class="form-control part_description_input-xxx" />
                        </div>
                        <div class="col-sm-2">
                            <input type="text" placeholder="Quantity" name="quantity[]" data-uniq="xxx" class="form-control number_only quantity_input-xxx" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> CREATE </button>
        </div>
    </div>
</form>



@endsection

@section('script')

<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
@include('admin.master.orders-data-excel._js')
<script>

    // var option_remove = [];
    var data = [
        {id: 'Januari', text: 'Januari', title: 'Januari'},
        {id: 'Februari', text: 'Februari', title: 'Februari'},
        {id: 'Maret', text: 'Maret', title: 'Maret'},
        {id: 'April', text: 'April', title: 'April'},
        {id: 'Mei', text: 'Mei', title: 'Mei'},
        {id: 'Juni', text: 'Juni', title: 'Juni'},
        {id: 'Juli', text: 'Juli', title: 'Juli'},
        {id: 'Agustus', text: 'Agustus', title: 'Agustus'},
        {id: 'September', text: 'September', title: 'September'},
        {id: 'Oktober', text: 'Oktober', title: 'Oktober'},
        {id: 'November', text: 'November', title: 'November'},
        {id: 'Desember', text: 'Desember', title: 'Desember'},
    ];

    $("#month").select2({
        data: data,
        escapeMarkup: function(markup) {
            return markup;
        }
    })


    $(function() {

        $('.time').inputmask("hh:mm:ss", {
            placeholder: "hh:mm:ss",
            insertMode: false,
            showMaskOnHover: false,
            hourFormat: "24",
            clearIncomplete: true
        });
        // $('.date').inputmask("dd-mm-yyyy", {
        //     placeholder: "dd-mm-yyyy",
        //     clearIncomplete: true
        // });
        Helper.dateFormat('.date_format');
        Helper.onlyNumberInput('.number_only');

    });


    $(function() {
        var xx = 1;

        $(document)
            .on('focus', 'input[name="code_material[]"], input[name="part_description[]"]', function(e) {
                if ($(this).is('input[name="code_material[]"]:last')) {
                    templateNewInputPartData(xx)
                    xx++;
                }

                if ($(this).is('input[name="quantity[]"]:last')) {
                    templateNewInputPartData(xx)
                    xx++;
                }
            })

        $(document)
            .on('click', '.btn_add_my_part', function() {
                templateNewInputPartData(xx)
                    xx++;
            })

        // delete part my inv
        $(document)
            .on('click', '.btn_remove_my_part', function() {
                $('.btn_add_my_part').prop("disabled", false);
                var button_id = $(this).attr("data-uniq");
                $('#my_part_row_' + button_id + '').remove();
                // console.log(option_remove);
            });

    })

    function templateNewInputPartData(xx) {
        $('.dynamic_part').append((`
            <div class="position-relative row form-group" id="my_part_row_${xx}">
                <div class="col-sm-3">
                    <input type="text" placeholder="Code Material" name="code_material[]" data-uniq="${xx}" class="form-control code_material_input-${xx}" />
                </div>
                <div class="col-sm-5">
                    <input type="text" placeholder="Part Description" name="part_description[]" data-uniq="${xx}" class=" part_description-${xx} form-control" />
                </div>
                <div class="col-sm-2">
                    <input type="text" placeholder="Quantity" name="quantity[]" data-uniq="${xx}" class=" quantity-${xx} number_only form-control" />
                </div>
                <div class="col-sm-1" style="padding-left:0px;">
                    <button type="button" data-uniq="${xx}" class="btn-transition btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                </div>
            </div>
        `));
        Helper.onlyNumberInput('.number_only');
    }

    $("#form-data").submit(function(e) {
        var data = Helper.serializeForm($(this));
        Axios.post('/orders-data-excel', data)
            .then(function(response) {
                // Helper.successNotif(response);
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Orders Data Excel Has Been Saved',
                    });
                    window.location.href = Helper.redirectUrl('/admin/orders-data-excel/show');
                }

            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
        e.preventDefault();

    });



</script>


@endsection
