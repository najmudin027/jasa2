@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/orders-data-excel/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off">
    <input type="hidden" name="id" value="{{ $getData->id }}">
    <div class="col-md-6" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                #1 Services
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Service Order No</label>
                    <div class="col-sm-12">
                        <input name="service_order_no" placeholder="Service Order No" type="text" class="form-control" value="{{ $getData->service_order_no }}" required>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">ASC Name</label>
                    <div class="col-sm-12">
                        <input name="asc_name" placeholder="ASC Name" type="text" value="{{ $getData->asc_name }}" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Customer Name</label>
                    <div class="col-sm-12">
                        <input name="customer_name" placeholder="Customer Name" type="text" value="{{ $getData->customer_name }}" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Type Job</label>
                    <div class="col-sm-12">
                        <input name="type_job" placeholder="Type Job" type="text" class="form-control"  value="{{ $getData->type_job }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Defect Type Description</label>
                    <div class="col-sm-12">
                        <textarea name="defect_type" class="form-control" rows="3" >{{ $getData->defect_type }}</textarea>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Code</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="engineer_code" name="engineer_code" style="width:100%">
                            <option value="{{ $getData->engineer_code }}" selected>{{ $getData->engineer_code }}</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Name</label>
                    <div class="col-sm-12">
                        <input name="engineer_name" id="engineer_name" placeholder="Engineer Name" type="text" value="{{ $getData->engineer_name }}" class="form-control" readonly>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Assist Engineer Name</label>
                    <div class="col-sm-12">
                        <input name="assist_engineer_name" placeholder="Assist Engineer Name" type="text" class="form-control" value="{{ $getData->assist_engineer_name }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Merk Brand</label>
                    <div class="col-sm-12">
                        <input name="merk_brand" placeholder="Merk Brand" type="text" class="form-control" value="{{ $getData->merk_brand }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Model Product</label>
                    <div class="col-sm-12">
                        <input name="model_product" placeholder="Model Product" type="text" class="form-control" value="{{ $getData->model_product }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Status</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="status" name="status" style="width:100%">
                            <option value="{{ $getData->status }}" selected>{{ $getData->status }}</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Reason</label>
                    <div class="col-sm-12">
                        <input name="reason" placeholder="Reason" type="text" class="form-control" value="{{ $getData->reason }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Remark Reason</label>
                    <div class="col-sm-12">
                        <input name="remark_reason" placeholder="Remark Reason" type="text" class="form-control" value="{{ $getData->remark_reason }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Pending Aging Days</label>
                    <div class="col-sm-12">
                        <input name="pending_aging_days" placeholder="Pending Aging Days" type="text" class="form-control" value="{{ $getData->pending_aging_days }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                #4 Completed Service Time
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Repair Completed Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="repair_completed_date" type="text" class="form-control date_format" value="{{ $getData->repair_completed_date->format('d-m-Y') }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Repair Completed Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="repair_completed_time" type="text" class="form-control time" value="{{ $getData->repair_completed_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Closed Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="closed_date" id="closed_date" type="text" class="form-control" value="{{ $getData->closed_date->format('d-m-Y') }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Closed Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="closed_time" type="text" class="form-control time" value="{{ $getData->closed_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Rating</label>
                    <div class="col-sm-12">
                        <input name="rating" placeholder="Rating" type="text" class="form-control" value="{{ $getData->rating }}">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                #2 Address
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Street</label>
                    <div class="col-sm-12">
                        <textarea name="street" placeholder="Street" class="form-control" rows="3">{{ $getData->street }}</textarea>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">City</label>
                    <div class="col-sm-12">
                        <input name="city" placeholder="City" type="text" class="form-control" value="{{ $getData->city }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Mobile)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_mobile" placeholder="Phone No (Mobile)" type="text" class="form-control number_only" value="{{ $getData->phone_no_mobile }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Home)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_home" placeholder="Phone No (Home)" type="text" class="form-control number_only" value="{{ $getData->phone_no_home }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Office)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_office" placeholder="Phone No (Office)" type="text" class="form-control number_only" value="{{ $getData->phone_no_office }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                3# Services Time
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Month</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="month" name="month" style="width:100%">
                            <option value="{{ $getData->month }}" selected>{{ $getData->month }}</option>
                        </select>
                        <small class="form-text text-muted">
                            Tahun <?php echo date("Y") ?>
                        </small>
                    </div>

                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Request Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="date" id="request_date" type="text" class="form-control date_format" value="{{ $getData->date->format('d-m-Y') }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Request Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="request_time" type="text" class="form-control time" value="{{ $getData->request_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Picked Order Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="engineer_picked_order_date" type="text" class="form-control date_format" value="{{ $getData->engineer_picked_order_date->format('d-m-Y') }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Picked Order Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="engineer_picked_order_time" type="text" class="form-control time" value="{{ $getData->engineer_picked_order_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Admin Assigned to Engineer Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="admin_assigned_to_engineer_date" type="text" class="form-control date_format" value="{{ $getData->admin_assigned_to_engineer_date->format('d-m-Y') }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Admin Assignned to Engineer Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="admin_assigned_to_engineer_time" type="text" class="form-control time" value="{{ $getData->admin_assigned_to_engineer_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Assigned Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="engineer_assigned_date" type="text" class="form-control date_format" value="{{ $getData->engineer_assigned_date->format('d-m-Y') }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Assigned Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="engineer_assigned_time" type="text" class="form-control time" value="{{ $getData->engineer_assigned_time }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Tech 1st Appointment Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="tech_1st_appointment_date" type="text" class="form-control date_format" value="{{ $getData->tech_1st_appointment_date->format('d-m-Y') }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Tech 1st Appointment Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="tech_1st_appointment_time" type="text" class="form-control time" value="{{ $getData->tech_1st_appointment_time }}">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">1st Visit Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="1st_visit_date" type="text" class="form-control date_format" value="{{ $getData->{'1st_visit_date'}->format('d-m-Y') }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">1st Visit Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="1st_visit_time" type="text" class="form-control time" value="{{ $getData->{'1st_visit_time'} }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> SAVE ORDERS DATA EXCEL</button>
        </div>
    </div>
</form>

<div class="col-md-12" style="margin-top: 10px;">
    <form id="part-data-form">
            <input type="hidden" id="orders_data_excel_id" value="{{ $getData->id }}" name="orders_data_excel_id">
        <div class="card">
            <div class="card-header header-border">
                Parts Data
            </div>
            <div class="card-body">
                <div class="dynamic_part">
                    <div class="position-relative row form-group">
                        <div class="col-sm-3">
                            Code Material
                        </div>
                        <div class="col-sm-5">
                            Part Description
                        </div>
                        <div class="col-sm-2">
                            Quantity
                        </div>
                        <div class="col-sm-1" style="padding-left:0px;">
                            <button type="button" class="btn btn-success btn_add_my_part"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                    @foreach($partData as $key => $part)
                        <?php $uniq = $key+1; ?>
                        <div class="position-relative row form-group dynamic_sparepart_content" id="my_part_row_{{ $uniq }}">
                            <input type="hidden" value="{{ $part->id }}" name="part_data_id[]" />
                            <div class="col-sm-3">
                                <input type="text" placeholder="name" name="code_material[]" data-uniq="{{ $uniq }}" class="form-control code_material_input-{{ $uniq }}" value="{{ $part->code_material }}" />
                            </div>
                            <div class="col-sm-5">
                                <input type="text" placeholder="Part Description" name="part_description[]" data-uniq="{{ $uniq }}" class="form-control part_description_input-{{ $uniq }}" value="{{ $part->part_description }}" />
                            </div>
                            <div class="col-sm-2">
                                <input type="text" placeholder="unit" name="quantity[]" data-uniq="{{ $uniq }}" class="form-control number_only quantity_input-{{ $uniq }}" value="{{ $part->quantity }}" />
                            </div>
                            <div class="col-sm-1" style="padding-left:0px;">
                                <button type="button" data-part_data_id="{{ $part->id }}" data-uniq="{{ $uniq }}" class="btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="card-footer ">
                <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> SAVE PART DATA</button>
            </div>
        </div>
    </form>
</div>

<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Orders Data Excel Logs
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-orders-data-excel-logs" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Type Modified</th>
                        <th>By User (Name)</th>
                        <th>Created At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection

@section('script')
@include('admin.master.orders-data-excel._js')
<script>
    var option_remove =[];
    var id = $('input[name="id"]').val();
    var data = [
        {id: 'Januari', text: 'Januari', title: 'Januari'},
        {id: 'Februari', text: 'Februari', title: 'Februari'},
        {id: 'Maret', text: 'Maret', title: 'Maret'},
        {id: 'April', text: 'April', title: 'April'},
        {id: 'Mei', text: 'Mei', title: 'Mei'},
        {id: 'Juni', text: 'Juni', title: 'Juni'},
        {id: 'Juli', text: 'Juli', title: 'Juli'},
        {id: 'Agustus', text: 'Agustus', title: 'Agustus'},
        {id: 'September', text: 'September', title: 'September'},
        {id: 'Oktober', text: 'Oktober', title: 'Oktober'},
        {id: 'November', text: 'November', title: 'November'},
        {id: 'Desember', text: 'Desember', title: 'Desember'},
    ];

    $("#month").select2({
        data: data,
        escapeMarkup: function(markup) {
            return markup;
        }
    })


    $(function() {
        // ClassApp.select2CodeMaterialLoad();
        $('.time').inputmask("hh:mm:ss", {
            placeholder: "hh:mm:ss",
            insertMode: false,
            showMaskOnHover: false,
            hourFormat: "24",
            clearIncomplete: true
        });
        $('.date').inputmask("dd-mm-yyyy", {
            placeholder: "dd-mm-yyyy",
            clearIncomplete: true
        });
        Helper.dateFormat('.date_format');
    });

    Helper.onlyNumberInput('.number_only');

    $("#form-data").submit(function(e) {

        var form = Helper.serializeForm($(this));
        var fd = new FormData($(this)[0]);
        console.log(form, data);

        $.ajax({
            url:Helper.apiUrl('/orders-data-excel/'+id+''),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Orders Data Excel Has Been Saved',
                    });
                    location.reload();
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });

                    })
                }

            },
        });
        e.preventDefault();
    });

    SparepartClass = {
        create: function(xx) {
            $('.dynamic_part').append((`
                <input type="hidden" value="0" name="part_data_id[]">
                <div class="position-relative row form-group" id="my_part_row_${xx}">
                    <div class="col-sm-3">
                        <input type="text" placeholder="Code Material" name="code_material[]" data-uniq="${xx}" class="form-control code_material_input-${xx}" />
                    </div>
                    <div class="col-sm-5">
                        <input type="text" placeholder="Part Description" name="part_description[]" data-uniq="${xx}" class="part_descriptioninput-${xx} form-control" />
                    </div>
                    <div class="col-sm-2">
                        <input type="text" placeholder="unit" name="quantity[]" data-uniq="${xx}" class=" number_only quantity_input-${xx} form-control" />
                    </div>
                    <div class="col-sm-1" style="padding-left:0px;">
                        <button type="button" data-part_data_id="0" data-uniq="${xx}" class="btn-transition btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            `));

            Helper.onlyNumberInput('.number_only');

        },
        delete: function($el) {
            var uniq = $el.attr("data-uniq");
            var part_data_id = parseInt($el.attr("data-part_data_id"));

            if (part_data_id != 0) {
                Helper.confirm(function() {
                    Axios.delete('/orders-data-excel/destroy_part_data/' + part_data_id)
                        .then(function(response) {
                            Helper.successNotif('Success Delete');
                            $('#my_part_row_' + uniq + '').remove();
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
            } else {
                $('#my_part_row_' + uniq + '').remove();
            }
        }
    };

    $(function() {
        var xx = 1;

        Helper.onlyNumberInput('.quantity_i');

        $('#part-data-form')
            .submit(function(e) {
                var data = Helper.serializeForm($(this));
                Axios.post('/orders-data-excel/update_part_data/' + data.orders_data_excel_id, data)
                    .then(function(response) {
                        // Helper.successNotif(response);
                        // location.reload();
                        if (response != 0) {
                            iziToast.success({
                                title: 'OK',
                                position: 'topRight',
                                message: 'Orders Data Excel Has Been Saved',
                            });
                            // window.location.href = Helper.redirectUrl('/admin/orders-data-excel/show');
                        }

                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
                e.preventDefault();
            });

        $(document)
            .on('click', '.btn_add_my_part', function() {
                console.log('asd')
                SparepartClass.create(new Date().getTime() + xx)
                    xx++;
            })

        // add row part
        $(document)
            .on('focus', 'input[name="code_material[]"], input[name="part_description[]"]', function(e) {
                if ($(this).is('input[name="code_material[]"]:last')) {
                    SparepartClass.create(new Date().getTime() + xx)
                    xx++;
                }

                if ($(this).is('input[name="quantity[]"]:last')) {
                    SparepartClass.create(new Date().getTime() + xx)
                    xx++;
                }
            })

        // delete part my inv
        $(document)
            .on('click', '.btn_remove_my_part', function() {
                $('.btn_add_my_part').prop("disabled", false);
                SparepartClass.delete($(this));
            });
    })

    globalCRUD.datatables({
        url: '/orders-data-excel/datatables_logs/'+id+'',
        selector: '#table-orders-data-excel-logs',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "type",
            name: "type",
            render: function(row,data,full) {
                if(row == 1) {
                    return 'Created';
                } else if(row == 2) {
                    return 'Updated'
                } else {
                    return 'Deleted';
                }
            }
        },
        {
            data: "user.name",
            name: "user.name",
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm");
            }
        }
        ]
    })


</script>

@endsection
