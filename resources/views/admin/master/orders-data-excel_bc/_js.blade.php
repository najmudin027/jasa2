<link rel="stylesheet" href="{{ asset('adminAssets/css/easy-autocomplete.min.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/easy-autocomplete.themes.min.css') }}">
<script type="text/javascript" src="{{ asset('adminAssets/js/jquery.easy-autocomplete.min.js') }}"></script>
<script>




    globalCRUD.select2('#asc_id', '/asc-excel/select2');
    $('#asc_id').on('select2:select', function (e) {
        $('#asc_name').val(e.params.data.text);
    });

    $(function() {
        $('#closed_date').prop('readonly', true);
    })

    $(document).on('change', '#asc_id', function() {
        if($(this).val() != "") {
            $('select[name="code_material_id[]"]').prop("disabled", false);
            $('.notice_asc_name').hide();
            $('.btn_add_my_part').prop("disabled", false);
            ClassApp.select2CodeMaterial('xxx',$(this).val());

        }
    })


    $(document).on('change','#closed_date', function(e) {
        if($('input[name="date"]').val() != "") {
            var startDate = moment($('#request_date').val(), "DD.MM.YYYY");
            var endDate = moment($('#closed_date').val(), "DD.MM.YYYY");
            var result = endDate.diff(startDate, 'days');
            if(result >= 0) {
                $('input[name="pending_aging_days"]').val(result);
            } else {
                Helper.warningNotif('Closed Date cant less than Request Date')
                $(this).val('');
            }
        }

    });
    $(document).on('change', '#request_date', function(e) {
        if($(this).val() != "") {
            $('#closed_date').prop('disabled', false);
            $('#closed_date').val('');
            $('.warning_closed_date').hide();
            $('#closed_date').datetimepicker({
                minDate: $('#request_date').val(),
                format: 'd-m-Y',
                formatDate: 'd-m-Y',
                timepicker: false,
                scrollInput: false
                // inline:true,
            });
        }
    });

    globalCRUD.select2('#engineer_code', '/engineer-code-excel/select2', function(item) {
                return {
                    id: item.engineer_code,
                    text:item.engineer_code,
                    engineer_name:item.engineer_name,
                }
            });

    globalCRUD.select2('#status', '/status-excel/select2', function(item) {
                return {
                    id: item.status,
                    text:item.status,
                }
            });

    $('#engineer_code').on('select2:select', function (e) {
        $('#engineer_name').val(e.params.data.engineer_name);
    });
    // var data_stock_id = [];
    var ClassApp = {
        // select2CodeMaterialLoad : function() {
        //     $('select[name="code_material_id[]"]').each(function(){
        //         var uniq = $(this).attr('data-uniq');
        //         ClassApp.select2CodeMaterial(uniq,$('#asc_id').val())
        //         ClassApp.checkStock(uniq)
        //         $(document).on('change', '#asc_id', function() {
        //             if($(this).val() != "") {
        //                 $('select[name="code_material_id[]"]').prop("disabled", false);
        //                 $('.notice_asc_name').hide();
        //                 $('.btn_add_my_part').prop("disabled", false);
        //                 ClassApp.select2CodeMaterial(uniq,$(this).val());

        //             }
        //         })
        //     });

        // },

        // select2CodeMaterial : function(xx, asc_id=null) {
        //     var option_remove=[];
        //     $('select[name="code_material_id[]"]').each(function() {
        //         if($(this).val() != null) {
        //             if(!option_remove.includes(parseInt($(this).val()))) {
        //                 option_remove.push(parseInt($(this).val()));
        //             }
        //         }
        //     });
        //     globalCRUD.select2('.code_material_id_input-'+xx+'', '/part-data-excel-stock/select2/'+asc_id+'/'+option_remove.join(', ')+'', function(item) {

        //         return {
        //             id: item.id,
        //             text:item.code_material,
        //             part_description:item.part_description,
        //             stock:item.stock
        //         }
        //     });


        //     $('.code_material_id_input-'+xx+'').on('select2:select', function (e) {

        //         // if(option_remove != null) {
        //         //     if(option_remove.includes(parseInt(e.params.data.id))) {
        //         //         $('.btn_add_my_part').prop("disabled", true);
        //         //         Helper.warningNotif("Opps sudah dipilih!");
        //         //         return false;
        //         //     } else {
        //         //         $('.btn_add_my_part').prop("disabled", false);
        //         //     }
        //         // }
        //         $('.remove_my_part_'+xx+'').attr('data-code_material_id',e.params.data.id),
        //         $('.part_description_input-'+xx+'').val(e.params.data.part_description);
        //         $('.code_material_input-'+xx+'').val(e.params.data.text);
        //         $('.stock_part_input-'+xx+'').val(e.params.data.stock);
        //         $('.btn_add_my_part').prop("disabled", false);
        //         console.log(option_remove);
        //     });

        // },
        // checkStock: function(xx) {
        //     $('.quantity_input-' + xx + '').keyup(function(e) {
        //         var max = parseInt($('.stock_part_input-' + xx + '').val());
        //         console.log(max)
        //         if (parseInt($(this).val()) > max) {
        //             e.preventDefault();
        //             $(this).val(max);
        //         } else {
        //             $(this).val();
        //         }
        //     });
        // },
        // autoCompleteField: function(elem_this) {
        // var namefield = elem_this.attr('id');
        // var val;
        // var options = {
        //         url: function(phrase) {
        //             return Helper.apiUrl('/ode/autoCompleteField');
        //         },
        //         adjustWidth: false,

        //         getValue: function(element) {
        //             return element[namefield];
        //         },

        //         template: {
        //             type: "custom",
        //             method: function(value, item) {
        //                 if(namefield == 'engineer_code') {
        //                     return value+' - <i>Engineer Name ('+item.engineer_name+')</i>';
        //                 } else if(namefield == 'engineer_name') {
        //                     return value+' - <i>Engineer Code ('+item.engineer_code+')</i>';
        //                 } else {
        //                     return value
        //                 }

        //             }
        //         },


        //         list: {
        //             onSelectItemEvent: function() {
        //                 if(namefield === 'engineer_code') {
        //                     let engineer_name = $("#engineer_code").getSelectedItemData().engineer_name;
        //                     $("#engineer_name").val(engineer_name).trigger("change");
        //                 } else if(namefield === 'engineer_name') {
        //                     let engineer_code = $("#engineer_name").getSelectedItemData().engineer_code;
        //                     $("#engineer_code").val(engineer_code).trigger("change");
        //                 }

        //             },
        //             match: {
        //                 enabled: true
        //             }
        //         },

        //         preparePostData: function(data) {
        //             data.phrase = $("#"+namefield+"").val();
        //             val = data.phrase;
        //             return data;
        //         },

        //         ajaxSettings: {
        //             dataType: "json",
        //             method: "POST",
        //             data: {
        //                 dataType: "json",
        //                 namefield : namefield,
        //                 val : val
        //             }
        //         },


        //         requestDelay: 400
        //     };

        //     $("#"+namefield+"").easyAutocomplete(options);
        // },
    };
</script>
