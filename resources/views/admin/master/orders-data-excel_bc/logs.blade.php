@extends('admin.home')
@section('content')

<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Orders Data Excel Logs (Deleted Only)
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-orders-data-excel-logs" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Service Order No.</th>
                        <th>Type Modified</th>
                        <th>By User (Name)</th>
                        <th>Created At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            General Journal Logs (Deleted Only)
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-general-journal-logs" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Code</th>
                        <th>Type Modified</th>
                        <th>By User (Name)</th>
                        <th>Created At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Excel Income Logs (Deleted Only)
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-excel-income-logs" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Code</th>
                        <th>Type Modified</th>
                        <th>By User (Name)</th>
                        <th>Created At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    //orders data excel logs
    globalCRUD.datatables({
        url: '/orders-data-excel/datatables_logs/0/3',
        selector: '#table-orders-data-excel-logs',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "service_order_no",
            name: "service_order_no",
        },
        {
            data: "type",
            name: "type",
            render: function(row,data,full) {
                // if(row == 1) {
                //     return 'Created';
                // } else if(row == 2) {
                //     return 'Updated'
                // } else {
                    return '<div class="mb-2 mr-2 badge badge-pill badge-danger">Deleted</div>';
                // }
            }
        },
        {
            data: "user.name",
            name: "user.name",
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm");
            }
        }
        ]
    })

    globalCRUD.datatables({
        url: '/general-journal/datatables_logs/0/3',
        selector: '#table-general-journal-logs',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "code_journal",
            name: "code_journal",
        },
        {
            data: "type",
            name: "type",
            render: function(row,data,full) {
                // if(row == 1) {
                //     return 'Created';
                // } else if(row == 2) {
                //     return 'Updated'
                // } else {
                    return '<div class="mb-2 mr-2 badge badge-pill badge-danger">Deleted</div>';
                // }
            }
        },
        {
            data: "user.name",
            name: "user.name",
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm");
            }
        }
        ]
    })

    globalCRUD.datatables({
        url: '/excel-income/datatables_logs/0/3',
        selector: '#table-excel-income-logs',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "code",
            name: "code",
        },
        {
            data: "type",
            name: "type",
            render: function(row,data,full) {
                // if(row == 1) {
                //     return 'Created';
                // } else if(row == 2) {
                //     return 'Updated'
                // } else {
                    return '<div class="mb-2 mr-2 badge badge-pill badge-danger">Deleted</div>';
                // }
            }
        },
        {
            data: "user.name",
            name: "user.name",
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm");
            }
        }
        ]
    })

</script>
@endsection
