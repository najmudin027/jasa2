@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/orders-data-excel/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;">
    <input type="hidden" name="id" value="{{ $getData->id }}">
    <div class="col-md-6" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                #1 Services
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Service Order No.</label>
                    <div class="col-sm-12">
                        <input name="service_order_no" placeholder="Service Order No." type="text" value="{{ $getData->service_order_no }}" class="form-control" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">ASC Name</label>
                    <div class="col-sm-12">
                        <input name="asc_name" placeholder="ASC Name" type="text" value="{{ $getData->asc_name }}" class="form-control" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Customer Name</label>
                    <div class="col-sm-12">
                        <input name="customer_name" placeholder="Customer Name" type="text" value="{{ $getData->customer_name }}" class="form-control" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Type Job</label>
                    <div class="col-sm-12">
                        <input name="type_job" placeholder="Type Job" type="text" class="form-control" disabled  value="{{ $getData->type_job }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Defect Type Description</label>
                    <div class="col-sm-12">
                        <textarea name="defect_type" class="form-control" disabled rows="3" >{{ $getData->defect_type }}</textarea>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Code</label>
                    <div class="col-sm-12">
                        <input name="engineer_code" placeholder="Engineer Code" type="text" class="form-control" disabled value="{{ $getData->engineer_code }}" >
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Name</label>
                    <div class="col-sm-12">
                        <input name="engineer_name" placeholder="Engineer Name" type="text" value="{{ $getData->engineer_name }}" class="form-control" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Assist Engineer Name</label>
                    <div class="col-sm-12">
                        <input name="assist_engineer_name" placeholder="Assist Engineer Name" type="text" class="form-control" disabled value="{{ $getData->assist_engineer_name }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Assist Engineer Name</label>
                    <div class="col-sm-12">
                        <input name="assist_engineer_name" placeholder="Assist Engineer Name" type="text" class="form-control" disabled value="{{ $getData->assist_engineer_name }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Merk Brand</label>
                    <div class="col-sm-12">
                        <input name="merk_brand" placeholder="Merk Brand" type="text" class="form-control" disabled value="{{ $getData->merk_brand }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Model Product</label>
                    <div class="col-sm-12">
                        <input name="module_product" placeholder="Model Product" type="text" class="form-control" disabled value="{{ $getData->module_product }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Status</label>
                    <div class="col-sm-12">
                        <input name="status" placeholder="Status" type="text" class="form-control" disabled value="{{ $getData->status }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Reason</label>
                    <div class="col-sm-12">
                        <input name="reason" placeholder="Reason" type="text" class="form-control" disabled value="{{ $getData->reason }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Remark Reason</label>
                    <div class="col-sm-12">
                        <input name="remark_reason" placeholder="Remark Reason" type="text" class="form-control" disabled value="{{ $getData->remark_reason }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Pending Aging Days</label>
                    <div class="col-sm-12">
                        <input name="pending_aging_days" placeholder="Pending Aging Days" type="text" class="form-control" disabled value="{{ $getData->pending_aging_days }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                #4 Completed Service Time
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Repair Completed Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="repair_completed_date" type="text" class="form-control date_format" value="{{ $getData->repair_completed_date->format('d-m-Y') }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Repair Completed Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="repair_completed_time" type="text" class="form-control time" value="{{ $getData->repair_completed_time }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Closed Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="closed_date" type="text" class="form-control date_format" value="{{ $getData->closed_date->format('d-m-Y') }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Closed Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="closed_time" type="text" class="form-control time" value="{{ $getData->closed_time }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Rating</label>
                    <div class="col-sm-12">
                        <input name="rating" placeholder="Rating" type="text" class="form-control" disabled required="" value="{{ $getData->rating }}" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                #2 Address
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Street</label>
                    <div class="col-sm-12">
                        <textarea name="street" placeholder="Street" class="form-control" disabled rows="3">{{ $getData->street }}</textarea>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">City</label>
                    <div class="col-sm-12">
                        <input name="city" placeholder="City" type="text" class="form-control" disabled value="{{ $getData->city }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Mobile)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_mobile" placeholder="Phone No (Mobile)" type="text" class="form-control" disabled value="{{ $getData->phone_no_mobile }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Home)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_home" placeholder="Phone No (Home)" type="text" class="form-control" disabled value="{{ $getData->phone_no_home }}">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Phone No (Office)</label>
                    <div class="col-sm-12">
                        <input name="phone_no_office" placeholder="Phone No (Office)" type="text" class="form-control" disabled value="{{ $getData->phone_no_office }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                3# Services Time
            </div>
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Month</label>
                    <div class="col-sm-12">
                        <select class="form-control" disabled id="month" name="month" style="width:100%" disabled>
                            <option value="{{ $getData->month }}" selected>{{ $getData->month }}</option>
                        </select>

                    </div>

                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="date" type="text" class="form-control date_format" value="{{ $getData->date->format('d-m-Y') }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Request Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="request_time" type="text" class="form-control time" value="{{ $getData->request_time }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Picked Order Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="engineer_picked_order_date" type="text" class="form-control date_format" value="{{ $getData->engineer_picked_order_date->format('d-m-Y') }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Picked Order Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="engineer_picked_order_time" type="text" class="form-control time" value="{{ $getData->engineer_picked_order_time }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Admin Assign to Engineer Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="admin_assigned_to_engineer_date" type="text" class="form-control date_format" value="{{ $getData->admin_assigned_to_engineer_date->format('d-m-Y') }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Admin Assignned to Engineer Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="admin_assigned_to_engineer_time" type="text" class="form-control time" value="{{ $getData->admin_assigned_to_engineer_time }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Assigned Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="engineer_assigned_date" type="text" class="form-control date_format" value="{{ $getData->engineer_assigned_date->format('d-m-Y') }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group" disabled>
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Engineer Assigned Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="engineer_assigned_time" type="text" class="form-control time" value="{{ $getData->engineer_assigned_time }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Tech 1st Appointment Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="tech_1st_appointment_date" type="text" class="form-control date_format" value="{{ $getData->tech_1st_appointment_date->format('d-m-Y') }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Tech 1st Appointment Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="tech_1st_appointment_time" type="text" class="form-control time" value="{{ $getData->tech_1st_appointment_time }}" disabled>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">1st Visit Date</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar-alt"></i>
                            </div>
                        </div>
                        <input name="1st_visit_date" type="text" class="form-control date_format" value="{{ $getData->{'1st_visit_date'}->format('d-m-Y') }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">1st Visit Time</label>
                    <div class="col-sm-12 input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                        <input name="1st_visit_time" type="text" class="form-control time" value="{{ $getData->{'1st_visit_time'} }}" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Parts Data
            </div>
            <div class="card-body">

                <div class="dynamic_part">
                    <div class="position-relative row form-group">
                        <div class="col-sm-3">
                            Code Material
                        </div>
                        <div class="col-sm-5">
                            Part Description
                        </div>
                        <div class="col-sm-2">
                            Quantity
                        </div>
                    </div>
                    @foreach($partData as $part)
                        <div class="position-relative row form-group">
                            <div class="col-sm-3">
                                <input type="text" name="code_material[]" class="form-control" disabled value="{{ $part->code_material }}" disabled/>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" name="part_description[]" class="form-control" disabled  value="{{ $part->part_description }}" disabled/>
                            </div>
                            <div class="col-sm-2">
                                <input type="text" name="quantity[]" class="form-control" disabled  value="{{ $part->quantity }}" disabled/>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</form>

<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Orders Data Excel Logs
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-orders-data-excel-logs" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Type Modified</th>
                        <th>By User (Name)</th>
                        <th>Created At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('.date_format').datetimepicker({
            timepicker:false,
            format:'d-m-Y',
            mask:true
        });
    });
    var id = $('input[name="id"]').val();
    globalCRUD.datatables({
        url: '/orders-data-excel/datatables_logs/'+id+'',
        selector: '#table-orders-data-excel-logs',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "type",
            name: "type",
            render: function(row,data,full) {
                if(row == 1) {
                    return 'Created';
                } else if(row == 2) {
                    return 'Updated'
                } else {
                    return 'Deleted';
                }
            }
        },
        {
            data: "user.name",
            name: "user.name",
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm");
            }
        }
        ]
    })
</script>

@endsection
