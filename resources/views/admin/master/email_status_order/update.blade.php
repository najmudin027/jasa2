@extends('admin.home')
@section('content')
<style>
.legend {
    font-size:8pt;
    margin-bottom:7px;
}


.ck-editor__editable_inline {
    min-height: 350px;
}

/*Select2 ReadOnly Start*/
select[readonly].select2-hidden-accessible + .select2-container {
        pointer-events: none;
        touch-action: none;
    }

    select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
        background: #eee;
        box-shadow: none;
    }

    select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow, select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
        display: none;
    }

/*Select2 ReadOnly End*/
</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/email-status-order/show') }}" class="mb-2 mr-2 btn btn-primary add-bank-transfer" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
        <form id="form-email-status-update">
            <input type="hidden" id="id_email" name="id_email" value="{{ $id_email }}">
            <input type="hidden" id="orders_statuses_id" name="orders_statuses_id" value="{{ $getData[0]->orders_statuses_id }}">
            <div class="card-body">
                <div class="card-header">
                    Template Email
                    <div class="btn-actions-pane-right">
                        <div class="nav">
                            <a data-toggle="tab" href="#tab-eg2-0" class="btn-tab btn-wide active btn btn-outline-success btn-md" data-desc="customer">Template Customer</a>
                            <a data-toggle="tab" href="#tab-eg2-1" class="btn-tab btn-wide mr-1 ml-1  btn btn-outline-success btn-md" data-desc="technicians">Template Technician</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        @foreach($getData as $key => $data)
                            <div class="tab-pane <?php if($key==0) {echo 'active';}?>" id="tab-eg2-{{ $key }}" role="tabpanel">
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">Subject</label>
                                    <div class="col-sm-10">
                                        <input name="subject{{ $key }}" value="{{ $data->subject }}" placeholder="Title Name" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">Orders Status</label>
                                    <div class="col-sm-10">
                                        <select id="orders_statuses_id{{ $key }}" name="orders_statuses_id{{ $key }}" style="width:100%" readonly>
                                            <option value="{{ $data->orders_statuses_id }}" selected>{{ $data->order_status->name }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">Content</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="content{{ $key }}" id="content{{ $key }}" maxlength="300" placeholder="Start Typin..."autofocus>{{ $data->content }}</textarea>
                                    </div>
                                    <div class="col-sm-2">
                                        <h6>Legend : </h6>
                                        <p class="legend">[{NAME_CUSTOMER}]</p>
                                        <p class="legend">[{NAME_TECHNICIAN}]</p>
                                        <p class="legend">[{EMAIL_CUSTOMER}]</p>
                                        <p class="legend">[{EMAIL_TECHNICIAN}]</p>
                                        <p class="legend">[{ORDER_CODE}]</p>
                                        <p class="legend">[{STATUS_ORDER}]</p>
                                        <p class="legend">[{SCHEDULE}]</p>
                                        <p class="legend">[{TICKET_NO}]</p>
                                        <p class="legend">[{SUBJECT_TICKET}]</p>
                                        <p class="legend">[{DESCRIPTION_TICKET}]</p>
                                        <p class="legend">[{STATUS_TICKET}]</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer ">
                    <div class="btn-actions-pane-right">
                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Save Change</button>
                    </div>
                </div>

            </div>
        </form>
        </div>

    </div>
</div>
@endsection

@section('script')

<script>
    Helper.wysiwygEditor('#content0');
    Helper.wysiwygEditor('#content1');
    globalCRUD.select2("#orders_statuses_id0", '/order-status/select2');
    globalCRUD.select2("#orders_statuses_id1", '/order-status/select2');

    $('#form-email-status-update').submit(function(e){
        console.log('asd');
        globalCRUD.handleSubmit($(this))
            .updateTo(function(formData){
                return '/email-status-order/' + formData.orders_statuses_id;
            })
            .redirectTo(function(resp){
                return '/admin/email-status-order/show'
            })
        e.preventDefault();
    })

</script>


@endsection
