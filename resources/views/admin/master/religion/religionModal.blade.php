<form id="form-religion">
    <div class="modal fade" id="modal-religion" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">ADD RELIGION</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Nama</label>
                    <div class="form-group">
                        <input name="name" placeholder="Nama" id="name" type="text" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm waves-effect btn-clus" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-sm btn-oke waves-effect"><i class="fa fa-plus"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
</form>