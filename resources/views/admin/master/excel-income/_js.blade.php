<script>
    var _uniq_counter_description = 1;

    Helper.onlyNumberInput('.number_only');

    var ClassApp = {
        hitungTotal: function() {
            Helper.unMask('input[name="in_nominal[]"]');
            Helper.unMask('input[name="refund[]"]');
            var grand_total = 0;
            var total_in_nominal = 0;
            var total_refund = 0;
            $('input[name="in_nominal[]"]').each(function() {
                var uniq = $(this).attr('data-uniq');
                var in_nominal = $('.in_nominal_input-' + uniq).val() == '' ? 0 : parseInt($('.in_nominal_input-' + uniq).val());
                var refund = $('.refund_input-' + uniq).val() == '' ? 0 : parseInt($('.refund_input-' + uniq).val());

                total_in_nominal += in_nominal;
                total_refund += refund;

            });
            $('#total_in_nominal').html('Rp. '+Helper.toCurrency(total_in_nominal));
            $('#total_refund').html('Rp. '+Helper.toCurrency(total_refund));
            grand_total = total_in_nominal - total_refund;
            $('#grand_total').html('Rp. '+Helper.toCurrency(grand_total));

        },

        templateNewInputDescription: function(xx) {
            $('.dynamic_description').append((`
                <div class="input-group col-md-12 mb-2" id="my_description_row-${xx}">
                    <input type="hidden" value="0" name="description_id[]">
                    <div class="input-group-append col-md-12">
                        <textarea name="description[]" placeholder="" type="text" class="form-control description-uniq-${xx}" data-uniq="${xx}" required="required" ></textarea>
                        <button type="button" data-uniq="${xx}" class="btn-transition btn btn-sm btn-danger btn_remove_description" data-description_data_id="0"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            `));

        },
        detailHide: function(uniq, status = 'hide') {
            if (status == 'hide') {
                $('.detail-body-uniq-' + uniq).hide();
            }

            if (status == 'show') {
                $('.detail-body-uniq-' + uniq).show();
            }
        },
        detailDelete: function(uniq, detail_id = null) {
            Helper.confirm(function() {
                if (detail_id) {
                    ClassApp
                        .destroyDetail(detail_id, function(resp) {
                            $('.detail-uniq-' + uniq).remove();
                        })
                } else {
                    $('.detail-uniq-' + uniq).remove();
                }
            });
        },
        deleteDescription: function($el) {
            var uniq = $el.attr("data-uniq");
            var details_id = parseInt($el.attr("data-description_data_id"));

            if (details_id != 0) {
                Helper.confirm(function() {
                    Axios.delete('/excel-income/detail_description/' + details_id)
                        .then(function(response) {
                            Helper.successNotif('Success Delete');
                            $('#my_description_row-' + uniq + '').remove();
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
                return false;
            } else {
                $('#my_description_row-' + uniq + '').remove();
            }
        },

        saveData: function(el,e) {
            Helper.unMask('.currency');
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);
            $.ajax({
                url:Helper.apiUrl('/excel-income'),
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        iziToast.success({
                            title: 'OK',
                            position: 'topRight',
                            message: 'Excel Income Has Been Saved',
                        });
                        window.location.href = Helper.redirectUrl('/admin/excel-income/show');
                    }
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });

            e.preventDefault();
        },
        saveDetail: function(el, e, id) {
            Helper.unMask('.currency');
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);
            $.ajax({
                url:Helper.apiUrl('/excel-income/add_detail/'+id+''),
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        iziToast.success({
                            title: 'OK',
                            position: 'topRight',
                            message: 'Excel Income Has Been Saved',
                        });
                        window.location.href = Helper.redirectUrl('/admin/excel-income/show');
                    }
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });

            e.preventDefault();
        },

        updateDetail: function(el, e, id) {
            Helper.unMask('.currency');
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);

            $.ajax({
                url:Helper.apiUrl('/excel-income/update_detail/'+id+''),
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        iziToast.success({
                            title: 'OK',
                            position: 'topRight',
                            message: 'Excel Income Detail Has Been Saved',
                        });
                        window.location.href = Helper.redirectUrl('/admin/excel-income/show');
                    }
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                            iziToast.error({
                                title: 'Error',
                                position: 'topRight',
                                message:  pesan[0]
                            });

                        })
                    }

                },
            });

            e.preventDefault();
        },
        updateData: function(el, e, id) {
            Helper.unMask('.currency');
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);

            $.ajax({
                url:Helper.apiUrl('/excel-income/'+id+''),
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        iziToast.success({
                            title: 'OK',
                            position: 'topRight',
                            message: 'Excel Income Has Been Saved',
                        });
                        window.location.href = Helper.redirectUrl('/admin/excel-income/show');
                    }
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                            iziToast.error({
                                title: 'Error',
                                position: 'topRight',
                                message:  pesan[0]
                            });

                        })
                    }

                },
            });

            e.preventDefault();
        }

    }

    var data = [
        {id: '1', text: 'January', title: 'January'},
        {id: '2', text: 'February', title: 'February'},
        {id: '3', text: 'March', title: 'March'},
        {id: '4', text: 'April', title: 'April'},
        {id: '5', text: 'May', title: 'May'},
        {id: '6', text: 'June', title: 'June'},
        {id: '7', text: 'July', title: 'July'},
        {id: '8', text: 'August', title: 'August'},
        {id: '9', text: 'September', title: 'September'},
        {id: '10', text: 'October', title: 'October'},
        {id: '11', text: 'November', title: 'November'},
        {id: '12', text: 'December', title: 'December'},
    ];

    $("#month").select2({
        data: data,
        escapeMarkup: function(markup) {
            return markup;
        }
    })



</script>
