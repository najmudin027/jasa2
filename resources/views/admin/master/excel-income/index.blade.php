@extends('admin.home')
@section('content')
<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }


</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize" style="margin-left:150px;">
                <form id="form-search">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Fr</span>
                                </div>
                                <input type="text" name="date_from" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">To</span>
                                </div>
                                <input type="text" name="date_to" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div role="group" class="mb-2 btn-group-sm btn-group btn-group-toggle">
                                <button type="submit" class="mb-1 mr-1 btn btn-sm btn-primary btn-gradient-alternate"><i class="fa fa-search"></i> Filter </button>
                                {{-- <button type="button" class="mb-1 mr-1 btn btn-sm btn-success btn-gradient-alternate" id="export_data"><i class="fa fa-download"></i> Export Data </a></button> --}}
                                <a href="{{ url('admin/excel-income/create') }}" class="mb-1 mr-1 btn btn-sm btn-primary btn-gradient-alternate"><i class="fa fa-plus"></i> Add </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>


        </div>

        <div class="card-body">
        <table style="width: 100%;" id="table-excel-income" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Code</th>
                        <th>Month</th>
                        <th>Total In</th>
                        <th>Total Refund</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>
     $(function() {
        $('.date_format').datetimepicker({
            timepicker:false,
            format:'d-m-Y',
            allowBlank: false,
        });

        var d = new Date();
        var startDate = d.setMonth(d.getMonth() - 1);
        var e = new Date();

        $('input[name="date_from"]').val(moment(startDate).format('DD-MM-YYYY'));
        $('input[name="date_to"]').val(moment(e.setDate(e.getDate())).format('DD-MM-YYYY'));

    });

    var tableExcelIncome = globalCRUD.datatables({
        url: '/excel-income/datatables',
        selector: '#table-excel-income',
        columnsField: [{
                    "className": 'details-control',
                    "defaultContent": ''
                },
                 'code',
                 {
                    data: "desc",
                    name: "desc",
                    render: function(row,type,full) { // changed by month-year
                        return full.desc +' '+full.month+' - '+full.year;
                    }
                },
                {
                    data: "total_in_nominal",
                    name: "total_in_nominal",
                    render: function(row,type,full) {
                        return 'Rp.'+Helper.toCurrency(row);
                    }
                },
                {
                    data: "total_refund",
                    name: "total_refund",
                    render: function(row,type,full) {
                        return 'Rp.'+Helper.toCurrency(row);
                    }
                },
                ],
        actionLink: {
            add_detail: function(row) {
                return "/admin/excel-income/add_detail/" + row.id;
            },
            update: function(row) {
                return "/admin/excel-income/edit/" + row.id;
            },
            delete: function(row) {
                return "/excel-income/" + row.id;
            },
            detail: function(row) {
                return "/admin/excel-income/detail/" + row.id;
            },
            export_detail_excel: function(row) {
                return "/admin/excel-income/export_detail_excel/" + row.id;
            }
        }
    });

    function checkValidation() {
        var isValid = true;
        var date_from = $('input[name="date_from"]').val();
        var date_to = $('input[name="date_to"]').val();
        if(date_from == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date Fr !');
            return false;
        } else if (date_to == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date To !');
            return false;
        }
        return isValid;
    }

    $("#export_data").click(function(e) {
        var date_from = $('input[name="date_from"]').val();
        var date_to = $('input[name="date_to"]').val();
        var url = '/admin/excel-income/export';
        if(checkValidation()) {
            url = '/admin/excel-income/export?date_from='+date_from+'&date_to='+date_to+'';
            Helper.redirectTo(url);
        }
        // console.log(url);

        e.preventDefault();
    });

    $("#form-search").submit(function(e) {
        e.preventDefault();
        if(checkValidation()) {
            var input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/excel-income/datatables/search' + playload);
            tableExcelIncome.table.reloadTable(url);
        }

    })

    function templatex(d) {
        console.log(d)
        template = '';
        template += '<div class="col-md-12">';
        template += '<h5>Excel Income Detail</h5>';
        template += '<table class="table" id="example">';
        template += '<tr>';
        template += '<td>No</td>';
        template += '<td>Source</td>';
        template += '<td>Date Transaction</td>';
        template += '<td>Description</td>';
        template += '<td>Date Transfer</td>';
        template += '<td>In Nominal</td>';
        template += '<td>Refund</td>';
        template += '<td>Branch</td>';
        template += '<td>Action</td>';
        template += '</tr>';

        _.each(d.excel_income_detail, function(i, key) {
            var description = [];
            _.each(i.details_desc, function(val, key) {
                description.push(val.description);
            });
            description = _.join(description, ', ');

            console.log(description);
            template += '<tr>';
            template += '<td>' + (key + 1) + '</td>';
            template += '<td>' + (i.source != null ? i.source : '-') + '</td>';
            template += '<td>' + (i.date_transaction != null ? moment(i.date_transaction).format("DD-MM-YYYY") : '-') + '</td>';
            template += '<td>' + description + '</td>';
            template += '<td>' + (i.date_transfer != null ? moment(i.date_transfer).format("DD-MM-YYYY") : '-') + '</td>';
            template += '<td>' + Helper.toCurrency(i.in_nominal) + '</td>';
            template += '<td>' + Helper.toCurrency(i.refund) + '</td>';
            template += '<td>' + (i.branch != null ? i.branch : '-') + '</td>';
            template += '<td><a target="_blank" title="Edit Detail" href="'+Helper.url('/admin/excel-income/edit_detail/'+i.id)+'" class="btn-hover-shine btn btn-primary btn-sm btn-success"><i class="fa fa-pencil"></i></a>&nbsp;<a title="Delete Detail" data-id="' + i.id + '" class="btn-hover-shine btn btn-sm btn-danger detail_delete"><i class="fa fa-trash"></i></a></td>';


            template += '</tr>';
        })
        template += '</table>';
        template += '</div>';

        return template;
    }


    var table = $('#table-excel-income').DataTable();
    $('#table-excel-income tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(templatex(row.data())).show();
            $(row.child()).addClass('smalltable');
            tr.addClass('shown');
        }
    });

    // delete detail
    $(document).on('click', ".detail_delete", function() {
        var id = $(this).attr("data-id");
        globalCRUD.delete(Helper.apiUrl('/excel-income/destroy_details_data/'+id))
        tableOrder.table.reloadTable();
    });
    </script>


@endsection
