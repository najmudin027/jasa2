@extends('admin.home')
@section('content')

<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            {{ $title }}
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-orders-data-excel-logs" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Service Order No.</th>
                        <th>Type Modified</th>
                        <th>By User (Name)</th>
                        <th>Created At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
<div class="col-md-12" style="margin-top: 10px;">

@endsection

@section('script')
<script>
    globalCRUD.datatables({
        url: '/orders-data-excel/datatables_logs/0/3',
        selector: '#table-orders-data-excel-logs',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "service_order_no",
            name: "service_order_no",
        },
        {
            data: "type",
            name: "type",
            render: function(row,data,full) {
                // if(row == 1) {
                //     return 'Created';
                // } else if(row == 2) {
                //     return 'Updated'
                // } else {
                    return '<div class="mb-2 mr-2 badge badge-pill badge-danger">Deleted</div>';
                // }
            }
        },
        {
            data: "user.name",
            name: "user.name",
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm");
            }
        }
        ]
    })

</script>
@endsection
