@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/excel-income/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;">
    <input type="hidden" name="id" value="{{ $getData->id }}">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Year</label>
                    <div class="col-sm-12">
                        <input name="year" type="text" value="{{ $getData->year }}" class="form-control" required readonly>
                    </div>
                </div>
                {{-- <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Code</label>
                    <div class="col-sm-12">
                        <input name="code" placeholder="Title" type="text" value="{{ $getData->code }}" class="form-control" readonly>
                    </div>
                </div> --}}
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Month</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="month" name="month" style="width:100%" disabled>
                            <option value="{{ $getData->month }}" selected>{{ $getData->month }}</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Description</label>
                    <div class="col-sm-12">
                        <textarea name="desc" class="form-control desc" maxlength="300" rows="5" readonly>{{ $getData->desc }}</textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                Details
                {{-- <div class="btn-actions-pane-right text-right">
                    <button type="button" class="btn btn-success btn_add_details"><i class="fa fa-plus"></i></button>
                </div> --}}
            </div>

            <div class="card-body">
                <div class="row dynamic_details">
                    <input type="hidden" value="{{ count($detailsData) }}" id="count_details_data" />
                    @foreach($detailsData as $key => $details)
                    <?php $uniq = $key+1; ?>
                        <div class="col-md-12 detail-uniq-{{ $uniq }} mb-1" id="details_row_{{ $uniq }}"  style="amrgin-bottom:10px;">
                            <div class="card">
                                <div class="card-header header-border">
                                    # <?php echo ($key+1) ?>
                                    <div class="btn-actions-pane-right text-right">
                                        @if($key != 0)
                                            <div role="group" class="btn-group-sm btn-group">
                                                <button data-uniq="{{ $uniq }}" data-hide="1" class="btn btn-sm btn-white btn-warning btn-hide-detail btn-hide-detail-uniq-{{ $uniq }}" type="button">Hide</button>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-body detail-body-uniq-{{ $uniq }}">
                                    <input type="hidden" value="{{ $details->id }}" name="details_id[]" />
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">Date Transaction</label>
                                        <div class="col-sm-12">
                                            <input name="date_transaction[]" type="text" class="form-control date_transaction_input-{{ $uniq }} date_format" data-uniq="{{ $uniq }}" value="{{ ($details->date_transaction != null ? $details->date_transaction->format('d-m-Y') : "") }}" required readonly>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">Service Order No.</label>
                                        <div class="col-sm-12">
                                            <input name="source[]" placeholder="" type="text" class="form-control source_input-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $details->source }}" required readonly>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">Defect Type Description</label>
                                        <div class="dynamic_description col-md-12">

                                            @foreach($details->details_desc as $key => $desc)
                                                <input type="hidden" value="{{ count($details->details_desc) }}" id="count_details_desc" />
                                                <?php $uniq = uniqid(); ?>
                                                <div class="input-group col-md-12 mb-2" id="my_description_row-{{ $uniq }}">
                                                    <div class="input-group-append col-md-12">
                                                        <input type="hidden" value="{{ $desc->id }}" name="description_id[]"/>
                                                        <textarea name="description[]" placeholder="" type="text" class="form-control description-uniq-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $desc->description }}" required="required" disabled></textarea>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">Date Transfer</label>
                                        <div class="col-sm-12">
                                            <input name="date_transfer[]" type="text" class="form-control date_transfer_input-{{ $uniq }} date_format" data-uniq="{{ $uniq }}" value="{{ ($details->date_transfer != null ? $details->date_transfer->format('d-m-Y') : "") }}" required readonly>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">In</label>
                                        <div class="col-sm-12">
                                            <input name="in_nominal[]" placeholder="In" type="text" class="form-control currency in_nominal_input-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $details->in_nominal }}" required readonly>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">Refund</label>
                                        <div class="col-sm-12">
                                            <input name="refund[]" placeholder="Refund" type="text" class="form-control currency refund_input-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $details->refund }}" required readonly>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">ASC Name</label>
                                        <div class="col-sm-12">
                                            <input name="branch[]" placeholder="Astech Service Center Name" type="text" class="form-control branch_input-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $details->branch }}" required readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Ending Total
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <label for="exampleEmail" class="col-sm-12 col-form-label">Total In : <span id="total_in_nominal"></span></label>
                   </div>
                    <div class="col-md-4 text-center">
                        <label for="exampleEmail" class="col-sm-12 col-form-label">Total Refund : <span id="total_refund"></span></label>

                    </div>
                    <div class="col-md-4 text-center">
                        <label for="exampleEmail" class="col-sm-12 col-form-label">Grand Total : <span id="grand_total"></span></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
@include('admin.master.excel-income._js')
<script>
    Helper.hideSidebar();
    for(i=1; i <= parseInt($('#count_details_data').val()); i++) {
        Helper.wysiwygEditor('.description_input-'+i+'');
    }
    Helper.currency('.currency');
    ClassApp.hitungTotal();
    var id = $('input[name="id"]').val();
    $(function() {
        $('.date_format').datetimepicker({
            timepicker:false,
            format:'d-m-Y',
            mask:true
        });

    })

    // hide vendor
    $(document).on('click', '.btn-hide-detail', function() {
        uniq = $(this).attr('data-uniq');
        statusHide = $(this).attr('data-hide');

        if (statusHide == 0) {
            status = 'show';
            $(this).attr('data-hide', 1).text('hide');
        } else {
            status = 'hide';
            $(this).attr('data-hide', 0).text('show');
        }
        ClassApp.detailHide(uniq, status);
    })
</script>

@endsection
