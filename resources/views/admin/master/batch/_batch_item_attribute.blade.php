@foreach($getAvailableAttribute as $attr)
<fieldset class="position-relative form-group">
    <label for="" class="">{{ $attr['attribute']->name }}</label><br>
    @foreach($attr['terms'] as $term)
    <?php $active = $item->varian->productVarianAttributes->where('product_attribute_id', $term->product_attribute_id)->where('product_attribute_term_id', $term->product_attribute_term_id)->first() ? 'active' : ''; ?>
    <button 
        type="button" 
        data-uniq="{{ $uniq }}" 
        data-attribute_id="{{ $term->product_attribute_id }}" 
        data-term_id="{{ $term->product_attribute_term_id }}" 
        class="{{ $active }} btn btn-outline-primary btn-attribute-{{ $uniq }} btn-uniq-id-{{ $term->product_attribute_id }}-{{ $uniq }}" data-combine="">
        {{ $term->term->name }}
    </button>
    @endforeach
</fieldset>
@endforeach