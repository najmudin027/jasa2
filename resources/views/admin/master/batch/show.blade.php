@extends('admin.home')
@section('content')
<input type="hidden" id="input-id" value="{{ $batch->id }}">
<div class="col-md-12">
    <div class="main-card mb-3 card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <input type="hidden" name="id" id="batch_id" value="{{ $batch->id }}">
            <div class="form-row">
                <div class="col-md-6">
                    <div class="position-relative form-group">
                        <label for="" class="">Batch No</label>
                        <input name="batch_no" value="{{ $batch->batch_no }}" id="batch_no" type="text" class="form-control" required readonly>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="position-relative form-group">
                        <label for="" class="">Date</label>
                        <input name="batch_date" value="{{ $batch->batch_date->format('Y-m-d') }}" id="batch_date" type="date" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="position-relative form-group">
                        <label for="" class="">Shipping Cost</label>
                        <input name="shipping_cost" id="shipping_cost" value="{{ $batch->shipping_cost }}" placeholder="Shipping Cost" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="position-relative form-group">
                        <label for="" class="">Extra Cost</label>
                        <input name="extra_cost" id="extra_cost" value="{{ $batch->extra_cost }}" placeholder="Extra Cost" type="text" class="form-control" required>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="main-card mb-3 card header-border">
        <div class="card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                History
            </div>
        </div>
        <div class="card-body">
            <table id="table-batch" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Status Shipment</th>
                        <th>Date </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="main-card mb-3 card header-border">
        <div class="card-body">
            <table class="table table-user-information">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Product</td>
                        <td>Consigment</td>
                        <td>Status</td>
                        <td>Quantity</td>
                        <td>Value</td>
                    </tr>
                </thead>
                <tbody class="item-batch-area">
                    @foreach($batch->items as $no => $item)
                    <tr>
                        <td> {{ $no + 1 }}</td>
                        <td> {{ $item->item_name }}</td>
                        <td>
                            {{ $item->is_consigment == 1 ? 'Yes' : '' }}
                            {{ $item->is_consigment == 0 ? 'No' : '' }}
                        </td>
                        <td>{{ $item->status->name }}</td>
                        <td>{{ $item->quantity_format }}</td>
                        <td>{{ $item->value_format }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    var tbl_history = globalCRUD.datatables({
        url: '/batch/shipment-histories/datatables/' + $('#input-id').val(),
        selector: '#table-batch',
        dom: '',
        columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                width: '2%'

            },
            {
                data: 'batch_status_shipment.name',
                name: 'batch_status_shipment.name',
                sortable: false
            },
            {
                data: 'datetime',
                name: 'datetime',
                sortable: false,
                render: function(data, type, full) {
                    return moment(full.datetime).format("DD MMMM YYYY HH:mm:ss");
                }
            }
        ],
    });
</script>
@endsection