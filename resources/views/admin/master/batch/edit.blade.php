@extends('admin.home')
@section('content')
<form id="form-batch" style="display: contents">
    <div class="col-md-12">
        <div class="main-card mb-3 card">
            <div class="card-header-tab card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    {{ $title }}
                </div>
            </div>
            <div class="card-body">
                <input type="hidden" name="id" id="batch_id" value="{{ $batch->id }}">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Batch No</label>
                            <input name="batch_no" value="{{ $batch->batch_no }}" id="batch_no" type="text" class="form-control" required readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Date</label>
                            <input name="batch_date" value="{{ $batch->batch_date->format('Y-m-d') }}" id="batch_date" type="date" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Shipping Cost</label>
                            <input name="shipping_cost" id="shipping_cost" value="{{ $batch->shipping_cost }}" placeholder="Shipping Cost" type="text" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Extra Cost</label>
                            <input name="extra_cost" id="extra_cost" value="{{ $batch->extra_cost }}" placeholder="Extra Cost" type="text" class="form-control" required>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="main-card mb-3 card header-border">
            <div class="card-body">
                <table class="table table-user-information">
                    <thead>
                        <tr>
                            <td>Product</td>
                            <td>Consigment</td>
                            <td>Status</td>
                            <td>Quantity</td>
                            <td>Value</td>
                        </tr>
                    </thead>
                    <tbody class="item-batch-area">
                        @foreach($batch->items as $item)
                        <?php $index = time(); ?>
                        <tr class="row-item-batch-{{ $index }}">
                            <td>
                                <a href="#" data-index="{{ $index }}" class="add-product add-product-{{ $index }}"><i class="fa fa-plus"></i> {{ $item->product_name }}</a>
                            </td>
                            <td>
                                <select name="is_consigment[]" class="form-control" style="width: 100%" required>
                                    <option value="1" {{ $item->is_consigment == 1 ? 'selected' : '' }}>Iya</option>
                                    <option value="0" {{ $item->is_consigment == 0 ? 'selected' : '' }}>Tidak</option>
                                </select>
                            </td>
                            <td>
                                <select name="batch_status_id[]" class="form-control status-select uniq-status-select-{{ $index }}" style="width: 100%" required>
                                    <option value="{{ $item->status->id }}" selected>{{ $item->status->name }}</option>
                                </select>
                            </td>
                            <td>
                                <input name="quantity[]" placeholder="Quantity" type="number" class="form-control" min="1" value="{{ $item->quantity }}" required />
                            </td>
                            <td>
                                <input name="value[]" placeholder="Value" type="text" class="input-sparator-mask form-control" value="{{ $item->value }}" required />
                                <input type="hidden" name="product_vendor_id[]" value="{{ $item->product_vendor_id }}" class="product_vendor_id-{{ $index }}" />
                                <input type="hidden" name="product_varian_id[]" value="{{ $item->product_varian_id }}" class="product_varian_input product_varian_id-{{ $index }}" />
                                <input type="hidden" name="vendor_id[]" value="{{ $item->vendor_id }}" class="vendor_id-{{ $index }}" />
                                <input type="hidden" name="product_id[]" value="{{ $item->product_id }}" class="p-id product_id-{{ $index }}" />
                                <input type="hidden" name="batch_item_id[]" value="{{ $item->id }}" class="p-id product_id-{{ $index }}" />
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <button type="button" class="add-item btn btn-success btn-sm">
                    <i class="fa fa-plus"></i> Add Item
                </button>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary btn-sm" type="submit">
                    <i class="fa fa-plus"></i> Save
                </button>
            </div>
        </div>
    </div>

</form>


<div class="modal fade" id="modal-product" data-backdrop="false" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select Product</h4>
            </div>
            <div class="modal-body scroll-area-sm" style="max-height: 400px;">
                <input type="hidden" value="" id="item_index">
                <table id="table-product" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>No</th>
                            <th>Product Name</th>
                            <th>Stock</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
@endsection

@section('script')
@include('admin.master.batch.batch_js')
<script>
    var tableProduk = $('#table-product').DataTable({
        processing: true,
        serverSide: true,
        select: false,
        dom: 'Bflrtip',
        responsive: true,
        order: [2, 'asc'],
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/product/datatables'),
            "type": "get",
            "data": function(d) {
                return $.extend({}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },
        columns: [{
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                sortable: false,
                width: "10%"
            },
            {
                data: "name",
                name: "name",
            },
            {
                data: "id",
                name: "id",
            },
        ],
    });

    function formatx(d) {
        template = '';
        template += '<table class="table">';
        template += '<tr>';
        template += '<th>No</th>';
        template += '<th>Vendor</th>';
        template += '<th>Vendor Code</th>';
        template += '<th>Part Code</th>';
        template += '<th>Brand Code</th>';
        template += '<th>Product Code</th>';
        template += '</tr>';
        _.each(d.product_vendors, function(i, key) {
            if (i.product_varians.length == 0) {
                template += '<tr>';
                template += '<td>' + (key + 1) + '</td>';
                template += '<td>' + i.vendor.name + '</td>';
                template += '<td>' + i.vendor_code + '</td>';
                template += '<td>' + i.part_code + '</td>';
                template += '<td>' + i.brand_code + '</td>';
                template += '<td>' + i.product_code + '</td>';
                template += '<td><button data-product_vendor_id="' + i.id + '" class="select-vendor btn btn-sm btn-danger"><i class="fa fa-plus"></i> Select</button></td>';
                template += '</tr>';
            } else {
                _.each(i.product_varians, function(productVarian) {
                    attribute = '';
                    _.each(productVarian.product_varian_attributes, function(pva) {
                        attribute += pva.attribute.name + ' : ' + pva.term.name; // + ' ' + pva.term.color_code;
                    })
                    template += '<tr>';
                    template += '<td>' + (key + 1) + '</td>';
                    template += '<td>' + i.vendor.name + ' | ' + attribute + '</td>';
                    template += '<td>' + i.vendor_code + '</td>';
                    template += '<td>' + i.part_code + '</td>';
                    template += '<td>' + i.brand_code + '</td>';
                    template += '<td>' + i.product_code + '</td>';
                    template += '<td><button data-varian_id="' + productVarian.id + '" class="select-varian btn btn-sm btn-danger"><i class="fa fa-plus"></i> Select</button></td>';
                    template += '</tr>';
                })
            }
        })
        template += '</table>';

        return template;
    }

    $('#table-product tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = tableProduk.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(formatx(row.data())).show();
            tr.addClass('shown');
        }
    });
</script>
<script>
    var indexBatchItem = 0;
    $('.add-item').click(function() {
        $('.item-batch-area').append(templateItemBatch(indexBatchItem));
        BatchObj.loadSelect2StatusBatchItem(indexBatchItem);
        indexBatchItem++;

        Helper.thousandSeparatorMaskInput('.input-sparator-mask')
    })

    $(document).on('click', '.add-product', function() {
        $('#item_index').val($(this).attr('data-index'));
        $('#modal-product').modal('show')
    })

    $(document).on('click', '.select-varian', function(e) {
        product_varian_id = $(this).attr('data-varian_id');

        var valid_duplicate = true;
        $('.product_varian_input').each(function() {
            console.log(product_varian_id, $(this).val(), (product_varian_id == $(this).val()))
            if (product_varian_id == $(this).val()) {
                valid_duplicate = false;
                console.log('valid_duplicate', valid_duplicate)
                return;
            }
        })
        if (valid_duplicate == false) {
            Helper.errorNotif('data sudah dipilih');
            return;
        }

        index = $('#item_index').val();
        Axios.get('/product_search/find_varian/' + product_varian_id)
            .then(function(response) {
                $('#modal-product').modal('hide');
                product_name = response.data.data.product_vendor.product.name + ', ';
                product_name += response.data.data.product_vendor.vendor.name + ' ';
                product_name += response.data.data.attribute_name + ' ';
                $('.add-product-' + index).text(product_name);
                $('.vendor_id-' + index).val(response.data.data.product_vendor.vendor.id);
                $('.product_id-' + index).val(response.data.data.product_vendor.product.id);
                $('.product_vendor_id-' + index).val(response.data.data.product_vendor.id);
                $('.product_varian_id-' + index).val(response.data.data.id);
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })

    function templateItemBatch(index) {
        temp = (`
        <tr class="row-item-batch-${index}">
            <td>
                <a href="#" data-index="${index}" class="add-product add-product-${index}"><i class="fa fa-plus"></i> Add Product</a>
            </td>
            <td>
                <select name="is_consigment[]" class="form-control" style="width: 100%" required>
                    <option value="1" selected>Yes</option>
                    <option value="0">No</option>
                </select>
            </td>
            <td>
                <select name="batch_status_id[]" class="form-control status-select uniq-status-select-${index}" style="width: 100%" required>
                    <option value="" selected>Select Status</option>
                </select>
            </td>
            <td>
                <input name="quantity[]" placeholder="Quantity" type="number" class="form-control" min="1" required/>
            </td>
            <td>
                <input name="value[]" placeholder="Value" type="text" class="input-sparator-mask form-control" required/>
                <input type="hidden" name="product_vendor_id[]" value="0" class="product_vendor_id-${index}" />
                <input type="hidden" name="product_varian_id[]" value="0" class="product_varian_input product_varian_id-${index}" />
                <input type="hidden" name="vendor_id[]" value="0" class="vendor_id-${index}" />
                <input type="hidden" name="product_id[]" value="0" class="p-id product_id-${index}" />
                <input type="hidden" name="batch_item_id[]" value="0" class="p-id product_id-${index}" />
            </td>
            <td>
                <button class="btn btn-sm btn-danger hapus-item-batch" data-index="${index}"><i class="fa fa-close"></i></button>
            </td>
        </tr>
        `);

        return temp;
    }

    // submit event
    $('#form-batch')
        .submit(function(e) {
            Helper.unMask('.value');
            Helper.unMask('#shipping_cost')
            Helper.unMask('#extra_cost')

            var data = Helper.serializeForm($(this));
            console.log(data);
            BatchObj.update(data);
            e.preventDefault()
        })
</script>
@endsection