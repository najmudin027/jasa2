@extends('admin.home')
@section('content')
<style>
    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    .btn {
        border: 2px solid gray;
        color: gray;
        background-color: white;
        border-radius: 8px;
        font-size: 12px;
        font-weight: bold;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }

    .validation {
        color: red;
    }
</style>

<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-6">
                    <div class="position-relative form-group">
                        <div class="col-md-4">
                            <label for="" class="">Status Shipment</label>
                            <select name="batch_status_shipment_id" id="batch_status_shipment_id" class="form-control" {{ $batch->is_confirmed == 1 ? 'disabled' : ''}}>
                                <option value="{{ isset($last_status->batch_status_shipment->id) ? $last_status->batch_status_shipment->id : '' }}" selected>{{ isset($last_status->batch_status_shipment->name) ?$last_status->batch_status_shipment->name : '-Select-' }}</option>
                            </select>
                        </div>
                        <div class="card-body">

                            <table id="table-batch" class="display table table-hover table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Status Shipment</th>
                                        <th>Date </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="batch-info"><div class="no-gutters row">
                        <div class="col-md-12">
                            <div class="pt-0 pb-0 card-body">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <div class="widget-content p-0">
                                            <div class="widget-content-outer">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">Date</div>
                                                        <div class="widget-subheading">{{ $batch->batch_date->format('d-m-Y') }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="widget-content p-0">
                                            <div class="widget-content-outer">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">Shipping cost</div>
                                                        <div class="widget-subheading">Total Shipping Cost</div>
                                                    </div>
                                                    <div class="widget-content-right">
                                                        <div class="widget-numbers text-danger">{{ \App\Helpers\formatCurrency($batch->shipping_cost, 0, '.', ',') }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="widget-content p-0">
                                            <div class="widget-content-outer">
                                                <div class="widget-content-wrapper">
                                                    <div class="widget-content-left">
                                                        <div class="widget-heading">Extra Cost</div>
                                                        <div class="widget-subheading">Total Extra Cost</div>
                                                    </div>
                                                    <div class="widget-content-right">
                                                        <div class="widget-numbers text-warning">{{ \App\Helpers\formatCurrency($batch->extra_cost, 0, '.', ',') }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form id="form-edit-order" enctype="multipart/form-data" style="display: contents;">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="header-border card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Batch Code : {{ $batch->batch_no }}
                </div>
            </div>
            <div class="card-body">
                <label for="" class=""><b>Shipped On : {{ date('d-m-Y', strtotime($batch->batch_date)) }}</b></label>

                <input type="hidden" name="batch_id" id="batch_id" value="{{ \Request::segment(4) }}">
                <table id="table-batch-item" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Product Name</th>
                            <th>Value</th>
                            <th>Qty Initial</th>
                            <th>Qty Return</th>
                            <th>Reasoning</th>
                            <th>Proof</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-top: 10px">
        <div class="card">
            <div class="card-footer">
                <div class="card-body">
                    @if($batch->is_confirmed == 1 && $is_confirmed_all == 0)
                    <button class="btn btn-primary btn-sm" id="edit-order">
                        SAVE
                    </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
{{-- @include('admin.master.batch.batch_js') --}}
<script>

    var tbl = "";
    globalCRUD.select2("#batch_status_shipment_id", '/batch-status-shipment/select2')
    // table
    var batch_id = $('#batch_id').val();
    var tbl_history = globalCRUD.datatables({
        url: '/batch/shipment-histories/datatables/' + batch_id + '',
        selector: '#table-batch',
        dom: '',
        columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                width: '2%'

            },
            {
                data: 'batch_status_shipment.name',
                name: 'batch_status_shipment.name',
                sortable: false
            },
            {
                data: 'datetime',
                name: 'datetime',
                sortable: false,
                render: function(data, type, full) {
                    return moment(full.datetime).format("DD MMMM YYYY HH:mm:ss");
                }
            }
        ],

    });

    tbl = $('#table-batch-item').DataTable({
        ajax: {
            url: Helper.apiUrl('/batch/batch_item_edit_order/datatables/' + batch_id + ''),
            type: "get",
            error: function() {},
            complete: function() {},
        },
        selector: '#table-batch-item',
        dom: 't',
        columns: [

            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',

            },
            {
                data: 'product_name',
                name: 'product_name',
                sortable: false
            },
            {
                data: 'value',
                name: 'value',
                class: 'text-center',
                sortable: false,
                render: function(data, type, row) {
                    var price = row.value;
                    return Helper.toCurrency(price);
                }
            },
            {
                data: 'quantity',
                name: 'quantity',
                class: 'text-center',
                sortable: false
            },

            {
                render: function(data, type, row) {
                    var max = parseInt($('#max-' + row.id + '').val());
                    $('.quantity_return-' + row.id + '').keyup(function(e) {
                        console.log($(this).val() > max)
                        if ($(this).val() > max) {
                            e.preventDefault();
                            $(this).val(max);
                        } else {
                            $(this).val();
                        }
                    });
                    return '<input type="hidden" name="quantity[]" id="max-' + row.id + '" value=' + row.quantity + '>' +
                        '<input type="hidden" name="batch_item_id[]" value=' + row.id + '>' + '<input type="number" class="form-control quantity_return-' + row.id + '" name="quantity_return[]" style="width: 75px;" ' + (row.is_accepted == 0 || row.is_confirmed == 1 ? "readonly" : "") + ' value="' + row.quantity_return + '">';
                }
            },
            {
                render: function(data, type, row) {
                    return '<textarea type="reason" class="form-control reason-' + row.id + '" name="reason[]" ' + (row.is_accepted == 0 || row.is_confirmed == 1 ? "readonly" : "") + ' >' + (row.reason != null ? row.reason : '') + '';
                }
            },
            {
                render: function(data, type, row) {

                    $('#filename-' + row.id + '').change(function() {
                        if (this.files.length > 4) {
                            $('#notif-' + row.id + '').css('display', 'block');
                            $('#edit-order').prop('disabled', true);
                        } else {
                            $('#notif-' + row.id + '').css('display', 'none');
                            $('#edit-order').prop('disabled', false);

                        }
                        var size = 6; //in MB
                        var maxSize = 1024 * (1024 * size);
                        if (this.files[0].size > maxSize) {
                            Helper.errorNotif('Error. File size may not exceed ' + size + ' MB');
                        }
                        var ext = this.value.match(/\.(.+)$/)[1];
                        switch (ext) {
                            case 'mkv':
                            case 'mp4':
                            case 'jpg':
                            case 'jpeg':
                            case 'png':
                                break;
                            default:
                            Helper.errorNotif('Extension must .jpg, .jpeg, or png & video .mp4 .mkv maks 6MB!');
                        }
                    });
                    if (row.reason_image == null && row.is_accepted == 1 && row.is_confirmed == 0) {
                        return '<div class="upload-btn-wrapper">' +
                            '<button class="btn">Upload</button>' +
                            '<input type="file" name="filename-' + row.id + '[]" id="filename-' + row.id + '" class="putImages" multiple="" />' +
                            '</div>' +
                            '<div class="validation" id="notif-' + row.id + '" style="display:none;"> Upload Max 4 Files allowed </div>';
                    } else if (row.reason_image == null && row.is_accepted == 0) {
                        return '<div class="upload-btn-wrapper">' +
                            '<button class="btn" disabled>Upload</button>' +
                            '</div>';
                    } else if (row.reason_image == null && row.is_confirmed == 1) {
                        return '-';
                    } else {
                        return row.reason_image;
                    }
                }
            },
        ]
    });



    $('#form-edit-order').submit(function(e) {
        Helper.loadingStart();
        e.preventDefault();
        var data = new FormData(this);
        data.append('batch_id', batch_id);
        console.log(data);
        $.ajax({
            url: Helper.apiUrl('/batch/update_item_order'),
            type: 'post',
            data: data,
            processData: false,
            contentType: false,
            success: function(response) {
                Helper.loadingStop()
                $('#edit-order').attr('disabled', true)
                tbl.ajax.reload()
                Helper.successNotif('Update shipment batch success updated !')
                window.location.href = Helper.redirectUrl('/admin/batch/shipment/'+batch_id+'');
            }


        });

    });

    $('#batch_status_shipment_id').change(function() {
        Helper.confirm(() => {
            $.ajax({
                url: Helper.apiUrl('/batch/update_shipment_history'),
                type: 'post',
                data: {
                    status_shipment: $(this).val(),
                    batch_id: batch_id
                },
                success: function(response) {
                    Helper.successNotif('History shipment success updated !')
                    tbl_history.table.reloadTable()
                    tbl.ajax.reload()
                    window.location.href = Helper.redirectUrl('/admin/batch/shipment/' + batch_id + '');
                }
            });
        });
    })
</script>
@endsection
