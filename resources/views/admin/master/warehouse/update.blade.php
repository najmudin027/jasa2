@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header header-bg">Update Warehouse</div>
        <div class="card-body">
            <h5 class="card-title" style="color:white">Update Warehouse</h5>
            <form id="form-warehouses-update">
                <input type="hidden" name="id" value="{{ $editWarehouse->id }}">

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Warehouses Name</label>
                    <div class="col-sm-10">
                        <input name="name" value="{{ $editWarehouse->name }}" id="examplename" placeholder="Warehouses" type="text" class="form-control">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                        <input name="address" value="{{ $editWarehouse->address }}" placeholder="Warehouses" type="text" class="form-control">
                    </div>
                </div>

                <div class="position-relative row form-check">
                    <div class="col-sm-10 offset-sm-2">
                        <button class="btn btn-success" id="update"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')

<script>
    $('#form-warehouses-update').submit(function(e) {
        globalCRUD.handleSubmit($(this))
            .updateTo(function(formData) {
                return '/warehouses/' + formData.id;
            })
            .redirectTo(function(resp) {
                return '/admin/warehouse'
            })
    })
</script>
{{-- @include('admin.script.master._warehouseScript') --}}
@endsection