@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card header-border">
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="">Months</label>
                            <select class="form-control" name="month" id="month">
                                <option value="{{date("n")}}" selected>{{ date("F") }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="">Years</label>
                            <select class="form-control" name="year" id="year">
                                @for($year=intval(date("Y")); $year > (intval(date("Y")) - 2); $year--)
                                    <option value="{{$year}}" <?php if($year == date("Y")) { echo 'selected'; } ?>>{{$year}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="">Margin Sell Items Rate Auto (%)</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control number_only" name="margin_rate_auto" id="margin_rate_auto" max="100" min="0" value="{{ $margin_rate_auto }}">
                                <div class="input-group-append">
                                    <button class="btn btn-danger" type="button" id="update_margin_rate_auto">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm mt-2" type="submit">Search</button>
            </form>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Report HPP Items
            </div>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <table id="table" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Items</th>
                                    <th>Hpp Average</th>
                                    <th>Setting Margin (Auto) %</th>
                                    <th>Setting Margin (Manual) %</th>
                                    <th>Suggest To Sell</th>
                                    <th>Suggest To Sell (Manual)</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>

    Helper.selectMonth();
    $("#margin_rate_auto").keyup(function() {
        var max = parseInt($(this).attr('max'));
        var min = parseInt($(this).attr('min'));
        if ($(this).val() > max) {
            $(this).val(max);
        } else if ($(this).val() < min) {
            $(this).val(min);
        }
    });

    $(".margin_rate_manual").keyup(function() {
        var max = parseInt($(this).attr('max'));
        var min = parseInt($(this).attr('min'));
        if ($(this).val() > max) {
            $(this).val(max);
        } else if ($(this).val() < min) {
            $(this).val(min);
        }
    });

    var tbl = globalCRUD.datatables({
        url: '/report-hpp-items/datatables',
        selector: '#table',
        actionLink: "",
        columnsField: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',

            },
            {
                data: 'part_description',
                name: 'part_description',
            },
            {
                data: 'hpp_average_month',
                name: 'hpp_average_month',
                render: function(data, type, row) {
                    return 'Rp. '+Helper.toCurrency(data);
                }
            },
            {
                data: 'margin_rate_auto',
                name: 'margin_rate_auto',
                render: function (data,type,row) {
                    var nilai = data;
                    if(row.hpp_item != null) {
                        return '<span class="text-secondary">'+nilai+'</span>';
                    } else {
                        return nilai;
                    }
                }

            },
            {
                render: function(data, type, row) { //margin_rate_manual
                    var nilai = 0;
                    if(row.hpp_item != null) {
                        if(row.hpp_item.margin_rate_manual != null && row.hpp_item.margin_rate_manual != 0) {
                            nilai = row.hpp_item.margin_rate_manual
                        }
                    }
                    Helper.onlyNumberInput('.number_only');
                    return `<div class='form-row'>
                                <div class='col-md-8'>
                                    <input type='text' class='form-control form-control-sm number_only margin_rate_manual_${row.part_data_stock_id}' name='margin_rate_manual' data-pdes_id="${row.part_data_stock_id}" id='' max='100' min='0' value=${nilai}>
                                </div>
                                <div class='col-md-4'>
                                    <button class='btn btn-primary btn-sm mr-2 update_margin_rate' data-part_description="${row.part_description}" data-pdes_id="${row.part_data_stock_id}" title='Update Margin Rate'>confirm</button>
                                </div>
                            </div>`;

                }
            },
            {
                render: function(data, type, row) { //suggest_to_sell
                    var nilai = 0;
                    if(row.hpp_item != null) {
                        if(row.hpp_item.margin_rate_manual != null && row.hpp_item.margin_rate_manual != 0) {
                            nilai = Math.ceil(row.hpp_average_month + ((row.hpp_average_month * row.hpp_item.margin_rate_manual) / 100));
                        }
                        if(row.hpp_item.margin_rate_manual == 0) {
                            nilai = row.hpp_average_month
                        }
                    } else {
                        nilai = Math.ceil(row.hpp_average_month + ((row.hpp_average_month * row.margin_rate_auto) / 100));
                    }
                    return 'Rp. '+Helper.toCurrency(nilai);
                }
            },
            {
                render: function(data, type, row) { //suggest_to_sell_manual
                    var nilai = 0;
                    if(row.hpp_item != null && row.hpp_item.suggest_to_sell_manual != null && row.hpp_item.suggest_to_sell_manual != 0) {
                        nilai = row.hpp_item.suggest_to_sell_manual;
                    }

                    return `<div class='form-row'>
                                <div class='col-md-8'>
                                    <input type='text' class='form-control form-control-sm suggest_to_sell_manual_${row.part_data_stock_id} number_only currency' name='suggest_to_sell_manual' data-pdes_id="${row.part_data_stock_id}" id='' max='100' min='0' value=${nilai}>
                                </div>
                                <div class='col-md-4'>
                                    <button class='btn btn-primary btn-sm mr-2 update_suggest_to_sell_manual' data-part_description="${row.part_description}" data-pdes_id="${row.part_data_stock_id}" title='Update Margin Rate'>confirm</button>
                                </div>
                            </div>`;

                }
            },
            {
                render: function(data, type, row){

                    return "<a href='/admin/report-hpp-items/details/"+ row.part_data_stock_id + "/"+$('#month').val()+"/"+$('#year').val()+"' class='btn btn-primary btn-sm' title='Details Hpp Items' target='_blank'><i class='fa fa-bars'></i></a>&nbsp;<a href='/admin/report-hpp-items/details-yearly/"+ row.part_data_stock_id+"' class='btn btn-danger btn-sm' title='Details Hpp Items per Year' target='_blank'><i class='fa fa-bars'></i> Yearly</a>";

                }
            },
        ]
    });

    function checkValidation() {
        var isValid = true;
        var month = $('#month').val();
        var year = $('#year').val();
        var margin_rate_auto = $('#margin_rate_auto').val();
        if(month == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Month !');
            return false;
        } else if (year == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Year !');
            return false;
        } else if (margin_rate_auto == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Margin Sell Auto !');
            return false;
        }
        return isValid;
    }

    $("#form-search").submit(function(e) {
        e.preventDefault();
        if(checkValidation()) {
            var input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/report-hpp-items/datatables/' + playload);
            tbl.table.reloadTable(url);
        }
    });

    $(document).on('click', '#update_margin_rate_auto', function(e) {
        Helper.confirm(function() {
            $.ajax({
                url:Helper.apiUrl('/report-hpp-items/update/margin_rate_auto'),
                type: 'post',
                data: {
                    margin_rate_auto : $('#margin_rate_auto').val(),
                },
                success: function(response) {
                    console.log(response);
                    if (response != 0) {
                        Helper.successNotif('Success Update Margin Rate Auto');
                        tbl.table.reloadTable();
                    }
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });

            e.preventDefault();
        });
    });

    $(document).on('click', '.update_margin_rate', function(e) {
        var $this = $(this);
        Helper.confirm(function() {
            Helper.unMask('.currency');
            $.ajax({
                url:Helper.apiUrl('/report-hpp-items/update/margin_rate_manual'),
                type: 'post',
                data: {
                    margin_rate_manual : $('.margin_rate_manual_'+$this.attr('data-pdes_id')+'').val(),
                    pdes_id : $this.attr('data-pdes_id'),
                    month : $('#month').val(),
                    year : $('#year').val(),
                },
                success: function(response) {
                    console.log(response);
                    if (response != 0) {
                        Helper.successNotif('Success update Margin Sell Manual pada Part Description = '+$this.attr('data-part_description')+'');
                        tbl.table.reloadTable();
                    }
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });

            e.preventDefault();
        });
    });

    $(document).on('click', '.update_suggest_to_sell_manual', function(e) {
        var $this = $(this);
        Helper.confirm(function() {
            Helper.unMask('.currency');
            $.ajax({
                url:Helper.apiUrl('/report-hpp-items/update/suggest_to_sell_manual'),
                type: 'post',
                data: {
                    suggest_to_sell_manual : $('.suggest_to_sell_manual_'+$this.attr('data-pdes_id')+'').val(),
                    pdes_id : $this.attr('data-pdes_id'),
                    month : $('#month').val(),
                    year : $('#year').val(),
                },
                success: function(response) {
                    console.log(response);
                    if (response != 0) {
                        Helper.successNotif('Success update Suggest To Sell Manual pada Part Description = '+$this.attr('data-part_description')+'');
                        tbl.table.reloadTable();
                    }
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });

            e.preventDefault();
        });
    });



</script>
@endsection
