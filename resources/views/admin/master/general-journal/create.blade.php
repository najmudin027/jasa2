@extends('admin.home')
@section('content')
<style>
    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    .btnx {
        border: 2px solid gray;
        color: gray;
        background-color: white;
        border-radius: 8px;
        font-size: 12px;
        font-weight: bold;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }

    .validation {
        color: red;
    }
</style>
<style>
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 100%;
}
</style>

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/general-journal/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off" enctype="multipart/form-data">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Title</label>
                    <div class="col-sm-12">
                        <input name="title" placeholder="Title" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Code</label>
                    <div class="col-sm-12">
                        <input name="code" placeholder="Title" type="text" class="form-control" value="{{ $code }}" readonly>
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                Table Details
                <div class="btn-actions-pane-right text-right">
                    <button type="button" class="btn btn-success btn_add_details"><i class="fa fa-plus"></i></button>
                </div>
            </div>

            <div class="card-body">
                <table class="table" id="table-details">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Date</th>
                            <th>ASC Name</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th width=10%>Qty</th>
                            <th>Debit</th>
                            <th>Credit</th>
                            <th>Price Pcs</th>
                            <th>Balance</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="dynamic_details">
                        <tr>
                            <td ><span class="counter">1</span></td>
                            <td width="10%">
                                <input name="date[]" type="text" class="form-control date_input-xxx date_format" data-uniq="xxx" required>
                            </td>
                            <td width="10%">
                                <select name="asc_id[]" class="form-control asc_id_input-xxx" required style="width:100%">
                                </select>
                            </td>
                            <td width="10%">
                                <select name="category[]" type="text" class="form-control category_input-xxx" data-uniq="xxx" style="width:100%" required>
                                </select>
                            </td>
                            <td width="10%">
                                <input name="description[]" type="text" class="form-control description_input-xxx" data-uniq="xxx">

                                <select name="part_data_stock_inventories_id_z[]" placeholder="Category" type="text" class="form-control part_data_stock_inventories_id_z_input-xxx" data-uniq="xxx" style="width:100%;display:none;">
                                </select>
                                <input name="part_data_stock_inventories_id[]" type="text" class="form-control part_data_stock_inventories_id_input-xxx" data-uniq="xxx" style="display:none;">

                            </td>
                            <td>
                                <input name="quantity[]" placeholder="Qty" type="text" class="form-control number_only quantity_input-xxx" data-uniq="xxx" required>
                                <input name="stock_available[]" type="text" class="form-control number_only stock_available_input-xxx" data-uniq="xxx" style="display:none;">
                            </td>
                            <td class="debit">
                                <input name="debit[]" placeholder="Debit" type="text" class="form-control number_only debit_input-xxx" data-uniq="xxx" required>
                            </td>
                            <td class="kredit">
                                <input name="kredit[]" placeholder="Kredit" type="text" class="form-control number_only kredit_input-xxx" data-uniq="xxx" required>
                            </td>
                            <td class="price_pcs" width="5%">
                                <input name="price_pcs[]" placeholder="" type="text" class="form-control price_pcs_input-xxx" data-uniq="xxx" readonly>
                            </td>
                            <td class="balance">
                                <input name="balance[]" placeholder="" type="text" class="form-control balance_input-xxx number_only" data-uniq="xxx" readonly>
                            </td>
                            <td>
                                <div class="upload-btn-wrapper">
                                    <button data-uniq = "xxx" class="btn-transition btn btn-sm btn-secondary btnx-xxx"><i class="fa fa-upload"></i></button></button>
                                    <input type="file" data-uniq="xxx" name="filename[]" class="filename_input-xxx putImages" />
                                </div>
                            </td>
                        </tr>
                        <tfoot>
                            <tr>
                                <td colspan="3">Ending</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><input name="total_debit" placeholder="" type="text" class="form-control" id="total_debit" readonly></td>
                                <td><input name="total_kredit" placeholder="" type="text" class="form-control" id="total_kredit" readonly></td>
                                <td><input name="total_balance" placeholder="" type="text" class="form-control" id="total_balance" readonly></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">img</label>
                    <div class="col-sm-6">
                        <input name="image" placeholder="Image" type="file" class="form-control" id="imgInp">
                    </div>
                    <div class="col-sm-3">
                        <a download data-fancybox href="#" id="img-fancy">
                            <img id='img-upload'/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> CREATE </button>
        </div>
    </div>
</form>






@endsection

@section('script')
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<script>

    globalCRUD.select2('.asc_id_input-xxx', '/asc-excel/select2');
    Helper.hideSidebar();
    $(function() {
        $('.part_data_stock_inventories_id_z_input-xxx').hide();
        ClassApp.select2AscId('xxx');
        ClassApp.select2Category('xxx');

        $('.time').inputmask("hh:mm:ss", {
            placeholder: "hh:mm:ss",
            insertMode: false,
            showMaskOnHover: false,
            hourFormat: "24",
            clearIncomplete: true
        });
        $('.date').inputmask("dd-mm-yyyy", {
            placeholder: "dd-mm-yyyy",
            clearIncomplete: true
        });
        Helper.dateFormat('.date_format');
    });
    Helper.onlyNumberInput('.number_only');

    $(function() {
        var xx = 1;
        $(document)
            .on('click', '.btn_add_details', function() {
                templateNewInputDetailData(xx)
                    xx++;
            })
        // delete detail
        $(document)
            .on('click', '.btn_remove_details', function() {
                var button_id = $(this).attr("data-uniq");
                $('#details_row_' + button_id + '').remove();
            });

    })

    function templateNewInputDetailData(xx) {

        $('.dynamic_details').append((`
            <tr id="details_row_${xx}">
                <td><span class="counter">${xx}</span></td>
                <td>
                    <input name="date[]" type="text" class="form-control date_input-${xx} date_format" data-uniq="${xx}" required="required">
                </td>
                <td width="10%">
                    <select name="asc_id[]" class="form-control asc_id_input-${xx}" required data-uniq="${xx}" style="width:100%">
                    </select>
                </td>
                <td>
                    <select name="category[]" type="text" class="form-control category_input-${xx}" data-uniq="${xx}" style="width:100%" required>
                    </select>

                </td>
                <td>
                    <input name="description[]" placeholder="Description" type="text" class="form-control description_input-${xx}" data-uniq="${xx}">

                    <select name="part_data_stock_inventories_id_z[]" type="text" class="form-control part_data_stock_inventories_id_z_input-${xx}" data-uniq="${xx}" style="width:100%;display:none;">
                    </select>
                    <input name="part_data_stock_inventories_id[]" type="text" class="form-control part_data_stock_inventories_id_input-${xx}" data-uniq="${xx}" style="display:none;">

                </td>
                <td>
                    <input name="quantity[]" placeholder="Quantity" type="text" class="form-control quantity_input-${xx} number_only" data-uniq="${xx}" required="required">
                    <input name="stock_available[]" type="text" class="form-control number_only stock_available_input-${xx}" data-uniq="${xx}" style="display:none;">

                </td>
                <td>
                    <input name="debit[]" placeholder="Debit" type="text" class="form-control debit_input-${xx} number_only" data-uniq="${xx}" required="required">
                </td>
                <td>
                    <input name="kredit[]" placeholder="Kredit" type="text" class="form-control kredit_input-${xx} number_only" data-uniq="${xx}" required="required">
                </td>
                <td>
                    <input name="price_pcs[]" placeholder="" type="text" class="form-control price_pcs_input-${xx} number_only" data-uniq="${xx}" readonly>
                </td>
                <td>
                    <input name="balance[]" placeholder="" type="text" class="form-control balance_input-${xx} number_only" data-uniq="${xx}" readonly>
                </td>
                <td>
                    <div role="group" class="mb-2 btn-group-sm btn-group">
                        <button type="button" data-uniq="${xx}" class="btn-transition btn btn-sm btn-danger btn_remove_details"><i class="fa fa-close"></i></button> &nbsp;
                        <div class="upload-btn-wrapper">
                            <button data-uniq="${xx}" class="btn-transition btn btn-sm btn-secondary btnx-${xx}"><i class="fa fa-upload"></i></button>
                            <input type="file" name="filename[]" class="form-control filename_input-${xx}" data-uniq="${xx}">
                        </div> &nbsp;
                    </div>
                </td>
            </tr>
        `));
        $('.part_data_stock_inventories_id_input_z-'+xx+'').hide();
        globalCRUD.select2(".asc_id_input-"+xx+"", '/asc-excel/select2');
        ClassApp.select2AscId(xx);
        ClassApp.select2Category(xx);


        Helper.dateFormat('.date_format');
        Helper.onlyNumberInput('.number_only')
    }
    var ClassApp = {
        hitungBalance: function() {
            // var balance = 0;
            var currentBalance = 0;
            var totalDebit = 0;
            var totalKredit = 0;
            $('input[name="balance[]"]').each(function() {
                var uniq = $(this).attr('data-uniq');
                var debit = $('.debit_input-' + uniq).val() == '' ? 0 : +parseInt($('.debit_input-' + uniq).val());
                var kredit = $('.kredit_input-' + uniq).val() == '' ? 0 : -parseInt($('.kredit_input-' + uniq).val());

                currentBalance += debit + kredit;
                totalDebit += debit;
                totalKredit += kredit;
                $('#total_debit').val(totalDebit);
                $('#total_kredit').val(totalKredit);
                $('#total_balance').val(currentBalance);
                $('.balance_input-' + uniq).val(currentBalance);

                var price_pcs = 0;
                if($('.category_input-' + uniq).val() == 'material') {
                    price_pcs = parseInt($('.kredit_input-' + uniq).val()) / parseInt($('.quantity_input-' + uniq).val());
                }
                $('.price_pcs_input-' + uniq).val(Math.ceil(ClassApp.preventNan(price_pcs)));
            })
        },
        select2AscId: function (xx) {
            $(document).on('change', '.asc_id_input-'+xx+'', function() {
                if($(this).val() != "") {
                    ClassApp.select2PartDataStock(xx,$(this).val());
                }
            });
        },
        select2Category: function(xx)  {
            var data_category = [

                {id: 'operational', text: 'Operational', title: 'Operational'},
                {id: 'material', text: 'Material', title: 'Material'},

            ];
            $(".category_input-"+xx+"").select2({
                data: data_category,
                escapeMarkup: function(markup) {
                    return markup;
                }
            })
            $('.category_input-'+xx+'').change(function() {
                if($(this).val() == 'operational') {
                    $('.description_input-'+xx+'').show();
                    $('.part_data_stock_inventories_id_z_input-'+xx+'').hide();
                    ClassApp.select2PartDataStock(xx,$('.asc_id_input-'+xx+'').val(),false);
                } else if($(this).val() == 'material') {
                    $('.description_input-'+xx+'').hide();
                    ClassApp.select2PartDataStock(xx,$('.asc_id_input-'+xx+'').val(),true);
                    $('.part_data_stock_inventories_id_z_input-'+xx+'').show();
                }
            })
        },
        select2PartDataStock : function(xx, asc_id=null, visible = false) {
            globalCRUD.select2('.part_data_stock_inventories_id_z_input-'+xx+'', '/part-data-excel-stock-inventory/select2/part_description/'+asc_id+'/filter_petty', function(item) {
                return {
                    id: item.id,
                    text:item.part_data_stock.part_description,
                    stock:item.stock
                }
            },visible);

            $('.part_data_stock_inventories_id_z_input-'+xx+'').on('select2:select', function (e) {
                $('.stock_available_input-'+xx+'').val(e.params.data.stock);
                $('.description_input-'+xx+'').val(e.params.data.text);
                $('.part_data_stock_inventories_id_input-'+xx+'').val(e.params.data.id);
            });
        },
        setDebit: function (uniq) {
            return $('.kredit_input-' + uniq).val(0);
        },
        setKredit: function (uniq) {
            return $('.debit_input-' + uniq).val(0);
        },
        setQuantity: function (uniq) {
            return $('.quantity_input-' + uniq).val();
        },
        preventNan: function(val) {
            if (isNaN(val)) {
                return 0;
            }
            return val;
        }
    }

    $(document).on('keyup', 'input[name="debit[]"]', function() {
        ClassApp.setDebit($(this).attr('data-uniq'));
        ClassApp.hitungBalance();
    });

    $(document).on('keyup', 'input[name="quantity[]"]', function() {
        ClassApp.setQuantity($(this).attr('data-uniq'));
        ClassApp.hitungBalance();
    });

     $(document).on('keyup', 'input[name="kredit[]"]', function() {
        ClassApp.setKredit($(this).attr('data-uniq'));
        ClassApp.hitungBalance();
    });

    $(document).on('change', 'input[name="filename[]"]', function() {
        var uniq = $(this).attr("data-uniq");
        if ($(this).get(0).files.length != 0) {
            $('.btnx-' + uniq + '').toggleClass('btn-info');
        }
    })


    // delete details row
    $(document).on('click', '.btn_remove_details', function() {
        var button_id = $(this).attr("data-uniq");
        $('#details_row_' + button_id + '').remove();
        ClassApp.hitungBalance();
    });


    $("#form-data").submit(function(e) {
        var form = Helper.serializeForm($(this));
        var fd = new FormData($(this)[0]);
        var files = $('#imgInp')[0].files[0];
        if(files){
            fd.append('image', files);
        }
        // console.log(form, fd);
        // Display the key/value pairs
        for (var pair of fd.entries()) {
            console.log(pair[0]+ ', ' + pair[1]);
        }

        $.ajax({
            url:Helper.apiUrl('/general-journal'),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'General Journal Has Been Saved',
                    });
                    window.location.href = Helper.redirectUrl('/admin/general-journal/show');
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });

                    })
                }

            },
        });

        e.preventDefault();
    });
</script>

<script>
    $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                    $('#img-fancy').attr('href', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		});
	});
</script>


@endsection
