@extends('admin.home')
@section('content')
<style>
    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    .btnx {
        border: 2px solid gray;
        color: gray;
        background-color: white;
        border-radius: 8px;
        font-size: 12px;
        font-weight: bold;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }

    .validation {
        color: red;
    }
</style>
<style>
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 100%;
}
</style>

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/general-journal/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off" enctype="multipart/form-data">
    <input type="hidden" name="id" value="{{ $getData->id }}">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Title</label>
                    <div class="col-sm-12">
                        <input name="title" placeholder="Title" type="text" class="form-control" value="{{ $getData->title }}" required>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Code</label>
                    <div class="col-sm-12">
                        <input name="code" placeholder="Title" type="text" class="form-control" value="{{ $getData->code }}" readonly>
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                Table Details
                <div class="btn-actions-pane-right text-right">
                    <button type="button" class="btn btn-success btn_add_details"><i class="fa fa-plus"></i></button>
                </div>
            </div>

            <div class="card-body">
                <table class="table" id="table-details">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Date</th>
                            <th>ASC Name</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th width=10%>Qty</th>
                            <th>Debit</th>
                            <th>Credit</th>
                            <th>Price Pcs</th>
                            <th>Balance</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="dynamic_details">
                        <input type="hidden" value="{{ count($detailsData) }}" id="count_details_data" />
                        @foreach($detailsData as $key => $details)
                        <?php $uniq = $key+1; ?>
                            <tr class="dynamic_details_content " id="details_row_{{ $uniq }}">
                                <input type="hidden" value="{{ $details->id }}" name="details_id[]" />
                                <td><span class="counter">{{ $uniq }}</span></td>
                                <td width="10%">
                                    <input type="text" name="date[]" data-uniq="{{ $uniq }}" class="form-control date_format date_input-{{ $uniq }}" value="{{ $details->date->format('d-m-Y') }}" required/>
                                </td>
                                <td width="10%">
                                    <select name="asc_id[]" class="form-control asc_id_input-{{ $uniq }}"  data-uniq="{{ $uniq }}" style="width:100%" >
                                        @if($details->asc_id != null)
                                            <option value="{{ $details->asc_id }}" >{{ $details->asc->name }}</option>
                                        @else
                                            <option value="kosong">Kosong</option>
                                        @endif
                                    </select>
                                </td>
                                <td  width="10%">
                                    <select name="category[]" class="form-control category_input-{{ $uniq }}" style="width:100%" data-uniq="{{ $uniq }}" >
                                        <option value="{{ $details->category }}" >{{ $details->category }}</option>
                                    </select>
                                </td>
                                <td width="10%">
                                    @if($details->category == "operational")
                                        <input type="text" placeholder="description" name="description[]" data-uniq="{{ $uniq }}" class="form-control description_input-{{ $uniq }}" value="{{ $details->description }}" />

                                        <select name="part_data_stock_inventories_id_z[]" type="text" class="form-control part_data_stock_inventories_id_z_input-{{ $uniq }}" data-uniq="{{ $uniq }}" style="width:100%;display:none;" >
                                        </select>

                                    @else
                                    <input name="description[]" type="text" class="form-control description_input-{{ $uniq }}" data-uniq="{{ $uniq }}" style="display:none;" value="{{ $details->description }}">

                                    <select name="part_data_stock_inventories_id_z[]" class="form-control part_data_stock_inventories_id_z_input-{{ $uniq }}" style="width:100%" data-uniq="{{ $uniq }}" >
                                        @if($details->part_data_stock_inventories_id != null)
                                            <option value="{{ $details->part_data_stock_inventories_id }}" >{{ $details->part_data_stock_inventory != null ? $details->part_data_stock_inventory->part_data_stock->part_description : 'Kosong' }}</option>
                                        @else
                                            <option value="">Kosong</option>
                                        @endif
                                    </select>
                                    @endif
                                    <input name="part_data_stock_inventories_id[]" type="text" class="form-control part_data_stock_inventories_id_input-{{ $uniq }}" data-uniq="{{ $uniq }}" style="display:none;" value="{{ $details->part_data_stock_inventories_id }}">
                                </td>
                                <td>
                                    <input type="text" placeholder="quantity" name="quantity[]" data-uniq="{{ $uniq }}" class="form-control quantity quantity_input-{{ $uniq }}" value="{{ $details->quantity }}" required/>
                                    <input type="hidden" name="stock_edit_available[]" data-uniq="{{ $uniq }}" class="form-control stock_edit_available_input-{{ $uniq }}" value="{{ $details->stock_edit_available }}" />

                                </td>
                                <td class="debit">
                                    <input type="text" placeholder="debit" name="debit[]" data-uniq="{{ $uniq }}" class="form-control number_only debit_input-{{ $uniq }}" value="{{ $details->debit }}" />
                                </td>
                                <td class="kredit">
                                    <input type="text" placeholder="kredit" name="kredit[]" data-uniq="{{ $uniq }}" class="form-control number_only kredit_input-{{ $uniq }}" value="{{ $details->kredit }}" />
                                </td>
                                <td class="price_pcs">
                                    <input type="text" placeholder="price_pcs" name="price_pcs[]" data-uniq="{{ $uniq }}" class="form-control number_only price_pcs_input-{{ $uniq }}" value="" readonly/>
                                </td>
                                <td class="balance">
                                    <input type="text" placeholder="balance" name="balance[]" data-uniq="{{ $uniq }}" class="form-control number_only balance_input-{{ $uniq }}" value="{{ $details->balance }}" readonly/>
                                </td>
                                <td>
                                    <div role="group" class="mb-2 btn-group-sm btn-group">

                                        <button type="button" data-details_data_id="{{ $details->id }}" data-uniq="{{ $uniq }}" class="btn-transition btn btn-sm btn-danger btn_remove_details"><i class="fa fa-close"></i></button>&nbsp;

                                        <div class="upload-btn-wrapper">
                                            <button data-uniq="{{ $uniq }}" class="btn-transition btn btn-sm btn-secondary btnx-{{ $uniq }}"><i class="fa fa-upload"></i></button></button>
                                            <input type="file" data-uniq="{{ $uniq }}" name="filename[]" class="filename_input-{{ $uniq }} putImages" />
                                        </div>&nbsp;
                                        @if($details->filename != null)
                                            <a class="btn-transition btn btn-sm btn-secondary" download data-fancybox href="{{ url('/storage/general-journal-detail/'.$details->filename.'') }}"><i class="fa fa-file-image-o"></i></a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        <tfoot>
                            <tr>
                                <td colspan="3">Ending</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><input name="total_debit" placeholder="" type="text" class="form-control" id="total_debit" readonly></td>
                                <td><input name="total_kredit" placeholder="" type="text" class="form-control" id="total_kredit" readonly></td>
                                <td><input name="total_balance" placeholder="" type="text" class="form-control" id="total_balance" readonly></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Image</label>
                    <div class="col-sm-6">
                        <input name="image" placeholder="Image" type="file" class="form-control" id="imgInp">
                    </div>
                    <div class="col-sm-3">
                        <div class="col-sm-3">
                            @if($getData->image == null )
                                <img id="img-upload" src="http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png" alt="your image" style="width:100px;height:100px"/>
                            @else
                            <a id="img-fancy" download data-fancybox href="{{asset('/storage/general-journal/' . $getData->image)}}">
                                <img id='img-upload' src="{{asset('/storage/general-journal/' . $getData->image)}}" alt="your image" style="width:125px;height:125px">
                            </a>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> SAVE </button>
        </div>
    </div>
</form>

<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            General Journal Logs
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="general-journal-logs" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Type Modified</th>
                        <th>By User (Name)</th>
                        <th>Created At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<script>

    Helper.hideSidebar();
    var id = $('input[name="id"]').val();
    $(document).ready(function() {
        ClassApp.hitungBalance();
        Helper.onlyNumberInput('.number_only');
        Helper.dateFormat('.date_format');
    });

    $(function() {
        ClassApp.select2LoadAll();
        var xx = parseInt($('#count_details_data').val())+1;

        $(document)
            .on('click', '.btn_add_details', function() {
                templateNewInputDetailData(xx)
                    xx++;
            })

    })

    function templateNewInputDetailData(xx) {

        $('.dynamic_details').append((`
            <tr id="details_row_${xx}">
                <input type="hidden" value="0" name="details_id[]">
                <td><span class="counter">${xx}</span></td>
                <td>
                    <input name="date[]" type="text" class="form-control date_input-${xx} date_format" data-uniq="${xx}" required="required">
                </td>
                <td width="10%">
                    <select name="asc_id[]" class="form-control asc_id_input-${xx}" required data-uniq="${xx}" style="width:100%">
                    </select>
                </td>
                <td>
                    <select name="category[]" placeholder="Category" type="text" class="form-control date category_input-${xx}" data-uniq="${xx}">
                </td>
                <td>
                    <input name="description[]" placeholder="Description" type="text" class="form-control description_input-${xx}" data-uniq="${xx}">

                    <select name="part_data_stock_inventories_id_z[]" type="text" class="form-control part_data_stock_inventories_id_z_input-${xx}" data-uniq="${xx}" style="width:100%;display:none;">
                    </select>
                    <input name="part_data_stock_inventories_id[]" type="text" class="form-control part_data_stock_inventories_id_input-${xx}" data-uniq="${xx}" style="display:none;">

                </td>
                <td>
                    <input name="quantity[]" placeholder="Quantity" type="text" class="form-control quantity_input-${xx} number_only" data-uniq="${xx}" required>

                </td>
                <td>
                    <input name="debit[]" placeholder="Debit" type="text" class="form-control debit_input-${xx} number_only" data-uniq="${xx}">
                </td>
                <td>
                    <input name="kredit[]" placeholder="Kredit" type="text" class="form-control kredit_input-${xx} number_only" data-uniq="${xx}">
                </td>
                <td>
                    <input name="price_pcs[]" placeholder="Price pcs" type="text" class="form-control price_pcs_input-${xx} number_only" data-uniq="${xx}" readonly>
                </td>
                <td>
                    <input name="balance[]" placeholder="" type="text" class="form-control balance_input-${xx} number_only" data-uniq="${xx}" readonly>
                </td>
                <td>
                    <div role="group" class="mb-2 btn-group-sm btn-group">
                    <button type="button" data-details_data_id="0" data-uniq="${xx}" class="btn-transition btn btn-sm btn-danger btn_remove_details"><i class="fa fa-close"></i></button>&nbsp;
                        <div class="upload-btn-wrapper">
                            <button data-uniq="${xx}" class="btn-transition btn btn-sm btn-secondary btnx-${xx}"><i class="fa fa-upload"></i></button>
                            <input type="file" name="filename[]" class="form-control filename_input-${xx}" data-uniq="${xx}">
                        </div>
                    </div>
                </td>
            </tr>
        `));
        $('.part_data_stock_inventories_id_z_input-'+xx+'').hide();
        globalCRUD.select2(".asc_id_input-"+xx+"", '/asc-excel/select2');
        ClassApp.select2AscId(xx);
        ClassApp.select2Category(xx);

        Helper.dateFormat('.date_format');
        Helper.onlyNumberInput('.number_only')
    }
    var ClassApp = {
        hitungBalance: function() {
            // var balance = 0;
            var currentBalance = 0;
            var totalDebit = 0;
            var totalKredit = 0;
            $('input[name="balance[]"]').each(function() {
                var uniq = $(this).attr('data-uniq');
                var debit = $('.debit_input-' + uniq).val() == '' ? 0 : +parseInt($('.debit_input-' + uniq).val());
                var kredit = $('.kredit_input-' + uniq).val() == '' ? 0 : -parseInt($('.kredit_input-' + uniq).val());

                currentBalance += debit + kredit;
                totalDebit += debit;
                totalKredit += kredit;
                $('.balance_input-' + uniq).val(currentBalance);
                $('#total_debit').val(totalDebit);
                $('#total_kredit').val(totalKredit);
                $('#total_balance').val(currentBalance);
                //harga satuan
                var price_pcs = 0;
                if($('.category_input-' + uniq).val() == 'material') {
                    price_pcs = parseInt($('.kredit_input-' + uniq).val()) / parseInt($('.quantity_input-' + uniq).val());
                }
                $('.price_pcs_input-' + uniq).val(Math.ceil(ClassApp.preventNan(price_pcs)));

            })
        },
        select2LoadAll : function() {
            $('select[name="asc_id[]"]').each(function(){
                var uniq = $(this).attr('data-uniq');
                ClassApp.select2AscId(uniq);
                ClassApp.select2Category(uniq);
            });
        },
        select2AscId: function (xx) {
            globalCRUD.select2('.asc_id_input-'+xx+'', '/asc-excel/select2');
            ClassApp.select2PartDataStock(xx,$('.asc_id_input-'+xx+'').val());
            $(document).on('change', '.asc_id_input-'+xx+'', function() {
                if($(this).val() != "") {
                    ClassApp.select2PartDataStock(xx,$(this).val());
                }
            });
        },
        select2Category: function(xx)  {
            var data_category = [

                {id: 'operational', text: 'operational', title: 'operational'},
                {id: 'material', text: 'material', title: 'material'},

            ];
            $(".category_input-"+xx+"").select2({
                data: data_category,
                escapeMarkup: function(markup) {
                    return markup;
                }
            })
            $('.category_input-'+xx+'').change(function() {
                if($(this).val() == 'operational') {
                    $('.description_input-'+xx+'').show();
                    $('.part_data_stock_inventories_id_z_input-'+xx+'').hide();
                    $('.part_data_stock_inventories_id_input-'+xx+'').val('');
                    $('.part_data_stock_inventories_id_z_input-'+xx+'').val('');
                    ClassApp.select2PartDataStock(xx,$('.asc_id_input-'+xx+'').val(),false);
                } else if($(this).val() == 'material') {
                    $('.description_input-'+xx+'').hide();
                    ClassApp.select2PartDataStock(xx,$('.asc_id_input-'+xx+'').val(),true);
                    $('.part_data_stock_inventories_id_z_input-'+xx+'').show();
                }
            })
        },
        select2PartDataStock : function(xx, asc_id=null,visible = false) {
            globalCRUD.select2('.part_data_stock_inventories_id_z_input-'+xx+'', '/part-data-excel-stock-inventory/select2/part_description/'+asc_id+'/filter_petty', function(item) {
                return {
                    id: item.id,
                    text:item.part_data_stock.part_description,
                    stock:item.stock
                }
            },visible);
            $('.part_data_stock_inventories_id_z_input-'+xx+'').on('select2:select', function (e) {
                $('.description_input-'+xx+'').val(e.params.data.text);
                $('.part_data_stock_inventories_id_input-'+xx+'').val(parseInt(e.params.data.id));
            });
        },
        setDebit: function (uniq) {
            return $('.kredit_input-' + uniq).val(0);
        },
        setKredit: function (uniq) {
            return $('.debit_input-' + uniq).val(0);
        },
        setQuantity: function (uniq) {
            return $('.quantity_input-' + uniq).val();
        },
        preventNan: function(val) {
            if (isNaN(val)) {
                return 0;
            }
            return val;
        },
        delete: function($el) {
            var uniq = $el.attr("data-uniq");
            var details_id = parseInt($el.attr("data-details_data_id"));

            if (details_id != 0) {
                Helper.confirm(function() {
                    Axios.delete('/general-journal/destroy_details_data/' + details_id)
                        .then(function(response) {
                            if (response.status == 204) {
                                Helper.successNotif(Helper.deleteMsg());
                            } else {
                                Helper.successNotif(response.data.msg);
                            }
                            $('#details_row_' + uniq + '').remove();
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
                return false;
            } else {
                $('#details_row_' + uniq + '').remove();
            }
        },
        editStockAvailable: function($el,e) {
            var max = parseInt($('.stock_edit_available_input-'+$el.attr("data-uniq")+'').val());
            // console.log($('.stock_edit_available_input-'+xx+'').val());
            if ($el.val() < max) {
                console.log($el.val(max));
                e.preventDefault();
                Helper.warningNotif('Tidak dapat edit stok kurang dari '+max+', karena terdapat part yang sudah di pakai di orders-data !');
                $el.val(max);
            } else {
                console.log($el.val());
                $el.val();
            }

        }

    }

    $(document).on('keyup', 'input[name="debit[]"]', function() {
        ClassApp.setDebit($(this).attr('data-uniq'));
        ClassApp.hitungBalance();
    });

    $(document).on('keyup', 'input[name="kredit[]"]', function() {
        ClassApp.setKredit($(this).attr('data-uniq'));
        ClassApp.hitungBalance();
    });

    $(document).on('keyup', 'input[name="quantity[]"]', function(e) {
        ClassApp.setQuantity($(this).attr('data-uniq'));
        ClassApp.editStockAvailable($(this),e);
        ClassApp.hitungBalance();
    });

    $(document).on('change', 'input[name="filename[]"]', function() {
        var uniq = $(this).attr("data-uniq");
        if ($(this).get(0).files.length != 0) {
            $('.btnx-' + uniq + '').toggleClass('btn-info');
        }
    })

    // delete part my inv
    $(document).on('click', '.btn_remove_details', function() {
        ClassApp.delete($(this));
     });


    $("#form-data").submit(function(e) {
        var form = Helper.serializeForm($(this));
        var fd = new FormData($(this)[0]);
        var files = $('#imgInp')[0].files[0];
        if(files){
            fd.append('image', files);
        }
        // console.log(form, fd);
        // Display the key/value pairs
        for (var pair of fd.entries()) {
            console.log(pair[0]+ ', ' + pair[1]);
        }

        $.ajax({
            url:Helper.apiUrl('/general-journal/'+id+''),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'General Journal Has Been Saved',
                    });
                    // window.location.href = Helper.redirectUrl('/admin/general-journal/show');
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });

                    })
                }
            },
        });

        e.preventDefault();
    });

    globalCRUD.datatables({
        url: '/general-journal/datatables_logs/'+id+'',
        selector: '#general-journal-logs',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "type",
            name: "type",
            render: function(row,data,full) {
                if(row == 1) {
                    return 'Created';
                } else if(row == 2) {
                    return 'Updated'
                } else {
                    return 'Deleted';
                }
            }
        },
        {
            data: "user.name",
            name: "user.name",
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm");
            }
        }
        ]
    })
</script>

<script>
    $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                    $('#img-fancy').attr('href', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		});
	});
</script>


@endsection
