@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div id="copied_success"></div>
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                GENERAL MASTER VOUCHERS
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/general-voucher/create') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New Vouchers</a>
            </div>
        </div>

        <div class="card-body">
            <table style="width: 100%;" id="table-general-voucer" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Voucher Type</th>
                        <th>Nominal</th>
                        <th>Maximum Discount</th>
                        <th>Minimal Payment</th>
                        <th>Valid Util</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<input id="input_copy" type="text" name="id" value="" style="display: none"> 
@endsection

@section('script')
    <script>
       

        $(document).on('click', '#btn_copy', function(){
            $('#input_copy').val($(this).attr('data-order-code'));

            var copyText = document.getElementById("input_copy");
            copyText.select();
            copyText.setSelectionRange(0, 99999)
            document.execCommand("copy");
            $("#copied_success").html('');
            var voucerTemplate = '';
            voucerTemplate += `<div class="alert alert-success alert-success" role="alert">
                                    <center>
                                        <i class="fa fa-check"></i> 
                                        <strong>Voucher Code `+ copyText.value +` Successfully Copied</strong>
                                    </center>
                                </div>`;
            $("#copied_success").append(voucerTemplate);
            // alert("Copied the text: " + copyText.value);
        });

        var table = $('#table-general-voucer').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            select: true,
            dom: 'Bflrtip',
            // ordering:'true',
            // order: [1, 'desc'],
            responsive: true,
            language: {
                search: '',
                searchPlaceholder: "Search..."
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/admin/voucher-general/datatables'),
                type: "get",

            },
            columns: [
                {
                    data: "code",
                    name: "code",
                    render: function(data, type, full) {
                        return '<span id="value_password-'+ full.id +'" class="password-span">'+full.code+'</span>' + '' + '<button id="btn_copy" data-order-id='+ full.id +' data-order-code='+ full.code +' class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Copy To Clipboard" style="float:right"><i class="fa fa-copy"></i></button>';
                    }
                },
                {
                    data: "voucher_type",
                    name: "voucher_type",
                    render: function(data, type, full) {
                        if(full.voucher_type == 1){
                            return "Cashback";
                        }else{
                            return "Subtraction";
                        }
                        
                    }
                },
                {
                    data: "value",
                    name: "value",
                    render: function(data, type, full) {
                        if(full.type == 1){
                            return full.value+ " %";
                        }else{
                            return "Rp. " + full.value;
                        }
                        
                    }
                },
                {
                    data: "max_nominal",
                    name: "max_nominal",
                    render: function(data, type, full) {
                        if(full.type == 1){
                            return "Rp." + full.max_nominal;
                        }else{
                            return "-";
                        }
                    }
                },
                {
                    data: "min_payment",
                    name: "min_payment",
                    render: function(data, type, full) {
                            return  "Rp. " + full.min_payment;
                    }
                },
                {
                    data: "valid_until",
                    name: "valid_until",
                    render: function(data, type, full) {
                        return moment(full.valid_until).format("DD MMMM YYYY HH:mm");
                        
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        deleted = '<button type="button" data-ids ="'+full.id+'" id="delete" class="btn btn-danger btn-sm" data-toggle="tooltip" data-html="true" title="<b>Delete</b>"><i class="fa fa-trash"></i></button>';
                        update  = "<a href='/admin/general-voucher/update/"+ full.id +"' class='btn btn-search btn-sm btn-warning'  data-toggle='tooltip' data-html='true' title='<b>Detail</b>'><i class='fa fa-pencil'></i></a>";
                        return deleted + ' ' + update;
                    }
                },
            ]
        });

        $(document).on('click', '#delete', function(e) {
            // Helper.confirm(function(){
                id = $(this).attr('data-ids'),
                Helper.loadingStart();
                $.ajax({
                    url:Helper.apiUrl('/admin/voucher-general/delete/' + id),
                    type: 'delete',

                    success: function(res) {
                        Helper.successNotif('Success');
                        table.ajax.reload();
                    },
                    error: function(xhr, status, error) {
                        alert('gagal');
                        Helper.loadingStop();
                    }
                })
            // })
            e.preventDefault()
        })
        
        
    </script>
@endsection
