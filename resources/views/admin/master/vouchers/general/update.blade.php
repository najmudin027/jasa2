@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
               {{-- <button class="btn btn-primary btn-sm add-bank-transfer">Back</button> --}}
            </div>
        </div>

        <div class="card-body">
            {{-- <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Voucher Code</label>
                <div class="col-sm-9">
                    <input name="code" placeholder="VCR-{{ date('y-m-d') }}-Your code" type="text" class="form-control">
                </div>
            </div> --}}

            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Voucher Type</label>
                <div class="col-sm-9">
                    <select class="voucer_type" name="voucher_type" style="width:100%">
                        <option value="1">Cashback</option>
                        <option value="2">Subtraction</option>
                    </select>
                    <strong><span id="error-type" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Type</label>
                <div class="col-sm-9">
                    <select class="js-example-basic-single" id="type_voucher" name="type" style="width:100%">
                        @if ($showData->type == 1)
                            <option value="1">Percentage (%)</option>
                            <option value="2">Fix (Rp)</option>
                        @else
                            <option value="2">Fix (Rp)</option>
                            <option value="1">Percentage (%)</option>
                        @endif
                    </select>
                    <strong><span id="error-type" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Value</label>
                <div class="col-sm-9">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" value="{{ $showData->value }}" placeholder="0" id="demo" name="value">
                        @if($showData->type == 1)
                            <div class="input-group-append">
                                <span class="input-group-text">% (Percentage)</span>
                            </div>
                        @else
                            <div class="input-group-append">
                                <span class="input-group-text">Rp. (Rupiah)</span>
                            </div>
                        @endif
                    </div>
                        {{-- <input name="value" placeholder="" value="{{ $showData->value }}" type="text" id="values" class="form-control"> --}}
                        <strong><span id="error-value" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Max Nominal</label>
                <div class="col-sm-9">
                    @if($showData->max_nominal != null)
                        <input name="max_nominal" placeholder="" value="{{ $showData->max_nominal }}" type="text" id="max_nominal" class="form-control">
                        <strong><span id="error-max_nominal" style="color:red"></span></strong>
                    @else
                        <input name="max_nominal" placeholder="" value="{{ $showData->max_nominal }}" type="text" id="max_nominal" class="form-control" disabled>
                        <strong><span id="error-max_nominal" style="color:red"></span></strong>
                    @endif
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Minimal Payment</label>
                <div class="col-sm-9">
                    <input name="min_payment" placeholder="" value="{{ $showData->min_payment }}" type="text" class="form-control">
                    <strong><span id="error-min_payment" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Valid At</label>
                <div class="col-sm-9">
                    <input name="valid_at" id="datetimepicker1" placeholder="" value="{{ $showData->valid_at }}" type="text" class="form-control">
                    <strong><span id="error-valid_at" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Valid Until</label>
                <div class="col-sm-9">
                    <input name="valid_until" id="datetimepicker2" value="{{ $showData->valid_until }}" placeholder="" type="text" class="form-control">
                    <strong><span id="error-valid_until" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label" >Desc</label>
                <div class="col-sm-9">
                    <textarea name="desc" id="" cols="30" rows="10" class="form-control">{{ $showData->desc }}</textarea>
                    <strong><span id="error-desc" style="color:red"></span></strong>
                </div>
            </div>
            <div class="position-relative row form-check">
            <div class="col-sm-10 offset-sm-2">
                <button class="btn btn-success" id="saved_voucher" style="float:right"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
            </div>
            <input type="hidden" name="id" data-id="{{ $showData->id }}" value="{{ $showData->id }}">
        </div><br>
        </div>
    </div>
</div>
@endsection

@section('script')
    <link rel="stylesheet" type="text/css" href="{{ asset('/datePicker/jquery.datetimepicker.css') }}" >
    <script src="{{ asset('/datePicker/jquery.datetimepicker.full.min.js') }}"></script>

    {{-- date picker --}}
    <script>
        $(document).ready(function(){
            jQuery('#datetimepicker1').datetimepicker({
                format:'Y-m-d',
                mask:true
            });
        });

        $(document).ready(function(){
            jQuery('#datetimepicker2').datetimepicker({
                format:'Y-m-d',
                mask:true
            });
        });
    </script>

    {{-- append type --}}
    <script>
        $('#type_voucher').change(function(){
            if( $(this).val() == 1){
                $('#max_nominal').prop('disabled', false);
                $('#values').prop('disabled', false);
            }else if($(this).val() == 2){
                $('#max_nominal').prop('disabled', true);
                $('#values').prop('disabled', false);
            }else{
                $('#max_nominal').prop('disabled', true);
                $('#values').prop('disabled', true);
            }
        });
    </script>

    {{-- save data --}}
    <script>
        $(document).ready(function() {
            $('.voucer_type').select2();
            $('.js-example-basic-single').select2();
        });

        $(document).on('click', '#saved_voucher', function(e) {
            // Helper.confirm(function(){
                id = $('input[name="id"]').val(),
                Helper.loadingStart();
                $.ajax({
                    url:Helper.apiUrl('/admin/voucher-general/update/' + id),
                    type: 'post',
                    data : {
                        voucher_type : $('select[name="voucher_type"]').val(),
                        type : $('select[name="type"]').val(),
                        id : $('input[name="id"]').val(),
                        value : $('input[name="value"]').val(),
                        max_nominal : $('input[name="max_nominal"]').val(),
                        min_payment : $('input[name="min_payment"]').val(),
                        valid_at : $('input[name="valid_at"]').val(),
                        valid_until : $('input[name="valid_until"]').val(),
                        desc : $('textarea[name="desc"]').val(),
                    },

                    success: function(res) {
                        Helper.successNotif('Success');
                        Helper.redirectTo('/admin/general-voucher/show');
                    },
                    error: function(xhr, status, error) {
                        if(xhr.status == 422){
                            error = xhr.responseJSON.data;
                            _.each(error, function(pesan, field){
                                $('#error-'+ field).text(pesan[0])
                            })
                        }
                        Helper.loadingStop();
                    }
                })
            // })
            e.preventDefault()
        })
        
    </script>
@endsection
