@extends('admin.home')
@section('content')
<style>
    .tgl {
        display: none;
    }

    .tgl,
    .tgl:after,
    .tgl:before,
    .tgl *,
    .tgl *:after,
    .tgl *:before,
    .tgl+.tgl-btn {
        box-sizing: border-box;
    }

    .tgl::-moz-selection,
    .tgl:after::-moz-selection,
    .tgl:before::-moz-selection,
    .tgl *::-moz-selection,
    .tgl *:after::-moz-selection,
    .tgl *:before::-moz-selection,
    .tgl+.tgl-btn::-moz-selection {
        background: none;
    }

    .tgl::selection,
    .tgl:after::selection,
    .tgl:before::selection,
    .tgl *::selection,
    .tgl *:after::selection,
    .tgl *:before::selection,
    .tgl+.tgl-btn::selection {
        background: none;
    }

    .tgl+.tgl-btn {
        outline: 0;
        display: block;
        width: 7em;
        height: 2em;
        position: relative;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .tgl+.tgl-btn:after,
    .tgl+.tgl-btn:before {
        position: relative;
        display: block;
        content: "";
        width: 50%;
        height: 100%;
    }

    .tgl+.tgl-btn:after {
        left: 0;
    }

    .tgl+.tgl-btn:before {
        display: none;
    }

    .tgl:checked+.tgl-btn:after {
        left: 50%;
    }


    .tgl-skewed+.tgl-btn {
        overflow: hidden;
        -webkit-transform: skew(-10deg);
        transform: skew(-10deg);
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        -webkit-transition: all .2s ease;
        transition: all .2s ease;
        font-family: sans-serif;
        background: #888;
    }

    .tgl-skewed+.tgl-btn:after,
    .tgl-skewed+.tgl-btn:before {
        -webkit-transform: skew(10deg);
        transform: skew(10deg);
        display: inline-block;
        -webkit-transition: all .2s ease;
        transition: all .2s ease;
        width: 100%;
        text-align: center;
        position: absolute;
        line-height: 2em;
        font-weight: bold;
        color: #fff;
        text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);
    }

    .tgl-skewed+.tgl-btn:after {
        left: 100%;
        content: attr(data-tg-on);
    }

    .tgl-skewed+.tgl-btn:before {
        left: 0;
        content: attr(data-tg-off);
    }

    .tgl-skewed+.tgl-btn:active {
        background: #888;
    }

    .tgl-skewed+.tgl-btn:active:before {
        left: -10%;
    }

    .tgl-skewed:checked+.tgl-btn {
        background: #1976d2;
    }

    .tgl-skewed:checked+.tgl-btn:before {
        left: -100%;
    }

    .tgl-skewed:checked+.tgl-btn:after {
        left: 0;
    }

    .tgl-skewed:checked+.tgl-btn:active:after {
        left: 10%;
    }
</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
               {{-- <button class="btn btn-primary btn-sm add-bank-transfer">Back</button> --}}
            </div>
        </div>

        <div class="card-body">
            {{-- <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Voucher Code</label>
                <div class="col-sm-9">
                    <input name="code" placeholder="VCR-{{ date('y-m-d') }}-Your code" type="text" class="form-control">
                </div>
            </div> --}}

            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Type</label>
                <div class="col-sm-9">
                    <select class="js-example-basic-single" id="type_voucher" name="type" style="width:100%">
                        <option value="">Type</option>
                        <option value="1">Percent (%)</option>
                        <option value="2">Fix (Rp)</option>
                    </select>
                    <strong><span id="error-type" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Value</label>
                <div class="col-sm-9">
                    <input name="value" placeholder="" type="text" id="values" class="form-control" disabled>
                    <strong><span id="error-value" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Max Nominal</label>
                <div class="col-sm-9">
                    <input name="max_nominal" placeholder="" type="text" id="max_nominal" class="form-control" disabled>
                    <strong><span id="error-max_nominal" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Minimal Payment</label>
                <div class="col-sm-9">
                    <input name="min_payment" placeholder="" type="text" class="form-control">
                    <strong><span id="error-min_payment" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Valid At</label>
                <div class="col-sm-9">
                    <input name="valid_at" id="datetimepicker1" placeholder="" type="text" class="form-control">
                    <strong><span id="error-valid_at" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Valid Until</label>
                <div class="col-sm-9">
                    <input name="valid_until" id="datetimepicker2" placeholder="" type="text" class="form-control">
                    <strong><span id="error-valid_until" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label" >Desc</label>
                <div class="col-sm-9">
                    <textarea name="desc" id="" cols="30" rows="10" class="form-control"></textarea>
                    <strong><span id="error-desc" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label" >Active/No Active This Vouchers</label>
                <div class="col-sm-9">
                    <input name="status" class="tgl tgl-skewed" id="cb3" type="checkbox" checked />
                    <label class="tgl-btn" data-tg-off="Inactive" data-tg-on="Active" for="cb3"></label>
                </div>
            </div>
            <div class="position-relative row form-check">
            <div class="col-sm-10 offset-sm-2">
                <button class="btn btn-success" id="saved_voucher" style="float:right"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
            </div>
        </div><br>
        </div>
    </div>
</div>
@endsection

@section('script')
    <link rel="stylesheet" type="text/css" href="{{ asset('/datePicker/jquery.datetimepicker.css') }}" >
    <script src="{{ asset('/datePicker/jquery.datetimepicker.full.min.js') }}"></script>

    {{-- date picker --}}
    <script>
        $(document).ready(function(){
            jQuery('#datetimepicker1').datetimepicker({
                format:'Y-m-d',
                mask:true
            });
        });

        $(document).ready(function(){
            jQuery('#datetimepicker2').datetimepicker({
                format:'Y-m-d',
                mask:true
            });
        });
    </script>

    {{-- append type --}}
    <script>
        $('#type_voucher').change(function(){
            if( $(this).val() == 1){
                $('#max_nominal').prop('disabled', false);
                $('#values').prop('disabled', false);
            }else if($(this).val() == 2){
                $('#max_nominal').prop('disabled', true);
                $('#values').prop('disabled', false);
            }else{
                $('#max_nominal').prop('disabled', true);
                $('#values').prop('disabled', true);
            }
        });
    </script>

    {{-- save data --}}
    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });

        $(document).on('click', '#saved_voucher', function(e) {
            // Helper.confirm(function(){
                Helper.loadingStart();
                $.ajax({
                    url:Helper.apiUrl('/admin/new-user-voucher/create'),
                    type: 'post',
                    data : {
                        type : $('select[name="type"]').val(),
                        value : $('input[name="value"]').val(),
                        max_nominal : $('input[name="max_nominal"]').val(),
                        min_payment : $('input[name="min_payment"]').val(),
                        valid_at : $('input[name="valid_at"]').val(),
                        valid_until : $('input[name="valid_until"]').val(),
                        desc : $('textarea[name="desc"]').val(),
                        status : $('#cb3').is(":checked") ? 1 : 0,
                    },

                    success: function(res) {
                        Helper.successNotif('Success');
                        Helper.redirectTo('/admin/new-register-voucher/show');
                    },
                    error: function(xhr, status, error) {
                        if(xhr.status == 422){
                            error = xhr.responseJSON.data;
                            _.each(error, function(pesan, field){
                                $('#error-'+ field).text(pesan[0])
                            })
                        }
                        Helper.loadingStop();
                    }
                })
            // })
            e.preventDefault()
        })
    </script>
@endsection
