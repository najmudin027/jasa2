@extends('admin.home')
@section('content')
<style>
    .btn:focus,
    .btn:active,
    button:focus,
    button:active {
        outline: none !important;
        box-shadow: none !important;
    }

    #image-gallery .modal-footer {
        display: block;
    }

    .thumb {
        margin-top: 15px;
        margin-bottom: 15px;
    }

    .upload-description-header {
        color: red;
        font-weight: 800;
    }

    #upload {
        width: 500px;
        height: 250px;
        margin: auto;
        margin-bottom: 25px;
        margin-top: 25px;
        padding: 25px;
        border: 2px dashed #028AF4;
    }

    .upload-description-content {
        list-style-type: decimal;
    }

    .Portfolio {
        position: relative;
        margin: 5px;
        border: 2px solid black;
        float: left;
        width: 180px;
        transition-duration: 0.4s;
        border-radius: 5px;
        animation: winanim 0.5s;
        -webkit-backface-visibility: visible;
        backface-visibility: visible;
        box-shadow: 0 3px 5px -1px rgba(0, 0, 0, .2), 0 5px 8px 0 rgba(0, 0, 0, .14), 0 1px 14px 0 rgba(0, 0, 0, .12)
    }

    .Portfolio:hover {
        box-shadow: 0 12px 16px 0 rgba(0, 0, 0, .24), 0 17px 50px 0 rgba(0, 0, 0, .19);
    }

    .Portfolio img {
        width: 100%;
        height: auto;
        border-radius: 5px
    }
</style>
<div class="col-md-12">
    @if(session()->has('error'))
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>{{ session()->get('error') }} </strong>
    </div>
    @endif
    <div class="tab-content">
        <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
            <div class="row">
                <div class="col-md-12">
                    <div class="card-header-tab card-header">
                        <div class="card-header-title">
                            {{-- <i class="header-icon lnr-bicycle icon-gradient bg-love-kiss"> </i> --}}
                            GALLERY IMAGES
                        </div>
                        <ul class="nav">
                            <li class="nav-item"><a data-toggle="tab" href="#tab-eg5-0" class="active nav-link">Upload Images</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#tab-eg5-1" class="nav-link">Gallery</a></li>
                        </ul>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-eg5-0" role="tabpanel">
                                    <form id="form-upload-galery" enctype="multipart/form-data">
                                        <div class="col-md-12">
                                            {{-- <b>Upload file information media about the description of symptoms <span class="text-danger">(max 4 files image/video)</span></b>. --}}

                                            <input type="file" name="filename[]" id="filer_input2" multiple="multiple">
                                            <hr>
                                        </div>
                                        <div class="col-md-12">
                                            <button class="btn btn-primary waves-effect btn-block btn_upload" id="save_images" type="button">
                                                Upload
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="tab-eg5-1" role="tabpanel">

                                    @if($viewGalery->count() < 1 ) <h3 align="center">Image Empty</h3>
                                        <p align="center">please select the Upload Image tab to upload images</p>
                                        @endif
                                        <div class="container">
                                            <div class="row">
                                                @if($viewGalery->count() > 0 )
                                                <button type="button" data-type="0" id="active" class="btn btn-primary" id="save" style="float:left">&nbsp;Open Delete Multiple</button>&nbsp;&nbsp;
                                                <button type="submit" id="deleteImage" class="btn btn-danger" style="float:left">&nbsp;Delete</i></button>
                                                @endif
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row" id="load">
                                            <div class="row">
                                                {{-- <u>
                                                    @foreach ($viewGalery as $item)
                                                        <i>
                                                            @include('admin.master.galeryImage.styleImageClick')
                                                            <input type="checkbox" data-image-id="{{ $item->id }}" name="deleteImage" class="delete" id="myCheckbox{{ $item->id }}" hidden>
                                                <label for="myCheckbox{{ $item->id }}">
                                                    <img src="{{url('admin/storage/images/' . $item->filename)}}" class="update-image edit-detail-image" id="filename" data-id="{{ $item->id }}" style="width:126px;height:126px"><br>
                                                </label>
                                                <input type="hidden" name="filename" value="{{ $item->filename }}">
                                                </i>
                                                @endforeach
                                                </u> --}}
                                                @include('admin.master.galeryImage.styleImageClick')
                                                <ul class="jFiler-items-list jFiler-items-grid">
                                                    <u>
                                                        @foreach($viewGalery as $item)
                                                        <i class="jFiler-item jFiler-no-thumbnail" data-jfiler-index="3" style="">
                                                            <div class="jFiler-item-container">
                                                                <div class="jFiler-item-inner">
                                                                    <div class="jFiler-item-thumb">
                                                                        <input type="checkbox" data-image-id="{{ $item->id }}" name="deleteImage" class="delete" id="myCheckbox{{ $item->id }}" hidden>
                                                                        <label for="myCheckbox{{ $item->id }}">
                                                                            <div class="jFiler-item-status"></div>
                                                                            {{-- <div class="jFiler-item-thumb-overlay">
                                                                                        <div class="jFiler-item-info">
                                                                                            <div style="display:table-cell;vertical-align: middle;">
                                                                                                <span class="jFiler-item-title">
                                                                                                    <b title="">asd</b>
                                                                                                </span>
                        
                                                                                            </div>
                                                                                        </div>
                                                                                    </div> --}}
                                                                            <div class="jFiler-item-thumb-image">
                                                                                <img src="{{url('admin/storage/images/' . $item->filename)}}" class="update-image edit-detail-image" data-id="{{ $item->id }}" draggable="false">
                                                                            </div>
                                                                        </label>
                                                                        <input type="hidden" name="filename" value="{{ $item->filename }}">
                                                                    </div>
                                                                    <div class="jFiler-item-assets jFiler-row">
                                                                        <ul class="list-inline pull-left">
                                                                            <li></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </i>
                                                        @endforeach
                                                    </u>

                                                </ul>
                                            </div>

                                        </div>
                                        @if($viewGalery->count() >= 12 )
                                        <div style="float:right">
                                            <a href="http://" id="loadMore" type="button" data-page="1">Load More. . . </a>
                                        </div>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.master.galeryImage.modalDetailImage')
@endsection
<style>
    .jFiler-input-dragDrop {
        margin: 0 0 0 0;
    }
</style>
@include('admin.script.master._galeryScript')

@section('script')
<link rel="stylesheet" href="{{ asset('adminAssets/css/jquery.filer.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/themes/jquery.filer-dragdropbox-theme.css') }}">
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/jquery.filer.min.js') }}"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script>
    // $(document).on('click', '.edit-detail-image', function() {
    //     $('.hamburger--elastic').addClass('is-active');
    //     $('.fixed-sidebar').addClass('closed-sidebar');
    //     $('#modal-update-image').modal('show');
    // })

    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })
    // modal show
    var $modal = $('#modal-update-image').modal({
        show: false
    });

    var ProductAdditionalObj = {

        // update data
        update: function(data) {
            id = $('input[name="id"]').val()
            $.ajax({
                url: Helper.apiUrl('/gallery-image/' + id),
                data: data,
                type: 'put',
                success: function(resp) {
                    ProductAdditionalObj.successHandle(resp);
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            })
        },
        // isi field input
        isiDataFormModal: function(id) {
            $.ajax({
                url: Helper.apiUrl('/gallery-image/' + id),
                type: 'get',
                success: function(resp) {
                    console.log(resp)
                    $("#alternatif_name").val(resp.data.alternatif_name);
                    // $('#imageArea').html('<img src="'+ resp.data.src +'" style="width:380px;height:380px"/>');
                    $('#imageArea').html('<center><img src="' + resp.data.src + '" class="update-image edit-detail-image" draggable="true" style="width:400px;"></center>');
                    $('#image_url').val(resp.data.src);
                    $("#filename").val(resp.data.filename);
                    $("#title").val(resp.data.title);
                    $("#desc").val(resp.data.desc);
                    $("#id").val(resp.data.id);
                    $('input[name="deleteImage"]').each(function() {
                        console.log('bisa')
                        $(this).prop('checked', false)
                    })
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            })

        },
        // hadle ketika response sukses
        successHandle: function(resp) {
            // send notif
            Helper.successNotif(resp.msg);
            // close modal
            $modal.modal('hide');
        },
    }
</script>

<script>
    $('#deleteImage').hide()
    $('#active')
        .click(function() {
            type = $(this).attr('data-type');
            if (type == 0) {
                type = false;
                type_baru = 1;
                // alert('Delete Multiple Hass Been Active')
                $('#deleteImage').show()
            } else {
                type = true;
                type_baru = 0;
                // alert('Delete Multiple Non Active')
                $('#deleteImage').hide()
            }
            $('input[name="deleteImage"]').each(function() {
                $(this).attr('disabled', type);
                if (type == true) {
                    $(this).prop('checked', false)
                }
            })

            $(this).attr('data-type', type_baru).text(type_baru == 1 ? 'Close Delete Multiple' : 'Open Delete Multiple');
        })

    $('.delete')
        .change(function() {
            var id = $('.delete:checked').map(function() {
                return $(this).attr('data-image-id')
            }).get()
            console.log(id)
        });


    $('.update-image')
        .on('click', function() {
            types = $('#active').attr('data-type')

            if (parseInt(types) == 0) {
                types == false
                $modal.modal('show');
            }
        });


    $('#deleteImage')
        .click(function() {
            var id = $('.delete:checked').map(function() {
                return $(this).attr('data-image-id')
            }).get()
            var name = $('.delete:checked').map(function() {
                return $(this).attr('data-image-name')
            }).get()
            $.ajax({
                url: Helper.frontApiUrl('/gallery-image/image/delete'),
                type: 'post',
                data: {
                    id
                },
                success: function(response) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Image Has Been Deleted',
                    });
                    window.location.href = Helper.redirectUrl('/admin/gallery-image/show-main');
                },
                error:function(){
                    Helper.errorNotif('wtf is going on');
                }

            });
        });


    // $('#save')
    //     .click(function(e){
    //         swal.fire({
    //             type: 'success',
    //             title: 'Images Has Been Saved',
    //             icon: "success",
    //         });
    //         window.location.href = Helper.redirectUrl('/admin/gallery-image/show-main');
    //     });

    function save(e) {
        var form = new FormData($('#form-upload-galery')[0]);
        console.log(form)
        Helper.loadingStart();
        Axios.post(Helper.apiUrl('/admin/galery/upload'), form, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                Helper.successNotif('Success, Successfully submit offer !');
                window.location.href = Helper.redirectUrl('/admin/gallery-image/show-main');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    }

    $(document).on('click', '#save_images', function(e) {
        save(e);
    });

    $(document).on('click', '#filer_input2', function(e) {
        $('#save_images').prop('disabled', false);
    });

    $(document).ready(function(e) {
        $('#save_images').prop('disabled', true);
    });



    $('#form-detail-image').submit(function(e) {
        var data = {};
        console.log(data)

        $.each($(this).serializeArray(), function(i, field) {
            data[field.name] = field.value;
        });

        if (data.id != '') {} else {
            ProductAdditionalObj.update(data);
        }

        e.preventDefault();
    })

    $(document)
        .on('click', ".edit-detail-image", function() {
            id = $(this).attr('data-id');
            types = $('#active').attr('data-type')

            if (parseInt(types) == 0) {
                types = false
                ProductAdditionalObj.isiDataFormModal(id);
                $modal.modal('show');

            }

        })
</script>

<script>
    $(function() {

        $(document).on('click', '#loadMore', function(e) {
            e.preventDefault();
            var page = parseInt($(this).attr('data-page')) + 1;
            var url = Helper.redirectUrl('/admin/gallery-image/show-main?page=' + page)
            console.log(url)

            $(this).attr('data-page', page)
            // $('#load a').css('color', '#dfecf6');
            // $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 1000;" src="https://i.gifer.com/YCZH.gif" />');

            getArticles(url);
        });

        function getArticles(url) {
            $.ajax({
                url: url
            }).done(function(data) {
                console.log(data)
                $('#load').append(data);
            }).fail(function() {
                alert('Articles could not be loaded.');
            });
        }
    });
</script>

@endsection