<form id="form-additional-product">
    <div class="modal fade" id="modal-additional-product" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Additional Product</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Additional Code</label>
                    <div class="form-group">
                        <input name="additional_code" placeholder="additional code" id="additional_code" type="text" class="form-control">
                    </div>
                    <span id="error-additional_code" style="color:red"></span>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>