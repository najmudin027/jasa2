@extends('admin.home')
@section('content')
<form id="form-product" style="display: contents">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="card card">
                        <div class="header-bg card-header">
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                {{ $title }}
                            </div>
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="id" id="id" value="{{ $product->id }}">
                            <div class="position-relative form-group">
                                <label>Product Name</label>
                                <input name="name" id="name" value="{{ $product->name }}" placeholder="Product Name" type="text" class="form-control" onload="convertToSlug(this.value)" onkeyup="convertToSlug(this.value)" required>
                            </div>

                            <div class="position-relative form-group">
                                <label>Name Slug</label>
                                <div class="input-group">
                                    <input name="slug" id="to_slug" value="{{ $product->slug }}" placeholder="Product Slug" type="text" class="form-control" required>
                                </div>
                            </div>

                            <div class="position-relative form-group" style="display: none;">
                                <label>Product SKU</label>
                                <input name="sku" id="sku" value="{{ $product->sku }}" placeholder="Product sku" type="text" class="form-control">
                            </div>

                            <div class="position-relative form-group">
                                <label>Product Category</label>
                                <select class="form-control select-category" name="product_category_id[]" style="width:100%" multiple>
                                    @foreach($product->productCombineCategories as $combineC)
                                    <option value="{{ $combineC->product_category->id }}" selected>{{ $combineC->product_category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="position-relative form-group">
                                <label>Product Model</label>
                                <select class="form-control select-model" id="product_model_id" name="product_model_id" style="width:100%">
                                    <option value="{{ $product->model->id }}" selected>{{ $product->model->name }}</option>
                                </select>
                            </div>

                            <div class="position-relative form-group">
                                <label>Product Additional</label>
                                <select class="form-control select-additonal-product" id="product_additionals_id" name="product_additionals_id" style="width:100%">
                                    <option value="{{ $product->additional->id }}" selected>{{ $product->additional->additional_code }}</option>
                                </select>
                            </div>

                            <div class="position-relative form-group">
                                <label>Product Description</label>
                                <textarea name="product_description" id="product_description" class="form-control">{{ $product->deskripsi }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="header-border card">
                        <div class="card-body" id="attribute-product-area">
                            <div class="position-relative form-group" style="display: none;">
                                <label>Stock</label>
                                <input name="header_product_stock" id="header_product_stock" placeholder="Stock" type="text" class="form-control">
                            </div>
                            <div class="form-row">
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label>Length (cm)</label>
                                        <div class="input-group">
                                            <input name="header_product_length" placeholder="Length" type="number" class="form-control" value="{{ $product->length }}">
                                            <div class="input-group-append"><span class="input-group-text">cm</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label>Width (cm)</label>
                                        <div class="input-group">
                                            <input name="header_product_width" placeholder="Width" type="number" class="form-control" value="{{ $product->height }}">
                                            <div class="input-group-append"><span class="input-group-text">cm</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label>Height (cm)</label>
                                        <div class="input-group">
                                            <input name="header_product_height" placeholder="Height" type="number" class="form-control" value="{{ $product->weight }}">
                                            <div class="input-group-append"><span class="input-group-text">cm</span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label>Weight (gram)</label>
                                        <div class="input-group">
                                            <input name="header_product_weight" placeholder="weight" type="number" class="form-control" value="{{ $product->width }}">
                                            <div class="input-group-append"><span class="input-group-text">gram</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @include('admin.master.product.products._attribute_header', [
                            'product' => $product,
                            'uniq' => $product->created_at->format('Y-m-d') . $product->id
                            ])
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-secondary" type="button" id="btn-add-attribute-product">
                                <i class="fa fa-plus"></i> Add Attribute
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 20px;" id="product-vendor-area">
                    @foreach($product->productVendors as $product_vendor)
                    @include('admin.master.product.products._product_vendor', [
                    'product_vendor' => $product_vendor,
                    'uniq' => $product_vendor->created_at->format('Y-m-d') . $product_vendor->id
                    ])
                    @endforeach
                </div>
                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <div class="position-relative row form-check">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="remove btn btn-sm btn-secondary mr-2" type="button" id="btn-add-product-vendor" style="float:left">
                                            <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Product Vendor
                                        </button>
                                        <button class="btn btn-sm btn-primary" type="submit">
                                            <i class="fa fa-save"></i>&nbsp;&nbsp;Save
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="main-card col-md-3 card prodInfo header-border" style="position:fixed;height:auto;">
                    <div class="card-body">
                        <div class="position-relative form-group">
                            <label>Publish At</label>
                            <input type="date" class="form-control" name="publish_at" width="100%" value="{{ $product->publish_at->format('Y-m-d') }}" />
                        </div>

                        <div class="position-relative form-group">
                            <label>Status</label>
                            <select class="select-product-status" name="product_status_id" style="width:100%">
                                <option value="{{ $product->status->id }}" selected>{{ $product->status->name }}</option>
                            </select>
                        </div>

                        <div class="position-relative form-group">
                            <label>Main Images</label>
                            <input type="hidden" name="galery_image_id" id="galery_image_id" value="{{ $product->galery_image_id }}">
                            <br>
                            <span class="input-group-btn">
                                <a href="" onclick="window.open('/admin/gallery-image/show-gallery?type=main', 'newwindow', 'width=1000,height=600'); return false;" target="_blank"><i class="fa fa-image"></i> Add Image</a>
                            </span>
                            <div id="mainImageArea">
                                <img class="img-thumbnail" src="{{ isset($product->gallery->src ) ? $product->gallery->src : '' }}" style="width:70%">
                            </div>
                        </div>


                        <div class="position-relative form-group">
                            <label>Galery Images</label>
                            <br>
                            <span class="input-group-btn">
                                <a href="" onclick="window.open('/admin/gallery-image/show-gallery?type=galery', 'newwindow', 'width=1000,height=600'); return false;" target="_blank"><i class="fa fa-upload"></i> Add Image</a>
                            </span>
                            <div class="scroll-area-sm">
                                <div class="scrollbar-container ps--active-y">
                                    <div class="container">
                                        <div class="row">
                                            <div class="row" id="galeryArea">
                                                @foreach($product->product_images as $image)
                                                <?php if ($image->gallery != null): ?>
                                                    <div class="col-lg-6 col-md-4 col-xs-6 img-thumb-{{ $image->gallery->id }}">

                                                        <img class="img-thumbnail" src="{{ $image->gallery->src }}" data-image-id="{{ $image->gallery->id }}" alt="Another alt text">

                                                        <div class="middle">
                                                            <div class="text">
                                                                <button class="btn btn-danger removeImage" data-img="{{ $image->gallery->id }}">
                                                                    <i class="fa fa-close"></i>
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                <?php endif ?>                                                
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary" type="submit" style="width:100%">
                            <i class="fa fa-save"></i> Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
@include('admin.master.product.products._js')
<script>
    // start
    ProductObj.initEdit();

    // delete vendor
    $(document)
        .on('click', '.btn-delete-vendor', function() {
            uniq = $(this).attr('data-uniq');
            product_vendor_id = $(this).attr('data-id');

            console.log('product_vendor_id', product_vendor_id)
            ProductObj.productVendorDelete(uniq, product_vendor_id);
        })

    // delete varian
    $(document)
        .on('click', '.btn-delete-varian', function() {
            uniq = $(this).attr('data-uniq');
            uniq_varian = $(this).attr('data-uniq_varian');
            varian_id = $(this).attr('data-varian_id');
            ProductObj.deleteVarian(uniq, uniq_varian, varian_id);
        })

    // ganti attribute header
    $(document)
        .on('change', ".select-header-attribute", function(e) {
            uniq = $(this).attr('data-uniq');
            areaAppendTerm = '.select-header-term-area-' + uniq;

            att_id = $(this).val();
            checkDobel = ProductObj.checkDobelAttribute(att_id);

            console.log(checkDobel)
            if (checkDobel == 1) {
                attribute = _.find(_attributes, item => (item.id == att_id))
                ProductObj.appendTerm(areaAppendTerm, attribute.childs, uniq);
            } else {
                $('.attribute-form-uniq-' + uniq).remove();
                Helper.errorNotif('attribute sudah dipilih');
            }
        });

    // add btn tambah attribte
    // $('#btn-add-attribute-product')
    //     .click(function() {
    //         total_attribute = _attributes.length;
    //         current_attribute = $('.select-header-attribute').length;
    //         if (total_attribute > current_attribute) {
    //             $('#attribute-product-area')
    //                 .append(TemplateObj.attributeSetSelect(_attributes));
    //         } else {
    //             Helper.errorNotif('melebihi total attribute');
    //         }
    //     })


    // submit form
    $('#form-product').submit(function(e) {
        data = {
            id: $('#id').val(),
            name: $('#name').val(),
            product_sku: $('#sku').val(),
            publish_at: $('input[name="publish_at"]').val(),
            product_description: $('#product_description').val(),
            product_model_id: $('#product_model_id').val(),
            product_status_id: $('.select-product-status').val(),
            product_additionals_id: $('#product_additionals_id').val(),
            product_category_id: $('.select-category').val(),
            product_header_attribute: ProductObj.getProductAttHeaderValue(),
            product_stock: $('input[name="header_product_stock"]').val(),
            product_length: $('input[name="header_product_length"]').val(),
            product_width: $('input[name="header_product_width"]').val(),
            product_height: $('input[name="header_product_height"]').val(),
            product_weight: $('input[name="header_product_weight"]').val(),
            product_slug: $('input[name="slug"]').val(),
            vendors: ProductObj.getVendorValue(),
            product_images: getImageSelected(),
            galery_image_id: $('#galery_image_id').val()
        }

        console.log('update', data);
        ProductObj.update(data);
        e.preventDefault();
    })
</script>

@endsection