@extends('admin.home')
@section('content')
<div class="col-md-12 card">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            {{ $title }}
        </div>
        <div class="btn-actions-pane-right text-capitalize">
            <a href="{{ url('/show-list-product') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-arrow-circle-left"></i> Add Product Model</a>
        </div>
    </div>
    <div class="card-body">
        <div class="position-relative row form-group">
            <label for="exampleEmail" class="col-sm-2 col-form-label">Product Name</label>
            <div class="col-sm-4">
                <input  placeholder="Product Name" value=" {{ $showDataProd->name }} " type="text" class="form-control" disabled>
            </div>

            <label for="exampleEmail" class="col-sm-2 col-form-label">Product Models</label>
            <div class="col-sm-4">
                <input  placeholder="Product Models" value=" {{ $showDataProd->model->name }} " type="text" class="form-control" disabled>
            </div>
        </div>
        <div class="position-relative row form-group">
            <label for="exampleEmail" class="col-sm-2 col-form-label">Product Category</label>
            <div class="col-sm-4">
                <input  placeholder="Product Name" value=" Test " type="text" class="form-control" disabled>
            </div>

            <label for="exampleEmail" class="col-sm-2 col-form-label">Product Additionals</label>
            <div class="col-sm-4">
                <input  placeholder="Product Models" value="asd asd asd" type="text" class="form-control" disabled>
            </div>
        </div>
        <div class="position-relative row form-group">
            <label for="exampleEmail" class="col-sm-2 col-form-label">Product Additionals</label>
            <div class="col-sm-10">
                <textarea class="form-control" id="summary-ckeditor" name="asd">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea dolorum aperiam autem nihil cumque eveniet labore! Perferendis, eaque cumque! Unde nobis possimus officia laborum iste incidunt, dolores cupiditate laudantium nulla.
                </textarea>
                {{-- <input  placeholder="Product Additionals" type="text" class="form-control"> --}}
            </div>
        </div>
        <hr>
        <div class="card-body">
            @foreach ($showDataVendor->product_vendor as $a => $val)
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal" align="center">
                    Detail Vendors
                </div><br>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Vendor Name </label>
                    <div class="col-sm-4">
                        <input  placeholder="vendor Name" value=" {{ $val->vendor->name }} " type="text" class="form-control" disabled>
                    </div>

                    <label for="exampleEmail" class="col-sm-2 col-form-label">Vendor Code</label>
                    <div class="col-sm-4">
                        <input  placeholder="vendor Code" value=" {{ $val->vendor->code }} " type="text" class="form-control" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Part Code</label>
                    <div class="col-sm-4">
                        <input  placeholder="Part Code" type="text" value=" {{ $val->part_code }} " class="form-control" disabled>
                    </div>
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Brand Code</label>
                    <div class="col-sm-4">
                        <input  placeholder="Brand Code" value=" {{ $val->brand_code }} " type="text" class="form-control" disabled>
                    </div>
                </div><br>
                <table id="vendorTables" class="table table-hover table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr align="center">
                            <td colspan="3">Product Attr</td>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach ($val->product_varian as $b => $varian)
                        <tr>
                            @foreach ($varian->product_varian_attribute as $b => $attr)
                            
                                <td>{{ $attr->attribute->name }} &nbsp;&nbsp; : &nbsp; {{ $attr->term->name }}</td>
                                
                            
                            @endforeach
                            <td>Stock : &nbsp;&nbsp;{{ $varian->stock }}</td>
                        </tr>
                        
                    @endforeach
                    </tbody>
                </table>
                {{-- @foreach ($val->product_varian as $b => $values)
                    @foreach ($values->product_varian_attribute as $c => $items) {{ $items->attribute->name }} --}}
                        <div class="position-relative row form-group col-md-12">
                            {{-- <label for="exampleEmail" class="col-sm-2 col-form-label">Product Attribute</label>
                            <div class="col-sm-4">
                                <input  placeholder="Product Attribute" value="  " type="text" class="form-control" disabled>
                            </div>
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Product Attribute Term</label>
                            <div class="col-sm-4">
                                <input  placeholder="Product Attribute Term" value="  " type="text" class="form-control" disabled>
                            </div> --}}
                            
                        </div>
                    {{-- @endforeach
                @endforeach --}}
                <hr>
            @endforeach
        </div>
    </div>
</div>
<div class="d-block text-center card-footer"></div>




    @include('admin.template._mainScript')
    @include('admin.script.master._productScript')
@endsection
