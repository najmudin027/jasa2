@extends('admin.home')
@section('content')
<style>
    label.btn span {
    font-size: 1.2em ;
    }
    label input[type="checkbox"] ~ i.fa.fa-square-o{
        color: #c8c8c8;    display: inline;
    }
    label input[type="checkbox"] ~ i.fa.fa-check-square-o{
        display: none;
    }
    label input[type="checkbox"]:checked ~ i.fa.fa-square-o{
        display: none;
    }
    label input[type="checkbox"]:checked ~ i.fa.fa-check-square-o{
        color: #7AA3CC;    display: inline;
    }
</style>
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-8">
                <div class="main-card mb-3 card">
                    <div class="card-header">Create Productsasd</div>
                    <div class="table-responsive"><br>
                        <div class="container">
                            <a href="{{ url('/show-list-product') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
                            <div class="card-body"><h5 class="card-title" style="color:white">Update Role</h5>
                                <form action="" id="store">

                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Product Name</label>
                                        <div class="col-sm-10">
                                            <input name="name" id="examplename" placeholder="Product Name" type="text" class="form-control">
                                        </div>
                                    </div>
                                    {{-- <input type="hidden" name="vendor_have_varian" value="2"> --}}
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Product zxczxzxcz</label>
                                        <div class="input-group col-md-10">
                                            <select class="js-example-basic-single" id="model_id" name="product_model_id" style="width:100%">
                                                <option selected-disabled>Select Model</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Product Additional</label>
                                        <div class="input-group col-md-10">
                                            <select class="js-example-basic-single" id="product_additionals_id" name="product_additionals_id" style="width:100%">
                                                <option selected-disabled>Select Model</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="" id="product-info-area">

                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Product Additionalssssads</label>
                                        <div class="input-group col-md-10">
                                            <button class="remove btn btn-outline-secondary" type="button" id="btn-add-product-info" style="float:left">
                                                <i class="fa fa-image"></i>&nbsp;&nbsp;Add
                                            </button>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-check">
                                        <div class="col-sm-10 offset-sm-2" style="float:right">
                                            
                                        </div><br>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <button class="remove btn btn-outline-secondary" type="button" id="btn-add-product-info" style="float:left">
                                                        <i class="fa fa-plus"></i>&nbsp;&nbsp;Add
                                                    </button>
                                                </div>
                                                <div class="col-md-6">
                                                    <button class="btn btn-success" id="save" style="float:right">
                                                        <i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Save Data
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="d-block text-center card-footer">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.template._mainScript')
    @include('admin.script.master._productScript')
@endsection
