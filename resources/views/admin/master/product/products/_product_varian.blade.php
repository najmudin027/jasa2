<div class="header-border main-card mb-3 card varian-uniq-{{ $uniq }}-{{ $uniq_varian }}">
    <div class="card-header">
        VENDOR VARIAN {{ $uniq }}.{{ $uniq_varian }}
        <div class="btn-actions-pane-right">
            <div role="group" class="btn-group-sm btn-group">
                <button data-uniq_varian="{{ $uniq_varian }}" data-uniq="{{ $uniq }}" data-varian_id="{{ $varian->id }}" class="btn btn-sm btn-danger btn-delete-varian btn-delete-varian-uniq-{{ $uniq }}-{{ $uniq_varian }}" type="button">
                    <i class="fa fa-remove"></i> Delete
                </button>
                <button data-uniq_varian="{{ $uniq_varian }}" data-uniq="{{ $uniq }}" data-hide="1" class="btn btn-sm btn-white btn-warning btn-hide-varian btn-hide-varian-uniq-{{ $uniq }}-{{ $uniq_varian }}" type="button">
                    Hide
                </button>
            </div>
        </div>
    </div>
    <div class="card-body varian-body-uniq-{{ $uniq }}-{{ $uniq_varian }}">
        <input type="hidden" name="product_varian_id[]" class="varian-id-{{ $uniq }} varian_id_input-{{ $uniq }}-{{ $uniq_varian }}" value="{{ $varian->id }}">
        <div class="position-relative form-group">
            <label>Varian SKU</label>
            <input name="varian_sku[]" placeholder="varian_sku" value="{{ $varian->sku }}" type="text" data-uniq_varian="{{ $uniq_varian }}" class="form-control varian-sku-{{ $uniq }} varian_sku_input-{{ $uniq }}-{{ $uniq_varian }}">
        </div>
        <div class="position-relative form-group" style="display: none;">
            <label>Stock</label>
            <input name="stock[]" placeholder="stock" value="{{ $varian->stock }}" type="text" data-uniq_varian="{{ $uniq_varian }}" class="form-control varian_stock_input-{{ $uniq }}-{{ $uniq_varian }}">
        </div>
        <div class="position-relative form-group">
            <label>Length</label>
            <input name="varian_length[]" placeholder="length" value="{{ $varian->length }}" type="text" data-uniq_varian="{{ $uniq_varian }}" class="form-control varian_length_input-{{ $uniq }}-{{ $uniq_varian }}">
        </div>
        <div class="position-relative form-group">
            <label>Width</label>
            <input name="varian_width[]" placeholder="width" type="text" value="{{ $varian->width }}" data-uniq_varian="{{ $uniq_varian }}" class="form-control varian_width_input-{{ $uniq }}-{{ $uniq_varian }}">
        </div>
        <div class="position-relative form-group">
            <label>Height</label>
            <input name="varian_height[]" placeholder="height" type="text" value="{{ $varian->height }}" data-uniq_varian="{{ $uniq_varian }}" class="form-control varian_height_input-{{ $uniq }}-{{ $uniq_varian }}">
        </div>
        <div class="position-relative form-group">
            <label>Weight</label>
            <input name="varian_weight[]" placeholder="weight" type="number" data-uniq_varian="{{ $uniq_varian }}" class="form-control varian_weight_input-{{ $uniq }}-{{ $uniq_varian }}" value="{{ $varian->weight }}">
        </div>
        <div class="position-relative form-group">
            <label>Power</label>
            <input name="varian_power[]" placeholder="power" type="text" data-uniq_varian="{{ $uniq_varian }}" class="form-control varian_power_input-{{ $uniq }}-{{ $uniq_varian }}" value="{{ $varian->power }}">
        </div>

        <div data-uniq="{{ $uniq }}" data-uniq_varian="{{ $uniq_varian }}" class="vendor-varian-attribute-area-select vendor-varian-attribute-area-{{ $uniq }}-{{ $uniq_varian }}">
            <div class="position-relative form-group">
                @foreach($varian->productVarianAttributes as $pva)
                 <input 
                    data-uniq_attr="{{ $pva->attribute->id }}" 
                    data-uniq_varian="{{ $uniq_varian }}" 
                    data-uniq="{{ $uniq }}" 
                    value="{{ $pva->id }}" 
                    type="hidden" 
                    class="form-control product_varian_attribute_id_input-{{ $uniq }}-{{ $uniq_varian }}-{{ $pva->attribute->id }}" 
                    name="product_varian_attribute_id[]">

                <input type="hidden" name="attribute_id[]" value="{{ $pva->attribute->id }}" data-uniq_attr="{{ $pva->attribute->id }}" class="attr-select-{{ $uniq }}-{{ $uniq_varian }} attribute-select-{{ $uniq }}-{{ $uniq_varian }}-{{ $pva->attribute->id }}">
                <label>{{ $pva->attribute->name }}</label>
                <select data-uniq_attr="{{ $pva->attribute->id }}" data-uniq_varian="{{ $uniq_varian }}" data-uniq="{{ $uniq }}" class="form-control term-select-{{ $uniq }}-{{ $uniq_varian }}-{{ $pva->attribute->id }}" name="term_id[]">
                    @foreach($data[$pva->attribute->id]['term_name'] as $key => $term)
                    @if($term['term_id'] == $pva->term->id)
                    <option value="{{ $pva->term->id }}" selected>{{ $pva->term->name . ' ' .  $pva->term->color_code . ' ' .  $pva->term->hex_rgb}}</option>
                    @else
                    <option value="{{ $term['term_id'] }}">{{ $term['term_name'] }}</option>
                    @endif
                    @endforeach
                </select>
                @endforeach
            </div>
        </div>
    </div>
</div>