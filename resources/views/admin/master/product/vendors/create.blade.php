@extends('admin.home')
@section('content')
<form action="" method="post" id="form-vendor-create" style="display: contents;">
    <div class="col-md-12">
        <div class="card">
            <div class="header-bg card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    {{ $title }}
                </div>
                <div class="btn-actions-pane-right text-capitalize">
                    <a href="{{ url('/admin/vendor/show') }}" class="btn btn-primary"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
                </div>
            </div>
            <div class="card-body">
                <div class="position-relative form-group">
                    <label>Name</label>
                    <input name="name" placeholder="Name" type="text" class="form-control" required>
                </div>

                <div class="position-relative form-group">
                    <label>Code</label>
                    <input name="code" placeholder="Code" type="text" class="form-control" required>
                </div>

                <div class="position-relative form-group">
                    <label>Office Email</label>
                    <input name="office_email" placeholder="office email" type="email" class="form-control" required>
                </div>

                <div class="position-relative form-group">
                    <label>Busines Type</label>
                    <fieldset class="position-relative form-group">
                        <div class="position-relative form-check">
                            <label class="form-check-label">
                                <input name="is_business_type" type="radio" value=1 class="form-check-input" checked required>
                                Yes
                            </label>
                        </div>
                        <div class="position-relative form-check">
                            <label class="form-check-label">
                                <input name="is_business_type" type="radio" value=0 class="form-check-input" required>No
                            </label>
                        </div>
                    </fieldset>
                </div>

                <div class="position-relative form-group" id="provHide">
                    <label>User Vendor</label>
                    <input type="checkbox" class="checkbox-user" checked>
                    <div class="vendor-area">
                        <select class="js-example-basic-single" id="iserID" name="user_id" style="width:100%">
                            <option value="">Select User</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="card-footer">
                <button class="btn btn-success btn-sm" type="submit"><i class="fa fa-sign-in" aria-hidden="true"></i> Save</button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    globalCRUD.select2("#iserID", '/user/select2')

    $('#form-vendor-create').submit(function(e) {
        globalCRUD.handleSubmit($(this))
            .storeTo('/vendor')
            .redirectTo(function(resp) {
                return '/admin/vendor/show';
            })
        e.preventDefault();
    });

    $('.checkbox-user').change(function(e) {
        if (this.checked) {
            $('.vendor-area').show()
        } else {
            $('.vendor-area').hide();
            $('#iserID').val('').change();
        }
    })
</script>
@endsection