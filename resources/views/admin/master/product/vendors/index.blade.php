@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table id="form-vendor-table" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>No</th>
                        <th>Vendor Name </th>
                        <th>Code</th>
                        <th>Office Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ route('admin.vendor.create.web') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Vendor</a>
            <button type="button" class="btn btn-sm btn-danger delete-selected-btn"><i class="fa fa-remove"></i> DELETE VENDOR</button>
        </div>
    </div>
</div>
@endsection
@section('script')
@include('admin.script.master._vendorScript')
<script>
    globalCRUD.datatables({
        url: '/vendor/datatables',
        selector: '#form-vendor-table',
        columnsField: ['', 'DT_RowIndex', 'name', 'code', 'office_email'],
        selectable: true,
        buttonDeleteBatch: '.delete-selected-btn',
        actionLink: {
            update: function(row) {
                return '/admin/vendor/' + row.id + '/edit';
            },
            delete: function(row) {
                return '/vendor/' + row.id;
            },
            deleteBatch: function(selectedid) {
                return "/vendor/destroy_batch/";
            },
        },
    })
</script>
@endsection