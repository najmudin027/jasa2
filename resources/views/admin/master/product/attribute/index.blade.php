@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">

            </div>
        </div>
        <div class="card-body">
            <table id="product-attribute-table" class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Child</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ route('product.product_attribute.create.web') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add New Attribute</a>
        </div>
    </div>
</div>
@endsection

@section('script')
@include('admin.master.product.attribute._js')
<script>
    ProductAttributeObj.tableInit();
</script>
@endsection