@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
            <a href="{{ route('product.product_attribute.index.web') }}" class="btn btn-clus btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
        <div class="card-body">
            <form class="" id="form-attribute-create">
                <div class="position-relative form-group">
                    <label for="name" class="label-control">Attribute Name</label>
                    <input name="name" id="name" placeholder="name" type="text" class="form-control form-control-sm" required>
                </div>
                <div class="position-relative form-group">
                    <label for="name" class="label-control">Child Name</label>
                    <div id="childs-input-area"></div>
                </div>
                <button class="btn btn-oke btn-sm" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> Save</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
@include('admin.master.product.attribute._js')

<script>
    $('#childs-input-area').append(ProductAttributeObj.templateInputChild());

    $('#form-attribute-create')
        .submit(function(e) {
            ProductAttributeObj.store(
                Helper.serializeForm($(this))
            );
            e.preventDefault();
        });

    // ada new field when in last elemenr
    $(document)
        .on('focus', 'input[name="childs[]"]', function(e) {
            if ($(this).is('input[name="childs[]"]:last')) {
                $('#childs-input-area').append(
                    ProductAttributeObj.templateInputChild()
                );
            }
        })

    // delete 
    $(document)
        .on('click', '.delete-child', function(e) {
            id = $(this).attr('data-id');
            if ($('.delete-child').length > 1) {
                $(this).parent().parent().parent().parent().remove();
            }
        })
</script>
@endsection