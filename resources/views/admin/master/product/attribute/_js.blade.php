<script>
    var ProductAttributeObj = {
        table: null,
        datatables: function() {
            selector = '#product-attribute-table';
            url = '/product_attribute/datatables';
            DataTablesHelper.setConfig({
                orderBy: [1, 'asc']
            });
            table = DataTablesHelper.make(selector, url, this.columns());
            this.table = table;

            return table;
        },
        // datatables columns
        columns: function() {
            return [{
                    data: "DT_RowIndex",
                    name: "DT_RowIndex"
                },
                {
                    data: "name",
                    name: "name",
                },
                {
                    data: "id",
                    name: "id",
                    searchable: false,
                    orderable: false,
                    render: function(data, type, full) {
                        return full.childs.length;
                    }
                },
                {
                    data: "id",
                    name: "id",
                    searchable: false,
                    orderable: false,
                    render: function(data, type, full) {
                        edit_btn = "<button data-id='" + full.id + "' class='edit-product-attribute btn btn-warning btn-sm btn-white btn-hover-shine'><i class='fa fa-pencil'></i> Edit</button>";

                        delete_btn = "<button data-id='" + full.id + "'  class='delete-product-attribute btn btn-danger btn-sm btn-hover-shine'><i class='fa fa-trash'></i> Delete</button>";

                        return edit_btn + " " + delete_btn;
                    }
                },
            ];
        },
        tableInit: function() {
            // load datatables
            this.datatables();

            // button update
            $(document)
                .on('click', '.edit-product-attribute', function(e) {
                    id = $(this).attr('data-id');
                    Helper.redirectTo('/admin/product-attribute/' + id + '/edit');
                });

            //button delete
            $(document)
                .on('click', '.delete-product-attribute', function(e) {
                    id = $(this).attr('data-id');
                    ProductAttributeObj.delete(id);
                })
        },
        // refresh table
        reloadTable: function() {
            if (ProductAttributeObj.table != null) {
                ProductAttributeObj.table.ajax.reload();
            }
        },
        // insert
        store: function(data) {
            Helper.loadingStart();
            Axios.post('/product_attribute', data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg)
                        .redirectTo('/admin/product-attribute');
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        },
        // update
        update: function(data) {
            Helper.loadingStart();
            Axios.put('/product_attribute/' + data.id, data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg)
                        .redirectTo('/admin/product-attribute');
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        },
        // delete
        delete: function(id) {
            Helper.confirmDelete(function() {
                Helper.loadingStart();
                Axios.delete('/product_attribute/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif(Helper.deleteMsg());
                        // reload
                        ProductAttributeObj.reloadTable();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            })
        },
        // delete child
        deleteChild: function(id, $el) {
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.delete('product_attribute/destroy_child/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif(Helper.deleteMsg());
                        // remove element
                        $el.remove();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            })
        },
        // template new child
        templateInputChild: function() {
            input = '';
            input += '<div class="form-row">';
            input += '<div class="col-md-6">';
            input += '<div class="input-group">';
            input += '<div class="input-group-prepend">';
            input += '<span class="input-group-text delete-child" data-id="0" style="cursor: pointer"><i class="fa fa-remove"></i></span>';
            input += '</div>';
            input += '<input type="hidden" name="child_id[]" value="0">';
            input += '<input name="childs[]" placeholder="name" value="" type="text" class="child-input form-control form-control-sm">';
            input += '</div>';
            input += '</div>';
            input += '<div class="col-md-3">';
            input += '<input name="color_code[]" placeholder="code" type="text" class="form-control form-control-sm">';
            input += '</div>';
            input += '<div class="col-md-3">';
            input += '<input name="hex_rgb[]" placeholder="HEX RGB" type="color" class="form-control form-control-sm">';
            input += '</div>';
            input += '</div>';
            return input;
        },
    }
</script>