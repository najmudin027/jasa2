@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
            <a href="{{ route('product.product_attribute.index.web') }}" class="btn btn-clus btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
            </div>
        </div>
        <div class="card-body">
            <form class="" id="form-attribute-update">
                <div class="position-relative form-group">
                    <label for="name" class="label-control">Attribute Name</label>
                    <input name="id" value="{{ $attribute->id }}" type="hidden">
                    <input name="name" id="name" value="{{ $attribute->name }}" placeholder="name" type="text" class="form-control form-control-sm">
                </div>
                <div class="position-relative form-group">
                    <label for="name" class="label-control">Child Name</label>
                    <div id="childs-input-area">
                        @foreach($attribute->childs as $child)
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text delete-child" data-id="{{ $child->id }}" style="cursor: pointer"><i class="fa fa-remove"></i></span>
                                    </div>
                                    <input type="hidden" name="child_id[]" value="{{ $child->id }}">
                                    <input name="childs[]" placeholder="name" value="{{ $child->name }}" type="text" class="child-input form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <input name="color_code[]" placeholder="code" value="{{ $child->color_code }}" type="text" class="form-control form-control-sm">
                            </div>
                            <div class="col-md-3">
                                <input name="hex_rgb[]" placeholder="HEX RGB" value="{{ $child->hex_rgb }}" type="color" class="form-control form-control-sm">
                            </div>
                        </div>
                        @endforeach

                        @if($attribute->childs->count() == 0)
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text delete-child" data-id="{{ $child->id }}" style="cursor: pointer"><i class="fa fa-remove"></i></span>
                                    </div>
                                    <input type="hidden" name="child_id[]" value="0">
                                    <input name="childs[]" placeholder="name" value="" type="text" class="child-input form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <input name="color_code[]" placeholder="code" type="text" class="form-control form-control-sm">
                            </div>
                            <div class="col-md-3">
                                <input name="hex_rgb[]" placeholder="HEX RGB" type="color" class="form-control form-control-sm">
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <button class="btn btn-oke btn-sm" type="submit"><i class="fa fa-plus" aria-hidden="true"></i> Save</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
@include('admin.master.product.attribute._js')

<script>
    // ada new field when in last elemenr
    $(document)
        .on('focus', 'input[name="childs[]"]', function(e) {
            if ($(this).is('input[name="childs[]"]:last')) {
                $('#childs-input-area').append(
                    ProductAttributeObj.templateInputChild()
                );
            }
        })

    // delete child
    $(document)
        .on('click', '.delete-child', function(e) {
            id = $(this).attr('data-id');
            if ($('.delete-child').length > 1) {
                $el = $(this).parent().parent().parent().parent();
                if (id == '0') {
                    $el.remove();
                } else {
                    ProductAttributeObj.deleteChild(id, $el);
                }
            }
        })

    // update
    $('#form-attribute-update')
        .submit(function(e) {
            ProductAttributeObj.update(
                Helper.serializeForm($(this))
            );
            e.preventDefault();
        })
</script>
@endsection