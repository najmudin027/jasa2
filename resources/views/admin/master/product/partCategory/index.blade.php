@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                
            </div>
        </div>
        <div class="card-body">
            <table id="table-part-category" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Part Category</th>
                        <th>Part Code</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-body">
            <button class="btn btn-primary btn-sm add-part-category"><i class="fa fa-plus"></i> Add Part Category</button>
        </div>
    </div>
</div>
@include('admin.master.product.partCategory.modalPartCategory')


@endsection

@section('script')
<script>
    globalCRUD.datatables({
        orderBy: [1, 'asc'],
        url: '/product_part_category/datatables',
        selector: '#table-part-category',
        columnsField: ['DT_RowIndex', 'part_category', 'part_code'],
        modalSelector: "#modal-part-category",
        modalButtonSelector: ".add-part-category",
        modalFormSelector: "#form-part-category",
        actionLink: {
            store: function() {
                return "/product_part_category";
            },
            update: function(row) {
                return "/product_part_category/" + row.id;
            },
            delete: function(row) {
                return "/product_part_category/" + row.id;
            },
        }
    })
</script>
@endsection
