<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script>
    ProductPartCategoryObj = {
        // table
        table: null,
        datatables_url: '/product_part_category/datatables',
        datatables_selector: '#table-part-category',
        modal: $('#modal-part-category').modal({
            show: false
        }),
        datatables: function() {
            table = DataTablesHelper.make(
                this.datatables_selector,
                this.datatables_url,
                this.columns()
            );

            this.table = table;

            return table;
        },
        // datatables columns
        columns: function() {
            return [{
                    data: "id",
                    name: "id"
                },
                {
                    data: "part_category",
                    name: "part_category"
                },
                {
                    data: "part_code",
                    name: "part_code"
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        edit_btn = "<button data-id='" + full.id + "' class='edit-part-category btn btn-warning btn-white btn-sm'><i class='fa fa-pencil'></i></button>";

                        delete_btn = "<button data-id='" + full.id + "' class='delete-part-category btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";

                        return edit_btn + " " + delete_btn;
                    }
                },
            ];
        },
        // refresh table
        reloadTable: function() {
            if (ProductPartCategoryObj.table != null) {
                ProductPartCategoryObj.table.ajax.reload();
            }

            return this;
        },


        // modal
        modalShow: function(title = '', row = null) {
            if (title == '') {
                title = $(this).text();
            }

            // jika akan edit
            if (row !== null) {
                // isi data
                this.isiDataFormModal(row);
            } else {
                // kosongkan form input
                this.kosongkanDataFormModal();
            }

            // ganti text
            this.modal.find('.modal-title').text(title);
            // show modal
            this.modal.modal('show');
        },
        modalHide: function() {
            // show modal
            this.modal.modal('hide');

            return this;
        },
        kosongkanDataFormModal: function() {
            $("#part_code").val('');
            $("#part_category").val('');
            $("#id").val('');
        },
        isiDataFormModal: function(row) {
            $("#part_code").val(row.part_code);
            $("#part_category").val(row.part_category);
            $("#id").val(row.id);
        },


        // request
        store: function(data) {
            Axios.post('/product_part_category/', data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg);

                    // reload table
                    ProductPartCategoryObj.modalHide().reloadTable();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        },
        update: function(data) {
            Axios.put('/product_part_category/' + data.id, data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg);

                    // reload table
                    ProductPartCategoryObj.modalHide().reloadTable();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        },
        delete: function(id) {
            Helper.confirm(function() {
                Axios.delete('/product_part_category/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('data berhasil didelete');
                        // reload 
                        ProductPartCategoryObj.reloadTable();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            })
        }
    }
</script>

<script>
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })
</script>