@extends('admin.home')
@section('content')
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header-tab card-header">
                        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                            Update Product Category
                        </div>
                        <div class="btn-actions-pane-right text-capitalize">
                            <a href="{{ url('/show-product-category') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
                        </div>
                    </div>
                    <div class="table-responsive"><br>
                        <div class="container">                            <div class="card-body">
                                {{-- <form action="" method="post"> --}}
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Product Category Name</label>
                                        <div class="col-sm-10">
                                            <input name="name" value=" {{ $editProductCategory->name }} " id="examplename" type="text" class="form-control">
                                        </div>
                                    </div>
                                    <input type="hidden" name="slug" value=" {{ $editProductCategory->slug }} " id="examplename" class="form-control">
                                    <input type="hidden" name="product_cate_code" value=" {{ $editProductCategory->product_cate_code }} " id="examplename"  class="form-control">
                                    <input type="hidden" name="id" value=" {{ $editProductCategory->id }} ">
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Parent Category</label>
                                        <div class="col-sm-10">
                                            <select class="parent" name="parent_id" style="width:100%">
                                                <option value="{{ $editProductCategory->parent_id }}" selected>{{ $editProductCategory->name }}</option></select>
                                        </div>
                                    </div>

                                    <div class="position-relative row form-check">
                                        <div class="col-sm-10 offset-sm-2">
                                            <button class="btn btn-success" id="update"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                                        </div>
                                    </div>
                                {{-- </form> --}}
                            </div>
                        </div>
                    </div>
                    <div class="d-block text-center card-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.template._mainScript')
    @include('admin.script.master._productCategoryScript')
@endsection
