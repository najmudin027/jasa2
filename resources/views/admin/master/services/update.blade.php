@extends('admin.home')
@section('content')
<style>
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 100%;
}
</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
               <a class="btn btn-primary btn-sm add-services" href="{{ url('/admin/services/show') }}">Back</a>
            </div>
        </div>
        <input type="hidden" name="id" value="{{ $getServices->id }}">
        <div class="card-body">
            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Service Name</label>
                <div class="col-sm-9">
                    <input name="name" value="{{ $getServices->name }}" placeholder="Services Name" type="text"  onload="convertToSlug(this.value)" onkeyup="convertToSlug(this.value)" class="form-control">
                </div>
            </div>

            <div class="position-relative row form-group" id="districShow">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Slug</label>
                <div class="col-sm-9">
                    <input name="slug" value="{{ $getServices->slug }}" placeholder="Slug" type="text" id="to_slug" class="form-control">
                </div>
            </div>

            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Images</label>
                <div class="col-sm-6">
                    <input name="icon" placeholder="Image" type="file" class="form-control" id="imgInp">
                </div>
                <div class="col-sm-3">
                    @if($getServices->images == null )
                        <img id="img-upload" src="http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png" alt="your image" style="width:100px;height:100px"/>
                    @else
                        <img id='img-upload' src="{{asset('/storage/services/' . $getServices->images)}}" alt="your image" style="width:125px;height:125px">
                    @endif

                </div>
            </div>
            <div class="position-relative row form-check">
            <div class="col-sm-10 offset-sm-2">
                <button class="btn btn-success" id="save" style="float:right"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
            </div>
        </div><br>
        </div>

    </div>
</div>
@endsection

@section('script')
<script>
    function convertToSlug(str) {
        elem = 'to_slug';
        Helper.toSlug(str, elem);
    }
    $(document).ready(function() {
        $('#to_slug').prop('readonly', true);
    });
    $("#save").click(function() {
        id = $('input[name="id"]').val();
        var fd = new FormData();
        var files = $('#imgInp')[0].files[0];
        if(files){
            fd.append('icon', files);
        }
        fd.append('name', $('input[name="name"]').val());
        fd.append('slug', $('input[name="slug"]').val());

        $.ajax({
            url:Helper.apiUrl('/services/'+id+''),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Services Has Been Saved',
                    });
                    window.location.href = Helper.redirectUrl('/admin/services/show');
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });

                    })
                }

            },
        });
    });
</script>


<script>
    $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		});
	});
</script>

@endsection
