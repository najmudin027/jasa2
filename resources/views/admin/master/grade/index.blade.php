@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New Grade</button>
            </div>
        </div>
        <div class="card-body">
            <table id="table-grade" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Min</th>
                        <th>Max</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<form id="form-grade">
    <div class="modal fade" id="modal-grade" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Grade</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Name</label>
                    <div class="form-group">
                        <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                    </div>
                    <label>Min Value</label>
                    <div class="form-group">
                        <input name="min_value" placeholder="value" id="min_value" type="number" class="form-control" required>
                    </div>
                    <label>Max Value</label>
                    <div class="form-group">
                        <input name="max_value" placeholder="value" id="max_value" type="number" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<script>
    globalCRUD.datatables({
        orderBy: [1, 'asc'],
        url: '/grades/datatables',
        selector: '#table-grade',
        columnsField: ['DT_RowIndex', 'name', 'min_value', 'max_value'],
        modalSelector: "#modal-grade",
        modalButtonSelector: ".add-grade",
        modalFormSelector: "#form-grade",
        actionLink: {
            store: function() {
                return "/grades";
            },
            update: function(row) {
                return "/grades/" + row.id;
            },
            delete: function(row) {
                return "/grades/" + row.id;
            },
        }
    })
</script>
@endsection
