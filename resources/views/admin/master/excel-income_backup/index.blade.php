@extends('admin.home')
@section('content')
<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }


</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize" style="margin-left:150px;">
                <form id="form-search">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Fr</span>
                                </div>
                                <input type="text" name="date_from" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">To</span>
                                </div>
                                <input type="text" name="date_to" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div role="group" class="mb-2 btn-group-sm btn-group btn-group-toggle">
                                <button type="submit" class="mb-1 mr-1 btn btn-sm btn-primary btn-gradient-alternate"><i class="fa fa-search"></i> Filter </button>
                                {{-- <button type="button" class="mb-1 mr-1 btn btn-sm btn-success btn-gradient-alternate" id="export_data"><i class="fa fa-download"></i> Export Data </a></button> --}}
                                <a href="{{ url('admin/excel-income/create') }}" class="mb-1 mr-1 btn btn-sm btn-primary btn-gradient-alternate"><i class="fa fa-plus"></i> Add </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>


        </div>

        <div class="card-body">
        <table style="width: 100%;" id="table-excel-income" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Title</th>
                        <th>Code</th>
                        <th>Description</th>
                        <th>Total In</th>
                        <th>Total Refund</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>
     $(function() {
        $('.date_format').datetimepicker({
            timepicker:false,
            format:'d-m-Y',
            allowBlank: false,
        });

        var d = new Date();
        var startDate = d.setMonth(d.getMonth() - 1);
        var e = new Date();

        $('input[name="date_from"]').val(moment(startDate).format('DD-MM-YYYY'));
        $('input[name="date_to"]').val(moment(e.setDate(e.getDate())).format('DD-MM-YYYY'));

    });

    var tableOrder = globalCRUD.datatables({
        url: '/excel-income/datatables',
        selector: '#table-excel-income',
        columnsField: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                sortable: false,
                },
                'title', 'code','description',
                {
                    data: "total_in_nominal",
                    name: "total_in_nominal",
                    render: function(row,type,full) {
                        return 'Rp.'+Helper.toCurrency(row);
                    }
                },
                {
                    data: "total_refund",
                    name: "total_refund",
                    render: function(row,type,full) {
                        return 'Rp.'+Helper.toCurrency(row);
                    }
                },
                ],
        actionLink: {
            update: function(row) {
                return "/admin/excel-income/edit/" + row.id;
            },
            delete: function(row) {
                return "/excel-income/" + row.id;
            },
            detail: function(row) {
                return "/admin/excel-income/detail/" + row.id;
            },
            export_detail_excel: function(row) {
                return "/admin/excel-income/export_detail_excel/" + row.id;
            }
        }
    });

    function checkValidation() {
        var isValid = true;
        var date_from = $('input[name="date_from"]').val();
        var date_to = $('input[name="date_to"]').val();
        if(date_from == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date Fr !');
            return false;
        } else if (date_to == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date To !');
            return false;
        }
        return isValid;
    }

    $("#export_data").click(function(e) {
        var date_from = $('input[name="date_from"]').val();
        var date_to = $('input[name="date_to"]').val();
        var url = '/admin/excel-income/export';
        if(checkValidation()) {
            url = '/admin/excel-income/export?date_from='+date_from+'&date_to='+date_to+'';
            Helper.redirectTo(url);
        }
        // console.log(url);

        e.preventDefault();
    });

    $("#form-search").submit(function(e) {
        e.preventDefault();
        if(checkValidation()) {
            var input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/excel-income/datatables/search' + playload);
            tableOrder.table.reloadTable(url);
        }

    })

    </script>
@endsection
