<script>
    var _uniq_counter = 1;
    var _uniq_counter_description = 1;
    var is_update = $('#is_update').val();
    var description = 'description';
    if(is_update) {
        description = 'new_description';
    }
    var ClassApp = {
        hitungTotal: function() {
            Helper.unMask('input[name="in_nominal[]"]');
            Helper.unMask('input[name="refund[]"]');
            var grand_total = 0;
            var total_in_nominal = 0;
            var total_refund = 0;
            $('input[name="in_nominal[]"]').each(function() {
                var uniq = $(this).attr('data-uniq');
                var in_nominal = $('.in_nominal_input-' + uniq).val() == '' ? 0 : parseInt($('.in_nominal_input-' + uniq).val());
                var refund = $('.refund_input-' + uniq).val() == '' ? 0 : parseInt($('.refund_input-' + uniq).val());

                total_in_nominal += in_nominal;
                total_refund += refund;

            });
            $('#total_in_nominal').html('Rp. '+Helper.toCurrency(total_in_nominal));
            $('#total_refund').html('Rp. '+Helper.toCurrency(total_refund));
            grand_total = total_in_nominal - total_refund;
            $('#grand_total').html('Rp. '+Helper.toCurrency(grand_total));

        },
        templateNewInputDetailData: function(xx,zz) {

            $('.dynamic_details').append((`
                <div class="col-md-6 detail-uniq-${xx}" id="details_row_${xx}" style="amrgin-bottom:10px;">
                    <input type="hidden" value="0" name="details_id[]">
                    <div class="card">
                        <div class="card-header header-border">
                            <div class="btn-actions-pane-right text-right">
                                <div role="group" class="btn-group-sm btn-group">
                                    <button data-uniq="${xx}" class="btn btn-danger btn-sm btn-delete-detail btn-delete-detail-uniq-${xx}" type="button">
                                    <i class="fa fa-remove"></i>Delete</button>
                                    <button data-uniq="${xx}" data-hide="1" class="btn btn-sm btn-white btn-warning btn-hide-detail btn-hide-detail-uniq-${xx}" type="button">Hide</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body detail-body-uniq-${xx}">
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-12 col-form-label">Date Transaction</label>
                                <div class="col-sm-12">
                                    <input name="date_transaction[]" type="text" class="form-control date_transaction_input-${xx} date_format" data-uniq="${xx}" required="required">
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-12 col-form-label">Source</label>
                                <div class="col-sm-12">
                                    <input name="source[]" placeholder="" type="text" class="form-control source_input-${xx}" data-uniq="${xx}" required="required">
                                </div>
                            </div>
                            <div class="position-relative row form-group description-area-${xx}">
                                <label for="exampleEmail" class="col-sm-12 col-form-label">Description</label>
                                <div class="description-uniq-${xx}-${zz} input-group">
                                    <div class="input-group-append col-md-12">
                                        <input name="${description}_${xx}[]" placeholder="" type="text" class="form-control description_${xx}-${zz}" data-uniq="${xx}" required="required">
                                        `+ClassApp.descriptionAddButton(xx,zz)+`
                                    </div>
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-12 col-form-label">Date Transfer</label>
                                <div class="col-sm-12">
                                    <input name="date_transfer[]" type="text" class="form-control date_transfer_input-${xx} date_format" data-uniq="${xx}" required="required">
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-12 col-form-label">In</label>
                                <div class="col-sm-12">
                                    <input name="in_nominal[]" placeholder="In" type="text" class="form-control currency in_nominal_input-${xx}" data-uniq="${xx}" required="required">
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-12 col-form-label">Refund</label>
                                <div class="col-sm-12">
                                    <input name="refund[]" placeholder="Refund" type="text" class="form-control currency refund_input-${xx}" data-uniq="${xx}" required="required">
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-12 col-form-label">Branch</label>
                                <div class="col-sm-12">
                                    <input name="branch[]" placeholder="" type="text" class="form-control branch_input-${xx}" data-uniq="${xx}" required="required">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `));

            Helper.dateFormat('.date_format');
            Helper.onlyNumberInput('.number_only')
            Helper.currency('.currency')
        },
        description: function(uniq, uniq_desc) {
            var xx = uniq;
            if (xx == 'xxx') {
                xx = '0';
            }
            return (`<input type="hidden" value="0" name="details_desc_id_${uniq}[]">
                    <div class="input-group">
                        <div class="description-uniq-${uniq}-${uniq_desc} input-group-append col-md-12">
                            <input name="${description}_${xx}[]" placeholder="" type="text" class="form-control description-${uniq}-${uniq_desc}" data-uniq="${uniq_desc}" required="required">
                            `+ClassApp.descriptionDeleteButton(uniq, uniq_desc)+`
                        </div>
                    </div>`);
        },
        descriptionAddButton: function(uniq, uniq_desc) {
            template = '';
            template += '<button data-uniq="' + uniq + '" data-uniq-description="' + uniq_desc + '"  class="btn btn-success btn-sm btn-add-description btn-add-description-uniq-' + uniq + '-' + uniq_desc + '" type="button">';
            template += '<i class="fa fa-plus"></i>';
            template += '</button>';
            return template;
        },
        descriptionDeleteButton: function(uniq, uniq_desc) {
            template = '';
            template += '<button data-uniq="' + uniq + '" data-uniq-description="' + uniq_desc + '"  class="btn btn-danger btn-sm btn-delete-description btn-delete-description-uniq-' + uniq + '" type="button">';
            template += '<i class="fa fa-trash-o"></i>';
            template += '</button>';
            return template;
        },
        // Description Detail
        appendDescription: function(uniq, uniq_desc) {
            template = ClassApp.description(uniq, uniq_desc);

            $('.description-area-' + uniq).append(template)
        },
        clearDetailDescription: function(uniq) {
            $('.description-area-' + uniq).html('');
        },
        deleteDetailDescription: function(uniq, uniq_desc, desc_id = null) {
            Helper.confirm(function() {
                if (desc_id) {
                    ClassApp
                        .destroyDetailDescription(desc_id, function(resp) {
                            $('.description-uniq-' + uniq + '-' + uniq_desc).remove();
                        })
                } else {
                    $('.description-uniq-' + uniq + '-' + uniq_desc).remove();
                }
            });
        },
        destroyDetailDescription: function(id, callback) {
            Helper.loadingStart();
            $.ajax({
                type: 'delete',
                url: Helper.apiUrl('/excel-income/detail_description/' + id),
                success: function(resp) {
                    Helper.loadingStop();
                    callback(resp);
                    Helper.successNotif(resp.msg)
                    location.reload();
                },
                error: function(xhr, status, error) {
                    Helper.loadingStop();
                    Helper.errorMsgRequest(xhr, status, error);
                }
            });
        },

        saveData: function(el,e) {
            Helper.unMask('.currency');
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);
            $.ajax({
                url:Helper.apiUrl('/excel-income'),
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        iziToast.success({
                            title: 'OK',
                            position: 'topRight',
                            message: 'Excel Income Has Been Saved',
                        });
                        window.location.href = Helper.redirectUrl('/admin/excel-income/show');
                    }
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });

            e.preventDefault();
        },
        updateData: function(el, e, id) {
            Helper.unMask('.currency');
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);

            $.ajax({
                url:Helper.apiUrl('/excel-income/'+id+''),
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response != 0) {
                        iziToast.success({
                            title: 'OK',
                            position: 'topRight',
                            message: 'Excel Income Has Been Saved',
                        });
                        window.location.href = Helper.redirectUrl('/admin/excel-income/show');
                    }
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                            iziToast.error({
                                title: 'Error',
                                position: 'topRight',
                                message:  pesan[0]
                            });

                        })
                    }

                },
            });

            e.preventDefault();
        },
        detailHide: function(uniq, status = 'hide') {
            if (status == 'hide') {
                $('.detail-body-uniq-' + uniq).hide();
            }

            if (status == 'show') {
                $('.detail-body-uniq-' + uniq).show();
            }
        },
        detailDelete: function(uniq, detail_id = null) {
            Helper.confirm(function() {
                if (detail_id) {
                    ClassApp
                        .destroyDetail(detail_id, function(resp) {
                            $('.detail-uniq-' + uniq).remove();
                        })
                } else {
                    $('.detail-uniq-' + uniq).remove();
                }
            });
        },
        destroyDetail: function(id, callback) {
            Helper.loadingStart();
            Axios.delete('/excel-income/destroy_details_data/' + id)
                .then(function(response) {
                    Helper.successNotif('Success Delete');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });

        },

    }

    // add description btn
    $(document).on('click', ".btn-add-description", function(e) {
        uniq = $(this).attr('data-uniq');
        uniq_description = _uniq_counter_description;
        ClassApp.appendDescription(uniq, uniq_description);
        _uniq_counter_description++;
    });

    // delete description btn
    $(document).on('click', '.btn-delete-description', function() {
        uniq = $(this).attr('data-uniq');
        var desc_id = $(this).attr('data-id');
        uniq_description = $(this).attr('data-uniq-description');
        ClassApp.deleteDetailDescription(uniq, uniq_description, desc_id);
    })


</script>
