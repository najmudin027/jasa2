@extends('admin.home')
@section('content')
<div class="col-md-12 card">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            {{ $title }}
        </div>
        <div class="btn-actions-pane-right text-capitalize">
            <a href="{{ route('admin.country.index.web') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
        </div>
    </div>
    <div class="card-body">
        <form action="" method="post" id="form">
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input name="name" id="examplename" placeholder="Indonesia" type="text" class="form-control">
                    <strong><span id="error-name" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Code</label>
                <div class="col-sm-10">
                    <input name="code" id="examplename" placeholder="INA" type="text" class="form-control">
                    <strong><span id="error-code" style="color:red"></span></strong>
                </div>
                <span id="error-code" style="color:red"></span>
            </div>

            <div class="position-relative row form-check">
                <div class="col-sm-10 offset-sm-2">
                    <button class="btn btn-success" id="save"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script>
    $('#save').click(function(e) {
        $.ajax({
            type: 'post',
            data: {
                name: $('input[name="name"]').val(),
                code: $('input[name="code"]').val(),
            },

            url: Helper.apiUrl('/country'),
            success: function(html) {
                console.log(html)
                iziToast.success({
                    title: 'OK',
                    position: 'topRight',
                    message: 'Countries Has Been Saved',
                });
                window.location.href = Helper.redirectUrl('/admin/country');
            },
            error: function(xhr, status, error) {
                if (xhr.status == 422) {
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field) {
                        $('#error-' + field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message: pesan[0]
                        });

                    })
                }

            },
        });
        e.preventDefault();
    });

    // $('#form').submit(function(e) {
    //     globalCRUD.handleSubmit($(this))
    //         .storeTo('/country') // API
    //         .redirectTo(function(resp) {
    //             return '/admin/country/show';
    //         })

    //     e.preventDefault();
    // });
</script>

@endsection