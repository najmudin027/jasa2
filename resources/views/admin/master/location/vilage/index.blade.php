@extends('admin.home')
@section('content')
<div class="col-md-12 card">
    <div class="header-bg card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            {{ $title }}
        </div>
    </div>
    <div class="card-body">
        <table id="table" class="display table table-hover table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th>No</th>
                    <th>Village Name</th>
                    <th>Distric Name</th>
                    <th>City Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="card-footer">
        <a href="{{ route('admin.village.create.web') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> ADD VILLAGE</a>
        {{-- <a href="#" class="mb-2 mr-2 btn btn-sm btn-success" data-toggle="modal" data-target="#importExcel" data-backdrop="false"><i class="fa fa-file"></i> IMPORT</a> --}}
        <button type="button" class="mb-2 mr-2 btn btn-sm btn-danger delete-selected"><i class="fa fa-remove"></i> DELETE VILLAGE</button>
    </div>
</div>
@include('admin.master.modalImportExcel')
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        orderBy: [2, 'asc'],
        url: '/village/datatables',
        columnsField: ['', 'DT_RowIndex', 'name', 'district.name', 'district.city.name'],
        actionLink: {
            update: function(row) {
                return "/admin/village/" + row.id + '/edit';
            },
            delete: function(row) {
                return "village/" + row.id; // API
            },
            deleteBatch: function(selectedid) {
                return "village/destroy_batch/"; // API
            },
            import: function(row) {
                return "village/importExcel"; //API
            }
        },
        selectable: true,
        import: true
    })
</script>
{{-- @include('admin.script.master._vilageScript') --}}
@endsection