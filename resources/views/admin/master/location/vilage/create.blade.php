@extends('admin.home')
@section('content')
<div class="col-md-12 card">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            Create Master Village
        </div>
        <div class="btn-actions-pane-right text-capitalize">
            <a href="{{ route('admin.village.index.web') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
        </div>
    </div>
    <div class="card-body">
        <form action="" method="post" id="form">
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Country Name</label>
                <div class="col-sm-10">
                    <select id="country" name="ms_countries_id" style="width:100%"></select>
                </div>
            </div>

            <div class="position-relative row form-group" id="provinceShow">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Province Name</label>
                <div class="col-sm-10">
                    <select id="province" name="ms_province_id" style="width:100%"></select>
                </div>
            </div>

            <div class="position-relative row form-group" id="cityShow">
                <label for="exampleEmail" class="col-sm-2 col-form-label" >City Name</label>
                <div class="col-sm-10">
                    <select id="city" name="ms_city_id" style="width:100%">

                    </select>
                </div>
            </div>

            <div class="position-relative row form-group" id="districtShow">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Distric Name</label>
                <div class="col-sm-10">
                    <select id="district" name="ms_district_id" style="width:100%">

                    </select>
                    <strong><span id="error-ms_district_id" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Village Name</label>
                <div class="col-sm-10">
                    <input name="name" id="examplename" placeholder="Village" type="text" class="form-control">
                    <strong><span id="error-name" style="color:red"></span></strong>
                </div>
            </div>

            <div class="position-relative row form-check">
                <div class="col-sm-10 offset-sm-2">
                    <button class="btn btn-success" id="save"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
@section('script')
<script>
    $('#save').click(function(e) {
        $.ajax({
            type: 'post',
            data: {
                name: $('input[name="name"]').val(),
                ms_district_id: $('select[name="ms_district_id"]').val(),
            },

            url: Helper.apiUrl('/village'),
            success: function(html) {
                console.log(html)
                iziToast.success({
                    title: 'OK',
                    position: 'topRight',
                    message: 'City Has Been Saved',
                });
                window.location.href = Helper.redirectUrl('/admin/village');
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });
                    
                    })
                }
                
            },
        });
        e.preventDefault();
    });
    // $('#form').submit(function(e) {
    //     globalCRUD.handleSubmit($(this))
    //         .storeTo('/village') // API
    //         .redirectTo(function(resp) {
    //             return '/admin/village/show';
    //         })

    //     e.preventDefault();
    // });
</script>
    @include('admin.script.master._vilageScript')
@endsection
