@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>

        <div class="card-body">
            <table id="table" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>No</th>
                        <th>City Name</th>
                        <th>Province Name</th>
                        <th>Country Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>No</th>
                        <th>City Name</th>
                        <th>Province Name</th>
                        <th>Country Name</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class="card-body">
            <a href="{{ route('admin.city.create.web') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> ADD CITY</a>
            {{-- <a href="#" class="mb-2 mr-2 btn btn-sm btn-success" data-toggle="modal" data-target="#importExcel" data-backdrop="false"><i class="fa fa-file"></i> IMPORT</a> --}}
            <button type="button" class="mb-2 mr-2 btn btn-sm btn-danger delete-selected"><i class="fa fa-remove"></i> DELETE CITY</button>
        </div>
    </div>
</div>
@include('admin.master.modalImportExcel')
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        orderBy: [2, 'asc'],
        url: '/city/datatables',
        columnsField: [
            '',
            'DT_RowIndex',
            'name',
            {
                data: 'province.name',
                name: 'province.name',
                orderable: false,
            },
            {
                data: 'province.country.name',
                name: 'province.country.name',
                orderable: false,
            },
        ],
        actionLink: {
            update: function(row) {
                return "/admin/city/" + row.id + '/edit';
            },
            delete: function(row) {
                return "city/" + row.id; // API
            },
            deleteBatch: function(selectedid) {
                return "city/destroy_batch/"; // API
            },
            import: function(row) {
                return "city/importExcel"; //API
            }
        },
        selectable: true,
        import: true
    })
</script>
@endsection