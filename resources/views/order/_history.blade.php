<div class="card">
    <div class="card-header header-border">
        History Transaction
    </div>
    <div class="card-body">
        <div class="price scroll-area-sm" style="margin-top:25px; margin-bottom: 30px; height:200px">
            <div class="scrollbar-container ps--active-y ps">
                <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                    @foreach($histories as $key => $history_order)
                    <?php
                    $color = '';
                    $text = '';
                    if ($history_order->orders_statuses_id == 1) {
                        $color = 'primary';
                        $text = 'New order has created - ';
                    } elseif ($history_order->orders_statuses_id == 2) {
                        $color = 'warning';
                        $text = 'Job has pending, need waiting confirmation at - ';
                    } elseif ($history_order->orders_statuses_id == 3) {
                        $color = 'success';
                        $text = 'Job has approved by ' . $order->service_detail[0]->technician->user->name . ' at - ';
                    } elseif ($history_order->orders_statuses_id == 8) {
                        $color = 'warning';
                        $text = 'Pending Payment at - ';
                    } elseif ($history_order->orders_statuses_id == 9) {
                        $color = 'danger';
                        $text = 'Processing at - ';
                    } elseif ($history_order->orders_statuses_id == 4) {
                        $color = 'success';
                        $text = 'Payment successfully paid at - ';
                    } elseif ($history_order->orders_statuses_id == 5) {
                        $color = 'danger';
                        $text = 'Order has canceled at - ';
                    } elseif ($history_order->orders_statuses_id == 6) {
                        $color = 'warning';
                        $text = 'Order has rescheduled at - ';
                    } elseif ($history_order->orders_statuses_id == 7) {
                        $color = 'success';
                        $text = 'Job has done ! at - ';
                    }
                    ?>
                    <div class="vertical-timeline-item vertical-timeline-element">
                        <div><span class="vertical-timeline-element-icon bounce-in"><i class="badge badge-dot badge-dot-xl badge-{{ $color }}"> </i></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h2 class="timeline-title">{{ $history_order->order_status->name }}</h2>
                                <p>{{ $text }}<b class="text-{{ $color }}">{{ $history_order->created_at }}</b></p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
