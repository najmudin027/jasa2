$('.btn').addClass('btn-hover-shine');

var hideAvatarDropdown, hideModal;
$(document).ready(function(e) {
    // agar user profile bisa diclik
    $(".link-dropdown-avatar").click(function() {
        $('.dropdown-avatar').toggleClass('show')
        $('.dropdown-noty').removeClass('show')
        hideAvatarDropdown = true;
    })

    $(".link-dropdown-noty").click(function() {
        $('.dropdown-noty').toggleClass('show')
        $('.dropdown-avatar').removeClass('show')
        hideAvatarDropdown = true;
    })

    $(document).click(function() {
        if (hideAvatarDropdown) {
            $('.dropdown-avatar').removeClass('show')
            $('.dropdown-noty').removeClass('show')
            hideAvatarDropdown = false;
        }
    });
});

// datepicker
$(".use-datepicker-ui").datepicker("option", "dateFormat", 'yy-mm-dd');

$.fn.select2.defaults.set("theme", "bootstrap");

// Restricts input for the set of matched elements to the given inputFilter function.
(function($) {
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    };
}(jQuery));

// var local = window.location.hostname;
window.Helper = {
    onlyNumberInput: function(selector) {
        $(selector).inputFilter(function(value) {
            return /^\d*$/.test(value); // Allow digits only, using a RegExp
        });

        return this;
    },
    wysiwygEditor: function(selector) {
        ClassicEditor
            .create(document.querySelector(selector), {
                removePlugins: ['Heading', 'Link'],
                toolbar: ['bold', 'italic', 'underline', 'bulletedList']
            })
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    },
    hideSidebar: function() {
        $('.hamburger--elastic').addClass('is-active');
        $('.fixed-sidebar').addClass('closed-sidebar');
    },
    ratingStar: function(star, light = '', dark = '') {
        if (light == '') {
            light = '<span class="star"><i class="fa fa-star"></i></span>';
        }
        if (dark == '') {
            dark = '<span class="star"><i class="fa fa-star-o"></i></span>';
        }

        rating = parseInt(star);
        starTemplate = '';

        if (rating > 0) {
            var range = _.range(0, rating);
            _.each(range, function(value, key) {
                starTemplate += light + ' ';
            });

            if (rating < 5) {
                var range = _.range(0, 5 - rating);
                _.each(range, function(value, key) {
                    starTemplate += dark + ' ';
                });
            }
        } else {
            var range = _.range(0, 5);
            _.each(range, function(value, key) {
                starTemplate += dark + ' ';
            });
        }

        return starTemplate;
    },
    // url: function(url = '') {
    //     return 'http://'+local+'/' + url;
    // },
    // apiUrl: function(url = '') {
    //     return 'http://'+local+'/api' + url;
    // },
    thousandsSeparators: function(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    loadingStart: function() {
        $('#loading-indikator').addClass('is-active');
    },
    loadingStop: function() {
        $('#loading-indikator').removeClass('is-active');
    },

    url: function(url = '') {
        return '{{ url("/") }}' + url;
    },

    apiUrl: function(url = '') {
        return '{{ url("/") }}/api' + url;
    },

    redirectUrl: function(url) {
        return '{{ url("/") }}' + url;
    },

    frontApiUrl: function(url) {
        return '{{ url("/") }}/admin' + url;
    },

    saveToLocalstorage: function(key, val) {
        // val = JSON.stringify(val)
        localStorage.setItem(key, val);
    },

    numberRow: function(table) {
        table.on('order.dt search.dt', function() {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    },

    infoNotif: function(text) {
        this.loadingStop();

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100000",
            "hideDuration": "100000",
            "timeOut": "100000",
            "extendedTimeOut": "100000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        toastr.info(text)

        return this;
    },

    successNotif: function(text) {
        this.loadingStop();

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100000",
            "hideDuration": "100000",
            "timeOut": "100000",
            "extendedTimeOut": "100000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        toastr.success(text)

        return this;
    },

    errorNotif: function(text) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100000",
            "hideDuration": "100000",
            "timeOut": "100000",
            "extendedTimeOut": "100000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        toastr.error(text)

        return this;
    },

    warningNotif: function(text) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "100000",
            "hideDuration": "100000",
            "timeOut": "100000",
            "extendedTimeOut": "100000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        toastr.warning(text)

        return this;
    },

    errorMsgRequest: function(xhr, status, error) {
        this.errorNotif(xhr.responseJSON.msg);
        console.log('xhr', xhr)
        console.log('status', status)
        console.log('error', error)

        if (xhr.status == 422) {
            error = xhr.responseJSON.data;
            _.each(error, function(pesan, field) {
                // send notif
                Helper.errorNotif(pesan[0]);
                // clean msg
                $('.error-validation-mgs').remove();
                // append new message
                $('[name="' + field + '"]').after('<span class="error-validation-mgs" id="error-' + field + '" style="display:block; color:red"><br/>' + pesan[0] + '</span>');
                // stop loop
                return false;
            })
        }
    },

    toSlug: function(str, elem) {
        //replace all special characters | symbols with a space
        str = str.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ').toLowerCase();
        // trim spaces at start and end of string
        str = str.replace(/^\s+|\s+$/gm, '');
        // replace space with dash/hyphen
        str = str.replace(/\s+/g, '-');
        $('#' + elem + '').val(str);
        //return str;
    },

    // create confirm dialog
    confirm: function(callbackAction, option = null) {
        if (option == null) {
            option = {
                title: "Are You Sure",
                message: "You sure about this?",
            };
        }
        bootbox.confirm({
            message: option.message,
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function(result) {
                if (result) {
                    callbackAction();
                }
            }
        });
    },

    confirmDelete: function(callbackAction) {
        Helper.confirm(callbackAction, {
            title: "DeLete Data",
            message: "Do you want to delete this data?",
        })
    },

    deleteMsg: function() {
        return 'Data successfully deleted';
    },

    // redirect
    redirectTo: function(url = '') {
        if (url == '') {
            location.reload();
            return;
        }
        window.location.href = Helper.redirectUrl(url);
    },

    // axios handle error
    handleErrorResponse: function(error) {
        console.log('handleErrorResponse', error);
        this.loadingStop();
        if (error.response) {
            // console.log(error.response.data);
            // console.log(error.response.status);
            // console.log(error.response.headers);

            if (error.response.status == 422) {

                messages = error.response.data.data;
                // clean msg
                $('.error-validation-mgs').remove();
                _.each(messages, function(pesan, field) {
                    // append new message
                    $('[name="' + field + '"]').after('<strong><span class="error-validation-mgs" id="error-' + field + '" style="color:red; display:block;">' + pesan[0] + '</span></strong>');
                    // stop loop
                    return false;
                })

            }
            this.errorNotif(error.response.data.msg);
        }

        return this;
    },

    serializeForm: function($el) {
        data = {};
        $.each($el.serializeArray(), function(i, field) {
            if (_.includes(field.name, '[]') !== true) {
                data[field.name] = field.value;
            } else {
                field_name = field.name.replace('[]', '');
                data[field_name] = $('[name="' + field.name + '"]').map(function() {
                    return $(this).val()
                }).get();
            }
        });

        return data;
    },
    currency: function(el) {
        VMasker($('' + el + '')).maskMoney({
            precision: 0,
            separator: ',',
            delimiter: '.',
            unit: 'Rp.',
            zeroCents: false,

        });
    },

    thousandSeparatorMaskInput: function(el) {
        VMasker($('' + el + '')).maskMoney({
            precision: 0,
            separator: ',',
            delimiter: '.',
            zeroCents: false,
        });
    },

    toCurrency: function(val) {
        return new Intl.NumberFormat().format(val);

    },

    unMask: function(el) {
        var el = $('' + el + '')
        return VMasker(el).unMask();
    },

    date: function(el, start, format = 'Y-m-d', func) {
        var date = new Date();
        date.setDate(date.getDate() + start);
        $('' + el + '').datetimepicker({
            minDate: date,
            format: 'Y-m-d',
            formatDate: 'Y-m-d',
            timepicker: false,
            numberOfMonths: 3,
            // inline:true,
        });
    },

    dateScheduleJob: function(el, technician_id = null) {
        var date = new Date();
        date.setDate(date.getDate() + 1);
        $('' + el + '').datetimepicker({
            minDate: date,
            format: 'Y-m-d',
            timepicker: false,
            numberOfMonths: 3,
            onSelectDate: function(ct, $i) {
                    var schedule = moment($i.val()).format('YYYY-MM-DD');
                    Axios.post(Helper.apiUrl('/customer/request-job/hours-available'), {
                            schedule: schedule,
                            technician_id: (technician_id != null) ? technician_id : $('#select-technicians').val() //jika tidak null adalah date yang berada di customer request job, sedangkan null berada di admin create order
                        })
                        .then(function(response) {
                            console.log(response)
                            var list_hours = '';
                            var checked = '';
                            var isChecked = false;
                            $.each(response.data.all_jam, function(index, value) {
                                var disabled = '';
                                var hour = value.replace('is available', '');
                                var hour_label = value.replace('is available', '');
                                if (value.search('is available') >= 0) {
                                    if (isChecked == false) {
                                        // checked = 'checked';
                                        // isChecked = true;
                                    }
                                } else {
                                    disabled = 'disabled';
                                    hour_label = '<font color="red"><strike>' + value + '</strike></font>';
                                }

                                list_hours += (`
                                    <div class="col-md-3 position-relative form-check">
                                        <label class="form-check-label">
                                            <input name="hours" type="radio" class="hours form-check-input" value="${hour}" ${disabled} required> &nbsp;${hour_label}
                                        </label>
                                    </div>
                                `);
                            });
                            list_hours += (`<input name="hours_disable" type="hidden" id="hours_disable" value="${response.data.jam_disable}">`);

                            $('#display_hours_available').html(list_hours);
                            $('#technician_schedules').html(response.data.jadwal_teknisi);
                            jam_teknisi = response.data.jam_teknisi;
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                }
                // inline:true,
        });
    },

    noNegative: function() {
        $(':input[type="number"]').keypress(function(event) {
            if (event.which == 45 || event.which == 189) {
                event.preventDefault();
            }
        });
    }
}

window.DataTablesHelper = {
    table: null,
    config: {
        prefixName: '',
        modalSelector: null,
        modalButtonSelector: null,
        modalFormSelector: null,
        url: '',
        orderBy: [],
        selector: '#table',
        columns: [],
        columnsField: [],
        actionLink: {
            only_icon: true,
            render: null,
            store: '',
            update: '',
            delete: '',
            deleteBatch: '',
            mutation: ''
        },
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        selectable: false,
        buttonDeleteBatch: '.delete-selected',
        buttonImportExcel: '#import',
        modalImportExcel: '#importExcel'

    },
    reloadTable: function(url = null) {
        if (url == null) {
            this.table.ajax.reload();
        } else {
            this.table.ajax.url(url).load();
        }

        return this;
    },
    configuration: function(url, columns) {
        if (DataTablesHelper.config.selectable) {
            return {
                processing: true,
                serverSide: true,
                select: {
                    style: 'single',
                },
                columnDefs: [{
                        orderable: false,
                        searchable: false,
                        defaultContent: '',
                        className: 'select-checkbox text-left',
                        targets: 0,
                        sortable: false,
                    },
                    {
                        targets: 1,
                        className: 'text-center',
                        searchable: false,
                        sortable: false
                            // render: $.fn.dataTable.render.number('.', ',', 2 )
                    }
                ],
                dom: DataTablesHelper.config.dom,
                buttons: [{
                    extend: 'colvis',
                    columnText: function(dt, idx, title) {
                        return (idx + 1) + ': ' + title;
                    }
                }],
                ordering: 'true',
                order: DataTablesHelper.config.orderBy,
                responsive: true,
                language: {
                    buttons: {
                        colvis: '<i class="fa fa-list-ul"></i>'
                    },
                    search: '',
                    searchPlaceholder: "Search...",
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                },
                ajax: {
                    url: Helper.apiUrl(url),
                    "type": "get",
                    "data": function(d) {
                        return $.extend({}, d, {
                            "extra_search": $('#extra').val()
                        });
                    }
                },
                columns: columns
            }
        } else {
            return {
                processing: true,
                serverSide: true,
                dom: 'Bflrtip',
                responsive: true,
                ordering: 'true',
                order: DataTablesHelper.config.orderBy,
                columnDefs: [{
                    targets: 0,
                    width: "10%",
                    searchable: false,
                    sortable: false
                        // render: $.fn.dataTable.render.number('.', ',', 2 )
                }],
                language: {
                    buttons: {
                        colvis: '<i class="fa fa-list-ul"></i>'
                    },
                    search: '',
                    searchPlaceholder: "Search...",
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                },
                dom: DataTablesHelper.config.dom,
                buttons: [{
                        extend: 'colvis'
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function(e, dt, node, config) {
                            dt.ajax.reload();
                        }
                    }
                ],
                ajax: {
                    url: Helper.apiUrl(url),
                    "type": "get",
                    "data": function(d) {
                        return $.extend({}, d, {
                            "extra_search": $('#extra').val()
                        });
                    }
                },
                columns: columns
            }
        }
    },
    make: function(selector, url, columns) {
        return $(selector).DataTable(this.configuration(url, columns));
    },
    generateColumn: function(columnsField, config) {
        columns = [];
        _.each(columnsField, function(val, key) {
            if (typeof val === 'string') {
                columns.push({
                    data: val,
                    name: val,
                })
            } else {
                columns.push(val)
            }
        });

        prefixClass = config.selector.replace('#', '');

        columns.push({
            data: 'id',
            name: 'id',
            searchable: false,
            orderable: false,
            render: function(data, type, full) {
                edit_btn = "<button data-id='" + full.id + "' class='" + prefixClass + "-edit btn-hover-shine btn btn-warning btn-sm btn-white'><i class='fa fa-pencil'></i> Edit</button>";

                delete_btn = "<button data-id='" + full.id + "'  class='" + prefixClass + "-delete btn-hover-shine btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</button>";
                if (config.actionLink.only_icon == false) {
                    edit_btn = "<button data-id='" + full.id + "' class='" + prefixClass + "-edit btn-hover-shine btn btn-warning btn-sm btn-white'><i class='fa fa-pencil'></i></button>";

                    delete_btn = "<button data-id='" + full.id + "'  class='" + prefixClass + "-delete btn-hover-shine btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
                }
                if (config.actionLink.render != null) {
                    actionData = {
                        edit_btn,
                        delete_btn,
                    }
                    return config.actionLink.render(data, type, full, actionData);
                }

                return edit_btn + " " + delete_btn;
            }
        });


        return columns;
    },
    setConfig: function(configuration) {
        new_config = this.config;
        _.each(configuration, function(val, key) {
            new_config[key] = val;
        })
        this.config = new_config;

        return new_config;
    },
    generate: function(configuration) {
        config = this.setConfig(configuration);

        // hide btn delete batch
        if (config.selectable) {
            $(config.buttonDeleteBatch).hide();
        }

        if (config.columns.length) {
            this.table = this.make(
                config.selector, config.url, config.columns
            );
        }

        if (config.columnsField.length) {
            this.table = this.make(
                config.selector, config.url, this.generateColumn(config.columnsField, config)
            );

            // event here
            this.datatablesEvent();
        }

        return this;
    },
    datatablesEvent: function() {
        config = this.config;

        prefixClass = config.selector.replace('#', '');
        this.prefixName = prefixClass;

        // show btn delete batch
        if (config.selectable) {
            DataTablesHelper.table.on('deselect', function(e, dt, type, indexes) {
                if (type === 'row') {
                    var data = DataTablesHelper.table.rows('.selected').data();
                    console.log('deselect', data.length)
                    if (data.length) {
                        $(config.buttonDeleteBatch).show();
                    } else {
                        $(config.buttonDeleteBatch).hide();
                    }
                }
            });

            DataTablesHelper.table.on('select', function(e, dt, type, indexes) {
                if (type === 'row') {
                    var data = DataTablesHelper.table.rows('.selected').data();
                    console.log('data.length', data.length)
                    if (data.length) {
                        $(config.buttonDeleteBatch).show();
                    } else {
                        $(config.buttonDeleteBatch).hide();
                    }
                }
            });
        }

        // edit data
        $(document)
            .on('click', "." + prefixClass + "-edit", function() {
                var row = DataTablesHelper.table.row($(this).parents('tr')).data();

                if (config.modalSelector !== null) {
                    ModalHelper.modalShow('Edit ' + prefixClass, row);
                } else {
                    Helper.redirectTo(config.actionLink.update(row));
                }

            })

        // delete data
        $(document).on('click', "." + prefixClass + "-delete", function() {
                var row = DataTablesHelper.table.row($(this).parents('tr')).data();
                globalCRUD.delete(config.actionLink.delete(row))
            })
            // jika selected row treu
        if (config.selectable) {
            // console.log(config.buttonDeleteBatch);
            $(document).on('click', config.buttonDeleteBatch, function() {
                var ids = $.map(DataTablesHelper.table.rows('.selected').data(), function(item) {
                    return item.id;
                });
                globalCRUD.delete(config.actionLink.deleteBatch(ids), {
                    id: ids
                });
                console.log(ids)
            });
        }
        if (config.import) {
            $(document).on('click', config.buttonImportExcel, function() {
                var fd = new FormData();
                var files = $('#file')[0].files[0];
                if (files) {
                    fd.append('file', files);
                }
                globalCRUD.import(config.actionLink.import(), fd);
                setTimeout(function() {
                    $(config.modalImportExcel).hide();
                }, 1500);
            });
        }
    },

}

window.ModalHelper = {
    modul: '',
    modal: null,
    button: null,
    form: null,
    config: {},
    generate: function(configuration) {
        this.config = configuration;
        this.modal = $(configuration.modalSelector).modal({
            show: false
        });
        this.button = configuration.modalButtonSelector;
        this.modul = configuration.prefixName;
        this.form = configuration.modalFormSelector;
        if (configuration.modalFormSelector !== null) {
            this.modalEvent();
        }

        return this;
    },
    modalEvent: function(configuration) {
        // add data
        $(document).on('click', this.button, function() {
            textModal = 'Add ' + ModalHelper.modul;
            ModalHelper.modalShow(textModal.toUpperCase());
        });

        // submit data
        $(document).on('submit', this.form, function(e) {
            data = Helper.serializeForm($(ModalHelper.form));
            if (data.id == '') {
                globalCRUD.store(ModalHelper.config.actionLink.store(), data);
            } else {
                globalCRUD.update(ModalHelper.config.actionLink.update(data), data);
            }
            e.preventDefault();
        });
    },
    modalShow: function(text, row = null) {
        // clean msg
        $('.error-validation-mgs').remove();

        this.modal.modal('show');
        if (row == null) {
            this.kosongkanDataFormModal();
        } else {
            this.isiDataFormModal(row)
        }
    },
    modalClose: function() {
        this.modal.modal('hide');
    },
    kosongkanDataFormModal: function() {
        row = Helper.serializeForm($(this.form));
        _.each(row, function(val, key) {
            $('[name="' + key + '"]').val('').change();
        })
    },
    isiDataFormModal: function(row) {
        _.each(row, function(val, key) {

            if (key == 'product_part_category_id') {
                console.log(key, val)
            }
            $('[name="' + key + '"]').val(val).change();
        })
    }
};

window.globalCRUD = {
    requestAjax: null,
    redirect: '/',
    successHandle: null,
    errorHandle: null,
    table: null,
    form: null,
    modal: null,
    datatables: function(configuration) {
        table = DataTablesHelper.generate(configuration);
        this.table = table;

        if (table.config.modalSelector !== null) {
            this.modal = ModalHelper.generate(DataTablesHelper.config);
        }

        return this;
    },
    show: function(url) {
        return Axios.get(url);
    },
    store: function(url, data) {
        Helper.loadingStart();
        store = Axios.post(url, data);

        this.requestAjax = store;

        store.then(function(response) {
                if (globalCRUD.successHandle !== null) {
                    globalCRUD.successHandle(response);
                } else {
                    globalCRUD.getDefaultSuccessHandle(response);
                }
            })
            .catch(function(error) {
                if (globalCRUD.errorHandle !== null) {
                    globalCRUD.errorHandle(error);
                } else {
                    globalCRUD.getDefaultErrorHandle(error);
                }
            });
    },
    storeTo: function(url, data = null) {
        if (data == null) {
            data = Helper.serializeForm(this.form);
        }

        if (typeof data === 'function') {
            data = data(Helper.serializeForm(this.form));
        }

        if (typeof url === 'function') {
            this.store(url, data);
        } else {
            this.store(url, data);
        }

        return this;
    },
    update: function(url, data) {
        Helper.loadingStart();
        update = Axios.put(url, data);

        this.requestAjax = update;

        update.then(function(response) {
                if (globalCRUD.successHandle !== null) {
                    globalCRUD.successHandle(response);
                } else {
                    globalCRUD.getDefaultSuccessHandle(response);
                }
            })
            .catch(function(error) {
                if (globalCRUD.errorHandle !== null) {
                    globalCRUD.errorHandle(error);
                } else {
                    globalCRUD.getDefaultErrorHandle(error);
                }
            });
    },
    updateTo: function(url, data = null) {
        if (data == null) {
            data = Helper.serializeForm(this.form);
        }

        if (typeof data === 'function') {
            data = data(Helper.serializeForm(this.form));
        }

        if (typeof url === 'function') {
            this.update(url(data), data);
        } else {
            this.update(url, data);
        }

        return this;
    },
    delete: function(url, data = null) {
        Helper.confirmDelete(function() {
            Helper.loadingStart();
            Axios.delete(url, {
                    data: data
                }).then(function(response) {
                    globalCRUD.axiosResponseSuccess(response);
                })
                .catch(function(error) {
                    globalCRUD.axiosResponseError(error);
                })
        })
    },
    import: function(url, data = null) {
        Axios.post(url, data).then(function(response) {
            globalCRUD.axiosResponseSuccess(response);
            console.log('response', response);
        }).catch(function(error) {
            globalCRUD.axiosResponseError(error);
            console.log('error', error);
        })
    },
    handleSubmit: function($el) {
        this.form = $el;
        return this;
    },
    getDefaultSuccessHandle: function(response) {
        Helper.loadingStop();

        if (response.status == 204) {
            // send notif
            Helper.successNotif(Helper.deleteMsg());
        } else {
            // send notif
            Helper.successNotif(response.data.msg);
        }

        if (this.table !== null) {
            this.table.reloadTable();
        }

        if (table.config.modalSelector !== null) {
            ModalHelper.modalClose();
        }

        if (this.redirect !== '/') {
            Helper.redirectTo(this.redirect());
        }
    },
    getDefaultErrorHandle: function(error) {
        Helper.loadingStop();
        Helper.handleErrorResponse(error)
    },
    axiosResponseSuccess: function(response) {
        if (globalCRUD.successHandle !== null) {
            globalCRUD.successHandle(response);
        } else {
            globalCRUD.getDefaultSuccessHandle(response);
        }
    },
    axiosResponseError: function(error) {
        if (globalCRUD.errorHandle !== null) {
            globalCRUD.errorHandle(error);
        } else {
            globalCRUD.getDefaultErrorHandle(error);
        }
    },
    redirectTo: function(url, full_url = false) {
        if (this.requestAjax !== null) {
            this.requestAjax.then(function(response) {
                if (typeof url === 'function') {
                    if (full_url) {
                        window.location.href = url(response);
                    } else {
                        Helper.redirectTo(url(response));
                    }
                } else {
                    Helper.redirectTo(url);
                }
            })
        }

        if (this.requestAjax == null) {
            Helper.redirectTo(url);
        }
    },
    backTo: function(url) {
        this.redirectTo(url, true);
    },
    select2Static: function(selector, url, callback = null) {
        Axios.get(url)
            .then(function(response) {
                data = response.data.data;

                if (callback !== null) {
                    res = $.map(data, callback);
                } else {
                    res = $.map(data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });
                }

                $(selector).select2({
                    data: res
                });
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        return this;
    },
    select2: function(selector, url = null, callback = null) {
        if (url == null) {
            $(selector).select2();
            return this;
        }

        $(selector).select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl(url),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    if (callback !== null) {
                        var res = $.map(data.data, callback);
                    }
                    return {
                        results: res
                    };
                }
            },
            closeOnSelect: true
        });
        return this;
    }
}