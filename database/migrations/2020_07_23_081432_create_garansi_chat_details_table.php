<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGaransiChatDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garansi_chat_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('garansi_chat_id')->unsigned()->index();
            $table->foreign('garansi_chat_id')
                ->references('id')
                ->on('garansi_chats')
                ->onDelete('cascade');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->longText('message')->nullable();
            $table->longText('order_json')->nullable();
            $table->text('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('garansi_chat_details');
    }
}
