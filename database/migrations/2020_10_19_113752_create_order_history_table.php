<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_history', function (Blueprint $table) {
            $table->id();
            $table->integer('order_id');
            $table->string('order_code');
            $table->string('service_type');
            $table->string('price'); 
            $table->double('beginning_balance');
            $table->double('ending_balance');
            $table->double('number_of_pieces');
            $table->double('commission_to_technician')->nullable();
            $table->string('status');
            $table->string('technician_name')->nullable();
            $table->string('type_history')->nullable();
            $table->string('symptom_name')->nullable();
            $table->string('total_penalty')->nullable();
            $table->string('product_group_name')->nullable();
            $table->text('parts')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_history');
    }
}
