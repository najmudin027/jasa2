<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsPriceServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_price_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ms_services_types_id')->unsigned()->index();
            $table->foreign('ms_services_types_id')
                ->references('id')
                ->on('ms_services_types');
            $table->bigInteger('technicians_id')->unsigned()->index();
            $table->foreign('technicians_id')
                ->references('id')
                ->on('technicians')
                ->onDelete('cascade');
            $table->bigInteger('product_group_id')->unsigned()->index();
            $table->foreign('product_group_id')
                ->references('id')
                ->on('product_groups');
            $table->double('value');
            $table->integer('commission')->nullable();
            $table->double('commission_value')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_price_services');
    }
}
