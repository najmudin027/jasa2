<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatchItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('item_name');
            $table->double('value');
            $table->integer('quantity');
            $table->integer('quantity_return')->nullable();
            $table->string('reason')->nullable();
            $table->string('filename')->nullable();
            $table->tinyInteger('is_consigment')->default(1)->comment('barang punya perusahaan atau pribadi');
            $table->tinyInteger('is_accepted')->default(0)->comment('0 belum diterima / status shipment == receive, 1 sudah di terima');
            $table->tinyInteger('is_confirmed')->default(0)->comment('0 belum konfirmasi barang ketika barang ssudah received , 1 sudah confirm barang (bisa return / tetap) ');
            $table->tinyInteger('is_inventory')->default(0)->comment('0 belum masuk inventory, 1 sudah masuk inventory');
            $table->bigInteger('batch_status_id')->unsigned()->index();
            $table->foreign('batch_status_id')
                ->references('id')
                ->on('batch_statuses');
            $table->timestamps();

            $table->bigInteger('product_id')->unsigned()->index()->nullable();
            $table->foreign('product_id')
                ->references('id')
                ->on('ms_products');

            $table->bigInteger('product_vendor_id')->unsigned()->index()->nullable();
            $table->foreign('product_vendor_id')
                ->references('id')
                ->on('product_vendors');

            $table->bigInteger('product_varian_id')->unsigned()->index()->nullable();
            $table->foreign('product_varian_id')
                ->references('id')
                ->on('product_varians');

            $table->bigInteger('batch_id')->unsigned()->index();
            $table->foreign('batch_id')
                ->references('id')
                ->on('batches')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_items');
    }
}
