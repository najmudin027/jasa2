<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductAttributeTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_attribute_terms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('color_code')->nullable();
            $table->string('hex_rgb')->nullable();
            $table->timestamps();
            $table->bigInteger('product_attribute_id')->unsigned()->index();
            $table->foreign('product_attribute_id')
                ->references('id')
                ->on('product_attributes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_attribute_terms');
    }
}
