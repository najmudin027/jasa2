<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255)->comment("Nama Product");
            $table->string('slug', 255)->comment("slug");
            $table->timestamp('publish_at')->nullable();
            $table->text('deskripsi');
            $table->string('sku')->nullable();
            $table->text('product_attribute_header')->nullable();
            $table->integer('weight')->nullable();
            $table->integer('length')->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->string('power', 150)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('ms_services_id')->unsigned()->index()->nullable();
            
            $table->bigInteger('galery_image_id')->unsigned()->index()->nullable();
            $table->foreign('galery_image_id')
                ->references('id')
                ->on('galery_image');

            $table->bigInteger('product_status_id')->unsigned()->index();
            $table->foreign('product_status_id')
                ->references('id')
                ->on('product_statuses');
            
            $table->bigInteger('product_model_id')->unsigned()->index();
            $table->foreign('product_model_id')
                ->references('id')
                ->on('product_models');
            
            $table->bigInteger('product_additionals_id')->unsigned()->index()->nullable();
            $table->foreign('product_additionals_id')
                ->references('id')
                ->on('product_additionals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_products');
    }
}
