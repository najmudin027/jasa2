<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmpSparepartDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_sparepart_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('orders_id')->nullable();
            $table->integer('technicians_sparepart_id')->nullable();
            $table->integer('technicians_id')->nullable();            
            $table->text('name_sparepart')->nullable();
            $table->double('price')->nullable();
            $table->integer('quantity')->nullable();
            // $table->longText('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_sparepart_details');
    }
}
