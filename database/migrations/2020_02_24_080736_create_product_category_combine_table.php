<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoryCombineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_category_combine', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('ms_product_id')->unsigned()->index();
            $table->foreign('ms_product_id')
                ->references('id')
                ->on('ms_products')
                ->onDelete('cascade');

            $table->bigInteger('product_category_id')->unsigned()->index();
            $table->foreign('product_category_id')
                ->references('id')
                ->on('ms_product_category')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_category_combine');
    }
}
