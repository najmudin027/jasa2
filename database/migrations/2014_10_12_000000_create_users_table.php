
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->tinyInteger('is_online')->default(0)->comment('0 offline, 1 online');
            $table->string('email')->unique()->nullable();
            $table->string('phone')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('otp_verified_at')->nullable();
            $table->string('otp_code')->nullable();
            $table->string('otp_url')->nullable();
            $table->string('device_token')->nullable();
            $table->string('api_token', 80)->unique()->nullable()->default(null);
            $table->string('password');
            $table->string('status')->default(1);
            $table->tinyInteger('is_login_as_teknisi')->nullable();
            $table->text('auth_token')->nullable();
            $table->integer('badge_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
