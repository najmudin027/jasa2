<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechniciansSparepartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technicians_spareparts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ms_services_id')->unsigned()->index();
            $table->foreign('ms_services_id')
                ->references('id')
                ->on('ms_services');
            $table->bigInteger('technicians_id')->unsigned()->index()->nullable();
            $table->foreign('technicians_id')
                ->references('id')
                ->on('technicians');
            $table->text('name', 255);
            $table->double('price');
            $table->longText('description');
            $table->text('images')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technicians_spareparts');
    }
}
