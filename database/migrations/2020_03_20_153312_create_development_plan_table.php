<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevelopmentPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('development_plan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status');
            $table->date('date_time');
            $table->timestamps();

            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->bigInteger('curr_sequence_id')->unsigned()->index();
            $table->foreign('curr_sequence_id')
                ->references('id')
                ->on('curriculum_squence');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('development_plan');
    }
}
