<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_experiences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('position', 30)->comment('Posisi');
            $table->string('company_name', 30)->comment('Nama perusahaan');
            $table->date('period_start')->comment('Tahun mulai kerja');
            $table->date('period_end')->comment('Tahun akhir kerja');
            $table->tinyInteger('type')->comment('Pengalaman internal atau eksternal');
            $table->timestamps();
            $table->softDeletes();

            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->bigInteger('job_title_id')->unsigned()->index();
            $table->foreign('job_title_id')
                ->references('id')
                ->on('job_titles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_experiences');
    }
}
