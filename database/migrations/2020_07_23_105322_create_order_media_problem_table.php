<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderMediaProblemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_media_problem', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('orders_id')->unsigned()->index();
            $table->foreign('orders_id')
                ->references('id')
                ->on('orders');
            $table->string('filename');
            $table->string('extension',10);
            $table->enum('type', ['image', 'video']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_media_problem');
    }
}
