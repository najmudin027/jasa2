<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsProductCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_product_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255)->comment("Nama Product Category");
            $table->string('product_cate_code', 20)->comment("Kategori Product Code");
            $table->string('slug', 255)->comment("Slug untuk URL");
            $table->bigInteger('parent_id')->nullable();
            $table->bigInteger('order')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_product_category');
    }
}
