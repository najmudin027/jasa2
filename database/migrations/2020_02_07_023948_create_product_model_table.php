<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductModelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_models', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('model_code', 20)->comment("Kategori Product Code");
            $table->string('name', 255)->comment("Nama Product Category");
            $table->timestamps();
            $table->softDeletes();

            $table->bigInteger('product_brands_id')->unsigned()->index();
            $table->bigInteger('ms_product_category_id')->unsigned()->index();
            
            $table->foreign('ms_product_category_id')
                ->references('id')
                ->on('ms_product_category');
            $table->foreign('product_brands_id')
                ->references('id')
                ->on('product_brands');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_models');
        
    }
}
