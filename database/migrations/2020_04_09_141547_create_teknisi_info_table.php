<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeknisiInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teknisi_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('attachment')->nullable();
            $table->string('no_identity')->nullable();

            $table->bigInteger('ms_curriculum_id')->unsigned()->index()->nullable();
            $table->foreign('ms_curriculum_id')
                ->references('id')
                ->on('development_plan');

            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teknisi_infos');
    }
}
