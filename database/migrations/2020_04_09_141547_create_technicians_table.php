<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechniciansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technicians', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('rekening_bank_name')->nullable();
            $table->string('rekening_number')->nullable();
            $table->string('rekening_name')->nullable();
            $table->string('skill')->nullable();
            $table->integer('status')->default(1);
            $table->bigInteger('job_title_id')->unsigned()->index()->nullable();
            $table->foreign('job_title_id')
                ->references('id')
                ->on('job_titles');

            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technicians');
    }
}
