<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatchItemReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_item_reasons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('batch_item_id')->unsigned()->index();
            $table->foreign('batch_item_id')
                ->references('id')
                ->on('batch_items');
            $table->string('filename');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_item_reasons');
    }
}
