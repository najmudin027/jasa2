<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsVillagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_villages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('meta')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->bigInteger('ms_district_id')->unsigned()->index();
            $table->foreign('ms_district_id')
                ->references('id')
                ->on('ms_districts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_villages');
    }
}
