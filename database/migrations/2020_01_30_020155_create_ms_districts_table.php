<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_districts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('meta')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->bigInteger('ms_city_id')->unsigned()->index();
            $table->foreign('ms_city_id')
                ->references('id')
                ->on('ms_cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_districts');
    }
}
