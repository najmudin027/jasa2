<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatingAndReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating_and_reviews', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('rating_id')->unsigned()->index();
            $table->foreign('rating_id')
                ->references('id')
                ->on('ratings');
            $table->bigInteger('review_id')->unsigned()->index();
            $table->foreign('review_id')
                ->references('id')
                ->on('reviews');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rating_and_reviews');
    }
}
