<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEWalletHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_wallet_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ms_wallet_id');
            // $table->foreign('ms_wallet_id')
            //     ->references('id')
            //     ->on('e_wallet');
            $table->integer('type_transaction_id')->nullable();
            $table->string('attachment')->nullable();
            $table->integer('transfer_status_id')->nullable();
            $table->string('payment_metod_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('number_of_pieces')->nullable();            
            // $table->foreign('user_id')
            //     ->references('id')
            //     ->on('users');
            $table->string('nama_rekening')->nullable();
            $table->integer('saldo')->nullable();
            $table->integer('nominal')->nullable();
            $table->text('payment_token')->nullable();
            $table->text('payment_url')->nullable();
            $table->string('status')->nullable();
            $table->string('payment_status')->nullable();
            $table->string('order_code')->nullable();
            $table->integer('payment_type_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_wallet_history');
    }
}
