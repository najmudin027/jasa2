<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailOtherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_other', function (Blueprint $table) {
            $table->id();
            $table->string('name_module');
            $table->string('description')->nullable();
            $table->string('subject');
            $table->text('content');
            $table->tinyInteger('type')->comment('1.customer 2.technician 3.admin	');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_other');
    }
}
