<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceItemsssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('value');
            
            $table->bigInteger('ms_unit_type_id')->unsigned()->index();
            $table->foreign('ms_unit_type_id')
                ->references('id')
                ->on('unit_types');

            $table->bigInteger('ms_unit_type_id')->unsigned()->index();
                $table->foreign('ms_unit_type_id')
                    ->references('id')
                    ->on('unit_types');

            $table->bigInteger('ms_city_id')->unsigned()->index();
            $table->foreign('ms_city_id')
                ->references('id')
                ->on('ms_cities');

            $table->bigInteger('ms_district_id')->unsigned()->index();
            $table->foreign('ms_district_id')
                ->references('id')
                ->on('ms_districts');

            $table->bigInteger('ms_village_id')->unsigned()->index();
            $table->foreign('ms_village_id')
                ->references('id')
                ->on('ms_villages');

            $table->bigInteger('ms_product_id')->unsigned()->index();
            $table->foreign('ms_product_id')
                ->references('id')
                ->on('ms_products');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_items');
    }
}
