<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('description');
            $table->timestamps();

            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->bigInteger('order_id')->unsigned()->index();
            $table->foreign('order_id')
                ->references('id')
                ->on('orders');

            $table->bigInteger('technician_id')->unsigned()->index()->nullable();
            $table->foreign('technician_id')
                ->references('id')
                ->on('technicians');

            $table->bigInteger('id_tech_team')->unsigned()->index()->nullable();
            $table->foreign('id_tech_team')
                ->references('id')
                ->on('ms_tech_teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
