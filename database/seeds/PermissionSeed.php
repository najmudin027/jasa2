<?php

use App\Model\Master\MsAdditionalInfo;
use App\Model\Master\WebSetting;
use App\Model\Menu;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            $prefix_admin = '/admin';
            // insert permiison
            Permission::insert($this->dataPermission($prefix_admin));
            // insert role
            Role::insert($this->dataRole($prefix_admin));
            // sync Permissions
            Role::where('name', 'admin')->first()->givePermissionTo(Permission::all());
            Role::where('name', 'ngga.bisa.delete')->first()->givePermissionTo(Permission::where('name', 'not like', '%delete%')->get());
            Role::where('name', 'Technician')->first()->givePermissionTo(Permission::where('name', 'dashboard')->get());
            Role::where('name', 'Customer')->first()->givePermissionTo(Permission::where('name', 'dashboard')->get());
            // create user
            User::create($this->dataUser($prefix_admin))->assignRole(Role::where('name', 'admin')->first());
            User::create($this->dataUser2($prefix_admin))->assignRole(Role::where('name', 'ngga.bisa.delete')->first());
            User::create($this->dataUserCustomer())->assignRole(Role::where('name', 'Customer')->first());
            User::create($this->dataUserTeknisi())->assignRole(
                Role::whereIn('name', ['Customer', 'Technician'])->get()
            );
            // menu create
            $this->getMenus($prefix_admin);

            MsAdditionalInfo::create([
                'date_of_birth'  => date("Y-m-d"),
                'first_name'     => 'teknisi',
                'user_id'        => 4,
                'gender'         => 1,
                'last_name'      => 'teknisi'
            ]);

            $web_settings = array(
                array('id' => '2', 'title_setting' => 'Astech', 'metta_setting' => 'Absolute Solution For Technologies', 'logo' => '1605863516.png', 'created_at' => '2020-11-20 15:00:21', 'updated_at' => '2020-11-20 16:11:56', 'image_url' => 'https://dev.protustanuhandaru.com/public/web_setting_image/1605863516.png')
            );
            WebSetting::insert($web_settings);

            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            dd($e);
        }
    }

    public function dataPermission($prefix_admin)
    {
        $data   = [];
        $moduls = [
            'setting',
            'gallery_image',
            'chat',

            // tecnician
            'technician',
            'job_title_category',
            'job_title',
            'job_experience',

            // list transaksi
            // tickets
            'ticket',
            'permission',
            'role',
            'user',
            'category',
            'menu',
            'country',
            'province',
            'city',
            'district',
            'village',
            'zipcode',
            'address',
            'product_attribute',
            'product_additional',
            'product_brand',
            'product_part_category',
            'product',
            'vendor',
            'product_category',
            'model',
            'product_model',
            'religion',
            'marital',
            'batch',
            'batch_status',
            'address_type',
            'unit_type',
            'warehouse',
            'price_item',
            'inventory',
            'import_excel',
            'profile',
            'user_group',
            'list_address',
            'curriculum',
            'education',

            'product_status',
            'user_status',
            'development',
            'development_program',
            'additional_info',
            'services',
            'symptom',
            'price_services',
            'service_type',
            'service_status',
            'order_status',
            'technician_sparepart',
            'team',
            'grade',
            'tat_grup',
            'product_group',
            'order',
            'garansi',

        ];
        foreach ($moduls as $modul) {
            $data[] = ['guard_name' => config('auth.defaults.guard'), 'name' => $modul . '.show', 'groups' => $modul];
            $data[] = ['guard_name' => config('auth.defaults.guard'), 'name' => $modul . '.create', 'groups' => $modul];
            $data[] = ['guard_name' => config('auth.defaults.guard'), 'name' => $modul . '.update', 'groups' => $modul];
            $data[] = ['guard_name' => config('auth.defaults.guard'), 'name' => $modul . '.delete', 'groups' => $modul];
        }

        $data[] = ['guard_name' => config('auth.defaults.guard'), 'name' => 'dashboard', 'groups' => 'dashboard'];

        return $data;
    }

    public function dataRole($prefix_admin)
    {
        return [
            ['name' => 'ngga.bisa.delete', 'guard_name' => config('auth.defaults.guard')],
            ['name' => 'admin', 'guard_name' => config('auth.defaults.guard')],
            ['name' => 'Technician', 'guard_name' => config('auth.defaults.guard')],
            ['name' => 'Customer', 'guard_name' => config('auth.defaults.guard')],

        ];
    }

    public function dataUser($prefix_admin)
    {
        return [
            'name'              => 'admin',
            'email'             => 'admin@admin.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('password'), // password
            'remember_token'    => Str::random(10),
            'api_token'         => Str::random(80),
        ];
    }
    public function dataUser2($prefix_admin)
    {
        return [
            'name'              => 'admin yg ga bisa delete',
            'email'             => 'admin@delete.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('password'), // password
            'remember_token'    => Str::random(10),
            'api_token'         => Str::random(80),
        ];
    }
    public function dataUserCustomer()
    {
        return [
            'name'              => 'customer',
            'email'             => 'customer@customer.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('password'), // password
            'remember_token'    => Str::random(10),
            'api_token'         => Str::random(80),
        ];
    }
    public function dataUserTeknisi()
    {
        return [
            'name'              => 'teknisi',
            'email'             => 'teknisi@teknisi.com',
            'email_verified_at' => now(),
            'password'          => Hash::make('password'), // password
            'remember_token'    => Str::random(10),
            'api_token'         => Str::random(80),
        ];
    }

    public function dataMenus($prefix_admin)
    {
        $setPermission         = Permission::where('name', 'permission.show')->first();
        $Role                  = Permission::where('name', 'role.show')->first();
        $User                  = Permission::where('name', 'user.show')->first();
        $Category              = Permission::where('name', 'category.show')->first();
        $menu                  = Permission::where('name', 'menu.show')->first();
        $country               = Permission::where('name', 'country.show')->first();
        $city                  = Permission::where('name', 'city.show')->first();
        $district              = Permission::where('name', 'district.show')->first();
        $village               = Permission::where('name', 'village.show')->first();
        $zipcode               = Permission::where('name', 'zipcode.show')->first();
        $address               = Permission::where('name', 'address.show')->first();
        $product_attribute     = Permission::where('name', 'product_attribute.show')->first();
        $product_additional    = Permission::where('name', 'product_additional.show')->first();
        $product_brand         = Permission::where('name', 'product_brand.show')->first();
        $product_part_category = Permission::where('name', 'product_part_category.show')->first();
        $product               = Permission::where('name', 'product.show')->first();
        $vendor                = Permission::where('name', 'vendor.show')->first();
        $product_category      = Permission::where('name', 'product_category.show')->first();
        $model                 = Permission::where('name', 'model.show')->first();
        $product_model         = Permission::where('name', 'product_model.show')->first();
        $religion              = Permission::where('name', 'religion.show')->first();
        $marital               = Permission::where('name', 'marital.show')->first();
        $batch                 = Permission::where('name', 'batch.show')->first();
        $batch_status          = Permission::where('name', 'batch_status.show')->first();
        $address_type          = Permission::where('name', 'address_type.show')->first();
        $unit_type             = Permission::where('name', 'unit_type.show')->first();
        $warehouse             = Permission::where('name', 'warehouse.show')->first();
        $price_item            = Permission::where('name', 'price_item.show')->first();
        $inventory             = Permission::where('name', 'inventory.show')->first();
        $import_excel          = Permission::where('name', 'import_excel.show')->first();
        $profile               = Permission::where('name', 'profile.show')->first();
        $user_group            = Permission::where('name', 'user_group.show')->first();
        $list_address          = Permission::where('name', 'list_address.show')->first();
        $curriculum            = Permission::where('name', 'curriculum.show')->first();
        $education             = Permission::where('name', 'education.show')->first();
        $job_title_category    = Permission::where('name', 'job_title_category.show')->first();
        $job_title             = Permission::where('name', 'job_title.show')->first();
        $job_experience        = Permission::where('name', 'job_experience.show')->first();
        $product_status        = Permission::where('name', 'product_status.show')->first();
        $galleryImages         = Permission::where('name', 'galleryImages.show')->first();
        $gallery_image         = Permission::where('name', 'gallery_image.show')->first();
        $user_status           = Permission::where('name', 'user_status.show')->first();
        $development           = Permission::where('name', 'development.show')->first();
        $development_program   = Permission::where('name', 'development_program.show')->first();
        $bank_transfer         = Permission::where('name', 'bank_transfer.show')->first();
        $additional_info       = Permission::where('name', 'additional_info.show')->first();
        // dd($setPermission->id);



        $MasterData = Menu::create( // id = 2
            [
                'name'          => 'Galery Images',
                'url'           => '' . $prefix_admin . '/gallery-image/show-main',
                'module'        => 'gallery-image',
                'icon'          => 'fa fa-folder-open-o',
                'permission_id' => $galleryImages->id,
                'display_order' => '2',
            ]
        );

        // $shippingBatch = Menu::create( // id = 50
        //     [
        //         'name' => 'Shipping Batch',
        //         'url' => '' . $prefix_admin . '/batch/show',
        //         'module' => 'batch',
        //         'icon' => 'fa fa-truck',
        //         'permission_id' => $setPermission->id,
        //         // 'parent_id' =>  $inventory->id,
        //         'display_order' => '6'
        //     ]
        // );
        // $shippingBatchStatus = Menu::create( // id = 50
        //     [
        //         'name' => 'Shipping Batch Status',
        //         'url' => '' . $prefix_admin . '/batch/status',
        //         'module' => 'batch',
        //         'icon' => 'fa fa-truck',
        //         'permission_id' => $setPermission->id,
        //         // 'parent_id' =>  $inventory->id,
        //         'display_order' => '7'
        //     ]
        // );

        $MasterData = Menu::create( // id = 2
            [
                'name'          => 'Master Data',
                'url'           => '#',
                'icon'          => 'fa fa-meetup',
                'permission_id' => $setPermission->id,
                'display_order' => '2',
            ]
        );

        $MasterProduct = Menu::create( // id = 3
            [
                'name'          => 'Master Product',
                'url'           => '#',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $MasterData->id,
                'display_order' => '1',
            ]
        );

        $MasterLocations = Menu::create( // id = 4
            [
                'name'          => 'Master Location',
                'url'           => '#',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $MasterData->id,
                'display_order' => '2',
            ]
        );

        $inventory = Menu::create( //id = 6
            [
                'name'          => 'Inventory',
                'url'           => '#',
                'icon'          => 'fa fa-database',
                'permission_id' => $setPermission->id,
                'display_order' => '3',
            ]
        );

        $userProfile = Menu::create( // id = 5
            [
                'name'          => 'User Management',
                'url'           => '#',
                'icon'          => 'fa fa-user',
                'permission_id' => $setPermission->id,
                'display_order' => '4',
            ]
        );

        $curriculum = Menu::create( // id = 5
            [
                'name'          => 'Curriculum',
                'url'           => '#',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $userProfile->id,
                'display_order' => '2',
            ]
        );

        $seting = Menu::create( //id = 6
            [
                'name'          => 'Setting',
                'url'           => '#',
                'icon'          => 'fa fa-cog',
                'permission_id' => $setPermission->id,
                'display_order' => '5',
            ]
        );

        $menus = [
            [
                'name'          => 'Country',
                'url'           => '' . $prefix_admin . '/country/show',
                'module'        => 'country',
                'icon'          => '#',
                'permission_id' => $country->id,
                'parent_id'     => $MasterLocations->id,
                'display_order' => '1',
            ],
            [
                'name'          => 'Province',
                'url'           => '' . $prefix_admin . '/province/show',
                'module'        => 'province',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $MasterLocations->id,
                'display_order' => '2',
            ],
            [
                'name'          => 'City',
                'url'           => '' . $prefix_admin . '/city/show',
                'module'        => 'city',
                'icon'          => '#',
                'permission_id' => $city->id,
                'parent_id'     => $MasterLocations->id,
                'display_order' => '3',
            ],

            [
                'name'          => 'District',
                'url'           => '' . $prefix_admin . '/district/show',
                'module'        => 'district',
                'icon'          => '#',
                'permission_id' => $district->id,
                'parent_id'     => $MasterLocations->id,
                'display_order' => '4',
            ],
            [
                'name'          => 'Village',
                'url'           => '' . $prefix_admin . '/village/show',
                'module'        => 'village',
                'icon'          => '#',
                'permission_id' => $village->id,
                'parent_id'     => $MasterLocations->id,
                'display_order' => '5',
            ],
            [
                'name'          => 'Zipcode',
                'url'           => '' . $prefix_admin . '/zipcode/show',
                'module'        => 'zipcode',
                'icon'          => '#',
                'permission_id' => $zipcode->id,
                'parent_id'     => $MasterLocations->id,
                'display_order' => '6',
            ],
            [
                'name'          => 'Import Excel Location',
                'url'           => '' . $prefix_admin . '/import-master',
                'module'        => 'import-master',
                'icon'          => '#',
                'permission_id' => $import_excel->id,
                'parent_id'     => $MasterLocations->id,
                'display_order' => '7',
            ],
            // Master Product
            [
                'name'          => 'Product Additional',
                'url'           => '' . $prefix_admin . '/product-additional/show',
                'module'        => 'product-additional',
                'icon'          => '#',
                'permission_id' => $product_additional->id,
                'parent_id'     => $MasterProduct->id,
                'display_order' => '1',
            ],
            [
                'name'          => 'Product Attribute',
                'url'           => '' . $prefix_admin . '/product-attribute/show',
                'module'        => 'product-attribute',
                'icon'          => '#',
                'permission_id' => $product_attribute->id,
                'parent_id'     => $MasterProduct->id,
                'display_order' => '2',
            ],
            [
                'name'          => 'Product Brand',
                'url'           => '' . $prefix_admin . '/product-brand/show',
                'module'        => 'product-brand',
                'icon'          => '#',
                'permission_id' => $product_brand->id,
                'parent_id'     => $MasterProduct->id,
                'display_order' => '3',
            ],
            [
                'name'          => 'Product Category',
                'url'           => '' . $prefix_admin . '/product-category/show',
                'module'        => 'product-category',
                'icon'          => '#',
                'permission_id' => $product_category->id,
                'parent_id'     => $MasterProduct->id,
                'display_order' => '4',
            ],
            [
                'name'          => 'Product Model',
                'url'           => '' . $prefix_admin . '/product-model/show',
                'module'        => 'product-model',
                'icon'          => '#',
                'permission_id' => $product_model->id,
                'parent_id'     => $MasterProduct->id,
                'display_order' => '5',
            ],
            [
                'name'          => 'Product Part Category',
                'url'           => '' . $prefix_admin . '/product-part-category/show',
                'module'        => 'product-part-category',
                'icon'          => '#',
                'permission_id' => $product_part_category->id,
                'parent_id'     => $MasterProduct->id,
                'display_order' => '6',
            ],
            [
                'name'          => 'Product',
                'url'           => '' . $prefix_admin . '/product/show',
                'module'        => 'product',
                'icon'          => '#',
                'permission_id' => $product->id,
                'parent_id'     => $MasterProduct->id,
                'display_order' => '7',
            ],
            [
                'name'          => 'Vendor',
                'url'           => '' . $prefix_admin . '/vendor/show',
                'module'        => 'vendor',
                'icon'          => '#',
                'permission_id' => $vendor->id,
                'parent_id'     => $inventory->id,
                'display_order' => '3',
            ],
            [
                'name'          => 'Product Unit Type',
                'url'           => '' . $prefix_admin . '/unit-type/show',
                'module'        => 'unit-type',
                'icon'          => '#',
                'permission_id' => $unit_type->id,
                'parent_id'     => $MasterProduct->id,
                'display_order' => '9',
            ],
            [
                'name'          => 'Product Price Item',
                'url'           => '' . $prefix_admin . '/price-item/show',
                'module'        => 'price-item',
                'icon'          => '#',
                'permission_id' => $price_item->id,
                'parent_id'     => $MasterProduct->id,
                'display_order' => '10',
            ],

            // Seting Management
            [
                'name'          => 'Menu Management',
                'url'           => '' . $prefix_admin . '/menu/show',
                'module'        => 'menu',
                'icon'          => '#',
                'permission_id' => $menu->id,
                'parent_id'     => $seting->id,
                'display_order' => '1',
            ],

            // user provile
            [
                'name'          => 'Education',
                'url'           => '' . $prefix_admin . '/educations/show',
                'module'        => 'educations',
                'icon'          => '#',
                'permission_id' => $education->id,
                'parent_id'     => $curriculum->id,
                'display_order' => '1',
            ],
            [
                'name'          => 'Curriculum',
                'url'           => '' . $prefix_admin . '/curriculum/show',
                'module'        => 'curriculum',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $curriculum->id,
                'display_order' => '2',
            ],
            [
                'name'          => 'Development Program',
                'url'           => '' . $prefix_admin . '/development/show',
                'module'        => 'development',
                'icon'          => '#',
                'permission_id' => $development_program->id,
                'parent_id'     => $curriculum->id,
                'display_order' => '3',
            ],
            [
                'name'          => 'Curriculum squence',
                'url'           => '' . $prefix_admin . '/sequence/show',
                'module'        => 'sequence',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $curriculum->id,
                'display_order' => '4',
            ],
            [
                'name'          => 'Development Plan',
                'url'           => '' . $prefix_admin . '/devPlant/show',
                'module'        => 'devPlant',
                'icon'          => '#',
                'permission_id' => $development->id,
                'parent_id'     => $curriculum->id,
                'display_order' => '5',
            ],
            [
                'name'          => 'Maritals',
                'url'           => '' . $prefix_admin . '/marital/show',
                'module'        => 'marital',
                'icon'          => '#',
                'permission_id' => $marital->id,
                'parent_id'     => $userProfile->id,
                'display_order' => '3',
            ],
            [
                'name'          => 'Religion',
                'url'           => '' . $prefix_admin . '/religion/show',
                'module'        => 'religion',
                'icon'          => '#',
                'permission_id' => $religion->id,
                'parent_id'     => $userProfile->id,
                'display_order' => '4',
            ],
            [
                'name'          => 'Users',
                'url'           => '' . $prefix_admin . '/user/show',
                'module'        => 'user',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $userProfile->id,
                'display_order' => '2',
            ],
            [
                'name'          => 'Status',
                'url'           => '' . $prefix_admin . '/user_status/show',
                'module'        => 'user',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $userProfile->id,
                'display_order' => '2',
            ],

            [
                'name'          => 'Inventory',
                'url'           => '' . $prefix_admin . '/inventory/show',
                'module'        => 'inventory',
                'icon'          => 'fa fa-database',
                'permission_id' => $setPermission->id,
                'parent_id'     => $inventory->id,
                'display_order' => '1',
            ],

            [
                'name'          => 'Warehouse',
                'url'           => '' . $prefix_admin . '/warehouse/show',
                'module'        => 'warehouse',
                'icon'          => 'fa fa-bank',
                'permission_id' => $warehouse->id,
                'parent_id'     => $inventory->id,
                'display_order' => '2',
            ],

            [
                'name'          => 'Import Batch Inventory',
                'url'           => '' . $prefix_admin . '/import-batch-inventory',
                'module'        => 'import-batch-inventory',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $inventory->id,
                'display_order' => '5',
            ],

            [
                'name'          => 'Shipping',
                'url'           => '' . $prefix_admin . '/batch/show',
                'module'        => 'batch',
                'icon'          => 'fa fa-truck',
                'permission_id' => $batch->id,
                'parent_id'     => $inventory->id,
                'display_order' => '6',
            ],

            [
                'name'          => 'Shipping Status',
                'url'           => '' . $prefix_admin . '/batch-status/show',
                'module'        => 'batch-status',
                'icon'          => 'fa fa-truck',
                'permission_id' => $setPermission->id,
                'parent_id'     => $inventory->id,
                'display_order' => '7',
            ],

            [
                'name'          => 'Shipment Status',
                'url'           => '' . $prefix_admin . '/batch-status-shipment/show',
                'module'        => 'batch-status-shipment',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $inventory->id,
                'display_order' => '8',
            ],

            [
                'name'          => 'Master Services',
                'url'           => '' . $prefix_admin . '/services/show',
                'module'        => 'services',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $MasterData->id,
                'display_order' => '1',
            ],

            [
                'name'          => 'Inventory Mutation Logs',
                'url'           => '' . $prefix_admin . '/inventory-mutation-logs',
                'module'        => 'inventory-mutation-logs',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $inventory->id,
                'display_order' => '1',
            ],

            [
                'name'          => 'Master Symptom',
                'url'           => '' . $prefix_admin . '/symptom/show',
                'module'        => 'symptom',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $MasterData->id,
                'display_order' => '1',
            ],

            [
                'name'          => 'Master Service Type',
                'url'           => '' . $prefix_admin . '/service_type/show',
                'module'        => 'service_type',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $MasterData->id,
                'display_order' => '1',
            ],

            [
                'name'          => 'Master Service Status',
                'url'           => '' . $prefix_admin . '/service_status/show',
                'module'        => 'service_status',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $MasterData->id,
                'display_order' => '4',
            ],

            [
                'name'          => 'Master Order Status',
                'url'           => '' . $prefix_admin . '/order_status/show',
                'module'        => 'order_status',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $MasterData->id,
                'display_order' => '5',
            ],

            [
                'name'          => 'Master Technician Sparepart',
                'url'           => 'teknisi/technician_sparepart/show',
                'module'        => 'technician_sparepart',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $MasterData->id,
                'display_order' => '5',
            ],

            [
                'name'          => 'My Member Team',
                'url'           => '/teknisi/team/list',
                'module'        => 'team',
                'icon'          => '#',
                'permission_id' => $setPermission->id,
                'parent_id'     => $MasterData->id,
                'display_order' => '5',
            ],

        ];
        Menu::insert($menus);
    }

    public function getMenus($prefix_admin)
    {
        // gallery images
        Menu::create([
            'name'          => 'Galery Images',
            'url'           => '' . $prefix_admin . '/gallery-image/show-main',
            'module'        => 'gallery-image',
            'icon'          => 'fa fa-folder-open-o',
            'permission_id' => Permission::where('name', 'gallery_image.show')->first()->id,
            'display_order' => '1',
        ]);

        $this->teknisiMenu($prefix_admin);
        $this->transaksiMenu($prefix_admin);

        Menu::create([
            'name'          => 'Tickets',
            'url'           => 'admin/tickets',
            'module'        => 'tickets',
            'icon'          => 'fa fa-envelope-open-o',
            'permission_id' => Permission::where('name', 'ticket.show')->first()->id,
            'display_order' => '4',
        ]);
        $this->masterMenu($prefix_admin);
        $this->inventoryMenu($prefix_admin);
        $this->userMenu($prefix_admin);
    }

    public function teknisiMenu($prefix_admin)
    {
        $teknisi = Menu::create([
            'name'          => 'Technician',
            'url'           => '#',
            'module'        => 'technician',
            'icon'          => 'fa fa-graduation-cap',
            'permission_id' => Permission::where('name', 'technician.show')->first()->id,
            'display_order' => '2',
        ]);

        $teknisi_menu = [
            [
                'name'          => 'Technician List',
                'module'          => 'technician',
                'url'           => '/admin/technician',
                'permission_id' => Permission::where('name', 'technician.show')->first()->id,
            ],
        ];

        foreach ($teknisi_menu as $key => $row) {
            Menu::create([
                'name'          => $row['name'],
                'url'           => $row['url'],
                'module'        => $row['module'],
                'icon'          => 'fa fa-angle-right',
                'permission_id' => $row['permission_id'],
                'display_order' => '2',
                'parent_id' => $teknisi->id,
            ]);
        }
    }

    public function transaksiMenu($prefix_admin)
    {
        $id = Permission::where('name', 'dashboard')->first()->id;
        $ts = Menu::create([
            'name'          => 'List Transactions',
            'url'           => '#',
            'module'        => 'transaction',
            'icon'          => 'fa fa-exchange',
            'permission_id' => $id,
            'display_order' => '3',
        ]);

        $teknisi_menu = [
            [
                'name'          => 'History Transaction',
                'module'          => 'transactions',
                'url'           => '/admin/transactions/show',
            ],
            [
                'name'          => 'Garansi Transaction',
                'module'          => 'transactions',
                'url'           => '/admin/transactions/show/garansi',
            ],
            [
                'name'          => 'Delete Transaction',
                'module'          => 'transactions-delete',
                'url'           => '/admin/transactions-delete/show/Trash',
            ],
            [
                'name'          => 'List Complaint',
                'module'          => 'transaction-complaint',
                'url'           => '/admin/transaction-complaint/show',
            ],
            [
                'name'          => 'Pending Transfer',
                'module'          => 'transaction-success',
                'url'           => '/admin/transaction-success/show',
            ],
        ];

        foreach ($teknisi_menu as $key => $row) {
            Menu::create([
                'name'          => $row['name'],
                'url'           => $row['url'],
                'module'        => $row['module'],
                'icon'          => 'fa fa-angle-right',
                'permission_id' => $id,
                'display_order' => '3',
                'parent_id' => $ts->id,
            ]);
        }
    }

    public function masterMenu($prefix_admin)
    {
        $id = Permission::where('name', 'dashboard')->first()->id;
        $ms = Menu::create([
            'name'          => 'Master Data',
            'url'           => '#',
            'icon'          => 'fa fa-meetup',
            'permission_id' => $id,
            'display_order' => '5',
        ]);

        $msProduk = Menu::create([
            'name'          => 'Master Product',
            'url'           => '#',
            'icon'          => 'fa fa-angle-right',
            'permission_id' => $id,
            'display_order' => '5',
            'parent_id' => $ms->id,
        ]);

        $msLocation = Menu::create([
            'name'          => 'Master Location',
            'url'           => '#',
            'icon'          => 'fa fa-angle-right',
            'permission_id' => $id,
            'display_order' => '5',
            'parent_id' => $ms->id,
        ]);

        $menuProduk = [
            [
                'name'          => 'Product Additional',
                'module'          => 'product-additional',
                'url'           => '/admin/product-additional',
            ],
            [
                'name'          => 'Product Attribute',
                'module'          => 'product-attribute',
                'url'           => '/admin/product-attribute',
            ],
            [
                'name'          => 'Product Brand',
                'module'          => 'product-brand',
                'url'           => '/admin/product-brand',
            ],
            [
                'name'          => 'Product Category',
                'module'          => 'product-category',
                'url'           => '/admin/product-category',
            ],
            [
                'name'          => 'Product Model',
                'module'          => 'product-model',
                'url'           => '/admin/product-model',
            ],
            [
                'name'          => 'Product Part Category',
                'module'          => 'product-part-category',
                'url'           => '/admin/product-part-category',
            ],
            [
                'name'          => 'Product',
                'module'          => 'product',
                'url'           => '/admin/product',
            ],
            [
                'name'          => 'Product Unit Type',
                'module'          => 'unit-type',
                'url'           => '/admin/unit-type',
            ],
            [
                'name'          => 'Product Price Item',
                'module'          => 'price-item',
                'url'           => '/admin/price-item',
            ],
        ];

        foreach ($menuProduk as $key => $row) {
            Menu::create([
                'name'          => $row['name'],
                'url'           => $row['url'],
                'module'        => $row['module'],
                'icon'          => 'fa fa-angle-right',
                'permission_id' => $id,
                'display_order' => '5',
                'parent_id' => $msProduk->id,
            ]);
        }

        $menuLokasi = [
            [
                'name'          => 'Country',
                'module'          => 'country',
                'url'           => '/admin/country',
            ],
            [
                'name'          => 'Province',
                'module'          => 'province',
                'url'           => '/admin/province',
            ],
            [
                'name'          => 'City',
                'module'          => 'city',
                'url'           => '/admin/city',
            ],
            [
                'name'          => 'District',
                'module'          => 'district',
                'url'           => '/admin/district',
            ],
            [
                'name'          => 'Village',
                'module'          => 'village',
                'url'           => '/admin/village',
            ],
            [
                'name'          => 'Zipcode',
                'module'          => 'zipcode',
                'url'           => '/admin/zipcode',
            ],
            [
                'name'          => 'Import Excel Location',
                'module'          => 'import-master',
                'url'           => '/admin/import-master',
            ],
        ];

        foreach ($menuLokasi as $key => $row) {
            Menu::create([
                'name'          => $row['name'],
                'url'           => $row['url'],
                'module'        => $row['module'],
                'icon'          => 'fa fa-angle-right',
                'permission_id' => $id,
                'display_order' => '5',
                'parent_id' => $msLocation->id,
            ]);
        }

        $teknisi_menu = [
            [
                'name'          => 'Service Status',
                'module'          => 'service_status',
                'url'           => '/admin/service-status/show',
            ],
            [
                'name'          => 'Order Status',
                'module'          => 'order_status',
                'url'           => '/admin/order-status/show',
            ],
            [
                'name'          => 'Bank Transfer',
                'module'          => 'bank',
                'url'           => '/admin/bank/show',
            ],
            [
                'name'          => 'Services',
                'module'          => 'services',
                'url'           => '/admin/services/show',
            ],
            [
                'name'          => 'Symptom',
                'module'          => 'symptom',
                'url'           => '/admin/symptom/show',
            ],
            [
                'name'          => 'Service Type',
                'module'          => 'service-type',
                'url'           => '/admin/service-type/show',
            ],
            [
                'name'          => 'Symptom',
                'module'          => 'symptom',
                'url'           => '/admin/symptom/show',
            ],
            [
                'name'          => 'Bank',
                'module'          => 'bank_transfer',
                'url'           => '/admin/bank/show',
            ],
            [
                'name'          => 'Batch Status',
                'module'          => 'batch-status',
                'url'           => '/admin/batch-status/show',
            ],
            [
                'name'          => 'Shipment Status',
                'module'          => 'batch-status-shipment',
                'url'           => '/admin/batch-status-shipment/show',
            ],
            [
                'name'          => 'Warehouse',
                'module'          => 'warehouse',
                'url'           => '/admin/warehouse',
            ],
            [
                'name'          => 'Vendor',
                'module'          => 'vendor',
                'url'           => '/admin/vendor',
            ],
        ];

        foreach ($teknisi_menu as $key => $row) {
            Menu::create([
                'name'          => $row['name'],
                'url'           => $row['url'],
                'module'        => $row['module'],
                'icon'          => 'fa fa-angle-right',
                'permission_id' => $id,
                'display_order' => '5',
                'parent_id' => $ms->id,
            ]);
        }
    }

    public function inventoryMenu($prefix_admin)
    {
        $id = Permission::where('name', 'dashboard')->first()->id;
        $inventory = Menu::create([
            'name'          => 'Inventory',
            'url'           => '#',
            'icon'          => 'fa fa-database',
            'permission_id' => $id,
            'display_order' => '6',
        ]);

        $ship = Menu::create([
            'name'          => 'Shipping',
            'url'           => '#',
            'icon'          => 'fa fa-angle-right',
            'permission_id' => $id,
            'display_order' => '6',
            'parent_id' => $inventory->id,
        ]);

        $inventoryMenu = [
            [
                'name'          => 'Inventory Mutation Logs',
                'module'          => 'inventory-mutation-logs',
                'url'           => '/admin/inventory-mutation-logs',
            ],
            [
                'name'          => 'Import Batch Inventory',
                'module'          => 'import-batch-inventory',
                'url'           => '/admin/import-batch-inventory',
            ],
            [
                'name'          => 'Inventory',
                'module'          => 'inventory',
                'url'           => '/admin/inventory/show',
            ]
        ];

        foreach ($inventoryMenu as $key => $row) {
            Menu::create([
                'name'          => $row['name'],
                'url'           => $row['url'],
                'module'        => $row['module'],
                'icon'          => 'fa fa-angle-right',
                'permission_id' => $id,
                'display_order' => '6',
                'parent_id' => $inventory->id,
            ]);
        }

        $shipMenu = [
            [
                'name'          => 'Shipping List',
                'module'          => 'batch',
                'url'           => '/admin/batch/show',
            ],
            [
                'name'          => 'Create Shipping',
                'module'          => 'batch',
                'url'           => '/admin/batch-create',
            ],
        ];

        foreach ($shipMenu as $key => $row) {
            Menu::create([
                'name'          => $row['name'],
                'url'           => $row['url'],
                'module'        => $row['module'],
                'icon'          => 'fa fa-angle-right',
                'permission_id' => $id,
                'display_order' => '6',
                'parent_id' => $ship->id,
            ]);
        }
    }

    public function userMenu($prefix_admin)
    {
        $id = Permission::where('name', 'dashboard')->first()->id;
        $user = Menu::create([
            'name'          => 'User Management',
            'url'           => '#',
            'icon'          => 'fa fa-user',
            'permission_id' => $id,
            'display_order' => '7',
        ]);

        $curriculum = Menu::create([
            'name'          => 'Curriculum',
            'url'           => '#',
            'icon'          => 'fa fa-angle-right',
            'permission_id' => $id,
            'display_order' => '7',
            'parent_id' => $user->id,
        ]);

        $menuProduk = [
            [
                'name'          => 'Education',
                'module'          => 'educations',
                'url'           => '/admin/educations/show',
            ],
            [
                'name'          => 'Curriculum',
                'module'          => 'curriculum',
                'url'           => '/admin/curriculum/show',
            ],
            [
                'name'          => 'Development Program',
                'module'          => 'development',
                'url'           => '/admin/development/show',
            ],
            [
                'name'          => 'Curriculum squence',
                'module'          => 'sequence',
                'url'           => '/admin/product-category/show',
            ],
            [
                'name'          => 'Development Plan',
                'module'          => 'devPlant',
                'url'           => '/admin/devPlant/show',
            ]
        ];

        foreach ($menuProduk as $key => $row) {
            Menu::create([
                'name'          => $row['name'],
                'url'           => $row['url'],
                'module'        => $row['module'],
                'icon'          => 'fa fa-angle-right',
                'permission_id' => $id,
                'display_order' => '7',
                'parent_id' => $curriculum->id,
            ]);
        }

        $menuProduk = [
            [
                'name'          => 'Users',
                'module'          => 'user',
                'url'           => '/admin/user/show',
            ],
            [
                'name'          => 'Maritals',
                'module'          => 'marital',
                'url'           => '/admin/marital/show',
            ],
            [
                'name'          => 'Religion',
                'module'          => 'religion',
                'url'           => '/admin/religion/show',
            ],
        ];

        foreach ($menuProduk as $key => $row) {
            Menu::create([
                'name'          => $row['name'],
                'url'           => $row['url'],
                'module'        => $row['module'],
                'icon'          => 'fa fa-angle-right',
                'permission_id' => $id,
                'display_order' => '7',
                'parent_id' => $user->id,
            ]);
        }

        $seting = Menu::create([
            'name'          => 'Setting',
            'url'           => '#',
            'icon'          => 'fa fa-cog',
            'permission_id' => $id,
            'display_order' => '9',
        ]);

        $menuset = [
            [
                'name'          => 'Menu Management',
                'module'          => 'menu',
                'url'           => '/admin/menu/show',
            ],
            [
                'name'          => 'General Settings',
                'module'          => 'general-setting',
                'url'           => '/admin/general-setting/show',
            ],
        ];

        foreach ($menuset as $key => $row) {
            Menu::create([
                'name'          => $row['name'],
                'url'           => $row['url'],
                'module'        => $row['module'],
                'icon'          => 'fa fa-angle-right',
                'permission_id' => $id,
                'display_order' => '9',
                'parent_id' => $seting->id,
            ]);
        }
    }
}
