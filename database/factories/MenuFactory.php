<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Menu;
use Faker\Generator as Faker;
use Spatie\Permission\Models\Permission;

$factory->define(Menu::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'url' => $faker->url,
        'icon' => $faker->name,
        'permission_id' =>  Permission::first()->id,
        'display_order' => 1,
        'parent_id' => null
    ];
});
