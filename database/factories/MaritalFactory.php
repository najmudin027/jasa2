<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Master\Marital;
use Faker\Generator as Faker;

$factory->define(Marital::class, function (Faker $faker) {
    return [
        'status' => $faker->name
    ];
});
