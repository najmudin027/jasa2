<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Product\ProductBrand;
use App\Model\Product\ProductPartCategory;
use Faker\Generator as Faker;

$factory->define(ProductBrand::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'code' => $faker->name,
        'product_part_category_id' => factory(ProductPartCategory::class)->create()->id
    ];
});
