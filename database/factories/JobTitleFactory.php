<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Technician\JobTitle;
use App\Model\Technician\JobTitleCategory;
use App\User;
use Faker\Generator as Faker;

$factory->define(JobTitle::class, function (Faker $faker) {
    return [
        'description' => $faker->name,
        'user_id' => factory(User::class)->create()->id,
        'job_title_category_id' => factory(JobTitleCategory::class)->create()->id
    ];
});
