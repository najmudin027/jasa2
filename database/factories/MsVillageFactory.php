<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Master\MsDistrict;
use App\Model\Master\MsVillage;
use Faker\Generator as Faker;

$factory->define(MsVillage::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'ms_district_id' => factory(MsDistrict::class)->create()->id
    ];
});
