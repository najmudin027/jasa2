<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Master\MsAddress;
use App\Model\Master\MsCity;
use App\Model\Master\MsDistrict;
use App\Model\Master\MsVillage;
use App\Model\Master\MsZipcode;
use App\User;
use Faker\Generator as Faker;

$factory->define(MsAddress::class, function (Faker $faker) {
    return [
        'address' => $this->faker->address,
        'phone1' => $this->faker->phoneNumber,
        'phone2' => $this->faker->phoneNumber,
        'user_id' => factory(User::class)->create()->id,
        'ms_city_id' => factory(MsCity::class)->create()->id,
        'ms_district_id' => factory(MsDistrict::class)->create()->id,
        'ms_village_id' => factory(MsVillage::class)->create()->id,
        'ms_zipcode_id' => factory(MsZipcode::class)->create()->id,
    ];
});
