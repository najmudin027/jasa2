<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Technician\JobTitleCategory;
use Faker\Generator as Faker;

$factory->define(JobTitleCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
