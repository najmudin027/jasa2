<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Technician\JobExperience;
use App\Model\Technician\JobTitle;
use App\User;
use Faker\Generator as Faker;

$factory->define(JobExperience::class, function (Faker $faker) {
    return [
        'position' => $faker->name,
        'company_name' => $faker->name,
        'period_start' => date('Y-m-d'),
        'period_end' => date('Y-m-d'),
        'type' => 1,
        'user_id' => factory(User::class)->create()->id,
        'job_title_id' => factory(JobTitle::class)->create()->id,
    ];
});
