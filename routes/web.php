<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\User;
use App\Model\Master\GeneralSetting;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Response;
use App\Model\Master\WebSetting;

Auth::routes();

Route::get('storage/media-btb/{filename}', function ($filename) {
    // dd('asd');
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/btb/media/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/media-request-order/{filename}', function ($filename) {
    // dd('asd');
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/request_order_image/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/order-attachment/{filename}', function ($filename) {
    // dd('asd');
    $userLogin = Auth::user()->id;
    $path = storage_path('app/public/request_order_image/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/image/profile-teknisi/{filename}', function ($filename) {
    $userLogin = Auth::user()->id;
    $path      = storage_path('app/public/user/profile/avatar/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file     = File::get($path);
    $type     = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::post('payments/notification', 'PaymentController@notification');
Route::get('payments/completed', 'PaymentController@completed');
Route::get('payments/failed', 'PaymentController@failed');
Route::get('payments/unfinish', 'PaymentController@unFinish');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('auth/{provider}', 'AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'AuthController@handleProviderCallback');
Route::get('/error-request', 'AuthController@errorMessage');

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    $date = new DateTime();
    $timeZone = $date->getTimezone();

    return date_default_timezone_get() . ' ' . date('d F Y H:i:s');
});

// ==========================
// storage link
// =============================================
require base_path('routes/web/storage_link.php');

Route::get('/privacy-policy', function () {
    $title_header_global = WebSetting::value('title_setting');
    $metta_header_global = WebSetting::value('metta_setting');
    $get_logo_astech = WebSetting::value('logo');
    $getPoliceTitle = GeneralSetting::where('name', 'privacy_police_title')->value('value');
    $getPoliceDesc = GeneralSetting::where('name', 'privacy_police_desc')->value('value');
    return view('admin.template.privacy-police', compact('title_header_global', 'metta_header_global', 'get_logo_astech', 'getPoliceTitle', 'getPoliceDesc'));
})->middleware('guest');

Route::get('/term-and-condition', function () {
    $title_header_global = WebSetting::value('title_setting');
    $metta_header_global = WebSetting::value('metta_setting');
    $get_logo_astech = WebSetting::value('logo');
    $getTermTitle = GeneralSetting::where('name', 'agreement_title')->value('value');
    $getTermDesc = GeneralSetting::where('name', 'agreement_text')->value('value');
    return view('admin.template.term-and-condition', compact('title_header_global', 'metta_header_global', 'get_logo_astech', 'getTermTitle', 'getTermDesc'));
})->middleware('guest');

Route::get('/astech-admin', function () {
    return view('admin.template.login');
})->middleware('guest')->name('show.login');

Route::get('/', function () {
    $title_header_global = WebSetting::value('title_setting');
    $metta_header_global = WebSetting::value('metta_setting');
    $social_media_login_fb = GeneralSetting::where('name', 'social_media_login_fb')->first();
    $social_media_login_google = GeneralSetting::where('name', 'social_media_login_google')->first();
    $get_logo_astech = WebSetting::value('logo');
    return view('users.template.login', compact('title_header_global', 'metta_header_global', 'get_logo_astech', 'social_media_login_fb', 'social_media_login_google'));
})->middleware('guest');

Route::get('/user/register', function () {
    $showRole = Role::get();
    $getAgreement = GeneralSetting::where('name', 'agreement_text')->first();
    $getModalTitle = GeneralSetting::where('name', 'agreement_title')->first();
    $google_maps_key_setting = GeneralSetting::where('name', 'google_maps_key_setting')->value('value');
    $title_header_global = WebSetting::value('title_setting');
    $metta_header_global = WebSetting::value('metta_setting');
    $get_logo_astech = WebSetting::value('logo');
    return view('users.template.register', compact(
        'showRole',
        'getAgreement',
        'getModalTitle',
        'google_maps_key_setting',
        'title_header_global',
        'metta_header_global',
        'get_logo_astech'
    ));
})->middleware('guest');

Route::get('get/logo/from-storage/{filename}', function ($filename) {
    $path = storage_path('app/public/web_setting_image/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file     = File::get($path);
    $type     = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

// Auth::routes();
//register New
Route::post('/registers', 'AuthController@newRegisteredUsers')->name('auth.new.register');

Route::post('/login', 'AuthController@login')->name('auth.login');
Route::post('/loginUsers', 'AuthController@loginUser')->name('auth.login');
Route::post('/register', 'AuthController@register')->name('auth.register');
Route::get('logout', 'HomeController@logout');

Route::get('/verify', 'HomeController@verify');
Route::get('/login/form', 'HomeController@login');



Route::get('/email/verify/{id}', 'VerificationController@verify')->name('email.verification');
Route::post('/otp/verify', 'VerificationController@otpVerify')->name('otp.verification');
Route::post('/otp/resend', 'VerificationController@otpResend')->name('otp.resend.verification');
Route::post('/email/resend', 'VerificationController@resend')->name('email.resend.verification');

Route::post('/otp/verify_ajax', 'VerificationController@otpVerifyAjax')->name('otp.verification.ajax');

// Route::post('/otp/send', 'VerificationController@otpSend')->name('otp.send');


// Route::get('/user/login', 'HomeController@backLogin')->name('home.login');

Route::get('user/category-services', 'Admin\MasterServicesController@showServiceForUser')->name('services.user');

Route::get('/admin/noty/list', 'NotifikasiController@readAll');
Route::get('/customer/noty/list', 'NotifikasiController@readAll');
Route::get('/teknisi/noty/list', 'NotifikasiController@readAll');
Route::group(['prefix' => 'noty', 'middleware' => ['auth']], function () {
    Route::get('/read/{id}', 'NotifikasiController@read')->name('noty.read');
    Route::get('/delete', 'NotifikasiController@deleteAll')->name('noty.delete');
});


Route::group(['prefix' => 'customer', 'middleware' => ['auth', 'role:Customer', 'user.active']], function () {
    Route::get('/preview', 'Admin\ProfileController@preview');

    Route::get('/filing-technician/{id}', 'HomeController@detail')->name('home.detail');
    Route::get('/filing-technician/update/{id}', 'HomeController@update')->name('home.detail');
    Route::get('/checkout/{order_id}', 'Admin\CheckOutController@index');
    Route::get('/invoice/{order_id}', 'Admin\CheckOutController@invoice');
    Route::get('/order_detail/{order_id}', 'Admin\CheckOutController@showOrderDetail');
    Route::get('/payment/confirmation/{order_id}', 'Admin\CheckOutController@paymentConfirmation');

    Route::get('/user/info', 'Admin\ProfileController@teknisiInfo')->name('user.info');
    Route::get('/information/update/{id}', 'Admin\ProfileController@infoUpdate')->name('user.info.update');

    //user address
    Route::get('/address/show', 'Admin\ProfileController@listCreate')->name('user.address');
    Route::get('/address/create', 'Admin\ProfileController@addressCreate')->name('users.address.create');
    Route::get('/address/update/{id}', 'Admin\ProfileController@updateAddress')->name('user.address.update');
    Route::get('/detail-transaction-customer/{order_id}', 'CustomerController@customer_service_detail')->name('customer.service.detail');
    Route::get('/dashboard', 'CustomerController@dashboard')->name('user.home');
    Route::get('/category-services', 'Admin\MasterServicesController@showServiceForUser')->middleware('auth')->name('services.user');
    Route::get('/request_list', 'CustomerController@request_list');
    // Route::get('/request_job/{slug_service}', 'CustomerController@job_request_create');
    Route::get('/create-request-job', 'CustomerController@job_request_create');
    Route::get('/new-create-request-job', 'CustomerController@new_job_request_create');
    Route::get('/service_detail/{order_id}', 'CustomerController@service_detail');

    Route::get('/detail-history-transactions/{order_id}', 'CustomerController@showDetailHistory');

    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'Customer\Web\ProfileController@index')->name('profile.show.index');
        Route::get('/info/edit', 'Customer\Web\ProfileController@editInfo');
        Route::get('/address/create', 'Customer\Web\ProfileController@createAddress');
        Route::get('/address/{id}/edit', 'Customer\Web\ProfileController@editAddress');
    });

    Route::group(['prefix' => 'tickets'], function () {
        Route::get('/', 'Customer\Web\TicketController@index');
        Route::get('/{id}/detail', 'Customer\Web\TicketController@show');
    });

    Route::group(['prefix' => 'chats'], function () {
        Route::get('/firebase', 'Customer\Web\ChatController@firebaseIndex');

        Route::get('/', 'Customer\Web\ChatController@index')->name('customer.chat');
        Route::get('/{id}/detail', 'Customer\Web\ChatController@show');
    });

    Route::group(['prefix' => 'garansi'], function () {
        Route::get('/', 'Customer\Web\GaransiController@index');
        Route::get('/{id}/detail', 'Customer\Web\GaransiController@show');
    });

    Route::group(['prefix' => 'reviews'], function () {
        Route::get('/waiting', 'Customer\Web\ReviewController@waiting')->name('customer.reviews.waiting');
        Route::get('/done', 'Customer\Web\ReviewController@done')->name('customer.reviews.done');
    });

    Route::get('/replay_item', 'CustomerController@replayItem');
    Route::get('/technician/{id}', 'CustomerController@technician')->name('customer.detail_teknisi');

    Route::get('address', 'Admin\ProfileController@listCreate');

    //status
    Route::get('/request_list/status/3', 'Admin\requestListStatusController@approve');
    Route::get('/request_list/status/2', 'Admin\requestListStatusController@pending');
    Route::get('/request_list/status/4', 'Admin\requestListStatusController@payConfrim');
    Route::get('/request_list/status/8', 'Admin\requestListStatusController@payConfrimCustomer');
    Route::get('/request_list/status/9', 'Admin\requestListStatusController@payConfrimAdmin');
    Route::get('/request_list/status/7', 'Admin\requestListStatusController@jobsDone');
    Route::get('/request_list/status/5', 'Admin\requestListStatusController@jobsCancel');

    Route::get('/request-job/list-technician', 'CustomerController@getListTechnicians');

    //list transacsion complain
    Route::get('/transacsion_list/complaint', 'CustomerController@showListComplaint');
    Route::get('/transacsion_list/complaint/detail/{order_id}', 'CustomerController@detailComplaint');

    // topup wallet
    Route::get('/topup/show-list', 'CustomerController@showListTopup');
    Route::get('/topup/checkout/{id}', 'CustomerController@topupCheckout');
    Route::get('/topup/details/{id}', 'CustomerController@topupDetails');
    Route::get('/topup/checkout/confirmation/{id}', 'CustomerController@topupConfirmation');

    Route::get('/topup-history/show-history', 'CustomerController@showHistoryPayment');
});

Route::group(['prefix' => 'teknisi', 'middleware' => ['auth', 'role:Technician', 'user.active']], function () {
    Route::group(['prefix' => 'chats'], function () {
        Route::get('/', 'Customer\Web\ChatController@index')->name('teknisi.chat');
        Route::get('/{id}/detail', 'Customer\Web\ChatController@show');
    });

    Route::group(['prefix' => 'profile'], function () {
        Route::get('/info/edit', 'Customer\Web\ProfileController@editInfo');
    });

    // detail balance
    Route::get('/balance/detail', 'TeknisiController@balanceDetail');
    Route::get('/transacsion_list/complaint/detail/{order_id}', 'CustomerController@detailComplaint');

    //user address
    Route::get('/address/show', 'Admin\ProfileController@teknisiAddress')->name('user.address');
    Route::get('/address/create', 'Admin\ProfileController@addressCreate')->name('user.address.create');
    Route::get('/address/update/{id}', 'Admin\ProfileController@updateAddress')->name('user.address.update');

    Route::get('/dashboard', 'TeknisiController@dashboard')->name('teknisi.home');
    Route::get('/list-garansi-job', 'TeknisiController@garansiIndex');
    Route::get('/list-request-job', 'TeknisiController@listRequestJob')->name('teknisi.listRequestJob');
    Route::get('/list-request-job/awaiting', 'TeknisiController@listRequestJobAwaiting')->name('teknisi.listRequestJob.awaiting');
    Route::get('/list-request-job/onprogress', 'TeknisiController@listRequestJobOnProgress')->name('teknisi.listRequestJob.onprogress');
    Route::get('/list-request-job/done', 'TeknisiController@listRequestJobDone')->name('teknisi.listRequestJob.done');
    Route::get('/list-request-job/cancel', 'TeknisiController@listRequestCancel')->name('teknisi.listRequestJob.cancel');
    Route::get('/request-job-accept/{order_id}', 'TeknisiController@service_detail')->name('teknisi.service_detail');
    Route::get('/order/delivery_note/{order_id}', 'TeknisiController@workmanshipReportPdf');
    Route::get('/detail-transaction-admin/{order_id}', 'TeknisiController@admin_service_detail')->name('admin.service.detail');
    Route::get('/detail-transaction-customer/{order_id}', 'TeknisiController@customer_service_detail')->name('customer.service.detail');

    Route::get('/technician_sparepart/show', 'TechnicianSparepartController@index')->name('teknisi.technician_sparepart.index');
    Route::get('/technician_sparepart/create', 'TechnicianSparepartController@create')->name('teknisi.technician_sparepart.create');
    Route::get('/technician_sparepart/edit/{id}', 'TechnicianSparepartController@update')->name('teknisi.technician_sparepart.update');

    Route::get('/service_price', 'TeknisiController@service_price')->name('teknisi.service_price');

    Route::get('/profile', 'TeknisiController@profile')->name('teknisi.profile');

    Route::get('update/{id}', 'Admin\ProfileController@updateProfile')->name('profil.update');
    // Route::get('/profile/show', 'Admin\TechnicianController@index')->name('technician.index.web');

    // complaint
    Route::get('/complaint-jobs', 'TeknisiController@complaintList');
    Route::get('/complaint-jobs/detail/{order_id}', 'TeknisiController@detailComplaint');


    // Team Technicians
    Route::get('/team/list', 'TeknisiController@techniciansTeamList');

    // Teknisi My Schedules
    Route::get('/my-schedules', 'TeknisiController@mySchedules');

    // Teknisi My Schedules
    Route::get('/review-list', 'TeknisiController@reviewList');


    Route::get('storage/inventory/image/{filename}', function ($filename) {
        $userLogin = Auth::user()->id;
        $path      = storage_path('app/public/images/Id_' . $userLogin . '/' . $filename);
        if (!File::exists($path)) {
            abort(404);
        }
        $file     = File::get($path);
        $type     = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    });
});


Route::get('/register-form', 'HomeController@showregisterForm')->name('register.form');
Route::get('/forgot-password-form', 'HomeController@showForgotPasswordForm')->name('register.form');
Route::post('/forgot-password-send', 'HomeController@passwordReset')->name('register.form');
Route::post('/forgot-password-update', 'HomeController@updatePassword')->name('register.form');
Route::get('/create-password-form', 'HomeController@showCreatePasswordForm')->name('create.password.form');
Route::get('/verification-otp-form', 'HomeController@showOtpVerification')->name('verification.otp.form');


// Route::get('show-main', 'Admin\ImageUploadController@fileCreate')->middleware('permission:gallery_image.show');
Route::get('show-gallery', 'Admin\ImageUploadController@listImage'); //->middleware('permission:gallery_image.show');

Route::group(['middleware' => ['auth', 'user.active']], function () {
    //For Admin Route
    Route::group(['prefix' => 'admin'], function () {


        Route::get('firebase_push_admin', 'NotifikasiController@firebasePushView');
        Route::get('home', 'HomeController@showdashboar')->name('home');
        Route::get('home/user', 'HomeController@showDashboarUsers')->name('home.user');
        Route::get('teknisi/detail/{id}', 'HomeController@detail')->name('home.detail');

        Route::get('/chats', 'Customer\Web\ChatController@adminIndex')->middleware('permission:chat.show');
        Route::get('chats/{user_id}/technician', 'Customer\Web\ChatController@technician')->middleware('permission:chat.show');

        Route::get('garansi/{id}/detail', 'Customer\Web\GaransiController@show')->middleware('permission:garansi.show');

        Route::get('/invoice/{order_id}', 'Admin\CheckOutController@invoice');
        Route::get('/all-orders', 'TeknisiController@allOrders');

        Route::get('/transacsion_list/complaint/detail/{order_id}', 'CustomerController@detailComplaint')->middleware('permission:order.show');
        Route::get('/order/service_detail/{order_id}', 'Admin\MasterOrderController@service_detail')
            ->name('admin.service.detail')
            ->middleware('permission:order.show');

        Route::get('/order/workmanship_report_pdf/{order_id}', 'TeknisiController@workmanshipReportPdf')
            ->middleware('permission:order.show');

        // web Setting
        Route::get('/web-setting', 'Admin\WebSettingController@webSetting')->middleware('permission:setting.show');

        // Permission
        Route::get('/permission', 'Admin\PermissionController@showIndexPermission')->middleware('permission:permission.show');

        // Role
        Route::group(['prefix' => 'role'], function () {
            Route::get('show', 'Admin\PermissionController@showIndexRole')->middleware('permission:role.show')->name('index.role');
            Route::get('create', 'Admin\RoleController@create')->middleware('permission:role.create');
            Route::get('update/{id}', 'Admin\RoleController@edit')->middleware('permission:role.update');
        });

        Route::group(['prefix' => 'agreement'], function () {
            Route::get('create', 'Admin\AgreementController@create')->middleware('permission:setting.show');
        });

        Route::group(['prefix' => 'banner'], function () {
            Route::get('show', 'Admin\MasterBannerController@index')->middleware('permission:banner.show');
            Route::get('create', 'Admin\MasterBannerController@create')->middleware('permission:banner.create');
        });

        Route::group(['prefix' => 'agreement-order'], function () {
            Route::get('create', 'Admin\AgreementController@agreementOrder')->middleware('permission:setting.show');
        });

        Route::group(['prefix' => 'term-order-completed'], function () {
            Route::get('show', 'Admin\AgreementController@agreementOrderCompleted')->middleware('permission:setting.show');
        });

        Route::group(['prefix' => 'privacy-policy'], function () {
            Route::get('show', 'Admin\AgreementController@privacyPolicy')->middleware('permission:setting.show');
        });

        //RoleAdmin
        Route::post('/roles', 'Admin\RoleController@store')->name('admin.role.store');
        Route::put('/roles/{id}', 'Admin\RoleController@update')->name('admin.role.update');

        //Menu
        Route::group(['prefix' => 'menu'], function () {
            Route::get('show', 'Admin\SettingMenuController@showListMenu')->middleware('permission:menu.show');
            Route::get('create', 'Admin\SettingMenuController@createMenus')->middleware('permission:menu.create');
            Route::get('update/{id}', 'Admin\SettingMenuController@updateMenus')->middleware('permission:menu.update');
        });

        // User
        Route::group(['prefix' => 'user'], function () {
            Route::get('show', 'Admin\ManageUsersController@showManageUsers')->middleware('permission:user.show');
            Route::get('create', 'Admin\ManageUsersController@createManageUsers')->middleware('permission:user.create');
            Route::get('update/{id}', 'Admin\ManageUsersController@updateManageUsers')->middleware('permission:user.update');
            Route::get('detail/{id}', 'Admin\ManageUsersController@detail')->middleware('permission:user.show');
            Route::get('info/{id}', 'Admin\ManageUsersController@info')->middleware('permission:user.show');
            Route::get('address/{id}/create', 'Admin\ManageUsersController@addressCreate')->middleware('permission:user.show');
            Route::get('address/{id}/edit', 'Admin\ManageUsersController@addressEdit')->middleware('permission:user.show');
        });

        // user b2b
        Route::group(['prefix' => 'user-business-to-business'], function () {
            Route::get('show', 'Admin\ManageUsersB2BController@index')->middleware('permission:user.show');
            Route::get('create', 'Admin\ManageUsersB2BController@create')->middleware('permission:user.create');
            Route::get('update/{id}', 'Admin\ManageUsersB2BController@update')->middleware('permission:user.update');
        });

        // request order b2b
        Route::group(['prefix' => 'business-to-business-user-request-order'], function () {
            Route::get('show', 'Admin\B2BOrderRequestController@index');
            Route::get('create', 'Admin\B2BOrderRequestController@create');
            Route::get('detail/{request_code}', 'Admin\B2BOrderRequestController@detail');
            Route::get('detail-request-order/{id}', 'Admin\B2BOrderRequestController@detailOrderReq');
            Route::get('create-quotation/{id}', 'Admin\B2BOrderRequestController@createQuotation');
            Route::get('view-quotation/{id}', 'Admin\B2BOrderRequestController@viewQuotation');
            Route::get('preview-quotation/{id}', 'Admin\B2BOrderRequestController@previewQuotation');
            Route::get('assign-teknisi/{id}', 'Admin\B2BOrderRequestController@viewPageTeknisi');
            Route::get('update-schedule-teknisi/{id}', 'Admin\B2BOrderRequestController@viewPageUpdateScheduleTeknisi');
            Route::get('print-quotation/{id}', 'Admin\B2BOrderRequestController@print');
            Route::get('preview-invoice/{id}', 'Admin\B2BOrderRequestController@previewInvoice');
            Route::get('file-quotation/{id}', 'Admin\B2BOrderRequestController@file_quotation');
            // Route::get('api', 'Api\OrderUserB2bController@listTechnicianSchedule');
        });

        // request order b2b
        Route::group(['prefix' => 'business-to-business-general-settings'], function () {
            Route::get('show', 'Admin\B2bGeneralSettingController@index');
        });

        # Badge
        Route::group(['prefix' => 'badge'], function () {
            // list data
            Route::get('/show', 'Admin\MasterBadgeController@showBadge')
                ->name('admin.badge.index.web')
                ->middleware('permission:user.show');
        });

        //Mass Import Master
        Route::get('/import-master', 'Admin\ImportExcelController@showImportExcelMaster')->middleware('permission:import_excel.show');
        Route::get('/import-batch-inventory', 'Admin\ImportExcelController@showImportExcelBatchInventory')->middleware('permission:import_excel.show');

        // ==========================
        // master lokasi
        // =============================================
        require base_path('routes/web/admin_lokasi.php');

        // ==========================
        // master product here
        // =============================================
        require base_path('routes/web/admin_product.php');

        // ==========================
        // master here
        // =============================================
        require base_path('routes/web/admin_master.php');

        //Batch
        Route::group(['prefix' => 'batch'], function () {
            Route::get('show', 'Admin\MasterBatchController@index')->middleware('permission:batch.show');
            Route::get('create', 'Admin\MasterBatchController@create')->middleware('permission:batch.create');
            Route::get('update/{id}', 'Admin\MasterBatchController@edit')->middleware('permission:batch.update');
            Route::get('status', 'Admin\MasterBatchController@statusIndex')->middleware('permission:batch.show');
            Route::get('status-shipment', 'Admin\MasterBatchController@statusShipmentIndex')->middleware('permission:batch.show');
            Route::get('shipment/{id}', 'Admin\MasterBatchController@batchShipment')->middleware('permission:batch.show');
            Route::get('{id}/show', 'Admin\MasterBatchController@show')->middleware('permission:batch.show');
        });
        //Batch Status
        Route::group(['prefix' => 'batch-status'], function () {
            Route::get('show', 'Admin\MasterBatchController@statusIndex')->middleware('permission:batch.show');
        });
        //Batch Status Shipment
        Route::group(['prefix' => 'batch-status-shipment'], function () {
            Route::get('show', 'Admin\MasterBatchController@statusShipmentIndex')->middleware('permission:batch.show');
        });
        //Inventory
        Route::group(['prefix' => 'inventory'], function () {
            Route::get('show', 'Admin\MasterInventoryController@showListInventory')->middleware('permission:inventory.show');
            Route::get('create', 'Admin\MasterInventoryController@createInventory')->middleware('permission:inventory.create');
            Route::get('update/{id}', 'Admin\MasterInventoryController@updateInventory')->middleware('permission:inventory.update');
            Route::get('mutation/{id}', 'Admin\MasterInventoryController@mutationInventory')->middleware('permission:inventory.update');
        });
        Route::get('/inventory-mutation-logs', 'Admin\MasterInventoryController@showMutationLogs')->middleware('permission:inventory.show');
        Route::get('/inventory-mutation-histories/{inventory_id?}', 'Admin\MasterInventoryController@showMutationHistories')->middleware('permission:inventory.show');


        //Mass Import Batch Inventory
        Route::get('/import-batch-inventory', 'Admin\ImportExcelController@showImportExcelBatchInventory')->middleware('permission:inventory.show');

        //Import Orders Data Excel
        Route::get('/import-orders-data-excel', 'Admin\ImportExcelController@showImportOrdersDataExcel')->middleware('permission:import_excel.show');

        //Gallery Image
        Route::group(['prefix' => 'gallery-image'], function () {
            Route::get('show-main', 'Admin\ImageUploadController@ShowDataImages')->middleware('permission:gallery_image.show');
            Route::get('show-gallery', 'Admin\ImageUploadController@listImage')->middleware('permission:gallery_image.show');
            Route::get('uploads', 'Admin\ImageUploadController@uploadImages')->middleware('permission:gallery_image.show');
            Route::post('upload/store', 'Admin\ImageUploadController@fileStore')->middleware('permission:gallery_image.create');
            Route::post('image/delete', 'Admin\ImageUploadController@fileDestroy')->middleware('permission:gallery_image.delete');
            Route::post('users/fileupload/', 'Admin\ImageUploadController@fileupload')->middleware('permission:gallery_image.create');
        });

        // Bank Transfer
        Route::group(['prefix' => 'bank'], function () {
            Route::get('midtrans-settings', 'Admin\MasterBankTransfer@midtransSettings')->middleware('permission:curriculum.show')->name('bank.show');
            Route::get('show-list-payment', 'Admin\MasterBankTransfer@showIndexListPayment')->middleware('permission:curriculum.show')->name('bank.show');
            Route::get('show', 'Admin\MasterBankTransfer@index')->middleware('permission:curriculum.show')->name('bank.show');
            Route::get('create', 'Admin\MasterBankTransfer@create')->middleware('permission:curriculum.create')->name('bank.create');
            Route::get('edit/{id}', 'Admin\MasterBankTransfer@update')->middleware('permission:curriculum.update')->name('bank.edit');
        });

        // Services
        Route::group(['prefix' => 'services'], function () {
            Route::get('show', 'Admin\MasterServicesController@index')->middleware('permission:curriculum.show')->name('services.show');
            Route::get('create', 'Admin\MasterServicesController@create')->middleware('permission:curriculum.show')->name('services.create');
            Route::get('edit/{id}', 'Admin\MasterServicesController@update')->middleware('permission:curriculum.show')->name('services.edit');
        });

        //Service Status
        Route::group(['prefix' => 'service-status'], function () {
            Route::get('show', 'Admin\MasterServicesController@serviceStatusIndex')->middleware('permission:curriculum.show');
        });


        // Development Plan
        Route::group(['prefix' => 'payemnt'], function () {
            Route::get('show', 'Admin\ListPaymentController@index')->middleware('permission:curriculum.show')->name('bank.show');
        });

        //Symptom
        Route::group(['prefix' => 'symptom'], function () {
            Route::get('show', 'Admin\MasterSymptomController@showListSymptom')->middleware('permission:symptom.show');
            Route::get('create', 'Admin\MasterSymptomController@create')->middleware('permission:symptom.create');
            Route::get('update/{id}', 'Admin\MasterSymptomController@edit')->middleware('permission:symptom.update');
        });

        //Symptom_code
        Route::group(['prefix' => 'symptom-code'], function () {
            Route::get('show', 'Admin\MasterSymptomCodeController@showListSymptom')->middleware('permission:symptom.show');
        });

        //repair Code
        Route::group(['prefix' => 'repair-code'], function () {
            Route::get('show', 'Admin\MasterRepairCodeController@showListSymptom')->middleware('permission:symptom.show');
        });

        //Service Type
        Route::group(['prefix' => 'service-type'], function () {
            Route::get('show', 'Admin\MasterServiceTypeController@showListServiceType')->middleware('permission:service_type.show');
            Route::get('create', 'Admin\MasterServiceTypeController@create')->middleware('permission:service_type.create');
            Route::get('update/{id}', 'Admin\MasterServiceTypeController@edit')->middleware('permission:service_type.update');
        });

        // tiekt
        Route::group(['prefix' => 'tickets'], function () {
            Route::get('/', 'Admin\MasterTicketController@index')->middleware('permission:ticket.show');
            Route::get('/{id}', 'Admin\MasterTicketController@detail')->name('tickets.detail')->middleware('permission:ticket.show');
        });

        // order
        Route::group(['prefix' => 'transactions'], function () {
            Route::get('create/{ticket_id?}', 'Admin\TransactionCreateController@index');
        });

        // Orders Data Excel
        Route::group(['prefix' => 'orders-data-excel'], function () {
            Route::get('show', 'Admin\MasterOrdersDataExcelController@index')->middleware('permission:orders_data_excel.show')->name('orders-data-excel.show');
            Route::get('create', 'Admin\MasterOrdersDataExcelController@create')->middleware('permission:orders_data_excel.create')->name('orders-data-excel.create');
            Route::get('edit/{id}', 'Admin\MasterOrdersDataExcelController@update')->middleware('permission:orders_data_excel.update')->name('orders-data-excel.edit');
            Route::get('detail/{id}', 'Admin\MasterOrdersDataExcelController@detail')->middleware('permission:orders_data_excel.show')->name('orders-data-excel.detail');
            Route::get('export', 'Admin\MasterOrdersDataExcelController@exportData')->middleware('permission:orders_data_excel.create')->name('orders-data-excel.export.api');

        });

        //Master Jasa Excel
         Route::group(['prefix' => 'jasa-excel'], function () {
            Route::get('show', 'Admin\MasterJasaExcelController@index')->middleware('permission:master_jasa.show')->name('jasa-excel.show.api');
            Route::get('import', 'Admin\MasterJasaExcelController@importData')->middleware('permission:master_jasa.create')->name('jasa-excel.export.api');
        });

        //Part Data Excel Stock
        Route::group(['prefix' => 'part-data-excel-stock'], function () {
            Route::get('show', 'Admin\MasterPartDataExcelStockController@index')->middleware('permission:master_part_data_excel_stock.show')->name('part-data-excel-stock.show.api');
            Route::get('mutation/{id}', 'Admin\MasterPartDataExcelStockController@mutationPdes')->middleware('permission:master_part_data_excel_stock.update');
            Route::get('history-in-out/{id}', 'Admin\MasterPartDataExcelStockController@historyInOutPdes')->middleware('permission:master_part_data_excel_stock.update');
            Route::get('export', 'Admin\MasterPartDataExcelStockController@exportData')->middleware('permission:master_part_data_excel_stock.create')->name('part-data-excel-stock.export.api');
        });

        // Part Data Excel Stock Inventory
        Route::group(['prefix' => 'part-data-excel-stock-inventory'], function () {
            Route::get('show', 'Admin\MasterPartDataExcelStockInventoryController@index')->middleware('permission:master_part_data_excel_stock_inventory.show')->name('part-data-excel-stock-inventory.show');

            Route::get('history-in-out/{id}', 'Admin\MasterPartDataExcelStockInventoryController@historyInOutPdesi')->middleware('permission:master_part_data_excel_stock.update');
            Route::get('create', 'Admin\MasterPartDataExcelStockInventoryController@create')->middleware('permission:master_part_data_excel_stock_inventory.create')->name('part-data-excel-stock-inventory.create');
            Route::get('edit/{id}', 'Admin\MasterPartDataExcelStockInventoryController@update')->middleware('permission:master_part_data_excel_stock_inventory.update')->name('part-data-excel-stock-inventory.edit');
            Route::get('export', 'Admin\MasterPartDataExcelStockInventoryController@exportData')->middleware('permission:master_part_data_excel_stock_inventory.create')->name('part-data-excel-stock-inventory.export.api');
        });
        Route::get('master-selling-price', 'Admin\MasterPartDataExcelStockInventoryController@masterSellingIndex')->middleware('permission:master_selling_price.show')->name('part-data-excel-stock-inventory.master-selling-price');

        // Part Data Excel Stock Bundle
        Route::group(['prefix' => 'part-data-excel-stock-bundle'], function () {
            Route::get('show', 'Admin\MasterPartDataExcelStockBundleController@index')->middleware('permission:master_part_data_excel_stock_bundle.show')->name('part-data-excel-stock-bundle.show');
            Route::get('create', 'Admin\MasterPartDataExcelStockBundleController@create')->middleware('permission:master_part_data_excel_stock_bundle.create')->name('part-data-excel-stock-bundle.create');
            Route::get('edit/{id}', 'Admin\MasterPartDataExcelStockBundleController@update')->middleware('permission:master_part_data_excel_stock_bundle.update')->name('part-data-excel-stock-bundle.edit');
            Route::get('detail/{id}', 'Admin\MasterPartDataExcelStockBundleController@detail')->middleware('permission:master_part_data_excel_stock_bundle.show')->name('part-data-excel-stock-bundle.detail');
            // Route::get('export', 'Admin\MasterPartDataExcelStockBundleController@exportData')->middleware('permission:master_part_data_excel_stock_bundle.create')->name('part-data-excel-stock-bundle.export.api');

        });

        // Leftover Stock
        Route::get('/leftover-stock', 'Admin\MasterPartDataExcelStockInventoryController@leftoverStock')->middleware('permission:master_part_data_excel_stock_inventory.show');

        //Mutation Logs Part Stock
        Route::get('/mutation-logs-part-stock/{pdes_id?}', 'Admin\MasterPartDataExcelStockController@showMutationHistories')->middleware('permission:master_part_data_excel_stock.show');

        ;
        Route::get('engineer-code-excel/show', 'Admin\MasterEngineerCodeExcelController@index')->middleware('permission:master_engineer_code_excel.show')->name('engineer-code-excel.show.api');
        ;
        Route::get('status-excel/show', 'Admin\MasterStatusExcelController@index')->middleware('permission:master_status_excel.show')->name('status-excel.show.api');
        ;
        Route::get('asc-excel/show', 'Admin\MasterASCExcelController@index')->middleware('permission:master_asc_excel.show')->name('asc-excel.show.api');
        ;
        Route::get('type-job-excel/show', 'Admin\MasterTypeJobExcelController@index')->middleware('permission:master_type_job_excel.show')->name('type-job-excel.show.api');
        ;
        // (Orders Data Excel & General Journal) Logs
        Route::get('data-logs', 'Admin\MasterOrdersDataExcelController@dataLogs')->middleware('permission:order.show')->name('orders-data-excel.logs');

        // General Journal
        Route::group(['prefix' => 'general-journal'], function () {
            Route::get('show', 'Admin\MasterGeneralJournalController@index')->middleware('permission:jurnal.show')->name('general-journal.show');
            Route::get('create', 'Admin\MasterGeneralJournalController@create')->middleware('permission:jurnal.create')->name('general-journal.create');
            Route::get('edit/{id}', 'Admin\MasterGeneralJournalController@update')->middleware('permission:jurnal.update')->name('general-journal.edit');
            Route::get('detail/{id}', 'Admin\MasterGeneralJournalController@detail')->middleware('permission:jurnal.show')->name('general-journal.detail');

            Route::get('export', 'Admin\MasterGeneralJournalController@exportData')->middleware('permission:jurnal.create')->name('general-journal.export.api');

            Route::get('export_detail_excel/{id}', 'Admin\MasterGeneralJournalController@exportDetailExcel')->middleware('permission:export.all')->name('general-journal.export.api');

        });
         // Report Hpp Items
         Route::get('/report-hpp-items', 'Admin\MasterGeneralJournalController@showReportHppItems')->middleware('permission:jurnal.show');

         // Report HPP Details
         Route::get('/report-hpp-items/details/{pdes_id}/{month}/{year}', 'Admin\MasterGeneralJournalController@showReportHppItemDetails')->middleware('permission:jurnal.show');

         // Report HPP Details /Year
         Route::get('/report-hpp-items/details-yearly/{pdes_id}', 'Admin\MasterGeneralJournalController@showReportHppItemDetailsYearly')->middleware('permission:jurnal.show');

         // Excel Income
         Route::group(['prefix' => 'excel-income'], function () {
            Route::get('show', 'Admin\MasterExcelIncomeController@index')->middleware('permission:excel-income.show')->name('excel-income.show');
            Route::get('create', 'Admin\MasterExcelIncomeController@create')->middleware('permission:excel-income.create')->name('excel-income.create');
            Route::get('edit/{id}', 'Admin\MasterExcelIncomeController@update')->middleware('permission:excel-income.update')->name('excel-income.edit');
            Route::get('detail/{id}', 'Admin\MasterExcelIncomeController@detail')->middleware('permission:excel-income.show')->name('excel-income.detail');
            Route::get('add_detail/{id}', 'Admin\MasterExcelIncomeController@addDetail')->middleware('permission:excel-income.show')->name('excel-income.add_detail');
            Route::get('edit_detail/{detail_id}', 'Admin\MasterExcelIncomeController@editDetail')->middleware('permission:excel-income.show')->name('excel-income.edit_detail');

            Route::get('export_detail_excel/{id}', 'Admin\MasterExcelIncomeController@exportDetailExcel')->middleware('permission:export.all')->name('excel-income.export.api');
            Route::get('export', 'Admin\MasterExcelIncomeController@exportData')->middleware('permission:export.all')->name('excel-income.export.api');

        });

        //List Transaction
        Route::group(['prefix' => 'transactions', 'middleware' => ['permission:order.show']], function () {
            Route::get('show', 'Admin\MasterOrderController@index');
            Route::get('show/newOrder', 'Admin\MasterOrderController@newOrder');
            Route::get('show/pending', 'Admin\MasterOrderController@pending');
            Route::get('show/approve-request', 'Admin\MasterOrderController@approveRequest');
            Route::get('show/pending-payment', 'Admin\MasterOrderController@pendingPayment');
            Route::get('show/processing', 'Admin\MasterOrderController@processing');
            Route::get('show/confirmation-payment', 'Admin\MasterOrderController@confrimPay');
            Route::get('show/done', 'Admin\MasterOrderController@done');
            Route::get('show/cancel', 'Admin\MasterOrderController@cancel');
            Route::get('/excel_download', 'Admin\MasterOrderController@previewDataOrder');
        });

        //List Transaction
        Route::group(['prefix' => 'klaim-garansi', 'middleware' => ['permission:order.show']], function () {
            Route::get('show', 'Admin\MasterOrderController@garansiIndex');
        });

        //List Transaction
        Route::group(['prefix' => 'list-review', 'middleware' => ['permission:order.show']], function () {
            Route::get('show', 'Admin\MasterOrderController@reviewIndex');
        });

        //List Transaction
        Route::group(['prefix' => 'transaction-success', 'middleware' => ['permission:order.show']], function () {
            Route::get('show/', 'Admin\MasterOrderController@showTransactionSuccess');
            Route::get('detail/{id}', 'Admin\MasterOrderController@detailSuccessOrder');
            Route::get('detail/pending/{order_id}', 'Admin\MasterOrderController@detailPendingOrder');
        });

        Route::group(['prefix' => 'transactions-delete', 'middleware' => ['permission:order.show']], function () {
            Route::get('show/Trash', 'Admin\MasterOrderController@showTrash');
        });

        Route::group(['prefix' => 'transaction-complaint', 'middleware' => ['permission:order.show']], function () {
            Route::get('show', 'Admin\MasterOrderController@showComplaintList');
        });

        Route::group(['prefix' => 'general-setting'], function () {
            Route::get('/', 'Admin\MasterSettingController@showFormCommissionSetting')->middleware('permission:setting.show');
        });

        Route::group(['prefix' => 'social-media'], function () {
            Route::get('/settings', 'Admin\MasterSettingController@socialMediaSetting')->middleware('permission:setting.show');
        });

        Route::group(['prefix' => 'social-login-setting'], function () {
            Route::get('/show', 'Admin\MasterSettingController@socialLogin')->middleware('permission:setting.show');
        });

        Route::group(['prefix' => 'google-maps-key-setting'], function () {
            Route::get('/', 'Admin\MasterSettingController@googleMapsKeySettings')->middleware('permission:setting.show');
        });

        Route::group(['prefix' => 'fireabse-setting'], function () {
            Route::get('/', 'Admin\MasterSettingController@firebaseSettings')->middleware('permission:setting.show');
        });

        Route::group(['prefix' => 'social-login-type-setting'], function () {
            Route::get('/', 'Admin\MasterSettingController@socialMediaLogin')->middleware('permission:setting.show');
        });

        // admin list topup wallet
        Route::group(['prefix' => 'topup', 'middleware' => ['permission:order.show']], function () {
            Route::get('/show-list', 'Admin\MasterOrderController@showListTopup');
            Route::get('/show-list-detail/{id}', 'Admin\MasterOrderController@showListDetailTopup');
            Route::get('/detail/{id}', 'Admin\MasterOrderController@detailListTopup');
        });


        //Get file Import Excel
        Route::get('get/logo/from-storage/{filename}', function ($filename) {
            $path = storage_path('app/public/web_setting_image/' . $filename);
            if (!File::exists($path)) {
                abort(404);
            }
            $file     = File::get($path);
            $type     = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        });

        Route::get('storage/banner/{filename}', function ($filename) {
            $path = storage_path('app/public/banner/' . $filename);
            if (!File::exists($path)) {
                abort(404);
            }
            $file     = File::get($path);
            $type     = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        });

        //Get file Import Excel
        Route::get('storage/{filename}', function ($filename) {
            $path = storage_path('app/public/file_import/' . $filename);
            if (!File::exists($path)) {
                abort(404);
            }
            $file     = File::get($path);
            $type     = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        });

        //Get file All
        Route::get('storage/other/{filename}', function ($filename) {
            $path = storage_path('app/public/file_import_all/' . $filename);
            if (!File::exists($path)) {
                abort(404);
            }
            $file     = File::get($path);
            $type     = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        });

        Route::get('storage/image/{filename}', function ($filename) {
            $userLogin = Auth::user()->id;
            $path      = storage_path('app/public/images/Id_' . $userLogin . '/' . $filename);
            if (!File::exists($path)) {
                abort(404);
            }
            $file     = File::get($path);
            $type     = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        });

        Route::get('storage/image/profile/{filename}', function ($filename) {
            $userLogin = Auth::user()->id;
            $path      = storage_path('app/public/ImageProfile/' . $filename);
            if (!File::exists($path)) {
                abort(404);
            }
            $file     = File::get($path);
            $type     = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        });



        Route::get('storage/icon/{filename}', function ($filename) {
            $userLogin = Auth::user()->id;
            $path      = storage_path('app/public/bankIcon/' . $filename);
            if (!File::exists($path)) {
                abort(404);
            }
            $file     = File::get($path);
            $type     = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        });

        Route::get('storage/attachment/{filename}', function ($filename) {
            $userLogin = Auth::user()->id;
            $path = storage_path('app/public/attachment/' . $filename);
            if (!File::exists($path)) {
                abort(404);
            }
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        });




        //Email Status order
        Route::group(['prefix' => 'email-status-order'], function () {
            Route::get('show', 'Admin\MasterEmailTemplateController@index')->middleware('permission:email_template.show');
            Route::get('create', 'Admin\MasterEmailTemplateController@create')->middleware('permission:email_template.create');
            Route::get('edit/{id}', 'Admin\MasterEmailTemplateController@update')->middleware('permission:email_template.update');
        });

        //Email other
        Route::group(['prefix' => 'email-other'], function () {
            Route::get('show', 'Admin\MasterEmailTemplateController@indexEmailOther')->middleware('permission:email_template.show');
            Route::get('create', 'Admin\MasterEmailTemplateController@createEmailOther')->middleware('permission:email_template.create');
            Route::get('edit/{id}', 'Admin\MasterEmailTemplateController@updateEmailOther')->middleware('permission:email_template.update');
        });

        //Report

        Route::get('report-customer', 'Admin\MasterReportController@customer')->middleware('permission:report_customer');
        Route::get('report-technician', 'Admin\MasterReportController@technician')->middleware('permission:report_teknisi');
        Route::get('report-order-service', 'Admin\MasterReportController@order_service')->middleware('permission:report_service');
        Route::get('chart-order-service', 'Admin\MasterReportController@chart_order_service')->middleware('permission:report_service');



        // Route::group(['prefix' => 'customer'], function () {
        //     Route::get('/request_job', 'Admin\TechnicianController@job_request_create');
        // });

        // ============================
        // teknisi
        // =======================

        // teknisi
        Route::group(['prefix' => 'technician'], function () {
            Route::get('/', 'Admin\TechnicianController@index')
                ->name('technician.index.web')
                ->middleware('permission:technician.show');
            // Route::get('/create', 'Admin\TechnicianController@create')
            //     ->name('technician.create.web');
            // Route::get('/profile', 'Admin\TechnicianController@profile')
            //     ->name('technician.profile.web');
            Route::get('/edit/{id}', 'Admin\TechnicianController@edit')
                ->name('technician.edit.web')
                ->middleware('permission:technician.update');
            Route::get('/info/{user_id}', 'Admin\TechnicianController@editInfo')
                ->name('technician.edit_info.web')
                ->middleware('permission:technician.update');
            // Route::get('/show-all-', 'Admin\TechnicianController@showAllTechnician')
            //     ->name('technician.edit.web');
        });

        Route::group(['prefix' => 'all-technician'], function () {
            Route::get('/show', 'Admin\TechnicianController@showAllTechnician')->middleware('permission:technician.show');
        });

        Route::group(['prefix' => 'general-voucher'], function () {
            Route::get('/show', 'Admin\MasterVoucherGeneralController@index')
                ->name('general.voucher.index');
            Route::get('/create', 'Admin\MasterVoucherGeneralController@create')
                ->name('general.voucher.create');
            Route::get('/update/{id}', 'Admin\MasterVoucherGeneralController@update')
                ->name('general.voucher.update');
        });

        Route::group(['prefix' => 'new-register-voucher'], function () {
            Route::get('/show', 'Admin\MasterVoucherGeneralController@newRegisterCreate')
                ->name('general.voucher');
        });

        // COMPANY
        Route::group(['prefix' => 'company'], function () {
            Route::get('/', 'Admin\CompanyController@index');
            Route::get('/list_outlet-by-company/{id}', 'Admin\CompanyController@showOutlet');
            Route::get('/list_outlet-by-company/update/{id}', 'Admin\CompanyController@updateOutlet');
        });

        // business_to_business
        Route::group(['prefix' => 'business_to_business'], function () {
            Route::get('/', 'Admin\BusinessToBusinessController@index')->middleware('permission:btb.show');
            Route::get('/log', 'Admin\BusinessToBusinessController@log')->middleware('permission:btb.show');
            Route::get('/{id}/show', 'Admin\BusinessToBusinessController@show')->middleware('permission:btb.show');
            Route::get('/{id}/detail', 'Admin\BusinessToBusinessController@detail')->middleware('permission:btb.show');
            Route::get('/{id}/detail-outlet', 'Admin\BusinessToBusinessController@detailOutlet')->middleware('permission:btb.show');
            Route::get('/{uniq_key}/download', 'Admin\BusinessToBusinessController@mediaDownload')->middleware('permission:btb.show');
        });

        // business-to-business-master-brand
        Route::group(['prefix' => 'business-to-business-master-brand'], function () {
            Route::get('/show', 'Admin\B2BMasterBrandController@index');
        });

        // business-to-business-master-type
        Route::group(['prefix' => 'business-to-business-master-type'], function () {
            Route::get('/show', 'Admin\B2BMasterTypeController@index');
        });

        // business-to-business-master-teknisi
        Route::group(['prefix' => 'business-to-business-master-teknisi'], function () {
            Route::get('/show', 'Admin\B2BMasterTeknisiController@index')->name('teknisi.show');
            Route::get('/create', 'Admin\B2BMasterTeknisiController@create');
            Route::get('/update/{id}', 'Admin\B2BMasterTeknisiController@update');
            Route::post('/store', 'Admin\B2BMasterTeknisiController@store');
            Route::post('/edit/{id}', 'Admin\B2BMasterTeknisiController@edit');
            Route::post('/destroy/{id}', 'Admin\B2BMasterTeknisiController@destroy');
        });
    });
});
