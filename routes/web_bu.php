<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('admin.template.login');
});

// Route::get('/uploads', function () {
//     return view('galeryImage');
// });

// Route::get('/list-galery',function(){
//     return view('listImages');
// });

// Auth::routes();
Route::post('/login', 'AuthController@login')->name('auth.login');
Route::post('/register', 'AuthController@register')->name('auth.register');
Route::get('logout', 'HomeController@logout');

Route::get('/verify', 'HomeController@verify');


Route::get('/email/verify/{id}', 'VerificationController@verify')->name('email.verification');
Route::post('/otp/verify', 'VerificationController@otpVerify')->name('otp.verification');
Route::post('/otp/resend', 'VerificationController@otpResend')->name('otp.resend.verification');
Route::post('/email/resend', 'VerificationController@resend')->name('email.resend.verification');

Route::get('/home', 'HomeController@showdashboar')->middleware('auth')->name('home');
Route::get('/register-form', 'HomeController@showregisterForm')->name('register.form');
Route::get('/forgot-password-form', 'HomeController@showForgotPasswordForm')->name('register.form');
Route::get('/create-password-form', 'HomeController@showCreatePasswordForm')->name('create.password.form');
Route::get('/verification-otp-form', 'HomeController@showOtpVerification')->name('verification.otp.form');

Route::group(['middleware' => ['auth']], function () {
    // web Setting
    Route::get('/web-setting', 'Admin\WebSettingController@webSetting')
        ->middleware('permission:menu.show')
        ->name('web.setting');

    // view Permission
    Route::get('/permission', 'Admin\PermissionController@showIndexPermission')
        ->middleware('permission:permission.show')
        ->name('index.permission');
    Route::get('/role', 'Admin\PermissionController@showIndexRole')
        ->middleware('permission:role.show')
        ->name('index.role');
    Route::get('/role-create', 'Admin\RoleController@create')
        ->middleware('permission:role.create')
        ->name('index.create');
    Route::get('/role-update/{id}', 'Admin\RoleController@edit')
        ->middleware('permission:role.update')
        ->name('index.update');

    //view manageUsers
    Route::get('/manage-users', 'Admin\ManageUsersController@showManageUsers')
        ->middleware('permission:user.show')
        ->name('manage.users');
    Route::get('/users-create', 'Admin\ManageUsersController@createManageUsers')
        ->middleware('permission:user.create')
        ->name('users.create');
    Route::get('/users-update/{id}', 'Admin\ManageUsersController@updateManageUsers')
        ->middleware('permission:user.update')
        ->name('update.create');

    //view Category
    Route::get('/list-category', 'Admin\CategoryController@showListCategory')
        ->middleware('permission:category.show')
        ->name('list.category');
    Route::get('/create-category', 'Admin\CategoryController@createListCategory')
        ->middleware('permission:category.view')
        ->name('create.category');
    Route::get('/create-child-category/{id}', 'Admin\CategoryController@createchildCategory')
        ->middleware('permission:category.create')
        ->name('create.child.category');
    Route::get('/update-category/{id}', 'Admin\CategoryController@updateListCategory')
        ->middleware('permission:category.update')
        ->name('update.category');

    // Master Vendor
    Route::get('/show-list-vendors', 'Admin\MasterVendorController@showListVendor')
        ->middleware('permission:vendor.show')
        ->name('show.list.vendors');
    Route::get('/create-vendors', 'Admin\MasterVendorController@createVendor')
        ->middleware('permission:vendor.create')
        ->name('create.vendors');
    Route::get('/update-vendors/{id}', 'Admin\MasterVendorController@updateVendor')
        ->middleware('permission:vendor.update')
        ->name('update.vendors');

    //view Menu
    Route::get('/show-menu', 'Admin\SettingMenuController@showListMenu')
        ->middleware('permission:menu.show')
        ->name('list.menu');
    Route::get('/create-menus', 'Admin\SettingMenuController@createMenus')
        ->middleware('permission:menu.create')
        ->name('create.menu');
    Route::get('/update-menus/{id}', 'Admin\SettingMenuController@updateMenus')
        ->middleware('permission:menu.update')
        ->name('update.menu');

    //view Profile
    Route::get('/show-profile', 'Admin\ProfileController@showProfile')
        ->middleware('permission:profile.show')
        ->name('show.profile');
    Route::get('/update-profile/{id}', 'Admin\ProfileController@updateProfile')
        ->middleware('permission:profile.update')
        ->name('update.profile');

    //view Address
    Route::get('/list-address', 'Admin\ProfileController@listCreate')
        ->middleware('permission:list_address.show')
        ->name('list.address');
    Route::get('/address-create', 'Admin\ProfileController@addressCreate')
        ->middleware('permission:list_address.create')
        ->name('address.create');
    Route::get('/detail-address/{id}', 'Admin\ProfileController@detailAddress')
        ->middleware('permission:list_address.view')
        ->name('detail.address');
    Route::get('/update-address/{id}', 'Admin\ProfileController@updateAddress')
        ->middleware('permission:list_address.update')
        ->name('update.address');

    // view City
    Route::get('/show-city', 'Admin\MasterCityController@showCity')
        ->middleware('permission:city.show')
        ->name('show.city');
    Route::get('/create-city', 'Admin\MasterCityController@createCity')
        ->middleware('permission:city.create')
        ->name('create.city');
    Route::get('/update-city/{id}', 'Admin\MasterCityController@updateCity')
        ->middleware('permission:city.update')
        ->name('update.city');

    //view Countries
    Route::get('/show-countries', 'Admin\MasterCountries@showCountries')
        ->middleware('permission:country.show')
        ->name('show.countries');
    Route::get('/create-countries', 'Admin\MasterCountries@createCountries')
        ->middleware('permission:country.create')
        ->name('create.countries');
    Route::get('/update-countries/{id}', 'Admin\MasterCountries@updateCountries')
        ->middleware('permission:country.update')
        ->name('update.countries');

    // view distric
    Route::get('/show-distric', 'Admin\MasterDistricController@showDistric')
        ->middleware('permission:district.show')
        ->name('show.distric');
    Route::get('/create-distric', 'Admin\MasterDistricController@createDistric')
        ->middleware('permission:district.create')
        ->name('create.distric');
    Route::get('/update-distric/{id}', 'Admin\MasterDistricController@updateDistric')
        ->middleware('permission:district.update')
        ->name('update.distric');

    //view Province
    Route::get('/show-province', 'Admin\MasterProvinceController@showprovince')
        ->middleware('permission:province.show')
        ->name('show.province');
    Route::get('/create-province', 'Admin\MasterProvinceController@createProvince')
        ->middleware('permission:province.create')
        ->name('create.province');
    Route::get('/update-province/{id}', 'Admin\MasterProvinceController@updateProvince')
        ->middleware('permission:province.update')
        ->name('update.province');

    // view vilage
    Route::get('/show-vilage', 'Admin\MasterVilageController@showVilage')
        ->middleware('permission:village.show')
        ->name('show.vilage');
    Route::get('/create-vilage', 'Admin\MasterVilageController@createVilage')
        ->middleware('permission:village.create')
        ->name('create.vilage');
    Route::get('/update-vilage/{id}', 'Admin\MasterVilageController@updateVilage')
        ->middleware('permission:village.update')
        ->name('update.vilage');

    // view zipcode
    Route::get('/show-zipcode', 'Admin\MasterZipCodeController@showZipCode')
        ->middleware('permission:zipcode.show')
        ->name('show.zipcode');
    Route::get('/create-zipcode', 'Admin\MasterZipCodeController@createZipCode')
        ->middleware('permission:zipcode.create')
        ->name('create.zipcode');
    Route::get('/update-zipcode/{id}', 'Admin\MasterZipCodeController@updateZipCode')
        ->middleware('permission:zipcode.update')
        ->name('update.zipcode');

    //view Product Category
    Route::get('/show-product-category', 'Admin\MasterProductCategoryController@showProductCategory')
        ->middleware('permission:product_category.show')
        ->name('show.product.category');
    Route::get('/create-product-category', 'Admin\MasterProductCategoryController@createProductCategory')
        ->middleware('permission:product_category.create')
        ->name('create.product.category');
    Route::get('/update-product-category/{id}', 'Admin\MasterProductCategoryController@updateProductCategory')
        ->middleware('permission:product_category.update')
        ->name('update.product.category');
    Route::get('/search-category', 'Admin\MasterProductCategoryController@search')
        ->middleware('permission:')
        ->name('search.category');

    //view Product Model
    Route::get('/show-product-model', 'Admin\MasterProductModelController@showProductModel')
        ->middleware('permission:product_model.show')
        ->name('show.product.model');
    Route::get('/create-product-model', 'Admin\MasterProductModelController@createProductModel')
        ->middleware('permission:product_model.create')
        ->name('create.product.model');
    Route::get('/update-product-model/{id}', 'Admin\MasterProductModelController@updateProductModel')
        ->middleware('permission:product_model.update')
        ->name('update.product.model');

    // view, create, delete, update master Attribute
    Route::get('/show-list-attribute', 'Admin\MasterAttributeController@showListAttribute')
        ->middleware('permission:product_attribute.show')
        ->name('show.list.attribute');
    Route::get('/create-master-attribute', 'Admin\MasterAttributeController@createMasterAttribute')
        ->middleware('permission:product_attribute.create')
        ->name('create.master.attribute');
    Route::get('/update-master-attribute/{id}', 'Admin\MasterAttributeController@updateMasterAttribute')
        ->middleware('permission:product_attribute.update')
        ->name('update.master.attribute');
    Route::get('/child-master-attribute-create/{id}', 'Admin\MasterAttributeController@childMasterAttribute')
        ->middleware('permission:product_attribute.create')
        ->name('child.master.attribute.create');

    // view, create, delete, update master Additional
    Route::get('/show-additional', 'Admin\MasterAdditionalController@showAddtitonal')
        ->middleware('permission:product_additional.show')
        ->name('show.additional');
    Route::get('/create-additional', 'Admin\MasterAdditionalController@createAddtitonal')
        ->middleware('permission:product_additional.create')
        ->name('create.additional');
    Route::get('/update-additional/{id}', 'Admin\MasterAdditionalController@updateAddtitonal')
        ->middleware('permission:product_additional.update')
        ->name('update.additional');

    //master part category
    Route::get('/show-list-part-category', 'Admin\MasterPartCategoryController@showPartCategory')
        ->middleware('permission:product_part_category.show')
        ->name('show.list.part-category');
    Route::get('/create-part-category', 'Admin\MasterPartCategoryController@createPartCategory')
        ->middleware('permission:product_part_category.create')
        ->name('create.part-category');
    Route::get('/update-part-category/{id}', 'Admin\MasterPartCategoryController@updatePartCategory')
        ->middleware('permission:product_part_category.update')
        ->name('update.part-category');

    //update master Prod brand
    Route::get('/show-list-product-brand', 'Admin\MasterProductBrandController@showProductBrand')
        ->middleware('permission:product_brand.show')
        ->name('show.list.product.brand');
    Route::get('/create-product-brand', 'Admin\MasterProductBrandController@createProductBrand')
        ->middleware('permission:product_brand.create')
        ->name('create.product.brand');
    Route::get('/update-product-brand/{id}', 'Admin\MasterProductBrandController@updateProductBrand')
        ->middleware('permission:product_brand.update')
        ->name('update.product.brand');

    // Setting Marital
    Route::get('/show-list-marital', 'Admin\MasterMaterilaController@showListMarital')
        ->middleware('permission:marital.show')
        ->name('show.list.marital');
    Route::get('/create-marital', 'Admin\MasterMaterilaController@createMarital')
        ->middleware('permission:marital.create')
        ->name('create.marital');
    Route::get('/update-marital/{id}', 'Admin\MasterMaterilaController@updateMarital')
        ->middleware('permission:marital.update')
        ->name('update.marital');

    // master religion
    Route::get('/religions', 'Admin\MasterReligionController@index')
        ->middleware('permission:religion.show')
        ->name('religion.index.web');

    // view, create, delete, update master Attribute
    Route::get('/show-list-attribute', 'Admin\MasterAttributeController@showListAttribute')
        ->middleware('permission:product_attribute.show')
        ->name('show.list.attribute');
    Route::get('/create-master-attribute', 'Admin\MasterAttributeController@createMasterAttribute')
        ->middleware('permission:product_attribute.create')
        ->name('create.master.attribute');
    Route::get('/update-master-attribute/{id}', 'Admin\MasterAttributeController@updateMasterAttribute')
        ->middleware('permission:product_attribute.update')
        ->name('update.master.attribute');
    Route::get('/child-master-attribute-create/{id}', 'Admin\MasterAttributeController@childMasterAttribute')
        ->middleware('permission:product_attribute.create')
        ->name('child.master.attribute.create');

    // master unit types
    Route::get('/show-list-unit-types', 'Admin\MasterUnitTypesController@showListUnitType')
        ->middleware('permission:unit_type.show')
        ->name('show.list.unit.types');

    // view warehouse
    Route::get('/show-list-warehouse', 'Admin\MasterWarehouseController@showWarehouse')
        ->middleware('permission:warehouse.show')
        ->name('show.warehouse');
    Route::get('/create-warehouse', 'Admin\MasterWarehouseController@createWarehouse')
        ->middleware('permission:warehouse.create')
        ->name('create.warehouse');
    Route::get('/update-warehouse/{id}', 'Admin\MasterWarehouseController@updateWarehouse')
        ->middleware('permission:warehouse.update')
        ->name('update.warehouse');

    // master product
    Route::get('/show-list-product', 'Admin\MasterProductController@showListProduct')
        ->middleware('permission:product.show')
        ->name('show.list.product');
    Route::get('/create-product', 'Admin\MasterProductController@createProduct')
        ->middleware('permission:product.create')
        ->name('create.product');
    Route::get('/edit-product/{id}', 'Admin\MasterProductController@editProduct')
        ->middleware('permission:product.update')
        ->name('edit.product');
    Route::get('/detail-product/{id}', 'Admin\MasterProductController@detailProduct')
        ->middleware('permission:product.show')
        ->name('detail.product');

    // master product status
    Route::get('/product_status', 'Admin\MasterProductStatusController@index')
        ->middleware('permission:product_status.show')
        ->name('product_status.index.web');

    //master price items
    Route::get('/show-list-price-items', 'Admin\MasterPriceItemsController@showListPriceItem')
        ->middleware('permission:price_item.show')
        ->name('show.list.price.items');
    Route::get('/update-price-items/{id}', 'Admin\MasterPriceItemsController@updatePriceItems')
        ->middleware('permission:price_item.update')
        ->name('update.price.items');

    // master inventory
    Route::get('/show-list-inventory', 'Admin\MasterInventoryController@showListInventory')
        ->middleware('permission:inventori.show')
        ->name('show.list.inventory');
    Route::get('/create-inventory', 'Admin\MasterInventoryController@createInventory')
        ->middleware('permission:inventori.create')
        ->name('create.inventory');
    Route::get('/update-inventory/{id}', 'Admin\MasterInventoryController@updateInventory')
        ->middleware('permission:inventori.update')
        ->name('update.inventory');

    // master Address type
    Route::get('/show-list-address-type', 'Admin\MasterTypeAddressController@showListAddressType')
        ->name('show.list.address.type');

    //Mass Import Master
    Route::get('/import-master', 'Admin\ImportExcelController@showImportExcelMaster')
        ->middleware('permission:import_master.show')
        ->name('show.import');

    //Mass Import Batch Inventory
    Route::get('/import-batch-inventory', 'Admin\ImportExcelController@showImportExcelBatchInventory')
    ->middleware('permission:import_batchinventory.show')
    ->name('show.import.batchinventory');

    //galery image
    Route::get('/show-list-image', 'Admin\ImageUploadController@fileCreate')
        // ->middleware('')
        ->name('list-image');
    Route::get('/list-galery', 'Admin\ImageUploadController@listImage')
        // ->middleware('')
        ->name('list-galery');
    Route::get('/uploads', 'Admin\ImageUploadController@uploadImages')
        // ->middleware('')
        ->name('uploads');
    Route::post('image/upload/store', 'Admin\ImageUploadController@fileStore')
        // ->middleware('')
        ->name('store');
    Route::post('/image/delete', 'Admin\ImageUploadController@fileDestroy');



    Route::post('/users/fileupload/','Admin\ImageUploadController@fileupload')->name('users.fileupload');

    //Batch
    Route::group(['prefix' => 'batch'], function() {
        Route::get('show', 'Admin\MasterBatchController@index')->middleware('permission:batch.show');
        Route::get('create', 'Admin\MasterBatchController@create')->middleware('permission:batch.create');
        Route::get('update/{id}', 'Admin\MasterBatchController@edit')->middleware('permission:batch.update');
        Route::get('status', 'Admin\MasterBatchController@statusIndex')->middleware('permission:batch.status');
    });


    // require base_path('routes/web/permission.php');
    // require base_path('routes/web/user.php');
    require base_path('routes/web/role.php');
    // require base_path('routes/web/batch.php');
});
