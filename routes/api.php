<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'Api\AuthController@login')->name('auth.login.api');
Route::post('/register', 'Api\AuthController@register')->name('auth.register.api');

Route::post('login-user-b2b', 'Api\UserB2bLoginController@login');
Route::post('register-user-b2b', 'Api\UserB2bLoginController@registerUserB2b');

Route::post('login-teknisi-b2b', 'Api\TeknisiB2bLoginController@login');


Route::post('/forgot-password', 'Api\AuthController@sendPasswordResetEmail');
Route::post('/register-customer', 'AuthController@register')->name('auth.register');
// Route::post('/order/update_changed_sparepart_on_working/{get_id_order}', 'Api\OrderController@approvedChangeSpareparts');

// reset password
Route::prefix('password')->group(function () {
    Route::post('/create', 'Api\PasswordResetController@create')
        ->name('password.reset.create.api');
    Route::post('/change', 'Api\PasswordResetController@change')
        ->name('password.reset.change.api');
    Route::get('/find/{token}', 'Api\PasswordResetController@find')
        ->name('password.reset.find.api');
});

Route::group(['middleware' => ['auth:api']], function () {

    Route::post('/firebase_send_push', 'NotifikasiController@firebaseSendMessage');

    // chat
    Route::group(['prefix' => 'chats'], function () {
        Route::put('/{user_id}/online', 'Customer\Api\ChatController@onlineUser');
        Route::put('/{user_id}/offline', 'Customer\Api\ChatController@offlineUser');
        Route::get('/teknisi/{user_id}/{chat_id}', 'Customer\Api\ChatController@detailAdmin');
        Route::get('/{chat_id}', 'Customer\Api\ChatController@detail');
        Route::post('/search', 'Customer\Api\ChatController@search');
        Route::post('/reply', 'Customer\Api\ChatController@reply');
        Route::post('/firebase_save_token', 'Customer\Api\ChatController@firebaseSaveToken');
        Route::group(['prefix' => 'action_order'], function () {
            Route::put('job_complate/{order_id}', 'Customer\Api\ChatController@actionJobComplate');
            Route::put('job_working/{order_id}', 'Customer\Api\ChatController@actionJobWorking');
            Route::put('job_done/{order_id}', 'Customer\Api\ChatController@actionJobDone');
        });
    });

    // order
    Route::group(['prefix' => 'order'], function () {
        Route::get('review-list', 'Api\OrderController@reviewListDatatables');
        Route::get('datatables/{technician_id?}', 'Api\OrderController@datatables');
        Route::get('datatables/search', 'Api\OrderController@datatablesSearch');
        Route::put('complate_job/{order_id}', 'Api\OrderController@toComplateJob');
        Route::put('accept_job/{order_id}', 'Api\OrderController@acceptJob');
        Route::put('reject_job/{order_id}', 'Api\OrderController@rejectJob');
        Route::put('accept_job_customer/{order_id}', 'Api\OrderController@acceptJobByCustomer');
        Route::put('assign_new_teknisi/{service_detail_id}', 'Api\OrderController@assignNewTeknisi');
        Route::put('garansi_revisit/{service_detail_id}', 'Api\OrderController@garansiRevisit');
        Route::put('garansi_customer_reply/{garansi_id}', 'Customer\Api\GaransiController@setCustomerReply');
        Route::put('garansi_reject/{garansi_id}', 'Customer\Api\GaransiController@rejectGaransi');
        Route::put('update_sparepart_detail/{order_id}', 'Api\OrderController@updateSparepartDetail');
        Route::put('bill_detail/{order_id}', 'Api\OrderController@updateBillDetail');
        Route::put('on_working/{order_id}', 'Api\OrderController@toOnWorking');
        Route::put('done_job/{order_id}', 'Api\OrderController@toDoneJob');
        Route::post('upload_media/{order_id}', 'Api\OrderController@uploadMedia');
    });

    Route::delete('order/destroy_sparepart_detail/{sparepart_detail_id}', 'Api\OrderController@destroySparepartDetail');
    Route::delete('order/destroy_item_detail/{item_detail_id}', 'Api\OrderController@destroyItemDetail');
    Route::post('/order/update_changed_sparepart_on_working/{get_id_order}', 'Api\OrderController@approvedChangeSpareparts');


    Route::group(['prefix' => 'technician-offer'], function () {
        Route::post('/update/{id}', 'HomeController@updateOfferTechnician');
    });

    // Email Verification Routes...
    Route::post('email/verify/{id}', 'Api\VerificationController@verify')
        ->name('verification.verify');
    Route::post('email/resend', 'Api\VerificationController@resend')
        ->name('verification.resend');

    // Permission
    Route::group(['prefix' => 'permission'], function () {
        Route::get('datatables', 'Api\PermissionController@datatables')->middleware('permission:permission.show')->name('permissions.datatables.api');
        Route::get('select2', 'Api\PermissionController@select2')->middleware('permission:permission.show')->name('permissions.select2.api');
        Route::get('', 'Api\PermissionController@index')->middleware('permission:permission.show')->name('permissions.index.api');
        Route::post('', 'Api\PermissionController@store')->middleware('permission:permission.create')->name('permissions.store.api');
        Route::get('{id}', 'Api\PermissionController@show')->middleware('permission:permission.create')->name('permissions.show.api');
        Route::put('{id}', 'Api\PermissionController@update')->middleware('permission:permission.update')->name('permissions.update.api');
        Route::delete('{id}', 'Api\PermissionController@destroy')->middleware('permission:permission.delete')->name('permissions.destroy.api');
    });

    // Role
    Route::group(['prefix' => 'role'], function () {
        Route::get('datatables', 'Api\RoleController@datatables')->middleware('permission:role.show')->name('roles.datatables.api');
        Route::get('select2', 'Api\RoleController@select2')->middleware('permission:role.show')->name('roles.select2.api');
        Route::get('', 'Api\RoleController@index')->middleware('permission:role.show')->name('roles.index.api');
        Route::post('', 'Api\RoleController@store')->middleware('permission:role.create')->name('roles.store.api');
        Route::get('{id}', 'Api\RoleController@show')->middleware('permission:role.create')->name('roles.show.api');
        Route::put('{id}', 'Api\RoleController@update')->middleware('permission:role.update')->name('roles.update.api');
        Route::delete('{id}', 'Api\RoleController@destroy')->middleware('permission:role.delete')->name('roles.destroy.api');
    });

    // User
    Route::group(['prefix' => 'user'], function () {
        Route::get('select2', 'Api\UserController@select2')->middleware('permission:user.show')->name('users.select2.api');
        Route::get('teknisi-select2', 'Api\UserController@select2Teknisi')->middleware('permission:user.show')->name('users.select2Teknisi.api');
        Route::get('datatables', 'Api\UserController@datatables')->middleware('permission:user.show')->name('users.datatables.api');
        Route::get('', 'Api\UserController@index')->middleware('permission:user.show')->name('users.index.api');
        Route::post('', 'Api\UserController@store')->middleware('permission:user.create')->name('users.store.api');
        Route::post('/info', 'Api\UserController@info')->middleware('permission:user.create')->name('users.store_info.api');
        Route::post('/address/create', 'Api\UserController@addressCreate')->middleware('permission:user.create')->name('users.store_address.api');
        Route::get('{id}', 'Api\UserController@show')->middleware('permission:user.create')->name('users.show.api');
        Route::put('{id}', 'Api\UserController@update')->middleware('permission:user.update')->name('users.update.api');
        Route::delete('{id}', 'Api\UserController@destroy')->middleware('permission:user.delete')->name('users.destroy.api');
        Route::put('status/{id}', 'Api\UserController@updateStatus')->middleware('permission:user.update')->name('users.update.api');
        Route::put('/change_password/{id}', 'Api\UserController@changePassword')->middleware('permission:user.update');
        Route::put('/change_status/{id}', 'Api\UserController@changStatus')->middleware('permission:user.update');
        Route::put('/main_address/{id}', 'Api\UserController@mainAddress')->middleware('permission:user.update');
        Route::post('reject/{id}', 'Api\UserController@updateStatusReject')->middleware('permission:user.update')->name('users.update.api');
        Route::put('/address/update/{id}', 'Api\UserController@updateAddress')->middleware('permission:user.update');
        Route::post('/profile/image/{id}', 'Api\UserController@updateProfileImage')->middleware('permission:user.update');
        Route::post('/otp/send', 'Api\UserController@otpSend')->name('otp.send');
        Route::post('/update/number', 'Api\UserController@updatePhoneNumber')->name('otp.send');
    });

    // b2b
    Route::group(['prefix' => 'user-b-2-b'], function () {
        Route::get('datatables', 'Api\UserB2BControler@datatables');
        Route::get('select2', 'Api\UserB2BControler@select2');
        Route::post('/', 'Api\UserB2BControler@store');
        Route::put('{id}', 'Api\UserB2BControler@update');
        Route::delete('{id}', 'Api\UserB2BControler@destroy');
        Route::put('/change_status/{id}', 'Api\UserB2BControler@changStatus');

        Route::get('/dropdown-get-user-b2b', 'Api\UserB2BControler@getUserB2b');
        Route::get('/dropdown-get-outlet', 'Api\UserB2BControler@getOutlet');
        Route::get('/dropdown-get-outlet-admin/{id}', 'Api\UserB2BControler@getOutletAdmin');
        // Route::get('/dropdown-get-outlet-admin', 'Api\UserB2BControler@getOutletAdmin');
        Route::get('/dropdown-get-unit/{id}', 'Api\UserB2BControler@getOutletUnit');
        Route::get('/show-list-data-cart', 'Api\UserB2BControler@showDataCart');

        Route::post('/show-list-quotation', 'Api\UserB2BControler@listQuotation');
        Route::post('/show-detail-quotation', 'Api\UserB2BControler@detailQuotation');
        Route::post('/show-detail-invoice', 'Api\UserB2BControler@detailInvoice');

        Route::post('/request-order-multiple', 'Api\UserB2BControler@addToCartOrder');
        Route::post('/request-order-single', 'Api\UserB2BControler@storeOrderRequest');
        Route::post('/update-request-order', 'Api\UserB2BControler@updateRequestOrder');
        Route::post('/request-order-tmp', 'Api\UserB2BControler@requestOrderTmp');
        Route::post('/request-order-tmp-save', 'Api\UserB2BControler@storeOrderInTmp');
        Route::post('/request-order-store', 'Api\UserB2BControler@requestOrderStore');
        Route::delete('/destroy-request-order/{id}', 'Api\UserB2BControler@destroyRequestOrder');
        Route::post('/destroy-list-cart', 'Api\UserB2BControler@destroyCartData');
    });

    Route::group(['prefix' => 'request-order-b-2-b'], function () {
        Route::get('/datatables', 'Api\OrderUserB2bController@datatables');
        Route::get('/datatables-transaction-detail/{request_code}', 'Api\OrderUserB2bController@datatablesTransDetail');
        Route::get('/history-orders', 'Api\OrderUserB2bController@historyOrder');
        Route::post('/search', 'Api\OrderUserB2bController@historyOrderSearch');
        Route::get('/list-outlet-by-company', 'Api\OrderUserB2bController@getOutletByCompany');
        Route::get('/list-unit-by-outlet/{id}', 'Api\OrderUserB2bController@getUnitByOutlet');
        Route::get('/list-unit-detail-outlet/{id}', 'Api\OrderUserB2bController@getUnitDetail');
        Route::get('show-berita-acara/{type}/{id}', 'Api\OrderUserB2bController@showBeritaAcara');
        Route::get('show-quotation/{type}/{id}', 'Api\OrderUserB2bController@showQuotation');
        Route::get('show-invoice/{type}/{id}', 'Api\OrderUserB2bController@showInvoice');
        Route::get('show-po/{type}/{id}', 'Api\OrderUserB2bController@showPo');
        Route::get('show-image-order/{code}', 'Api\OrderUserB2bController@showImageOrder');
        Route::post('get-list-teknisi', 'Admin\MasterOrderController@getListTeknisi');
        Route::post('assign-teknisi/{id}', 'Api\OrderUserB2bController@assignTeknisi');
        Route::post('update-schedule-teknisi/{id}', 'Api\OrderUserB2bController@updateScheduleTeknisi');
        Route::post('/request-job/hours-available-b2b', 'CustomerController@getHoursAvailableTeknisiB2b');
        Route::get('list-schedule-teknisi/{id}', 'Api\OrderUserB2bController@scheduleListTeknisi');

        Route::post('delete-berita-acara/{id}', 'Api\OrderUserB2bController@deleteBeritaAcara');
        Route::post('delete-quotation/{id}', 'Api\OrderUserB2bController@deleteQuotation');
        Route::post('delete-invoice/{id}', 'Api\OrderUserB2bController@deleteInvoice');
        Route::post('delete-po/{id}', 'Api\OrderUserB2bController@deletePo');

        // teknisi b2b
        Route::get('list-technician-schedule', 'Api\OrderUserB2bController@listTechnicianSchedule');
        Route::post('teknisi-b2b-checkin', 'Api\OrderUserB2bController@checkinTeknisi');
        Route::post('teknisi-b2b-checkout', 'Api\OrderUserB2bController@checkoutTeknisi');
        Route::post('save-quotation-order', 'Api\OrderUserB2bController@saveQuotationOrder');
        Route::post('save-and-edit-quotation-order/{id}', 'Api\OrderUserB2bController@editAndCreateQuotationOrder');
        Route::get('list-quotation-order/{id}', 'Api\OrderUserB2bController@listQuotationOrder');
        Route::delete('delete-part-quotation/{id}', 'Api\OrderUserB2bController@deletePartQuot');
        Route::delete('delete-schedule/{id}', 'Api\OrderUserB2bController@deleteShedule');


    });

    // Menu
    Route::group(['prefix' => 'menu'], function () {
        Route::get('datatables', 'Api\MenuController@datatables')->middleware('permission:menu.show')->name('menus.datatables.api');
        Route::get('select2', 'Api\MenuController@select2')->middleware('permission:menu.show')->name('menus.select2.api');
        Route::get('', 'Api\MenuController@index')->middleware('permission:menu.show')->name('menus.index.api');
        Route::post('', 'Api\MenuController@store')->middleware('permission:menu.create')->name('menus.store.api');
        Route::get('{menu}', 'Api\MenuController@show')->middleware('permission:menu.create')->name('menus.show.api');
        Route::put('{menu}', 'Api\MenuController@update')->middleware('permission:menu.update')->name('menus.update.api');
        Route::delete('{menu}', 'Api\MenuController@destroy')->middleware('permission:menu.delete')->name('menus.destroy.api');
        Route::post('move', 'Api\MenuController@updateMoveMenu')->middleware('permission:menu.update')->name('menus.move.api');
    });

    // Badge
    Route::group(['prefix' => 'badge'], function () {
        Route::get('datatables', 'Api\Master\BadgeController@datatables')->middleware('permission:user.show')->name('badge.datatables.api');
        Route::get('select2', 'Api\Master\BadgeController@select2')->middleware('permission:user.show')->name('badge.select2.api');
        Route::get('', 'Api\Master\BadgeController@index')->middleware('permission:user.show')->name('badge.index.api');
        Route::get('{badge}', 'Api\Master\BadgeController@show')->middleware('permission:user.create')->name('badge.show.api');
        Route::post('', 'Api\Master\BadgeController@store')->middleware('permission:user.create')->name('badge.store.api');
        Route::post('{badge}', 'Api\Master\BadgeController@update')->middleware('permission:user.update')->name('badge.update.api');
        Route::delete('destroy_batch', 'Api\Master\BadgeController@destroyBatch')->middleware('permission:user.delete')->name('badge.destroy_bacth.api');
        Route::delete('{badge}', 'Api\Master\BadgeController@destroy')->middleware('permission:user.delete')->name('badge.destroy.api');
    });

    // Product Attribute
    Route::group(['prefix' => 'product_attribute'], function () {
        Route::get('childs/{product_attribute_id}', 'Api\Product\ProductAttributeController@childs')->middleware('permission:product_attribute.show')->name('product_attribute.childs.api');
        Route::get('datatables', 'Api\Product\ProductAttributeController@datatables')->middleware('permission:product_attribute.show')->name('product_attribute.datatables.api');
        Route::get('select2', 'Api\Product\ProductAttributeController@select2')->middleware('permission:product_attribute.show')->name('product_attribute.select2.api');
        Route::get('', 'Api\Product\ProductAttributeController@index')->middleware('permission:product_attribute.show')->name('product_attribute.index.api');
        Route::post('', 'Api\Product\ProductAttributeController@store')->middleware('permission:product_attribute.create')->name('product_attribute.store.api');
        Route::get('{attribute}', 'Api\Product\ProductAttributeController@show')->middleware('permission:product_attribute.create')->name('product_attribute.show.api');
        Route::put('update_batch', 'Api\Product\ProductAttributeController@updateBatch')->middleware('permission:product_attribute.update')->name('product_attribute.update_batch.api');
        Route::put('{attribute}', 'Api\Product\ProductAttributeController@update')->middleware('permission:product_attribute.update')->name('product_attribute.update.api');
        Route::delete('destroy_child/{term}', 'Api\Product\ProductAttributeController@destroyChild')->middleware('permission:product_attribute.delete')->name('product_attribute.destroy_child.api');
        Route::delete('{attribute}', 'Api\Product\ProductAttributeController@destroy')->middleware('permission:product_attribute.delete')->name('product_attribute.destroy.api');
    });

    // Product Additional
    Route::group(['prefix' => 'product_additional'], function () {
        Route::get('datatables', 'Api\Product\ProductAdditionalController@datatables')->middleware('permission:product_additional.show')->name('product_additional.datatables.api');
        Route::get('select2', 'Api\Product\ProductAdditionalController@select2')->middleware('permission:product_additional.show')->name('product_additional.select2.api');
        Route::get('', 'Api\Product\ProductAdditionalController@index')->middleware('permission:product_additional.show')->name('product_additional.index.api');
        Route::get('{product_additional}', 'Api\Product\ProductAdditionalController@show')->middleware('permission:product_additional.create')->name('product_additional.show.api');
        Route::post('', 'Api\Product\ProductAdditionalController@store')->middleware('permission:product_additional.create')->name('product_additional.store.api');
        Route::put('{product_additional}', 'Api\Product\ProductAdditionalController@update')->middleware('permission:product_additional.update')->name('product_additional.update.api');
        Route::delete('destroy_batch', 'Api\Product\ProductAdditionalController@destroyBatch')->middleware('permission:product_additional.delete')->name('product_additional.destroy_bacth.api');
        Route::delete('{product_additional}', 'Api\Product\ProductAdditionalController@destroy')->middleware('permission:product_additional.delete')->name('product_additional.destroy.api');
    });
    // Product Part Category
    Route::group(['prefix' => 'product_part_category'], function () {
        Route::get('datatables', 'Api\Product\ProductPartCategoryController@datatables')->middleware('permission:product_part_category.show')->name('product_part_category.datatables.api');
        Route::get('select2', 'Api\Product\ProductPartCategoryController@select2')->middleware('permission:product_part_category.show')->name('product_part_category.select2.api');
        Route::get('', 'Api\Product\ProductPartCategoryController@index')->middleware('permission:product_part_category.show')->name('product_part_category.index.api');
        Route::post('', 'Api\Product\ProductPartCategoryController@store')->middleware('permission:product_part_category.create')->name('product_part_category.store.api');
        Route::get('{category}', 'Api\Product\ProductPartCategoryController@show')->middleware('permission:product_part_category.show')->name('product_part_category.show.api');
        Route::put('{category}', 'Api\Product\ProductPartCategoryController@update')->middleware('permission:product_part_category.update')->name('product_part_category.update.api');
        Route::delete('{category}', 'Api\Product\ProductPartCategoryController@destroy')->middleware('permission:product_part_category.delete')->name('product_part_category.destroy.api');
    });
    // Product Brand
    Route::group(['prefix' => 'product_brand'], function () {
        Route::get('datatables', 'Api\Product\ProductBrandController@datatables')->middleware('permission:product_brand.show')->name('product_brand.datatables.api');
        Route::get('select2', 'Api\Product\ProductBrandController@select2')->middleware('permission:product_brand.show')->name('product_brand.select2.api');
        Route::get('', 'Api\Product\ProductBrandController@index')->middleware('permission:product_brand.show')->name('product_brand.index.api');
        Route::post('', 'Api\Product\ProductBrandController@store')->middleware('permission:product_brand.create')->name('product_brand.store.api');
        Route::get('{brand}', 'Api\Product\ProductBrandController@show')->middleware('permission:product_brand.create')->name('product_brand.show.api');
        Route::put('{brand}', 'Api\Product\ProductBrandController@update')->middleware('permission:product_brand.update')->name('product_brand.update.api');
        Route::delete('{brand}', 'Api\Product\ProductBrandController@destroy')->middleware('permission:product_brand.delete')->name('product_brand.destroy.api');
    });
    // Product Category
    Route::group(['prefix' => 'product_category'], function () {
        Route::get('datatables', 'Api\Product\ProductCategoryController@datatables')->middleware('permission:product_category.show')->name('product_category.datatables.api');
        Route::get('select2', 'Api\Product\ProductCategoryController@select2')->middleware('permission:product_category.show')->name('product_category.select2.api');
        Route::get('', 'Api\Product\ProductCategoryController@index')->middleware('permission:product_category.show')->name('product_category.index.api');
        Route::post('', 'Api\Product\ProductCategoryController@store')->middleware('permission:product_category.create')->name('product_category.store.api');
        Route::get('{product_category}', 'Api\Product\ProductCategoryController@show')->middleware('permission:product_category.create')->name('product_category.show.api');
        Route::put('{product_category}', 'Api\Product\ProductCategoryController@update')->middleware('permission:product_category.update')->name('product_category.update.api');
        Route::delete('destroy_batch', 'Api\Product\ProductCategoryController@destroyBatch')->middleware('permission:product_category.delete')->name('product_category.destroy_batch.api');
        Route::delete('{product_category}', 'Api\Product\ProductCategoryController@destroy')->middleware('permission:product_category.delete')->name('product_category.destroy.api');
        Route::post('move', 'Api\Product\ProductCategoryController@updateMoveCategory')->middleware('permission:product_category.update')->name('product_category.store.api');
    });
    // Product Model
    Route::group(['prefix' => 'product_model'], function () {
        Route::get('datatables', 'Api\Product\ProductModelController@datatables')->middleware('permission:product_model.show')->name('product_model.datatables.api');
        Route::get('select2', 'Api\Product\ProductModelController@select2')->middleware('permission:product_model.show')->name('product_model.select2.api');
        Route::get('select2/{id}', 'Api\Product\ProductModelController@select2p')->middleware('permission:product_model.show')->name('product_model.select2.api');
        Route::get('', 'Api\Product\ProductModelController@index')->middleware('permission:product_model.show')->name('product_model.index.api');
        Route::post('', 'Api\Product\ProductModelController@store')->middleware('permission:product_model.create')->name('product_model.store.api');
        Route::get('{product_model}', 'Api\Product\ProductModelController@show')->middleware('permission:product_model.create')->name('product_model.show.api');
        Route::put('{product_model}', 'Api\Product\ProductModelController@update')->middleware('permission:product_model.update')->name('product_model.update.api');
        Route::delete('{product_model}', 'Api\Product\ProductModelController@destroy')->middleware('permission:product_model.delete')->name('product_model.destroy.api');
    });
    // Product
    Route::group(['prefix' => 'product'], function () {
        Route::get('datatables', 'Api\Master\ProductController@datatables')->middleware('permission:product.show')->name('product.datatables.api');
        Route::get('select2', 'Api\Master\ProductController@select2')->middleware('permission:product_status.show')->name('product.select2.api');
        Route::get('datatables/search', 'Api\Master\ProductController@searchDatatables')->middleware('permission:product_status.show')->name('product_status.datatables_search.api');
        Route::get('', 'Api\Master\ProductController@index')->middleware('permission:product.show')->name('product.index.api');
        Route::post('', 'Api\Master\ProductController@store')->middleware('permission:product.create')->name('product.store.api');
        Route::get('{product}', 'Api\Master\ProductController@detailsProduct')->middleware('permission:product.create')->name('product.show.api');
        Route::post('{product}', 'Api\Master\ProductController@update')->middleware('permission:product.update')->name('product.update.api');
        Route::delete('{product}', 'Api\Master\ProductController@destroy')->middleware('permission:product.delete')->name('product.destroy.api');
        Route::get('filter-by-date', 'Api\Master\ProductController@filterByDate')->middleware('permission:product.show')->name('product.filter.api');
    });
    //  Edit View (delete vendor or varian)
    Route::delete('product_vendor/{product_vendor}', 'Api\Master\ProductController@deleteProductVendor')->middleware('permission:product.delete');
    Route::delete('product_varian/{product_varian}', 'Api\Master\ProductController@deleteProductVarian')->middleware('permission:product.delete');
    // Product Status
    Route::group(['prefix' => 'product_status'], function () {
        Route::get('datatables', 'Api\Product\ProductStatusController@datatables')->middleware('permission:product_status.show')->name('product_status.datatables.api');
        Route::get('select2', 'Api\Product\ProductStatusController@select2')->middleware('permission:product_status.show')->name('product_status.select2.api');
        Route::get('', 'Api\Product\ProductStatusController@index')->middleware('permission:product_status.show')->name('product_status.index.api');
        Route::post('', 'Api\Product\ProductStatusController@store')->middleware('permission:product_status.create')->name('product_status.store.api');
        Route::get('{status}', 'Api\Product\ProductStatusController@show')->middleware('permission:product_status.create')->name('product_status.show.api');
        Route::put('{status}', 'Api\Product\ProductStatusController@update')->middleware('permission:product_status.update')->name('product_status.update.api');
        Route::delete('{status}', 'Api\Product\ProductStatusController@destroy')->middleware('permission:product_status.delete')->name('product_status.destroy.api');
    });

    // Vendor
    Route::group(['prefix' => 'vendor'], function () {
        Route::get('datatables', 'Api\Master\VendorController@datatables')->middleware('permission:vendor.show')->name('vendors.datatables.api');
        Route::get('select2', 'Api\Master\VendorController@select2')->name('vendors.select2.api');
        Route::get('', 'Api\Master\VendorController@index')->middleware('permission:vendor.show')->name('vendors.index.api');
        Route::post('', 'Api\Master\VendorController@store')->middleware('permission:vendor.create')->name('vendors.store.api');
        Route::get('{vendor}', 'Api\Master\VendorController@show')->middleware('permission:vendor.create')->name('vendors.show.api');
        Route::put('{vendor}', 'Api\Master\VendorController@update')->middleware('permission:vendor.update')->name('vendors.update.api');
        Route::delete('destroy_batch', 'Api\Master\VendorController@destroyBatch')->middleware('permission:vendor.delete')->name('vendors.destroy_bacth.api');
        Route::delete('{vendor}', 'Api\Master\VendorController@destroy')->middleware('permission:vendor.delete')->name('vendors.destroy.api');
    });
    // Address Type
    Route::group(['prefix' => 'address_type'], function () {
        Route::get('', 'Api\Master\AddressTypeController@index')->middleware('permission:address_type.show')->name('address.type.index.api');
        Route::get('datatables', 'Api\Master\AddressTypeController@datatables')->middleware('permission:address_type.show')->name('address.type.datatables.api');
        Route::get('select2', 'Api\Master\AddressTypeController@select2')->name('address.type.select2.api');
        Route::post('', 'Api\Master\AddressTypeController@store')->middleware('permission:address_type.create')->name('address.type.store.api');
        Route::put('{addresses}', 'Api\Master\AddressTypeController@update')->middleware('permission:address_type.update')->name('address.type.update.api');
        Route::delete('{addresses}', 'Api\Master\AddressTypeController@destroy')->middleware('permission:address_type.delete')->name('address.type.destroy.api');
    });
    // Marital
    Route::group(['prefix' => 'marital'], function () {
        Route::get('datatables', 'Api\Master\MaritalController@datatables')->middleware('permission:marital.show')->name('maritals.datatables.api');
        Route::get('select2', 'Api\Master\MaritalController@select2')->name('maritals.select2.api');
        Route::get('', 'Api\Master\MaritalController@index')->middleware('permission:marital.show')->name('maritals.index.api');
        Route::post('', 'Api\Master\MaritalController@store')->middleware('permission:marital.create')->name('maritals.store.api');
        Route::get('{marital}', 'Api\Master\MaritalController@show')->middleware('permission:marital.create')->name('maritals.show.api');
        Route::put('{marital}', 'Api\Master\MaritalController@update')->middleware('permission:marital.update')->name('maritals.update.api');
        Route::delete('destroy_batch', 'Api\Master\MaritalController@destroyBatch')->middleware('permission:marital.delete')->name('maritals.destroy_bacth.api');
        Route::delete('{marital}', 'Api\Master\MaritalController@destroy')->middleware('permission:marital.delete')->name('maritals.destroy.api');
    });
    // Religion
    Route::group(['prefix' => 'religion'], function () {
        Route::get('datatables', 'Api\Master\ReligionController@datatables')->middleware('permission:religion.show')->name('religions.datatables.api');
        Route::get('select2', 'Api\Master\ReligionController@select2')->name('religions.select2.api');
        Route::get('', 'Api\Master\ReligionController@index')->middleware('permission:religion.show')->name('religions.index.api');
        Route::post('', 'Api\Master\ReligionController@store')->middleware('permission:religion.create')->name('religions.store.api');
        Route::get('{religion}', 'Api\Master\ReligionController@show')->middleware('permission:religion.create')->name('religions.show.api');
        Route::put('{religion}', 'Api\Master\ReligionController@update')->middleware('permission:religion.update')->name('religions.update.api');
        Route::delete('destroy_batch', 'Api\Master\ReligionController@destroyBatch')->middleware('permission:religion.delete')->name('religions.destroy_bacth.api');
        Route::delete('{religion}', 'Api\Master\ReligionController@destroy')->middleware('permission:religion.delete')->name('religions.destroy.api');
    });
    // General Setting
    Route::group(['prefix' => 'general-setting'], function () {
        Route::post('save_commission', 'Api\Master\GeneralSettingController@store')->middleware('permission:setting.update');
        Route::post('save_key_midtrans', 'Api\Master\GeneralSettingController@storeServeyKeyMidtrans')->middleware('permission:setting.update');
        Route::post('switch_btn/{id}', 'Api\Master\GeneralSettingController@switchBtn')->middleware('permission:setting.update');
        Route::post('aggrement/save', 'Api\Master\GeneralSettingController@aggrementSave')->middleware('permission:setting.update');
        Route::post('aggrement-order/save', 'Api\Master\GeneralSettingController@aggrementOrderSave')->middleware('permission:setting.update');
        Route::post('privacy-police/save', 'Api\Master\GeneralSettingController@privacyPoliceStore')->middleware('permission:setting.update');
        Route::post('aggrement-order-completed/save', 'Api\Master\GeneralSettingController@aggrementOrderSave')->middleware('permission:setting.update');
        Route::post('social-media-save', 'Api\Master\GeneralSettingController@socialMediaSave')->middleware('permission:setting.update');
        Route::post('social-login-save', 'Api\Master\GeneralSettingController@socialLoginSave')->middleware('permission:setting.update');
        Route::post('/firebase-setting-save', 'Api\Master\GeneralSettingController@firebaseSettingUpdate')->middleware('permission:setting.update');
        Route::post('/social-login-setting-save', 'Api\Master\GeneralSettingController@socialLoginStore')->middleware('permission:setting.update');
        Route::put('{id}', 'Api\Master\GeneralSettingController@googleMapsKeySetting')->middleware('permission:setting.update');
    });
    // Batch
    Route::group(['prefix' => 'batch'], function () {
        Route::get('details/{id_batch?}', 'Api\Master\BatchController@details')->middleware('permission:batch.show')->name('batches.details.api');
        Route::get('info_template/{batch?}', 'Api\Master\BatchController@info_template')->middleware('permission:batch.show')->name('batches.info_template.api');
        Route::get('generate_batch_no', 'Api\Master\BatchController@generateBatchNo')->middleware('permission:batch.show')->name('batches.generate.api');
        Route::get('shipment-histories/datatables/{batch_id}', 'Api\Master\BatchController@datatablesShipmentHistories')->middleware('permission:batch.show')->name('batches.datatables.api');
        Route::get('batch_item_edit_order/datatables/{batch_id}', 'Api\Master\BatchController@datatablesBatchItemEditOrder')->middleware('permission:batch.show')->name('batches.datatables.api');
        Route::get('batch_item_reason/{batch_item_id}', 'Api\Master\BatchController@batchItemReason')->middleware('permission:batch.show')->name('batches.generate.api');
        Route::get('datatables', 'Api\Master\BatchController@datatables')->middleware('permission:batch.show')->name('batches.datatables.api');

        Route::get('datatables/search', 'Api\Master\BatchController@searchDatatables')->middleware('permission:batch.show')->name('batches.datatables_search.api');
        Route::post('update_item_order', 'Api\Master\BatchController@updateItemOrder')->middleware('permission:batch.show')->name('batches.datatables_search.api');
        Route::post('update_shipment_history', 'Api\Master\BatchController@updateShipmentHistory')->middleware('permission:batch.show')->name('batches.datatables_search.api');

        Route::get('select2', 'Api\Master\BatchController@select2')->name('batches.select2.api');
        Route::get('select2/accepted', 'Api\Master\BatchController@select2Accepted')->name('batches.select2Accepted.api');
        Route::get('child/select2', 'Api\Master\BatchController@select2child')->name('batches.select2.api');
        Route::get('', 'Api\Master\BatchController@index')->middleware('permission:batch.show')->name('batches.index.api');
        Route::post('', 'Api\Master\BatchController@store')->middleware('permission:batch.create')->name('batches.store.api');
        Route::get('{batch}', 'Api\Master\BatchController@show')->middleware('permission:batch.create')->name('batches.show.api');
        Route::put('{batch}', 'Api\Master\BatchController@update')->middleware('permission:batch.update')->name('batches.update.api');
        Route::delete('destroy_batch', 'Api\Master\BatchController@destroyBatch')->middleware('permission:batch.delete')->name('batches.destroy_bacth.api');
        Route::delete('{batch}', 'Api\Master\BatchController@destroy')->middleware('permission:batch.delete')->name('batches.destroy.api');
    });

    // Batch Status
    Route::group(['prefix' => 'batch-status'], function () {
        Route::get('/datatables', 'Api\Master\BatchStatusController@datatables')->middleware('permission:batch_status.show')->name('batch_status.datatables.api');
        Route::get('/select2', 'Api\Master\BatchStatusController@select2')->name('batch_status.select2.api');
        Route::get('/', 'Api\Master\BatchStatusController@index')->middleware('permission:batch_status.show')->name('batch_status.index.api');
        Route::post('/', 'Api\Master\BatchStatusController@store')->middleware('permission:batch_status.create')->name('batch_status.store.api');
        Route::get('/{status}', 'Api\Master\BatchStatusController@show')->middleware('permission:batch_status.create')->name('batch_status.show.api');
        Route::put('/{status}', 'Api\Master\BatchStatusController@update')->middleware('permission:batch_status.update')->name('batch_status.update.api');
        Route::delete('/{status}', 'Api\Master\BatchStatusController@destroy')->middleware('permission:batch_status.delete')->name('batch_status.destroy.api');
    });
    // Batch Status Shipment
    Route::group(['prefix' => 'batch-status-shipment'], function () {
        Route::post('set_accepted', 'Api\Master\BatchStatusShipmentController@setAccepted')->middleware('permission:batch_status.create')->name('batch_status_shipment.store.api');
        Route::get('/datatables', 'Api\Master\BatchStatusShipmentController@datatables')->middleware('permission:batch_status.show')->name('batch_status_shipment.datatables.api');
        Route::get('/select2', 'Api\Master\BatchStatusShipmentController@select2')->name('batch_status.select2.api');
        Route::get('/', 'Api\Master\BatchStatusShipmentController@index')->middleware('permission:batch_status.show')->name('batch_status_shipment.index.api');
        Route::post('/', 'Api\Master\BatchStatusShipmentController@store')->middleware('permission:batch_status.create')->name('batch_status_shipment.store.api');
        Route::get('/{status}', 'Api\Master\BatchStatusShipmentController@show')->middleware('permission:batch_status.create')->name('batch_status_shipment.show.api');
        Route::put('/{status}', 'Api\Master\BatchStatusShipmentController@update')->middleware('permission:batch_status.update')->name('batch_status_shipment.update.api');
        Route::delete('/{status}', 'Api\Master\BatchStatusShipmentController@destroy')->middleware('permission:batch_status.delete')->name('batch_status_shipment.destroy.api');
    });
    // Unit Type
    Route::group(['prefix' => 'unit-type'], function () {
        Route::get('datatables', 'Api\Master\UnitTypesController@datatables')->middleware('permission:unit_type.show')->name('unit.types.datatables.api');
        Route::get('select2', 'Api\Master\UnitTypesController@select2')->name('unit.types.select2.api');
        Route::get('', 'Api\Master\UnitTypesController@index')->middleware('permission:unit_type.show')->name('unit.types.index.api');
        Route::post('', 'Api\Master\UnitTypesController@store')->middleware('permission:unit_type.create')->name('unit.types.store.api');
        Route::get('{unitType}', 'Api\Master\UnitTypesController@show')->middleware('permission:unit_type.create')->name('unit.types.show.api');
        Route::put('{unitType}', 'Api\Master\UnitTypesController@update')->middleware('permission:unit_type.update')->name('unit.types.update.api');
        Route::delete('{unitType}', 'Api\Master\UnitTypesController@destroy')->middleware('permission:unit_type.delete')->name('unit.types.destroy.api');
    });
    // Warehouses
    Route::group(['prefix' => 'warehouses'], function () {
        Route::get('datatables', 'Api\Master\WarehouseController@datatables')->middleware('permission:warehouse.show')->name('warehouse.datatables.api');
        Route::get('select2', 'Api\Master\WarehouseController@select2')->name('warehouse.select2.api');
        Route::get('select2p/{warehouse}', 'Api\Master\WarehouseController@select2p')->middleware('permission:warehouse.show')->name('warehouse.select2.api');
        Route::get('', 'Api\Master\WarehouseController@index')->middleware('permission:warehouse.show')->name('warehouse.index.api');
        Route::post('', 'Api\Master\WarehouseController@store')->middleware('permission:warehouse.create')->name('warehouse.store.api');
        Route::get('{warehouse}', 'Api\Master\WarehouseController@show')->middleware('permission:warehouse.show')->name('warehouse.show.api');
        Route::put('{warehouse}', 'Api\Master\WarehouseController@update')->middleware('permission:warehouse.show')->name('warehouse.show.api');
        Route::delete('{warehouse}', 'Api\Master\WarehouseController@destroy')->middleware('permission:warehouse.delete')->name('warehouse.destroy.api');
    });
    // Price Item
    Route::group(['prefix' => 'price-item'], function () {
        Route::get('datatables', 'Api\Master\PriceItemsController@datatables')->middleware('permission:price_item.show')->name('price.items.datatables.api');
        Route::get('select2', 'Api\Master\PriceItemsController@select2')->middleware('permission:education.show')->name('price.items.select2.api');
        Route::get('', 'Api\Master\PriceItemsController@index')->middleware('permission:price_item.show')->name('price.items.index.api');
        Route::post('', 'Api\Master\PriceItemsController@store')->middleware('permission:price_item.create')->name('price.items.store.api');
        Route::put('{priceItem}', 'Api\Master\PriceItemsController@update')->middleware('permission:price_item.update')->name('price.items.update.api');
        Route::delete('{priceItem}', 'Api\Master\PriceItemsController@delete')->middleware('permission:price_item.delete')->name('price.items.delete.api');
    });
    // Inventory
    Route::group(['prefix' => 'inventory'], function () {
        Route::post('mutation', 'Api\Master\InventoryController@mutation')->middleware('permission:inventory.show')->name('inventori.select2.api');
        Route::get('mutation-logs', 'Api\Master\InventoryController@mutationLogsDatatables')->middleware('permission:inventory.show')->name('inventori.datatables.api');
        Route::get('mutation-histories/{inventory_id?}', 'Api\Master\InventoryController@inventoryMutationHistoriesDatatables')->middleware('permission:inventory.show')->name('inventori.datatables.api');
        Route::get('datatables', 'Api\Master\InventoryController@dataTable')->middleware('permission:inventory.show')->name('inventori.datatables.api');
        Route::get('select2', 'Api\Master\InventoryController@select2')->middleware('permission:inventory.show')->name('inventori.select2.api');
        Route::get('datatables/search', 'Api\Master\InventoryController@searchDatatables')->middleware('permission:inventory.show')->name('inventori.datatables_search.api');
        Route::get('', 'Api\Master\InventoryController@index')->middleware('permission:inventory.show')->name('inventori.index.api');
        Route::get('{id}/approval_modal_template', 'Api\Master\InventoryController@approvalModalTemplate')->middleware('permission:inventory.show')->name('inventori.approvalModalTemplate.api');
        Route::post('', 'Api\Master\InventoryController@store')->middleware('permission:inventory.create')->name('inventori.store.api');
        Route::put('{inventory}', 'Api\Master\InventoryController@update')->middleware('permission:inventory.update')->name('inventori.update.api');
        Route::put('{id}/approve', 'Api\Master\InventoryController@approve')->middleware('permission:inventory.update')->name('inventori.approve.api');
        Route::delete('{inventory}', 'Api\Master\InventoryController@destroy')->middleware('permission:inventory.delete')->name('inventori.destroy.api');
    });

    // Gallery Image
    Route::group(['prefix' => 'gallery-image'], function () {
        Route::get('{galeryImages}', 'Api\Master\GaleryImageController@show')->middleware('permission:gallery_image.show')->name('galeryImages.datatables.api');
        Route::put('{galeryImages}', 'Api\Master\GaleryImageController@update')->middleware('permission:gallery_image.update')->name('galeryImages.update.api');
    });

    // Country
    Route::group(['prefix' => 'country'], function () {
        Route::get('datatables', 'Api\Master\CountryController@datatables')->middleware('permission:country.show')->name('countries.datatables.api');
        Route::get('select2', 'Api\Master\CountryController@select2')->name('countries.select2.api');
        Route::get('', 'Api\Master\CountryController@index')->middleware('permission:country.show')->name('countries.index.api');
        Route::post('', 'Api\Master\CountryController@store')->middleware('permission:country.create')->name('countries.store.api');
        Route::post('importExcel', 'Api\Master\CountryController@importExcel')->middleware('permission:country.create')->name('countries.import.api');
        Route::get('{country}', 'Api\Master\CountryController@show')->middleware('permission:country.create')->name('countries.show.api');
        Route::put('{country}', 'Api\Master\CountryController@update')->middleware('permission:country.update')->name('countries.update.api');
        Route::delete('destroy_batch', 'Api\Master\CountryController@destroyBatch')->middleware('permission:country.delete')->name('countries.destroy_bacth.api');
        Route::delete('{country}', 'Api\Master\CountryController@destroy')->middleware('permission:country.delete')->name('countries.destroy.api');
    });
    // Province
    Route::group(['prefix' => 'province'], function () {
        Route::get('datatables', 'Api\Master\ProvinceController@datatables')->middleware('permission:province.show')->name('provinces.datatables.api');
        Route::get('select2', 'Api\Master\ProvinceController@select2')->name('provinces.select2.api');
        Route::get('', 'Api\Master\ProvinceController@index')->middleware('permission:province.show')->name('provinces.index.api');
        Route::post('', 'Api\Master\ProvinceController@store')->middleware('permission:province.create')->name('provinces.store.api');
        Route::post('importExcel', 'Api\Master\ProvinceController@importExcel')->middleware('permission:province.create')->name('countries.import.api');
        Route::get('{province}', 'Api\Master\ProvinceController@show')->middleware('permission:province.show')->name('provinces.show.api');
        Route::put('{province}', 'Api\Master\ProvinceController@update')->middleware('permission:province.update')->name('provinces.update.api');
        Route::delete('destroy_batch', 'Api\Master\ProvinceController@deleteByChecked')->middleware('permission:province.delete')->name('provinces.destroy_multiple.api');
        Route::delete('{province}', 'Api\Master\ProvinceController@destroy')->middleware('permission:province.delete')->name('provinces.destroy.api');
    });
    // City
    Route::group(['prefix' => 'city'], function () {
        Route::get('datatables', 'Api\Master\CityController@datatables')->middleware('permission:city.show')->name('cities.datatables.api');
        Route::get('select2', 'Api\Master\CityController@select2')->name('cities.select2.api');
        Route::get('', 'Api\Master\CityController@index')->middleware('permission:city.show')->name('cities.index.api');
        Route::post('', 'Api\Master\CityController@store')->middleware('permission:city.create')->name('cities.store.api');
        Route::post('importExcel', 'Api\Master\CityController@importExcel')->middleware('permission:province.create')->name('cities.import.api');
        Route::get('{city}', 'Api\Master\CityController@show')->middleware('permission:city.create')->name('cities.show.api');
        Route::put('{city}', 'Api\Master\CityController@update')->middleware('permission:city.update')->name('cities.update.api');
        Route::delete('destroy_batch', 'Api\Master\CityController@destroyBatch')->middleware('permission:city.delete')->name('cities.destroy_bacth.api');
        Route::delete('{city}', 'Api\Master\CityController@destroy')->middleware('permission:city.delete')->name('cities.destroy.api');
    });
    // District
    Route::group(['prefix' => 'district'], function () {
        Route::get('datatables', 'Api\Master\DistrictController@datatables')->middleware('permission:district.show')->name('districts.datatables.api');
        Route::get('select2', 'Api\Master\DistrictController@select2')->name('districts.select2.api');
        Route::get('', 'Api\Master\DistrictController@index')->middleware('permission:district.show')->name('districts.index.api');
        Route::post('', 'Api\Master\DistrictController@store')->middleware('permission:district.create')->name('districts.store.api');
        Route::post('importExcel', 'Api\Master\DistrictController@importExcel')->middleware('permission:district.create')->name('district.import.api');
        Route::get('{district}', 'Api\Master\DistrictController@show')->middleware('permission:district.create')->name('districts.show.api');
        Route::put('{district}', 'Api\Master\DistrictController@update')->middleware('permission:district.update')->name('districts.update.api');
        Route::delete('destroy_batch', 'Api\Master\DistrictController@destroyBatch')->middleware('permission:district.delete')->name('districts.destroy_bacth.api');
        Route::delete('{district}', 'Api\Master\DistrictController@destroy')->middleware('permission:district.delete')->name('districts.destroy.api');
    });
    // Village
    Route::group(['prefix' => 'village'], function () {
        Route::get('datatables', 'Api\Master\VillageController@datatables')->middleware('permission:village.show')->name('villages.datatables.api');
        Route::get('select2', 'Api\Master\VillageController@select2')->name('villages.select2.api');
        Route::get('', 'Api\Master\VillageController@index')->middleware('permission:village.show')->name('villages.index.api');
        Route::post('', 'Api\Master\VillageController@store')->middleware('permission:village.create')->name('villages.store.api');
        Route::post('importExcel', 'Api\Master\VillageController@importExcel')->middleware('permission:village.create')->name('village.import.api');
        Route::get('{village}', 'Api\Master\VillageController@show')->middleware('permission:village.create')->name('villages.show.api');
        Route::put('{village}', 'Api\Master\VillageController@update')->middleware('permission:village.update')->name('villages.update.api');
        Route::delete('destroy_batch', 'Api\Master\VillageController@destroyBatch')->middleware('permission:village.delete')->name('villages.destroy_bacth.api');
        Route::delete('{village}', 'Api\Master\VillageController@destroy')->middleware('permission:village.delete')->name('villages.destroy.api');
    });
    // Zipcode
    Route::group(['prefix' => 'zipcode'], function () {
        Route::get('/datatables', 'Api\Master\ZipcodeController@datatables')->middleware('permission:zipcode.show')->name('zipcode.datatables.api');
        Route::get('/select2', 'Api\Master\ZipcodeController@select2')->name('zipcode.select2.api');
        Route::get('/', 'Api\Master\ZipcodeController@index')->middleware('permission:zipcode.show')->name('zipcode.index.api');
        Route::post('/', 'Api\Master\ZipcodeController@store')->middleware('permission:zipcode.create')->name('zipcode.store.api');
        Route::post('/importExcel', 'Api\Master\ZipcodeController@importExcel')->middleware('permission:zipcode.create')->name('zipcode.import.api');
        Route::get('/{zipcode}', 'Api\Master\ZipcodeController@show')->middleware('permission:zipcode.create')->name('zipcode.show.api');
        Route::put('/{zipcode}', 'Api\Master\ZipcodeController@update')->middleware('permission:zipcode.update')->name('zipcode.update.api');
        Route::delete('/destroy_batch', 'Api\Master\ZipcodeController@destroyBatch')->middleware('permission:zipcode.delete')->name('zipcode.destroy_bacth.api');
        Route::delete('/{zipcode}', 'Api\Master\ZipcodeController@destroy')->middleware('permission:zipcode.delete')->name('zipcode.destroy.api');
    });

    // Additional Info
    Route::group(['prefix' => 'additional_info'], function () {
        Route::post('', 'Api\Master\AdditionalInfoController@store')->middleware('permission:additional_info.create')->name('additional_info.store.api');
    });

    //IMPORT EXCEL
    Route::post('/import-excel', 'Api\Master\ImportExcelController@importExcel')->middleware('permission:import_excel.show')->name('import.excel');

    // Import Batch Inventory
    Route::post('batchinventory/save-import-excel', 'Api\Master\ImportExcelController@saveImportExcel')->middleware('permission:import_excel.show')->name('import.excel');

    // Import Orders Data Excel
    Route::post('ordersdataexcel/save-import-excel', 'Api\Master\ImportExcelController@saveImportExcelOrdersDataExcel')->middleware('permission:import_excel.show')->name('import.excel');



    // User Address
    Route::group(['prefix' => 'user_address'], function () {
        Route::get('datatables', 'Api\Master\AddressController@datatables')->name('addresses.datatables.api');
        Route::get('datatables/teknisi', 'Api\Master\AddressController@datatablesTeknisi')->name('addresses.datatables.api');
        Route::get('/datatables_user/{user_id}', 'Api\Master\AddressController@datatablesUser')->name('addresses.datatables.api');
        Route::get('/', 'Api\Master\AddressController@index')->name('addresses.index.api');
        Route::post('/', 'Api\Master\AddressController@store')->name('addresses.store.api');
        Route::get('{address}', 'Api\Master\AddressController@show')->name('addresses.show.api');
        Route::put('main/{address}', 'Api\Master\AddressController@setMain')->name('addresses.setMain.api');
        Route::post('{address}', 'Api\Master\AddressController@update')->name('addresses.update.api');
        Route::delete('{address}', 'Api\Master\AddressController@destroy')->name('addresses.destroy.api');
    });
    // User Group
    Route::group(['prefix' => 'user_group'], function () {
        Route::get('datatables', 'Api\Master\UserGroupController@datatables')->middleware('permission:user_group.show')->name('user_group.datatables.api');
        Route::get('select2', 'Api\Master\UserGroupController@select2')->middleware('permission:user_group.show')->name('user_group.select2.api');
        Route::get('', 'Api\Master\UserGroupController@index')->middleware('permission:user_group.show')->name('user_group.index.api');
        Route::post('', 'Api\Master\UserGroupController@store')->middleware('permission:user_group.create')->name('user_group.store.api');
        Route::get('{user_group}', 'Api\Master\UserGroupController@show')->middleware('permission:user_group.create')->name('user_group.show.api');
        Route::put('main/{user_group}', 'Api\Master\UserGroupController@setMain')->middleware('permission:user_group.update')->name('user_group.setMain.api');
        Route::put('{user_group}', 'Api\Master\UserGroupController@update')->middleware('permission:user_group.update')->name('user_group.update.api');
        Route::delete('{user_group}', 'Api\Master\UserGroupController@destroy')->middleware('permission:user_group.delete')->name('user_group.destroy.api');
    });

    // teknisi
    Route::group(['prefix' => 'technician'], function () {
        Route::get('datatables', 'Api\Technician\TechnicianController@datatables')
            ->middleware('permission:technician.show')
            ->name('technician.datatables.api');

        Route::get('select2', 'Api\Technician\TechnicianController@select2')
            ->middleware('permission:technician.show')
            ->name('technician.select2.api');

        Route::get('', 'Api\Technician\TechnicianController@index')
            ->middleware('permission:technician.show')->name('technician.index.api');

        Route::get('{technician}', 'Api\Technician\TechnicianController@show')
            ->middleware('permission:technician.show')
            ->name('technician.show.api');

        Route::get('{technician}/experience_datatables', 'Api\Technician\TechnicianController@experienceDatatables')
            ->middleware('permission:technician.show')
            ->name('technician.show.api');

        Route::get('{technician}/curriculum_datatables', 'Api\Technician\TechnicianController@curriculumDatatables')
            ->middleware('permission:technician.show')
            ->name('technician.show.api');

        Route::get('{technician}/price_list_datatables', 'Api\Technician\TechnicianController@priceServiceDatatables')
            ->middleware('permission:technician.show')
            ->name('technician.show.api');

        Route::post('', 'Api\Technician\TechnicianController@store')
            ->middleware('permission:technician.create')
            ->name('technician.store.api');

        Route::post('{technician}/store_experience', 'Api\Technician\TechnicianController@storeExperience')
            ->middleware('permission:technician.create')
            ->name('technician.storeExperience.api');

        Route::post('{technician}/store_curriculum', 'Api\Technician\TechnicianController@storeCurriculum')
            ->middleware('permission:technician.create')
            ->name('technician.storeExperience.api');

        Route::delete('{id}/destroy_curriculum', 'Api\Technician\TechnicianController@curriculumDestroy')
            ->middleware('permission:technician.create')
            ->name('technician.storeExperience.api');

        Route::post('{technician}/store_price', 'Api\Technician\TechnicianController@storePriceService')
            ->middleware('permission:technician.create')
            ->name('technician.storePriceService.api');

        Route::post('{user_id}/store_info', 'Api\Technician\TechnicianController@storeInfo')
            ->middleware('permission:technician.create');

        Route::put('{technician}/job_info', 'Api\Technician\TechnicianController@updateJobInfo')
            ->middleware('permission:technician.update')
            ->name('technician.update.api');

        Route::put('{technician}/rek_info', 'Api\Technician\TechnicianController@updateRekInfo')
            ->middleware('permission:technician.update')
            ->name('technician.update.api');

        Route::delete('{technician}', 'Api\Technician\TechnicianController@destroy')
            ->middleware('permission:technician.delete')
            ->name('technician.destroy.api');

        Route::delete('{id}/price_service', 'Api\Technician\TechnicianController@priceServiceDestroy')
            ->middleware('permission:technician.delete');
    });

    // profile
    Route::group(['prefix' => 'technician_profile'], function () {
        Route::get('job_experience', 'Api\Technician\TechnicianProfileController@jobExperienceList')
            ->name('technician_profile.job_experience.datatables.api');
        Route::get('price_service', 'Api\Technician\TechnicianProfileController@priceServiceList')
            ->name('technician_profile.price_service.datatables.api');
        Route::post('job_info', 'Api\Technician\TechnicianProfileController@jobInfo')
            ->name('technician_profile.job_info.store.api');
        Route::post('rek_info', 'Api\Technician\TechnicianProfileController@rekInfo')
            ->name('technician_profile.job_info.store.api');
        Route::post('price_service_store', 'Api\Technician\TechnicianProfileController@priceServiceStore')
            ->name('technician_profile.price_service_store.store.api');
        Route::post('job_experience', 'Api\Technician\TechnicianProfileController@jobExperience')
            ->name('technician_profile.job_experience.store.api');
        Route::put('price_service_update/{id}', 'Api\Technician\TechnicianProfileController@priceServiceUpdate')
            ->name('technician_profile.price_service_store.update.api');
        Route::delete('{id}/job_experience', 'Api\Technician\TechnicianProfileController@jobExperienceDestroy');
        Route::delete('{id}/price_service', 'Api\Technician\TechnicianProfileController@priceServiceDestroy');
    });

    // Job Title Category
    Route::group(['prefix' => 'job_title_category'], function () {
        Route::get('datatables', 'Api\Technician\JobTitleCategoryController@datatables')->middleware('permission:job_title_category.show')->name('job_title_categories.datatables.api');
        Route::get('select2', 'Api\Technician\JobTitleCategoryController@select2')->name('job_title_categories.select2.api');
        Route::get('', 'Api\Technician\JobTitleCategoryController@index')->middleware('permission:job_title_category.show')->name('job_title_categories.index.api');
        Route::post('', 'Api\Technician\JobTitleCategoryController@store')->middleware('permission:job_title_category.create')->name('job_title_categories.store.api');
        Route::get('{category}', 'Api\Technician\JobTitleCategoryController@show')->middleware('permission:job_title_category.create')->name('job_title_categories.show.api');
        Route::put('{category}', 'Api\Technician\JobTitleCategoryController@update')->middleware('permission:job_title_category.update')->name('job_title_categories.update.api');
        Route::delete('destroy_batch', 'Api\Technician\JobTitleCategoryController@destroyBatch')->middleware('permission:job_title_category.delete')->name('job_title_categories.destroy_batch.api');
        Route::delete('{category}', 'Api\Technician\JobTitleCategoryController@destroy')->middleware('permission:job_title_category.delete')->name('job_title_categories.destroy.api');
    });
    // Job Title
    Route::group(['prefix' => 'job_title'], function () {
        Route::get('datatables', 'Api\Technician\JobTitleController@datatables')->middleware('permission:job_title.show')->name('job_titles.datatables.api');
        Route::get('select2', 'Api\Technician\JobTitleController@select2')->middleware('permission:job_title.show')->name('job_titles.select2.api');
        Route::get('', 'Api\Technician\JobTitleController@index')->middleware('permission:job_title.show')->name('job_titles.index.api');
        Route::post('', 'Api\Technician\JobTitleController@store')->middleware('permission:job_title.create')->name('job_titles.store.api');
        Route::get('{job}', 'Api\Technician\JobTitleController@show')->middleware('permission:job_title.create')->name('job_titles.show.api');
        Route::put('{job}', 'Api\Technician\JobTitleController@update')->middleware('permission:job_title.update')->name('job_titles.update.api');
        Route::delete('destroy_batch', 'Api\Technician\JobTitleController@destroyBatch')->middleware('permission:job_title.delete')->name('job_titles.destroy_batch.api');
        Route::delete('{job}', 'Api\Technician\JobTitleController@destroy')->middleware('permission:job_title.delete')->name('job_titles.destroy.api');
    });
    // Job Experience
    Route::group(['prefix' => 'job_experience'], function () {
        Route::get('datatables', 'Api\Technician\JobExperienceController@datatables')->middleware('permission:job_experience.show')->name('job_experiences.datatables.api');
        Route::get('select2', 'Api\Technician\JobExperienceController@select2')->middleware('permission:job_experience.show')->name('job_experiences.select2.api');
        Route::get('', 'Api\Technician\JobExperienceController@index')->middleware('permission:job_experience.show')->name('job_experiences.index.api');
        Route::post('', 'Api\Technician\JobExperienceController@store')->middleware('permission:job_experience.create')->name('job_experiences.store.api');
        Route::get('{experience}', 'Api\Technician\JobExperienceController@show')->middleware('permission:job_experience.create')->name('job_experiences.show.api');
        Route::put('{experience}', 'Api\Technician\JobExperienceController@update')->middleware('permission:job_experience.update')->name('job_experiences.update.api');
        Route::delete('destroy_batch', 'Api\Technician\JobExperienceController@destroyBatch')->middleware('permission:job_experience.delete')->name('job_experiences.destroy_batch.api');
        Route::delete('{experience}', 'Api\Technician\JobExperienceController@destroy')->middleware('permission:job_experience.delete')->name('job_experiences.destroy.api');
    });

    // User Status
    Route::group(['prefix' => 'user_status'], function () {
        Route::get('datatables', 'Api\Master\UserStatusController@datatables')->middleware('permission:user_status.show')->name('user_status.datatables.api');
        Route::get('select2', 'Api\Master\UserStatusController@select2')->middleware('permission:user_status.show')->name('user_status.select2.api');
        Route::get('', 'Api\Master\UserStatusController@index')->middleware('permission:user_status.show')->name('user_status.index.api');
        Route::post('', 'Api\Master\UserStatusController@store')->middleware('permission:user_status.create')->name('user_status.store.api');
        Route::put('{userStatus}', 'Api\Master\UserStatusController@update')->middleware('permission:user_status.update')->name('user_status.update.api');
        //  Route::delete('destroy_batch', 'Api\Master\UserStatusController@destroyBatch')->middleware('permission:user_status.delete')->name('user_status.destroy_batch.api');
        Route::delete('{userStatus}', 'Api\Master\UserStatusController@destroy')->middleware('permission:user_status.delete')->name('user_status.destroy.api');
    });

    // Address Search
    Route::group(['prefix' => 'address_search'], function () {
        Route::get('find_country/{country_id?}', 'Api\AddressSearchController@findCountry')->name('address_search.find_country.api');
        Route::get('find_province/{province_id?}', 'Api\AddressSearchController@findProvince')->name('address_search.find_province.api');
        Route::get('find_city/{city_id?}', 'Api\AddressSearchController@findCity')->name('address_search.find_city.api');
        Route::get('find_district/{district_id?}', 'Api\AddressSearchController@findDistrict')->name('address_search.find_district.api');
        Route::get('find_village/{village_id?}', 'Api\AddressSearchController@findVillage')->name('address_search.find_village.api');
        Route::get('find_zipcode/{zipcode_id?}', 'Api\AddressSearchController@findZipcode')->name('address_search.find_zipcode.api');
    });

    // development program
    Route::group(['prefix' => 'development_program'], function () {
        Route::get('datatables', 'Api\Master\DevelopmentProgramController@datatables')->middleware('permission:development_program.show')->name('development_program.datatables.api');
        Route::get('select2', 'Api\Master\DevelopmentProgramController@select2')->name('development_program.select2.api');
        Route::get('', 'Api\Master\DevelopmentProgramController@index')->middleware('permission:development_program.show')->name('development_program.index.api');
        Route::post('', 'Api\Master\DevelopmentProgramController@store')->middleware('permission:development_program.create')->name('development_program.store.api');
        Route::put('{dProgram}', 'Api\Master\DevelopmentProgramController@update')->middleware('permission:development_program.update')->name('development_program.update.api');
        //  Route::delete('destroy_batch', 'Api\Master\DevelopmentProgramController@destroyBatch')->middleware('permission:development_program.delete')->name('development_program.destroy_batch.api');
        Route::delete('{dProgram}', 'Api\Master\DevelopmentProgramController@destroy')->middleware('permission:development_program.delete')->name('development_program.destroy.api');
    });

    // Curriculum
    Route::group(['prefix' => 'curriculum'], function () {
        Route::get('datatables', 'Api\Master\CurriculumController@datatables')->middleware('permission:curriculum.show')->name('curriculum.datatables.api');
        Route::get('select2', 'Api\Master\CurriculumController@select2')->name('curriculum.select2.api');
        Route::get('', 'Api\Master\CurriculumController@index')->middleware('permission:curriculum.show')->name('curriculum.index.api');
        Route::post('', 'Api\Master\CurriculumController@store')->middleware('permission:curriculum.create')->name('curriculum.store.api');
        Route::put('{curiculum}', 'Api\Master\CurriculumController@update')->middleware('permission:curriculum.update')->name('curriculum.update.api');
        Route::delete('{curiculum}', 'Api\Master\CurriculumController@destroy')->middleware('permission:curriculum.delete')->name('curriculum.destroy.api');
    });

    // Education
    Route::group(['prefix' => 'education'], function () {
        Route::get('datatables', 'Api\Master\EducationsController@datatables')->middleware('permission:education.show')->name('education.datatables.api');
        Route::get('select2', 'Api\Master\EducationsController@select2')->name('education.select2.api');
        Route::get('', 'Api\Master\EducationsController@index')->middleware('permission:education.show')->name('education.index.api');
        Route::post('', 'Api\Master\EducationsController@store')->middleware('permission:education.create')->name('education.store.api');
        Route::put('{education}', 'Api\Master\EducationsController@update')->middleware('permission:education.update')->name('education.update.api');
        Route::delete('{education}', 'Api\Master\EducationsController@destroy')->middleware('permission:education.delete')->name('education.destroy.api');
    });

    // Curriculum Sequence
    Route::group(['prefix' => 'sequence'], function () {
        Route::get('datatables', 'Api\Master\CurriculumSequenceController@datatables')->middleware('permission:curriculum.show')->name('curriculum.datatables.api');
        Route::get('select2', 'Api\Master\CurriculumSequenceController@select2')->name('curriculum.select2.api');
        Route::get('', 'Api\Master\CurriculumSequenceController@index')->middleware('permission:curriculum.show')->name('curriculum.index.api');
        Route::post('', 'Api\Master\CurriculumSequenceController@store')->middleware('permission:curriculum.create')->name('curriculum.store.api');
        Route::put('{curSequence}', 'Api\Master\CurriculumSequenceController@update')->middleware('permission:curriculum.update')->name('curriculum.update.api');
        Route::delete('{curSequence}', 'Api\Master\CurriculumSequenceController@destroy')->middleware('permission:curriculum.delete')->name('curriculum.destroy.api');
    });

    // Dev Plant
    Route::group(['prefix' => 'development_plant'], function () {
        Route::get('datatables', 'Api\Master\DevelopmentPlantController@datatables')->middleware('permission:curriculum.show')->name('curriculum.datatables.api');
        Route::get('select2', 'Api\Master\DevelopmentPlantController@select2')->name('curriculum.select2.api');
        Route::get('', 'Api\Master\DevelopmentPlantController@index')->middleware('permission:curriculum.show')->name('curriculum.index.api');
        Route::post('', 'Api\Master\DevelopmentPlantController@store')->middleware('permission:curriculum.create')->name('curriculum.store.api');
        Route::put('{devPlant}', 'Api\Master\DevelopmentPlantController@update')->middleware('permission:curriculum.update')->name('curriculum.update.api');
        Route::delete('{devPlant}', 'Api\Master\DevelopmentPlantController@destroy')->middleware('permission:curriculum.delete')->name('curriculum.destroy.api');
    });

    // Bank Transfer
    Route::group(['prefix' => 'bank_transfer'], function () {
        Route::get('datatables', 'Api\Master\BankTransferController@datatables')->middleware('permission:curriculum.show')->name('bank_transfer.datatables.api');
        Route::get('select2', 'Api\Master\BankTransferController@select2')->middleware('permission:curriculum.show')->name('bank_transfer.select2.api');
        Route::get('', 'Api\Master\BankTransferController@index')->middleware('permission:curriculum.show')->name('bank_transfer.index.api');
        Route::post('', 'Api\Master\BankTransferController@store')->middleware('permission:curriculum.create')->name('bank_transfer.store.api');
        Route::post('{bankTransfer}', 'Api\Master\BankTransferController@update')->middleware('permission:curriculum.update')->name('bank_transfer.update.api');
        Route::delete('{bankTransfer}', 'Api\Master\BankTransferController@destroy')->middleware('permission:curriculum.delete')->name('bank_transfer.destroy.api');
    });

    // Teknisi Info
    Route::group(['prefix' => 'teknisi_info'], function () {
        Route::get('datatables', 'Api\Master\TeknisiInfoController@datatables')->middleware('permission:technician.show');
        Route::get('select2', 'Api\Master\TeknisiInfoController@select2')->middleware('permission:technician.show');
        Route::get('', 'Api\Master\TeknisiInfoController@index')->middleware('permission:technician.show');
        Route::post('', 'Api\Master\TeknisiInfoController@store')->middleware('permission:technician.create');
        Route::post('all', 'Api\Master\TeknisiInfoController@create');
        Route::post('update/all', 'Api\Master\TeknisiInfoController@updateDataTechnician');
        Route::put('{tekInfo}', 'Api\Master\TeknisiInfoController@update')->middleware('permission:technician.update');
        Route::delete('{tekInfo}', 'Api\Master\TeknisiInfoController@destroy')->middleware('permission:technician.delete');
    });

    // Services
    Route::group(['prefix' => 'services'], function () {
        Route::get('datatables', 'Api\Master\ServicesController@datatables')->middleware('permission:services.show')->name('services.datatables.api');
        Route::get('select2', 'Api\Master\ServicesController@select2')->name('services.select2.api');
        Route::get('', 'Api\Master\ServicesController@index')->middleware('permission:services.show')->name('services.index.api');
        Route::post('', 'Api\Master\ServicesController@store')->middleware('permission:services.create')->name('services.store.api');
        Route::post('{services}', 'Api\Master\ServicesController@update')->middleware('permission:services.update')->name('services.update.api');
        Route::delete('{services}', 'Api\Master\ServicesController@destroy')->middleware('permission:services.delete')->name('services.destroy.api');
    });

    // Service Status
    Route::group(['prefix' => 'service-status'], function () {
        Route::get('/datatables', 'Api\Master\ServiceStatusController@datatables')->middleware('permission:services.show')->name('service_status.datatables.api');
        Route::get('/select2', 'Api\Master\ServiceStatusController@select2')->name('service_status.select2.api');
        Route::get('/', 'Api\Master\ServiceStatusController@index')->middleware('permission:services.show')->name('service_status.index.api');
        Route::post('/', 'Api\Master\ServiceStatusController@store')->middleware('permission:services.create')->name('service_status.store.api');
        Route::get('/{status}', 'Api\Master\ServiceStatusController@show')->middleware('permission:services.create')->name('service_status.show.api');
        Route::put('/{status}', 'Api\Master\ServiceStatusController@update')->middleware('permission:services.update')->name('service_status.update.api');
        Route::delete('/{status}', 'Api\Master\ServiceStatusController@destroy')->middleware('permission:services.delete')->name('service_status.destroy.api');
    });

    // Order Status
    Route::group(['prefix' => 'order-status'], function () {
        Route::get('/datatables', 'Api\Master\OrderStatusController@datatables')->middleware('permission:services.show')->name('service_status.datatables.api');
        Route::get('/select2', 'Api\Master\OrderStatusController@select2')->name('order_status.select2.api');
        Route::get('/', 'Api\Master\OrderStatusController@index')->middleware('permission:services.show')->name('order_status.index.api');
        Route::post('/', 'Api\Master\OrderStatusController@store')->middleware('permission:services.create')->name('order_status.store.api');
        Route::get('/{status}', 'Api\Master\OrderStatusController@show')->middleware('permission:services.create')->name('order_status.show.api');
        Route::put('/{status}', 'Api\Master\OrderStatusController@update')->middleware('permission:services.update')->name('order_status.update.api');
        Route::delete('/{status}', 'Api\Master\OrderStatusController@destroy')->middleware('permission:services.delete')->name('order_status.destroy.api');
    });

    // Orders Data Excel
    Route::group(['prefix' => 'orders-data-excel'], function () {
         //Bundle Data
         Route::post('/add_bundle_data', 'Api\Master\OrdersDataExcelController@addBundleData');
         Route::post('/update_bundle_data/{bundle_id}', 'Api\Master\OrdersDataExcelController@updateBundleData');
         Route::delete('/destroy_bundle_data/{bundle_excel_id}', 'Api\Master\OrdersDataExcelController@destroyBundleData');

         //Part Data
         Route::post('update_part_data/{orders_data_excel}', 'Api\Master\OrdersDataExcelController@updatePartData');
         Route::delete('/destroy_part_data/{orders_data_excel}/{mutation_id?}', 'Api\Master\OrdersDataExcelController@destroyPartData');

         //Jasa Data
         Route::post('/update_jasa_data/{orders_data_excel}', 'Api\Master\OrdersDataExcelController@updateJasaData');
         Route::delete('/destroy_jasa_data/{jasa_data_excel}/', 'Api\Master\OrdersDataExcelController@destroyJasaData');

        Route::get('/datatables', 'Api\Master\OrdersDataExcelController@datatables')->middleware('permission:orders_data_excel.show')->name('orders-data-excel.datatables.api');
        Route::get('datatables/search', 'Api\Master\OrdersDataExcelController@datatablesSearch')->middleware('permission:orders_data_excel.show')->name('orders-data-excel.datatables_search.api');
        Route::get('/datatables_logs/{orders_data_excel}/{type?}', 'Api\Master\OrdersDataExcelController@datatablesLogs')->middleware('permission:orders_data_excel.show')->name('orders-data-excel.datatables.api');
        Route::get('/', 'Api\Master\OrdersDataExcelController@index')->middleware('permission:orders_data_excel.show')->name('orders-data-excel.index.api');
        Route::post('/', 'Api\Master\OrdersDataExcelController@store')->middleware('permission:orders_data_excel.create')->name('orders-data-excel.store.api');
        Route::get('/{orders_data_excel}', 'Api\Master\OrdersDataExcelController@show')->middleware('permission:orders_data_excel.create')->name('orders-data-excel.show.api');
        Route::post('/{orders_data_excel}', 'Api\Master\OrdersDataExcelController@update')->middleware('permission:orders_data_excel.update')->name('orders-data-excel.update.api');
        Route::delete('/{orders_data_excel}', 'Api\Master\OrdersDataExcelController@destroy')->middleware('permission:orders_data_excel.delete')->name('orders-data-excel.destroy.api');



    });

    // Master Jasa Excel (use on orders-data-excel)
    Route::group(['prefix' => 'jasa-excel'], function () {
        Route::get('datatables', 'Api\Master\JasaExcelController@datatables')->middleware('permission:master_jasa.show')->name('master-jasa.datatables.api');
        Route::get('datatables/search', 'Api\Master\JasaExcelController@datatablesSearch')->middleware('permission:master_jasa.show')->name('part-data-excel-stock.datatables.api');
        Route::get('select2/', 'Api\Master\JasaExcelController@select2')->name('master-jasa.select2.api');
        Route::get('', 'Api\Master\JasaExcelController@index')->middleware('permission:master_jasa.show')->name('master-jasa.index.api');
        Route::post('', 'Api\Master\JasaExcelController@store')->middleware('permission:master_jasa.create')->name('master-jasa.store.api');
        Route::get('{je}', 'Api\Master\JasaExcelController@show')->middleware('permission:master_jasa.create')->name('master-jasa.show.api');
        Route::put('{je}', 'Api\Master\JasaExcelController@update')->middleware('permission:master_jasa.update')->name('master-jasa.update.api');
        Route::delete('{je}', 'Api\Master\JasaExcelController@destroy')->middleware('permission:master_jasa.delete')->name('master-jasa.destroy.api');
        Route::delete('destroy_batch', 'Api\Master\JasaExcelController@destroyBatch')->middleware('permission:master_jasa.delete')->name('master-jasa.destroy_bacth.api');
    });

    // Part Data Excel Stock
    Route::group(['prefix' => 'part-data-excel-stock'], function () {
        Route::get('datatables', 'Api\Master\PartDataExcelStockController@datatables')->middleware('permission:master_part_data_excel_stock.show')->name('part-data-excel-stock.datatables.api');
        Route::get('datatables/search', 'Api\Master\PartDataExcelStockController@datatablesSearch')->middleware('permission:master_part_data_excel_stock.show')->name('part-data-excel-stock.datatables.api');
        Route::get('select2/{search_by}/{asc_id?}/{filter_stock?}', 'Api\Master\PartDataExcelStockController@select2')->name('part-data-excel-stock.select2.api');
        Route::get('select2', 'Api\Master\PartDataExcelStockController@select2B2B')->name('part-data-excel-stock.select2.api');
        // filter_stock(filter_petty,filter_order)
        Route::get('', 'Api\Master\PartDataExcelStockController@index')->middleware('permission:master_part_data_excel_stock.show')->name('part-data-excel-stock.index.api');
        Route::post('', 'Api\Master\PartDataExcelStockController@store')->middleware('permission:master_part_data_excel_stock.create')->name('part-data-excel-stock.store.api');
        Route::get('{pdes}', 'Api\Master\PartDataExcelStockController@show')->middleware('permission:master_part_data_excel_stock.create')->name('part-data-excel-stock.show.api');
        Route::put('{pdes}', 'Api\Master\PartDataExcelStockController@update')->middleware('permission:master_part_data_excel_stock.update')->name('part-data-excel-stock.update.api');
        Route::delete('{pdes}', 'Api\Master\PartDataExcelStockController@destroy')->middleware('permission:master_part_data_excel_stock.delete')->name('part-data-excel-stock.destroy.api');
        Route::delete('destroy_batch', 'Api\Master\PartDataExcelStockController@destroyBatch')->middleware('permission:master_part_data_excel_stock.delete')->name('part-data-excel-stock.destroy_bacth.api');
    });

    // Part Data Stock Excel Stock Bundle
    Route::group(['prefix' => 'part-data-excel-stock-bundle'], function () {
        Route::get('/datatables', 'Api\Master\PartDataExcelStockBundleController@datatables')->middleware('permission:master_part_data_excel_stock_bundle.show')->name('part-data-excel-stock-bundle.datatables.api');
        Route::get('/details-data/{pdesb_id}', 'Api\Master\PartDataExcelStockBundleController@datatablesBundleDetails')->middleware('permission:master_part_data_excel_stock_bundle.show')->name('part-data-excel-stock-bundle.datatables-bundle-details.api');
        Route::get('datatables/search', 'Api\Master\PartDataExcelStockBundleController@datatablesSearch')->middleware('permission:master_part_data_excel_stock_bundle.show')->name('part-data-excel-stock-bundle.datatables_search.api');
        Route::get('/datatables_logs/{pdesb}/{type?}', 'Api\Master\PartDataExcelStockBundleController@datatablesLogs')->middleware('permission:master_part_data_excel_stock_bundle.show')->name('part-data-excel-stock-bundle.datatables.api');
        Route::get('/', 'Api\Master\PartDataExcelStockBundleController@index')->middleware('permission:master_part_data_excel_stock_bundle.show')->name('part-data-excel-stock-bundle.index.api');
        Route::post('/', 'Api\Master\PartDataExcelStockBundleController@store')->middleware('permission:master_part_data_excel_stock_bundle.create')->name('part-data-excel-stock-bundle.store.api');
        Route::get('/{pdesb}', 'Api\Master\PartDataExcelStockBundleController@show')->middleware('permission:master_part_data_excel_stock_bundle.create')->name('part-data-excel-stock-bundle.show.api');
        Route::post('/{pdesb}', 'Api\Master\PartDataExcelStockBundleController@update')->middleware('permission:master_part_data_excel_stock_bundle.update')->name('part-data-excel-stock-bundle.update.api');
        Route::delete('/{pdesb}', 'Api\Master\PartDataExcelStockBundleController@destroy')->middleware('permission:master_part_data_excel_stock_bundle.delete')->name('part-data-excel-stock-bundle.destroy.api');
        Route::get('select2/{asc_id?}', 'Api\Master\PartDataExcelStockBundleController@select2')->middleware('permission:master_part_data_excel_stock_bundle.delete')->name('part-data-excel-stock-bundle.select2.api');

        Route::delete('/destroy_details_data/{pdesbd}', 'Api\Master\PartDataExcelStockBundleController@destroyDetails');

    });

    // Part Data Excel Stock Inventory
    Route::group(['prefix' => 'part-data-excel-stock-inventory'], function () {
        //Get Selling Price
        Route::get('get-selling-price/{pdesi_id}', 'Api\Master\PartDataExcelStockInventoryController@getSellingPrice')->middleware('permission:master_part_data_excel_stock_inventory.show')->name('part-data-excel-stock-inventory.get_selling_price.api');
        //Set Selling Price
        Route::post('set-selling-price', 'Api\Master\PartDataExcelStockInventoryController@setSellingPrice')->middleware('permission:master_part_data_excel_stock_inventory.update')->name('part-data-excel-stock-inventory.set_selling_price.api');

        Route::post('mutation', 'Api\Master\PartDataExcelStockInventoryController@mutation')->middleware('permission:master_part_data_excel_stock_inventory.show')->name('part-data-excel-stock-inventory.mutation.api');
        Route::get('mutation-logs', 'Api\Master\PartDataExcelStockInventoryController@mutationLogsDatatables')->middleware('permission:master_part_data_excel_stock_inventory.show')->name('part-data-excel-stock-inventory.datatables.api');
        Route::get('histories-in-out/{pdes_id?}', 'Api\Master\PartDataExcelStockInventoryController@pdesInOutHistoriesDatatables')->middleware('permission:master_part_data_excel_stock_inventory.show')->name('part-data-excel-stock-inventory.datatables.api');
        Route::get('leftover-stock', 'Api\Master\PartDataExcelStockInventoryController@leftoverStockDatatables')->middleware('permission:master_part_data_excel_stock_inventory.show')->name('part-data-excel-stock-inventory.datatables.api');
        Route::get('datatables', 'Api\Master\PartDataExcelStockInventoryController@datatables')->middleware('permission:master_part_data_excel_stock_inventory.show')->name('part-data-excel-stock-inventory.datatables.api');
        Route::get('datatables/search', 'Api\Master\PartDataExcelStockInventoryController@datatablesSearch')->middleware('permission:master_part_data_excel_stock_inventory.show')->name('part-data-excel-stock-inventory.datatables.api');
        Route::get('select2/{search_by}/{asc_id?}/{not_in_asc_id?}', 'Api\Master\PartDataExcelStockInventoryController@select2')->name('part-data-excel-stock-inventory.select2.api');
        Route::get('select2/{search_by}/{asc_id?}/{filter_stock?}', 'Api\Master\PartDataExcelStockController@select2')->name('part-data-excel-stock.select2.api');
        Route::get('select2', 'Api\Master\PartDataExcelStockController@select2B2B')->name('part-data-excel-stock.select2.api');
        // filter_stock(filter_petty,filter_order)
        Route::get('', 'Api\Master\PartDataExcelStockInventoryController@index')->middleware('permission:master_part_data_excel_stock_inventory.show')->name('part-data-excel-stock-inventory.index.api');
        Route::post('', 'Api\Master\PartDataExcelStockInventoryController@store')->middleware('permission:master_part_data_excel_stock_inventory.create')->name('part-data-excel-stock-inventory.store.api');
        Route::get('{pdesi}', 'Api\Master\PartDataExcelStockInventoryController@show')->middleware('permission:master_part_data_excel_stock_inventory.create')->name('part-data-excel-stock-inventory.show.api');
        Route::post('{pdesi}', 'Api\Master\PartDataExcelStockInventoryController@update')->middleware('permission:master_part_data_excel_stock_inventory.update')->name('part-data-excel-stock-inventory.update.api');
        Route::delete('{pdesi}', 'Api\Master\PartDataExcelStockInventoryController@destroy')->middleware('permission:master_part_data_excel_stock_inventory.delete')->name('part-data-excel-stock-inventory.destroy.api');
        Route::delete('destroy_batch', 'Api\Master\PartDataExcelStockInventoryController@destroyBatch')->middleware('permission:master_part_data_excel_stock_inventory.delete')->name('part-data-excel-stock-inventory.destroy_bacth.api');

    });


    // Engineer Code Excel
    Route::group(['prefix' => 'engineer-code-excel'], function () {
        Route::get('datatables', 'Api\Master\EngineerCodeExcelController@datatables')->middleware('permission:master_engineer_code_excel.show')->name('engineer-code-excel.datatables.api');
        Route::get('select2', 'Api\Master\EngineerCodeExcelController@select2')->name('engineer-code-excel.select2.api');
        Route::get('', 'Api\Master\EngineerCodeExcelController@index')->middleware('permission:master_engineer_code_excel.show')->name('engineer-code-excel.index.api');
        Route::post('', 'Api\Master\EngineerCodeExcelController@store')->middleware('permission:master_engineer_code_excel.create')->name('engineer-code-excel.store.api');
        Route::get('{ece}', 'Api\Master\EngineerCodeExcelController@show')->middleware('permission:master_engineer_code_excel.create')->name('engineer-code-excel.show.api');
        Route::put('{ece}', 'Api\Master\EngineerCodeExcelController@update')->middleware('permission:master_engineer_code_excel.update')->name('engineer-code-excel.update.api');
        Route::delete('destroy_batch', 'Api\Master\EngineerCodeExcelController@destroyBatch')->middleware('permission:master_engineer_code_excel.delete')->name('engineer-code-excel.destroy_bacth.api');
        Route::delete('{ece}', 'Api\Master\EngineerCodeExcelController@destroy')->middleware('permission:master_engineer_code_excel.delete')->name('engineer-code-excel.destroy.api');
    });

      // Status Excel
      Route::group(['prefix' => 'status-excel'], function () {
        Route::get('datatables', 'Api\Master\StatusExcelController@datatables')->middleware('permission:master_status_excel.show')->name('status-excel.datatables.api');
        Route::get('select2', 'Api\Master\StatusExcelController@select2')->name('status-excel.select2.api');
        Route::get('', 'Api\Master\StatusExcelController@index')->middleware('permission:master_status_excel.show')->name('status-excel.index.api');
        Route::post('', 'Api\Master\StatusExcelController@store')->middleware('permission:master_status_excel.create')->name('status-excel.store.api');
        Route::get('{status_excel}', 'Api\Master\StatusExcelController@show')->middleware('permission:master_status_excel.create')->name('status-excel.show.api');
        Route::put('{status_excel}', 'Api\Master\StatusExcelController@update')->middleware('permission:master_status_excel.update')->name('status-excel.update.api');
        Route::delete('destroy_batch', 'Api\Master\StatusExcelController@destroyBatch')->middleware('permission:master_status_excel.delete')->name('status-excel.destroy_bacth.api');
        Route::delete('{status_excel}', 'Api\Master\StatusExcelController@destroy')->middleware('permission:master_status_excel.delete')->name('status-excel.destroy.api');
    });

     // ASC Excel
     Route::group(['prefix' => 'asc-excel'], function () {
        Route::get('datatables', 'Api\Master\ASCExcelController@datatables')->middleware('permission:master_asc_excel.show')->name('asc-excel.datatables.api');
        Route::get('select2', 'Api\Master\ASCExcelController@select2')->name('asc-excel.select2.api');
        Route::get('select2p/{asc_excel}', 'Api\Master\ASCExcelController@select2p')->name('asc-excel.select2.api');
        Route::get('', 'Api\Master\ASCExcelController@index')->middleware('permission:master_asc_excel.show')->name('asc-excel.index.api');
        Route::post('', 'Api\Master\ASCExcelController@store')->middleware('permission:master_asc_excel.create')->name('asc-excel.store.api');
        Route::get('{asc_excel}', 'Api\Master\ASCExcelController@show')->middleware('permission:master_asc_excel.create')->name('asc-excel.show.api');
        Route::put('{asc_excel}', 'Api\Master\ASCExcelController@update')->middleware('permission:master_asc_excel.update')->name('asc-excel.update.api');
        Route::delete('destroy_batch', 'Api\Master\ASCExcelController@destroyBatch')->middleware('permission:master_asc_excel.delete')->name('asc-excel.destroy_bacth.api');
        Route::delete('{asc_excel}', 'Api\Master\ASCExcelController@destroy')->middleware('permission:master_asc_excel.delete')->name('asc-excel.destroy.api');
    });

    // Type Job Excel
    Route::group(['prefix' => 'type-job-excel'], function () {
        Route::get('datatables', 'Api\Master\TypeJobExcelController@datatables')->middleware('permission:master_type_job_excel.show')->name('type-job-excel.datatables.api');
        Route::get('select2', 'Api\Master\TypeJobExcelController@select2')->name('type-job-excel.select2.api');
        Route::get('', 'Api\Master\TypeJobExcelController@index')->middleware('permission:master_type_job_excel.show')->name('type-job-excel.index.api');
        Route::post('', 'Api\Master\TypeJobExcelController@store')->middleware('permission:master_type_job_excel.create')->name('type-job-excel.store.api');
        Route::get('{type_job_excel}', 'Api\Master\TypeJobExcelController@show')->middleware('permission:master_type_job_excel.create')->name('type-job-excel.show.api');
        Route::put('{type_job_excel}', 'Api\Master\TypeJobExcelController@update')->middleware('permission:master_type_job_excel.update')->name('type-job-excel.update.api');
        Route::delete('destroy_batch', 'Api\Master\TypeJobExcelController@destroyBatch')->middleware('permission:master_type_job_excel.delete')->name('type-job-excel.destroy_bacth.api');
        Route::delete('{type_job_excel}', 'Api\Master\TypeJobExcelController@destroy')->middleware('permission:master_type_job_excel.delete')->name('type-job-excel.destroy.api');
    });

    // General Journal
    Route::group(['prefix' => 'general-journal'], function () {
        Route::get('/datatables', 'Api\Master\GeneralJournalController@datatables')->middleware('permission:jurnal.show')->name('general-journal.datatables.api');
        Route::get('datatables/search', 'Api\Master\GeneralJournalController@datatablesSearch')->middleware('permission:jurnal.show')->name('general-journal.datatables_search.api');
        Route::get('/datatables_logs/{general_journal}/{type?}', 'Api\Master\GeneralJournalController@datatablesLogs')->middleware('permission:jurnal.show')->name('general-journal.datatables.api');
        Route::get('/', 'Api\Master\GeneralJournalController@index')->middleware('permission:jurnal.show')->name('general-journal.index.api');
        Route::post('/', 'Api\Master\GeneralJournalController@store')->middleware('permission:jurnal.create')->name('general-journal.store.api');
        Route::get('/{general_journal}', 'Api\Master\GeneralJournalController@show')->middleware('permission:jurnal.create')->name('general-journal.show.api');
        Route::post('/{general_journal}', 'Api\Master\GeneralJournalController@update')->middleware('permission:jurnal.update')->name('general-journal.update.api');
        Route::delete('/{general_journal}', 'Api\Master\GeneralJournalController@destroy')->middleware('permission:jurnal.delete')->name('general-journal.destroy.api');
        //Details Data
        Route::put('update_details_data/{general_journal}', 'Api\Master\GeneralJournalController@updateDetailsData');
        Route::delete('/destroy_details_data/{gjd_id}', 'Api\Master\GeneralJournalController@destroyDetailsData');

        //Get Price Hpp Average
        Route::get('/part/price_hpp_average_all/{pdes_id}/{out_stock}', 'Api\Master\GeneralJournalController@getHppAverageAll')->middleware('permission:orders_data_excel.show');
    });
    //HPP Average
    Route::get('/report-hpp-items/datatables', 'Api\Master\GeneralJournalController@hppItemDatatables')->middleware('permission:jurnal.show')->name('report-hpp-items.datatables.api');
    //HPP Average Per General Journal Details
    Route::get('/report-hpp-items/datatables/details/{pdes_id}/', 'Api\Master\GeneralJournalController@hppItemDatatablesDetails')->middleware('permission:jurnal.show')->name('report-hpp-items.datatables_details.api');
    //HPP Average Per General Journal Details Year
    Route::get('/report-hpp-items/datatables/details-yearly/', 'Api\Master\GeneralJournalController@hppItemDatatablesDetailsYearly')->middleware('permission:jurnal.show')->name('report-hpp-items.datatables_details_year.api');
     //Update Margin Rate Auto
     Route::post('/report-hpp-items/update/margin_rate_auto', 'Api\Master\GeneralJournalController@updateMarginRateAuto')->middleware('permission:jurnal.show')->name('report-hpp-items.update.margin_rate_auto.api');
    //Update Margin Rate Manual
    Route::post('/report-hpp-items/update/margin_rate_manual', 'Api\Master\GeneralJournalController@updateMarginRateManual')->middleware('permission:jurnal.show')->name('report-hpp-items.update.margin_rate_manual.api');
    //Update Margin HPP Average Manual MOnth
    Route::post('/report-hpp-items/update/hpp_average_manual_month', 'Api\Master\GeneralJournalController@updateHppAverageManualMonth')->middleware('permission:jurnal.show')->name('report-hpp-items.update.hpp_average_manual_month.api');
    //Update Margin HPP Average Manual Year
    Route::post('/report-hpp-items/update/hpp_average_manual_all', 'Api\Master\GeneralJournalController@updateHppAverageManualAll')->middleware('permission:jurnal.show')->name('report-hpp-items.update.hpp_average_manual_all.api');
    //Update Suggest To Sell Manual
    Route::post('/report-hpp-items/update/suggest_to_sell_manual', 'Api\Master\GeneralJournalController@updateSuggestToSellManual')->middleware('permission:jurnal.show')->name('report-hpp-items.update.suggest_to_sell_manual.api');
     //Update Margin Rate Manual Details
     Route::post('/report-hpp-items/details/update/margin_rate_manual', 'Api\Master\GeneralJournalController@updateMarginRateManualDetail')->middleware('permission:jurnal.show')->name('report-hpp-items.update.margin_rate_manual.api');
     //Update Suggest To Sell Manual Details
     Route::post('/report-hpp-items/details/update/margin_rate_manual', 'Api\Master\GeneralJournalController@updateMarginRateManualDetail')->middleware('permission:jurnal.show')->name('report-hpp-items.update.margin_rate_manual_detail.api');
     Route::post('/report-hpp-items/details/update/suggest_to_sell_manual', 'Api\Master\GeneralJournalController@updateSuggestToSellManualDetail')->middleware('permission:jurnal.show')->name('report-hpp-items.update.suggest_to_sell_manual_detail.api');


    // Excel Income
    Route::group(['prefix' => 'excel-income'], function () {

        Route::get('/datatables', 'Api\Master\ExcelIncomeController@datatables')->middleware('permission:excel-income.show')->name('excel-income.datatables.api');
        Route::get('datatables/search', 'Api\Master\ExcelIncomeController@datatablesSearch')->middleware('permission:jurnal.show')->name('general-journal.datatables_search.api');
        Route::get('/datatables_logs/{excel_income}/{type?}', 'Api\Master\ExcelIncomeController@datatablesLogs')->middleware('permission:excel-income.show')->name('excel-income.datatables.api');
        Route::get('/', 'Api\Master\ExcelIncomeController@index')->middleware('permission:excel-income.show')->name('excel-income.index.api');
        Route::post('/', 'Api\Master\ExcelIncomeController@store')->middleware('permission:excel-income.create')->name('excel-income.store.api');
        Route::get('/{excel_income}', 'Api\Master\ExcelIncomeController@show')->middleware('permission:excel-income.show')->name('excel-income.show.api');
        Route::post('/{excel_income}', 'Api\Master\ExcelIncomeController@update')->middleware('permission:excel-income.update')->name('excel-income.update.api');
        Route::delete('/{excel_income}', 'Api\Master\ExcelIncomeController@destroy')->middleware('permission:excel-income.delete')->name('excel-income.destroy.api');

        //Details Data
        Route::post('/add_detail/{excel_income}', 'Api\Master\ExcelIncomeController@createDetail');
        Route::post('/update_detail/{excel_income_detail}', 'Api\Master\ExcelIncomeController@updateDetail');
        Route::delete('/destroy_details_data/{excel_income}', 'Api\Master\ExcelIncomeController@destroyDetailsData');
        Route::delete('/detail_description/{desc_id}', 'Api\Master\ExcelIncomeController@destroyDetailsDesc');
    });

    // Symptom
    Route::group(['prefix' => 'symptom'], function () {
        Route::get('datatables', 'Api\Master\SymptomController@datatables')->middleware('permission:symptom.show')->name('symptom.datatables.api');
        Route::get('select2', 'Api\Master\SymptomController@select2')->name('symptom.select2.api');
        Route::get('', 'Api\Master\SymptomController@index')->middleware('permission:symptom.show')->name('symptom.index.api');
        Route::get('{id}', 'Api\Master\SymptomController@show');
        Route::get('relatable/{id}', 'Api\Master\SymptomController@showRelatable');
        Route::get('{id}/service_type', 'Api\Master\SymptomController@serviceType');
        Route::post('', 'Api\Master\SymptomController@store')->middleware('permission:symptom.create')->name('symptom.store.api');
        Route::post('{symptom}', 'Api\Master\SymptomController@update')->middleware('permission:symptom.update')->name('symptom.update.api');
        Route::delete('{symptom}', 'Api\Master\SymptomController@delete')->middleware('permission:symptom.delete')->name('symptom.destroy.api');
    });

    // Service Type
    Route::group(['prefix' => 'service_type'], function () {
        Route::get('datatables', 'Api\Master\ServiceTypeController@datatables')->middleware('permission:service_type.show')->name('service_type.datatables.api');
        Route::get('select2', 'Api\Master\ServiceTypeController@select2')->name('service_type.select2.api');
        Route::get('select2_symptom/{symptom_id}', 'Api\Master\ServiceTypeController@select2BySymptomId');
        Route::get('', 'Api\Master\ServiceTypeController@index')->middleware('permission:service_type.show')->name('service_type.index.api');
        Route::post('', 'Api\Master\ServiceTypeController@store')->middleware('permission:service_type.create')->name('service_type.store.api');
        Route::post('{service_type}', 'Api\Master\ServiceTypeController@update')->middleware('permission:service_type.update')->name('service_type.update.api');
        Route::delete('{service_type}', 'Api\Master\ServiceTypeController@delete')->middleware('permission:service_type.delete')->name('service_type.destroy.api');
    });

    // Technician Spareparts
    Route::group(['prefix' => 'technician_sparepart'], function () {
        Route::get('datatables', 'Api\Master\TechnicianSparepartController@datatables')->name('technician_sparepart.datatables.api');
        Route::get('select2/{user_id?}', 'Api\Master\TechnicianSparepartController@select2')->name('technician_sparepart.select2.api');
        Route::get('', 'Api\Master\TechnicianSparepartController@index')->name('technician_sparepart.index.api');
        Route::post('', 'Api\Master\TechnicianSparepartController@store')->name('technician_sparepart.store.api');
        Route::post('{technician_sparepart}', 'Api\Master\TechnicianSparepartController@update')->name('service_type.update.api');
        Route::delete('{technician_sparepart}', 'Api\Master\TechnicianSparepartController@destroy')->name('service_type.destroy.api');
    });


    // tickets
    Route::group(['prefix' => 'tickets'], function () {
        Route::get('datatables', 'Api\Master\TicketController@datatables')->middleware('permission:ticket.show');
        Route::post('', 'Api\Master\TicketController@store')->middleware('permission:ticket.create');
        Route::put('{ticket}', 'Api\Master\TicketController@update')->middleware('permission:ticket.update');
        Route::put('{ticket}/status', 'Api\Master\TicketController@updateStatus');
        Route::delete('{ticket}', 'Api\Master\TicketController@destroy')->middleware('permission:ticket.delete');
    });

    // grade
    Route::group(['prefix' => 'grades'], function () {
        Route::get('datatables', 'Api\Master\GradeController@datatables')->middleware('permission:ticket.show');
        Route::post('', 'Api\Master\GradeController@store')->middleware('permission:ticket.create');
        Route::put('{grade}', 'Api\Master\GradeController@update')->middleware('permission:ticket.update');
        Route::delete('{grade}', 'Api\Master\GradeController@destroy')->middleware('permission:ticket.delete');
    });

    // tat grup
    Route::group(['prefix' => 'tat_grup'], function () {
        Route::get('datatables', 'Api\Master\TATGrupController@datatables')->middleware('permission:ticket.show');
        Route::post('', 'Api\Master\TATGrupController@store')->middleware('permission:ticket.create');
        Route::put('{grup}', 'Api\Master\TATGrupController@update')->middleware('permission:ticket.update');
        Route::delete('{grup}', 'Api\Master\TATGrupController@destroy')->middleware('permission:ticket.delete');
    });

    // stymptom code
    Route::group(['prefix' => 'symptom-code'], function () {
        Route::get('datatables', 'Api\Master\SymptomCodeController@datatables')->middleware('permission:ticket.show');
        Route::get('select2', 'Api\Master\SymptomCodeController@select2');
        Route::post('', 'Api\Master\SymptomCodeController@store')->middleware('permission:ticket.create');
        Route::put('{symptomCode}', 'Api\Master\SymptomCodeController@update')->middleware('permission:ticket.update');
        Route::delete('{symptomCode}', 'Api\Master\SymptomCodeController@delete')->middleware('permission:ticket.delete');
    });

    //Repair Code
    Route::group(['prefix' => 'repair-code'], function () {
        Route::get('datatables', 'Api\Master\RepairCodeController@datatables')->middleware('permission:ticket.show');
        Route::get('select2', 'Api\Master\RepairCodeController@select2');
        Route::post('', 'Api\Master\RepairCodeController@store')->middleware('permission:ticket.create');
        Route::put('{repairCode}', 'Api\Master\RepairCodeController@update')->middleware('permission:ticket.update');
        Route::delete('{repairCode}', 'Api\Master\RepairCodeController@delete')->middleware('permission:ticket.delete');
    });
    // ProductGroupController
    Route::group(['prefix' => 'product_groups'], function () {
        Route::get('datatables', 'Api\Master\ProductGroupController@datatables')->middleware('permission:ticket.show');
        Route::get('select2/{type_id?}', 'Api\Master\ProductGroupController@select2');
        Route::post('', 'Api\Master\ProductGroupController@store')->middleware('permission:ticket.create');
        Route::post('{product}', 'Api\Master\ProductGroupController@update')->middleware('permission:ticket.update');
        Route::delete('{product}', 'Api\Master\ProductGroupController@destroy')->middleware('permission:ticket.delete');
    });

    // EmailStatusOrder
    Route::group(['prefix' => 'email-status-order'], function () {
        Route::get('datatables', 'Api\Master\EmailTemplateController@datatables')->middleware('permission:email_template.show');
        Route::get('select2/{type_id?}', 'Api\Master\EmailTemplateController@select2');
        Route::post('', 'Api\Master\EmailTemplateController@store')->middleware('permission:email_template.create');
        Route::put('{email}', 'Api\Master\EmailTemplateController@update')->middleware('permission:email_template.update');
    });

    // Email Other
    Route::group(['prefix' => 'email-other'], function () {
        Route::get('datatables', 'Api\Master\EmailTemplateController@datatablesEmailOther')->middleware('permission:email_template.show');
        Route::put('{email}', 'Api\Master\EmailTemplateController@updateEmailOther')->middleware('permission:email_template.update');
    });

    // company
    Route::group(['prefix' => 'company'], function () {
        Route::get('datatables', 'Api\CompanyController@datatables');
        Route::get('datatables-outlet-by-company/{id}', 'Api\CompanyController@datatablesOutletByCompany');
        Route::get('select2', 'Api\CompanyController@select2');
        Route::post('', 'Api\CompanyController@store');
        Route::put('{company}', 'Api\CompanyController@update');
        Route::delete('{company}', 'Api\CompanyController@destroy');
    });

    // btb
    Route::group(['prefix' => 'business_to_business'], function () {
        Route::get('/', 'Api\BusinessToBusinessController@bootstrapTable')->middleware('permission:btb.show');
        Route::get('/cek-berita-acara/{id}', 'Api\BusinessToBusinessController@beritaAcara');
        Route::get('/log/{btb?}', 'Api\BusinessToBusinessController@datatablesLog')->middleware('permission:btb.show');
        Route::get('/{id}/get_available_unit', 'Api\BusinessToBusinessController@getUnitAvailable');
        Route::post('/', 'Api\BusinessToBusinessController@store')->middleware('permission:btb.create');
        Route::get('/{btb_id}/outlet', 'Api\BusinessToBusinessController@bootstrapTableGetOutlet')->middleware('permission:btb.show');
        Route::get('/{outlet_id}/outlet_detail', 'Api\BusinessToBusinessController@bootstrapTableGetOutletDetail')->middleware('permission:btb.show');
        Route::get('/{outlet_id}/outlet_detail_add_parent_table', 'Api\BusinessToBusinessController@bootstrapTableGetOutletDetailAddParent')->middleware('permission:btb.show');
        Route::put('/{btb}', 'Api\BusinessToBusinessController@update')->middleware('permission:btb.update');
        Route::delete('/{btb}', 'Api\BusinessToBusinessController@destroy')->middleware('permission:btb.delete');

        Route::post('/outlet', 'Api\BusinessToBusinessController@storeOutlet')->middleware('permission:btb.create');
        Route::put('/{headerOurlet}/outlet', 'Api\BusinessToBusinessController@updateOutlet')->middleware('permission:btb.update');
        Route::delete('/{headerOurlet}/outlet', 'Api\BusinessToBusinessController@destroyOutlet')->middleware('permission:btb.delete');

        Route::post('/outlet_detail', 'Api\BusinessToBusinessController@storeOutletDetail')->middleware('permission:btb.create');
        Route::post('/outlet_detail_add_parent', 'Api\BusinessToBusinessController@storeOutletDetailAddParent')->middleware('permission:btb.create');
        Route::post('/{detailOutlet}/outlet_detail', 'Api\BusinessToBusinessController@updateOutletDetail')->middleware('permission:btb.update');
        Route::post('/{detailOutlet}/change_status', 'Api\BusinessToBusinessController@changeStatus')->middleware('permission:btb.update');
        Route::post('/{detailOutlet}/parent_outlet', 'Api\BusinessToBusinessController@parentOutletUpdate')->middleware('permission:btb.update');
        Route::delete('/{detailOutlet}/outlet_detail', 'Api\BusinessToBusinessController@destroyOutletDetail')->middleware('permission:btb.delete');
    });


    // Product Search
    Route::group(['prefix' => 'product_search'], function () {
        Route::get('/find_varian/{id}', 'Api\Product\ProductSearchController@findVarian');
        Route::get('/find_vendor/{id}', 'Api\Product\ProductSearchController@findVendor');
        //  search vendors by product id
        Route::get('/vendors/{product_id}', 'Api\Product\ProductSearchController@getVendorList');
        //  search varian by product id and vendor id
        Route::get('/varians/{product_id}/{vendor_id}', 'Api\Product\ProductSearchController@getVendorVarianList');
        //  search
        Route::post('/varian', 'Api\Product\ProductSearchController@getVarian');
    });

    // Customer Request Job
    Route::group(['prefix' => 'customer'], function () {
        // ticket
        Route::group(['prefix' => 'tickets'], function () {
            Route::get('/datatables', 'Customer\Api\TicketController@datatables');
            Route::post('/', 'Customer\Api\TicketController@store');
            Route::post('/reply', 'Customer\Api\TicketController@reply');
        });

        Route::group(['prefix' => 'transaction-history'], function () {
            Route::get('/datatables', 'Customer\Api\OrderController@historyTransactionDatatables');
            Route::get('/detail/datatables/{order_id}', 'Customer\Api\OrderController@historyTransactionDetailDatatables');
        });

        // garansi
        Route::group(['prefix' => 'garansi'], function () {
            Route::get('/datatables', 'Customer\Api\GaransiController@datatables');
            Route::get('/list_chat/{garansi_id}', 'Customer\Api\GaransiController@listChat');
            Route::post('/', 'Customer\Api\GaransiController@store');
            Route::post('/reply', 'Customer\Api\GaransiController@reply');
        });

        // review
        Route::group(['prefix' => 'reviews'], function () {
            Route::post('/', 'Customer\Api\ReviewController@store');
        });

        // profile
        Route::group(['prefix' => 'profile'], function () {
            Route::get('/address_list/datatables', 'Customer\Api\ProfileController@addressList');

            Route::post('/image', 'Customer\Api\ProfileController@updateProfileImage');
            Route::post('/address', 'Customer\Api\ProfileController@profileCreateAddress');

            Route::put('/info/update', 'Customer\Api\ProfileController@profileUpdateInfo');
            Route::put('/address/{id}', 'Customer\Api\ProfileController@profileUpdateAddress');
            Route::put('/address/main/{id}', 'Customer\Api\ProfileController@profileMainAddress');

            Route::delete('/address/{id}', 'Customer\Api\ProfileController@profileDeleteAddress');
        });

        Route::post('/request-job/save', 'CustomerController@saveRequestJob');
        Route::get('/request-job/list-technician', 'CustomerController@getListTechnicians');
        Route::post('/request-job/hours-available', 'CustomerController@getHoursAvailable');
        Route::post('/submit-penawaran', 'CustomerController@submitPenawaran');
        Route::post('/request-job/datatables', 'CustomerController@datatables');
        Route::post('/checkout/{order_id}', 'CustomerController@checkout'); // checkout
        Route::get('/request-job/datatablesRequestJob/{order_id}', 'CustomerController@datatables');
        Route::post('/cancel-jobs/{id}', 'CustomerController@cancelOrder')->name('order.cancel');
        Route::post('/cancel-jobs-is-approve/{id}', 'CustomerController@cancelOrderIsApprove')->name('order.cancel');
        Route::post('/durasi/{id}', 'CustomerController@infoDurasi')->name('order.cancel');
        Route::post('/get_last_order', 'CustomerController@infoLastTopup')->name('order.cancel');
        Route::post('/accept-order/{id}', 'CustomerController@acceptOrder')->name('accept.order');
        Route::get('/less-balance/show/{orders}', 'CustomerController@showDataLessBalance');
        Route::get('/change_additional_parts/show/{tmp_parts}', 'CustomerController@showChangeAdditionalParts');

        // user update status order
        Route::post('/complaint/{order_id}', 'CustomerController@complaint');
        Route::post('/approve/{order_id}', 'CustomerController@approve');
        Route::post('/complaint/close/{order_id}', 'CustomerController@complaintClose');

        // user list transaction complain
        Route::get('/complaint-transaction/datatables', 'CustomerController@datatablesComplaint');
        Route::post('/complaint-transacyion/save', 'CustomerController@saveOrderComplaint');
        Route::post('/request-ticket/reply/{order_id}', 'CustomerController@replyOrderComplaint');

        //done jobs
        Route::post('/done-jobs/{order_id}', 'CustomerController@customerDoneJobs');

        // customer topup
        Route::get('/topup-wallet/datatables', 'Api\Master\TopupWalletController@datatables');
        Route::post('/topup-wallet/save', 'Api\Master\TopupWalletController@store');
        Route::post('/topup-wallet/less-balance-saved', 'Api\Master\TopupWalletController@saved');
        Route::post('/topup-wallet/checkout/{id}', 'Api\Master\TopupWalletController@checkoutTopupWallet');
        Route::post('/topup-wallet/checkout/midtrans/{id}', 'Api\Master\TopupWalletController@checkoutWithMidtrans');
        Route::post('/topup-wallet/confirmationTopup/{id}', 'Api\Master\TopupWalletController@confirmationTopupWallet');
        Route::post('/reedem/voucer/{id}', 'Api\Master\TopupWalletController@reedemVoucer');
    });

    // Customer confrim Payment
    Route::group(['prefix' => 'confrim'], function () {
        Route::post('', 'Api\Master\ConfrimPaymentController@store');
    });


    Route::group(['prefix' => 'teknisi'], function () {
        // balance teknisi
        Route::get('/list-garansi-job', 'TeknisiController@garansiIndexDatatables');
        Route::get('/balance/detail/datatables', 'TeknisiController@datatablesListBalance');

        // Teknisi Requst Job
        Route::put('/update_sparepart_detail_before_accept/{order_id}', 'Api\OrderController@updateSparepartDetailBeforeApprove');
        Route::delete('destroy_sparepart_detail/{sparepart_detail_id}', 'Api\OrderController@destroySparepartDetailByTeknisi');
        Route::delete('destroy_tmp_sparepart_detail/{sparepart_detail_id}', 'Api\OrderController@destroyTmpSparepartDetailByTeknisi');
        Route::delete('destroy_tmp_item_detail/{sparepart_detail_id}', 'Api\OrderController@destroyTmpItemDetailByTeknisi');
        Route::get('/review-list', 'TeknisiController@reviewListDatatables');
        Route::get('/request-job/datatables', 'TeknisiController@datatables');
        Route::get('/complaint-list/datatables', 'TeknisiController@complaintListDatatables');
        Route::get('/request-job/datatables/all', 'TeknisiController@datatablesListJob');
        Route::post('/accepted-job/{order_id}', 'TeknisiController@acceptedJobSave');
        Route::post('/reject-job/{order_id}', 'TeknisiController@rejectJob');
        Route::get('/category/select2', 'Api\Master\ServicesController@select2');
        Route::get('/symptom/select2/{ms_services_id?}', 'Api\Master\SymptomController@select2');
        Route::post('/jobs-done/{order_id}', 'TeknisiController@jobsDone');
        Route::post('/on-working/{order_id}', 'TeknisiController@onWorking');

        //Teknisi Teams
        Route::get('/teams/datatables', 'TeknisiController@teamsDatatables');
        Route::get('/teams/member/datatables', 'TeknisiController@memberDatatables');
        Route::post('/teams/create', 'TeknisiController@teamsCreate');
        Route::post('/teams/update/{team_id}', 'TeknisiController@teamsUpdate');
        Route::delete('/teams/delete/{team_id}', 'TeknisiController@teamsDelete');
        Route::get('/teams/list', 'TeknisiController@teamsList');
        Route::get('/teams/show', 'TeknisiController@teamsShow');
        Route::post('/teams/editMember', 'TeknisiController@teamEditMember');
        Route::get('/teams/member/select2', 'TeknisiController@memberSelect2');
        Route::delete('/teams/leave/{team_id}', 'TeknisiController@teamsLeave');
        Route::get('/teams/invitation/datatables', 'TeknisiController@invitationDatatables');
        Route::post('/teams/approve-invitation', 'TeknisiController@approveInvitation');
    });

    // Admin All doing with job
    Route::group(['prefix' => 'admin'], function () {
        Route::post('/order/datatables', 'Admin\MasterOrderController@datatables');
        Route::post('/all-transfer/datatables/{id}', 'Admin\MasterOrderController@allTransferdatatables');
        Route::post('/order/update_status', 'Admin\MasterOrderController@updateOrderStatus');
        Route::post('/order/update_detail/{type_update}', 'Admin\MasterOrderController@updateOrderDetail');
        Route::post('/order/part_update', 'Admin\MasterOrderController@updatePart');
        Route::post('/order/find_technicians', 'Admin\MasterOrderController@find_technicians');
        Route::post('/order/create_order/save', 'Admin\MasterOrderController@AdminCreateOrderSave');
        Route::post('/order/display_price_services', 'Admin\MasterOrderController@displayPriceService');
        Route::get('/category/select2', 'Api\Master\ServicesController@select2');
        Route::get('/symptom/select2/{ms_services_id?}', 'Api\Master\SymptomController@select2');
        Route::get('/order/delete/{order_id}', 'Admin\MasterOrderController@softDeletes');
        Route::get('/order/trash/datatables', 'Admin\MasterOrderController@trashDatatables');
        Route::get('/order/undo/{order_id}', 'Admin\MasterOrderController@undo');
        Route::get('/order/undo_all', 'Admin\MasterOrderController@undoAll');
        Route::get('/order/delete_trash/{order_id}', 'Admin\MasterOrderController@deletedTrash');
        Route::get('/order/delete_trash_all', 'Admin\MasterOrderController@deletedTrashAll');
        Route::get('/complaint-transaction/datatables', 'Admin\MasterOrderController@datatablesComplaint');
        Route::post('/update/status-transfer', 'Admin\MasterOrderController@statusUpdate');
        Route::post('/update/status-transfer_balance', 'Admin\MasterOrderController@statusUpdateBalance');
        Route::post('/update/transfer/{order_id}', 'Admin\MasterOrderController@successTransfer');
        Route::post('/update/transfer/balance/{order_id}', 'Admin\MasterOrderController@successTransferBalance');
        Route::post('/complaint/close/{order_id}', 'CustomerController@complaintClose');
        Route::post('/websetting/create', 'Admin\WebSettingController@create');

        // galery image upload
        Route::post('/galery/upload', 'Admin\MasterOrderController@galeryUpload');
        Route::post('/galery/upload-banner', 'Admin\MasterBannerController@upload');
        Route::post('/galery/delete-banner/{id}', 'Admin\MasterBannerController@destroy');

        // admin topup datatables
        Route::get('/topup-wallet/datatables', 'Api\Master\TopupWalletController@adminListWallet');
        Route::get('/topup-wallet/datatables-admin/{id}', 'Api\Master\TopupWalletController@adminWalletListDatatables');
        Route::post('/approved-topup/{id}', 'Api\Master\TopupWalletController@approveTopupWallet');
        Route::post('/topup-wallet/save', 'Api\Master\TopupWalletController@adminStore');



        Route::post('/request_job_json', 'CustomerController@job_request_create_json');
        Route::get('/symptom/select2/{ms_services_id?}', 'Api\Master\SymptomController@select2');

        //vouchers general
        Route::get('/voucher-general/datatables', 'Api\Master\VoucherController@datatables');
        Route::post('/voucher-general/create', 'Api\Master\VoucherController@store');
        Route::post('/voucher-general/update/{voucherGeneral}', 'Api\Master\VoucherController@update');
        Route::delete('/voucher-general/delete/{voucherGeneral}', 'Api\Master\VoucherController@destroy');

        //new user voucer
        Route::post('/new-user-voucher/create', 'Api\Master\VoucherController@createVoucherForNewUsers');

        //Report
        Route::get('/report/datatables_customer/search', 'Api\Master\ReportController@datatables_customer_search');
        Route::get('/report/datatables_technician/search', 'Api\Master\ReportController@datatables_technician_search');
        Route::get('/report/datatables_order_service/search', 'Api\Master\ReportController@datatables_order_service_search');

        Route::get('/report/datatables_customer', 'Api\Master\ReportController@datatables_customer');
        Route::get('/report/datatables_technician', 'Api\Master\ReportController@datatables_technician');
    });

    Route::group(['prefix' => 'business-to-business-master-brand'], function () {
        Route::get('/datatables', 'Admin\B2BMasterBrandController@datatables');
        Route::post('/store', 'Admin\B2BMasterBrandController@store');
        Route::put('/update/{id}', 'Admin\B2BMasterBrandController@update');
        Route::delete('/destroy/{id}', 'Admin\B2BMasterBrandController@destroy');
    });

    Route::group(['prefix' => 'business-to-business-master-type'], function () {
        Route::get('/datatables', 'Admin\B2BMasterTypeController@datatables');
        Route::post('/store', 'Admin\B2BMasterTypeController@store');
        Route::put('/update/{id}', 'Admin\B2BMasterTypeController@update');
        Route::delete('/destroy/{id}', 'Admin\B2BMasterTypeController@destroy');
    });

    Route::group(['prefix' => 'business-to-business-settings'], function () {
        Route::post('/store', 'Admin\B2bGeneralSettingController@store');
    });

    Route::group(['prefix' => 'business-to-business-master-teknisi'], function () {
        Route::get('/datatables', 'Admin\B2BMasterTeknisiController@datatables');
        Route::delete('/delete/{id}', 'Admin\B2BMasterTeknisiController@delete');
    });

    // AUTO COMPLETE

    //product detail information autocomplete fill
    Route::group(['prefix' => 'pdi'], function () {
        Route::post('/name', 'AdminController@autoProductName');
    });

    //orders data excel autocomplete fill
    Route::group(['prefix' => 'ode'], function () {
        Route::post('/autoCompleteField', 'Api\Master\OrdersDataExcelController@autoCompleteField');
    });


    Route::post('/teknisi/inventory-company/select2', 'TeknisiController@inventoryCompanySelect2');
});

// ==========================
// api mobile
// =============================================
require base_path('routes/api/mobile_api.php');
