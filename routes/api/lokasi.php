<?php


use Illuminate\Support\Facades\Route;

//Country
Route::group(['prefix' => 'country'], function () {
    Route::get('datatables', 'Api\Master\CountryController@datatables')->middleware('permission:country.show')->name('countries.datatables.api');
    Route::get('select2', 'Api\Master\CountryController@select2')->middleware('permission:country.show')->name('countries.select2.api');
    Route::get('', 'Api\Master\CountryController@index')->middleware('permission:country.show')->name('countries.index.api');
    Route::post('', 'Api\Master\CountryController@store')->middleware('permission:country.create')->name('countries.store.api');
    Route::post('importExcel', 'Api\Master\CountryController@importExcel')->middleware('permission:country.create')->name('countries.import.api');
    Route::get('{country}', 'Api\Master\CountryController@show')->middleware('permission:country.create')->name('countries.show.api');
    Route::put('{country}', 'Api\Master\CountryController@update')->middleware('permission:country.update')->name('countries.update.api');
    Route::delete('destroy_batch', 'Api\Master\CountryController@destroyBatch')->middleware('permission:country.delete')->name('countries.destroy_bacth.api');
    Route::delete('{country}', 'Api\Master\CountryController@destroy')->middleware('permission:country.delete')->name('countries.destroy.api');
});
//Province
Route::group(['prefix' => 'province'], function () {
    Route::get('datatables', 'Api\Master\ProvinceController@datatables')->middleware('permission:province.show')->name('provinces.datatables.api');
    Route::get('select2', 'Api\Master\ProvinceController@select2')->middleware('permission:province.show')->name('provinces.select2.api');
    Route::get('', 'Api\Master\ProvinceController@index')->middleware('permission:province.show')->name('provinces.index.api');
    Route::post('', 'Api\Master\ProvinceController@store')->middleware('permission:province.create')->name('provinces.store.api');
    Route::post('importExcel', 'Api\Master\ProvinceController@importExcel')->middleware('permission:province.create')->name('countries.import.api');
    Route::get('{province}', 'Api\Master\ProvinceController@show')->middleware('permission:province.create')->name('provinces.show.api')->name('provinces.show.api');
    Route::put('{province}', 'Api\Master\ProvinceController@update')->middleware('permission:province.update')->name('provinces.update.api');
    Route::delete('destroy_multiple', 'Api\Master\ProvinceController@deleteByChecked')->middleware('permission:province.delete')->name('provinces.destroy_multiple.api');
    Route::delete('{province}', 'Api\Master\ProvinceController@destroy')->middleware('permission:province.delete')->name('provinces.destroy.api');
});
//City
Route::group(['prefix' => 'city'], function () {
    Route::get('datatables', 'Api\Master\CityController@datatables')->middleware('permission:city.show')->name('cities.datatables.api');
    Route::get('select2', 'Api\Master\CityController@select2')->middleware('permission:city.show')->name('cities.select2.api');
    Route::get('', 'Api\Master\CityController@index')->middleware('permission:city.show')->name('cities.index.api');
    Route::post('', 'Api\Master\CityController@store')->middleware('permission:city.create')->name('cities.store.api');
    Route::post('importExcel', 'Api\Master\CityController@importExcel')->middleware('permission:province.create')->name('cities.import.api');
    Route::get('{city}', 'Api\Master\CityController@show')->middleware('permission:city.create')->name('cities.show.api');
    Route::put('{city}', 'Api\Master\CityController@update')->middleware('permission:city.update')->name('cities.update.api');
    Route::delete('destroy_batch', 'Api\Master\CityController@destroyBatch')->middleware('permission:city.delete')->name('cities.destroy_bacth.api');
    Route::delete('{city}', 'Api\Master\CityController@destroy')->middleware('permission:city.delete')->name('cities.destroy.api');
});
//District
Route::group(['prefix' => 'district'], function () {
    Route::get('datatables', 'Api\Master\DistrictController@datatables')->middleware('permission:district.show')->name('districts.datatables.api');
    Route::get('select2', 'Api\Master\DistrictController@select2')->middleware('permission:district.show')->name('districts.select2.api');
    Route::get('', 'Api\Master\DistrictController@index')->middleware('permission:district.show')->name('districts.index.api');
    Route::post('', 'Api\Master\DistrictController@store')->middleware('permission:district.create')->name('districts.store.api');
    Route::post('importExcel', 'Api\Master\DistrictController@importExcel')->middleware('permission:district.create')->name('district.import.api');
    Route::get('{district}', 'Api\Master\DistrictController@show')->middleware('permission:district.create')->name('districts.show.api');
    Route::put('{district}', 'Api\Master\DistrictController@update')->middleware('permission:district.update')->name('districts.update.api');
    Route::delete('destroy_batch', 'Api\Master\DistrictController@destroyBatch')->middleware('permission:district.delete')->name('districts.destroy_bacth.api');
    Route::delete('{district}', 'Api\Master\DistrictController@destroy')->middleware('permission:district.delete')->name('districts.destroy.api');
});
//Village
Route::group(['prefix' => 'village'], function () {
    Route::get('datatables', 'Api\Master\VillageController@datatables')->middleware('permission:village.show')->name('villages.datatables.api');
    Route::get('select2', 'Api\Master\VillageController@select2')->middleware('permission:village.show')->name('villages.select2.api');
    Route::get('', 'Api\Master\VillageController@index')->middleware('permission:village.show')->name('villages.index.api');
    Route::post('', 'Api\Master\VillageController@store')->middleware('permission:village.create')->name('villages.store.api');
    Route::post('importExcel', 'Api\Master\VillageController@importExcel')->middleware('permission:village.create')->name('village.import.api');
    Route::get('{village}', 'Api\Master\VillageController@show')->middleware('permission:village.create')->name('villages.show.api');
    Route::put('{village}', 'Api\Master\VillageController@update')->middleware('permission:village.update')->name('villages.update.api');
    Route::delete('destroy_batch', 'Api\Master\VillageController@destroyBatch')->middleware('permission:village.delete')->name('villages.destroy_bacth.api');
    Route::delete('{village}', 'Api\Master\VillageController@destroy')->middleware('permission:village.delete')->name('villages.destroy.api');
});
//Zipcode
Route::group(['prefix' => 'zipcode'], function () {
    Route::get('/datatables', 'Api\Master\ZipcodeController@datatables')->middleware('permission:zipcode.show')->name('zipcode.datatables.api');
    Route::get('/select2', 'Api\Master\ZipcodeController@select2')->middleware('permission:zipcode.show')->name('zipcode.select2.api');
    Route::get('/', 'Api\Master\ZipcodeController@index')->middleware('permission:zipcode.show')->name('zipcode.index.api');
    Route::post('/', 'Api\Master\ZipcodeController@store')->middleware('permission:zipcode.create')->name('zipcode.store.api');
    Route::post('/importExcel', 'Api\Master\ZipcodeController@importExcel')->middleware('permission:zipcode.create')->name('zipcode.import.api');
    Route::get('/{zipcode}', 'Api\Master\ZipcodeController@show')->middleware('permission:zipcode.create')->name('zipcode.show.api');
    Route::put('/{zipcode}', 'Api\Master\ZipcodeController@update')->middleware('permission:zipcode.update')->name('zipcode.update.api');
    Route::delete('/destroy_batch', 'Api\Master\ZipcodeController@destroyBatch')->middleware('permission:zipcode.delete')->name('zipcode.destroy_bacth.api');
    Route::delete('/{zipcode}', 'Api\Master\ZipcodeController@destroy')->middleware('permission:zipcode.delete')->name('zipcode.destroy.api');
});
