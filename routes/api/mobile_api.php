<?php

// Customer Request Job

use Illuminate\Support\Facades\Route;

Route::post('/mobile/auth/login', 'Api\AuthController@login');
Route::post('/mobile/auth/register', 'Api\AuthController@register');
Route::post('/mobile/auth/social-media', 'Api\AuthController@bypassLoginSocialMedia');

Route::group(['prefix' => 'mobile'], function () {
    Route::get('/config', 'Api\Master\GeneralSettingController@globalPublicSetting');
    

    // Route::group(['prefix' => 'business'], function () {
    //     Route::get('/show-list', 'Api\BusinessToBusinessController@apiMobileList');
    //     Route::post('/search', 'Api\BusinessToBusinessController@apiMobileSearch');
    //     Route::post('/store', 'Api\BusinessToBusinessController@apiMobileStore');
    // });

    Route::group(['prefix' => 'master-list'], function () {
        Route::get('/banks', 'Api\Master\BankTransferController@index');
        Route::get('/pay-method', 'Api\Master\BankTransferController@indexType');
        Route::get('/repair-code', 'Api\Master\RepairCodeController@index');
        Route::get('/symptom-code', 'Api\Master\SymptomCodeController@index');
        Route::get('/banners', 'Admin\MasterBannerController@getListApi');
        Route::get('/cities', 'Api\Master\CityController@index');
        Route::get('/maritals', 'Api\Master\MaritalController@index');
        Route::get('/religions', 'Api\Master\ReligionController@index');
        Route::get('/services', 'Api\Master\ServicesController@index');
        Route::get('/service-categories', 'Api\Master\SymptomController@index');
        Route::get('/service-types', 'Api\Master\ServiceTypeController@index');
        Route::get('/product-groups', 'Api\Master\ProductGroupController@index');
        Route::get('/address-types', 'Api\Master\AddressTypeController@index');
        Route::get('/warehouses', 'Api\Master\WarehouseController@index');
        Route::get('/inventory-company', 'TeknisiController@inventoryCompanySelect2Mobile');
        Route::get('/order-status', 'Customer\Api\OrderController@getOrderStatus');
    });
});

Route::group(['prefix' => 'mobile', 'middleware' => ['auth:api', 'user.active', 'verified']], function () {

    Route::group(['prefix' => 'business'], function () {
        Route::get('/show-list', 'Api\BusinessToBusinessController@apiMobileList');
        Route::get('/{id}/show-detail', 'Api\BusinessToBusinessController@apiMobileShowDetail');
        Route::get('/get-statuses', 'Api\BusinessToBusinessController@apiGetStatus');
        Route::get('/show-log/{btb?}', 'Api\BusinessToBusinessController@apiMobileShowLog');
        Route::post('/search', 'Api\BusinessToBusinessController@apiMobileSearch');
        Route::post('/store', 'Api\BusinessToBusinessController@apiMobileStore');
        Route::post('/store-outlet', 'Api\BusinessToBusinessController@apiMobileStoreOutlet');
        Route::post('/edit-outlet/{headerOurlet}', 'Api\BusinessToBusinessController@apiMobileEditOutlet');
        Route::get('/show-data-outlet/{headerOurlet}', 'Api\BusinessToBusinessController@apiMobileShowDataEditOutlet');
        Route::post('/delete-outlet/{headerOurlet}', 'Api\BusinessToBusinessController@apiMobileDestroyOutlet');
        Route::post('/store-outlet-detail', 'Api\BusinessToBusinessController@apiMobileStoreDetailOutlet');
        Route::post('/edit-outlet-detail/{detailOutlet}', 'Api\BusinessToBusinessController@apiMobileEditDetailOutlet');
        Route::post('/delete-outlet-detail/{detailOutlet}', 'Api\BusinessToBusinessController@apiMobileDeleteDetailOutlet');
        Route::get('/show-data-outlet-detail/{detailOutlet}', 'Api\BusinessToBusinessController@apiMobileShowDetailOutlet');
    });

    Route::get('/vouchers', 'Api\Master\VoucherController@listForCustomer');

    Route::post('/firebase_save_token', 'Customer\Api\ChatController@firebaseSaveToken');
    Route::post('/firebase_send_push', 'NotifikasiController@firebaseSendMessage');

    Route::group(['prefix' => 'notifications'], function () {
        Route::get('/', 'NotifikasiController@APIIndex');
        Route::get('/read', 'NotifikasiController@APIListRead');
        Route::get('/unread', 'NotifikasiController@APIListUnread');
        Route::post('/mark-as-read', 'NotifikasiController@APIMarkAsRead');
    });

    Route::post('/available-technician-schedule', 'Customer\Api\OrderController@getHoursAvailable');

    Route::post('/submit-application', 'Api\Master\TeknisiInfoController@create');
    Route::post('/teknisi-submit-application', 'Api\Master\TeknisiInfoController@updateDataTechnician');
    Route::get('/teknisi-submit-application-info', 'Api\Master\TeknisiInfoController@submitAppInfo');

    // wallet
    Route::group(['prefix' => 'wallet'], function () {
        // info
        Route::get('/', 'Customer\Api\ProfileController@walletInfo');
        // top up list
        Route::get('/topup', 'Customer\Api\ProfileController@walletTopupHistoryList');
        // top up save
        Route::post('/topup', 'Api\Master\TopupWalletController@store');
        Route::post('/topup-bank-transfer/{id}', 'Api\Master\TopupWalletController@checkoutTopupWallet');
        // vaoucer code
        Route::post('/voucer/{id}', 'Api\Master\TopupWalletController@reedemVoucer');
        // detail top up
        Route::post('/detail/{id}', 'Api\Master\TopupWalletController@checkoutWithMidtrans');
        Route::get('/detail-bank-transfer-confirmation/{id}', 'Api\Master\TopupWalletController@topupConfirmationDetail');

        // bank tranfere save
        Route::post('/confirmation-bank-transfer/{id}', 'Api\Master\TopupWalletController@confirmationTopupWallet');
    });

    // chats
    Route::group(['prefix' => 'chats'], function () {
        // list users dalaamna chat
        Route::get('/list', 'Customer\Api\ChatController@lists');
        // list users dalaamna chat
        Route::get('/{chat_id}', 'Customer\Api\ChatController@detailJson');
        // balas chat
        Route::post('/reply', 'Customer\Api\ChatController@reply');
        // cari user dalam chat
        Route::post('/search', 'Customer\Api\ChatController@searchUserInMyChat');
    });

    // address
    Route::group(['prefix' => 'addresses'], function () {
        // list
        Route::get('/', 'Customer\Api\ProfileController@addressIndex');
        Route::get('show/{id}', 'Customer\Api\ProfileController@profileGetOneAddress');
        // create
        Route::post('/', 'Customer\Api\ProfileController@profileCreateAddress');
        // set main address
        Route::put('/{id}/main', 'Customer\Api\ProfileController@profileMainAddress');
        // update address
        Route::put('/{id}', 'Customer\Api\ProfileController@profileUpdateAddress');
        // delete alamat
        Route::delete('/{id}', 'Customer\Api\ProfileController@profileDeleteAddress');
    });

    // profile
    Route::group(['prefix' => 'profile'], function () {
        // view profile
        Route::get('/', 'Customer\Api\ProfileController@index');
        // view status
        Route::get('/status', 'Customer\Api\ProfileController@statusInfo');
        // change image profile
        Route::post('/update-image', 'Customer\Api\ProfileController@updateProfileImage');
        // update info
        Route::put('/update-info', 'Customer\Api\ProfileController@profileUpdateInfo');
    });

    Route::group(['prefix' => 'customer', 'middleware' => ['role:Customer']], function () {
        // orders
        Route::group(['prefix' => 'orders'], function () {
            // static total order
            Route::get('/statistic', 'Customer\Api\OrderController@statistic');
            // list order
            Route::get('/', 'Customer\Api\OrderController@index');
            // search order
            Route::post('/search', 'Customer\Api\OrderController@search');
            // detail order
            Route::get('/{order_id}/show', 'Customer\Api\OrderController@show');
            // save 
            Route::post('/', 'Customer\Api\OrderController@store');
            // info durasi
            Route::get('/{id}/durasi/', 'CustomerController@infoDurasi');
            // available 
            Route::post('/list-technicians', 'Customer\Api\OrderController@listTeknisi');
            // terima job
            Route::put('/{id}/accept-job', 'CustomerController@acceptOrder');
            // reject job
            Route::put('/{id}/cancel-job', 'CustomerController@cancelOrder');
            Route::post('/{id}/cancel-job-is-approve', 'CustomerController@cancelOrderIsApprove');
            // complete job
            Route::put('/{order_id}/complete-job', 'CustomerController@approve');
        });

        // garansi
        Route::group(['prefix' => 'warranty'], function () {
            // available garansi
            Route::get('/list-available', 'Customer\Api\GaransiController@listAvailable');
            // pending atau proses garansi
            Route::get('/list-pending', 'Customer\Api\GaransiController@listPending');
            // reject
            Route::get('/list-reject', 'Customer\Api\GaransiController@listReject');
            // done garansi
            Route::get('/list-done', 'Customer\Api\GaransiController@listDone');
            // request claim garansi
            Route::post('/', 'Customer\Api\GaransiController@store');
            // isi chat garansi
            Route::get('/{garansi_id}/chat', 'Customer\Api\GaransiController@listChatJson');
            // detail garansi
            Route::get('/{garansi_id}', 'Customer\Api\GaransiController@show');
            // balas chat
            Route::post('/reply', 'Customer\Api\GaransiController@reply');
        });

        // reviews
        Route::group(['prefix' => 'reviews'], function () {
            // static total review
            Route::get('/statistic', 'Customer\Api\ReviewController@statistic');
            // list waiting order to review
            Route::get('/list-waiting', 'Customer\Api\ReviewController@waiting');
            // list order done review
            Route::get('/list-done', 'Customer\Api\ReviewController@done');
            // save review
            Route::post('/', 'Customer\Api\ReviewController@store');
        });

        // tickets
        Route::group(['prefix' => 'tickets'], function () {
            // list users dalaamna chat
            Route::get('/', 'Customer\Api\TicketController@index');
            Route::post('/', 'Customer\Api\TicketController@store');
            Route::post('/reply', 'Customer\Api\TicketController@replyAsJson');
            Route::get('/{ticket_id}', 'Customer\Api\TicketController@show');
            Route::put('{ticket}/status', 'Api\Master\TicketController@updateStatus');
        });

        // complaint
        Route::group(['prefix' => 'complaint'], function () {
            Route::get('/', 'CustomerController@indexComplaint');
            Route::post('/{order_id}', 'CustomerController@complaint');
            Route::post('/reply/{order_id}', 'CustomerController@replyOrderComplaint');
            Route::post('/close/{order_id}', 'CustomerController@complaintClose');
            Route::get('/detail/{order_id}', 'CustomerController@detailComplaintJson');
        });

        // history-transactions
        Route::group(['prefix' => 'history-transactions'], function () {
            Route::get('/', 'Customer\Api\OrderController@historyTransactionApi');
            Route::get('/{order_id}/detail', 'Customer\Api\OrderController@historyTransactionDetailApi');
        });
    });

    Route::group(['prefix' => 'teknisi', 'middleware' => ['role:Technician']], function () {
        // information
        Route::group(['prefix' => 'info'], function () {
            Route::get('account', 'Api\Technician\TechnicianProfileController@accountInfo');
            Route::post('rekening', 'Api\Technician\TechnicianProfileController@rekInfo');
            Route::get('garansi_available_count', 'Teknisi\Api\OrderController@garansiAvail');
        });

        // orders
        Route::group(['prefix' => 'orders'], function () {
            // static total order
            Route::get('/statistic', 'Teknisi\Api\OrderController@statistic');
            // list order
            Route::get('/', 'Teknisi\Api\OrderController@index');
            // search order
            Route::post('/search', 'Teknisi\Api\OrderController@search');
            // detail order
            Route::get('/{order_id}/show', 'Teknisi\Api\OrderController@show');
            // update_sparepart_detail  
            Route::put('/{order_id}/update_sparepart_before_accept', 'Api\OrderController@updateSparepartDetailBeforeApprove');
            // delete sparepart
            Route::delete('/{sparepart_detail_id}/destroy_sparepart', 'Api\OrderController@destroySparepartDetail');
            // accept job 
            Route::put('/{order_id}/accept-job', 'TeknisiController@acceptedJobSave');
            // reject job
            Route::put('/{order_id}/reject-job', 'TeknisiController@rejectJob');
            // on work
            Route::put('/{order_id}/working-job', 'TeknisiController@onWorking');
            // job done
            Route::put('/{order_id}/done-job', 'TeknisiController@jobsDone');
        });

        // price_services
        Route::group(['prefix' => 'price_services'], function () {
            Route::get('/', 'Api\Technician\TechnicianProfileController@priceServiceListPaginate');
            Route::post('/', 'Api\Technician\TechnicianProfileController@priceServiceStore');
            Route::put('/{id}', 'Api\Technician\TechnicianProfileController@priceServiceUpdate');
            Route::delete('{id}', 'Api\Technician\TechnicianProfileController@priceServiceDestroy');
        });

        // complaint
        Route::group(['prefix' => 'complaint'], function () {
            Route::get('/', 'TeknisiController@complaintListIndex');
            Route::post('/reply/{order_id}', 'CustomerController@replyOrderComplaint');
            Route::post('/close/{order_id}', 'CustomerController@complaintClose');
            Route::get('/detail/{order_id}', 'TeknisiController@detailComplaintJson');
        });

        // review
        Route::group(['prefix' => 'reviews'], function () {
            Route::get('/', 'TeknisiController@reviewListIndex');
        });

        // garansi
        Route::group(['prefix' => 'warranty'], function () {
            Route::get('/', 'TeknisiController@listGaransiJob');
        });

        // balance
        Route::group(['prefix' => 'balance'], function () {
            Route::get('/details', 'TeknisiController@balanceDetailList');
            Route::get('/total', 'TeknisiController@balanceTotal');
        });

        // schedules
        Route::group(['prefix' => 'schedules'], function () {
            Route::get('/', 'TeknisiController@mySchedulesApi');
        });
    });
});
