<?php

use Illuminate\Support\Facades\Route;

// // category
// Route::group(['prefix' => 'category'], function () {
//     Route::get('datatables', 'Api\CategoryController@datatables')->middleware('permission:category.show')->name('categories.datatables.api');
//     Route::get('search', 'Api\CategoryController@search')->middleware('permission:category.show')->name('categories.datatables.api');
//     Route::get('', 'Api\CategoryController@index')->middleware('permission:category.show')->name('categories.index.api');
//     Route::post('', 'Api\CategoryController@store')->middleware('permission:category.create')->name('categories.store.api');
//     Route::get('{category}', 'Api\CategoryController@show')->middleware('permission:category.create')->name('categories.show.api');
//     Route::put('{category}', 'Api\CategoryController@update')->middleware('permission:category.update')->name('categories.update.api');
//     Route::delete('{category}', 'Api\CategoryController@destroy')->middleware('permission:category.delete')->name('categories.destroy.api');
//     Route::delete('destroy_batch', 'Api\Master\CountryController@destroyBatch')->middleware('permission:categories.delete')->name('categories.destroy_bacth.api');
// });
//Vendor
Route::group(['prefix' => 'vendor'], function () {
    Route::get('datatables', 'Api\Master\VendorController@datatables')->middleware('permission:vendor.show')->name('vendors.datatables.api');
    Route::get('select2', 'Api\Master\VendorController@select2')->name('vendors.select2.api');
    Route::get('', 'Api\Master\VendorController@index')->middleware('permission:vendor.show')->name('vendors.index.api');
    Route::post('', 'Api\Master\VendorController@store')->middleware('permission:vendor.create')->name('vendors.store.api');
    Route::get('{vendor}', 'Api\Master\VendorController@show')->middleware('permission:vendor.create')->name('vendors.show.api');
    Route::put('{vendor}', 'Api\Master\VendorController@update')->middleware('permission:vendor.update')->name('vendors.update.api');
    Route::delete('destroy_batch', 'Api\Master\VendorController@destroyBatch')->middleware('permission:vendor.delete')->name('vendors.destroy_bacth.api');
    Route::delete('{vendor}', 'Api\Master\VendorController@destroy')->middleware('permission:vendor.delete')->name('vendors.destroy.api');
});
//Address Type
Route::group(['prefix' => 'address_type'], function () {
    Route::get('', 'Api\Master\AddressTypeController@index')->middleware('permission:address_type.show')->name('address.type.index.api');
    Route::get('datatables', 'Api\Master\AddressTypeController@datatables')->middleware('permission:address_type.show')->name('address.type.datatables.api');
    Route::get('select2', 'Api\Master\AddressTypeController@select2')->middleware('permission:address_type.show')->name('address.type.select2.api');
    Route::post('', 'Api\Master\AddressTypeController@store')->middleware('permission:address_type.create')->name('address.type.store.api');
    Route::put('{addresses}', 'Api\Master\AddressTypeController@update')->middleware('permission:address_type.update')->name('address.type.update.api');
    Route::delete('{addresses}', 'Api\Master\AddressTypeController@destroy')->middleware('permission:address_type.delete')->name('address.type.destroy.api');
});
//Marital
Route::group(['prefix' => 'marital'], function () {
    Route::get('datatables', 'Api\Master\MaritalController@datatables')->middleware('permission:marital.show')->name('maritals.datatables.api');
    Route::get('select2', 'Api\Master\MaritalController@select2')->name('maritals.select2.api');
    Route::get('', 'Api\Master\MaritalController@index')->middleware('permission:marital.show')->name('maritals.index.api');
    Route::post('', 'Api\Master\MaritalController@store')->middleware('permission:marital.create')->name('maritals.store.api');
    Route::get('{marital}', 'Api\Master\MaritalController@show')->middleware('permission:marital.create')->name('maritals.show.api');
    Route::put('{marital}', 'Api\Master\MaritalController@update')->middleware('permission:marital.update')->name('maritals.update.api');
    Route::delete('destroy_batch', 'Api\Master\MaritalController@destroyBatch')->middleware('permission:marital.delete')->name('maritals.destroy_bacth.api');
    Route::delete('{marital}', 'Api\Master\MaritalController@destroy')->middleware('permission:marital.delete')->name('maritals.destroy.api');
});
//Religion
Route::group(['prefix' => 'religion'], function () {
    Route::get('datatables', 'Api\Master\ReligionController@datatables')->middleware('permission:religion.show')->name('religions.datatables.api');
    Route::get('select2', 'Api\Master\ReligionController@select2')->name('religions.select2.api');
    Route::get('', 'Api\Master\ReligionController@index')->middleware('permission:religion.show')->name('religions.index.api');
    Route::post('', 'Api\Master\ReligionController@store')->middleware('permission:religion.create')->name('religions.store.api');
    Route::get('{religion}', 'Api\Master\ReligionController@show')->middleware('permission:religion.create')->name('religions.show.api');
    Route::put('{religion}', 'Api\Master\ReligionController@update')->middleware('permission:religion.update')->name('religions.update.api');
    Route::delete('destroy_batch', 'Api\Master\ReligionController@destroyBatch')->middleware('permission:religion.delete')->name('religions.destroy_bacth.api');
    Route::delete('{religion}', 'Api\Master\ReligionController@destroy')->middleware('permission:religion.delete')->name('religions.destroy.api');
});
//Batch
Route::group(['prefix' => 'batch'], function () {
    Route::get('datatables', 'Api\Master\BatchController@datatables')->middleware('permission:batch.show')->name('batches.datatables.api');
    Route::get('select2', 'Api\Master\BatchController@select2')->name('batches.select2.api');
    Route::get('child/select2', 'Api\Master\BatchController@select2child')->name('batches.select2.api');
    Route::get('', 'Api\Master\BatchController@index')->middleware('permission:batch.show')->name('batches.index.api');
    Route::post('', 'Api\Master\BatchController@store')->middleware('permission:batch.create')->name('batches.store.api');
    Route::get('{batch}', 'Api\Master\BatchController@show')->middleware('permission:batch.create')->name('batches.show.api');
    Route::put('{batch}', 'Api\Master\BatchController@update')->middleware('permission:batch.update')->name('batches.update.api');
    Route::delete('destroy_batch', 'Api\Master\BatchController@destroyBatch')->middleware('permission:batch.delete')->name('batches.destroy_bacth.api');
    Route::delete('{batch}', 'Api\Master\BatchController@destroy')->middleware('permission:batch.delete')->name('batches.destroy.api');
});

//Batch Status
Route::group(['prefix' => 'batch-status'], function () {
    Route::get('/datatables', 'Api\Master\BatchStatusController@datatables')->middleware('permission:batch_status.show')->name('batch_status.datatables.api');
    Route::get('/select2', 'Api\Master\BatchStatusController@select2')->name('batch_status.select2.api');
    Route::get('/', 'Api\Master\BatchStatusController@index')->middleware('permission:batch_status.show')->name('batch_status.index.api');
    Route::post('/', 'Api\Master\BatchStatusController@store')->middleware('permission:batch_status.create')->name('batch_status.store.api');
    Route::get('/{status}', 'Api\Master\BatchStatusController@show')->middleware('permission:batch_status.create')->name('batch_status.show.api');
    Route::put('/{status}', 'Api\Master\BatchStatusController@update')->middleware('permission:batch_status.update')->name('batch_status.update.api');
    Route::delete('/{status}', 'Api\Master\BatchStatusController@destroy')->middleware('permission:batch_status.delete')->name('batch_status.destroy.api');
});
//Unit Type
Route::group(['prefix' => 'unit-type'], function () {
    Route::get('datatables', 'Api\Master\UnitTypesController@datatables')->middleware('permission:unit_type.show')->name('unit.types.datatables.api');
    Route::get('select2', 'Api\Master\UnitTypesController@select2')->name('unit.types.select2.api');
    Route::get('', 'Api\Master\UnitTypesController@index')->middleware('permission:unit_type.show')->name('unit.types.index.api');
    Route::post('', 'Api\Master\UnitTypesController@store')->middleware('permission:unit_type.create')->name('unit.types.store.api');
    Route::get('{unitType}', 'Api\Master\UnitTypesController@show')->middleware('permission:unit_type.create')->name('unit.types.show.api');
    Route::put('{unitType}', 'Api\Master\UnitTypesController@update')->middleware('permission:unit_type.update')->name('unit.types.update.api');
    Route::delete('{unitType}', 'Api\Master\UnitTypesController@destroy')->middleware('permission:unit_type.delete')->name('unit.types.destroy.api');
});
//Warehouses
Route::group(['prefix' => 'warehouses'], function () {
    Route::get('datatables', 'Api\Master\WarehouseController@datatables')->middleware('permission:warehouse.show')->name('warehouse.datatables.api');
    Route::get('select2', 'Api\Master\WarehouseController@select2')->middleware('permission:warehouse.show')->name('warehouse.select2.api');
    Route::get('', 'Api\Master\WarehouseController@index')->middleware('permission:warehouse.show')->name('warehouse.index.api');
    Route::post('', 'Api\Master\WarehouseController@store')->middleware('permission:warehouse.create')->name('warehouse.store.api');
    Route::get('{warehouse}', 'Api\Master\WarehouseController@show')->middleware('permission:warehouse.show')->name('warehouse.show.api');
    Route::put('{warehouse}', 'Api\Master\WarehouseController@update')->middleware('permission:warehouse.show')->name('warehouse.show.api');
    Route::delete('{warehouse}', 'Api\Master\WarehouseController@destroy')->middleware('permission:warehouse.delete')->name('warehouse.destroy.api');
});
//Price Item
Route::group(['prefix' => 'price-item'], function () {
    Route::get('datatables', 'Api\Master\PriceItemsController@datatables')->middleware('permission:price_item.show')->name('price.items.datatables.api');
    Route::get('select2', 'Api\Master\PriceItemsController@select2')->middleware('permission:education.show')->name('price.items.select2.api');
    Route::get('', 'Api\Master\PriceItemsController@index')->middleware('permission:price_item.show')->name('price.items.index.api');
    Route::post('', 'Api\Master\PriceItemsController@store')->middleware('permission:price_item.create')->name('price.items.store.api');
    Route::put('{priceItem}', 'Api\Master\PriceItemsController@update')->middleware('permission:price_item.update')->name('price.items.update.api');
    Route::delete('{priceItem}', 'Api\Master\PriceItemsController@delete')->middleware('permission:price_item.delete')->name('price.items.delete.api');
});
//Inventory
Route::group(['prefix' => 'inventory'], function () {
    Route::get('datatables', 'Api\Master\InventoryController@dataTable')->middleware('permission:inventory.show')->name('inventori.datatables.api');
    Route::get('select2', 'Api\Master\InventoryController@select2')->middleware('permission:education.show')->name('inventori.select2.api');
    Route::get('', 'Api\Master\InventoryController@index')->middleware('permission:inventory.show')->name('inventori.index.api');
    Route::post('', 'Api\Master\InventoryController@store')->middleware('permission:inventory.create')->name('inventori.store.api');
    Route::put('{inventory}', 'Api\Master\InventoryController@update')->middleware('permission:inventory.update')->name('inventori.update.api');
    Route::delete('{inventory}', 'Api\Master\InventoryController@destroy')->middleware('permission:inventory.delete')->name('inventori.destroy.api');
});
//Curriculum
Route::group(['prefix' => 'curriculum'], function () {
    Route::get('datatables', 'Api\Master\CurriculumController@datatables')->middleware('permission:curriculum.show')->name('curriculum.datatables.api');
    Route::get('select2', 'Api\Master\CurriculumController@select2')->middleware('permission:curriculum.show')->name('curriculum.select2.api');
    Route::get('', 'Api\Master\CurriculumController@index')->middleware('permission:curriculum.show')->name('curriculum.index.api');
    Route::post('', 'Api\Master\CurriculumController@store')->middleware('permission:curriculum.create')->name('curriculum.store.api');
    Route::put('{inventory}', 'Api\Master\CurriculumController@update')->middleware('permission:curriculum.update')->name('curriculum.update.api');
    Route::delete('{inventory}', 'Api\Master\CurriculumController@destroy')->middleware('permission:curriculum.delete')->name('curriculum.destroy.api');
});
//Education
Route::group(['prefix' => 'education'], function () {
    Route::get('datatables', 'Api\Master\EducationsController@datatables')->middleware('permission:education.show')->name('education.datatables.api');
    Route::get('select2', 'Api\Master\EducationsController@select2')->middleware('permission:education.show')->name('education.select2.api');
    Route::get('', 'Api\Master\EducationsController@index')->middleware('permission:education.show')->name('education.index.api');
    Route::post('', 'Api\Master\EducationsController@store')->middleware('permission:education.create')->name('education.store.api');
    Route::put('{education}', 'Api\Master\EducationsController@update')->middleware('permission:education.update')->name('education.update.api');
    Route::delete('{education}', 'Api\Master\EducationsController@destroy')->middleware('permission:education.delete')->name('education.destroy.api');
});
//Gallery Image
Route::group(['prefix' => 'gallery-image'], function () {
    Route::get('{galeryImages}', 'Api\Master\GaleryImageController@show')->middleware('permission:gallery_image.show')->name('galeryImages.datatables.api');
    Route::put('{galeryImages}', 'Api\Master\GaleryImageController@update')->middleware('permission:gallery_image.update')->name('galeryImages.update.api');
});
