<?php

// additional info

use Illuminate\Support\Facades\Route;

Route::prefix('additional_info')->group(function () {
    Route::post('/', 'Api\Master\AdditionalInfoController@store')
        ->name('additional_info.store.api');
});

// user address
Route::prefix('addresses')->group(function () {
    Route::get('/datatables', 'Api\Master\AddressController@datatables')
        ->middleware('permission:address.show')
        ->name('addresses.datatables.api');
    Route::get('/', 'Api\Master\AddressController@index')
        ->middleware('permission:address.show')
        ->name('addresses.index.api');
    Route::post('/', 'Api\Master\AddressController@store')
        ->middleware('permission:address.create')
        ->name('addresses.store.api');
    Route::get('/{address}', 'Api\Master\AddressController@show')
        ->middleware('permission:address.create')
        ->name('addresses.show.api');
    Route::put('/main/{address}', 'Api\Master\AddressController@setMain')
        ->middleware('permission:address.update')
        ->name('addresses.setMain.api');
    Route::put('/{address}', 'Api\Master\AddressController@update')
        ->middleware('permission:address.update')
        ->name('addresses.update.api');
    Route::delete('/{address}', 'Api\Master\AddressController@destroy')
        ->middleware('permission:address.delete')
        ->name('addresses.destroy.api');
});

// user group
Route::prefix('user_group')->group(function () {
    Route::get('/datatables', 'Api\Master\UserGroupController@datatables')
        ->middleware('permission:user_group.show')
        ->name('user_group.datatables.api');
    Route::get('/select2', 'Api\Master\UserGroupController@select2')
        ->middleware('permission:user_group.show')
        ->name('user_group.select2.api');
    Route::get('/', 'Api\Master\UserGroupController@index')
        ->middleware('permission:user_group.show')
        ->name('user_group.index.api');
    Route::post('/', 'Api\Master\UserGroupController@store')
        ->middleware('permission:user_group.create')
        ->name('user_group.store.api');
    Route::get('/{user_group}', 'Api\Master\UserGroupController@show')
        ->middleware('permission:user_group.create')
        ->name('user_group.show.api');
    Route::put('/main/{user_group}', 'Api\Master\UserGroupController@setMain')
        ->middleware('permission:user_group.update')
        ->name('user_group.setMain.api');
    Route::put('/{user_group}', 'Api\Master\UserGroupController@update')
        ->middleware('permission:user_group.update')
        ->name('user_group.update.api');
    Route::delete('/{user_group}', 'Api\Master\UserGroupController@destroy')
        ->middleware('permission:user_group.delete')
        ->name('user_group.destroy.api');
});
