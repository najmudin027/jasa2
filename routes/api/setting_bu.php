<?php

use App\Model\Master\MsProduct;
use App\Model\Master\Vendor;
use App\Model\Product\ProductAttribute;
use App\Model\Product\ProductVarian;
use App\Model\Product\ProductVendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

// permission
Route::prefix('permissions')->group(function () {
    Route::get('/datatables', 'Api\PermissionController@datatables')
        ->middleware('permission:permission.show')
        ->name('permissions.datatables.api');
    Route::get('/select2', 'Api\PermissionController@select2')
        ->name('permissions.select2.api');
    Route::get('/', 'Api\PermissionController@index')
        ->middleware('permission:permission.show')
        ->name('permissions.index.api');
    Route::post('/', 'Api\PermissionController@store')
        ->middleware('permission:permission.create')
        ->name('permissions.store.api');
    Route::get('/{id}', 'Api\PermissionController@show')
        ->middleware('permission:permission.create')
        ->name('permissions.show.api');
    Route::put('/{id}', 'Api\PermissionController@update')
        ->middleware('permission:permission.update')
        ->name('permissions.update.api');
    Route::delete('/{id}', 'Api\PermissionController@destroy')
        ->middleware('permission:permission.delete')
        ->name('permissions.destroy.api');
});

// role
Route::prefix('roles')->group(function () {
    Route::get('/datatables', 'Api\RoleController@datatables')
        ->middleware('permission:role.show')
        ->name('roles.datatables.api');
    Route::get('/select2', 'Api\RoleController@select2')
        ->name('roles.select2.api');
    Route::get('/', 'Api\RoleController@index')
        ->middleware('permission:role.show')
        ->name('roles.index.api');
    Route::post('/', 'Api\RoleController@store')
        ->middleware('permission:role.create')
        ->name('roles.store.api');
    Route::get('/{id}', 'Api\RoleController@show')
        ->middleware('permission:role.create')
        ->name('roles.show.api');
    Route::put('/{id}', 'Api\RoleController@update')
        ->middleware('permission:role.update')
        ->name('roles.update.api');
    Route::delete('/{id}', 'Api\RoleController@destroy')
        ->middleware('permission:role.delete')
        ->name('roles.destroy.api');
});

// user
Route::prefix('users')->group(function () {
    Route::get('/select2', 'Api\UserController@select2')
        ->name('users.select2.api');
    Route::get('/datatables', 'Api\UserController@datatables')
        ->middleware('permission:user.show')
        ->name('users.datatables.api');
    Route::get('/', 'Api\UserController@index')
        ->middleware('permission:user.show')
        ->name('users.index.api');
    Route::post('/', 'Api\UserController@store')
        ->middleware('permission:user.create')
        ->name('users.store.api');
    Route::get('/{id}', 'Api\UserController@show')
        ->middleware('permission:user.create')
        ->name('users.show.api');
    Route::put('/{id}', 'Api\UserController@update')
        ->middleware('permission:user.update')
        ->name('users.update.api');
    Route::delete('/{id}', 'Api\UserController@destroy')
        ->middleware('permission:user.delete')
        ->name('users.destroy.api');
});

// menu
Route::prefix('menus')->group(function () {
    Route::get('/datatables', 'Api\MenuController@datatables')
        ->middleware('permission:menu.show')
        ->name('menus.datatables.api');
    Route::get('/select2', 'Api\MenuController@select2')
        ->name('menus.select2.api');
    Route::get('/', 'Api\MenuController@index')
        ->middleware('permission:menu.show')
        ->name('menus.index.api');
    Route::post('/', 'Api\MenuController@store')
        ->middleware('permission:menu.create')
        ->name('menus.store.api');
    Route::get('/{menu}', 'Api\MenuController@show')
        ->middleware('permission:menu.create')
        ->name('menus.show.api');
    Route::put('/{menu}', 'Api\MenuController@update')
        ->middleware('permission:menu.update')
        ->name('menus.update.api');
    Route::delete('/{menu}', 'Api\MenuController@destroy')
        ->middleware('permission:menu.delete')
        ->name('menus.destroy.api');
    Route::post('move', 'Api\MenuController@updateMoveMenu')
        ->middleware('permission:product_category.update')
        ->name('product_category.store.api');
});

// web Settings
Route::prefix('web_setting')->group(function () {
    // Route::get('/datatables', 'Api\WebSettingController@datatables')
    //     ->middleware('permission:menu.show')
    //     ->name('menus.datatables.api');
    // Route::get('/select2', 'Api\WebSettingController@select2')
    //     ->name('menus.select2.api');
    Route::get('/', 'Api\WebSettingController@index')
        ->middleware('permission:menu.show')
        ->name('menus.index.api');
    Route::post('/', 'Api\WebSettingController@store')
        ->middleware('permission:menu.create')
        ->name('menus.store.api');
    // Route::get('/{menu}', 'Api\WebSettingController@show')
    //     ->middleware('permission:menu.create')
    //     ->name('menus.show.api');
    Route::put('/{menu}', 'Api\WebSettingController@update')
        ->middleware('permission:menu.update')
        ->name('menus.update.api');
    // Route::delete('/{menu}', 'Api\WebSettingController@destroy')
    //     ->middleware('permission:menu.delete')
    //     ->name('menus.destroy.api');
    // Route::post('move', 'Api\WebSettingController@updateMoveMenu')
    //     ->middleware('permission:product_category.update')
    //     ->name('product_category.store.api');
});

Route::prefix('coba-coba')->group(function () {

    Route::post('products/find_varian', function (Request $request) {
        $vendor = ProductVendor::where('ms_product_id', $request->product_id)
            ->where('vendor_id', $request->vendor_id)
            ->first();

        $response = ProductVarian::where('product_vendor_id', $vendor->id)
            ->whereHas('attributes', function ($query) use ($request) {
                foreach ($request->attribute as $key => $val) {
                    $att_id = $request->attribute[$key];
                    $term_id = $request->term[$key];
                    $query->where(function ($q) use ($att_id, $term_id) {
                        $q->where('product_attribute_term_id', $term_id)
                            ->where('product_attribute_id', $att_id);
                    });
                }
            })->first();

        return response()->json($response, 200);
    });

    Route::get('vendors/select2/{id}', function (Request $request, $id) {
        $vendors = ProductVendor::with('vendor')->where('ms_product_id', $id)->get();

        $data = [
            'msg' => 'test',
            'data' => $vendors,
        ];

        return response()->json($data, 200);
    });

    Route::get('products/{id}/{vendor_id}', function (Request $request, $id, $vendor_id) {

        $vendor = ProductVendor::where('ms_product_id', $id)
            ->where('vendor_id', $vendor_id)
            ->first();

        $response = ProductVarian::where('product_vendor_id', $vendor->id)
            ->with('attributes.term', 'attributes.attribute')
            ->get();

        $combination = [];
        $att = [];
        foreach ($response as $varian) {
            foreach ($varian->attributes as $pAttribute) {
                $combination[$pAttribute->attribute->id][$pAttribute->term->id] = $pAttribute;
            }
        }

        foreach ($combination as $attribute_id => $com) {
            $att[] = [
                'attribute' => ProductAttribute::find($attribute_id),
                'terms' => $com,
            ];
        }
        $data = [
            'msg' => 'test',
            'data' => $att,
        ];

        return response()->json($data, 200);
    });
});
