<?php

use Illuminate\Support\Facades\Route;

// product_attribute
Route::prefix('product_attribute')->group(function () {
    Route::get('/childs/{product_attribute_id}', 'Api\Product\ProductAttributeController@childs')
        ->middleware('permission:product_attribute.show')
        ->name('product_attribute.childs.api');
    Route::get('/datatables', 'Api\Product\ProductAttributeController@datatables')
        ->middleware('permission:product_attribute.show')
        ->name('product_attribute.datatables.api');
    Route::get('/select2', 'Api\Product\ProductAttributeController@select2')
        ->name('product_attribute.select2.api');
    Route::get('/', 'Api\Product\ProductAttributeController@index')
        ->middleware('permission:product_attribute.show')
        ->name('product_attribute.index.api');
    Route::post('/', 'Api\Product\ProductAttributeController@store')
        ->middleware('permission:product_attribute.create')
        ->name('product_attribute.store.api');
    Route::get('/{attribute}', 'Api\Product\ProductAttributeController@show')
        ->middleware('permission:product_attribute.create')
        ->name('product_attribute.show.api');
    Route::put('/update_batch', 'Api\Product\ProductAttributeController@updateBatch')
        ->middleware('permission:product_attribute.update')
        ->name('product_attribute.update_batch.api');
    Route::put('/{attribute}', 'Api\Product\ProductAttributeController@update')
        ->middleware('permission:product_attribute.update')
        ->name('product_attribute.update.api');
    Route::delete('/destroy_child/{term}', 'Api\Product\ProductAttributeController@destroyChild')
        ->middleware('permission:product_attribute.delete')
        ->name('product_attribute.destroy_child.api');
    Route::delete('/{attribute}', 'Api\Product\ProductAttributeController@destroy')
        ->middleware('permission:product_attribute.delete')
        ->name('product_attribute.destroy.api');
});

// product_additional
Route::prefix('product_additional')->group(function () {
    // get
    Route::get('/datatables', 'Api\Product\ProductAdditionalController@datatables')
        ->middleware('permission:product_additional.show')
        ->name('product_additional.datatables.api');
    Route::get('/select2', 'Api\Product\ProductAdditionalController@select2')
        ->name('product_additional.select2.api');
    Route::get('/', 'Api\Product\ProductAdditionalController@index')
        ->middleware('permission:product_additional.show')
        ->name('product_additional.index.api');
    Route::get('/{product_additional}', 'Api\Product\ProductAdditionalController@show')
        ->middleware('permission:product_additional.create')
        ->name('product_additional.show.api');

    // post
    Route::post('/', 'Api\Product\ProductAdditionalController@store')
        ->middleware('permission:product_additional.create')
        ->name('product_additional.store.api');

    // put
    Route::put('/{product_additional}', 'Api\Product\ProductAdditionalController@update')
        ->middleware('permission:product_additional.update')
        ->name('product_additional.update.api');

    // delete
    Route::delete('/destroy_batch', 'Api\Product\ProductAdditionalController@destroyBatch')
        ->middleware('permission:product_additional.delete')
        ->name('product_additional.destroy_bacth.api');
    Route::delete('/{product_additional}', 'Api\Product\ProductAdditionalController@destroy')
        ->middleware('permission:product_additional.delete')
        ->name('product_additional.destroy.api');
});

// product_part_category
Route::prefix('product_part_category')->group(function () {
    Route::get('/datatables', 'Api\Product\ProductPartCategoryController@datatables')
        ->middleware('permission:product_part_category.show')
        ->name('product_part_category.datatables.api');
    Route::get('/select2', 'Api\Product\ProductPartCategoryController@select2')
        ->name('product_part_category.select2.api');
    Route::get('/', 'Api\Product\ProductPartCategoryController@index')
        ->middleware('permission:product_part_category.show')
        ->name('product_part_category.index.api');
    Route::post('/', 'Api\Product\ProductPartCategoryController@store')
        ->name('product_part_category.store.api');
    Route::get('/{category}', 'Api\Product\ProductPartCategoryController@show')
        ->middleware('permission:product_part_category.show')
        ->name('product_part_category.show.api');
    Route::put('/{category}', 'Api\Product\ProductPartCategoryController@update')
        ->middleware('permission:product_part_category.update')
        ->name('product_part_category.update.api');
    Route::delete('/{category}', 'Api\Product\ProductPartCategoryController@destroy')
        ->middleware('permission:product_part_category.delete')
        ->name('product_part_category.destroy.api');
});


// product_brand
Route::prefix('product_brand')->group(function () {
    Route::get('/datatables', 'Api\Product\ProductBrandController@datatables')
        ->middleware('permission:product_brand.show')
        ->name('product_brand.datatables.api');
    Route::get('/select2', 'Api\Product\ProductBrandController@select2')
        ->name('product_brand.select2.api');
    Route::get('/', 'Api\Product\ProductBrandController@index')
        ->middleware('permission:product_brand.show')
        ->name('product_brand.index.api');
    Route::post('/', 'Api\Product\ProductBrandController@store')
        ->middleware('permission:product_brand.create')
        ->name('product_brand.store.api');
    Route::get('/{brand}', 'Api\Product\ProductBrandController@show')
        ->middleware('permission:product_brand.create')
        ->name('product_brand.show.api');
    Route::put('/{brand}', 'Api\Product\ProductBrandController@update')
        ->middleware('permission:product_brand.update')
        ->name('product_brand.update.api');
    Route::delete('/{brand}', 'Api\Product\ProductBrandController@destroy')
        ->middleware('permission:product_brand.delete')
        ->name('product_brand.destroy.api');
});

// product category
Route::prefix('product_category')->group(function () {
    Route::get('/datatables', 'Api\Product\ProductCategoryController@datatables')
        ->middleware('permission:product_category.show')
        ->name('product_category.datatables.api');
    Route::get('/select2', 'Api\Product\ProductCategoryController@select2')
        ->name('product_category.select2.api');
    Route::get('/', 'Api\Product\ProductCategoryController@index')
        ->middleware('permission:product_category.show')
        ->name('product_category.index.api');
    Route::post('/', 'Api\Product\ProductCategoryController@store')
        ->middleware('permission:product_category.create')
        ->name('product_category.store.api');
    Route::get('/{product_category}', 'Api\Product\ProductCategoryController@show')
        ->middleware('permission:product_category.create')
        ->name('product_category.show.api');
    Route::put('/{product_category}', 'Api\Product\ProductCategoryController@update')
        ->middleware('permission:product_category.update')
        ->name('product_category.update.api');
    Route::delete('/destroy_batch', 'Api\Product\ProductCategoryController@destroyBatch')
        ->middleware('permission:product_category.delete')
        ->name('product_category.destroy_batch.api');
    Route::delete('/{product_category}', 'Api\Product\ProductCategoryController@destroy')
        ->middleware('permission:product_category.delete')
        ->name('product_category.destroy.api');

    Route::post('move', 'Api\Product\ProductCategoryController@updateMoveCategory')
        ->middleware('permission:product_category.update')
        ->name('product_category.store.api');
});

// product model
Route::prefix('product_model')->group(function () {
    Route::get('/datatables', 'Api\Product\ProductModelController@datatables')
        ->middleware('permission:product_model.show')
        ->name('product_model.datatables.api');
    Route::get('/select2', 'Api\Product\ProductModelController@select2')
        ->name('product_model.select2.api');
    Route::get('/select2/{id}', 'Api\Product\ProductModelController@select2p')
        ->name('product_model.select2.api');
    Route::get('/', 'Api\Product\ProductModelController@index')
        ->middleware('permission:product_model.show')
        ->name('product_model.index.api');
    Route::post('/', 'Api\Product\ProductModelController@store')
        ->middleware('permission:product_model.create')
        ->name('product_model.store.api');
    Route::get('/{product_model}', 'Api\Product\ProductModelController@show')
        ->middleware('permission:product_model.create')
        ->name('product_model.show.api');
    Route::put('/{product_model}', 'Api\Product\ProductModelController@update')
        ->middleware('permission:product_model.update')
        ->name('product_model.update.api');
    Route::delete('/{product_model}', 'Api\Product\ProductModelController@destroy')
        ->middleware('permission:product_model.delete')
        ->name('product_model.destroy.api');
});

// product
Route::prefix('product')->group(function () {
    Route::get('/datatables', 'Api\Master\ProductController@datatables')
        ->middleware('permission:product.show')
        ->name('product.datatables.api');

    Route::get('/select2', 'Api\Master\ProductController@select2')
        ->name('product.select2.api');

    Route::get('/', 'Api\Master\ProductController@index')
        ->middleware('permission:product.show')
        ->name('product.index.api');

    Route::post('/', 'Api\Master\ProductController@store')
        ->middleware('permission:product.create')
        ->name('product.store.api');

    Route::get('/{product}', 'Api\Master\ProductController@detailsProduct')
        ->middleware('permission:product.create')
        ->name('product.show.api');

    // Route::put('/{product}', 'Api\Master\ProductController@update')
    //     ->middleware('permission:product.update')
    //     ->name('product.update.api');

    Route::post('/{product}', 'Api\Master\ProductController@update')
        ->middleware('permission:product.update')
        ->name('product.update.api');

    Route::delete('/{product}', 'Api\Master\ProductController@destroy')
        ->middleware('permission:product.delete')
        ->name('product.destroy.api');
});

// Edit View (delete vendor or varian)
Route::delete('product_vendor/{product_vendor}', 'Api\Master\ProductController@deleteProductVendor')
    ->middleware('permission:product.delete');
Route::delete('product_varian/{product_varian}', 'Api\Master\ProductController@deleteProductVarian')
    ->middleware('permission:product.delete');

Route::prefix('/get')->group(function () {
    //Product Edit Details
    Route::get('product-vendor-by/{product_id}/prod-id', 'Api\Master\ProductController@listProVenByProdId')
        ->middleware('permission:product.show')
        ->name('product.show.api');
    Route::get('product-varian-by/{product_vendor_id}/prod-vendor-id', 'Api\Master\ProductController@listProVarByProdVenId')
        ->middleware('permission:product.show')
        ->name('product.show.api');
    Route::get('product-varian-attr-by/{product_varian_id}/prod-varian-id', 'Api\Master\ProductController@listProVarAttrByProdVarId')
        ->middleware('permission:product.show')
        ->name('product.show.api');
});

// product status
Route::prefix('product_status')->group(function () {
    Route::get('/datatables', 'Api\Product\ProductStatusController@datatables')
        ->middleware('permission:product_status.show')
        ->name('product_status.datatables.api');

    Route::get('/select2', 'Api\Product\ProductStatusController@select2')
        ->name('product_status.select2.api');

    Route::get('/', 'Api\Product\ProductStatusController@index')
        ->middleware('permission:product_status.show')
        ->name('product_status.index.api');

    Route::post('/', 'Api\Product\ProductStatusController@store')
        ->middleware('permission:product_status.create')
        ->name('product_status.store.api');

    Route::get('/{status}', 'Api\Product\ProductStatusController@show')
        ->middleware('permission:product_status.create')
        ->name('product_status.show.api');

    Route::put('/{status}', 'Api\Product\ProductStatusController@update')
        ->middleware('permission:product_status.update')
        ->name('product_status.update.api');

    Route::delete('/{status}', 'Api\Product\ProductStatusController@destroy')
        ->middleware('permission:product_status.delete')
        ->name('product_status.destroy.api');
});
