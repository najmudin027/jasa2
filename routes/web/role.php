<?php

use App\Model\Category;
use App\Model\Master\MsProductModel;
use App\Model\Product\ProductVarian;
use App\Services\Product\ProductVarianService;
use Illuminate\Support\Facades\Route;

Route::post('/roles', 'Admin\RoleController@store')
    ->name('admin.role.store');
Route::put('/roles/{id}', 'Admin\RoleController@update')
    ->name('admin.role.update');

Route::get('/coba-coba', function () {
    $data = [];
    // level 1
    $data['name'] = 'name';
    $data['product_model_id'] = 'name';
    $data['product_additionals_id'] = 'name';
    $data['is_attribute'] = 1;
    // level 2
    $data['vendor_id']['vendor_1'] = 1;
    $data['vendor_id']['vendor_2'] = 2;
    // level 3
    $data['stock']['vendor_1']['varian_1'] = 20;
    $data['stock']['vendor_2']['varian_2'] = 12;
    // level 4
    $data['product_attribute_id']['vendor_1']['varian_1'] = [3, 2];
    $data['product_attribute_id']['vendor_2']['varian_2'] = [7, 4];

    $data['product_attribute_term_id']['vendor_1']['varian_1'] = [3, 2];
    $data['product_attribute_term_id']['vendor_2']['varian_2'] = [7, 4];

    (new ProductVarianService)
        ->setProduct(MsProductModel::first())
        ->create($data);

    return ProductVarian::with('product', 'attributes.attribute', 'attributes.term')->get();
});
