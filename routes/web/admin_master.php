<?php

# Price Item
Route::group(['prefix' => 'price-item'], function () {
    Route::get('/', 'Admin\MasterPriceItemsController@showListPriceItem')
        ->middleware('permission:price_item.show');
    Route::get('update/{id}', 'Admin\MasterPriceItemsController@updatePriceItems')
        ->middleware('permission:price_item.update');
});

# Unit Type
Route::group(['prefix' => 'unit-type'], function () {
    // list data
    Route::get('/', 'Admin\MasterUnitTypesController@showListUnitType')
        ->name('admin.unit_type.index.web')
        ->middleware('permission:unit_type.show');
});

# Warehouse
Route::group(['prefix' => 'warehouse'], function () {
    // list data
    Route::get('/', 'Admin\MasterWarehouseController@showWarehouse')
        ->name('admin.warehouse.index.web')
        ->middleware('permission:warehouse.show');

    // create data
    Route::get('create', 'Admin\MasterWarehouseController@createWarehouse')
        ->name('admin.warehouse.create.web')
        ->middleware('permission:warehouse.create');

    // edit data
    Route::get('{id}/edit', 'Admin\MasterWarehouseController@updateWarehouse')
        ->name('admin.warehouse.edit.web')
        ->middleware('permission:warehouse.update');
});

# Vendor
Route::group(['prefix' => 'vendor'], function () {
    // list data
    Route::get('/', 'Admin\MasterVendorController@showListVendor')
        ->name('admin.warehouse.index.web')
        ->middleware('permission:vendor.show');

    // create data
    Route::get('create', 'Admin\MasterVendorController@createVendor')
        ->name('admin.vendor.create.web')
        ->middleware('permission:vendor.create');

    // edit data
    Route::get('{id}/edit', 'Admin\MasterVendorController@updateVendor')
        ->name('admin.warehouse.edit.web')
        ->middleware('permission:vendor.update');
});

//Address
Route::group(['prefix' => 'address'], function () {
    Route::get('/teknisi/info', 'Admin\ProfileController@teknisiInfo');
    Route::get('/users/info', 'Admin\ProfileController@usersInfo');
    Route::get('show', 'Admin\ProfileController@listCreate');
    Route::get('/user/update/{id}', 'Admin\ProfileController@adminUpdateAddress');
    Route::get('create', 'Admin\ProfileController@addressCreate');
    Route::get('detail/{id}', 'Admin\ProfileController@detailAddress');
    Route::get('update/{id}', 'Admin\ProfileController@updateAddress');
});

// user status
Route::group(['prefix' => 'user_status'], function () {
    Route::get('/', 'Admin\UsersStatusController@index')->middleware('permission:user.show');
});

//Address Type
Route::group(['prefix' => 'address-type'], function () {
    Route::get('/', 'Admin\MasterTypeAddressController@showListAddressType')->middleware('permission:address_type.show');
});

//Marital
Route::group(['prefix' => 'marital'], function () {
    Route::get('/', 'Admin\MasterMaritalController@showListMarital')->middleware('permission:marital.show');
});

//Religion
Route::group(['prefix' => 'religion'], function () {
    Route::get('/', 'Admin\MasterReligionController@index')->middleware('permission:religion.show');
});

// grades
Route::group(['prefix' => 'grades'], function () {
    Route::get('/', 'Admin\MasterGradeController@index')->middleware('permission:grade.show');
});

// tat
Route::group(['prefix' => 'tat_grup'], function () {
    Route::get('/', 'Admin\MasterTatGrupController@index')->middleware('permission:tat_grup.show');
});

// product_groups
Route::group(['prefix' => 'product_groups'], function () {
    Route::get('/', 'Admin\ProductGroupController@index')->middleware('permission:product_group.show');
});

//Order Status
Route::group(['prefix' => 'order-status'], function () {
    Route::get('show', 'Admin\MasterServicesController@orderStatusIndex')->middleware('permission:order.show');
});

//education info
Route::group(['prefix' => 'educations'], function () {
    Route::get('show', 'Admin\EducationInfoController@index')->middleware('permission:gallery_image.show');
    Route::get('create', 'Admin\EducationInfoController@create')->middleware('permission:gallery_image.create');
    Route::get('update/{id}', 'Admin\EducationInfoController@update')->middleware('permission:gallery_image.update');
});

// Curriculum
Route::group(['prefix' => 'curriculum'], function () {
    Route::get('show', 'Admin\CurriculumInfoController@index')->middleware('permission:curriculum.show')->name('curriculum.show');
    Route::get('create', 'Admin\CurriculumInfoController@create')->middleware('permission:curriculum.create')->name('curriculum.create');
    Route::get('update', 'Admin\CurriculumInfoController@update')->middleware('permission:curriculum.update')->name('curriculum.update');
});

// Development Program
Route::group(['prefix' => 'development'], function () {
    Route::get('show', 'Admin\DevelopmentProgramInfoController@index')->middleware('permission:development_program.show')->name('development_program.show');
});

// Curriculum squence
Route::group(['prefix' => 'sequence'], function () {
    Route::get('show', 'Admin\CurriculumSequenceInfoController@index')->middleware('permission:curriculum.show')->name('curriculum_squence.show');
});

// Development Plan
Route::group(['prefix' => 'devPlant'], function () {
    Route::get('show', 'Admin\DevelopmentPlanInfoController@index')->middleware('permission:curriculum.show')->name('curriculum_squence.show');
});

// job-title-category
Route::group(['prefix' => 'job-title-category'], function () {
    Route::get('/', 'Admin\JobTitleCategoryController@index')
        ->middleware('permission:job_title_category.show')
        ->name('job-title-category.index.web');
});

// job-title
Route::group(['prefix' => 'job-title'], function () {
    Route::get('/', 'Admin\JobTitleController@index')
        ->middleware('permission:job_title.show')
        ->name('job-title.index.web');
});

// job-experience
Route::group(['prefix' => 'job-experience'], function () {
    Route::get('/', 'Admin\JobExperienceController@index')
        ->middleware('permission:job_experience.show')
        ->name('job-title.index.web');
});
