<?php
// Services
function routeImage($directory, $filename) {
    // $userLogin = Auth::user()->id;
    $path      = storage_path($directory . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file     = File::get($path);
    $type     = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    $response->header("pragma", "private");
    $response->header("Cache-Control", " private, max-age=86400");
    return $response;
}
Route::get('storage/general-journal/{filename}', function ($filename) {
    return routeImage('app/public/general-journal/',$filename);
});

Route::get('storage/general-journal-detail/{filename}', function ($filename) {
    return routeImage('app/public/general-journal-detail/',$filename);
});


Route::get('storage/services/{filename}', function ($filename) {
    return routeImage('app/public/services/',$filename);
});
Route::get('storage/services-category/{filename}', function ($filename) {
    return routeImage('app/public/symptom/',$filename);
});
Route::get('storage/services-type/{filename}', function ($filename) {
    return routeImage('app/public/services-type/',$filename);
});
Route::get('storage/product-group/{filename}', function ($filename) {
    return routeImage('app/public/product-group/',$filename);
});

Route::get('storage/buktiTransfer/{filename}', function ($filename) {
    return routeImage('app/public/buktiTransfer/',$filename);
});

Route::get('storage/attachment/{filename}', function ($filename) {
    return routeImage('app/public/attachment/',$filename);
});

Route::get('storage/user/profile/avatar/{filename}', function ($filename) {
    return routeImage('app/public/user/profile/avatar/',$filename);
});

// ============================
// customer
// =======================
Route::get('customer/storage/image/profile/{filename}', function ($filename) {
    return routeImage('app/public/ImageProfile/',$filename);
});

Route::get('customer/storage/attachment/{filename}', function ($filename) {
    return routeImage('app/public/attachment/',$filename);
});

Route::get('customer/storage/attachment/complaint/{filename}', function ($filename) {
    return routeImage('app/public/orderComplaint/',$filename);
});

Route::get('admin/storage/attachment/complaint/{filename}', function ($filename) {
    return routeImage('app/public/orderComplaint/',$filename);
});

Route::get('admin/storage/images/{filename}', function ($filename) {
    return routeImage('app/public/galery_images/',$filename);
});

Route::get('teknisi/storage/attachment/complaint/{filename}', function ($filename) {
    return routeImage('app/public/orderComplaint/',$filename);
});

Route::get('customer/storage/bankIcon/{filename}', function ($filename) {
    return routeImage('app/public/bankIcon/',$filename);
});

//Get file All
Route::get('storage/order_problem/{filename}', function ($filename) {
    return routeImage('app/public/order_media_problem/',$filename);
});

Route::get('storage/garansi/attachment/{filename}', function ($filename) {
    return routeImage('app/public/garansi/attachment/',$filename);
});

Route::get('storage/user/chat/{filename}', function ($filename) {
    return routeImage('app/public/user/chat/',$filename);
});

Route::get('storage/banner/{filename}', function ($filename) {
    return routeImage('app/public/banner/',$filename);
});


