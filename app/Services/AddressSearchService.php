<?php

namespace App\Services;

use App\Model\Master\MsDistrict;
use App\Model\Master\MsProvince;
use Illuminate\Http\Request;

class AddressSearchService
{
    public $availableRelation = [
        'province' => ['country', 'cities', 'districts']
    ];

    public function findDistrict(int $district_id, array $with = [], Request $request = null)
    {
        $with = $this->checkRelation(
            $this->getAvailRelation('province'),
            $with
        );

        if ($district_id) {
            return MsDistrict::with('villages', 'city', 'zipcode')
                ->find($district_id);
        }

        $response = MsDistrict::with('zipcode')
            ->where('name', 'like', '%' . $request->q . '%')
            ->when($request->city_id, function ($query, $city_id) {
                return $query->where('ms_city_id', $city_id);
            })
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function findProvince(int $id, $with = [])
    {
        $with = $this->checkRelation(
            $this->getAvailRelation('province'),
            $with
        );

        if ($with) {
            return MsProvince::with($with)->find($id);
        }

        return MsProvince::find($id);
    }

    public function checkRelation($availableRelation, $with)
    {
        $results = [];
        foreach ($with as $key => $name) {
            foreach ($availableRelation as $key => $avalName) {
                if ($name == $avalName) {
                    $results[] = $avalName;
                }
            }
        }

        return $results;
    }

    public function getAvailRelation($name)
    {
        return $this->availableRelation[$name];
    }
}
