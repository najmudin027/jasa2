<?php

namespace App\Services\Product;

use App\Model\Master\MsProductModel;
use App\Model\Product\ProductBrand;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductBrandService
{
    public $brand;

    /**
     * dipakai saat delete data
     * akan mengecek ke semua relasi 
     * pada model ini
     * untuk mencari jika model ini sudah di pakai di model lain
     * note: lumayan berat karna harus cek ke semua table
     */
    public $use_relation_check = true;

    /**
     * get list query
     *
     * @return App\Model\Product\ProductBrand;
     */
    public function list()
    {
        return ProductBrand::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Product\ProductBrand
     */
    public function datatables()
    {
        $query = $this->list()->with('part_category');

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Product\ProductBrand
     */
    public function select2(Request $request)
    {
        return ProductBrand::with('part_category')->where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     *
     * @param int $id
     *
     * @return \App\Model\Product\ProductBrand
     */
    public function find(int $id)
    {
        return ProductBrand::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Product\ProductBrand
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return ProductBrand::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getBrand()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        return $this->relasiCheck()->delete();
    }

    public function deleteById(array $ids)
    {
        try {
            return ProductBrand::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
            'code' => $data['code'],
            'product_part_category_id' => $data['product_part_category_id'],
        ];
    }

    public function relasiCheck()
    {
        $row = $this->getBrand();

        if ($this->use_relation_check) {
            // cek ke produk model
            $model = MsProductModel::where('product_brands_id', $row->id)->first();
            if ($model != null) {
                throw new \Exception('already used in (product model) module, model name (' . $model->name . ')');
            }
        }

        return $row;
    }

    public function setBrand(ProductBrand $brand)
    {
        $this->brand = $brand;
        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }
}
