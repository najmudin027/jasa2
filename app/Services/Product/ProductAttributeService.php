<?php

namespace App\Services\Product;

use App\Exceptions\HasChildException;
use App\Model\Master\MsProduct;
use App\Model\Product\ProductAttribute;
use App\Model\Product\ProductAttributeTerm;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class ProductAttributeService
{
    public $attribute;

    /**
     * get list query
     * 
     * @return App\Model\Product\ProductAttribute;
     */
    public function list()
    {
        return ProductAttribute::with('childs');
    }

    /**
     * get data
     * 
     * @param int $id
     * 
     * @return App\Model\Product\ProductAttribute;
     */
    public function find(int $id)
    {
        return ProductAttribute::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Product\ProductAttribute
     * 
     * @throws \Exception
     */
    public function create(array $data)
    {
        $dataAttribute = $this->generateData($data);

        DB::beginTransaction();
        try {
            // insert attribute
            $attribute = ProductAttribute::create($dataAttribute);
            // insert attribute term
            $dataChilds = $this->generateDataChild($attribute->id, $data);

            if (count($dataChilds) == 0) {
                throw new \Exception('Please fill child attribute');
            }
            if (count($dataChilds) > 0) {
                ProductAttributeTerm::insert($dataChilds);
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return $attribute;
    }

    /**
     * update data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function update(array $data)
    {
        $dataAttribute = [
            'name' => $data['name'],
        ];
        $attribute = $this->getAttribute();
        $dataChilds = $this->generateDataChild($attribute->id, $data, 'all');
        $childInsert = $dataChilds['insert'];
        $childUpdate = $dataChilds['update'];

        DB::beginTransaction();
        try {
            // insert attribute term
            if (count($childInsert) > 0) {
                ProductAttributeTerm::insert($childInsert);
            }

            // insert attribute term
            foreach ($childUpdate as $row) {
                ProductAttributeTerm::where('id', $row['id'])->update([
                    'name' => $row['name'],
                    'color_code' => $row['color_code'],
                    'hex_rgb' => $row['hex_rgb'],
                    'product_attribute_id' => $row['product_attribute_id'],
                ]);
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        // update attribute
        return $attribute->update($dataAttribute);
    }

    /**
     * update batch data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function updateBatch(array $data)
    {
        $names = $data['name'];
        $parent_id = $this->getParentId($data);

        try {
            foreach ($names as $key => $value) {
                $id = $data['id'][$key];
                $name = $data['name'][$key];
                $color_code = $data['color_code'] ?? null;
                $hex_rgb = $data['hex_rgb'] ?? null;

                ProductAttribute::where('id', $id)->update([
                    'name' => $name,
                    'parent_id' => $parent_id,
                    'color_code' => $color_code,
                    'hex_rgb' => $hex_rgb,
                ]);
            }

            return true;
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function delete()
    {
        $attribute = $this->getAttribute();

        $this->checkIfUsedInProduct();

        try {
            return $attribute->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteChild(ProductAttributeTerm $productAttributeTerm)
    {
        $this->checkIfUsedInProduct($productAttributeTerm);

        try {
            return $productAttributeTerm->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function checkIfUsedInProduct($attribute = null)
    {
        if ($attribute instanceof ProductAttributeTerm) {
            $product = MsProduct::whereJsonContains('product_attribute_header->terms', $attribute->id)->first();
            if ($product != null) {
                throw new \Exception('already used in the product module, product name (' . $product->name . ')');
            }
            return true;
        }

        if ($attribute == null) {
            $attribute = $this->getAttribute();
        }

        if ($attribute->childs->count() > 0) {
            $childs = $attribute->childs;
            foreach ($childs as $term) {
                $product = MsProduct::whereJsonContains('product_attribute_header->terms', $term->id)->first();
                if ($product != null) {
                    throw new \Exception('already used in the product module, product name (' . $product->name . ')');
                }
            }
        }
        $product = MsProduct::whereJsonContains('product_attribute_header->attributes', $attribute->id)->first();
        if ($product != null) {
            throw new \Exception('already used in the product module, product name (' . $product->name . ')');
        }

        return true;
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
        ];
    }

    public function generateDataChild(int $parent_id, array $data, string $type = 'insert')
    {
        if (isset($data['childs'])) {
            if (is_array($data['childs'])) {

                $childs = $data['childs'];
                $ids = isset($data['child_id']) ? $data['child_id'] : null;

                $insert = [];
                $update = [];
                foreach ($childs as $key => $name) {
                    // jika nama tidak kosong
                    if ($name != '') {
                        // jika ada input child_id maka update
                        if ($ids != null) {
                            // jika child id tidak nol maka update
                            if ($ids[$key] != '0') {
                                $update[] = [
                                    'id' => $ids[$key],
                                    'name' => $name,
                                    'color_code' => isset($data['color_code'][$key]) ? $data['color_code'][$key] : null,
                                    'hex_rgb' => isset($data['hex_rgb'][$key]) ? $data['hex_rgb'][$key] : null,
                                    'product_attribute_id' => $parent_id,
                                ];
                            } else {
                                // insert
                                $insert[] = [
                                    'name' => $name,
                                    'color_code' => isset($data['color_code'][$key]) ? $data['color_code'][$key] : null,
                                    'hex_rgb' => isset($data['hex_rgb'][$key]) ? $data['hex_rgb'][$key] : null,
                                    'product_attribute_id' => $parent_id,
                                ];
                            }
                        } else {
                            // insert
                            $insert[] = [
                                'name' => $name,
                                'color_code' => isset($data['color_code'][$key]) ? $data['color_code'][$key] : null,
                                'hex_rgb' => isset($data['hex_rgb'][$key]) ? $data['hex_rgb'][$key] : null,
                                'product_attribute_id' => $parent_id,
                            ];
                        }
                    }
                }
                if ($type == 'insert') {
                    return $insert;
                }
                if ($type == 'update') {
                    return $update;
                }
                return [
                    'insert' => $insert,
                    'update' => $update,
                ];
            }
        }

        return [];
    }

    public function getParentId(array $data)
    {
        $parent_id = null;
        if (isset($data['parent_id'])) {
            $category = ProductAttribute::where('id', $data['parent_id'])->first();
            if ($category) {
                $parent_id = $category->id;
            }
        }
        return $parent_id;
    }

    public function setAttribute(ProductAttribute $attribute)
    {
        $this->attribute = $attribute;
        return $this;
    }

    public function getAttribute()
    {
        return $this->attribute;
    }
}
