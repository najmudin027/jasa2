<?php

namespace App\Services\Product;

use App\Model\Master\MsProduct;
use App\Model\Master\MsProductModel;
use App\Model\Master\MsProductCategory;
use Illuminate\Database\QueryException;

class ProductModelService
{
    public $productModel;

    public $use_relation_check = true;

    /**
     * get list query
     *
     * @return App\Model\Master\MsProductModel;
     */
    public function list()
    {
        return MsProductModel::with('product_category', 'product_brand')->orderBy('id', 'desc');
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsProductModel
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return MsProductModel::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getProductModel()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        return $this->relasiCheck()->delete();
    }

    public function checkProductCategory(array $data)
    {
        $productCategory = MsProductCategory::where('id', $data['ms_product_category_id'])->first();

        if ($productCategory == null) {
            throw new \Exception('product category yg dipilih tidak ditemukan');
        }

        return $productCategory;
    }

    public function generateData(array $data)
    {
        $this->checkProductCategory($data);

        return [
            'name' => $data['name'],
            'product_brands_id' => $data['product_brands_id'],
            'ms_product_category_id' => $data['ms_product_category_id'],
            'model_code' => $data['model_code'],
        ];
    }

    public function relasiCheck()
    {
        $row = $this->getProductModel();

        if ($this->use_relation_check = true) {
            // cek ke produk model
            $product = MsProduct::where('product_model_id', $row->id)->first();
            if ($product != null) {
                throw new \Exception('already used in (product model) module, product name (' . $product->name . ')');
            }
        }

        return $row;
    }

    public function setProductModel(MsProductModel $productModel)
    {
        $this->productModel = $productModel;
        return $this;
    }

    public function getProductModel()
    {
        return $this->productModel;
    }
}
