<?php

namespace App\Services\Product;

use App\Model\Master\MsProduct as Product;
use App\Model\Product\ProductAttribute;
use App\Model\Product\ProductVarian;
use App\Model\Product\ProductVendor;
use Illuminate\Http\Request;

class ProductSearchService
{
    /**
     * get list product vendor
     *
     * @param int $product_id
     * 
     * @return \App\Model\Product\ProductVendor
     */
    public function getVendorList(int $product_id)
    {
        return ProductVendor::with('vendor')->where('ms_product_id', $product_id)
            ->get();
    }


    /**
     * get list vendor varian
     *
     * @param int $product_id
     * @param int $vendor_id
     * 
     * @return array
     */
    public function getVendorVarianList(int $product_id, int $vendor_id): array
    {
        $productVendor = ProductVendor::where('ms_product_id', $product_id)
            ->where('vendor_id', $vendor_id)
            ->first();

        $response = ProductVarian::where('product_vendor_id', $productVendor->id)
            ->with('attributes.term', 'attributes.attribute')
            ->get();

        $combination = [];
        $att = [];
        foreach ($response as $varian) {
            foreach ($varian->attributes as $pAttribute) {
                $combination[$pAttribute->attribute->id][$pAttribute->term->id] = $pAttribute;
            }
        }

        foreach ($combination as $attribute_id => $com) {
            $att[] = [
                'attribute' => ProductAttribute::find($attribute_id),
                'terms' => $com,
            ];
        }

        return $att;
    }

    /**
     * get one varian
     *
     * @param Request \Illuminate\Http\Request
     * 
     * @return \App\Model\Product\ProductVarian
     */
    public function getVarian(Request $request)
    {
        $productVendor = ProductVendor::where('ms_product_id', $request->product_id)
            ->where('vendor_id', $request->vendor_id)
            ->first();

        $varian = ProductVarian::where('product_vendor_id', $productVendor->id);

        foreach ($request->attribute as $key => $val) {
            $att_id = $request->attribute[$key];
            $term_id = $request->term[$key];
            $varian->where('variation->' . $att_id, $term_id);
        }

        return $varian->first();
    }

    /**
     * get one produk vendor
     *
     */
    public function findVendor($id)
    {
        return ProductVendor::with('product', 'vendor')->where('id', $id)
            ->first();
    }

    /**
     * get one varian
     */
    public function findVarian($id)
    {
        return ProductVarian::with([
            'product_vendor.vendor',
            'product_vendor.product',
            'product_varian_attribute.attribute',
            'product_varian_attribute.term',
        ])->where('id', $id)->first();
    }

    /**
     * get product
     *
     * @param string $sku
     * 
     * @return array
     */
    public function getProductBySKU(string $sku): array
    {
        $product = $this->findProductBySKU($sku);
        $productVendor = $this->findProductVendorBySKU($sku);
        $productVendorVarian = $this->findProductVendorVarianBySKU($sku);

        return [
            'product' => $product,
            'vendor' => $productVendor,
            'varian' => $productVendorVarian,
        ];
    }

    public function findProductBySKU(string $sku)
    {
        return Product::where('sku', $sku)->first();
    }

    public function findProductVendorBySKU(string $sku)
    {
        return  ProductVendor::where('sku', $sku)->get();
    }

    public function findProductVendorVarianBySKU(string $sku)
    {
        return ProductVarian::where('sku', $sku)->get();
    }
}
