<?php

namespace App\Services\Product;

use App\Model\Master\MsProduct;
use App\Model\Product\ProductAdditional;
use Illuminate\Database\QueryException;

class ProductAdditionalService
{
    public $productAdditional;

    /**
     * dipakai saat delete data
     * akan mengecek ke semua relasi 
     * pada model ini
     * untuk mencari jika model ini sudah di pakai di model lain
     * note: lumayan berat karna harus cek ke semua table
     */
    public $use_relation_check = true;

    /**
     * get list query
     * 
     * @return App\Model\Product\ProductAdditional;
     */
    public function list()
    {
        return ProductAdditional::query();
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Product\ProductAdditional
     * 
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return ProductAdditional::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getProductAdditional()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function delete()
    {
        return $this->relasiCheck()->delete();
    }

    /**
     * delete data
     * 
     * @param array ids
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function deleteById(array $ids)
    {
        try {
            return ProductAdditional::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'additional_code' => $data['additional_code'],
        ];
    }

    /**
     * check relasi saat delete data
     */
    public function relasiCheck()
    {
        $row = $this->getProductAdditional();

        if ($this->use_relation_check) {
            // cek ke produk
            $product = MsProduct::where('product_additionals_id', $row->id)->first();
            if ($product != null) {
                throw new \Exception('already used in product module, product name (' . $product->name . ')');
            }
        }

        return $row;
    }

    public function setProductAdditional(ProductAdditional $productAdditional)
    {
        $this->productAdditional = $productAdditional;
        return $this;
    }

    public function getProductAdditional()
    {
        return $this->productAdditional;
    }
}
