<?php

namespace App\Services;

use App\Model\Master\WebSetting;
use Illuminate\Database\QueryException;
use Spatie\Permission\Models\Permission;

class WebSettingService
{
    public $menu;

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Menu
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $menuData = [
            'app_name' => $data['app_name'],
            'image_logo' => $data['image_logo'],
            'image_icon' => $data['image_icon'],
        ];

        try {
            return WebSetting::create($menuData);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        $menuData = [
            'app_name' => $data['app_name'],
            'image_logo' => $data['image_logo'],
            'image_icon' => $data['image_icon'],
        ];

        $menu = $this->getMenu();

        try {
            return $menu->update($menuData);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function setMenu(Menu $menu)
    {
        $this->menu = $menu;
        return $this;
    }

    public function getMenu()
    {
        return $this->menu;
    }
}
