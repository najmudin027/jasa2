<?php

namespace App\Services\User;

use App\Model\PasswordReset;
use App\Notifications\PasswordResetRequestNotification;
use App\Notifications\PasswordResetSuccessNotification;
use App\Services\OtpService;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ResetPasswordService
{
    public $user;
    public $passwordReset;
    public $minute = 720;

    /**
     * request reset password
     *
     * @param array $data
     * 
     * @return \App\User
     * 
     * @throws \Exception
     */
    public function request(array $data)
    {
        // get password Reset token
        $passwordReset = $this->findUser($data)->createToken();

        // chcek jika berhasil createOrUpdate token
        if ($passwordReset) {

            // kirim notifikasi
            $this->sendNotification($passwordReset);

            return $this->getUser();
        }

        throw new \Exception('terjadi kesalahan');
    }

    /**
     * check valid token
     * 
     * @param string $token
     * 
     * @return bool
     */
    public function isValidToken(string $token): bool
    {
        // cari data
        $passwordReset = PasswordReset::where('token', $token)
            ->first();

        // jika data tidak ditemukan
        if ($passwordReset == null) {
            return false;
        }

        if ($passwordReset->otp_url !== null) {
            $otp = new OtpService();
            // otp check
            $check = $otp->verify($token, $passwordReset->otp_url);
        } else {
            // check token
            $check = Carbon::parse($passwordReset->updated_at)
                ->addMinutes($this->minute)
                ->isPast();
        }


        // jika lebih dari $this->minute menit
        if ($check) {
            $passwordReset->delete();
            return false;
        }

        $this->setPasswordReset($passwordReset);

        return true;
    }

    /**
     * ganti password
     * 
     * @param array $data
     * 
     * @return bool
     */
    public function changePassword(array $data)
    {
        $playload = [
            ['token', $data['token']],
            ['email', $data['email']]
        ];

        // find reset password
        $passwordReset = PasswordReset::where($playload)->firstOrFail();

        // check data
        if ($passwordReset == null) {
            throw new \Exception('invalid token');
        }

        DB::beginTransaction();
        try {
            // find user
            $user = User::where('email', $passwordReset->email)->firstOrFail();

            // update password
            $user->update([
                'password' => Hash::make($data['password'])
            ]);

            // delete password reset
            $passwordReset->delete();

            // commit db
            DB::commit();
        } catch (QueryException $ex) {
            // rollback db
            DB::rollback();

            throw new \Exception('terjadi kesalahan');
        };

        // send notification
        $user->notify(new PasswordResetSuccessNotification($passwordReset));

        return $user;
    }

    /**
     * create token
     * 
     * @return \App\Model\PasswordReset
     */
    public function createToken()
    {
        $user = $this->getUser();

        $passwordReset = false;

        if ($user->phone !== null) {
            $otp = (new OtpService())->create();

            $otp->setLabel($user->id);
            $token = $otp->now();
            $url = $otp->getProvisioningUri();

            // update or create
            $passwordReset = PasswordReset::updateOrCreate(
                ['phone' => $user->phone],
                [
                    'phone' => $user->phone,
                    'otp_url' => $url,
                    'token' => $token
                ]
            );
        }

        if ($user->email !== null) {
            // update or create
            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                [
                    'email' => $user->email,
                    'token' => Str::random(60)
                ]
            );
        }

        // set user
        $this->setUser($user);

        return $passwordReset;
    }

    /**
     * cari user
     * 
     * @return this
     * 
     * @throws \Exception
     */
    public function findUser($data)
    {
        // find user
        $user = User::where('email', $data['email'])
            ->orWhere('phone', $data['email'])
            ->first();

        // check user
        if ($user == null) {
            throw new \Exception('email / phone tidak ditemukan');
        }

        // set user
        $this->setUser($user);

        return $this;
    }

    /**
     * kirim notifikasi
     * 
     * @param \App\Model\PasswordReset $token
     * 
     * @return void
     */
    public function sendNotification(PasswordReset $passwordReset): void
    {
        // get user
        $user = $this->getUser();

        if ($user->phone !== null) {
            $otpService = (new OtpService());
            $otpService->period = 60 * 10;
            $otpService->sendSMS([
                'to' => $user->phone,
                'text' => $passwordReset->token,
            ]);
        }

        if ($user->email !== null) {
            // send notofication
            $user->notify(
                new PasswordResetRequestNotification($passwordReset->token)
            );
        }
    }


    # ====== getter setter ======

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setPasswordReset(PasswordReset $passwordReset)
    {
        $this->passwordReset = $passwordReset;
        return $this;
    }

    public function getPasswordReset()
    {
        return $this->passwordReset;
    }
}
