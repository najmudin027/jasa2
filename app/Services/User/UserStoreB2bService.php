<?php

namespace App\Services\User;


use App\User;
use DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Mail\SuccessRegisterEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserStoreB2bService
{
    public $role;

    /**
     * handle create data
     *
     * @param array $data
     *
     * @return \App\User
     *
     * @throws \Exception
     */
    public function handle(array $data)
    {
        $user = $this->create($data);
        return User::where('id', $user->id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\User
     *
     * @throws \Exception
     */

    public function createFromAdmin(array $data)
    {
        $phone1 = str_replace(' ', '', $data['phone1']);
        $phone2 = str_replace(' ', '', $data['phone2']);
        $phone3 = str_replace(' ', '', $data['phone3']);
        $password = $data['password'];
        $userData = [
            'name'      => $data['name'],
            'username'  => '-',
            'password'  => Hash::make($password),
            'email'     => $data['email'],
            'ms_company_id' => $data['ms_company_id'],
            'phone1'     => $phone1,
            'phone2'     => $phone2,
            'phone3'     => $phone3,
            'address'     => $data['address'],
            'status'    => 1,
            'api_token' => Str::random(80),
            'is_b2b_users' => 1
        ];

        DB::beginTransaction();
        try {

            $cek = User::where('email', $data['email'])->count();

            if($cek == 0){
                $user = User::create($userData);

                if(!empty($data['email'])){
                    Mail::to($data['email'])->send(new SuccessRegisterEmail($user->id,$password));
                    $response = [
                        'status'=>true,
                        'status_code'=>200,
                        'message'=>'success',
                        'email'=>'Sent Email register Email'
                    ];
                }else{
                    $response = [
                        'status'=>true,
                        'status_code'=>400,
                        'message'=>'error',
                        'email'=>'Failed Sent Email : register Email'
                    ];
                }
            }else{
                throw new \Exception('This Email Has Been Used');
            }
           
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new \Exception($e->getMessage());
        }

        return $user;
    }
}
