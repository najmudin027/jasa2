<?php

namespace App\Services\User;

use App\Model\Master\UserGroup;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserGroupService
{
    public $user_group;

    /**
     * get list query
     * 
     * @return App\Model\Master\UserGroup;
     */
    public function list()
    {
        return UserGroup::with('user');
    }

    /**
     * datatables data
     * 
     * @return \App\Model\Master\UserGroup
     */
    public function datatables()
    {
        $query = $this->list();
        return DataTables::of($query)->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return \App\Model\Master\UserGroup
     */
    public function select2(Request $request)
    {
        return UserGroup::where('property_name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Master\UserGroup
     * 
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return UserGroup::create($this->generateUserData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getUserGroup()->update($this->generateUserData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return $this->getUserGroup()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @param array $ids
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function deleteById(array $ids)
    {
        try {
            return UserGroup::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * generate data for insert or update
     * 
     * @return array
     */
    public function generateUserData(array $data): array
    {
        return [
            'property_name' => $data['property_name'],
            'business_type' => $data['business_type'],
            'user_id' => $data['user_id'],
        ];
    }

    public function setUserGroup(UserGroup $user_group)
    {
        $this->user_group = $user_group;
        return $this;
    }

    public function getUserGroup()
    {
        return $this->user_group;
    }
}
