<?php

namespace App\Services;

use OTPHP\TOTP;
use OTPHP\Factory;
use GuzzleHttp\Client;

class OtpService
{
    public $digits = 4;
    public $digest = 'sha1';
    public $period = 60 * 5;

    public function create()
    {
        $otp = TOTP::create(
            null,
            $this->period,
            $this->digest,
            $this->digits
        );

        return $otp;
    }

    public function verify($token, $url)
    {
        $token = str_replace(' ', '', $token);
        $otp = Factory::loadFromProvisioningUri($url);
        return $otp->verify($token);
    }

    public function sendSMS($data)
    {
        $basic  = new \Nexmo\Client\Credentials\Basic(env('NEXMO_KEY'), env('NEXMO_SECRET'));
        $client = new \Nexmo\Client($basic);

        $message = $client->message()->send([
            'to' => $data['to'],
            'from' => 'Nexmo',
            'text' => $data['text']
        ]);

        return $message;
    }

    public function viaNexmoCurl($data)
    {
        // send sms pake token
        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post('https://rest.nexmo.com/sms/json', [
            'form_params' => [
                'api_key' => env('NEXMO_KEY'),
                'api_secret' => env('NEXMO_SECRET'),
                'to' => $data['to'],
                'from' => 'NEXMO',
                'text' => $data['text']
            ]
        ]);
    }
}
