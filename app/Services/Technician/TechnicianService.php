<?php

namespace App\Services\Technician;

use DB;
use Auth;
use Storage;
use App\User;
use Illuminate\Http\Request;
use App\Model\Master\Curriculum;
use App\Model\Master\MsTechTeam;
use App\Model\Master\TeamInvite;
use Yajra\DataTables\DataTables;
use App\Model\Master\TeknisiInfo;
use App\Model\Technician\JobTitle;
use Spatie\Permission\Models\Role;
use App\Model\Master\MsPriceService;
use App\Model\Master\TeamTechnician;
use App\Model\Technician\Technician;
use Illuminate\Support\Facades\File;
use App\Model\Technician\JobExperience;
use Illuminate\Database\QueryException;
use App\Model\Master\DevelopmentProgram;
use App\Exceptions\SendErrorMsgException;
use App\Model\Master\TechnicianCurriculum;
use App\Model\Technician\JobTitleCategory;
use App\Services\Technician\JobExperienceService;
use Intervention\Image\ImageManagerStatic as Image;
use PhpParser\Node\Expr\FuncCall;

class TechnicianService
{
    public $technician;

    /**
     * get list query
     *
     * @return App\Model\Technician\JobTitle;
     */
    public static function list()
    {
        return Technician::with([
            'user.info',
            'user.badge',
            'price_services.service_type',
        ]);
    }

    public function listQuery()
    {
        return Technician::with([
            'user.info',
            'user.address.city',
            'user.badge',
            'price_services.service_type'
        ])
            ->select([
                'technicians.id',
                'technicians.user_id',
                DB::raw('SUM(ms_price_services.value) AS total_service_price'),
                DB::raw('COUNT(ms_price_services.id) AS jumlah_service_price'),
                DB::raw('COUNT(ratings.id) AS jumlah_rating'),
                DB::raw('SUM(ratings.value) AS total_rating'),
                DB::raw('COUNT(reviews.id) AS total_review'),
            ])
            ->leftJoin('ratings', 'ratings.technician_id', '=', 'technicians.id')
            ->leftJoin('reviews', 'reviews.technician_id', '=', 'technicians.id')
            ->leftJoin('ms_price_services', 'ms_price_services.technicians_id', '=', 'technicians.id')
            ->groupBy('technicians.id');
    }

    /**
     * datatables data
     *
     * @return \App\Model\Technician\JobTitle
     */
    public function datatables()
    {
        $query = Technician::with([
            'user.info',
            'user.badge'
        ])
            ->select([
                'technicians.id',
                'technicians.user_id',
                DB::raw('SUM(ms_price_services.value) AS total_service_price'),
                DB::raw('COUNT(ms_price_services.id) AS jumlah_service_price'),
                DB::raw('COUNT(ratings.id) AS jumlah_rating'),
                DB::raw('SUM(ratings.value) AS total_rating'),
                DB::raw('COUNT(reviews.id) AS total_review'),
            ])
            ->leftJoin('ratings', 'ratings.technician_id', '=', 'technicians.id')
            ->leftJoin('reviews', 'reviews.technician_id', '=', 'technicians.id')
            ->leftJoin('ms_price_services', 'ms_price_services.technicians_id', '=', 'technicians.id')
            ->groupBy('technicians.id');

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * JobExperience datatables data
     */
    public function experienceDatatables()
    {
        $technician = $this->getTechnician();

        $query = (new JobExperienceService())->list()->where('user_id', $technician->user_id);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function curriculumDatatables()
    {
        $technician = $this->getTechnician();

        $query = TechnicianCurriculum::with(['technician', 'curriculum'])
            ->where('technician_id', $technician->id)->get();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * PriceService datatables data
     */
    public function priceServiceDatatables()
    {
        $technician = $this->getTechnician();

        $query = MsPriceService::with(['product_group', 'service_type.symptom.services'])
            ->where('technicians_id', $technician->id);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * PriceService insert data
     */
    public function storePriceService($data)
    {
        $teknisi = Technician::where('id', $data['id'])->first();

        if ($teknisi == null) {
            throw new SendErrorMsgException('Please complete technician job first');
        }

        $priceService = MsPriceService::where('ms_services_types_id', $data['ms_services_types_id'])
            ->where('product_group_id', $data['product_group_id'])
            ->where('technicians_id', $teknisi->id)
            ->first();

        if ($priceService != null) {
            $priceService->update([
                'product_group_id' => $data['product_group_id'],
                'ms_services_types_id' => $data['ms_services_types_id'],
                'value'                => $data['value'],
                'commission'           => $data['commission'],
                'commission_value'     => $data['commission_value']

            ]);

            return $priceService;
        }

        return MsPriceService::create([
            'product_group_id' => $data['product_group_id'],
            'ms_services_types_id' => $data['ms_services_types_id'],
            'value'                => $data['value'],
            'technicians_id'       => $teknisi->id,
            'commission'           => $data['commission'],
            'commission_value'     => $data['commission_value']
        ]);
    }

    /**
     * price service delte data
     */
    public function priceServiceDestroy($id)
    {
        return MsPriceService::where('id', $id)->delete();
    }

    public function curriculumDestroy($id)
    {
        return TechnicianCurriculum::where('id', $id)->delete();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Technician\JobTitle
     */
    public function select2(Request $request)
    {
        return Technician::whereHas('user', function ($q) use ($request) {
            $q->where('name', 'like', '%' . $request->q . '%');
        })->limit(20)
            ->get();
    }

    /**
     * find data
     *
     * @param int $id
     *
     * @return \App\Model\Technician\JobTitle
     */
    public function find(int $id)
    {
        return Technician::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Technician\JobTitle
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        DB::beginTransaction();
        try {
            // insert data
            $this->generateDataTechnician($data);
            // commit db
            DB::commit();
        } catch (QueryException $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }
    }

    /**
     * create data job exp
     */
    public function createExperience(array $data)
    {
        $technician = $this->getTechnician();

        DB::beginTransaction();
        try {
            $JobTitleCategory = $this->createOrGetJobTitleCatgery($data['job_title_category_name']);

            $jobTitle = JobTitle::create([
                'user_id'               => $technician->user_id,
                'description'           => $data['job_title_description_ex'],
                'job_title_category_id' => $JobTitleCategory->id,
            ]);

            $JobExperience = JobExperience::create([
                'position'     => $data['position'],
                'company_name' => $data['company_name'],
                'period_start' => $data['period_start'],
                'period_end'   => $data['period_end'],
                'type'         => $data['type'],
                'user_id'      => $technician->user_id,
                'job_title_id' => $jobTitle->id,
            ]);

            // commit db
            DB::commit();
        } catch (QueryException $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $JobExperience;
    }

    /**
     * create data tech curriculum
     */
    public function createCurriculum(array $data)
    {
        $technician = $this->getTechnician();

        DB::beginTransaction();
        try {

            $curriculum = TechnicianCurriculum::create([
                'technician_id' => $technician->id,
                'curriculum_id' => $data['curriculum-select'],
            ]);

            // commit db
            DB::commit();
        } catch (QueryException $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $curriculum;
    }

    /**
     * create data info
     */
    public function createInfo($request)
    {
        $info = TeknisiInfo::where('user_id', $request->user_id)->first();

        $file_name = null;
        if ($request->hasFile('attachment')) {
            $image      = $request->file('attachment');
            $file_name   = time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(505, 505, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->stream(); // <-- Key point

            Storage::disk('local')->put('public/attachment/' . $file_name, $img);

            if ($info != null) {
                if ($info->attachment != null) {
                    $exists = Storage::disk('local')->has('/public/attachment/' . $info->attachment);
                    if ($exists) {
                        Storage::delete('/public/attachment/' . $info->attachment);
                    }
                }
            }
        }
        $data_info = [
            'user_id' => $request->user_id,
            'attachment' => $file_name,
            'no_identity' => $request->no_identity,
        ];
        DB::beginTransaction();
        try {
            if ($info == null) {
                $info = TeknisiInfo::create($data_info);
            } else {
                $update_devplan = Curriculum::whereHas('teknisiinfo', function ($q) use ($info) {
                    $q->where('ms_curriculum_id', $info->ms_curriculum_id);
                })->update(['curriculum_name' => $request->curriculum]);
                $info->update($data_info);
            }
            // commit db
            DB::commit();
        } catch (QueryException $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $info;
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function updateJobInfo(array $data)
    {
        $technician = $this->getTechnician();

        $JobTitleCategory = (new TechnicianProfileService)->createOrGetJobTitleCatgery($data['job_title_category_name']);

        DB::beginTransaction();
        try {
            if ($technician->job_title == null) {
                // data job title
                $jobTitle = JobTitle::create([
                    'user_id'               => $technician->user_id,
                    'description'           => $data['job_title_description'],
                    'job_title_category_id' => $JobTitleCategory->id,
                ]);

                $technician->update([
                    'job_title_id' => $jobTitle->id,
                    'skill'        => $data['skill'],
                ]);
            } else {
                // data job title
                $jobTitle = JobTitle::where('id', $technician->job_title_id)->update([
                    'description'           => $data['job_title_description'],
                    'job_title_category_id' => $JobTitleCategory->id,
                ]);

                $technician->update([
                    'skill' => $data['skill'],
                ]);
            }

            // commit db
            DB::commit();
        } catch (QueryException $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $jobTitle;
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function updateRekInfo(array $data)
    {
        $technician = $this->getTechnician();

        DB::beginTransaction();
        try {

            $technician->update([
                'rekening_bank_name' => $data['rekening_bank_name'],
                'rekening_number' => $data['rekening_number'],
                'rekening_name' => $data['rekening_name'],
            ]);
            // commit db
            DB::commit();
        } catch (QueryException $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $technician;
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $technician = $this->getTechnician();
        $user = $technician->user;
        DB::beginTransaction();
        try {
            MsTechTeam::where('technicians_id', $technician->id)->delete();
            TeamInvite::where('technicians_id', $technician->id)->delete();
            TeamTechnician::where('technicians_id', $technician->id)->delete();
            JobExperience::where('user_id', $technician->user_id)->delete();
            $technician->delete();
            JobTitle::where('user_id', $technician->user_id)->delete();

            if ($user != null) {
                $user->syncRoles(
                    Role::where('name', 'Customer')->get()
                );
            }
            // commit db
            DB::commit();
        } catch (QueryException $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return true;
    }

    /**
     * insert data teknisi
     */
    public function generateDataTechnician(array $data)
    {
        // data job title
        $jobTitle = JobTitle::create([
            'user_id'               => $data['user_id'],
            'description'           => $data['job_title_description'],
            'job_title_category_id' => $data['job_title_category_id'],
        ]);

        // data teknisi
        $teknisi = Technician::create([
            'user_id'      => $data['user_id'],
            'skill'        => $data['skill'],
            'job_title_id' => $jobTitle->id,
        ]);

        // data experience
        if (isset($data['experiences'])) {
            if (count($data['experiences']) > 0) {
                $experiences = $data['experiences'];
                foreach ($experiences as $experience) {
                    $JobTitleCategory = $this->createOrGetJobTitleCatgery($experience['job_title_category_name']);

                    $jobTitle = JobTitle::create([
                        'user_id'               => $data['user_id'],
                        'description'           => $experience['job_title_description'],
                        'job_title_category_id' => $JobTitleCategory->id,
                    ]);
                    JobExperience::create([
                        'position'     => $experience['position'],
                        'company_name' => $experience['company_name'],
                        'period_start' => $experience['period_start'],
                        'period_end'   => $experience['period_end'],
                        'type'         => $experience['type'],
                        'user_id'      => $data['user_id'],
                        'job_title_id' => $jobTitle->id,
                    ]);
                }
            }
        }

        $role = Role::where('name', 'Technician')->firstOrFail();
        $user = User::where('id', $data['user_id'])->firstOrFail();
        $user->assignRole($role);

        return $teknisi;
    }

    /**
     * insert / get data job title category
     */
    public function createOrGetJobTitleCatgery(string $name)
    {
        $JobTitleCategory = JobTitleCategory::where('name', $name)->first();
        if ($JobTitleCategory == null) {
            $JobTitleCategory = JobTitleCategory::create([
                'name' => $name,
            ]);
        }

        return $JobTitleCategory;
    }

    /**
     * get data teknisi by order
     */
    public static function getListBuOrder($order, $is_admin = null)
    {
        if ($is_admin) {
            $id_service_type = $order->service_type_id;
            $product_group_id = $order->product_group_id;
        } else {
            $id_service_type = $order->service_detail->map(function ($row) {
                return $row->services_type->id;
            });
        }

        $technicians = Self::list()
            ->whereHas('price_services', function ($query) use ($id_service_type, $product_group_id) {
                $query
                    ->where('product_group_id', $product_group_id)
                    ->whereIn('ms_services_types_id', $id_service_type);
            });


        if ($is_admin) {
            if ($order->customer_id != null) {
                $technicians = $technicians
                    ->where('status', 1)
                    ->whereHas('user', function ($x) use ($order) {
                        $x->where('name', 'like', '%' . $order->q . '%')->orWhere('email', 'like', '%' . $order->q . '%');
                    })
                    ->where('user_id', '!=', $order->customer_id)
                    ->limit(20)
                    ->get();
            } else {
                $technicians = $technicians
                    ->where('status', 1)
                    ->whereHas('user', function ($x) use ($order) {
                        $x->where('name', 'like', '%' . $order->q . '%')->orWhere('email', 'like', '%' . $order->q . '%');
                    })
                    ->limit(20)
                    ->get();
            }
        } else {
            $technicians = $technicians
                ->where('user_id', '!=', Auth::id())
                ->where('status', 1)
                ->paginate(4);
        }

        // return DB::getQueryLog();
        return $technicians;
    }

    public function setTechnician(Technician $technician)
    {
        $this->technician = $technician;
        return $this;
    }

    public function getTechnician()
    {
        return $this->technician;
    }
}
