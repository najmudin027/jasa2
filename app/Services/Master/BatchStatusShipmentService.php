<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Exceptions\HasChildException;
use Illuminate\Database\QueryException;
use App\Model\Master\BatchStatusShipment;

class BatchStatusShipmentService
{
    public $status;

    /**
     * get list query
     *
     * @return \App\Model\Master\BatchStatusShipment;
     */
    public function list()
    {
        return BatchStatusShipment::where('id', '!=', 3);
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\BatchStatusShipment
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }
    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\BatchStatusShipment
     */
    public function select2(Request $request)
    {
        return BatchStatusShipment::where('name', 'like', '%' . $request->q . '%')->where('id', '!=', 3)
            ->limit(20)
            ->get();
    }

    /**
     * find data
     *
     * @param int $id
     *
     * @return \App\Model\Master\BatchStatusShipment
     */
    public function find(int $id)
    {
        return BatchStatusShipment::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\BatchStatusShipment
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        if(isset($data['is_accepted'])) {
            $this->resetIsAccepted();
        }
        try {
            return BatchStatusShipment::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {

        if(isset($data['is_accepted'])) {
            $this->resetIsAccepted();
        }
        try {
            return $this->getStatus()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $status = $this->getStatus();

        if ($status->batch_shipment_history->count() > 0) {
            throw new HasChildException('tidak bisa delete sudah terpakai di History Shipment pada Status ini');
        }
        try {
            return $this->getStatus()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteById(array $ids)
    {
        try {
            return BatchStatusShipment::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * generate data
     *
     * @return array
     */
    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
            'is_accepted' => (isset($data['is_accepted']) ? 1:null)
        ];
    }

    public function setStatus(BatchStatusShipment $status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function resetIsAccepted() {
        return $reset = BatchStatusShipment::where('id', '>', 0)->update(['is_accepted' => 0]);
    }
}
