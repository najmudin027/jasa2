<?php

namespace App\Services\Master;

use App\Model\Master\GeneralSetting;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class GeneralSettingService
{
    public $general;

    /**
     * get list query
     *
     * @return App\Model\Master\GeneralSetting;
     */
    public function list()
    {
        return GeneralSetting::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\GeneralSetting
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\GeneralSetting
     */
    public function select2(Request $request)
    {
        return GeneralSetting::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\GeneralSetting
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return GeneralSetting::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getGeneralSetting()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getGeneralSetting()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return GeneralSetting::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
            'value' => $data['value'],
        ];
    }

    public function setGeneral(GeneralSetting $general)
    {
        $this->general = $general;
        return $this;
    }

    public function getGeneralSetting()
    {
        return $this->general;
    }
}
