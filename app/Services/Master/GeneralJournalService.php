<?php

namespace App\Services\Master;

use Carbon\Carbon;

use App\Helpers\ImageUpload;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Model\Master\GeneralJournal;
use Illuminate\Support\Facades\Auth;
use App\Model\Master\GeneralJournalLog;
use Illuminate\Database\QueryException;
use App\Model\Master\PartDataExcelStock;
use App\Model\Master\GeneralJournalDetail;
use App\Model\Master\PartDataExcelStockInventory;
use App\Services\Master\PartDataExcelStockService;
use App\Services\Master\PartDataExcelStockInventoryService;

class GeneralJournalService
{
    public $general_journal;

    /**
     * get list query
     *
     * @return App\Model\Master\GeneralJournal;
     */
    public function list()
    {
        return GeneralJournal::with('general_journal_detail');
    }

    public function datatables($request = null)
    {
        $query = $this->list();
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $query->whereHas('general_journal_detail', function ($q) use ($request) {
                $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
                $date_to = Carbon::parse($request->date_to)->format('Y-m-d');
                $q->where('date', '>=', $date_from)
                ->where('date', '<=', $date_to);
            });
        }
        $query = $query->orderBy('id', 'desc')->get();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\GeneralJournal
     *
     * @throws \Exception
     */
    public function create($row)
    {
        DB::beginTransaction();
        $stock_inventory_service = new PartDataExcelStockInventoryService();
        try {
            $insert_gj = GeneralJournal::create($this->generateData($row));
            foreach ($row['date'] as $key => $value) {
                // dd($value);


                $check_field = !empty($row['date'][$key]) && !empty($row['asc_id'][$key]) && !empty($row['category'][$key]) && ($row['quantity'][$key] != '') && ($row['debit'][$key] != '') && ($row['kredit'][$key] != '');
                if ($check_field) {
                    $path = GeneralJournalDetail::getImagePathUpload();
                    $filename = null;
                    if ($row->hasFile('filename.'.$key)) {
                        $image = (new ImageUpload)->upload($row->file('filename.'.$key), $path);
                        $filename = $image->getFilename();
                    }

                    $create_gjd = GeneralJournalDetail::create([
                        'general_journal_id' => $insert_gj->id,
                        'date' => Carbon::parse($row['date'][$key])->format('Y-m-d'),
                        'asc_id' => $row['asc_id'][$key],
                        'part_data_stock_inventories_id' => ($row['category'][$key] != 'operational' ? $row['part_data_stock_inventories_id'][$key] : null),
                        'category' => $row['category'][$key],
                        'description' => $row['description'][$key],
                        'quantity' => $row['quantity'][$key],
                        'debit' => $row['debit'][$key],
                        'kredit' => $row['kredit'][$key],
                        'balance' => $row['balance'][$key],
                        'filename' => $filename,
                    ]);

                    if($row['category'][$key] != 'operational') {
                        $data_history_inventory = [
                            'date' => Carbon::parse($create_gjd->date),
                            'pdesi_id_out' => null,
                            'pdesi_id_in' => $row['part_data_stock_inventories_id'][$key],
                            'amount' => $row['quantity'][$key],
                            'type_amount' =>  1,
                            'type_mutation' =>  0,
                            'general_journal_details_id' => $create_gjd->id,
                            'part_data_excel_id' => null
                        ];
                        $stock_inventory_service->historyLogProcess($data_history_inventory);
                        //$serpis->historyLogProcess(null, ($row['part_data_stock_id'][$key]), ($row['quantity'][$key]), 1, 0, $create_gjd->id, null);
                        $stock_inventory_service->increasePartDataStock(($row['part_data_stock_inventories_id'][$key]), ($row['quantity'][$key]));

                    }
                }
            }
            $this->logs($insert_gj, 1);
            DB::commit();
            return $insert_gj;
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }


    public function generateData($row)
    {
        $path = GeneralJournal::getImagePathUpload();
        // dd($path);
        $filename = null;
        if ($row->hasFile('image')) {
            $image = (new ImageUpload)->upload($row->file('image'), $path);
            $filename = $image->getFilename();
        }
        $data_gj = [
            "code" => $row['code'],
            "title" => $row['title'],
            "image" => $filename != null ? $filename : (isset($this->getData()->image) ? $this->getData()->image : null),
        ];
        return $data_gj;
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *GeneralJournalSer
     * @throws \Exception
     */
    public function updateX($data)
    {
        DB::beginTransaction();

        try {
            $update = $this->getData()->update($this->generateData($data));
            $this->updateDetails($data);
            $this->logs($this->getData(), 2);
            DB::commit();
            return $update;

        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteX()
    {
        $general_journal = $this->getData();
        DB::beginTransaction();

        try {
            $this->logs($general_journal, 3);
            $delete = $general_journal->delete();
            DB::commit();


        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\GeneralJournal
     *
     * @throws \Exception
     */

    public function setData(GeneralJournal $general_journal)
    {
        $this->general_journal = $general_journal;
        return $this;
    }

    public function getData()
    {
        return $this->general_journal;
    }

    public function deleteById(array $ids)
    {
        try {
            return GeneralJournal::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateCode()
    {
        $q          = GeneralJournal::count();
        $prefix     = 'GJ'; //prefix = GJ-000001
        $separator  = '-';
        $number     = 1; //format
        $new_number = sprintf("%06s", $number);
        $code       = $prefix . ($separator) . ($new_number);
        $gj_code   = $code;
        if ($q > 0) {
            $last     = GeneralJournal::orderBy('id', 'desc')->value('code');
            $last     = explode("-", $last);
            $gj_code = $prefix . ($separator) . (sprintf("%06s", $last[1] + 1));
        }
        return $gj_code;
    }

    public function logs($data, $type) { // type created/changed/deleted
        return $log = GeneralJournalLog::create([
            'general_journal_id' => $data->id,
            'code_journal' => $data->code,
            'user_id' => Auth::user()->id,
            'type' => $type,
        ]);

    }

    public function updateDetails($row)
    {

        $general_journal = GeneralJournal::with(['general_journal_detail'])
            ->where('id', $row->id)
            ->firstOrFail();
        $details_update = [];
        $details_insert = [];

        $details_id = $row->details_id;
        $date = $row->date;
        $asc_id = $row->asc_id;
        $category = $row->category;
        $part_data_stock_id = $row->part_data_stock_id;
        $description = $row->description;
        $quantity = $row->quantity;
        $debit = $row->debit;
        $kredit = $row->kredit;
        $balance = $row->balance;
        // loop request
        foreach ($details_id as $key => $value) {
            // check request
            $check_details = !empty($date[$key]) && !empty($asc_id[$key]) && !empty($category[$key]) && ($quantity[$key] != '') && ($debit[$key] != '') && ($kredit[$key] != '');

            if ($check_details) {
                // jika id tidak 0 == update
                if ($value != 0) {
                    $path = GeneralJournalDetail::getImagePathUpload();
                    $filename = null;
                    if ($row->hasFile('filename.'.$key)) {
                        $image = (new ImageUpload)->upload($row->file('filename.'.$key), $path);
                        $filename = $image->getFilename();
                    }
                    $gjd = GeneralJournalDetail::where('id', $value)->first('filename');
                    $details_update[] = [
                        'id' => $value,
                        'date' => Carbon::parse(($row['date'][$key]))->format('Y-m-d'),
                        'asc_id' => $row['asc_id'][$key],
                        'category' => $row['category'][$key],
                        'part_data_stock_inventories_id' => ($row['category'][$key] !== 'operational' ? $row['part_data_stock_inventories_id'][$key] : null),
                        'description' => $row['description'][$key],
                        'quantity' => $row['quantity'][$key],
                        'debit' => $row['debit'][$key],
                        'kredit' => $row['kredit'][$key],
                        'balance' => $row['balance'][$key],
                        'filename' => $filename != null ? $filename : $gjd->filename
                    ];
                    if ($gjd->filename != null) {
                        $file_path = storage_path('app/public/general-journal-detail/' . $gjd->filename);
                        if (file_exists($file_path)) {
                            unlink($file_path);
                        }
                    }


                } else { // jika id 0 == insert
                    $path = GeneralJournalDetail::getImagePathUpload();
                    $filename = null;
                    if ($row->hasFile('filename.'.$key)) {
                        $image = (new ImageUpload)->upload($row->file('filename.'.$key), $path);
                        $filename = $image->getFilename();
                    }
                    $details_insert[] = [
                        'general_journal_id' => $general_journal->id,
                        'asc_id' => $row['asc_id'][$key],
                        'date' => Carbon::parse(($row['date'][$key]))->format('Y-m-d'),
                        'category' => $row['category'][$key],
                        'part_data_stock_inventories_id' => ($row['category'][$key] !== 'operational' ? $row['part_data_stock_inventories_id'][$key] : null),
                        'description' => $row['description'][$key],
                        'quantity' => $row['quantity'][$key],
                        'debit' => $row['debit'][$key],
                        'kredit' => $row['kredit'][$key],
                        'balance' => $row['balance'][$key],
                        'filename' => $filename,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];

                }
            }
        }

        DB::beginTransaction();
        try {
            // update sparrepart
            $stock_inventory_service = new PartDataExcelStockInventoryService();
            if (count($details_update) > 0) {
                foreach ($details_update as $data) {
                    if($data['category'] !== 'operational') {
                        $stock_before_gjd = GeneralJournalDetail::where('id',$data['id'])->value('quantity');
                        if($data['quantity'] != $stock_before_gjd) {
                            $type_amount = 0;
                            $in = null;
                            $out = $data;
                            $amount = ($stock_before_gjd - $data['quantity']);
                            if ($data['quantity'] > $stock_before_gjd) {
                                $type_amount = 1;
                                $in = $data['part_data_stock_inventories_id'];
                                $out = null;
                                $amount = $data['quantity']-$stock_before_gjd;
                            }
                            $data_history_inventory = [
                                'date' => Carbon::parse($data['date'])->format('Y-m-d'),
                                'pdesi_id_out' => $out,
                                'pdesi_id_in' => $in,
                                'amount' =>  $amount,
                                'type_amount' =>  $type_amount,
                                'type_mutation' =>  1,
                                'general_journal_details_id' => $data['id'],
                                'part_data_excel_id' => null
                            ];
                            // return $data_history_inventory;
                            $stock_inventory_service->historyLogProcess($data_history_inventory);
                            // $pdes_service->historyLogProcess($out, $in, $data['quantity'], $type_amount, 4, $data['id'], null);
                        }
                        $stock_inventory_service->increasePartDataStockUpdate($data['part_data_stock_inventories_id'], $data['id'], $data['quantity']);
                    } else {
                        $stock_inventory_service->restoreStockIfOperational($data['id'], $data['quantity']);
                    }

                    GeneralJournalDetail::where('id', $data['id'])->update([
                        'date' => Carbon::parse($data['date'])->format('Y-m-d'),
                        'asc_id' => $data['asc_id'],
                        'category' => $data['category'],
                        'part_data_stock_inventories_id' => ($data['category'] !== 'operational' ? $data['part_data_stock_inventories_id'] : null),
                        'description' => $data['description'],
                        'quantity' => $data['quantity'],
                        'debit' => $data['debit'],
                        'kredit' => $data['kredit'],
                        'balance' => $data['balance'],
                        'filename' => $data['filename']
                    ]);

                }
            }

            // insert part_datas
            if (count($details_insert) > 0) {
                foreach($details_insert as $data) {
                    $gjd = GeneralJournalDetail::create([
                        'general_journal_id' => $data['general_journal_id'],
                        'date' => Carbon::parse($data['date'])->format('Y-m-d'),
                        'asc_id' => $data['asc_id'],
                        'category' => $data['category'],
                        'part_data_stock_inventories_id' =>  ($data['category'] !== 'operational' ? $data['part_data_stock_inventories_id'] : null),
                        'description' => $data['description'],
                        'quantity' => $data['quantity'],
                        'debit' => $data['debit'],
                        'kredit' => $data['kredit'],
                        'balance' => $data['balance'],
                        'filename' => $data['filename']
                    ]);

                    if($data['category'] !== 'operational') {
                        $data_history_inventory = [
                            'date' => $gjd->date,
                            'pdesi_id_out' => null,
                            'pdesi_id_in' => $data['part_data_stock_inventories_id'],
                            'amount' => $data['quantity'],
                            'type_amount' =>  1,
                            'type_mutation' =>  0,
                            'general_journal_details_id' => $gjd->id,
                            'part_data_excel_id' => null
                        ];
                        $stock_inventory_service->historyLogProcess($data_history_inventory);
                        // $serpis->historyLogProcess(null, $data['part_data_stock_id'], $data['quantity'], 1, 0, $gjd->id, null);
                        $stock_inventory_service->increasePartDataStock($data['part_data_stock_inventories_id'], $data['quantity']);
                    }
                }
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function getHppAverageAll($pdesi_id) {
        $data = GeneralJournalDetail::select([
                    DB::raw('SUM(general_journal_details.kredit) AS total_kredit'),
                    DB::raw('SUM(general_journal_details.quantity) AS total_quantity'),
                    'part_data_stock_inventories_id'
                ])
                ->join('ode_part_data_stock_inventories', 'ode_part_data_stock_inventories.id', '=', 'general_journal_details.part_data_stock_inventories_id')
                ->where('part_data_stock_inventories_id', $pdesi_id)
                ->where('category', 'material')
                ->groupBy('part_data_stock_inventories_id')->whereNotNull('part_data_stock_inventories_id');
        if($data->count() > 0 ) {
            $data =  $data->first();
            $nilai = ceil($data->total_kredit / $data->total_quantity);
        } else {
            $nilai = 0;
        }
        // if($data) {
        //     $sisa = $data->total_quantity - $out_stock;
        //     if($sisa <= 0) {
        //         $nilai = 0;
        //         $sisa = 0;
        //     } else {
        //         $nilai = ceil(($data->total_kredit - (($data->total_kredit / $data->total_quantity) * $out_stock)) / $sisa);
        //     }
        // }
        return $nilai;
    }

}
