<?php

namespace App\Services\Master;

use App\Helpers\ImageUpload;
use Illuminate\Http\Request;
use App\Model\Master\Services;
use App\Model\Master\MsSymptom;
use Yajra\DataTables\DataTables;
use Illuminate\Database\QueryException;

class ServicesService
{
    /**
     * dipakai saat delete data
     * akan mengecek ke semua relasi
     * pada model ini
     * untuk mencari jika model ini sudah di pakai di model lain
     * note: lumayan berat karna harus cek ke semua table
     */
    public $use_relation_check = true;

    public $Services;
    /**
     * get list query
     *
     * @return App\Model\Master\Services;
     */
    public function list()
    {
        return Services::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\Services
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\Services
     */
    public function select2(Request $request)
    {
        return Services::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\Services
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return Services::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {

            return $this->getServices()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete($id)
    {
        try {

            return $this->relasiCheck()->delete();
            // $name = Services::where('id', $id)->value('name');
            // $rand = rand(999999, 000000);
            // $update_name = Services::where('id', $id)->update(['name' => 'deleted-' . $name . '-' . $rand]);
            // return  $this->relasiCheck()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            $name = Services::where('id', $ids)->value('name');
            $rand = rand(999999, 000000);
            $update_name = Services::where('id', $ids)->update(['name' => 'deleted-' . $name . '-' . $rand]);
            return Services::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
            'slug' => $data['slug'],
            'images' => $data['images'],
        ];
    }

    public function setServices(Services $Services)
    {
        $this->Services = $Services;
        return $this;
    }

    public function getServices()
    {
        return $this->Services;
    }

    /**
     * check relasi saat delete data
     */
    public function relasiCheck()
    {
        $row = $this->getServices();

        if ($this->use_relation_check) {
            // cek ke produk
            $symptom = MsSymptom::where('ms_services_id', $row->id)->first();
            if ($symptom != null) {
                throw new \Exception('already used in service category module, service category name (' . $symptom->name . ')');
            }
        }

        return $row;
    }
}
