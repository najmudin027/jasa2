<?php

namespace App\Services\Master;

use App\Exceptions\SendErrorMsgException;
use App\Model\Master\BatchItem;
use App\Model\Master\Inventory;
use App\Model\Master\MsProduct;
use App\Model\Master\MsProductModel;
use App\Model\Product\ProductAdditional;
use App\Model\Product\ProductAttribute;
use App\Model\Product\ProductAttributeTerm;
use App\Model\Product\ProductCategoryCombine;
use App\Model\Product\ProductVarian;
use App\Model\Product\ProductVarianAttribute;
use App\Model\Product\ProductVendor;
use App\Services\Product\ProductImageService;
use DB;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductService
{
    public $product;

    /**
     * dipakai saat delete data
     * akan mengecek ke semua relasi 
     * pada model ini
     * untuk mencari jika model ini sudah di pakai di model lain
     * note: lumayan berat karna harus cek ke semua table
     */
    public $use_relation_check = true;

    /**
     * get list query
     *
     * @return App\Model\Master\MsProduct;
     */
    function list()
    {
        return MsProduct::with([
            'productCombineCategories.product_category',
            'product_images.gallery',
            'gallery',
            'status',
            'model',
            'additional',
            'productVendors.vendor',
            'productVendors.productVarians.productVarianAttributes.attribute',
            'productVendors.productVarians.productVarianAttributes.term',
        ]);
    }

    public function details()
    {
        $details = MsProduct::query();
    }

    public function searchDatatables(Request $request)
    {
        $query = $this->list();

        if (!empty($request->date_from) && !empty($request->date_to)) {
            $query->where('publish_at', '>=', $request->date_from)
                ->where('publish_at', '<=', $request->date_to);
        }

        if (!empty($request->product_category_id)) {
            $query->whereHas('product_category_combine', function ($q) use ($request) {
                $q->whereIn('product_category_id', explode(',', $request->product_category_id));
            });
        }
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsProduct
     *
     * @throws \Exception
     */

    public function create(array $data, $update = false)
    {
        $imgService = new ProductImageService;

        if (!isset($data['vendors'])) {
            throw new SendErrorMsgException('vendor belum dipilih');
        }

        DB::beginTransaction();
        // return $data;
        try {
            $insert_product = MsProduct::create($this->serializeDataProduct($data));

            // set product
            $this->setProduct($insert_product);
            // create product category
            $this->createOrUpdateProductCatgeory($data);

            // insert product images
            if (isset($data['product_images'])) {
                $imgService->setProduct($insert_product)
                    ->create($data['product_images']);
            }

            // insert product vendor
            $this->createProductVendor($data);

            DB::commit();
            return $data;
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        $product_data = $this->serializeDataProduct($data);

        $product    = $this->getProduct();
        $imgService = new ProductImageService;

        if (!isset($data['vendors'])) {
            throw new SendErrorMsgException('vendor belum dipilih');
        }

        try {
            // create product category
            $this->createOrUpdateProductCatgeory($data);

            // insert product images
            if (isset($data['product_images'])) {
                $imgService->setProduct($product)
                    ->deleteAndCreate($data['product_images']);
            }

            // insert product vendor
            $this->createProductVendor($data);

            // update product
            $product->update($product_data);
        } catch (QueryException $e) {
            throw new SendErrorMsgException($e->getMessage());
        }
    }

    public function updateVarian(array $data)
    {
        $this->deleteVarian($data);
        $this->create($data, true);
        return true;
    }

    public function deleteVarian(array $data)
    {
        $productVendor    = ProductVendor::where('ms_product_id', $data['id']);
        $productVendorId  = $productVendor->pluck('id')->toArray();
        $productVarian    = ProductVarian::whereIn('product_vendor_id', $productVendorId);
        $productVarianId  = $productVarian->pluck('id')->toArray();
        $productVarianAtt = ProductVarianAttribute::whereIn('product_varian_id', $productVarianId)->delete();
        $productVarianDel = $productVarian->delete();
        $productVendor    = $productVendor->delete();
    }

    public function deleteProductCategory($ms_product_id)
    {
        return ProductCategoryCombine::where('ms_product_id', $ms_product_id)->delete();
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        DB::beginTransaction();
        // return $data;
        try {
            $this->relasiCheck()->delete();
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return true;
    }

    public function checkModel(array $data)
    {
        $model = MsProductModel::where('id', $data['product_model_id'])->first();
        if ($model == null) {
            throw new SendErrorMsgException('product models yg dipilih tidak ditemukan');
        }

        return $model;
    }

    public function checkAdditional(array $data)
    {
        $additional = ProductAdditional::where('id', $data['product_additionals_id'])->first();
        if ($additional == null) {
            throw new SendErrorMsgException('product additional yg dipilih tidak ditemukan');
        }

        return $additional;
    }

    public function setProduct(MsProduct $product)
    {
        $this->product = $product;
        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function checkSlugDuplicate($slug)
    {
        $product = $this->getProduct();
        if ($product != null) {
            $product_slug = MsProduct::where('slug', '=', $slug)->where('id', '!=', $product->id)->count();
        } else {
            $product_slug = MsProduct::where('slug', '=', $slug)->count();
        }

        if ($product_slug > 0) {
            $count = $product_slug + 1;
            return $slug . '-' . $count;
        }

        return $slug;
    }

    public function generateJsonHeaderProduct($header_product)
    {
        $hasil      = [];
        $terms      = [];
        $attributes = [];
        foreach ($header_product as $row) {
            $attributes[] = (int) $row['attribute_id'];
            if (!isset($row['terms'])) {
                throw new SendErrorMsgException('Attribute Belum dipilih');
            }
            foreach ($row['terms'] as $term) {
                $terms[] = (int) $term;
            }
        }

        $hasil = [
            'variation'  => $header_product,
            'terms'      => $terms,
            'attributes' => $attributes,
        ];

        return json_encode($hasil);
    }

    public function serializeDataProduct(array $data)
    {
        $slug = $this->checkSlugDuplicate($data['product_slug']);

        $product_sku = $data['product_sku'];
        if (isset($data['vendors'])) {
            if (count($data['vendors']) > 0) {
                $product_sku = null;
            }
        }

        $product = [
            'name'                     => $data['name'],
            'product_model_id'         => $data['product_model_id'],
            'product_additionals_id'   => $data['product_additionals_id'],
            'galery_image_id'          => isset($data['galery_image_id']) ? $data['galery_image_id'] : null,
            'slug'                     => $slug,
            'sku'                      => $product_sku,
            'deskripsi'                => $data['product_description'],
            'product_attribute_header' => $this->generateJsonHeaderProduct($data['product_header_attribute']),
            'weight'                   => $data['product_weight'],
            'length'                   => $data['product_length'],
            'width'                    => $data['product_width'],
            'height'                   => $data['product_height'],
            'publish_at'               => $data['publish_at'],
            'product_status_id'        => $data['product_status_id'],
        ];

        return $product;
    }

    public function createOrUpdateProductCatgeory(array $data)
    {
        if (count($data['product_category_id']) == 0) {
            return;
        }

        $product = $this->getProduct();

        foreach ($data['product_category_id'] as $key => $val) {
            $findProductCombine = ProductCategoryCombine::where('ms_product_id', $product->id)
                ->where('product_category_id', $val)
                ->first();

            $product_category = [
                'ms_product_id'       => $product->id,
                'product_category_id' => $val,
            ];

            if ($findProductCombine == null) {
                ProductCategoryCombine::create($product_category);
            } else {
                $findProductCombine->update($product_category);
            }
        }

        return true;
    }

    public function createProductVendor(array $data)
    {
        $product = $this->getProduct();

        foreach ($data['vendors'] as $key_ven => $val_ven) {

            $product_vendor_sku = $val_ven['vendor_sku'];
            // if (isset($data['varians'])) {
            //     if (count($data['varians']) > 0) {
            //         $product_vendor_sku = null;
            //     }
            // }

            $product_vendor = [
                'sku'           => $product_vendor_sku,
                'ms_product_id' => $product->id,
                'vendor_id'     => $val_ven['vendor_id'],
                'vendor_code'   => $val_ven['vendor_code'],
                'stock'         => $val_ven['vendor_stock'],
                'part_code'     => $val_ven['part_code'],
                'brand_code'    => $val_ven['brand_code'],
                'product_code'  => $val_ven['product_code'],
            ];

            if ($val_ven['product_vendor_id'] == 0) {
                $productVendor = ProductVendor::create($product_vendor);
            } else {
                $productVendor = ProductVendor::where('id', $val_ven['product_vendor_id'])->first();
                if ($productVendor != null) {
                    $productVendor->update($product_vendor);
                }
            }

            $this->createProductVarian($val_ven, $productVendor);
        }

        return true;
    }

    public function createProductVarian(array $val_ven, $product_vendor)
    {
        if (!isset($val_ven['varians'])) {
            return;
        }

        if (count($val_ven['varians']) == 0) {
            return;
        }

        foreach ($val_ven['varians'] as $key_var => $val_var) {
            $varians = [
                'sku'               => $val_var['varian_sku'],
                'stock'             => $val_var['varian_stock'],
                'length'            => $val_var['varian_length'],
                'width'             => $val_var['varian_width'],
                'weight'            => $val_var['varian_weight'],
                'height'            => $val_var['varian_height'],
                'power'             => $val_var['varian_power'],
                'product_vendor_id' => $product_vendor->id,
            ];
            if ($val_var['product_varian_id'] == 0) {
                $productVarian = productVarian::create($varians);
            } else {
                $productVarian = productVarian::where('id', $val_var['product_varian_id'])->first();
                if ($productVarian != null) {
                    $productVarian->update($varians);
                }
            }

            $variation = [];
            if (isset($val_var['attributes']) && count($val_var['attributes']) > 0) {
                foreach ($val_var['attributes'] as $key_att => $val_att) {
                    $attributes = [
                        'product_attribute_id'      => $val_att['attribute_id'],
                        'product_attribute_term_id' => $val_att['term_id'],
                        'product_varian_id'         => $productVarian->id,
                    ];
                    $attr                   = ProductAttribute::where('id', $val_att['attribute_id'])->first();
                    $term                   = ProductAttributeTerm::where('id', $val_att['term_id'])->first();
                    $variation[$attr->id]   = $term->id;
                    $variation[$attr->name] = $term->name;

                    if ($val_att['product_varian_attribute_id'] == 0) {
                        $productVarianAttribute = ProductVarianAttribute::create($attributes);
                    } else {
                        $productVarianAttribute = ProductVarianAttribute::where('id', $val_att['product_varian_attribute_id'])->first();
                        if ($productVarianAttribute != null) {
                            $productVarianAttribute->update($attributes);
                        }
                    }
                }
            }

            $productVarian->update([
                'variation' => json_encode($variation),
            ]);
        }

        return true;
    }

    /**
     * check relasi saat delete data
     */
    public function relasiCheck()
    {
        $row = $this->getProduct();

        if ($this->use_relation_check) {
            // cek ke batch
            // $batchItem = BatchItem::with('batch')->where('product_id', $row->id)->first();
            // if ($batchItem != null) {
            //     throw new SendErrorMsgException('already used in (batch) module, batch no (' . $batchItem->batch->batch_no . ')');
            // }

            BatchItem::where('product_id', $row->id)->update([
                'product_id' => null,
                'product_vendor_id' => null,
                'product_varian_id' => null,
            ]);

            Inventory::where('product_id', $row->id)->update([
                'product_id' => null,
                'product_vendor_id' => null,
                'product_varian_id' => null,
            ]);
        }

        return $row;
    }
}
