<?php

namespace App\Services\Master;

use App\Exceptions\HasChildException;
use App\Model\Master\MsCity;
use App\Model\Master\MsDistrict;
use Illuminate\Database\QueryException;

class DistrictService
{
    public $district;

    /**
     * get list query
     *
     * @return App\Model\Master\MsDistrict;
     */
    public function list()
    {
        return MsDistrict::with('city');
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsDistrict
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $this->checkCity($data);

        $data = [
            'name'       => $data['name'],
            'ms_city_id' => $data['ms_city_id'],
        ];

        try {
            return MsDistrict::create($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        $this->checkCity($data);

        $data = [
            'name'       => $data['name'],
            'ms_city_id' => $data['ms_city_id'],
        ];

        $district = $this->getDistrict();

        try {
            return $district->update($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $district = $this->getDistrict();

        if ($district->villages->count() > 0) {
            throw new HasChildException('tidak bisa delete terdapat village pada districts ini');
        }

        try {
            return $district->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function checkCity(array $data)
    {
        $city = MsCity::where('id', $data['ms_city_id'])->firstOrFail();

        return $city;
    }

    public function setDistrict(MsDistrict $district)
    {
        $this->district = $district;
        return $this;
    }

    public function getDistrict()
    {
        return $this->district;
    }

    public function deleteById(array $ids)
    {
        try {
            return MsDistrict::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception('Terdapat Beberapa Kabupaten Pada Wilayah Ini');
        }
    }
}
