<?php

namespace App\Services\Master;

use App\Exceptions\SendErrorMsgException;
use App\Model\Master\Chat;
use App\Model\Master\ChatDetail;
use App\Model\Master\ChatMember;
use App\Model\Master\ChatMessage;
use App\Model\Master\Order;
use App\Model\Technician\Technician;
use App\Notifications\Chat\ReplyChatNotification;
use App\Services\NotifikasiService;
use App\Services\OrderService;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\Notification as Noty;

class ChatService
{
    /**
     * availabe
     * pusher|firebase
     */
    public $driver = 'firebase';

    public $chat;

    public $chats = [];
    /**
     * cari user yg mau di chat
     */
    public function search($request)
    {
        $chats = Chat::withCount('unread_message')
            ->whereHas('members_group', function ($query) use ($request) {
                $query->where('user_id', Auth::id());
            })
            ->where(function ($query) use ($request) {
                $query->where('room_no', 'like', '%' . $request->q . '%')
                    ->orWhereHas('members_group.user', function ($query) use ($request) {
                        $query->where('name', 'like', '%' . $request->q . '%');
                    })
                    ->orWhereHas('messages', function ($query) use ($request) {
                        $query->where('message', 'like', '%' . $request->q . '%');
                    });
            })
            ->with([
                'last_message',
                'members_group.user',
                'members_group' => function ($query) {
                    $query->where('user_id', '!=', Auth::id());
                }
            ])
            ->orderBy('updated_at', 'DESC')
            ->get();

        return view('admin.customer.chats._list_chat', [
            'chats' => $chats,
            'chat_id' => null,
        ])->render();
    }

    /**
     * bales chat
     */
    public function replyMobile($request)
    {
        $file_name = null;
        $file      = $request->file('file');
        if ($file) {
            $file_name = rand() . $file->getClientOriginalName();
            $file->storeAs('public/user/chat/', $file_name);
        }

        if ($file_name == null && empty($request->message)) {
            throw new SendErrorMsgException('message Required');
        }

        $chat = Chat::where('id', $request->chat_id)->firstOrFail();

        DB::beginTransaction();
        try {
            // storing tha message
            $message = ChatMessage::create([
                'message' => $request->message,
                'chat_id' => $chat->id,
                'user_id' => Auth::id(),
                'filename' => $file_name
            ]);

            $chat->update([
                'updated_at' => now()
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $message;
    }

    public function reply($request)
    {
        $file_name = null;
        $file      = $request->file('file');
        if ($file) {
            $file_name = rand() . $file->getClientOriginalName();
            $file->storeAs('public/user/chat/', $file_name);
        }

        if ($file_name == null && empty($request->message)) {
            throw new SendErrorMsgException('message Required');
        }

        $chat = Chat::with('members.user')
            ->where('id', $request->chat_id)
            ->firstOrFail();

        $member = ChatMember::where('chat_id', $chat->id)->where('user_id', Auth::id())->firstOrFail();
        $addr = Auth::user()->address;
        DB::beginTransaction();
        try {
            // storing tha message
            $message = ChatMessage::create([
                'message' => $request->message,
                'address_obj' => $request->message == 'share_map' ? json_encode($addr) : null,
                'chat_id' => $chat->id,
                'user_id' => Auth::id(),
                'filename' => $file_name
            ]);

            $chat->update([
                'updated_at' => now()
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        foreach ($chat->members as $member) {
            if (Auth::id() != $member->user_id) {
                $contentPenerima = view('admin.customer.chats._reply', [
                    'chat' => $message,
                    'auth' => $member->user,
                ])->render();

                if ($this->driver == 'pusher') {
                    \Notification::send(
                        $member->user,
                        new ReplyChatNotification($member->user, $message, $contentPenerima)
                    );
                }

                if ($this->driver == 'firebase') {
                    $dataPush = [
                        'type' => 'chat',
                        'title' => Auth::user()->name . " ( " .$chat->room_no. " )",
                        'body' => $request->message,
                        'count_unread' => Noty::unreadCount($member->user->id),
                        'reply' => $contentPenerima,
                        'id' => $message->chat_id,
                        'list' => '',
                    ];
                    if ($request->message == 'share_map') {
                        $dataPush = [
                            'type' => 'chat_map',
                            'title' => Auth::user()->name . " ( " .$chat->room_no. " )",
                            'body' => $request->message,
                            'count_unread' => Noty::unreadCount($member->user->id),
                            'reply' => $contentPenerima,
                            'id' => $message->chat_id,
                            'list' => '',
                            'address' => [
                                'address' => $addr->address,
                                'latitude' => $addr->latitude,
                                'longitude' => $addr->longitude,
                            ]
                        ];
                    }


                    (new NotifikasiService)->firebasePushNotifikasi($member->user, $dataPush);
                }
            }
        }


        $content = view('admin.customer.chats._reply', [
            'chat' => $message,
            'auth' => Auth::user(),
        ])->render();

        return [
            'message' => $message,
            'reply_content' => $content,
            'list_chat_content' => '',
            'driver' => $this->driver
        ];
    }

    public function replySebelumnya($request)
    {
        $file_name = null;
        $file      = $request->file('file');
        if ($file) {
            $file_name = rand() . $file->getClientOriginalName();
            $file->storeAs('public/user/chat/', $file_name);
        }

        if ($file_name == null && empty($request->message)) {
            throw new SendErrorMsgException('message Required');
        }
        DB::beginTransaction();
        try {
            // buat room barouzo
            if ($request->chat_id == null) {
                $chat = Chat::create([
                    'room_no' => time()
                ]);
            } else {
                $chat = Chat::where('id', $request->chat_id)->firstOrFail();
            }

            // masukan membrazou ke dalum rooum
            $member = ChatMember::where('chat_id', $chat->id)->where('user_id', Auth::id())->first();
            if ($member == null) {
                ChatMember::create([
                    'chat_id' => $chat->id,
                    'user_id' => Auth::id(),
                ]);
            }
            $member2 = ChatMember::where('chat_id', $chat->id)->where('user_id', $request->user_id)->first();
            if ($member2 == null) {
                ChatMember::create([
                    'chat_id' => $chat->id,
                    'user_id' => $request->user_id,
                ]);
            }

            // storing tha message
            $message = ChatMessage::create([
                'message' => $request->message,
                'chat_id' => $chat->id,
                'user_id' => Auth::id(),
                'filename' => $file_name
            ]);

            $chat->update([
                'updated_at' => now()
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        $penerima = User::where('id', $request->user_id)->first();

        $command = $this->commandChat(Auth::id(), $request->message, $message);
        if ($command != null) {
            $content = $command['content'];
            $content2 = $command['content2'];
        } else {
            $content = view('admin.customer.chats._reply', [
                'chat' => $message,
                'auth' => Auth::user(),
            ])->render();

            $content2 = view('admin.customer.chats._reply', [
                'chat' => $message,
                'auth' => $penerima,
            ])->render();
        }


        $list_chat = ''; //$this->getListChatContent(0);

        \Notification::send($penerima, new ReplyChatNotification($penerima, $message, $content2, $list_chat));

        return [
            'message' => $message,
            'reply_content' => $content,
            'list_chat_content' => $list_chat
        ];
    }

    /**
     * isi chat as customer / teknisi
     */
    public function detail($id)
    {
        $member = ChatMember::where('chat_id', $id)->where('user_id', Auth::id())->first();
        if ($member == null) {
            return '';
        }

        // update read at
        ChatMessage::where('chat_id', $id)
            ->where('user_id', '!=', Auth::id())->update([
                'read_at' => now()
            ]);

        $chats = ChatMessage::with('user')->where('chat_id', $id)->orderBy('created_at', 'desc')->paginate(10);
        $data_chat = $chats;

        $chats = $chats->sortBy('created_at')->groupBy(function ($item) {
            return $item->created_at->format('d/m/Y');
        });

        $content = view('admin.customer.chats.detail', [
            'chats' => $chats,
            'auth' => Auth::user(),
        ])->render();

        return [
            'content' => $content,
            'data' => $data_chat
        ];
    }

    /**
     * isi chat as customer / teknisi
     * but as json
     */
    public function detailJson($id)
    {
        $member = ChatMember::where('chat_id', $id)->where('user_id', Auth::id())->first();
        if ($member == null) {
            return null;
        }

        // update read at
        ChatMessage::where('chat_id', $id)
            ->where('user_id', '!=', Auth::id())->update([
                'read_at' => now()
            ]);

        $chats = ChatMessage::with('user')->where('chat_id', $id)->orderBy('created_at', 'desc')->paginate(7);

        // $chats->sortBy('created_at')->groupBy(function ($item) {
        //     return $item->created_at->format('d/m/Y');
        // });

        return $chats;
    }

    /**
     * isi chat as admin
     */
    public function detailAdmin($id, $user_id)
    {
        $member = ChatMember::where('chat_id', $id)->where('user_id', $user_id)->first();
        if ($member == null) {
            return '';
        }

        $chats = ChatMessage::with('user')->where('chat_id', $id)->orderBy('created_at', 'desc')->paginate(7);
        $data = $chats;
        $chats = $chats->sortBy('created_at')->groupBy(function ($item) {
            return $item->created_at->format('d/m/Y');
        });

        $content = view('admin.customer.chats.detail', [
            'chats' => $chats,
            'auth' => User::where('id', $user_id)->first(),
        ])->render();

        return [
            'content' => $content,
            'data' => $data
        ];
    }

    /**
     * list chat
     */
    public function getListChatContent($chat_id)
    {
        $chats = Chat::withCount('unreads')
            ->whereHas('members_group', function ($query) {
                $query->where('user_id', Auth::id());
            })
            ->with([
                'last_message',
                'members_group.user',
                'members_group' => function ($query) {
                    $query->where('user_id', '!=', Auth::id());
                }
            ])
            ->orderBy('updated_at', 'DESC')
            ->get();

        return view('admin.customer.chats._list_chat', [
            'chats' => $chats,
            'chat_id' => $chat_id,
        ])->render();
    }

    /**
     * auto reply chat order status
     */
    public function sendOrderHistory($user_to, $user_from, $order_id)
    {
        $user_ids = [$user_to, $user_from];
        $order = Order::with('order_status', 'symptom_code', 'repair_code', 'detail')->where('id', $order_id)->first();
        if ($order != null) {
            $order->update([
                'notifikasi_type' => 1
            ]);
        }
        $chat = Chat::where('room_no', $order->code)->first();
        if ($chat == null) {
            $chat = Chat::create([
                'room_no' => $order->code,
                'order_id' => $order->id,
                'is_has_order' => 1,
            ]);
        }

        foreach ($user_ids as $key => $val) {
            // masukan membrazou ke dalum rooum
            $member = ChatMember::where('chat_id', $chat->id)->where('user_id', $val)->first();
            if ($member == null) {
                ChatMember::create([
                    'chat_id' => $chat->id,
                    'user_id' => $val,
                ]);
            }
        }

        $message = (new OrderService())->getStatusDesc($order);
        // storing tha message
        $message = ChatMessage::create([
            'can_action' => 1,
            'message' => $message['text'],
            'order_obj' => json_encode($order),
            'chat_id' => $chat->id,
            'user_id' => $user_from,
        ]);

        $content = view('admin.customer.chats._reply', [
            'chat' => $message,
            'auth' => Auth::user(),
        ])->render();

        $penerima = User::where('id', $user_to)->first();

        $content2 = view('admin.customer.chats._reply', [
            'chat' => $message,
            'auth' => $penerima,
        ])->render();

        $list_chat = '';

        // \Notification::send($penerima, new ReplyChatNotification($penerima, $message, $content2, $list_chat));

        return [
            'reply_content' => $content,
            'list_chat_content' => $list_chat,
            'order' => $order,
        ];
    }

    public function sendOrderHistoryBackUp($user_to, $user_from, $order_id)
    {
        $user_ids = [$user_to, $user_from];

        $chatMember = ChatMember::with('chat')->whereIn('user_id', $user_ids)
            ->groupBy('chat_id')
            ->havingRaw('count(user_id) = ' . count($user_ids))
            ->first();

        if ($chatMember == null) {
            $chat = Chat::create([
                'room_no' => time()
            ]);

            foreach ($user_ids as $key => $val) {
                // masukan membrazou ke dalum rooum
                $member = ChatMember::where('chat_id', $chat->id)->where('user_id', $val)->first();
                if ($member == null) {
                    ChatMember::create([
                        'chat_id' => $chat->id,
                        'user_id' => $val,
                    ]);
                }
            }
        } else {
            $chat = $chatMember->chat;
        }

        $order = Order::with('order_status', 'symptom_code', 'repair_code', 'detail')->where('id', $order_id)->first();
        $message = (new OrderService())->getStatusDesc($order);
        // storing tha message
        $message = ChatMessage::create([
            'can_action' => 1,
            'message' => $message['text'],
            'order_obj' => json_encode($order),
            'chat_id' => $chat->id,
            'user_id' => $user_from,
        ]);

        $content = view('admin.customer.chats._reply', [
            'chat' => $message,
            'auth' => Auth::user(),
        ])->render();

        $penerima = User::where('id', $user_to)->first();

        $content2 = view('admin.customer.chats._reply', [
            'chat' => $message,
            'auth' => $penerima,
        ])->render();

        $list_chat = $this->getListChatContent($user_to);

        \Notification::send($penerima, new ReplyChatNotification($penerima, $message, $content2, $list_chat));

        return [
            'reply_content' => $content,
            'list_chat_content' => $list_chat,
            'order' => $order,
        ];
    }

    /**
     * chat command
     */
    public function commandChat($user_id, $message, ChatMessage $chatMessage)
    {
        $action = explode(':', $message);
        if (count($action) != 2) {
            return null;
        }

        $command_list = [
            'technician' => [
                'info' => function ($user_id) use ($message, $chatMessage) {
                    $order = Order::with('detail')->where('users_id', $user_id)->first();
                    $teknisi = Technician::with('user')->where('id', $order->detail->technicians_id)->first();

                    $msg =  view('admin.customer.chats._components._chat_command_teknisi_info', [
                        'message' => $message,
                        'teknisi' => $teknisi,
                    ])->render();

                    $chatMessage->message = $msg;
                    $chatMessage->is_html = 1;
                    $chatMessage->save();

                    $content = view('admin.customer.chats._components._chat_command_reply', [
                        'chat' => $chatMessage,
                        'auth' => Auth::user(),
                    ])->render();

                    $content2 = view('admin.customer.chats._components._chat_command_reply', [
                        'chat' => $chatMessage,
                        'auth' => User::where('id', $user_id)->first(),
                    ])->render();

                    return [
                        'content' => $content,
                        'content2' => $content2,
                    ];
                }
            ],
            'order' => [
                'list' => function ($user_id) use ($chatMessage, $message) {
                    $msg =  view('admin.customer.chats._components._chat_command_order_list', [
                        'message' => $message,
                        'orders' => Order::where('users_id', $user_id)->with('order_status')->orderBy('id', 'DESC')->limit(5)->get(),
                    ])->render();

                    $chatMessage->message = $msg;
                    $chatMessage->is_html = 1;
                    $chatMessage->save();

                    $content = view('admin.customer.chats._components._chat_command_reply', [
                        'chat' => $chatMessage,
                        'auth' => Auth::user(),
                    ])->render();

                    $content2 = view('admin.customer.chats._components._chat_command_reply', [
                        'chat' => $chatMessage,
                        'auth' => User::where('id', $user_id)->first(),
                    ])->render();

                    return [
                        'content' => $content,
                        'content2' => $content2,
                    ];
                }
            ]
        ];

        $command = $action[0];
        $command_param = $action[1];
        if (isset($command_list[$command])) {
            if (isset($command_list[$command][$command_param])) {
                return $command_list[$command][$command_param]($user_id);
            }
        }

        return null;
    }

    /**
     * get user list chat
     */
    public function users($user_id, $pagination = 0)
    {
        if ($pagination == 0) {
            $chats = Chat::with([
                'last_message',
                'members_group.user',
                'members_group' => function ($query) use ($user_id) {
                    $query->where('user_id', '!=', $user_id);
                }
            ])
                ->withCount('unread_message')
                ->whereHas('members_group', function ($query) use ($user_id) {
                    $query->where('user_id', $user_id);
                })
                ->orderBy('updated_at', 'DESC')
                ->get();
        } else {
            $chats = Chat::with([
                'last_message',
                'members_group.user',
                'members_group' => function ($query) use ($user_id) {
                    $query->where('user_id', '!=', $user_id);
                }
            ])
                ->withCount('unread_message')
                ->whereHas('members_group', function ($query) use ($user_id) {
                    $query->where('user_id', $user_id);
                })
                ->orderBy('updated_at', 'DESC')
                ->paginate($pagination);
        }


        return $chats;
    }

    /**
     * get user list chat
     */
    public function searchUserInMyChat($request)
    {
        $chats = Chat::withCount('unread_message')
            ->whereHas('members_group', function ($query) use ($request) {
                $query->where('user_id', Auth::id());
            })
            ->whereHas('members_group.user', function ($query) use ($request) {
                $query->Where('name', 'like', '%' . $request->q . '%');
            })
            ->with([
                'last_message',
                'members_group.user',
                'members_group' => function ($query) {
                    $query->where('user_id', '!=', Auth::id());
                }
            ])
            ->orderBy('updated_at', 'DESC')
            ->get();

        return $chats;
    }

    public function notifGaransi($garansi)
    {
        $chat_room = 'Claim-waranty' . '-' . $garansi->order->code;
        $chat = Chat::create([
            'room_no' => $chat_room,
            'order_id' => $garansi->order->id,
            'is_has_order' => 1,
        ]);

        $user_ids = [Auth::id(), $garansi->order->detail->teknisi->user->id];
        foreach ($user_ids as $key => $val) {
            // masukan membrazou ke dalum rooum
            $member = ChatMember::where('chat_id', $chat->id)->where('user_id', $val)->first();
            if ($member == null) {
                ChatMember::create([
                    'chat_id' => $chat->id,
                    'user_id' => $val,
                ]);
            }
        }

        // storing tha message
        $message = ChatMessage::create([
            'can_action' => 1,
            'message' => 'claim-waranty',
            'order_obj' => json_encode($garansi->order),
            'chat_id' => $chat->id,
            'user_id' => Auth::id(),
        ]);

        return $message;
    }
}
