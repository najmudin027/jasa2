<?php

namespace App\Services\Master;

use App\User;
use App\Model\Master\Vendor;
use App\Exceptions\HasChildException;
use Illuminate\Database\QueryException;

class VendorService
{
    public $vendor;

    /**
     * get list query
     *
     * @return App\Model\Master\Vendor;
     */
    public function list()
    {
        return Vendor::with('user');
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\Vendor
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return Vendor::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getVendor()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $vendor = $this->getVendor();
        if ($vendor->product_vendor->count() > 0) {
            throw new HasChildException('tidak bisa delete sudah terpakai di Product Vendor pada Vendor ini');
        }
        try {
            return $this->getVendor()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return Vendor::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function checkUser(array $data)
    {
        if (empty($data['user_id'])) {
            return;
        }
        $user = User::where('id', $data['user_id'])->first();

        if ($user == null) {
            throw new \Exception('user yg dipilih tidak ditemukan');
        }

        return $user;
    }

    public function generateData(array $data)
    {
        $this->checkUser($data);

        $hasil = [
            'name' => $data['name'],
            'code' => $data['code'],
            'office_email' => $data['office_email'],
            'is_business_type' => $data['is_business_type'],
            'user_id' => empty($data['user_id']) ? null : $data['user_id'],
        ];

        return $hasil;
    }

    public function setVendor(Vendor $vendor)
    {
        $this->vendor = $vendor;
        return $this;
    }

    public function getVendor()
    {
        return $this->vendor;
    }
}
