<?php

namespace App\Services\Master;

use App\Model\Master\MsDistrict;
use App\Model\Master\MsVillage;
use Illuminate\Database\QueryException;

class VillageService
{
    public $village;

    /**
     * get list query
     *
     * @return App\Model\Master\MsVillage;
     */
    public function list()
    {
        return MsVillage::with('district.city.province.country');
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsVillage
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $this->checkDistrik($data);

        $data = [
            'name' => $data['name'],
            'ms_district_id' => $data['ms_district_id'],
        ];

        try {
            return MsVillage::create($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        $this->checkDistrik($data);

        $data = [
            'name' => $data['name'],
            'ms_district_id' => $data['ms_district_id'],
        ];

        $village = $this->getVillage();

        try {
            return $village->update($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $village = $this->getVillage();

        try {
            return $village->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function checkDistrik(array $data)
    {
        $distrik = MsDistrict::where('id', $data['ms_district_id'])->firstOrFail();

        if ($distrik == null) {
            throw new \Exception('distrik yg dipilih tidak ditemukan');
        }

        return $distrik;
    }

    public function setVillage(MsVillage $village)
    {
        $this->village = $village;
        return $this;
    }

    public function getVillage()
    {
        return $this->village;
    }

    public function deleteById(array $ids)
    {
        try {
            return MsVillage::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
