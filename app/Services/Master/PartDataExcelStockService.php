<?php

namespace App\Services\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\Master\ASCExcel;
use App\Model\Master\Services;
use Yajra\DataTables\DataTables;
use App\Model\Master\PDESHistory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use App\Model\Master\PartDataExcelStock;
use Illuminate\Database\Eloquent\Collection;
use App\Model\Master\PartDataExcelStockInventory;


class PartDataExcelStockService
{
    public $pdes;
    /**
     * get list query
     *
     * @return App\Model\Master\PartDataExcelStock;
     */
    public function list()
    {
        return PartDataExcelStock::orderBy('id', 'DESC');
    }

    /**
     * datatables data
     *
     * @return \App\Model\Product\PartDataExcelStock
     */
    public function datatables($request = null)
    {
        $query = $this->list();

        if(!empty($request->date_from) && !empty($request->date_to)) {
            $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
            $date_to = Carbon::parse($request->date_to)->addDays(1)->format('Y-m-d');
            $query->whereBetween('created_at', array($date_from, $date_to));
        }

        $query = $query->get();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Product\PartDataExcelStock
     */
    public function select2($search_by, $asc_id=null, $filter_stock=null, Request $request)
    {
        $q =  PartDataExcelStock::with('get_price')->where($search_by, 'like', '%' . $request->q . '%');
        if ($asc_id != null) {
            $q->where('asc_id', $asc_id);
        }
        if($filter_stock == 'filter_petty') {
            return $q = $q->get()->filter(function($attr) {
                return $attr->stock_available !== null;
            });
        } else if($filter_stock == 'filter_order') {
            return $q = $q->get()->filter(function($attr) {
                return $attr->stock_available > 0;
            });
        } else {
            return $q = $q->get();
        }
    }

    public function select2B2B()
    {
        $q =  PartDataExcelStock::with('get_price');
        
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\PartDataExcelStock
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            $create = PartDataExcelStock::create($this->generateData($data));
            return $create;
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getPartDataExcelStock()->update($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getPartDataExcelStock()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return PartDataExcelStock::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'asc_id' => $data['asc_id'],
            'asc_name' => $data['asc_name'],
            'code_material' => $data['code_material'],
            'part_description' => $data['part_description'],
        ];
    }

    public function setPartDataExcelStock(PartDataExcelStock $pdes)
    {
        $this->pdes = $pdes;
        return $this;
    }

    public function getPartDataExcelStock()
    {
        return $this->pdes;
    }

}
