<?php

namespace App\Services\Master;

use App\Model\Master\DevelopmentProgram;
use Illuminate\Database\QueryException;

class DevelopmentProgramService
{
    public $developmentProgram;

    /**
     * get list query
     *
     * @return App\Model\Master\DevelopmentProgram;
     */
    public function list()
    {
        return DevelopmentProgram::query();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\DevelopmentProgram
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $developmentCreate = [
            'program_name' => $data['program_name'],
            'desc' => $data['desc'],
        ];

        try {
            return DevelopmentProgram::create($developmentCreate);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getDevProgram()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $developmentProgram = $this->getDevProgram();

        try {
            return $developmentProgram->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function setDevelopmentProgram(DevelopmentProgram $developmentProgram)
    {
        $this->developmentProgram = $developmentProgram;
        return $this;
    }

    public function getDevProgram()
    {
        return $this->developmentProgram;
    }

    public function generateData(array $data)
    {
        return [
            'program_name' => $data['program_name'],
            'desc' => $data['desc'],
        ];
    }
}
