<?php

namespace App\Services\Master;

use App\User;
use App\Model\Master\Educations;
use Illuminate\Database\QueryException;
use Auth;

class EducationService
{
    public $education;

    /**
     * get list query
     * 
     * @return App\Model\Master\Educations;
     */
    public function list()
    {
        $userLogin = Auth::user()->id;
        return Educations::where('user_id', $userLogin);
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Master\Educations
     * 
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return Educations::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function update(array $data)
    {
        try{
            return $this->getEducation()->update($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function delete()
    {
        $education = $this->getEducation();

        try {
            return $education->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return Educations::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function checkUser(array $data)
    {
        $user = User::where('id', $data['user_id'])->first();

        if ($user == null) {
            throw new \Exception('user yg dipilih tidak ditemukan');
        }

        return $user;
    }

    public function setEducation(Educations $education)
    {
        $this->education = $education;
        return $this;
    }

    public function getEducation()
    {
        return $this->education;
    }

    public function generateData(array $data)
    {
        $user =  auth()->user();

        $data = [
            'institution'   => $data['institution'],
            'place'         => $data['place'],
            'education_start_date'  => $data['education_start_date'],
            'education_end_date'    => $data['education_end_date'],
            'type'          => $data['type'],
            'major'         => $data['major'],
            'user_id' => isset($data['user_id']) ? $data['user_id'] : Auth::id(),
        ];

        return array_merge($data);
    }
}
