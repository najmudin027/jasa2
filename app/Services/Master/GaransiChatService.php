<?php

namespace App\Services\Master;

use App\Exceptions\SendErrorMsgException;
use App\User;
use Carbon\Carbon;
use App\Model\Master\Order;
use App\Model\Master\GaransiChat;
use Illuminate\Support\Facades\DB;
use App\Model\Master\ServiceDetail;
use App\Model\Technician\Technician;
use Illuminate\Support\Facades\Auth;
use App\Model\Master\GaransiChatDetail;
use Illuminate\Database\QueryException;
use App\Model\Master\TechnicianSchedule;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\AdminController;
use App\Model\Master\GeneralSetting;
use App\Model\Master\HistoryOrder;
use App\Model\Master\OrderLog;
use App\Services\NotifikasiService;
use App\Services\OrderService;

class GaransiChatService
{
    public $garansi;

    /**
     * get list query
     */
    public function list()
    {
        return GaransiChat::with('user', 'order');
    }

    public function available()
    {
        $setting = GeneralSetting::where('name', 'garansi_due_date')->firstOrFail();

        $orders = Order::selectRaw('orders.*, (DATE(schedule) + INTERVAL ' . $setting->value . ' DAY) AS due_date')
            ->with('service_detail.technician.user.info')
            ->where('orders_statuses_id', 10)
            ->where('users_id', Auth::id())
            ->whereRaw('DATEDIFF(CURDATE(), DATE(schedule) + INTERVAL ' . $setting->value . ' DAY) < 0')
            ->whereDoesntHave('garansi')
            ->paginate(10);

        return $orders;
    }

    public function totalAvailable($callback = null)
    {
        $setting = GeneralSetting::where('name', 'garansi_due_date')->firstOrFail();

        $orders = Order::selectRaw('orders.*, (DATE(schedule) + INTERVAL ' . $setting->value . ' DAY) AS due_date')
            ->with('service_detail.technician.user.info')
            ->where('orders_statuses_id', 10)
            ->whereRaw('DATEDIFF(CURDATE(), DATE(schedule) + INTERVAL ' . $setting->value . ' DAY) < 0')
            ->whereDoesntHave('garansi');

        if ($callback != null) {
            $callback($orders);
        }

        return $orders->count();
    }

    public function pending()
    {
        return GaransiChat::with('order.service_detail.technician.user.info')->where('user_id', Auth::id())->where('status', 'pending')->paginate(10);
    }

    public function done()
    {
        return GaransiChat::with('order.service_detail.technician.user.info')->where('user_id', Auth::id())->where('status', 'done')->paginate(10);
    }

    public function reject()
    {
        return GaransiChat::with('order.service_detail.technician.user.info')->where('user_id', Auth::id())->where('status', 'reject')->paginate(10);
    }

    /**
     * datatables data
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * reply html content
     */
    public function reply($request)
    {
        if (!Auth::user()->hasRole('admin')) {
            $garansi = GaransiChat::where('id', $request->garansi_chat_id)->firstOrFail();
            if ($garansi->user_id != Auth::id()) {
                throw new SendErrorMsgException('terjadi kesalahan silahkan coba lagi besok');
            }
            if ($garansi->customer_can_reply == 0) {
                $detailMessage = GaransiChatDetail::where('garansi_chat_id', $request->garansi_chat_id)
                    ->where('message', 'Please wait a moment, we are processing your request')
                    ->first();

                if ($detailMessage == null) {
                    $admin = User::role('admin')->first();

                    $detail = GaransiChatDetail::create([
                        'garansi_chat_id' => $request->garansi_chat_id,
                        'message'      => 'Please wait a moment, we are processing your request',
                        'user_id'  => $admin->id
                    ]);

                    $response = '
                        <div class="message">
                            <img class="avatar-md" src="' . $admin->avatar . '" data-toggle="tooltip" data-placement="top" title="' . $admin->name . '" alt="avatar">
                            <div class="text-main">
                                <div class="text-group">
                                    <div class="text">
                                        <p>' . $detail->message . '</p>
                                    </div>
                                </div>
                                <span>' . $detail->created_at->format('H:i') . '</span>
                            </div>
                        </div>';
                    return $response;
                } else {
                    throw new SendErrorMsgException('Please wait a moment, we are processing your request');
                }
            }
        }

        $file_name = null;
        if ($request->attachment != null) {
            $file      = $request->file('attachment');
            $file_name = rand() . $file->getClientOriginalName();
            $file->storeAs('public/garansi/attachment/', $file_name);
        }

        if ($file_name == null && empty($request->message)) {
            throw new SendErrorMsgException('message Required');
        }

        $detail = GaransiChatDetail::create([
            'garansi_chat_id' => $request->garansi_chat_id,
            'message'      => $request->message,
            'user_id'  => Auth::id(),
            'attachment'   => $file_name,
        ]);

        return view('admin.customer.garansi._replay_item', [
            'detail' => $detail,
            'auth' => Auth::user()
        ])->render();
    }

    /**
     * create from customer
     */
    public function createAsCustomer(array $data)
    {
        $setting = GeneralSetting::where('name', 'garansi_due_date')->firstOrFail();

        $order = Order::with(
            'symptom_code',
            'repair_code',
            'orderpayment',
            'user.ewallet',
            'service',
            'symptom',
            'detail',
            'order_status',
            'sparepart_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician.user',
            'service_detail.price_service',
            'history_order.order_status'
        )
            ->where('orders_statuses_id', 10)
            // ->whereRaw('DATEDIFF(CURDATE(), DATE(date_complete) + INTERVAL ' . $setting->value . ' DAY) < 0')
            ->whereRaw('DATEDIFF(CURDATE(), DATE(schedule) + INTERVAL ' . $setting->value . ' DAY) < 0')
            ->where('id', $data['order_id'])
            ->where('users_id', Auth::id())
            ->whereDoesntHave('garansi')
            ->firstOrFail();

        $garansi = GaransiChat::where('order_id', $data['order_id'])->where('user_id', Auth::id())->first();
        if ($garansi != null) {
            return $garansi;
        }

        $data_garansi = [
            'garansi_no' => $this->generateGaransiNo(),
            'subject'   => $data['subject'],
            'order_id'      => $data['order_id'],
            'user_id'  => Auth::id(),
            'customer_can_reply' => 0,
        ];

        DB::beginTransaction();
        try {
            $garansi = GaransiChat::create($data_garansi);

            GaransiChatDetail::create([
                'garansi_chat_id' => $garansi->id,
                'user_id'  => Auth::id(),
                'message'      => $data['desc'],
                'order_json' => json_encode($order),
            ]);

            Order::where('id', $data['order_id'])->update([
                'notifikasi_type' => 1,
                'customer_read_at' => null,
                'teknisi_read_at' => null,
                'admin_read_at' => null,
            ]);

            // commit db
            DB::commit();
            // $instance = new AdminController();
            // $instance->sendEmailOrder($order, 'warranty_claim'); // argument ke 2 adalah name_module hardcode dr table email_order
            (new NotifikasiService())->kirimNotikasiGaransi($garansi, $order);
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $garansi;
    }

    public function revisit($request, $service_detail_id)
    {
        $unit = 1;
        $technician = Technician::with('user')->where('id', $request->technician_id)->firstOrFail();

        $garansi = GaransiChat::where('id', $request->garansi_id)->firstOrFail();
        if ($garansi->customer_can_reply == 0) {
            throw new SendErrorMsgException('Please Accept Claim first');
        }

        $data_update = [
            'teknisi_json' => json_encode($technician),
            'teknisi_email' => $technician->user->email,
            'teknisi_phone' => $technician->user->phone,
            'technicians_id' => $request->technician_id,
        ];

        $service_detail = ServiceDetail::with('order')->where('id', $service_detail_id)->firstOrFail();

        $end_work = Carbon::parse($request->schedule)->addHour($request->estimation_hours);
        $data_schedule = [
            'order_id' => $service_detail->orders_id,
            'technician_id' => $request->technician_id,
            'schedule_date' => $request->schedule,
            'start_work' => $request->schedule,
            'end_work' => $end_work,
        ];

        DB::beginTransaction();
        try {
            $service_detail->update($data_update);
            $order = Order::where('id', $service_detail->orders_id)->update([
                'schedule' => $request->schedule . ' ' . $request->hours,
                'orders_statuses_id' => 9,
            ]);

            HistoryOrder::create([
                'orders_id'          => $service_detail->order->id,
                'orders_statuses_id' => 9
            ]);

            // inser schedule technician
            TechnicianSchedule::create($data_schedule);

            $order = Order::with('user', 'detail', 'order_status', 'symptom_code', 'repair_code')
                ->where('id', $service_detail->orders_id)
                ->firstOrFail();

            $updated_at = Carbon::now()->format('d-m-Y H:i:s');
            $content = 'Changed by <b>' . strtoupper('admin') . '</b> and order status to <b>' . strtoupper($order->order_status->name) . '</b>, at <span class="text-danger">' . $updated_at . '</span>';
            OrderLog::create([
                'order_id' => $order->id,
                'content' => $content
            ]);

            $garansi->update([
                'status' => 'revisit'
            ]);

            GaransiChatDetail::create([
                'garansi_chat_id' => $garansi->id,
                'message'      => 'Revisit Order',
                'order_json' => json_encode($order),
                'user_id'  => Auth::id(),
            ]);

            (new NotifikasiService())->kirimNotikasiGaransiStatus($garansi, 'revisit');

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $order;
    }

    /**
     * delete data
     */
    public function delete($id = null)
    {
        if ($id == null) {
            $garansi = $this->getGaransi();
        } else {
            $garansi = GaransiChat::where('id', $id)->firstOrFail();
        }

        GaransiChatDetail::where('garansi_chat_id', $garansi->id)->delete();
        return $garansi->delete();
    }

    /**
     * generate nomor garansi
     *
     * @return string | tahun-(n) example: 20-001
     */
    public static function generateGaransiNo()
    {
        // dapatkan tahun
        $year = date("Y");

        // dapatkan data tiket
        $garansi = GaransiChat::where(DB::raw('YEAR(created_at)'), $year)->orderBy('id', 'desc')->first();

        // jika tidak ada data
        if ($garansi == null) {
            $id = 1;
        } else {
            // jika ada data
            if ($garansi->garansi_no == null) {
                $id = 1;
            } else {
                $garansi_no = explode('-', $garansi->garansi_no);
                if (isset($garansi_no[1])) {
                    $id = abs($garansi_no[1]) + 1;
                } else {
                    $id = 1;
                }
            }
        }

        $number = date('y') . '-';

        $length = strlen($id);
        switch ($length) {
            case 1:
                $number .= '00' . $id;
                break;

            case 2:
                $number .= '0' . $id;
                break;

            default:
                $number .= $id;
                break;
        }

        return $number;
    }

    public function setGaransi(GaransiChat $garansi)
    {
        $this->garansi = $garansi;
        return $this;
    }

    public function getGaransi()
    {
        return $this->garansi;
    }
}
