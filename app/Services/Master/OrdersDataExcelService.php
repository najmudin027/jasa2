<?php

namespace App\Services\Master;

use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use App\Model\Master\ExcelIncome;
use App\Model\Master\PDESHistory;
use App\Model\Master\StatusExcel;
use Illuminate\Support\Facades\DB;
use App\Model\Master\JasaDataExcel;
use App\Model\Master\PartsDataExcel;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\HasChildException;
use App\Model\Master\OrdersDataExcel;
use App\Http\Controllers\ApiController;
use App\Model\Master\EngineerCodeExcel;
use App\Model\Master\ExcelIncomeDetail;
use Illuminate\Database\QueryException;
use App\Model\Master\OrdersDataExcelLog;
use App\Model\Master\PartDataExcelStock;
use App\Model\Master\PartsDataExcelBundle;
use App\Services\Master\ExcelIncomeService;
use App\Model\Master\PartDataExcelStockBundle;
use App\Model\Master\PartDataExcelStockInventory;
use App\Model\Master\ExcelIncomeDetailDescription;

class OrdersDataExcelService
{
    public $orders_data_excel;

    /**
     * get list query
     *
     * @return App\Model\Master\OrdersDataExcel;
     */
    public function list()
    {
        return OrdersDataExcel::with('part_data_excel')->orderBy('id', 'DESC');
    }

    public function datatables($request = null)
    {
        $query = $this->list();
        if(!empty($request->type_date)) {
            $by_date = $request->type_date == 'request_date' ? 'date' : 'repair_completed_date';
        }
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
            $date_to = Carbon::parse($request->date_to)->format('Y-m-d');
            $query->where($by_date, '>=', $date_from)
            ->where($by_date, '<=', $date_to);
        }
        $query = $query->get();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\OrdersDataExcel
     *
     * @throws \Exception
     */
    public function create(array $row)
    {
        DB::beginTransaction();
        $apicontroller = new ApiController();
        try {
            $stock_inventory_service = new PartDataExcelStockInventoryService();
            $insert_ode = OrdersDataExcel::create($this->generateData($row));
            // $this->insertAutoCompleteField($row);
            $this->insertBundleDetails($insert_ode->id,$row, $stock_inventory_service);
            $this->insertJasaDetails($insert_ode,$row);
            if(isset($row['part_data_stock_inventories_id']) && $row['part_data_stock_inventories_id'] != "") {
                foreach ($row['part_data_stock_inventories_id'] as $key => $value) {
                    // dd($value);
                    $check_part = !empty($row['code_material'][$key]) && !empty($row['part_description'][$key]) && !empty($row['quantity'][$key]);
                    if ($check_part) {
                        $pde = PartsDataExcel::create([
                            'orders_data_excel_id' => $insert_ode->id,
                            'part_data_stock_inventories_id' => $row['part_data_stock_inventories_id'][$key],
                            'code_material' => $row['code_material'][$key],
                            'part_description' => $row['part_description'][$key],
                            'quantity' => $row['quantity'][$key],
                            'is_order_by_customer' => $row['is_order_by_customer'][$key],
                            'price_hpp_average' => ($row['is_order_by_customer'][$key] == 1 ? $row['price_hpp_average'][$key] : null),
                            'selling_price' => ($row['is_order_by_customer'][$key] == 1 ? $row['selling_price'][$key] : null),
                            'selling_price_pcs' => ($row['is_order_by_customer'][$key] == 1 ? $row['selling_price_pcs'][$key] : null),
                        ]);
                        $decrease_stock = $stock_inventory_service->decreaseStock(($row['part_data_stock_inventories_id'][$key]), ($row['quantity'][$key]));
                        $data_history_inventory = [
                            'date' => Carbon::parse($pde->created_at)->format('Y-m-d'),
                            'pdesi_id_out' => $row['part_data_stock_inventories_id'][$key],
                            'pdesi_id_in' => null,
                            'amount' => ($row['quantity'][$key]),
                            'type_amount' =>  0,
                            'type_mutation' =>  3,
                            'general_journal_details_id' => null,
                            'part_data_excel_id' => $pde->id
                        ];
                        $stock_inventory_service->historyLogProcess($data_history_inventory);
                        if($decrease_stock == false) {
                            return $apicontroller->errorResponse(
                                $decrease_stock['stock_now'],
                                'Code Material = '.$row['code_material'][$key].'. Quantity melebihi jumlah stock terupdate sekarang = '.$decrease_stock['stock_now'].'',
                                400
                            );
                        }

                        if($row['is_order_by_customer'][$key]) {
                            $this->storeIsOrderByCustomerExcelIncome($row['repair_completed_date'], $row['service_order_no'], $row['defect_type'], $row['price_hpp_average'][$key], $row['selling_price'][$key], $pde->id, $row['asc_name']);
                        }

                    }
                }
            }



            $this->logs($insert_ode, 1);
            DB::commit();
            return $apicontroller->successResponse(
                $row,
                $apicontroller->successStoreMsg(),
                200
            );
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function insertJasaDetails($insert_ode, $row)
    {
        DB::beginTransaction();
        $apicontroller = new ApiController();
        try {
            if(isset($row['jasa_id']) && $row['jasa_id'] != "") {
                foreach ($row['jasa_id'] as $key => $value) {
                    // dd($value);
                    $check = !empty($row['jasa_id'][$key]) && !empty($row['price_j'][$key]);
                    if ($check) {
                        $jde = JasaDataExcel::create([
                            'orders_data_excel_id' => $insert_ode->id,
                            'jasa_id' => $row['jasa_id'][$key],
                            'jasa_name' => $row['jasa_name'][$key],
                            'price' => $row['price_j'][$key],
                        ]);
                    }
                }
            }
            DB::commit();
            return $apicontroller->successResponse(
                $row,
                $apicontroller->successStoreMsg(),
                200
            );
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function insertBundleDetails($ode_id, $row, $stock_inventory_service)
    {
        DB::beginTransaction();
        $apicontroller = new ApiController();
        try {
            if(isset($row['part_data_stock_bundle_id']) && $row['part_data_stock_bundle_id'] != "") {
                foreach ($row['part_data_stock_bundle_id'] as $key => $value) {
                    // dd($value);
                    $check_bundle = !empty($row['part_data_stock_bundle_id'][$key]);
                    if ($check_bundle) {
                        $pdeb = PartsDataExcelBundle::create([
                            'orders_data_excel_id' => $ode_id,
                            'part_data_stock_bundle_id' => $row['part_data_stock_bundle_id'][$key],
                            'quantity' => 1,
                            'bundle_name' => $row['bundle_name'][$key],
                            'main_price' => $row['main_price'][$key],
                        ]);
                    }
                    $data_bundle = PartDataExcelStockBundle::with('part_data_stock_bundle_detail')->where('id', $row['part_data_stock_bundle_id'][$key])->first();
                    foreach ($data_bundle->part_data_stock_bundle_detail as $bundle_detail) {
                        $pde = PartsDataExcel::create([
                            'orders_data_excel_id' => $ode_id,
                            'part_data_stock_inventories_id' => $bundle_detail->part_data_stock_inventories_id,
                            'part_data_excel_bundle_id' => $pdeb->id,
                            'part_data_stock_bundle_details_id' => $bundle_detail->id,
                            'code_material' => $bundle_detail->code_material,
                            'part_description' => $bundle_detail->part_description,
                            'quantity' => $bundle_detail->quantity,
                        ]);
                        $decrease_stock = $stock_inventory_service->decreaseStock($bundle_detail->part_data_stock_inventories_id, $bundle_detail->quantity);
                        $data_history_inventory = [
                            'date' => Carbon::parse($pde->created_at)->format('Y-m-d'),
                            'pdesi_id_out' => $bundle_detail->part_data_stock_inventories_id,
                            'pdesi_id_in' => null,
                            'amount' => $bundle_detail->quantity,
                            'type_amount' =>  0,
                            'type_mutation' =>  3,
                            'general_journal_details_id' => null,
                            'part_data_excel_id' => $pde->id,
                            'part_data_stock_bundle_details_id' => $bundle_detail->id
                        ];
                        $stock_inventory_service->historyLogProcess($data_history_inventory);
                        if($decrease_stock == false) {
                            return $apicontroller->errorResponse(
                                $decrease_stock['stock_now'],
                                'Code Material = '.$bundle_detail->code_material.'. Quantity melebihi jumlah stock terupdate sekarang = '.$decrease_stock['stock_now'].'',
                                400
                            );
                        }
                    }
                }
            }

            DB::commit();
            return $apicontroller->successResponse(
                $row,
                $apicontroller->successStoreMsg(),
                200
            );
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $row)
    {
        return $orders_data_excel = [
            "service_order_no" => $row['service_order_no'],
            "asc_id" => $row['asc_id'],
            "asc_name" => $row['asc_name'],
            "customer_name" => $row['customer_name'],
            "type_job" => $row['type_job'],
            "defect_type" => $row['defect_type'],
            "engineer_code" => $row['engineer_code'],
            "engineer_name" => $row['engineer_name'],
            "assist_engineer_name" => $row['assist_engineer_name'],
            "merk_brand" => $row['merk_brand'],
            "model_product" => $row['model_product'],
            "status" => $row['status'],
            "reason" => $row['reason'],
            "remark_reason" => $row['remark_reason'],
            "pending_aging_days" => $row['pending_aging_days'],
            "street" => $row['street'],
            "city" => $row['city'],
            "phone_no_mobile" => $row['phone_no_mobile'],
            "phone_no_home" => $row['phone_no_home'],
            "phone_no_office" => $row['phone_no_office'],
            "month" => $row['month'],
            "date" => Carbon::parse($row['date'])->format('Y-m-d'),
            "request_time" => Carbon::parse($row['request_time'])->format('H:i:s'),
            // "created_times" => $row['created_times'],
            "engineer_picked_order_date" => Carbon::parse($row['engineer_picked_order_date'])->format('Y-m-d'),
            "engineer_picked_order_time" => Carbon::parse($row['engineer_picked_order_time'])->format('H:i:s'),
            "admin_assigned_to_engineer_date" => Carbon::parse($row['admin_assigned_to_engineer_date'])->format('Y-m-d'),
            "admin_assigned_to_engineer_time" => Carbon::parse($row['admin_assigned_to_engineer_time'])->format('H:i:s'),
            "engineer_assigned_date" => Carbon::parse($row['engineer_assigned_date'])->format('Y-m-d'),
            "engineer_assigned_time" => Carbon::parse($row['engineer_assigned_time'])->format('H:i:s'),
            "tech_1st_appointment_date" => Carbon::parse($row['tech_1st_appointment_date'])->format('Y-m-d'),
            "tech_1st_appointment_time" => Carbon::parse($row['tech_1st_appointment_time'])->format('H:i:s'),
            "1st_visit_date" => Carbon::parse($row['1st_visit_date'])->format('Y-m-d'),
            "1st_visit_time" => Carbon::parse($row['1st_visit_time'])->format('H:i:s'),
            "repair_completed_date" => Carbon::parse($row['repair_completed_date'])->format('Y-m-d'),
            "repair_completed_time" => Carbon::parse($row['repair_completed_time'])->format('H:i:s'),
            "closed_date" => Carbon::parse($row['closed_date'])->format('Y-m-d'),
            "closed_time" => Carbon::parse($row['closed_time'])->format('H:i:s'),
            "rating" => $row['rating'],
        ];
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        DB::beginTransaction();

        try {
            // $this->insertAutoCompleteField($data);
            $update = $this->getData()->update($this->generateData($data));
            $this->logs($this->getData(), 2);
            DB::commit();
            return $update;

        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteX()
    {
        $orders_data_excel = $this->getData();
        DB::beginTransaction();

        try {
            $this->logs($orders_data_excel, 3);
            $delete = $orders_data_excel->delete();
            DB::commit();


        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\OrdersDataExcel
     *
     * @throws \Exception
     */

    public function setData(OrdersDataExcel $orders_data_excel)
    {
        $this->orders_data_excel = $orders_data_excel;
        return $this;
    }

    public function getData()
    {
        return $this->orders_data_excel;
    }

    public function deleteById(array $ids)
    {
        try {
            return OrdersDataExcel::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function logs($data, $type) { // type created/changed/deleted
        return $log = OrdersDataExcelLog::create([
            'orders_data_excel_id' => $data->id,
            'service_order_no' => $data->service_order_no,
            'user_id' => Auth::user()->id,
            'type' => $type,
        ]);

    }

    public function storeIsOrderByCustomerExcelIncome($date, $service_order_no, $defect_type, $hpp_avg, $selling_price, $pde_id, $asc_name) {
        $description = "Order Purchase By Customer";
        $date = $date;
        $month = Carbon::parse($date)->format('m');
        $year = Carbon::parse($date)->format('Y');
        $whereData = [
            ['desc', '=', $description],
            ['month', '=', $month],
            ['year', '=', $year],

        ];
        $cek_ei = ExcelIncome::where($whereData);
        $service = new ExcelIncomeService();
        if($cek_ei->count() == 0) {
            $cek_ei = ExcelIncome::create([
                'code' => $service->generateCode(),
                'month' => $month,
                'year' => $year,
                'desc' => $description
            ]);
        } else {
            $cek_ei = $cek_ei->first();
        }
        $duplicate = ExcelIncomeDetail::where('part_data_excel_id', $pde_id,)->where('excel_income_id', $cek_ei->id)->count();
        if($duplicate == 0) {
            $create_eid = ExcelIncomeDetail::create([
                'excel_income_id' => $cek_ei->id,
                'source' => $service_order_no,
                'date_transaction' => $date,
                'in_nominal' => $selling_price,
                'part_data_excel_id' => $pde_id,
                'branch' => $asc_name
            ]);
            $create_desc = ExcelIncomeDetailDescription::create([
                'excel_income_detail_id' => $create_eid->id,
                'description' => $defect_type
            ]);
        }
    }
    public function bundleDetailsDelete($pdeb_id) { //part_data_excel_bundle_id
        $data = PartsDataExcel::where('part_data_excel_bundle_id', $pdeb_id);
        if($data->count() > 0) {
            $getData = $data->get();

            foreach($getData as $detail) {
                if($detail->part_data_stock_inventories_id != null) {
                    $update_log_gjd_deleted = PDESHistory::where('part_data_excel_id', $detail->id)->update(['is_deleted' => 1]);
                    $data_history_inventory = [
                        'date' => Carbon::parse($detail->created_at)->format('Y-m-d'),
                        'pdesi_id_out' => null,
                        'pdesi_id_in' => $detail->part_data_stock_inventories_id,
                        'amount' => $detail->quantity,
                        'type_amount' =>  1,
                        'type_mutation' =>  5,
                        'general_journal_details_id' => null,
                        'part_data_excel_id' => $detail->id,
                    ];
                    $stock_inventory_service = new PartDataExcelStockInventoryService();
                    // return $data_history_inventory;
                    $stock_inventory_service->historyLogProcess($data_history_inventory);
                    // $pdes_service->historyLogProcess(null, $part->part_data_stock_id, $part->quantity, 1, 7, null, $part->id);
                    $stock_inventory_service->restoreStock($detail->part_data_stock_inventories_id, $detail->quantity);
                }
            }
            return PartsDataExcel::where('part_data_excel_bundle_id', $pdeb_id)->delete();
        }
        return null;
    }

}
