<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Database\QueryException;
use App\Model\Master\TypeJobExcel;

class TypeJobExcelService
{
    public $type_job_excel;

    /**
     * get list query
     *
     * @return App\Model\Master\TypeJobExcel;
     */
    public function list()
    {
        return TypeJobExcel::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Product\TypeJobExcel
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Product\TypeJobExcel
     */
    public function select2(Request $request)
    {
        return TypeJobExcel::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\TypeJobExcel
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return TypeJobExcel::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getTypeJobExcel()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getTypeJobExcel()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return TypeJobExcel::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
        ];
    }

    public function setTypeJobExcel(TypeJobExcel $type_job_excel)
    {
        $this->type_job_excel = $type_job_excel;
        return $this;
    }

    public function getTypeJobExcel()
    {
        return $this->type_job_excel;
    }
}
