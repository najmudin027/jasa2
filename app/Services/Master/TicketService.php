<?php

namespace App\Services\Master;

use App\Exceptions\SendErrorMsgException;
use Auth;
use App\User;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Model\Master\MsTicketDetail;
use App\Model\Master\MsTicket as Ticket;
use App\Http\Controllers\AdminController;
use App\Notifications\Ticket\TicketNewNotification;
use App\Notifications\Ticket\TicketReplyNotification;
use Illuminate\Database\QueryException;

class TicketService
{
    public $ticket;

    /**
     * get list query
     *
     * @return App\Model\Master\Ticket;
     */
    function list()
    {
        return Ticket::with('user', 'unread_ticket');
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\Ticket
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function reply($request)
    {
        $file_name = null;
        if ($request->attachment != null) {
            $file      = $request->file('attachment');
            $file_name = rand() . $file->getClientOriginalName();
            $file->storeAs('public/attachment/', $file_name);
        }

        if (Auth::user()->hasRole('Customer')) {
            $ticket = Ticket::where('users_id', Auth::id())->where('id', $request->ms_ticket_id)->firstOrFail();
        }

        $reply_ticket = MsTicketDetail::create([
            'ms_ticket_id' => $request->ms_ticket_id,
            'user_id'      => Auth::id(),
            'message'      => $request->message,
            'attachment'   => $file_name,
        ]);

        $details = MsTicketDetail::with('ticket.user')
            ->where('ms_ticket_id', $request->ms_ticket_id)
            ->where('user_id', '!=', Auth::id())
            ->groupBy('user_id')
            ->get();

        $content = view('admin.customer.tiket._replay_item', [
            'reply_ticket' => $reply_ticket,
        ])->render();

        foreach ($details as $detail) {
            \Notification::send($detail->user, new TicketReplyNotification($reply_ticket, $detail->user, $content));
        }

        return $content;
    }

    public function replyAsJson($request)
    {
        $file_name = null;
        if ($request->attachment != null) {
            $file      = $request->file('attachment');
            $file_name = rand() . $file->getClientOriginalName();
            $file->storeAs('public/attachment/', $file_name);
        }

        if (Auth::user()->hasRole('Customer')) {
            $ticket = Ticket::where('users_id', Auth::id())->where('id', $request->ms_ticket_id)->firstOrFail();
        }

        $reply_ticket = MsTicketDetail::create([
            'ms_ticket_id' => $request->ms_ticket_id,
            'user_id'      => Auth::id(),
            'message'      => $request->message,
            'attachment'   => $file_name,
        ]);

        return $reply_ticket;
    }

    public function create(array $data)
    {
        $administrator = User::getAdminRole();

        $ticket = Ticket::create([
            'status'    => 1,
            'ticket_no' => Ticket::genetateTicketNo(),
            'subject'   => $data['subject'],
            'desc'      => $data['desc'],
            'users_id'  => $data['user_id'],
        ]);

        foreach ($administrator as $user) {
            \Notification::send($user, new TicketNewNotification($ticket, $user));
        }
    }

    public function createAsCustomer(array $data)
    {
        DB::beginTransaction();
        try {
            $tiket =  Ticket::create([
                'ticket_no' => Ticket::genetateTicketNo(),
                'subject'   => $data['subject'],
                'desc'      => $data['desc'],
                'status'    => 1,
                'users_id'  => Auth::id(),
            ]);

            MsTicketDetail::create([
                'ms_ticket_id' => $tiket->id,
                'user_id'      => Auth::id(),
                'message'      => $data['desc']
            ]);
            $user = User::where('id', $tiket->users_id)->first(['name', 'email']);
            $parsing_email = [
                'ticket_no'          => $tiket->ticket_no,
                'subject_ticket'     => $data['subject'],
                'description_ticket' => $data['desc'],
                'customer_name'      => $user->name,
                'customer_email'     => $user->email,
                'status_ticket'      => 'Open'
            ];
            $administrator = User::getAdminRole();
            foreach ($administrator as $user) {
                \Notification::send($user, new TicketNewNotification($tiket, $user));
            }

            $instance = new AdminController();
            $instance->sendEmailOrder($parsing_email, 'ticket'); // argument 1 bukan order karna buat tiket baru, argument ke 2 adalah name_module hardcode dr table email_order
            // commit db
            DB::commit();
        } catch (QueryException $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }

        return $tiket;
    }

    public function update(array $data)
    {
        $ticket = $this->getTicket();

        return $ticket->update([
            'subject'  => $data['subject'],
            'desc'     => $data['desc'],
            'users_id' => $data['user_id'],
        ]);
    }

    public function updateStatus(array $data)
    {
        $ticket = $this->getTicket();

        $ticket->update([
            'status' => $data['status'],
        ]);

        return $this->getStatusHtml($data['status']);
    }

    public function delete($id = null)
    {
        if ($id == null) {
            $ticket = $this->getTicket();
        } else {
            $ticket = Ticket::where('id', $id)->firstOrFail();
        }
        MsTicketDetail::where('ms_ticket_id', $ticket->id)->delete();
        return $ticket->delete();
    }

    public function getStatusHtml($status)
    {
        switch ($status) {
            case '1':
                return '<span class="btn btn-sm btn-primary">open</span>';
                break;

            case '2':
                return '<span class="btn btn-sm btn-info">on progress</span>';
                break;

            case '3':
                return '<span class="btn btn-sm btn-dark">on resolving</span>';
                break;

            case '4':
                return '<span class="btn btn-sm btn-success">done</span> ';
                break;

            case '5':
                return '<span class="btn btn-sm btn-danger">closed</span> ';
                break;

            default:
                return $status;
                break;
        }
    }

    public function setTicket(Ticket $ticket)
    {
        $this->ticket = $ticket;
        return $this;
    }

    public function getTicket()
    {
        return $this->ticket;
    }
}
