<?php

namespace App\Services\Master;

use App\Model\Master\MsPriceItems;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PriceItemsService
{
    public $priceItem;
    /**
     * get list query
     *
     * @return App\Model\Master\MsPriceItems;
     */
    public function list()
    {
        return MsPriceItems::query();
    }

    public function datatables()
    {
        $query = $this->list()->with(['unit', 'city', 'district', 'village', 'product']);
        return DataTables::of($query)->addIndexColumn()->toJson();
    }

    public function select2(Request $request)
    {
        return MsPriceItems::where('value', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    public function create(array $data)
    {
        try{
            return MsPriceItems::create($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function update(array $data)
    {
        try{
            return $this->getPriceItems()->update($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }


    public function delete()
    {
        try {
            return $this->getPriceItems()->delete();
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function setPriceItems(MsPriceItems $priceItem)
    {
        $this->priceItem = $priceItem;
        return $this;
    }

    public function generateData(array $data)
    {
        return [
            'value' => $data['value'],
            'ms_unit_type_id' => $data['ms_unit_type_id'],
            'ms_city_id' => $data['ms_city_id'],
            'ms_district_id' => $data['ms_district_id'],
            'ms_village_id' => $data['ms_village_id'],
            'ms_product_id' => $data['ms_product_id'],
        ];
    }

    public function getPriceItems()
    {
        return $this->priceItem;
    }
}
