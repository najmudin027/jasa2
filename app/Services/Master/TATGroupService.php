<?php

namespace App\Services\Master;

use App\Model\Master\TATGroup;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TATGroupService
{
    public $grup;

    /**
     * get list query
     */
    public function list()
    {
        return TATGroup::query();
    }

    /**
     * datatables data
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     */
    public function select2(Request $request)
    {
        return TATGroup::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     */
    public function find(int $id)
    {
        return TATGroup::where('id', $id)->first();
    }

    /**
     * create data
     */
    public function create(array $data)
    {
        try {
            return TATGroup::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     */
    public function update(array $data)
    {
        try {
            return $this->getGrup()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     */
    public function delete()
    {
        return $this->getGrup()->delete();
    }

    /**
     * delete multiple data
     */
    public function deleteById(array $ids)
    {
        try {
            return TATGroup::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
        ];
    }

    public function setGrup(TATGroup $grup)
    {
        $this->grup = $grup;
        return $this;
    }

    public function getGrup()
    {
        return $this->grup;
    }
}
