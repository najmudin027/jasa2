<?php

namespace App\Services;

use Ap;
use App\Helpers\ImageUpload;
use DB;
use App\User;
use App\Model\RequestOrders;
use App\Model\RequestOrderDetail;
use App\Model\RequestOrderTmp;
use App\Model\RequestOrderImage;
use App\Model\RequestOrderImageTmp;
use App\Services\User\UserB2bUpdateService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Services\User\UserB2bLoginService;
use Intervention\Image\ImageManagerStatic as Image;
use App\Mail\SuccessRegisterEmail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use App\Services\User\UserStoreB2bService;
use App\Model\BusinessToBusinessOutletDetailTransaction;

class UserB2bService
{
    public $token_name = 'jasaWebb2b';
    public $user;
    public $notif    = true;
    public $redirect = '/';
    public $status;
    
    public function addToCard(array $data)
    {
        
        $getUnit = BusinessToBusinessOutletDetailTransaction::where('id', $data['ms_transaction_b2b_detail'])->first();
        // dd($getUnit);
        // if($file = $data['vidio']){
        //     foreach($data['vidio'] as $vidio){
        //         $name = time() . $vidio->getClientOriginalName();
        //         // $vidio->move('videos', $name);
        //         Storage::disk('local')->put('public/requst-vidio' . $name, $vidio);
        //     }
        // }

        try {
            $reqOrder = RequestOrders::create([
                'request_code' => $this->generateOrderCode(),
                'user_id' => $data['user_id'],
                'created_at' => date('Y-m-d H:i:s')
            ]);

            for ($j = 0; $j < count($data['ms_outlet_id']); $j++) {
                $storeDetail = BusinessToBusinessOutletDetailTransaction::create([
                    'business_to_business_outlet_transaction_id' => $data['ms_outlet_id'][$j],
                    'unit_ke' => $getUnit->unit_ke,
                    'merek' => $getUnit->merek,
                    'remark' => $data['remark'][$j],
                    'no_quotation' => '-',
                    'status_quotation' => 'Requested',
                    'no_po' => '-',
                    'no_invoice' => '-',
                    'nominal_quot' => 0,
                    'tanggal_pembayaran' => null,
                    'tanggal_quot' => null,
                    'tanggal_po' => null,
                    'tanggal_Invoice' => null,
                    'tanggal_pengerjaan' => null,
                    'file_berita_acara' => '-',
                    'file_quot' => '-',
                    'file_po' => '-',
                    'file_invoice' => '-',
                    'uniq_key' => '-',
                    'user_created' => Auth::id(),
                    'is_parent_b2b_outlet_id' => $getUnit->id,
                ]);

                // $vidio = $data['vidio'][$j];
                // $name_vidio = null;
                // if ($vidio != 'null') {
                //     $name_vidio = time() . $vidio->getClientOriginalName();
                //     $vidio->move('videos', $name_vidio);
                //     // Storage::disk('local')->put('/public/requst-vidio/' . $name_vidio, $vidio);
                // }else{
                //     $name_vidio = null;
                // }
                

                $simpan = [
                    'request_code' => $reqOrder->request_code,
                    'ms_outlet_id' => $data['ms_outlet_id'][$j],
                    'ms_transaction_b2b_detail' => $data['ms_transaction_b2b_detail'][$j],
                    'ms_b2b_detail' => $storeDetail->id,
                    'service_type' => $data['service_type'][$j],
                    'user_id' => Auth::user()->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'vidio' => null,
                    'remark' => $data['remark'][$j],
                ];

                $saveOrder = RequestOrderDetail::create($simpan);
            }
            
            // $path = RequestOrderImage::getImagePathUpload();
            // $filename = null;

            // if(!empty($data['image'])){
            //     if($data['image']){
            //         foreach($data['image'] as $image){
            //             if(!empty($name_gambar)){
            //                 $name_gambar = $image->getClientOriginalName();
            //                 $img = Image::make($image);
            //                 $img->stream(); // <-- Key point
            //                 Storage::disk('local')->put('public/request_order_image/' . $name_gambar, $img);
            //                 $filename[] = $name_gambar;
            //             }else{
            //                 $filename[] = null;
            //              }
            //         }
            //     }
            // }else{
            //     $filename = null;
            // }


            // for ($i = 0; $i < count($data['image']); $i++) {
            //     $insert[] = [
            //         'ms_request_order_id' => $reqOrder->request_code,
            //         'image' => $filename[$i],
            //         'created_at' => date('Y-m-d H:i:s'),
            //         'updated_at' => date('Y-m-d H:i:s'),
            //     ];
            // }

            // RequestOrderImage::insert($insert);

            DB::commit();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }


    public function requestOrderSingle(array $data)
    {
        $getUnit = BusinessToBusinessOutletDetailTransaction::where('id', $data['ms_transaction_b2b_detail'])->first();
        // dd($getUnit);
        if(!empty($data['vidio'] )){
            if($file = $data['vidio']){
                $name = time() . $file->getClientOriginalName();
                $file->move('videos', $name);
            }
        }else{
            $name = null;
        }
        

        try {

            $path = RequestOrderImage::getImagePathUpload();
            $filename = null;

            if(!empty($data['image'])){
                if($image = $data['image']){
                    $name = $image->getClientOriginalName();
                    $img = Image::make($image);
                    $img->stream(); // <-- Key point
                    Storage::disk('local')->put('public/request_order_image/' . $name, $img);
                    $filename = $name;
                }
            }else{
                $filename = null;
            }

            $reqOrder = RequestOrders::create([
                'request_code' => $this->generateOrderCode(),
                'user_id' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $storeDetail = BusinessToBusinessOutletDetailTransaction::create([
                'business_to_business_outlet_transaction_id' => $data['ms_outlet_id'],
                'unit_ke' => $getUnit->unit_ke,
                'merek' => $getUnit->merek,
                'remark' => '-',
                'no_quotation' => '-',
                'status_quotation' => 'Requested',
                'no_po' => '-',
                'no_invoice' => '-',
                'nominal_quot' => 0,
                'tanggal_pembayaran' => null,
                'tanggal_quot' => null,
                'tanggal_po' => null,
                'tanggal_Invoice' => null,
                'tanggal_pengerjaan' => null,
                'file_berita_acara' => '-',
                'file_quot' => '-',
                'file_po' => '-',
                'file_invoice' => '-',
                'uniq_key' => '-',
                'user_created' => Auth::id(),
                'is_parent_b2b_outlet_id' => $getUnit->id,
            ]);

            $storeOrder = RequestOrderDetail::create([
                'request_code' => $reqOrder->request_code,
                'ms_outlet_id' => $data['ms_outlet_id'],
                'ms_transaction_b2b_detail' => $data['ms_transaction_b2b_detail'],
                'ms_b2b_detail' => $storeDetail->id,
                'service_type' => $data['service_type'],
                'user_id' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'vidio' => !empty($name) ? $name : null,
                'remark' => $data['remark'],
                'image' => $filename,
            ]);

            // $insert = RequestOrderImage::create([
            //     'ms_request_order_id' => $reqOrder->request_code,
            //     'image' => $filename,
            //     'created_at' => date('Y-m-d H:i:s'),
            //     'updated_at' => date('Y-m-d H:i:s'),
            // ]);

        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function requestOrderTemporari(array $data)
    {
        
        $getUnit = BusinessToBusinessOutletDetailTransaction::where('id', $data['ms_transaction_b2b_detail'])->first();
        if(!empty($data['vidio'] )){
            if($file = $data['vidio']){
                $name = time() . $file->getClientOriginalName();
                $file->move('videos', $name);
            }
        }else{
            $name = null;
        }

        if(!empty($data['image'])){
            if($image = $data['image']){
                $name_image = $image->getClientOriginalName();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/request_order_image/' . $name_image, $img);
                $filename = $name_image;
            }
        }else{
            $filename = null;
        }

        try{
            $storeDetail = BusinessToBusinessOutletDetailTransaction::create([
                'business_to_business_outlet_transaction_id' => $data['ms_outlet_id'],
                'unit_ke' => $getUnit->unit_ke,
                'merek' => $getUnit->merek,
                'remark' => '-',
                'no_quotation' => '-',
                'status_quotation' => 'Requested',
                'no_po' => '-',
                'no_invoice' => '-',
                'nominal_quot' => 0,
                'tanggal_pembayaran' => null,
                'tanggal_quot' => null,
                'tanggal_po' => null,
                'tanggal_Invoice' => null,
                'tanggal_pengerjaan' => null,
                'file_berita_acara' => '-',
                'file_quot' => '-',
                'file_po' => '-',
                'file_invoice' => '-',
                'uniq_key' => '-',
                'user_created' => Auth::id(),
                'is_parent_b2b_outlet_id' => $getUnit->id,
            ]);

            $storeOrder = RequestOrderTmp::create([
                // 'request_code' => $reqOrder->request_code,
                'ms_outlet_id' => $data['ms_outlet_id'],
                'ms_transaction_b2b_detail' => $data['ms_transaction_b2b_detail'],
                'ms_b2b_detail' => $storeDetail->id,
                'service_type' => $data['service_type'],
                'user_id' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'vidio' => !empty($name) ? $name : null,
                'image' => $filename,
                'remark' => $data['remark'],
            ]);

        }catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateOrderCode()
    {
        $q          = RequestOrders::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmY');
        $number     = 1; //format
        $new_number = sprintf("%06s", $number);
        $code       = 'ORD-' . $date . ($separator) . ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = RequestOrders::orderBy('id', 'desc')->value('request_code');
            $last     = explode("-", $last);
            $order_code = 'ORD-' . $date . ($separator) . (sprintf("%06s", $last[2] + 1));
        }
        return $order_code;
    }

    public function createFromAdmin(array $data)
    {
        $storeService = new UserStoreB2bService();

        $user = $storeService->createFromAdmin($data);

        $this->setUser($user);

        return $user;
    }

    public function update(array $data, User $user)
    {
        $updateService = new UserB2bUpdateService();

        return $updateService->setUser($user)->handle($data);
    }

    public function deleteAll($id)
    {
        $user   = User::where(['id' => $id])->firstOrFail();

        DB::beginTransaction();
        try {
            $user->delete();
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new SendErrorMsgException($e->getMessage());
        }
    }

    public function login(array $credentials)   
    {
        $loginService = new UserB2bLoginService();
        $login = $loginService->handle($credentials);
        if ($login) {
            // $this->setUser($loginService->getUser())->setBearerToken();
            $this->setUser($loginService->getUser());
        }

        return $login;
    }

    public function dontSendNotif()
    {
        $this->notif = false;
        return $this;
    }

    public function getTokenName(): string
    {
        return $this->token_name;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
}