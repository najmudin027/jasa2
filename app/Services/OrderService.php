<?php

namespace App\Services;

use App\User;
use Carbon\Carbon;
use App\Events\OrderEvent;
use App\Model\Master\Order;
use App\Model\B2BAssignTeknisi;
use App\Model\Notification;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Model\Master\Ewallet;
use App\Model\Master\MsTicket;
use App\Model\Master\OrderLog;
use App\Model\Master\Services;
use App\Model\Master\Inventory;
use App\Model\Master\MsAddress;
use App\Model\Master\MsSymptom;
use App\Model\Master\EmailOther;
use App\Model\Master\ItemDetail;
use App\Traits\OrderStatusHandle;
use App\Model\Master\GaleryImages;
use App\Model\Master\HistoryOrder;
use App\Model\Master\ProductGroup;
use Illuminate\Support\Facades\DB;
use App\Model\Master\ServiceDetail;
use Illuminate\Support\Facades\Log;
use App\Model\Master\EwalletHistory;
use App\Model\Master\EwalletPayment;
use App\Model\Master\GeneralSetting;
use App\Model\Master\MsPriceService;
use App\Model\Master\MsServicesType;
use App\Model\Master\MsTicketDetail;
use App\Model\Technician\Technician;
use App\Services\Master\ChatService;
use Illuminate\Support\Facades\Auth;
use App\Model\Master\SparepartDetail;
use App\Model\Master\TechnicianSaldo;
use Illuminate\Support\Facades\Cache;
use App\Model\Master\EmailStatusOrder;
use App\Http\Controllers\ApiController;
use App\Model\Master\OrderMediaProblem;
use Illuminate\Database\QueryException;
use App\Http\Requests\OrderStoreRequest;
use App\Model\Master\TechnicianSchedule;
use App\Model\Master\TransactionHistory;
use Yajra\DataTables\Facades\DataTables;
use App\Exceptions\SendErrorMsgException;
use App\Http\Controllers\AdminController;
use App\Model\Master\TechnicianSparepart;
use App\Services\Master\InventoryService;
use App\Model\Master\ProductDetailInformation;
use App\Notifications\Order\OrderNotification;

class OrderService
{
    use OrderStatusHandle;

    public function createFromAdmin(OrderStoreRequest $request)
    {
        $service_type = MsServicesType::where('id', $request->service_type_id)->firstOrfail();
        $productgrup = ProductGroup::where('id', $request->product_group_id)->firstOrfail();
        $service = Services::where('id', $request->ms_service_id)->firstOrfail();
        $symptom = MsSymptom::where('id', $request->symptom_id)->firstOrfail();
        $wallet = Ewallet::where('user_id', $request->user_id)->first();
        $addr   = MsAddress::with('types')->where('id', $request->address_id)->firstOrFail();
        $user = User::with('info', 'address', 'ewallet')->where('id', $request->user_id)->firstOrFail();
        $price_service = MsPriceService::with('technician.user')->where('id', $request->price_service_id)->where('technicians_id', $request->technician_id)->firstOrFail();
        $status = 9;
        $unit = 1;
        $totalPart = $request->total_extra_cost;
        $result_deduction = 0;
        $order_id = DB::table('orders')->max('id') + 1;
        $history_wallet_id = EwalletHistory::max('id') + 1;
        $end_work = Carbon::parse($request->tech_schedule)->addHour($request->estimation_hours);

        if ($request->status == 'on') {
            if ($wallet == null) {
                throw new SendErrorMsgException('The wallet balance of this user is not sufficient for this service price');
            }
            if ($wallet->nominal < $request->grand_total) {
                throw new SendErrorMsgException('The wallet balance of this user is not sufficient for this service price');
            }
        }

        if ($wallet != null && $request->status == 'on') {
            $history_wallet_id = EwalletHistory::max('id') + 1;

            $data_history_wallet = [
                'id' => $history_wallet_id,
                'ms_wallet_id' => $wallet->id,
                'type_transaction_id' => 1, // pay
                'transfer_status_id' => 0,
                'saldo' => $wallet->nominal - $request->grand_total,
                'nominal' => $request->grand_total,
                'user_id' => $user->id,
            ];

            $data_wallet = [
                'nominal' => $wallet->nominal - $request->grand_total,
            ];

            $data_wallet_payment = [
                'order_id' => $order_id,
                'wallet_history_id' => $history_wallet_id
            ];

            $data_saldo_teknisi = [
                'technician_id' => $request->technician_id,
                'order_id' => $order_id,
            ];
        }

        $data_service_detail = [
            'orders_id'              => $order_id,
            'price'                  => $price_service->value,
            'ms_services_types_id'   => $request->service_type_id,
            'ms_price_services_id'   => $price_service->id,
            'teknisi_json'           => json_encode($price_service->technician),
            'teknisi_email'          => $price_service->technician->user->email,
            'teknisi_phone'          => $price_service->technician->user->phone,
            'ms_symptoms_id'         => $request->symptom_id,
            'symptom_name'           => $symptom->name,
            'service_type_name'      => $service_type->name,
            'schedule'               => $request->schedule,
            'issue'                  => $request->issue,
            'ms_service_statuses_id' => $status,
            'unit'                   => $unit,
            'technicians_id'         => $request->technician_id
        ];

        $data_item_detail = [];
        $data_inventory = [];
        if ($request->type_part == 'my-company' && $request->inventory_id != null && is_array($request->inventory_id)) {
            $inventory_id = $request->inventory_id;
            $inventory_price = $request->inventory_price;
            $inventory_qty = $request->inventory_qty;
            foreach ($request->inventory_id as $key => $val) {
                $check_inventory = !empty($inventory_id[$key]) && !empty($inventory_qty[$key]);

                if ($check_inventory) {
                    $inventory = Inventory::where('id', $inventory_id[$key])->firstOrFail();
                    if ($inventory_qty[$key] > $inventory->stock_available) {
                        throw new \Exception($inventory->item_name . ' melebihi qty yg tersedia');
                    }
                    $inventoryService = new InventoryService();
                    $inventoryService->mutationLogsProcess($inventory_id[$key], null, $inventory_qty[$key], $order_id, $request->technician_id);
                    $data_inventory[] = [
                        'data' => [
                            // 'stock_out' => $inventory->stock_out + $inventory_qty[$key]
                            'stock_now' => $inventory->stock_now - $inventory_qty[$key]
                        ],
                        'id' => $inventory->id,
                    ];
                    $data_item_detail[] = [
                        'orders_id'      => $order_id,
                        'inventory_id'   => $val,
                        'name_product'   => $inventory->item_name,
                        'quantity'       => $inventory_qty[$key],
                        'price'          => $inventory->cogs_value,
                    ];
                }
            }
        }

        $data_sparepart_detail = [];
        if ($request->type_part == 'my-inventory') {
            $teknisi_sparepart_name = $request->teknisi_sparepart_name;
            $teknisi_sparepart_price = $request->teknisi_sparepart_price;
            $teknisi_sparepart_qty = $request->teknisi_sparepart_qty;
            foreach ($request->teknisi_sparepart_name as $key => $val) {
                $check_part = !empty($teknisi_sparepart_name[$key]) && !empty($teknisi_sparepart_price[$key]) && !empty($teknisi_sparepart_qty[$key]);
                if ($check_part) {
                    $data_sparepart_detail[] = [
                        'orders_id'                => $order_id,
                        // 'technicians_sparepart_id' => $request->technician_id,
                        'technicians_id' => $request->technician_id,
                        'name_sparepart'           => $teknisi_sparepart_name[$key],
                        'quantity'                 => $teknisi_sparepart_qty[$key],
                        'price'                    => $teknisi_sparepart_price[$key],
                    ];
                }
            }
        }

        $result_deduction += ($unit * $price_service->commission_deduction);
        $data_order = [
            'id' => $order_id,
            'product_group_name' => $productgrup->name,
            'ms_ticket_id'       => $request->ticket_id,
            'ms_service_id'      => $request->ms_service_id,
            'product_group_id'      => $request->product_group_id,
            'ms_symptom_id'      => $request->symptom_id,
            'schedule'           => $request->schedule . ' ' . $request->hours,
            'note'               => $request->note,
            'address_type_id'    => $addr->ms_address_type_id,
            'address_type_name'    => $addr->types->name,
            'address'            => $addr->address,
            'users_id'           => $request->user_id,
            'grand_total'        => $request->grand_total,
            'code'               => $this->generateOrderCode(),
            'orders_statuses_id' => $status,
            'estimation_hours'   => $request->estimation_hours,
            'customer_json'      => json_encode($user),
            'customer_email' => $user->email,
            'customer_phone' => $user->phone,
            'service_name' => $service->name,
            'symptom_name' => $symptom->name,
            'symptom_detail' => $request->symptom_detail,
            'bill_to' => $request->bill_to,
            'is_less_balance'    => ($request->is_less_balance == 1) ? 1 : null,
            'is_approve' => 1,
            'payment_type' => $request->status == 'on' ? 1 : 0,
            'after_commission' => !empty($totalPart) ? ($totalPart + $result_deduction) : $result_deduction,
            'is_canceled'          => 0,
            'canceled_at'          => null
        ];



        $data_schedule = [
            'order_id' => $order_id,
            'technician_id' => $request->technician_id,
            'schedule_date' => $request->tech_schedule,
            'start_work' => $request->tech_schedule,
            'end_work' => $end_work->format('Y-m-d H:i:s'),
        ];

        $jadwal_teknisi = $this->checkJadwalTeknisi($request->technician_id, $request->tech_schedule, $end_work->format('Y-m-d H:i:s'));
        if ($jadwal_teknisi != null) {
            throw new SendErrorMsgException('schedule date has been used');
        }

        $debug = [
            'data_schedule' => $data_schedule,
            'jadwal_teknisi' => $jadwal_teknisi,
            'data_service_detail' => $data_service_detail,
            'data_order' => $data_order,
            'data_item_detail' => $data_item_detail,
            'data_sparepart_detail' => $data_sparepart_detail
        ];

        // return $debug;

        DB::beginTransaction();
        try {
            // insert order
            $order = Order::create($data_order);
            // inser schedule technician
            TechnicianSchedule::create($data_schedule);
            // history
            $this->createHistory($order->id, $order->orders_statuses_id);
            // insert service detail
            ServiceDetail::create($data_service_detail);
            // insert item detail
            if (count($data_item_detail) > 0) {
                ItemDetail::insert($data_item_detail);
            }
            // insert sparepart detail
            if (count($data_sparepart_detail) > 0) {
                SparepartDetail::insert($data_sparepart_detail);
            }

            // inventory update stok
            foreach ($data_inventory as $key => $data) {
                Inventory::where('id', $data['id'])->update($data['data']);
            }

            // WALLET
            if ($request->status == 'on' && $wallet != null) {
                // insert history wallet
                EwalletHistory::create($data_history_wallet);
                // update wallet
                $wallet->update($data_wallet);
                // insert wallet payment
                EwalletPayment::create($data_wallet_payment);
                // SALDO TEKNISI
                TechnicianSaldo::create($data_saldo_teknisi);
            }

            // media
            if ($request->hasfile('files')) {
                $file = $request->file('files');

                foreach ($file as $image) {
                    $name_file = rand(999999, 000000) . date("Y-m-d-H:i:s") . '.' . $image->getClientOriginalExtension();
                    $extension = $image->getClientOriginalExtension();
                    $media_data = new OrderMediaProblem;
                    $media_data->extension = $extension;
                    $media_data->type = $this->checkTypeMedia($extension);
                    $media_data->orders_id = $order->id;
                    $media_data->filename = $name_file;
                    $media_data->save();
                    $image->storeAs('public/order_media_problem/', $name_file);
                }
            }

            // ticketd
            if ($request->ticket_id != null) {
                MsTicketDetail::create([
                    'ms_ticket_id' => $request->ticket_id,
                    'user_id'      => Auth::id(),
                    'order_id'      => $order->id,
                    'message'      => 'New Order Created',
                ]);

                MsTicket::where('id', $request->ticket_id)->update([
                    'order_id'      => $order->id,
                ]);
            }

            if ($request->chat_notif != null) {
                (new ChatService())->sendOrderHistory($order->users_id, $price_service->technician->user->id, $order->id);
            }

            if ($request->email_notif != null) {
                $orderq = Order::with(
                    'orderpayment',
                    'user.ewallet',
                    'service',
                    'symptom',
                    'sparepart_detail',
                    'service_detail.symptom',
                    'service_detail.services_type.product_group',
                    'service_detail.technician.user',
                    'service_detail.price_service.service_type.symptom.services',
                    'history_order.order_status',
                    'order_status',
                    'user'
                )->where('id', $order->id)->first();
                // return $orderq;
                $common = new AdminController();
                $common->sendEmailOrder($orderq);
            }
            $orders = Order::with(
                'orderpayment',
                'user.ewallet',
                'service',
                'symptom',
                'sparepart_detail',
                'service_detail.symptom',
                'service_detail.services_type.product_group',
                'service_detail.technician.user',
                'service_detail.price_service.service_type.symptom.services',
                'history_order.order_status',
                'order_status',
                'user'
            )->where('id', $order->id)->first();
            $this->orderLog('create', null, $orders->id, $orders->order_status->name);

            (new NotifikasiService())->kirimNotifikasiOrder($order);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $order;
    }

    public function createFromCustomer(Request $request)
    {
        // dd($request->total_prices);
        $status = 2;
        if (isset($request->status)) {
            $status = $request->status;
        }
        $service_type = MsServicesType::where('id', $request->service_type_id)->firstOrfail();
        $productgrup = ProductGroup::where('id', $request->product_group_id)->firstOrfail();
        $service = Services::where('id', $request->ms_service_id)->firstOrfail();
        $symptom = MsSymptom::where('id', $request->symptom_id)->firstOrfail();
        $wallet = Ewallet::where('user_id', Auth::user()->id)->first();
        // if ($wallet == null) {
        //     throw new \Exception('Your Balance is not sufficient for This service price');
        // }
        $addr = MsAddress::with('types')->where('user_id', Auth::id())->where('id', $request->address_id)->firstOrFail();
        $user = User::with('info', 'address', 'ewallet')->where('id', Auth::user()->id)->firstOrFail();

        $teknisi = Technician::with('user')->where('id', $request->teknisi_id)->firstOrFail();
        $price_service = MsPriceService::where('ms_services_types_id', $request->service_type_id)->where('technicians_id', $teknisi->id)->firstOrFail();

        $data_order = [
            'ms_service_id'        => $request->ms_service_id,
            'ms_symptom_id'        => $request->symptom_id,
            'product_group_id'     => $request->product_group_id,
            'schedule'             => $request->schedule,
            'note'                 => $request->note,
            'address_type_id'      => $addr->ms_address_type_id,
            'address'              => $addr->address,
            'grand_total'          => $price_service->value,
            'users_id'             => Auth::user()->id,
            'code'                 => $this->generateOrderCode(),
            'symptom_detail'       => $request->symptom_detail,
            'orders_statuses_id'   => $status,
            'address_type_name'     => $addr->types->name,
            'product_group_name'    => $productgrup->name,
            'product_name_detail'  => $request->product_name,
            'brand_name_detail'    => $request->brand_name,
            'model_name_detail'    => $request->model_name,
            'serial_number_detail' => $request->serial_number,
            'remark_detail'        => $request->remark,
            'customer_json'        => json_encode($user),
            'customer_email'       => $user->email,
            'customer_phone'       => $user->phone,
            'service_name'         => $service->name,
            'symptom_name'         => $symptom->name,
            'bill_to'              => $addr->address,
            'is_approve'           => 0,
            'is_canceled'          => 0,
            'canceled_at'          => null
        ];


        if (empty($wallet->nominal)) {
            throw new SendErrorMsgException('Your Balance is not sufficient for This service price');
        }


        if ($wallet->nominal < $price_service->value) {
            throw new SendErrorMsgException('Your Balance is not sufficient for This service price');
        }

        $jadwal = date('Y-m-d H:i:s', strtotime($request->schedule));
        $orderSchedule = ServiceDetail::where('schedule', $jadwal)
            ->where('technicians_id', $request->teknisi_id)
            ->whereHas('order', function ($q) {
                $q->where('orders_statuses_id', 2);
            })
            ->first();

        if ($orderSchedule != null) {
            throw new SendErrorMsgException('schedule date has been used');
        }

        DB::beginTransaction();
        try {
            // create order
            $order = Order::create($data_order);

            $getDataOrder = Order::where('id', $order->id)
                ->with([
                    'user.ewallet',
                    'user.address',
                    'service',
                    'symptom',
                    'order_status',
                    'sparepart_detail',
                    'service_detail.symptom',
                    'service_detail.services_type',
                    'service_detail.price_service',
                    'user.ewallet'
                ])
                ->firstOrFail();



            // create history
            $this->createHistory($order->id, $order->orders_statuses_id);
            $find_pdi = ProductDetailInformation::where([
                ['product_name', 'like', '%' . $request->product_name . '%'],
                ['brand_name', 'like', '%' . $request->brand_name . '%'],
                ['model_name', 'like', '%' . $request->model_name . '%'],
                ['serial_number', 'like', '%' . $request->serial_number . '%'],
                ['remark', 'like', '%' . $request->remark . '%']
            ])->count();
            if ($find_pdi == 0) {
                ProductDetailInformation::create([
                    'product_name' => $request->product_name,
                    'brand_name' => $request->brand_name,
                    'model_name' => $request->model_name,
                    'serial_number' => $request->serial_number,
                    'remark' => $request->remark,
                    'product_group_name' => ProductGroup::where('id', $request->product_group_id)->value('name'),
                    'user_id' => Auth::user()->id
                ]);
            }

            if ($request->service_type_id != null) {
                ServiceDetail::create([
                    'orders_id'              => $order->id,
                    'price'                  => $price_service->value,
                    'ms_services_types_id'   => $request->service_type_id,
                    'ms_price_services_id'   => $price_service->id,
                    'teknisi_json'           => json_encode($price_service->technician),
                    'teknisi_email'          => $price_service->technician->user->email,
                    'teknisi_phone'          => $price_service->technician->user->phone,
                    'ms_symptoms_id'         => $request->symptom_id,
                    'symptom_name'           => $symptom->name,
                    'service_type_name'      => $service_type->name,
                    'schedule'               => $request->schedule,
                    'issue'                  => $request->issue,
                    'ms_service_statuses_id' => $status,
                    'unit'                   => 1,
                    'technicians_id'         => $request->teknisi_id
                ]);
            }

            // transaction History
            $transactionHistory = TransactionHistory::create([
                'type_history'      => 'Create Order',
                'order_code'        => $order->code,
                'order_id'          => $order->id,
                'service_type'      => $order->service->name,
                'symptom_name'      => $order->symptom->name,
                'product_group_name' => ProductGroup::where('id', $request->product_group_id)->value('name'),
                'price'             => $price_service->value,
                'beginning_balance' => Auth::user()->ewallet->nominal,
                'ending_balance'    => Auth::user()->ewallet->nominal - $price_service->value,
                'number_of_pieces'  => $price_service->value,
                'status'            => $status,
                'technician_name'   => $request->teknisi_id,
                'parts'             => null
            ]);

            $createWallet = order::where('id', $order->id)->update([
                'wallet_customer' => $getDataOrder->user->ewallet->nominal,
            ]);

            // history wallet order

            $waletHistoryCreate = EwalletHistory::create([
                'ms_wallet_id' => $getDataOrder->user->ewallet->id,
                'type_transaction_id' => 1, //status payment
                'saldo' => $getDataOrder->user->ewallet->nominal - $price_service->value,
                'nominal' => $price_service->value,
                'user_id' => $getDataOrder->user->id,
            ]);

            $createEwalletPay = EwalletPayment::create([
                'order_id' => $order->id,
                'wallet_history_id' => $waletHistoryCreate->id
            ]);

            $createWallet = Ewallet::where('user_id', $getDataOrder->user->id)->update([
                'nominal' => $getDataOrder->user->ewallet->nominal - $price_service->value,
            ]);

            $technicianSaldo = TechnicianSaldo::create([
                'technician_id' => $request->teknisi_id,
                'order_id' => $order->id
            ]);

            $name_file = null;
            if ($request->hasfile('files')) {
                $file = $request->file('files');
                foreach ($file as $image) {
                    $name_file = rand(999999, 000000) . date("Y-m-d-H:i:s") . '.' . $image->getClientOriginalExtension();
                    $extension = $image->getClientOriginalExtension();
                    $media_data = new OrderMediaProblem;
                    $media_data->extension = $extension;
                    $media_data->type = $this->checkTypeMedia($extension);
                    $media_data->orders_id = $order->id;
                    $media_data->filename = $name_file;
                    $media_data->save();
                    $image->storeAs('public/order_media_problem/', $name_file);
                }
            }


            $order = Order::with(
                'orderpayment',
                'user.ewallet',
                'service',
                'symptom',
                'sparepart_detail',
                'service_detail.symptom',
                'service_detail.services_type.product_group',
                'service_detail.technician.user',
                'service_detail.price_service.service_type.symptom.services',
                'history_order.order_status',
                'order_status',
                'user'
            )->where('id', $order->id)->first();
            // $common = new AdminController();
            // $common->sendEmailOrder($order);

            // $order = Order::with('order_status', 'user')->where('id', $order->id)->first();
            // $email_status_order = EmailStatusOrder::where('orders_statuses_id', $order->orders_statuses_id)->first();
            // event(new OrderEvent($order, $email_status_order));
            $this->orderLog('create', null, $order->id, $order->order_status->name);
            (new ChatService())->sendOrderHistory($order->detail->technician->user->id, $order->users_id, $order->id);
            (new NotifikasiService())->kirimNotifikasiOrder($order);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $order;
    }

    public function uploadGalery($request)
    {

        DB::beginTransaction();
        try {
            $name_file = null;
            if ($request->hasfile('filename')) {
                $file = $request->file('filename');

                foreach ($file as $image) {
                    $name_file = rand(999999, 000000) . date("Y-m-d-H:i:s") . '.' . $image->getClientOriginalExtension();
                    GaleryImages::create([
                        'filename' => $name_file,
                        'user_id' => Auth::user()->id
                    ]);
                    $image->storeAs('public/galery_images/', $name_file);
                }
                // dd($galery_store);
            }

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }
    }

    public function checkTypeMedia($extension_file)
    {
        $image = ['jpeg', 'jpg', 'png'];
        $video = ['mp4', 'm4a', 'mkv'];

        $merge = array_merge($image, $video);

        if (!in_array($extension_file, $merge)) {
            throw new \Exception('File Not Support');
        }

        $type = '';
        if (in_array($extension_file, $image)) {
            $type = 'image';
        } else {
            $type = 'video';
        }

        return $type;
    }

    public function generateOrderCode()
    {
        $q          = Order::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmY');
        $number     = 1; //format
        $new_number = sprintf("%06s", $number);
        $code       = $date . ($separator) . ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = Order::orderBy('id', 'desc')->value('code');
            $last     = explode("-", $last);

            $order_code = $date . ($separator) . (sprintf("%06s", $last[1] + 1));
        }
        return $order_code;
    }

    public function createHistory($order_id, $status_id)
    {
        return HistoryOrder::create([
            'orders_id'          => $order_id,
            'orders_statuses_id' => $status_id
        ]);
    }

    public function hasReview()
    {
        return Order::with([
            'user',
            'symptom',
            'service',
            'order_status',
            'service_detail.technician.user',
            'service_detail.symptom',
            'service_detail.services_type',
            'rating_and_review.review',
            'rating_and_review.rating',
        ])->whereNotNull('rating_and_review_id');
    }

    public function dontHaveReview()
    {
        return Order::with([
            'user',
            'symptom',
            'service',
            'order_status',
            'service_detail.technician.user.info',
            'service_detail.symptom',
            'service_detail.services_type',
        ])
            ->whereNull('rating_and_review_id')
            ->where('orders_statuses_id', 10);
    }

    public function getStatusDesc($history_order)
    {
        $color = '';
        $text = $history_order->order_status->name;

        return [
            'color' => $color,
            'text' => $text,
        ];
    }

    /**
     * order log
     */
    public function orderLog($func_name, $request, $order_id = null, $order_status = null)
    {
        // warning !! details info parameters
        // func_name = hanya untuk selain perubahan order status
        // request = parameter berupa request atau masukan data (array/object)s dan bisa string system.
        $changed_field = [];
        $user_name = '';
        if ($request === 'system') {
            $user_name = 'System Automatically';
        } else {
            $user_name = User::where('id', Auth::user()->id)->value('name');
        }
        $timestamp = Carbon::now()->format('d-m-Y H:i:s');

        $fields = [];
        if ($func_name == null) {
            $content = 'Changed by <b>' . strtoupper($user_name) . '</b> and order status to <b>' . strtoupper($order_status) . '</b>, at <span class="text-danger">' . $timestamp . '</span>';
        } else if ($func_name == 'create') {
            $content = 'Created by <b>' . strtoupper($user_name) . '</b> and order status <b>' . strtoupper($order_status) . '</b>, at <span class="text-danger">' . $timestamp . '</span>';
        } else {
            if ($func_name == 'updateBillDetail') {
                $data = Order::where('id', $order_id)->first(['address', 'bill_to']);
                if ($data->address != $request->bill_address) {
                    array_push($fields, 'Billing Address');
                }
                if ($data->bill_to != $request->bill_to) {
                    array_push($fields, 'Bill To');
                }
            } else if ($func_name == 'updateSparepartDetail') {
                if ($request->type_part == 'my-inventory') {
                    $sparepart_detail_id = $request->sparepart_detail_id;
                    $teknisi_sparepart_name = $request->teknisi_sparepart_name;
                    $teknisi_sparepart_price = $request->teknisi_sparepart_price;
                    $teknisi_sparepart_qty = $request->teknisi_sparepart_qty;
                    foreach ($sparepart_detail_id as $key => $value) {
                        $check_part = !empty($teknisi_sparepart_name[$key]) && !empty($teknisi_sparepart_price[$key]) && !empty($teknisi_sparepart_qty[$key]);
                        if ($check_part) {
                            if ($value != 0) {
                                $matching = SparepartDetail::where('id', $value)->first();
                                if (!($matching->name_sparepart == $teknisi_sparepart_name[$key] && $matching->price == $teknisi_sparepart_price[$key] && $matching->quantity == $teknisi_sparepart_qty[$key])) {
                                    array_push($fields, 'tech_part_update(' . $teknisi_sparepart_name[$key] . ',' . $teknisi_sparepart_price[$key] . ',' . $teknisi_sparepart_qty[$key] . ')');
                                }
                            } else {
                                array_push($fields, 'tech_part_new(' . $teknisi_sparepart_name[$key] . ',' . $teknisi_sparepart_price[$key] . ',' . $teknisi_sparepart_qty[$key] . ')');
                            }
                        }
                    }
                } else {
                    $item_detail_id = isset($request->item_detail_id) ? $request->item_detail_id : null;
                    $inventory_id = $request->inventory_id;
                    $inventory_qty = $request->inventory_qty;
                    if($item_detail_id) {
                        foreach ($item_detail_id as $key => $val) {
                            $check_inventory = !empty($inventory_id[$key]) && !empty($inventory_qty[$key]);
                            if ($check_inventory) {
                                $inventory = Inventory::where('id', $inventory_id[$key])->firstOrFail();
                                if ($val == 0) {
                                    array_push($fields, 'company_part_update(' . $inventory->item_name[$key] . ',' . $inventory->cogs_value[$key] . ',' . $inventory_qty[$key] . ')');
                                } else {
                                    array_push($fields, 'company_sparepart_new(' . $inventory->item_name[$key] . ',' . $inventory->cogs_value[$key] . ',' . $inventory_qty[$key] . ')');
                                }
                            }
                        }
                    }
                }
            } else if ($func_name == 'destroySparepartDetail') {
                array_push($fields, 'tech_part_delete(' . $request->name_sparepart . ',' . $request->price . ',' . $request->quantity . ')');
            } else if ($func_name == 'destroyItemDetail') {
                array_push($fields, 'company_part_delete(' . $request->name_product . ',' . $request->price . ',' . $request->quantity . ')');
            } else if ($func_name == 'assignNewTeknisi') {
                $price_service = MsPriceService::with('technician.user')
                    ->where('id', $request->teknisi_ms_price_services_id)
                    ->where('technicians_id', $request->technician_id)
                    ->firstOrFail();
                array_push($fields, 'assign new technician: ' . $price_service->technician->user->name . ' - ' . $price_service->technician->user->email);
            }

            $content = 'Changed "<b>' . implode(', ', $fields) . '"</b> by <b>"' . strtoupper($user_name) . '"</b>, at <span class="text-danger">' . $timestamp . '</span>';
        }

        return OrderLog::create([
            'order_id' => $order_id,
            'content' => $content
        ]);
    }

    /**
     * Media Order Upload
     */
    public function addMediaProblem($order_id, $request)
    {
        DB::beginTransaction();
        try {
            // media
            if ($request->hasfile('files')) {
                $file = $request->file('files');

                foreach ($file as $image) {
                    $name_file = rand(999999, 000000) . date("Y-m-d-H:i:s") . '.' . $image->getClientOriginalExtension();
                    $extension = $image->getClientOriginalExtension();
                    $media_data = new OrderMediaProblem();
                    $media_data->extension = $extension;
                    $media_data->type = $this->checkTypeMedia($extension);
                    $media_data->orders_id = $order_id;
                    $media_data->filename = $name_file;
                    $media_data->save();
                    $image->storeAs('public/order_media_problem/', $name_file);
                }
            }

            $order = Order::where('id', $order_id)->update([
                'symptom_detail' => $request->symptom_detail
            ]);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $order;
    }

    /**
     * datatables
     */
    public function datatables($request)
    {
        $query = Order::with([
            'orderpayment',
            'user.ewallet',
            'service',
            'ticket',
            'symptom',
            'sparepart_detail',
            'item_detail',
            'product_group',
            'service_detail.symptom',
            'service_detail.services_type',
            'service_detail.technician.user',
            'service_detail.price_service.service_type.symptom.services',
            'history_order.order_status',
            'order_status',
            'user',
            'garansi',
            'payment_method',
            'history_order'
        ]);
            
        if (!empty($request->order_status_id)) {
            $query->whereIn('orders_statuses_id', explode(',', $request->order_status_id));
        }

        if (!empty($request->klaim_garansi)) {
            $query->whereHas('garansi');
        }

        if (!empty($request->payment_type)) {
            $query->where('payment_type', $request->payment_type);
        }

        if (!empty($technician_id)) {
            // $query->where('users_id', $technician_id);
        }
        // setting duration auto cancel/jobs_done ;
        $settings_auto = GeneralSetting::where('id', 10)->value('value');
        if(!empty($request->abandoned_orders)) {
            $query->where(function ($q) {
                $q->where('orders_statuses_id', 2)
                ->orWhere('orders_statuses_id', 7);
            })
            ->whereHas('history_order', function ($q2) use($settings_auto) {
               $q2->where(DB::raw("DATE_ADD(created_at, INTERVAL ".$settings_auto." HOUR)"), '<', Carbon::now()->addMinutes(30))
                ->where(function ($q3) {
                    $q3->where('orders_statuses_id', 2)
                    ->orWhere('orders_statuses_id', 7);
                });
            });

        }


        if (!empty($request->order_date_range)) {
            $range = explode(' - ', $request->order_date_range);
            $date_from = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $range[0])));
            $date_to = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $range[1])));
            $query->where('schedule', '>=', $date_from)
                ->where('schedule', '<=', $date_to);
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatablesCustomer($request)
    {
        $query = Order::with([
            'orderpayment',
            'user.ewallet',
            'service',
            'symptom',
            'sparepart_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician.user',
            'service_detail.price_service.service_type.symptom.services',
            'history_order.order_status',
            'order_status',
            'user',
            'payment_method'
        ]);

        if (!empty($request->order_status_id)) {
            $query->whereIn('orders_statuses_id', explode(',', $request->order_status_id));
        }

        if (!empty($request->klaim_garansi)) {
            $query->whereHas('garansi');
        }

        if (!empty($request->payment_type)) {
            $query->where('payment_type', $request->payment_type);
        }


        if (!empty($request->order_date_range)) {
            $range = explode(' - ', $request->order_date_range);
            $date_from = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $range[0])));
            $date_to = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $range[1])));
            $query->where('schedule', '>=', $date_from)
                ->where('schedule', '<=', $date_to);
        }

        $query->where('users_id', Auth::id());

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatablesTeknisi($request)
    {
        $query = Order::with([
            'orderpayment',
            'user.ewallet',
            'service',
            'symptom',
            'product_group',
            'sparepart_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician.user.info',
            'service_detail.price_service.service_type.symptom.services',
            'history_order.order_status',
            'order_status',
            'user',
            'payment_method'
        ]);

        if (!empty($request->order_status_id)) {
            $query->whereIn('orders_statuses_id', explode(',', $request->order_status_id));
        }

        if (!empty($request->klaim_garansi)) {
            $query->whereHas('garansi', function ($query) {
                $query->where('status', '!=', 'done')->where('customer_can_reply', 1);
            });
        }

        if (!empty($request->payment_type)) {
            $query->where('payment_type', $request->payment_type);
        }


        if (!empty($request->order_date_range)) {
            $range = explode(' - ', $request->order_date_range);
            $date_from = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $range[0])));
            $date_to = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $range[1])));
            $query->where('schedule', '>=', $date_from)
                ->where('schedule', '<=', $date_to);
        }

        $query->whereHas('service_detail.technician', function ($query) use ($request) {
            $query->where('user_id', Auth::user()->id);
        });

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function sendEmailOrder($modul, $name_module = null, $type = null)
    {
        $datas = null;
        if ($name_module != null) {
            $datas = EmailOther::where('name_module', $name_module);
            if ($type != null) {
                $datas = $datas->where('type', $type);
            }
            $datas = $datas->get();
        } else {
            $datas = EmailStatusOrder::where('orders_statuses_id', $modul->orders_statuses_id)->get();
        }
        foreach ($datas as $data) {
            event(new OrderEvent($modul, $data));
        }
    }

    public function checkJadwalTeknisi($teknisi_id, $date_start, $date_end)
    {
        // SELECT * FROM `technician_schedules` WHERE schedule_date = '2020-12-22' AND (
        //     (start_work < '2020-12-22 14:00:00' AND '2020-12-22 14:00:00' < end_work)
        //     OR (start_work < '2020-12-22 17:00:00' AND '2020-12-22 17:00:00' < end_work)
        //     OR ('2020-12-22 14:00:00' < start_work AND start_work < '2020-12-22 17:00:00')
        // )

        $jadwal_teknisi = TechnicianSchedule::where('technician_id', $teknisi_id)
            ->where(function ($query) use ($date_start, $date_end) {
                $query->where(function ($query) use ($date_start) {
                    $query->where('start_work', '<=', $date_start)->whereRaw('? <= end_work', [$date_start]);
                })->orWhere(function ($query) use ($date_end) {
                    $query->where('start_work', '<=', $date_end)->whereRaw('? <= end_work', [$date_end]);
                })->orWhere(function ($query) use ($date_start, $date_end) {
                    $query->whereRaw('? <= start_work', [$date_start])->where('start_work', '<=', $date_end);
                });
            })
            ->first();

        return $jadwal_teknisi;
    }

    // fungsi ini hanya untuk admin
    public function updateSparepart($order_id, $request)
    {
        $order = Order::with(['orderpayment.orderhistory', 'user.masterwallet.history', 'service_detail'])
            ->where('id', $order_id)
            ->firstOrFail();

        $service_details = $order->service_detail;
        $total_service_detail = $service_details->sum(function ($row) {
            return ($row->price * $row->unit);
        });

        $total_sparepart = 0;
        $data_inventory_update = [];
        $data_item_detail_update = [];
        $data_item_detail_insert = [];
        $sparepart_detail_update = [];
        $sparepart_detail_insert = [];
        $data_order_update = [];
        $data_ewallet_history_update = [];

        // part my inventory
        if ($request->type_part == 'my-inventory') {

            $sparepart_detail_id = $request->sparepart_detail_id;
            $teknisi_sparepart_name = $request->teknisi_sparepart_name;
            $teknisi_sparepart_price = $request->teknisi_sparepart_price;
            $teknisi_sparepart_qty = $request->teknisi_sparepart_qty;

            // loop request
            foreach ($sparepart_detail_id as $key => $value) {
                // check request
                $check_part = !empty($teknisi_sparepart_name[$key]) && !empty($teknisi_sparepart_price[$key]) && !empty($teknisi_sparepart_qty[$key]);

                if ($check_part) {
                    // jika id tidak 0 == update
                    if ($value != 0) {
                        $sparepart_detail_update[] = [
                            'id' => $value,
                            'name_sparepart' => $teknisi_sparepart_name[$key],
                            'quantity' => $teknisi_sparepart_qty[$key],
                            'price' => $teknisi_sparepart_price[$key],
                        ];
                        $total_sparepart += $teknisi_sparepart_price[$key] * $teknisi_sparepart_qty[$key];
                    } else { // jika id 0 == insert
                        $sparepart_detail_insert[] = [
                            'orders_id' => $order->id,
                            'technicians_id' => $service_details[0]->technicians_id,
                            'name_sparepart' => $teknisi_sparepart_name[$key],
                            'quantity' => $teknisi_sparepart_qty[$key],
                            'price' => $teknisi_sparepart_price[$key],
                        ];
                        $total_sparepart += $teknisi_sparepart_price[$key] * $teknisi_sparepart_qty[$key];
                    }
                }
            }
        }

        if ($request->type_part == 'my-company') {
            $item_detail_id = $request->item_detail_id;
            $inventory_id = $request->inventory_id;
            $inventory_qty = $request->inventory_qty;
            // loop request

            // if($item_detail_id) {
                foreach ($item_detail_id as $key => $val) {
                    // check request
                    $check_inventory = !empty($inventory_id[$key]) && !empty($inventory_qty[$key]);

                    if ($check_inventory) {

                        // check qty
                        $inventory = Inventory::where('id', $inventory_id[$key])->firstOrFail();
                        if ($inventory_qty[$key] > $inventory->stock_available) {
                            throw new \Exception($inventory->item_name . ' melebihi qty yg tersedia');
                        }

                        if ($val == 0) {
                            $data_inventory_update[] = [
                                // 'stock_out' => $inventory->stock_out + $inventory_qty[$key],
                                'stock_now' => $inventory->stock_now - $inventory_qty[$key],
                                'id' => $inventory->id
                            ];

                            $data_item_detail_insert[] = [
                                'orders_id'      => $order_id,
                                'inventory_id'   => $inventory->id,
                                'name_product'   => $inventory->item_name,
                                'quantity'       => $inventory_qty[$key],
                                'price'          => $inventory->cogs_value,
                            ];

                            $total_sparepart += $inventory->cogs_value * $inventory_qty[$key];
                        } else {
                            $itemD = ItemDetail::where('id', $val)->first();

                            $data_inventory_update[] = [
                                // 'stock_out' => $inventory->stock_out - ($itemD->quantity - $inventory_qty[$key]),
                                'stock_now' => $inventory->stock_now + ($itemD->quantity - $inventory_qty[$key]),
                                'id' => $inventory->id
                            ];

                            $data_item_detail_update[] = [
                                'id' => $itemD->id,
                                'data' => [
                                    'inventory_id'   => $inventory->id,
                                    'name_product'   => $inventory->item_name,
                                    'quantity'       => $inventory_qty[$key],
                                    'price'          => $inventory->cogs_value,
                                ]
                            ];

                            $total_sparepart += $inventory->cogs_value * $inventory_qty[$key];
                        }
                    }
                }

            // }

            $sparepart_details = ItemDetail::where('orders_id', $order_id)->get();
            $total_sparepart_detail = $sparepart_details->sum(function ($row) {
                return ($row->price * $row->quantity);
            });
        }

        $grand_total = $total_service_detail + $total_sparepart;
        $result_deduction = ($service_details[0]->unit * $service_details[0]->price_services->commission_deduction);
        $hargaSparepart = $total_sparepart;

        if ($order->payment_type == 1) {
            if ($order->user->masterwallet == null) {
                throw new \Exception('This user does not have a wallet balance');
            }
            $wallet_customer_nominal = $order->user->masterwallet->nominal;
            if ($wallet_customer_nominal >= $total_sparepart) {
                $wallet_history =  EwalletHistory::where('id', $order->orderpayment->wallet_history_id)->first();
                $history_wallet_nominal = $wallet_history == null ? 0 : $wallet_history->nominal;
                $data_ewallet_history_update = [
                    'nominal' => $hargaSparepart + $total_service_detail,
                    'saldo' => $wallet_customer_nominal - (($hargaSparepart + $total_service_detail) - $history_wallet_nominal),
                ];
                $data_ewallet_update = [
                    'nominal' => $wallet_customer_nominal - (($hargaSparepart + $total_service_detail) - $history_wallet_nominal),
                ];
            } else {
                throw new \Exception('The wallet balance of this user is not sufficient for the total service price');
            }
        }

        $data_order_update = [
            'grand_total' => $grand_total,
            'after_commission' => $hargaSparepart > 0 ? ($hargaSparepart + $result_deduction) : $result_deduction,
        ];

        // return [
        //     'request' => $request->all(),
        //     'total_sparepart' => $total_sparepart,
        //     'data_order_update'  => $data_order_update,
        //     'data_ewallet_history_update' => $data_ewallet_history_update,
        //     'data_ewallet_update' => $data_ewallet_update
        // ];

        DB::beginTransaction();
        try {
            // update sparrepart
            if (count($sparepart_detail_update) > 0) {
                foreach ($sparepart_detail_update as $data) {
                    SparepartDetail::where('id', $data['id'])->update([
                        'name_sparepart' => $data['name_sparepart'],
                        'quantity' => $data['quantity'],
                        'price' => $data['price'],
                    ]);
                }
            }

            // insert sparrepart
            if (count($sparepart_detail_insert) > 0) {
                SparepartDetail::insert($sparepart_detail_insert);
            }

            // update ItemDetail
            if (count($data_item_detail_update) > 0) {
                foreach ($data_item_detail_update as $data) {
                    ItemDetail::where('id', $data['id'])->update($data['data']);
                }
            }

            // insert ItemDetail
            if (count($data_item_detail_insert) > 0) {
                ItemDetail::insert($data_item_detail_insert);
            }

            // update Inventory
            if (count($data_inventory_update) > 0) {
                foreach ($data_inventory_update as $data) {
                    Inventory::where('id', $data['id'])->update([
                        // 'stock_out' => $data['stock_out'],
                        'stock_now' => $data['stock_now'],

                    ]);
                }
            }

            // update order
            $order->update($data_order_update);

            // wallet
            if ($order->payment_type == 1) {
                EwalletHistory::where('id', $order->orderpayment->wallet_history_id)->update($data_ewallet_history_update);
                Ewallet::where('user_id', $order->user->id)->update($data_ewallet_update);
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function getHoursAvailable($request)
    {
        $schedule = TechnicianSchedule::where(['technician_id' => $request->technician_id, 'schedule_date' =>  $request->schedule, 'is_deleted' => 0])->get(['start_work', 'end_work']);
        $schedule_service_detail = ServiceDetail::where(['technicians_id' => $request->technician_id])->whereDate('schedule', $request->schedule)->whereHas('order', function ($item) {
            $item->where('orders_statuses_id', 2);
        })->get(['schedule']);

        $jam_teknisi = [];
        $jam_teknisi_hold = [];
        $jam_hold_temp = [];
        $jam_kerja_teknisi = [];
        $hours = 24;
        $break_time = 1;
        $min_work = 1;
        $jam_disable = [];
        if ($request->schedule == Carbon::now()->format('Y-m-d')) {
            $jam_sekarang = (int) Carbon::now()->format('H');
            if (Carbon::now()->format('i') > 0) {
                $jam_sekarang = (int) Carbon::now()->format('H') + 1;
            }
            for ($i = 0; $i <= $jam_sekarang; $i++) {
                $jam_disable[] = $i;
            }
        }

        $jadwal_teknisi_html = '';
        foreach ($schedule as $key => $data) {
            $start_work = (int) Carbon::parse($data->start_work)->format('H') - $break_time;
            $end_work = (int) Carbon::parse($data->end_work)->format('H');
            for ($i = $start_work; $i <= $end_work; $i++) {
                if (!in_array($i, $jam_teknisi)) {
                    $jam_teknisi[] = $i;
                }
            }
            for ($i = (int) Carbon::parse($data->start_work)->format('H'); $i <= (int) Carbon::parse($data->end_work)->format('H'); $i++) {
                $jam_kerja_teknisi[] = $i;
            }
            $jadwal_teknisi_html .= '<span class="badge badge-primary">' . Carbon::parse($data->start_work)->format('H:i') . '-' . Carbon::parse($data->end_work)->format('H:i') . '</span>&nbsp;';
        }

        foreach ($schedule_service_detail as $key => $data) {
            $start_work = (int) Carbon::parse($data->schedule)->format('H') - $break_time;
            $end_work = (int) Carbon::parse($data->schedule)->format('H');
            for ($i = $start_work; $i <= $end_work; $i++) {
                if (!in_array($i, $jam_teknisi)) {
                    $jam_hold_temp[] = $i;
                }
            }
        }

        $last_key = array_key_last($jam_teknisi);
        $jam_kerja_teknisi = Arr::sort(array_merge($jam_kerja_teknisi, $jam_hold_temp));
        $jam_teknisi = Arr::sort($jam_teknisi);
        $count = count($jam_teknisi);
        $add_on = [];

        for ($i = 0; $i < $hours; $i++) {
            if ($last_key != $i && array_key_exists($i, $jam_teknisi)) {
                $selisih = $jam_teknisi[$i + 1] - $jam_teknisi[$i];
                if (($selisih == $min_work)) {
                    $val = $jam_teknisi[$i + 1] - $break_time;
                    if (!in_array($val, $add_on)) {
                        $add_on[] = $val;
                    }
                }
            }
        }
        $jam_teknisi = array_merge($add_on, $jam_teknisi);
        $last_key = array_key_last($jam_teknisi);
        $selisih_min_work = [];
        foreach ($jam_teknisi as $key => $val) {
            if ($last_key != $key) {
                $selisih = $jam_teknisi[$key + 1] - $jam_teknisi[$key];
                if (($selisih < $min_work) && ($selisih != 1)) {
                    for ($j = $jam_teknisi[$key]; $j < $jam_teknisi[$key + 1]; $j++) {
                        $selisih_min_work[] = $j;
                    }
                }
            }
        }
        $jam_teknisi = array_merge(array_merge($jam_teknisi, $jam_hold_temp), $selisih_min_work);
        $jam_dipakai = Arr::sort(array_unique(array_merge($jam_teknisi, $jam_disable)));

        $final = [];
        $jam_available = [];
        $jam_terpakai = [];
        for ($i = 0; $i < $hours; $i++) {
            if (!in_array($i, $jam_dipakai)) {
                $final[] = str_replace($i, $i . ':00', $i) . ' is available';
                $jam_available[] = str_replace($i, $i . ':00', $i);
            } else {
                if (!in_array($i, $jam_disable)) {
                    if (in_array($i, $jam_hold_temp)) {
                        $jam_teknisi_hold[] = str_replace($i, $i . ':00', $i);
                        $final[] = str_replace($i, $i . ':00', $i) . ' is hold';
                    } else {
                    }
                }
                $jam_terpakai[] = str_replace($i, $i . ':00', $i);
                $final[] = str_replace($i, $i . ':00', $i);
            }
        }
        $is_available = array_filter(array_unique($final), function ($a) {
            return stristr($a, "is available") !== false;
        });
        $full_schedule = false;
        if ($is_available == null) {
            $full_schedule = true;
        }
        $data = [];
        $data['jam_available'] = $jam_available;
        $data['jadwal_teknisi'] = $jadwal_teknisi_html;
        $data['jam_teknisi_hold'] = $jam_teknisi_hold;
        $data['jam_disable'] = $jam_terpakai;
        $data['all_jam'] = $final;
        $data['jam_teknisi'] = $jam_kerja_teknisi;
        $data['add_on'] = $add_on;
        $data['selisih_min_work'] = $selisih_min_work;
        $data['full_schedule'] = $full_schedule;
        return $data;
    }

    public function getHoursAvailableB2b($request)
    {
        $schedule = B2BAssignTeknisi::where('b2b_technician_id', $request->technician_id)->where('schedule_date', $request->schedule)->where('is_deleted', 0)->get(['start_work', 'end_work']);
        // $schedule_service_detail = ServiceDetail::where(['technicians_id' => $request->technician_id])->whereDate('schedule', $request->schedule)->whereHas('order', function ($item) {
        //     $item->where('orders_statuses_id', 2);
        // })->get(['schedule']);

        $jam_teknisi = [];
        $jam_teknisi_hold = [];
        $jam_hold_temp = [];
        $jam_kerja_teknisi = [];
        $hours = 24;
        $break_time = 1;
        $min_work = 1;
        $jam_disable = [];
        if ($request->schedule == Carbon::now()->format('Y-m-d')) {
            $jam_sekarang = (int) Carbon::now()->format('H');
            if (Carbon::now()->format('i') > 0) {
                $jam_sekarang = (int) Carbon::now()->format('H') + 1;
            }
            for ($i = 0; $i <= $jam_sekarang; $i++) {
                $jam_disable[] = $i;
            }
        }

        $jadwal_teknisi_html = '';
        foreach ($schedule as $key => $data) {
            $start_work = (int) Carbon::parse($data->start_work)->format('H') - $break_time;
            $end_work = (int) Carbon::parse($data->end_work)->format('H');
            for ($i = $start_work; $i <= $end_work; $i++) {
                if (!in_array($i, $jam_teknisi)) {
                    $jam_teknisi[] = $i;
                }
            }
            for ($i = (int) Carbon::parse($data->start_work)->format('H'); $i <= (int) Carbon::parse($data->end_work)->format('H'); $i++) {
                $jam_kerja_teknisi[] = $i;
            }
            $jadwal_teknisi_html .= '<span class="badge badge-primary">' . Carbon::parse($data->start_work)->format('H:i') . '-' . Carbon::parse($data->end_work)->format('H:i') . '</span>&nbsp;';
        }

        // foreach ($schedule_service_detail as $key => $data) {
        //     $start_work = (int) Carbon::parse($data->schedule)->format('H') - $break_time;
        //     $end_work = (int) Carbon::parse($data->schedule)->format('H');
        //     for ($i = $start_work; $i <= $end_work; $i++) {
        //         if (!in_array($i, $jam_teknisi)) {
        //             $jam_hold_temp[] = $i;
        //         }
        //     }
        // }

        $last_key = array_key_last($jam_teknisi);
        $jam_kerja_teknisi = Arr::sort(array_merge($jam_kerja_teknisi, $jam_hold_temp));
        $jam_teknisi = Arr::sort($jam_teknisi);
        $count = count($jam_teknisi);
        $add_on = [];

        for ($i = 0; $i < $hours; $i++) {
            if ($last_key != $i && array_key_exists($i, $jam_teknisi)) {
                $selisih = $jam_teknisi[$i + 1] - $jam_teknisi[$i];
                if (($selisih == $min_work)) {
                    $val = $jam_teknisi[$i + 1] - $break_time;
                    if (!in_array($val, $add_on)) {
                        $add_on[] = $val;
                    }
                }
            }
        }
        $jam_teknisi = array_merge($add_on, $jam_teknisi);
        $last_key = array_key_last($jam_teknisi);
        $selisih_min_work = [];
        foreach ($jam_teknisi as $key => $val) {
            if ($last_key != $key) {
                $selisih = $jam_teknisi[$key + 1] - $jam_teknisi[$key];
                if (($selisih < $min_work) && ($selisih != 1)) {
                    for ($j = $jam_teknisi[$key]; $j < $jam_teknisi[$key + 1]; $j++) {
                        $selisih_min_work[] = $j;
                    }
                }
            }
        }
        $jam_teknisi = array_merge(array_merge($jam_teknisi, $jam_hold_temp), $selisih_min_work);
        $jam_dipakai = Arr::sort(array_unique(array_merge($jam_teknisi, $jam_disable)));

        $final = [];
        $jam_available = [];
        $jam_terpakai = [];
        for ($i = 0; $i < $hours; $i++) {
            if (!in_array($i, $jam_dipakai)) {
                $final[] = str_replace($i, $i . ':00', $i) . ' is available';
                $jam_available[] = str_replace($i, $i . ':00', $i);
            } else {
                if (!in_array($i, $jam_disable)) {
                    if (in_array($i, $jam_hold_temp)) {
                        $jam_teknisi_hold[] = str_replace($i, $i . ':00', $i);
                        $final[] = str_replace($i, $i . ':00', $i) . ' is hold';
                    } else {
                    }
                }
                $jam_terpakai[] = str_replace($i, $i . ':00', $i);
                $final[] = str_replace($i, $i . ':00', $i);
            }
        }
        $is_available = array_filter(array_unique($final), function ($a) {
            return stristr($a, "is available") !== false;
        });
        $full_schedule = false;
        if ($is_available == null) {
            $full_schedule = true;
        }
        $data = [];
        $data['jam_available'] = $jam_available;
        $data['jadwal_teknisi'] = $jadwal_teknisi_html;
        $data['jam_teknisi_hold'] = $jam_teknisi_hold;
        $data['jam_disable'] = $jam_terpakai;
        $data['all_jam'] = $final;
        $data['jam_teknisi'] = $jam_kerja_teknisi;
        $data['add_on'] = $add_on;
        $data['selisih_min_work'] = $selisih_min_work;
        $data['full_schedule'] = $full_schedule;
        return $data;
    }
}
