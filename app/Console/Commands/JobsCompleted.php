<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Model\Master\Order;
use App\Services\OrderService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Model\Master\GeneralSetting;
use App\Model\Master\TechnicianSaldo;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CustomerController;

class JobsCompleted extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobsCompleted:do';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Aotumatic Jobs Completed when customer doesnt press jobs completed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $settings = GeneralSetting::all();
        if($settings[9]->value !== "0") {
            $orders = Order::with([
                'orderpayment',
                'user.ewallet',
                'service',
                'symptom',
                'order_status',
                'sparepart_detail',
                'service_detail.symptom',
                'service_detail.services_type.product_group',
                'service_detail.technician.user',
                'service_detail.price_service',
                'history_order.order_status'
            ])
            ->where('orders_statuses_id', 7)
            ->whereHas('history_order', function ($q) use ($settings) {
                $q->where('orders_statuses_id', 7)->where('created_at', '<', Carbon::now()->subHours(intval($settings[9]->value)));
            })->where('created_at', '<', Carbon::now()->subHours(intval($settings[9]->value)))->get();

            foreach($orders as $key => $order) {
                try {
                    $orderService = new OrderService();
                    $order = $orderService->setOrder($order->id)
                    ->setChangeBySystem()
                    ->notSendNotif()
                    ->changeToComplate();
                    Log::info('autojobscompleted');
                } catch (QueryException $e) {
                    Log::error($e->getMessage());
                }
            }
        }

    }
}
