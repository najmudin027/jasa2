<?php

namespace App\Traits;

use App\Imports\CityImport;
use Illuminate\Http\Request;
use App\Imports\CountryImport;
use App\Imports\VillageImport;
use App\Imports\ZipcodeImport;
use App\Imports\DistrictImport;
use App\Imports\ProvinceImport;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

trait MasterImportTrait {


    public function importExcelTraits($type, $request)
	{


        $file = $request->file('file');
		$nama_file = rand().$file->getClientOriginalName();

        $data = '';
        if ($type === 'country') {
            $file->storeAs('public/file_country/',$nama_file);
            $data = Excel::import(new CountryImport, storage_path('app/public/file_country/'.$nama_file));
        } else if ($type === 'province') {
            $file->storeAs('public/file_province',$nama_file);
            $data = Excel::import(new ProvinceImport, storage_path('app/public/file_province/'.$nama_file));
        } else if ($type === 'city') {
            $file->storeAs('public/file_city',$nama_file);
            $data = Excel::import(new CityImport, storage_path('app/public/file_city/'.$nama_file));
        } else if ($type === 'district') {
            $file->storeAs('public/file_district',$nama_file);
            $data = Excel::import(new DistrictImport, storage_path('app/public/file_district/'.$nama_file));
        } else if ($type === 'village') {
            $file->storeAs('public/file_village',$nama_file);
            $data = Excel::import(new VillageImport, storage_path('app/public/file_village/'.$nama_file));
        } else if ($type === 'zipcode') {
            $file->storeAs('public/file_zipcode',$nama_file);
            $data = Excel::import(new ZipcodeImport, storage_path('app/public/file_zipcode/'.$nama_file));
        } else {
            return $this->successResponse(
                $data,
                'type (master) not required',
                400
            );
        }
        return $this->successResponse(
            $data,
            'success',
            204
        );

    }

}
