<?php

namespace App\Traits;

use App\Http\Requests\PermissionRequest;
use Spatie\Permission\Models\Permission;

trait PermissionWebService
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        // validasi
        $request->validated();

        // store
        $permission = Permission::create(['name' => $request->name]);

        // redirect
        return $this->redirectTo($permission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, $id)
    {
        // validasi
        $request->validated();

        // get permission
        $permission = Permission::where(['id' => $id])
            ->firstOrFail();

        // redirect
        return $this->redirectTo(
            $permission->update([
                'name' => $request->name
            ])
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // get permission
        $permission = Permission::where(['id' => $id])
            ->firstOrFail();

        // redirect
        return $this->redirectTo(
            $permission->delete()
        );
    }

    private function redirectTo($condition)
    {
        return $this->getRedirection($condition, [
            'route' => route('index.permission')
        ]);
    }
}
