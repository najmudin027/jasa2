<?php

namespace App\Traits;

use App\Http\Requests\RoleRequest;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

trait RoleWebService
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        // get all permission
        $permissions = Permission::whereIn('id', $request->permissions)->get();

        DB::beginTransaction();
        try {
            // create role
            $role = Role::create([
                'name' => $request->name,
            ]);

            // sync Permissions
            $role->syncPermissions($permissions);

            // commit db
            DB::commit();
        } catch (QueryException $ex) {
            // rollback db
            DB::rollback();

            $role = false;
        }

        return $this->redirectTo($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        // get data
        $role = Role::findOrFail($id);

        // get all permission
        $permissions = Permission::whereIn('id', $request->permissions)->get();

        DB::beginTransaction();
        try {
            // create role
            $role->update([
                'name' => $request->name,
            ]);

            // sync Permissions
            $role->syncPermissions($permissions);

            // commit db
            DB::commit();
        } catch (QueryException $ex) {
            // rollback db
            DB::rollback();

            $role = false;
        }

        return $this->redirectTo($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::where(['id' => $id])->firstOrFail();

        return $this->redirectTo($role->delete());
    }

    private function redirectTo($condition)
    {
        return $this->getRedirection($condition, [
            'route' => route('index.role')
        ]);
    }
}
