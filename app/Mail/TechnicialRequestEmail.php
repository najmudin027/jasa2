<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use App\Model\Master\GeneralSetting;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Model\Master\ServiceDetail;

class TechnicialRequestEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $serviceDetail;
    public $labels;
    public $get_email;
    public $get_number;
    public $get_address;
    public $facebook;
    public $youtube;
    public $instagram;
    public $twiter;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ServiceDetail $serviceDetail)
    {
        $this->serviceDetail = $serviceDetail;
        $this->labels = [
            'TIME' => $serviceDetail->schedule,
            'KELUHAN' => $serviceDetail->symptom->name,
            'LAYANAN' => $serviceDetail->services_type->name,
            'UNIT' => $serviceDetail->unit,
            'Informasi Tambahan' => $serviceDetail->order->note,
        ];
        $this->get_email =  GeneralSetting::where('name', 'email')->first();
        $this->get_number =  GeneralSetting::where('name', 'contact_us')->first();
        $this->get_address =  GeneralSetting::where('name', 'addreses')->first();
        $this->youtube = GeneralSetting::where('name', 'youtube')->first();
        $this->instagram = GeneralSetting::where('name', 'instagram')->first();
        $this->twiter = GeneralSetting::where('name', 'twiter')->first();
        $this->facebook = GeneralSetting::where('name', 'facebook')->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.technicia_request');
    }
}
