<?php

namespace App\Mail;

use Carbon\Carbon;
use App\Model\Master\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Model\Master\ItemDetail;
use App\Model\Master\ServiceDetail;
use App\Model\Master\SparepartDetail;
use App\Model\Master\GeneralSetting;
use App\Model\Master\WebSetting;
use App\Model\Master\EmailStatusOrder;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $modul;
    public $email_content;
    public $replace_string;
    public $sparepart_details;
    public $item_details;
    public $sender_subject;
    public $is_order;
    public $name_module_email = '';
    public $type;
    public $get_email;
    public $get_number;
    public $get_address;
    public $facebook;
    public $youtube;
    public $instagram;
    public $twiter;
    public $get_img_astech;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($modul, $email_content)
    {
        $this->is_order = false;
        $this->sender_subject = $email_content->subject;
        $this->modul = $modul;
        $this->type = $email_content->type;

        if ($modul instanceof \Illuminate\Database\Eloquent\Model) {
            $this->is_order = true;
            $this->sender_subject = 'Order Code #'.$this->modul->code.' - '.$email_content->subject;
        }

        $vars = array(
            // ORDER
            "[{NAME_CUSTOMER}]" => ($this->is_order == true ? $modul->user->name : (isset($modul['customer_name']) ? $modul['customer_name']: null)),
            "[{NAME_TECHNICIAN}]" => ($this->is_order == true ? $modul->service_detail[0]->technician->user->name: (isset($modul['technician_name']) ? $modul['technician_name']: null)),
            "[{EMAIL_CUSTOMER}]" => ($this->is_order == true ? $modul->user->email : (isset($modul['customer_email']) ? $modul['customer_email']: null)),
            "[{EMAIL_TECHNICIAN}]" =>  ($this->is_order == true ? $modul->service_detail[0]->technician->user->email : (isset($modul['technician_email']) ? $modul['technician_email']: null)),
            "[{STATUS_ORDER}]" => ($this->is_order == true ? $modul->order_status->name : null),
            "[{SCHEDULE}]" => ($this->is_order == true ? Carbon::parse($modul->schedule)->format('d F Y H:i:s') : null),
            "[{ORDER_CODE}]" => ($this->is_order == true ? $modul->code : (isset($modul['order_code']) ? $modul['order_code']: null)),

            // BUKAN ODER
            // (TICKET)
            "[{TICKET_NO}]" => isset($modul['ticket_no']) ? $modul['ticket_no'] : null,
            "[{SUBJECT_TICKET}]" => isset($modul['subject_ticket']) ? $modul['subject_ticket'] : null,
            "[{DESCRIPTION_TICKET}]" => isset($modul['description_ticket']) ? $modul['description_ticket'] : null,
            "[{STATUS_TICKET}]" => isset($modul['status_ticket']) ? $modul['status_ticket'] : null,
            // (REVIEW)
            "[{REVIEW_CUSTOMER}]" => isset($modul['description']) ? $modul['description'] : null,
            "[{RATING_REVIEW}]" => isset($modul['rating']) ? $modul['rating'] : null,

        );
        if($this->is_order == true) {

            $this->sparepart_details = SparepartDetail::where('orders_id', $modul->id)
                                    ->with(['order','technician_sparepart'])->get();
            $this->item_details = ItemDetail::where('orders_id', $modul->id)->with(['order'])->get();
            $this->get_email   = GeneralSetting::where('name', 'email')->first();
            $this->get_number  = GeneralSetting::where('name', 'contact_us')->first();
            $this->get_address = GeneralSetting::where('name', 'addreses')->first();
            $this->youtube = GeneralSetting::where('name', 'youtube')->first();
            $this->instagram = GeneralSetting::where('name', 'instagram')->first();
            $this->twiter = GeneralSetting::where('name', 'twiter')->first();
            $this->facebook = GeneralSetting::where('name', 'facebook')->first();
            $this->get_img_astech =  WebSetting::first();
            
        }
        $this->youtube = GeneralSetting::where('name', 'youtube')->first();
        $this->instagram = GeneralSetting::where('name', 'instagram')->first();
        $this->twiter = GeneralSetting::where('name', 'twiter')->first();
        $this->facebook = GeneralSetting::where('name', 'facebook')->first();
        $this->get_img_astech =  WebSetting::first();
        $this->replace_string = strtr($email_content->content,$vars);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $this->subject($this->sender_subject)->view('email.order_notification');
    }
}
