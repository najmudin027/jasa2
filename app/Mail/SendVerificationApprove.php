<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Model\Master\WebSetting;
use App\Model\Master\EmailOther;
use App\Model\Master\GeneralSetting;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendVerificationApprove extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $get_email;
    public $get_number;
    public $get_address;
    public $facebook;
    public $youtube;
    public $instagram;
    public $twiter;
    public $email_content;
    public $get_img_astech;
    public $replace_string;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->get_email =  GeneralSetting::where('name', 'email')->first();
        $this->get_number =  GeneralSetting::where('name', 'contact_us')->first();
        $this->get_address =  GeneralSetting::where('name', 'addreses')->first();
        $this->youtube = GeneralSetting::where('name', 'youtube')->first();
        $this->instagram = GeneralSetting::where('name', 'instagram')->first();
        $this->twiter = GeneralSetting::where('name', 'twiter')->first();
        $this->facebook = GeneralSetting::where('name', 'facebook')->first();
        $this->email_content = EmailOther::where('name_module', 'approved_registration')->first();
        $this->get_img_astech =  WebSetting::first();
        $vars = array(
            "[{NAME_CUSTOMER}]" => $this->user->name,
        );
        $this->replace_string = strtr($this->email_content->content,$vars);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.approvalNotif');
    }
}
