<?php
 
namespace App\Mail;
 
use Auth;
use App\User;
use App\Model\Master\EmailOther;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Model\Master\GeneralSetting;
use Illuminate\Queue\SerializesModels;
use App\Model\Master\WebSetting;
use Illuminate\Contracts\Queue\ShouldQueue;

class B2bSuccessRegisterTeknisi extends Mailable
{
    use Queueable, SerializesModels;

    public $facebook;
    public $youtube;
    public $instagram;
    public $twiter;
    public $get_img_astech;
    public $get_email;
    public $get_number;
    public $get_address;
 
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $id, $password;
    public function __construct($id,$password)
    {
        $this->id =  User::where('id',$id)->first();
        $this->password = $password;
        $this->youtube = GeneralSetting::where('name', 'youtube')->first();
        $this->instagram = GeneralSetting::where('name', 'instagram')->first();
        $this->twiter = GeneralSetting::where('name', 'twiter')->first();
        $this->facebook = GeneralSetting::where('name', 'facebook')->first();
        $this->get_img_astech =  WebSetting::first();
        $this->get_email   = GeneralSetting::where('name', 'email')->first();
        $this->get_number  = GeneralSetting::where('name', 'contact_us')->first();
        $this->get_address = GeneralSetting::where('name', 'addreses')->first();
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $subject = EmailOther::where('id', 13)->first();
        return $this/*->from('MlmSystem@mail.com')*/
                    ->subject($subject->subject)
                    ->view('email.SuccessRegisterTeknisi_notification')
                    ->with(
                        [
                            'content_email' => EmailOther::where('id', 13)->first(),
                            'user'=>$this->id,
                            'password'=>$this->password,
                        ]);
    }
}