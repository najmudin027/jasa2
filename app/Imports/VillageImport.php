<?php

namespace App\Imports;

use App\Model\Master\MsVillage;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class VillageImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MsVillage([
            'name'           => $row['name'],
            'ms_district_id' => $row['ms_district_id'],
        ]);
    }
}
