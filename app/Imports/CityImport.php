<?php

namespace App\Imports;

use App\Model\Master\MsCity;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CityImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MsCity([
            'name'           => $row['name'],
            'ms_province_id' => $row['ms_province_id'],
        ]);
    }
}
