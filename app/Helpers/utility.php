<?php

namespace App\Helpers;

function thousanSparator($num)
{
    return number_format($num, 0, '.', '.');
}

function onlyNumber($string)
{
    return preg_replace('/[^0-9]/', '', $string);
}

function formatCurrency($string)
{
    return 'Rp ' . number_format($string, 0, '.', ',');
}
