<?php

namespace App\Exports;

use App\Model\Master\Order;
// use Maatwebsite\Excel\Concerns\FromQuery;
// use Maatwebsite\Excel\Concerns\FromCollection;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;

class DataOrder implements FromView
{
    use Exportable;
    // public $payment_type;

    // public function __construct(int $payment_type)
    // {
    //     $this->payment_type = $payment_type;
    // }

    // public function query()
    // {
    //     return DB::table('orders')->Where('orders_statuses_id', $this->payment_type)->get();
    // }

    
    public function view(): View
    {
        $order = order::with(
                'user',
                'order_status',
                'service_detail.symptom',
                'service_detail.services_type'
            )->get();
        
        return view('export.order', [
            'order' => $order
        ]);
    }

    

    // public function collection()
    // {
    //     return Order::query()->where('orders_statuses_id', $this->type_id);
    // }
}