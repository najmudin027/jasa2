<?php

namespace App;

use Carbon\Carbon;
use App\Model\Company;
use App\Model\Master\Badge;
use App\Model\Master\Order;
use App\Model\Master\Ewallet;
use App\Model\Master\MsAddress;
use App\Model\Master\MsVillage;
use App\Model\Master\MsZipcode;
use App\Model\Master\MsDistrict;
use App\Model\Master\AddressType;
use App\Model\Master\TeknisiInfo;
use Laravel\Passport\HasApiTokens;
use App\Model\Technician\Technician;
use App\Model\Master\MsAdditionalInfo;
use App\Model\Master\OrdersDataExcelLog;
use App\Notifications\Order\OrderNotification;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Session;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, HasRoles;
    // use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'email',
        'phone',
        'phone1',
        'phone2',
        'phone3',
        'password',
        'otp_code',
        'otp_url',
        'otp_verified_at',
        'api_token',
        'auth_token',
        'status',
        'is_login_as_teknisi',
        'token',
        'note_reject',
        'provider',
        'provider_id',
        'email_verified_at',
        'is_social_login',
        'device_token',
        'ms_company_id',
        'attachment',
        'address',
        'is_b2b_users',
        'is_b2b_teknisi',
        'username',
        'image_teknisi',
    ];

    protected $appends = [
        'full_name',
        'avatar',
        // 'total_orders',
        // 'total_completed_orders',
        // 'total_pending_orders',
        // 'total_cancel_orders',
        // 'money_is_spent',
        // 'last_order'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // mutator

    public static function getImagePathUpload()
    {
        return 'public/user/profile/avatar';
    }

    public function getTotalOrdersAttribute()
    {
        $val = Order::where('users_id', $this->id);
        if (Session::get('date_range')) {
            $val = $val->dateRange(Session::get('date_range'));
        }
        $val = $val->count();
        if ($val) {
            return $val;
        }
        return null;
    }

    public function getTotalCompletedOrdersAttribute()
    {
        $val = Order::where('users_id', $this->id)->where('orders_statuses_id', 10)->where('transfer_status', 1);
        if (Session::get('date_range')) {
            $val = $val->dateRange(Session::get('date_range'));
        }
        $val = $val->count();
        if ($val) {
            return $val;
        }
        return null;
    }

    public function getTotalPendingOrdersAttribute()
    {
        $val = Order::where('users_id', $this->id)->where('transfer_status', 0);
        if (Session::get('date_range')) {
            $val = $val->dateRange(Session::get('date_range'));
        }
        $val = $val->count();
        if ($val) {
            return $val;
        }
        return null;
    }

    public function getTotalCancelOrdersAttribute()
    {
        $val = Order::where('users_id', $this->id)->where('orders_statuses_id', 5);
        if (Session::get('date_range')) {
            $val = $val->dateRange(Session::get('date_range'));
        }
        $val = $val->count();
        if ($val) {
            return $val;
        }
        return null;
    }

    public function getMoneyIsSpentAttribute()
    {
        $val = Order::where('users_id', $this->id);
        if (Session::get('date_range')) {
            $val = $val->dateRange(Session::get('date_range'));
        }
        if ($val->count() > 0) {
            return $val = $val->sum('grand_total');
        }
        return null;
    }

    public function getLastOrderAttribute()
    {
        $query = Order::where('users_id', $this->id);
        if (Session::get('date_range')) {
            $query = $query->dateRange(Session::get('date_range'));
        }
        if ($query->count() > 0) {
            return $query = $query->orderBy('created_at', 'DESC')->limit(1)->first(['id', 'code', 'created_at']);
        }
        return null;
    }

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = empty($value) ? null : preg_replace( '/[^0-9]/', '', $value);
    }

    public function getFullNameAttribute()
    {
        $info = $this->info;
        $name = $this->name;

        if ($info != null) {
            if (!empty($info->first_name)) {
                $name = $info->first_name . ' ' . $info->last_name;
            }
        }
        return ucfirst($name);
    }

    public function getAvatarAttribute()
    {
        if ($this->info == null) {
            return asset('/storage/user/profile/avatar/default_avatar.jpg');
        }

        if ($this->info->picture == null || $this->info->picture == '') {
            return asset('/storage/user/profile/avatar/default_avatar.jpg');
        }

        if ($this->is_social_login == 1) {
            return asset($this->info->picture);
        } else {
            return asset('/storage/user/profile/avatar/' . $this->info->picture);
        }
    }

    public function info()
    {
        return $this->hasOne(MsAdditionalInfo::class);
    }

    public function get_comp()
    {
        return $this->belongsTo(Company::class, 'ms_company_id', 'id');
    }

    public function teknisidetail()
    {
        return $this->hasOne(TeknisiInfo::class);
    }

    public function address()
    {
        return $this->hasOne(MsAddress::class, 'user_id', 'id')->where('is_main', 1);
    }

    public function addresses()
    {
        return $this->hasMany(MsAddress::class, 'user_id', 'id');
    }

    public function technician()
    {
        return $this->hasOne(Technician::class, 'user_id', 'id');
    }

    public function ewallet()
    {
        return $this->hasOne(Ewallet::class, 'user_id', 'id');
    }

    public function masterwallet()
    {
        return $this->hasOne(Ewallet::class, 'user_id', 'id');
    }

    public static function getAdminRole()
    {
        return User::role('admin')->get(); // Returns only users with the role 'admin'
    }

    public function badge()
    {
        return $this->belongsTo(Badge::class, 'badge_id');
    }

    public function last_order()
    {
        return $this->hasOne(Order::class, 'users_id')->orderBy('created_at', 'DESC');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'users_id');
    }

}
