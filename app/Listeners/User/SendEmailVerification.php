<?php

namespace App\Listeners\User;

use App\Events\UserRegister;
use App\Notifications\UserRegisterActivicationNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailVerification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegister  $event
     * @return void
     */
    public function handle(UserRegister $event)
    {
        $user = $event->user;

        $user->notify(new UserRegisterActivicationNotification($user));
    }
}
