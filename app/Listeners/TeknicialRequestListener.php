<?php

namespace App\Listeners;

use App\Events\CustomerRequestPenawaranEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\TecnicialRequestNotification;

class TeknicialRequestListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerRequestPenawaranEvent  $event
     * @return void
     */
    public function handle(CustomerRequestPenawaranEvent $event)
    {
        $order = $event->order;
        foreach ($order->service_detail as $detail) {
            $detail->technician->user->notify(new TecnicialRequestNotification($detail));
        }        
    }
}
