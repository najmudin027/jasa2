<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Master\PartDataExcelStock;
class B2BDataCostEstimations extends Model
{
    protected $table = 'business_to_business_cost_estimation';

    protected $guarded = [];

    public function get_stok()
    {
        return $this->belongsTo(PartDataExcelStock::class, 'part_data_stock_id', 'id');
    }
}
