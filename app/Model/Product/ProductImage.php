<?php

namespace App\Model\Product;

use App\Model\Master\ImageUpload;
use App\Model\Master\MsProduct;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = [
        'galery_image_id',
        'product_id'
    ];

    public function product()
    {
        return $this->belongsTo(MsProduct::class, 'product_id', 'id');
    }

    public function gallery()
    {
        return $this->belongsTo(ImageUpload::class, 'galery_image_id', 'id');
    }
}
