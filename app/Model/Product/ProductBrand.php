<?php

namespace App\Model\Product;

use App\Model\Master\MsProductModel;
use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{
    protected $fillable = [
        'name',
        'product_part_category_id',
        'code',
    ];

    public function part_category()
    {
        return $this->belongsTo(ProductPartCategory::class, 'product_part_category_id', 'id');
    }

    public function product_models()
    {
        return $this->hasMany(MsProductModel::class, 'product_brands_id', 'id');
    }
}
