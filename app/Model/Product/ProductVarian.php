<?php

namespace App\Model\Product;

use App\Model\Master\MsProductModel;
use App\Model\Product\ProductVendor;
use Illuminate\Database\Eloquent\Model;
use App\Model\Product\ProductVarianAttribute;

class ProductVarian extends Model
{
    // protected $fillable = [
    //     'stock',
    //     'product_vendor_id'
    // ];
    protected $table = 'product_varians';

    protected $guarded = [];

    protected $appends = ['attribute_name'];

    public function product_vendor()
    {
        return $this->belongsTo(ProductVendor::class, 'product_vendor_id', 'id');
    }

    public function attributes()
    {
        return $this->hasMany(ProductVarianAttribute::class, 'product_varian_id', 'id');
    }

    public function product_varian_attribute()
    {
        return $this->hasMany(ProductVarianAttribute::class, 'product_varian_id', 'id');
    }

    public function productVarianAttributes()
    {
        return $this->hasMany(ProductVarianAttribute::class, 'product_varian_id', 'id');
    }

    public function getAttributeNameAttribute()
    {
        $name = '';
        $product_varian_attributes = $this->product_varian_attribute;
        foreach ($product_varian_attributes as $product_varian_attribute) {
            $name .= $product_varian_attribute->attribute->name . ': ';
            $name .= $product_varian_attribute->term->name . ', ';
        }

        return trim($name, ', ');
    }
}
