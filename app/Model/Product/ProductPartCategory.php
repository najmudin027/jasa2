<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductPartCategory extends Model
{
    protected $fillable = [
        'part_category',
        'part_code',
    ];

    public function product_brands()
    {
        return $this->hasMany(ProductBrand::class, 'product_part_category_id', 'id');
    }
}
