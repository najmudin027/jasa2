<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GeneralSetting extends Model
{
    protected $table = 'general_settings';
    protected $fillable = [
        'name', 'value', 'desc', 'type'
    ];

}
