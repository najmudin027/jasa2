<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BusinessToBusinessTransaction extends Model
{
    protected $table = 'business_to_business_transactions';

    protected $guarded = [];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function business_to_business_outlet_transactions()
    {
        return $this->hasMany(BusinessToBusinessOutletTransaction::class, 'business_to_business_transaction_id', 'id');
    }

    public function user_create()
    {
        return $this->belongsTo(User::class, 'user_created', 'id');
    }

    public function user_update()
    {
        return $this->belongsTo(User::class, 'user_updated', 'id');
    }
}
