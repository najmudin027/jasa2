<?php

namespace App\Model;
use App\Model\Master\ASCExcel;
use App\Model\B2BLabor;

use Illuminate\Database\Eloquent\Model;

class B2BDataTransactions extends Model
{
    protected $table = 'business_to_business_data_quotations';

    protected $guarded = [];

    protected $appends = [
        'quotation_url',
        'invoice_url',
    ];

    public function getQuotationUrlAttribute()
    {
        return asset('/admin/business-to-business-user-request-order/preview-quotation/'.$this->id);
    }

    public function getInvoiceUrlAttribute()
    {
        return asset('/admin/business-to-business-user-request-order/preview-invoice/'.$this->id_transaction);
    }

    public function get_part()
    {
        return $this->hasMany(B2BDataCostEstimations::class, 'b2b_data_quotations', 'id');
    }

    public function get_type()
    {
        return $this->belongsTo(B2BMasterType::class, 'type_produk', 'id');
    }

    public function get_asc()
    {
        return $this->belongsTo(ASCExcel::class, 'asc_id', 'id');
    }

    public function get_labor()
    {
        return $this->belongsTo(B2BLabor::class, 'id', 'id_b2b_quot');
    }
    
    public function get_trans()
    {
        return $this->belongsTo(RequestOrderDetail::class, 'id_transaction', 'id');
    }
}
