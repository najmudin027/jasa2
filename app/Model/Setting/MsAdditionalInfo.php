<?php

namespace App\Model\Setting;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class MsAdditionalInfo extends Model
{
    protected $fillable = [
        'ms_religion_id',
        'ms_marital_id',
        'first_name',
        'last_name',
        'date_of_birth',
        'place_of_birth',
        'gender',
        'picture',
        'user_id'
    ];

    protected $appends = [
        'avatar',
    ];

    public static function getImagePathUpload()
    {
        return 'public/ImageProfile';
    }

    // mutator
    public function setDateOfBirthAttribute($value)
    {
        $this->attributes['date_of_birth'] = date("Y-m-d", strtotime($value));
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function religion()
    {
        return $this->belongsTo(Religion::class, 'ms_religion_id');
    }

    public function marital()
    {
        return $this->belongsTo(Marital::class, 'ms_marital_id');
    }

    # append attribute
    # =================
    /**
     * Get Image full path
     *
     * @return string
     */
    public function getAvatarAttribute()
    {
        $path = $this->getImagePathUpload() . '/' . $this->picture;

        return $this->picture == null ? '' : Storage::url($path);
    }
}
