<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RequestOrderImage extends Model
{
    protected $table = "ms_request_order_image";
    protected $fillable = [
        'image',
        'ms_request_order_id',
    ];

    public static function getImagePathUpload()
    {
        return 'public/request_order_image/';
    }
}