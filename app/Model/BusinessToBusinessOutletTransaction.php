<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BusinessToBusinessOutletTransaction extends Model
{
    protected $table = 'business_to_business_outlet_transactions';

    protected $guarded = [];

    

    public function business_to_business_transaction()
    {
        return $this->belongsTo(BusinessToBusinessTransaction::class, 'business_to_business_transaction_id', 'id');
    }

    public function business_to_business_outlet_detail_transactions()
    {
        return $this->hasMany(BusinessToBusinessOutletDetailTransaction::class, 'business_to_business_outlet_transaction_id', 'id');
    }

    public function user_create()
    {
        return $this->belongsTo(User::class, 'user_created', 'id');
    }

    public function user_update()
    {
        return $this->belongsTo(User::class, 'user_updated', 'id');
    }
}
