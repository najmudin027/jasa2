<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Model\B2BAssignTeknisi;

class RequestOrderDetail extends Model
{
    protected $table = "ms_request_order_detail";
    protected $fillable = [
        'request_code',
        'ms_outlet_id',
        'ms_transaction_b2b_detail',
        'service_type',
        'user_id',
        'read_at',
        'status_quotation',
        'ms_b2b_detail',
        'vidio',
        'remark',
        'image',
        'teknisi_id'
    ];

    public function get_b2b_order_detail()
    {
        return $this->belongsTo(BusinessToBusinessOutletDetailTransaction::class, 'ms_transaction_b2b_detail', 'id');
    }

    public function get_schedule()
    {
        return $this->belongsTo(B2BAssignTeknisi::class, 'id', 'order_id');
    }

    public function get_b2b_detail()
    {
        return $this->belongsTo(BusinessToBusinessOutletDetailTransaction::class, 'ms_b2b_detail', 'id');
    }

    public function get_outlet()
    {
        return $this->belongsTo(BusinessToBusinessOutletTransaction::class, 'ms_outlet_id', 'id');
    }

    public function get_user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}