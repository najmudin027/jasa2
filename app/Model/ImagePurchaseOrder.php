<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ImagePurchaseOrder extends Model
{
    protected $table = 'image_purchase_order';
    protected $fillable = [
        'type', 'image_po', 'ms_b2b_detail'
    ];
}
