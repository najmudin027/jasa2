<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BusinessToBusinessOutletDetailTransaction extends Model
{
    protected $table = 'business_to_business_outlet_detail_transactions';

    protected $guarded = [];
    protected $appends = ['total_parent'];

    public function getTotalParentAttribute()
    {
        $countStatus = BusinessToBusinessOutletDetailTransaction::whereNotNull('is_parent_b2b_outlet_id')->where('is_parent_b2b_outlet_id', $this->id)->count();

        return $countStatus;
    }

    public function get_type()
    {
        return $this->belongsTo(B2BMasterType::class, 'type', 'id');
    }
    
    public function b2b_detail_trans()
    {
        return $this->belongsTo(RequestOrders::class, 'id', 'ms_transaction_b2b_detail');
    }

    public function business_to_business_outlet_transaction()
    {
        return $this->belongsTo(BusinessToBusinessOutletTransaction::class, 'business_to_business_outlet_transaction_id', 'id');
    }

    public function user_create()
    {
        return $this->belongsTo(User::class, 'user_created', 'id');
    }

    public function user_update()
    {
        return $this->belongsTo(User::class, 'user_updated', 'id');
    }

    public function setTanggalPembayaranAttribute($value)
    {
        $this->attributes['tanggal_pembayaran'] = empty($value) ? null : date('Y-m-d', strtotime($value));
    }

    public function setTanggalQuotAttribute($value)
    {
        $this->attributes['tanggal_quot'] = empty($value) ? null : date('Y-m-d', strtotime($value));
    }

    public function setTanggalPoAttribute($value)
    {
        $this->attributes['tanggal_po'] = empty($value) ? null : date('Y-m-d', strtotime($value));
    }

    public function setTanggalInvoiceAttribute($value)
    {
        $this->attributes['tanggal_Invoice'] = empty($value) ? null : date('Y-m-d', strtotime($value));
    }
}
