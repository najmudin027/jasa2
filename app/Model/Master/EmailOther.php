<?php

namespace App\Model\Master;


use Illuminate\Database\Eloquent\Model;

class EmailOther extends Model
{
    protected $guarded = [];
    protected $table = 'email_other';

}
