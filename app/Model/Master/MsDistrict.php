<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class MsDistrict extends Model
{
    protected $table = 'ms_districts';

    protected $guarded = [];

    // protected $casts = [
    //     'meta' => 'array',
    // ];

    public function city()
    {
        return $this->belongsTo(MsCity::class, 'ms_city_id');
    }

    public function villages()
    {
        return $this->hasMany(MsVillage::class);
    }

    public function zipcode()
    {
        return $this->hasOne(MsZipcode::class);
    }

    public function warehouse()
    {
        return $this->hasMany(MsWarehouse::class);
    }
}
