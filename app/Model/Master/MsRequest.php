<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class MsRequest extends Model
{
    protected $table = 'ms_requests';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function support()
    {
        return $this->belongsTo(MsSupport::class, 'ms_supports_id');
    }

    public function ticket()
    {
        return $this->belongsTo(MsTicket::class, 'ms_tickets_id');
    }
}
