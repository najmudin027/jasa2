<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Marital extends Model
{
    protected $fillable = [
        'status',
    ];
}
