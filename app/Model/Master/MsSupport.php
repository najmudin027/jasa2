<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class MsSupport extends Model
{
    protected $table = 'ms_supports';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
