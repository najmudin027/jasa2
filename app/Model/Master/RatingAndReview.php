<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class RatingAndReview extends Model
{
    protected $guarded = [];
    protected $table = 'rating_and_reviews';

    public function rating()
    {
        return $this->belongsTo(Rating::class, 'rating_id');
    }

    public function review()
    {
        return $this->belongsTo(Review::class, 'review_id');
    }
}
