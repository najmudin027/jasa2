<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class RepairCode extends Model
{
    protected $guarded = [];
    protected $table = 'ms_repair_codes';
}
