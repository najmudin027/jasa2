<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ASCExcel extends Model
{
    protected $table = 'ode_astech_service_center';

    protected $guarded = [];

    public function asc()
    {
        return $this->hasMany(GeneralJournalDetail::class, 'asc_id', 'id');
    }

}
