<?php

namespace App\Model\Master;

use App\Model\Master\HistoryOrder;
use Illuminate\Database\Eloquent\Model;

class OrdersStatus extends Model
{
    protected $table = 'orders_statuses';
    protected $guarded = [];

    public function order()
    {
        return $this->hasMany(Order::class, 'orders_statuses_id');
    }

    public function history_order()
    {
        return $this->hasMany(HistoryOrder::class, 'orders_statuses_id');
    }

    public function email_status_order()
    {
        return $this->hasMany(EmailStatusOrder::class, 'orders_statuses_id');
    }

    public function getStatus($name){
    	return $this->where('name', $name)->firstOrFail();
    }
}
