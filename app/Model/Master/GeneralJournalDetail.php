<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class GeneralJournalDetail extends Model
{
    protected $table = 'general_journal_details';

    protected $guarded = [];

    protected $dates = ['date'];

    protected $appends = [
        'stock_edit_available',
        'margin_rate_auto',
    ];

    public function getStockEditAvailableAttribute()
    {
        $sum_stock = GeneralJournalDetail::where('part_data_stock_id', $this->part_data_stock_id)->sum('quantity');
        $count_id = GeneralJournalDetail::where('part_data_stock_id', $this->part_data_stock_id)->groupBy('part_data_stock_id')->count();
        $sum_stock_out = PartsDataExcel::where('part_data_stock_id', $this->part_data_stock_id)->sum('quantity');
        $stock_edit_ava = $sum_stock - $sum_stock_out;
        return $this->quantity - $stock_edit_ava;
        // return ($this->category == 'material' ? round(($sum_stock / $count_id),0) : $this->quantity);
    }

    public static function getImagePathUpload()
    {
        return 'public/general-journal-detail';
    }

    public function general_journal()
    {
        return $this->belongsTo(GeneralJournal::class, 'general_journal_id');
    }
    public function asc()
    {
        return $this->belongsTo(ASCExcel::class, 'asc_id');
    }
    public function part_data_stock_inventory()
    {
        return $this->belongsTo(PartDataExcelStockInventory::class, 'part_data_stock_inventories_id');
    }

    public function pds_inventory_mutation_histories()
    {
        return $this->hasMany(PDSMutationJoinHistories::class, 'general_journal_id', 'id');
    }

    public function getMarginRateAutoAttribute() {
        return intval(GeneralSetting::where('name', 'margin_rate_auto')->value('value'));
    }

    public function hpp_item()
    {
        return $this->belongsTo(HppItem::class, 'part_data_stock_id','pdes_id');
    }







}
