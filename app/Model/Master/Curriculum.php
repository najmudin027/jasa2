<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    protected $table = 'curriculum';
    protected $fillable = [
        'curriculum_name'
    ];

    public function teknisiinfo() {
        return $this->hasMany(TeknisiInfo::class, 'ms_curriculum_id', 'id');
    }

    public function technician_curriculum() {
        return $this->hasMany(TechnicianCurriculum::class, 'curriculum_id', 'id');
    }
}
