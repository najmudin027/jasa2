<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class HistoryOrder extends Model
{
    protected $table = 'history_orders';

    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(Order::class, 'orders_id');
    }

    public function order_status()
    {
        return $this->belongsTo(OrdersStatus::class, 'orders_statuses_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return date('Y-m-d H:i:s', strtotime($value));
    }
}
