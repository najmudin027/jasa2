<?php

namespace App\Model\Master;
use App\User;
use Illuminate\Database\Eloquent\Model;

class TopupWallet extends Model
{
    protected $table = "topup_wallet";
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    
}


