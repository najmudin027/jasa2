<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class MsCountry extends Model
{
    protected $table = 'ms_countries';

    protected $fillable = [
        'name', 'code'
    ];

     // protected $casts = [
    //     'meta' => 'array',
    // ];

    public function provinces()
    {
        return $this->hasMany(MsProvince::class);
    }
}
