<?php

namespace App\Model\Master;

use App\User;
use stdClass;
use Carbon\Carbon;
use App\Model\Master\Services;
use App\Model\Master\MsSymptom;
use App\Model\Master\HistoryOrder;
use App\Model\Master\ServiceDetail;
use App\Model\Master\EwalletHistory;
use App\Model\Master\EwalletPayment;
use App\Model\Master\GeneralSetting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Master\InventoryMutationHistories;

class Order extends Model
{
    use SoftDeletes;
    protected $table = 'orders';

    protected $guarded = [];


    protected $casts = [
        'date_complete' => 'datetime:Y-m-d H:i',
        'schedule' => 'datetime:Y-m-d H:i',
        'reschedule' => 'datetime:Y-m-d',
    ];

    protected $appends = [
        'url_detail',
        'name_customer',
        'phone_number',
        'pay_type',
        'customer',
        'countdown_autocancel',
        'countdown_autocompleted',
        'tgl_ubah_status_order'
    ];

    public function getTglUbahStatusOrderAttribute() {
        $query = HistoryOrder::where('orders_id', $this->id)->get();
        $data = Array();
        for($i = 2; $i <= 11; $i++) {
            $data['order_status_'.$i.''] = null;
        }
        foreach($query as $key => $att) {
            for($i = 2; $i <= 11; $i++) {
                if($att->orders_statuses_id == $i) {
                    // order_status_{number} cek di database tabel Order_statuses_id angka adalah id
                    $data['order_status_'.$i.''] = Carbon::parse($att->created_at)->format('Y-m-d H:i:s');
                }
            }
        }
        return $data;
    }

    public function getCountdownAutocancelAttribute()
    {
        return $this->getCountdown(9, [2,3]);
    }

    public function getCountdownAutocompletedAttribute()
    {
        return $this->getCountdown(9, [7]);
    }

    public function getCountdown($setting_id, array $order_statuses_id) {
        $settings = GeneralSetting::all();
        if($settings[$setting_id]->value !== "0" && $settings[$setting_id]->value !== "") {
            $created_at = HistoryOrder::where('orders_id', $this->id);
            // get countdown auto cancel
            if(count($order_statuses_id) === 1) {
                $created_at = $created_at->where('orders_statuses_id', $order_statuses_id);
            // get countdown auto completed job
            } else {
                $created_at = $created_at->where(function ($query) use ($order_statuses_id) {
                    $query->where('orders_statuses_id', $order_statuses_id[0])
                    ->orWhere('orders_statuses_id', $order_statuses_id[1]);
                });
            }
            $created_at = $created_at->value('created_at');
            if($created_at != null) {
                $val = Carbon::parse($created_at);
                return $val = $val->addHours(intval($settings[$setting_id]->value))->format('Y-m-d H:i');
            }
        }
        return null;
    }

    public function getCustomerAttribute()
    {
        return json_decode($this->customer_json);
    }

    public function product_group()
    {
        return $this->belongsTo(ProductGroup::class, 'product_group_id');
    }

    public function getPayTypeAttribute()
    {
        return $this->payment_type == 1 ? 'online' : 'offline';
    }

    public function saldo()
    {
        return $this->hasOne(TechnicianSaldo::class, 'order_id');
    }


    public function getNameCustomerAttribute()
    {
        return $this->user == null ? '' : $this->user->name;
    }

    public function getPhoneNumberAttribute()
    {
        return $this->user == null ? '' : $this->user->phone;
    }



    // mutator
    public function setScheduleAttribute($value)
    {
        $this->attributes['schedule'] = date("Y-m-d H:i:s", strtotime($value));
    }

    public function user()
    {
        // return $this->belongsTo(User::class, 'users_id')->withTrashed();
        return $this->belongsTo(User::class, 'users_id');
    }

    public function request()
    {
        return $this->belongsTo(MsRequest::class, 'ms_requests_id');
    }

    public function service()
    {
        // return $this->belongsTo(Services::class, 'ms_service_id')->withTrashed();
        return $this->belongsTo(Services::class, 'ms_service_id');
    }

    public function symptom()
    {
        // return $this->belongsTo(MsSymptom::class, 'ms_symptom_id')->withTrashed();
        return $this->belongsTo(MsSymptom::class, 'ms_symptom_id');
    }

    public function payment_method()
    {
        return $this->belongsTo(MsPaymentMethod::class, 'ms_payment_methods_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(MsWarehouse::class, 'ms_warehouses_id');
    }

    public function order_status()
    {
        return $this->belongsTo(OrdersStatus::class, 'orders_statuses_id');
    }

    public function ticket()
    {
        return $this->hasOne(MsTicket::class, 'order_id');
    }

    public function service_detail()
    {
        return $this->hasMany(ServiceDetail::class, 'orders_id');
    }

    public function detail()
    {
        return $this->hasOne(ServiceDetail::class, 'orders_id');
    }

    public function sparepart_detail()
    {
        return $this->hasMany(SparepartDetail::class, 'orders_id');
    }

    public function tmp_sparepart()
    {
        return $this->hasMany(TmpSparepartDetail::class, 'orders_id');
    }

    public function item_detail()
    {
        return $this->hasMany(ItemDetail::class, 'orders_id');
    }

    public function tmp_item_detail()
    {
        return $this->hasMany(TmpItemDetail::class, 'orders_id');
    }

    public function history_order()
    {
        return $this->hasMany(HistoryOrder::class, 'orders_id');
    }

    public function order_media_problem()
    {
        return $this->hasMany(OrderMediaProblem::class, 'orders_id');
    }

    public function bank()
    {
        return $this->belongsTo(BankTransfer::class, 'ms_payment_methods_id');
    }

    public function garansi()
    {
        return $this->hasOne(GaransiChat::class, 'order_id');
    }

    public function chat()
    {
        return $this->hasOne(Chat::class, 'order_id');
    }

    public function complaints()
    {
        return $this->hasMany(MsOrderComplaint::class, 'ms_orders_id');
    }

    public function unread_complaint()
    {
        return $this->hasOne(MsOrderComplaint::class, 'ms_orders_id')->where('user_id', '!=', Auth::id())->whereNull('read_at');
    }

    public function symptom_code()
    {
        return $this->belongsTo(SymptomCode::class, 'ms_symptom_code');
    }

    public function repair_code()
    {
        return $this->belongsTo(RepairCode::class, 'ms_repair_code');
    }

    public function rating_and_review()
    {
        return $this->belongsTo(RatingAndReview::class, 'rating_and_review_id');
    }

    public function orderpayment()
    {
        return $this->hasOne(EwalletPayment::class, 'order_id');
    }

    public function order_log()
    {
        return $this->hasMany(OrderLog::class, 'order_id');
    }

    public function inventory_mutation_histories()
    {
        return $this->hasMany(InventoryMutationHistories::class, 'order_id');
    }

    public function scopeDateRange($query, $data_range)
    {
        if (Session::get('date_range')) {
            $range = explode('-', Session::get('date_range'));
            $date_from = date('Y-m-d', strtotime($range[0]));
            $date_to = date('Y-m-d', strtotime($range[1]));
            $query->where('created_at', '>=', $date_from)
                ->where('created_at', '<=', $date_to);
        }
        return null;
    }

    public function getUrlDetailAttribute()
    {
        if(Auth::check()) {
            if (\Auth::user()->hasRole('admin')) {
                return url('/admin/order/service_detail/' . $this->id);
            }
            return url('/customer/service_detail/' . $this->id);
        }
        return null;

    }

    public function technician_schedule()
    {
        return $this->hasMany(TechnicianSchedule::class, 'technician_id');
    }

    public function markAsRead()
    {
        $user = Auth::user();

        if ($user->hasRole('admin')) {
            $total = 0;
            if ($this->customer_read_at != null) {
                $total++;
            }

            if ($this->teknisi_read_at != null) {
                $total++;
            }

            $this->update([
                'notifikasi_type' => $total > 2 ? 0 : 1,
                'admin_read_at' => now()
            ]);
        }

        if ($user->hasRole('Customer')) {
            $total = 0;

            if ($this->teknisi_read_at != null) {
                $total++;
            }

            if ($this->admin_read_at != null) {
                $total++;
            }

            $this->update([
                'notifikasi_type' => $total > 2 ? 0 : 1,
                'customer_read_at' => now()
            ]);
        }

        if ($user->hasRole('Technician')) {
            $total = 0;
            if ($this->customer_read_at != null) {
                $total++;
            }

            if ($this->admin_read_at != null) {
                $total++;
            }

            $this->update([
                'notifikasi_type' => $total > 2 ? 0 : 1,
                'teknisi_read_at' => now()
            ]);
        }
    }
}
