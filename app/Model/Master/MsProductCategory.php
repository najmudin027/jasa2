<?php

namespace App\Model\Master;

use App\Model\Master\MsModel;
use Illuminate\Database\Eloquent\Model;
use App\Model\Product\ProductBrand;

class MsProductCategory extends Model
{
    protected $table = 'ms_product_category';

    // protected $fillable = [
    //     'product_brand_id', 'product_cate_code', 'name', 'slug', 'parent_id','deleted_at'
    // ];
    protected $guarded = [];

    // protected $casts = [
    //     'meta' => 'array',
    // ];

    // Recursive function that builds the menu from an array or object of items
    // In a perfect world some parts of this function would be in a custom Macro or a View

    public function childs()
    {
        return $this->hasMany(MsProductCategory::class, 'parent_id', 'id');
    }

    public function brand()
    {
        return $this->belongsTo(ProductBrand::class, 'product_brand_id');
    }

    public function models()
    {
        return $this->hasMany(MsModel::class);
    }

    public function products()
    {
        return $this->hasMany(MsProduct::class);
    }

    public function product_category_combine()
    {
        return $this->hasMany(ProductCategoryCombine::class);
    }
}
