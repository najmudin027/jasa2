<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class OrderMediaProblem extends Model
{
    protected $guarded = [];
    protected $table = 'order_media_problem';

    protected $appends = ['media_src'];

    public function getMediaSrcAttribute()
    {
        return asset('/storage/order_problem/' . $this->filename);
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'orders_id', 'id');
    }

}
