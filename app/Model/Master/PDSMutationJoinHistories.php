<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use function App\Helpers\thousanSparator;

class PDSMutationJoinHistories extends Model
{
    protected $table = 'pds_mutation_join_histories';

    protected $guarded = [];

    public function history_out()
    {
        return $this->belongsTo(PDSInventoryMutationHistories::class, 'history_out');
    }

    public function history_in()
    {
        return $this->belongsTo(PDSInventoryMutationHistories::class, 'history_in');
    }

    public function mutation_join() {
        return $this->hasMany(PDSInventoryMutationHistories::class, 'mutation_join_id');
    }





}
