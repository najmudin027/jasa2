<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class TATGroup extends Model
{
    protected $table = 't_a_t_groups';
    protected $fillable = [
        'name',
    ];
}
