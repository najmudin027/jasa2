<?php

namespace App\Model\Master;
use App\User;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Ewallet extends Model
{
    protected $table = "e_wallet";
    protected $guarded = [];

    protected $appends = ['total_topup'];

    public function getTotalTopupAttribute()
    {
        $countStatus = EwalletHistory::where('ms_wallet_id', $this->id)->where('transfer_status_id', 1)->count();

        return $countStatus;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function history()
    {
        return $this->hasMany(EwalletHistory::class, 'ms_wallet_id');
    }
}


