<?php

namespace App\Model\Master;

use DB;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MsTicket extends Model
{
    protected $table = 'ms_tickets';

    protected $guarded = [];

    protected $appends = ['status_html', 'status_name'];

    /**
     * generate nomor tiket
     *
     * @return string | tahun-order_no example: 20-001
     */
    public static function genetateTicketNo()
    {
        // dapatkan tahun
        $year = date("Y");

        // dapatkan data tiket
        $tiket = MsTicket::where(DB::raw('YEAR(created_at)'), $year)->orderBy('id', 'desc')->first();

        // jika tidak ada data
        if ($tiket == null) {
            $id = 1;
        } else {
            // jika ada data
            if ($tiket->ticket_no == null) {
                $id = 1;
            } else {
                $ticket_no = explode('-', $tiket->ticket_no);
                if (isset($ticket_no[1])) {
                    $id = abs($ticket_no[1]) + 1;
                } else {
                    $id = 1;
                }
            }
        }

        $number = date('y') . '-';

        $length = strlen($id);
        switch ($length) {
            case 1:
                $number .= '00' . $id;
                break;

            case 2:
                $number .= '0' . $id;
                break;

            default:
                $number .= $id;
                break;
        }

        return $number;
    }

    public function getStatusHtmlAttribute()
    {
        switch ($this->status) {
            case '1':
                return '<span class="btn btn-sm btn-primary"><i class="fa fa-check"></i> status open</span>';
                break;

            case '2':
                return '<span class="btn btn-sm btn-info"<i class="fa fa-check"></i> status on progress</span>';
                break;

            case '3':
                return '<span class="btn btn-sm btn-dark"<i class="fa fa-check"></i> status on resolving</span>';
                break;

            case '4':
                return '<span class="btn btn-sm btn-success"><i class="fa fa-check"></i> status done</span> ';
                break;

            case '5':
                return '<span class="btn btn-sm btn-danger"><i class="fa fa-check"></i> status close</span> ';
                break;

            default:
                return $this->status;
                break;
        }
    }

    public function getStatusNameAttribute()
    {
        switch ($this->status) {
            case '1':
                return 'open';
                break;

            case '2':
                return 'progress';
                break;

            case '3':
                return 'resolving';
                break;

            case '4':
                return 'done ';
                break;

            case '5':
                return 'close';
                break;

            default:
                return $this->status;
                break;
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function support()
    {
        return $this->belongsTo(MsSupport::class, 'ms_supports_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function ticket_details()
    {
        return $this->hasMany(MsTicketDetail::class, 'ms_ticket_id');
    }

    public function unread_ticket()
    {
        return $this->hasOne(MsTicketDetail::class, 'ms_ticket_id')->where('user_id', '!=', Auth::id())->whereNull('read_at');
    }
}
