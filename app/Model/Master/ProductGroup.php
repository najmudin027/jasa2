<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    protected $table = 'product_groups';
    protected $guarded = [];

    protected $appends = ['link_image'];

    public static function getImagePathUpload()
    {
        return 'public/product-group';
    }

    public function getLinkImageAttribute()
    {
        return ($this->images == null) ? asset('/storage/services/service_not_found.jpeg') : asset('/storage/product-group/' . $this->images);
    }

    public function service_type()
    {
        return $this->belongsTo(MsServicesType::class, 'ms_services_type_id');
    }

    public function order()
    {
        return $this->hasMany(Order::class, 'product_group_id');
    }
}
