<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class JasaDataExcel extends Model
{
    protected $table = 'jasa_data_excel';

    protected $guarded = [];

    public function order_data_excel()
    {
        return $this->belongsTo(OrdersDataExcel::class, 'orders_data_excel_id');
    }

    public function jasa()
    {
        return $this->belongsTo(JasaExcel::class, 'jasa_id');
    }

}
