<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Model\Master\OrdersDataExcelLog;

class OrdersDataExcel extends Model
{
    protected $table = 'orders_data_excel';

    protected $guarded = [];

    protected $dates = ['date', 'engineer_picked_order_date', 'admin_assigned_to_engineer_date', 'engineer_assigned_date', 'tech_1st_appointment_date', '1st_visit_date', 'repair_completed_date', 'closed_date'];

    public function part_data_excel()
    {
        return $this->hasMany(PartsDataExcel::class, 'orders_data_excel_id', 'id');
    }
    public function jasa_data_excel()
    {
        return $this->hasMany(JasaDataExcel::class, 'orders_data_excel_id', 'id');
    }

    public function orders_data_excel_log()
    {
        return $this->hasMany(OrdersDataExcelLog::class, 'orders_data_excel_id', 'id');
    }

    public function pds_inventory_mutation_histories()
    {
        return $this->hasMany(PDSMutationJoinHistories::class, 'orders_data_excel_id', 'id');
    }



}
