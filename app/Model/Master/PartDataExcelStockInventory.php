<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Services\Master\GeneralJournalService;

class PartDataExcelStockInventory extends Model
{
    protected $table = 'ode_part_data_stock_inventories';

    protected $guarded = [];

    protected $appends = [
        'hpp_average',
        'out_stock'
    ];

    public function part_data_stock()
    {
        return $this->belongsTo(PartDataExcelStock::class, 'part_data_stock_id');
    }

    public function pdes_history()
    {
        return $this->hasMany(PDESHistory::class, 'part_data_stock_inventories_id', 'id');
    }

    public function part_data_excel()
    {
        return $this->hasMany(PartsDataExcel::class, 'part_data_stock_inventories_id', 'id');
    }

    public function getOutStockAttribute() {
        $data = $this->part_data_excel()->where('part_data_stock_inventories_id', $this->id);
        if($data->count() > 0) {
            return $data = intval($data->sum('quantity'));
        }
        return 0;

    }

    public function getHppAverageAttribute() {
        $gj_cont = new GeneralJournalService();
        return $gj_cont->getHppAverageAll($this->id);
    }




}
