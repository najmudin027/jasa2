<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Services\Master\GeneralJournalService;

class PartDataExcelStock extends Model
{
    protected $table = 'ode_part_data_stock';

    protected $guarded = [];


    public function get_price()
    {
        return $this->belongsTo(PartDataExcelStockInventory::class, 'id', 'part_data_stock_id');
    }

    public function part_data_excel()
    {
        return $this->hasMany(PartsDataExcel::class, 'part_data_stock_id', 'id');
    }

    public function general_journal_detail()
    {
        return $this->hasMany(GeneralJournalDetail::class, 'part_data_stock_id', 'id');
    }

    public function pds_inventory_mutation_histories()
    {
        return $this->hasMany(PDSMutationJoinHistories::class, 'part_data_stock_id', 'id');
    }

    public function asc()
    {
        return $this->belongsTo(ASCExcel::class, 'asc_id');
    }


}
