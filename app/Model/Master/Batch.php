<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use function App\Helpers\onlyNumber;

class Batch extends Model
{
    protected $guarded = [];

    protected $casts = [
        'batch_date' => 'datetime:Y-m-d',
    ];

    # Defining A Mutator

    public function setShippingCostAttribute($value)
    {
        $this->attributes['shipping_cost'] = onlyNumber($value);
    }

    public function setExtraCostAttribute($value)
    {
        $this->attributes['extra_cost'] = onlyNumber($value);
    }

    public function items()
    {
        return $this->hasMany(BatchItem::class);
    }

    public function batch_shipment_history()
    {
        return $this->hasMany(BatchShipmentHistory::class);
    }

    public function last_history()
    {
        return $this->hasOne(BatchShipmentHistory::class)->orderBy('id', 'DESC');
    }
}
