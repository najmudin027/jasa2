<?php

namespace App\Model\Master;


use Illuminate\Database\Eloquent\Model;

class OrderLog extends Model
{
    protected $guarded = [];
    protected $table = 'order_log';

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function order_status()
    {
        return $this->belongsTo(OrdersStatus::class, 'orders_statuses_id');
    }

}
