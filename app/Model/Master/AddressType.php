<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class AddressType extends Model
{
    protected $table = "address_types";
    protected $fillable = [
        'id',
        'name',
        'user_id'
    ];
}
