<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ExcelIncomeLog extends Model
{
    protected $table = 'excel_income_logs';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
