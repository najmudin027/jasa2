<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class GaransiChatDetail extends Model
{
    protected $table = 'garansi_chat_details';

    protected $guarded = [];

    protected $appends = ['order', 'attachment_src'];

    public function getOrderAttribute()
    {
        return json_decode($this->order_json);
    }

    public function getAttachmentSrcAttribute()
    {
        return ($this->attachment == null) ? '' : asset('storage/user/chat/' . $this->attachment);
    }


    public function garansi()
    {
        return $this->belongsTo(GaransiChat::class, 'garansi_chat_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
