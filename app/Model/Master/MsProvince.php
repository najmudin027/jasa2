<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class MsProvince extends Model
{
    protected $table = 'ms_provinces';

    protected $fillable = [
        'name', 'iso_code', 'ms_country_id', 'meta'
    ];

    // protected $casts = [
    //     'meta' => 'array',
    // ];

    public function country()
    {
        return $this->belongsTo(MsCountry::class, 'ms_country_id');
    }

    public function city()
    {
        return $this->hasMany(MsCity::class);
    }

    public function districts()
    {
        return $this->hasManyThrough(MsDistrict::class, MsCity::class);
    }
}
