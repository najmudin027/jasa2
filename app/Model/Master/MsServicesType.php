<?php

namespace App\Model\Master;

use App\Model\Master\MsPriceService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsServicesType extends Model
{
    // use SoftDeletes;

    protected $table = 'ms_services_types';

    protected $guarded = [];

    protected $appends = ['link_image'];

    public static function getImagePathUpload()
    {
        return 'public/services-type';
    }

    public function getLinkImageAttribute()
    {
        return ($this->images == null) ? asset('/storage/services/service_not_found.jpeg') : asset('/storage/services-type/' . $this->images);
    }

    public function symptom()
    {
        // return $this->belongsTo(MsSymptom::class, 'ms_symptoms_id')->withTrashed();
        return $this->belongsTo(MsSymptom::class, 'ms_symptoms_id');
    }

    public function price_service()
    {
        return $this->hasMany(MsPriceService::class, 'ms_services_types_id');
    }

    public function product_group()
    {
        return $this->hasMany(ProductGroup::class, 'ms_services_type_id');
    }
}
