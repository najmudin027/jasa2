<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PartDataExcelStockBundle extends Model
{
    protected $table = 'ode_part_data_stock_bundle';

    protected $guarded = [];

    protected $appends = [
        'available',
    ];

    public function part_data_stock_bundle_detail()
    {
        return $this->hasMany(PartDataExcelStockBundleDetail::class, 'part_data_stock_bundle_id', 'id');
    }

    public function pdes_history()
    {
        return $this->hasMany(PDESHistory::class, 'part_data_stock_bundle_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function asc()
    {
        return $this->belongsTo(ASCExcel::class, 'asc_id');
    }

    public function getAvailableAttribute()
    {
        $val = 1;
        $datas = PartDataExcelStockBundleDetail::with('part_data_stock_inventory')->where('part_data_stock_bundle_id', $this->id)->get();
        foreach($datas as $data) {
            if($data->quantity > $data->part_data_stock_inventory->stock) {
                return $val = 0;
            }
        }
        return $val;
    }

}
