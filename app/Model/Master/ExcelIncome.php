<?php

namespace App\Model\Master;

use App\User;
use App\Model\Master\ExcelIncomeDetail;
use Illuminate\Database\Eloquent\Model;

class ExcelIncome extends Model
{
    protected $table = 'excel_income';

    protected $guarded = [];

    protected $appends = [
        'total_in_nominal',
        'total_refund'
    ];


    public function excel_income_detail()
    {
        return $this->hasMany(ExcelIncomeDetail::class, 'excel_income_id', 'id');
    }

    public function excel_income_log()
    {
        return $this->hasMany(ExcelIncomeLog::class, 'excel_income_id', 'id');
    }

    public function getTotalInNominalAttribute() {
        if($this->excel_income_detail()) {
            return $this->excel_income_detail()->sum('in_nominal');
        }
        return 0;
    }

    public function getTotalRefundAttribute() {
        if($this->excel_income_detail()) {
            return $this->excel_income_detail()->sum('refund');
        }
        return 0;
    }

}
