<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class GeneralSetting extends Model
{
    protected $table = 'general_settings';
    protected $fillable = [
        'name',
        'value',
        'desc',
    ];
}
