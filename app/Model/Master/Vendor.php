<?php

namespace App\Model\Master;

use App\User;
use App\Model\Product\ProductVendor;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = [
        'name',
        'code',
        'office_email',
        'is_business_type',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product_vendor()
    {
        return $this->hasMany(ProductVendor::class);
    }

    public function product() {
        return $this->hasMany(MsProduct::class);
    }
}
