<?php

namespace App\Model\Master;

use DB;
use App\User;
use Illuminate\Database\Eloquent\Model;

class MsOrderComplaint extends Model
{
    protected $table = 'orders_complaint';

    protected $guarded = [];

    protected $appends = ['link_image'];

    public function getLinkImageAttribute()
    {
        return ($this->attachment == null) ? '' : asset('customer/storage/attachment/complaint/' . $this->attachment);
    }

    // protected $appends = ['status_html'];

    /**
     * generate nomor tiket
     *
     * @return string | tahun-order_no example: 20-001
     */

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'ms_orders_id');
    }
}
