<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class MsPriceItems extends Model
{
    protected $table = 'price_items';

    protected $fillable = [
        'value',
        'ms_unit_type_id',
        'ms_city_id',
        'ms_district_id',
        'ms_village_id',
        'ms_product_id'
    ];

    public function unit()
    {
        return $this->belongsTo(UnitTypes::class, 'ms_unit_type_id');
    }

    public function city()
    {
        return $this->belongsTo(MsCity::class, 'ms_city_id');
    }

    public function district()
    {
        return $this->belongsTo(MsDistrict::class, 'ms_district_id');
    }

    public function village()
    {
        return $this->belongsTo(MsVillage::class, 'ms_village_id');
    }

    public function product()
    {
        return $this->belongsTo(MsProduct::class, 'ms_product_id');
    }
}
