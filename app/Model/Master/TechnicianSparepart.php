<?php

namespace App\Model\Master;

use App\Model\Master\MsPriceService;
use App\Model\Technician\Technician;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechnicianSparepart extends Model
{
    use SoftDeletes;
    protected $table = 'technicians_spareparts';

    protected $guarded = [];

    public static function getImagePathUpload()
    {
        return 'public/services';
    }
    public function service()
    {
        return $this->belongsTo(Services::class, 'ms_services_id');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'technicians_id');
    }

    public function sparepart_detail() {
        return $this->hasMany(SparepartDetail::class);
    }
}
