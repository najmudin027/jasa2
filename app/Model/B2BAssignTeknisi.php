<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Model\RequestOrderDetail;

class B2BAssignTeknisi extends Model
{
    protected $table = 'business_to_business_schedule_teknisi';

    protected $guarded = [];

    public function get_teknisi()
    {
        return $this->belongsTo(User::class, 'b2b_technician_id', 'id');
    }

    public function get_order()
    {
        return $this->belongsTo(RequestOrderDetail::class, 'order_id', 'id');
    }
}
