<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BusinessToBusinessTransactionLog extends Model
{
    protected $table = 'business_to_business_transaction_logs';

    protected $guarded = [];

    protected $appends = [
        'before',
        'after',
    ];

    public function getBeforeAttribute()
    {
        return json_decode($this->playload_before);
    }

    public function getAfterAttribute()
    {
        return json_decode($this->playload_after);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
