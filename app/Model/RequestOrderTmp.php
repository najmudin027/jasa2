<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RequestOrderTmp extends Model
{
    protected $table = "ms_request_order_tmp";
    protected $guarded = [];

    public static function getImagePathUpload()
    {
        return 'public/request_order_image/';
    }

    public function business_to_business_outlet_detail_transactions()
    {
        return $this->hasMany(BusinessToBusinessOutletDetailTransaction::class, 'business_to_business_outlet_transaction_id', 'id');
    }
}