<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class TeknisiInfoRekRequest extends FormRequest
{
    use HandleFailedValidationApi;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rekening_bank_name' => ['string', 'max:100'],
            'rekening_number' => ['string', 'max:100'],
            'rekening_name' => ['string', 'max:100'],
        ];
    }
}
