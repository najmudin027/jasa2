<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class UserStoreB2bRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {return [
            'name' => ['required', 'string', 'max:255'],
            // 'username' => ['required', 'string', 'max:255'],
            'ms_company_id' => ['required'],
            // 'attachment' => 'required|image|mimes:jpeg,png,jpg',
            'email' =>['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ];
    }
}
