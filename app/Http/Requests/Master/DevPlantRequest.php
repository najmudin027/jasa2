<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class DevPlantRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->insertRule();
    }

    public function insertRule()
    {
        // update
        if (isset($this->id)) {
            return [
                'user_id' => ['required', 'string', 'max:50' . $this->id],
                'curr_sequence_id' => ['required', 'string', 'max:50' . $this->id],
                'status' => ['required', 'string', 'max:50' . $this->id],                
                'date_time' => ['required' . $this->id],
            ];
        }
        // insert
        return [
            'user_id' => ['required', 'string', 'max:50'],
            'curr_sequence_id' => ['required', 'string', 'max:50'],
            'status' => ['required', 'string', 'max:50'],            
            'date_time' => ['required'],
        ];
    }
}
