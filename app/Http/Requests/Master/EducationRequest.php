<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class EducationRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // update
        if (isset($this->id)) {
            return [
                // 'user_id'       => ['required'],
                'institution'   => ['required', 'string', 'max:50' . $this->id],
                'place'         => ['required', 'string', 'max:50' . $this->id],
                'education_start_date'  => ['required', 'date', 'max:50' . $this->id],
                'education_end_date'    => ['required', 'date', 'max:50' . $this->id],
                'type'          => ['required', 'string', 'max:50' . $this->id],
                'major'         => ['required', 'string', 'max:50' . $this->id],
            ];
        }
        // insert
        return [
            // 'user_id'       => ['required'],
            'institution'   => ['required', 'string', 'max:50'],
            'place'         => ['required', 'string', 'max:50'],
            'education_start_date'  => ['required', 'date', 'max:50'],
            'education_end_date'    => ['required', 'date', 'max:50'],
            'type'          => ['required', 'string', 'max:50'],
            'major'         => ['required', 'string', 'max:50'],
        ];
    }
}
