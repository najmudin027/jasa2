<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class InventoryRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            // 'cogs_value' => ['required', 'string', 'max:30'],
            // 'shipping' => ['required', 'numeric'],
            // 'product_id' => ['required', 'string', 'max:30'],
            // 'ms_batch_id' => ['required', 'string', 'max:30'],
            'ms_warehouse_id' => ['required', 'numeric'],
            'ms_batch_item_id' => ['required'],
            'ms_unit_type_id' => ['required'],
        ];
    }
}
