<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class UnitTypesRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->id)) {
            return $this->updateRule();
        }

        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'unit_name' => ['required', 'string', 'max:30', 'unique:unit_types,unit_name'],
        ];
    }

    public function updateRule()
    {
        return [
            'unit_name' => ['required', 'string', 'max:50', 'unique:unit_types,unit_name,' . $this->id],
        ];
    }
}
