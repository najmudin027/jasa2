<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class BankTransferRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // update
        // if (isset($this->id)) {
        //     return [
        //         'bank_name' => ['required', 'string' . $this->id],
        //         'no_rek' => ['required', 'string' . $this->id],
        //         'desc' => ['required', 'string' . $this->id],
        //         'icon' => ['required', 'string' . $this->id],
        //     ];
        // }
        // insert
        return [
            'bank_name' => ['required', 'string'],
            'name' => ['required', 'string'],
            'virtual_code' => ['required', 'string'],
            'desc' => ['required', 'string'],
            // 'icon' =>  ['file', 'max:1024', 'image', 'mimetypes:image/jpeg,image/png, image/gif']
        ];
    }
}
