<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class TeknisiInfoRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ms_religion_id' => ['required', 'integer'],
            'ms_marital_id' => ['required', 'integer'],
            'gender' => ['required', 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'date_of_birth' => ['required', 'date'],
            'address' => ['required', 'string'],
            // 'attachment' => ['required', 'file', 'max:1024', 'image', 'mimetypes:image/jpeg,image/png, image/gif'],
            'no_identity' => ['required', 'integer'],
        ];
    }
}
