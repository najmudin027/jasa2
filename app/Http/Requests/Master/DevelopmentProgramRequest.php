<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class DevelopmentProgramRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // update
        if (isset($this->id)) {
            return [
                'program_name' => [ $this->id],
                'desc' => [ $this->id],
            ];
        }
        // insert
        return [
            'program_name' => ['required', 'string', 'max:100'],
            'desc' => ['required', 'max:50'],
        ];
    }
}
