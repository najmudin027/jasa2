<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => ['required', 'string'],
            'ms_city_id' => ['required'],
            'ms_district_id' => ['required'],
            'ms_village_id' => ['required'],
            'ms_zipcode_id' => ['required'],
            'ms_address_type_id' => ['required'],
        ];
    }
}
