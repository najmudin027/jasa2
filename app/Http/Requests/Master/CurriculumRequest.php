<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class CurriculumRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // update
        if (isset($this->id)) {
            return [
                'curriculum_name' => ['required', 'string', 'max:50' . $this->id],
            ];
        }
        // insert
        return [
            'curriculum_name' => ['required', 'string', 'max:50'],
        ];
    }
}
