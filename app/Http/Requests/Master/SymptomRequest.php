<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class SymptomRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'ms_services_id' => ['required'],
            'name' => ['required', 'string', 'max:255'],


        ];
    }

    public function updateRule()
    {
        return [
            'ms_services_id' => ['required'],
            'name' => ['required', 'string', 'max:255'],
        ];
    }
}
