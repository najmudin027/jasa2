<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class PriceItemsRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'value' => ['required', 'string', 'max:30'],
            'ms_unit_type_id' => ['required', 'string', 'max:30'],
            'ms_city_id' => ['required', 'string', 'max:30'],
            'ms_district_id' => ['required', 'string', 'max:30'],
            'ms_village_id' => ['required', 'string', 'max:30'],
            'ms_product_id' => ['required', 'string', 'max:30'],
        ];
    }

    public function updateRule()
    {
        return [
            'value' => ['required', 'string', 'max:50', 'unique:price_items,' . $this->id],
            'ms_unit_type_id' => ['required', 'string', 'max:50', 'unique:price_items,' . $this->id],
            'ms_city_id' => ['required', 'string', 'max:50', 'unique:price_items,' . $this->id],
            'ms_district_id' => ['required', 'string', 'max:50', 'unique:price_items,' . $this->id],
            'ms_village_id' => ['required', 'string', 'max:50', 'unique:price_items,' . $this->id],
        ];
    }
}
