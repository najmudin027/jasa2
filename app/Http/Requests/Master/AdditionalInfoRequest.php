<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class AdditionalInfoRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ms_religion_id' => ['required', 'string', 'max:100'],
            'ms_marital_id' => ['required', 'string', 'max:100'],
            'first_name' => ['required', 'string', 'max:100'],
            'last_name' => ['string', 'max:100'],
            'date_of_birth' => ['date'],
            'place_of_birth' => ['max:50'],
            'gender' => ['numeric', 'max:1'],
            // 'picture' => ['file', 'max:1024', 'image', 'mimetypes:image/jpeg,image/png, image/gif']
        ];
    }
}
