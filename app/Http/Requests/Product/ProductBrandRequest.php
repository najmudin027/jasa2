<?php

namespace App\Http\Requests\Product;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class ProductBrandRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->id)) {
            return $this->updateRule();
        }

        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'name' => ['required', 'string', 'max:50', 'unique:product_brands,name'],
            'code' => ['required', 'string', 'max:50'],
            'product_part_category_id' => ['required']
        ];
    }

    public function updateRule()
    {
        return [
            'name' => ['required', 'string', 'max:50', 'unique:product_brands,name,' . $this->id],
            'code' => ['required', 'string', 'max:50'],
            'product_part_category_id' => ['required']
        ];
    }
}
