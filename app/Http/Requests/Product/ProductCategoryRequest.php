<?php

namespace App\Http\Requests\Product;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class ProductCategoryRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // insert
        return [
            'name' => ['required', 'max:255'],
            'slug' => ['required', 'max:100'],
            'product_cate_code' => ['required', 'string'],
        ];
    }
}
