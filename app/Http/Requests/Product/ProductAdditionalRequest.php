<?php

namespace App\Http\Requests\Product;

use App\Model\Product\ProductAdditional;
use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class ProductAdditionalRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->id)) {
            return $this->updateRule();
        }

        return $this->insertRule();
    }

    public function insertRule()
    {
        // insert
        return [
            'additional_code' => ['required', 'string', 'max:50', 'unique:' . (new ProductAdditional())->getTable()],
        ];
    }

    public function updateRule()
    {
        return [
            'additional_code' => ['required', 'string', 'max:50', 'unique:' . (new ProductAdditional())->getTable() . ',additional_code,' . $this->id],
        ];
    }
}
