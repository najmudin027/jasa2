<?php

namespace App\Http\Requests\Technician;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class JobTitleRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'description' => ['required'],
            'user_id' => ['required'],
            'job_title_category_id' => ['required'],
        ];
    }

    public function updateRule()
    {
        return [];
    }
}
