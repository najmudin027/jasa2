<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (is_numeric($this->email)) {
            $rule = ['required', 'max:13', 'regex:/^(\+62 ((\d{3}([ -]\d{3,})([- ]\d{4,})?)|(\d+)))|(\(\d+\) \d+)|\d{3}( \d+)+|(\d+[ -]\d+)|\d+$/', 'unique:users'];
        } else {
            $rule = ['required', 'string', 'email:rfc,dns', 'max:255', 'unique:users'];
        }
        
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => $rule,
            'phone' => ['string', 'max:255', 'unique:users', 'regex:/^(\+62 ((\d{3}([ -]\d{3,})([- ]\d{4,})?)|(\d+)))|(\(\d+\) \d+)|\d{3}( \d+)+|(\d+[ -]\d+)|\d+$/'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => preg_replace( '/[^0-9]/', '', $this->phone),
        ]);
    }
}
