<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // update
        if (isset($this->id)) {
            return $this->updateRule();
        }
        // insert
        return [
            'name' => ['required', 'string', 'max:150'],
            'url' => ['required', 'string'],
            'module' => ['required', 'string'],
            'icon' => ['required', 'string'],

        ];
    }

    public function updateRule()
    {
        return [
            'name' => ['required', 'string', 'max:150'],
            'url' => ['required', 'string'],
            'module' => ['required', 'string'],
            'icon' => ['required', 'string']
        ];
    }
}
