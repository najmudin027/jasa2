<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ListTeknisiRequest extends FormRequest
{
    use HandleFailedValidationApi;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sort_by' => [Rule::in(['name', 'price', 'rating', 'review', 'teknisi_join', 'service_join'])],
            'order_by' => [Rule::in(['asc', 'desc'])],
            'service_type_id' => ['required'],
            'product_group_id' => ['required']
        ];
    }
}
