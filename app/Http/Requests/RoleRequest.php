<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // update
        if (isset($this->id)) {
            return $this->updateRole();
        }
        // insert
        return $this->insertRole();
    }

    public function insertRole()
    {
        return [
            'name' => ['required', 'string', 'max:50', 'unique:roles'],
            'permissions' => ['required', 'array'],
        ];
    }

    public function updateRole()
    {
        return [
            'name' => ['required', 'string', 'max:50', 'unique:roles,name,' . $this->id],
            'permissions' => ['required', 'array'],
        ];
    }
}
