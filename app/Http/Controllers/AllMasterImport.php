<?php

namespace App\Imports;

use App\Imports\CityImport;
use App\Imports\CountryImport;
use App\Imports\VillageImport;
use App\Imports\DistrictImport;
use App\Imports\ProvinceImport;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithConditionalSheets;

class AllMasterImport implements WithMultipleSheets, SkipsUnknownSheets
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    use WithConditionalSheets;

    public function conditionalSheets(): array
    {
        return [
            'country'  => new CountryImport(),
            'province' => new ProvinceImport(),
            'city'     => new CityImport(),
            'district' => new DistrictImport(),
            'village'  => new VillageImport(),
        ];
    }

    public function onUnknownSheet($sheetName)
    {
        // E.g. you can log that a sheet was not found.
        info("Sheet {$sheetName} was skipped");
    }


}
