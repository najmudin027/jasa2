<?php

namespace App\Http\Controllers\Api\Technician;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Technician\JobTitleCategoryRequest;
use App\Model\Technician\JobTitleCategory;
use App\Services\Technician\JobTitleCategoryService as Service;
use Illuminate\Http\Request;

class JobTitleCategoryController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(JobTitleCategoryRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(JobTitleCategory $category)
    {
        return $this->successResponse($category);
    }

    /**
     * update data
     */
    public function update(JobTitleCategoryRequest $request, JobTitleCategory $category, Service $service)
    {
        return $this->successResponse(
            $service->setCategory($category)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(JobTitleCategory $category, Service $service)
    {
        return $this->successResponse(
            $service->setCategory($category)->delete(),
            'success',
            204
        );
    }
}
