<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\CategoryRequest;
use App\Model\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CategoryController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Category::with('childrens')->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * use datatables json structure
     *
     * @return \Illuminate\Http\Response
     */
    public function datatables()
    {
        return DataTables::of(Category::whereNull('parent_id')->with('childs'))
            ->toJson();
    }

    public function search(Request $request)
    {
        $response = Category::with('childs')
            ->where('name', 'like', '%' . $request->name . '%')
            ->limit(10)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request, CategoryService $categoryService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $categoryService->create($request->all()),
            'success',
            201
        );
    }

    /**
     * Display the specified resource.
     *
     *  @param  \App\Model\Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $response = $category;

        return $this->successResponse($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CategoryRequest  $request
     * @param  \App\Model\Category $category
     * @param  \App\Services\CategoryService $categoryService
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category, CategoryService $categoryService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $categoryService->setCategory($category)->update($request->all()),
            'success'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Category $category
     * @param  \App\Services\CategoryService $categoryService
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category, CategoryService $categoryService)
    {
        return $this->successResponse(
            $categoryService->setCategory($category)->delete(),
            'success',
            204
        );
    }
}
