<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\UserStoreB2bRequest;
use App\Http\Requests\UserUpdateB2bRequest;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\ApiController;
use Yajra\DataTables\DataTables;
use App\Services\UserB2bService;
use Illuminate\Http\Request;
use App\User;
use App\Model\GeneralSettingB2b;
use Auth;
use App\Model\Company;
use App\Model\RequestOrders;
use App\Model\RequestOrderTmp;
use App\Model\RequestOrderImageTmp;
use App\Model\RequestOrderDetail;
use App\Model\BusinessToBusinessTransaction;
use App\Model\BusinessToBusinessOutletTransaction;
use App\Model\B2BDataTransactions;
use App\Model\BusinessToBusinessOutletDetailTransaction;

class UserB2BControler extends ApiController
{
    public function getOutlet()
    {
        $showOutlet = BusinessToBusinessTransaction::where('company_id', Auth::user()->ms_company_id)->with('business_to_business_outlet_transactions')->first();

        return $this->successResponse($showOutlet);
    }

    public function getOutletAdmin($id)
    {
        // $getOutlet = BusinessToBusinessOutletTransaction::get();
        // return $this->successResponse($getOutlet);
        $getAdmin = BusinessToBusinessOutletTransaction::get();
        $findCompany = User::where('id', $id)->with('get_comp')->first();
        $getOutlet = BusinessToBusinessOutletTransaction::where('business_to_business_transaction_id', $findCompany->get_comp->id)->count();
        if($getOutlet > 0){
            $getOutlet = BusinessToBusinessOutletTransaction::where('business_to_business_transaction_id', $findCompany->get_comp->id)->get();
            return $this->successResponse($getOutlet);
        }else{
            return $this->errorResponse('data', 'Data Outlet Tidak Ditemukan');
        }
        // asd as d

        return $this->successResponse($adminShowOutlet);
    }

 
    public function getUserB2b()
    {
        $getUser = User::whereNotNull('is_b2b_users')->whereNull('is_b2b_teknisi')->get();
        return $this->successResponse($getUser);
    }

    public function getOutletUnit(Request $request, $id)
    {
        $showUnitWhereOutlet = BusinessToBusinessOutletDetailTransaction::where('business_to_business_outlet_transaction_id', $id)
        ->whereNull('is_parent_b2b_outlet_id')
        ->count();

        if($showUnitWhereOutlet > 0){
            $showUnitWhereOutlet = BusinessToBusinessOutletDetailTransaction::with('get_type')->where('business_to_business_outlet_transaction_id', $id)
            ->whereNull('is_parent_b2b_outlet_id')
            ->get();
            return $this->successResponse($showUnitWhereOutlet);
        }else{
            return $this->errorResponse('data', 'Tidak Ada Unit di Outlet Ini');
        }

        
    }

    public function addToCartOrder(Request $request, UserB2bService $userB2bService)
    {  
        $cekOutlet = BusinessToBusinessOutletTransaction::where('id', $request->ms_outlet_id)->count();
        $cekUnit = BusinessToBusinessOutletDetailTransaction::where('id', $request->ms_transaction_b2b_detail)->whereNull('is_parent_b2b_outlet_id')->count();
        if($cekOutlet == 1){
            if($cekUnit == 1){
                return $this->successResponse(
                    $userB2bService->addToCard($request->all()),
                    'success',
                    201
                );
            }else{
                return $this->errorResponse($cekUnit, 'Data Unit Tidak Ditemukan');
            }
        }else{
            return $this->errorResponse($cekOutlet, 'Data Outlet Tidak Ditemukan');
        }
        $request->validated();
    }

    public function updateRequestOrder(Request $request)
    {
        // dd($request->id);
        if(!empty($request->id)){
            $cek = BusinessToBusinessOutletDetailTransaction::where('id', $request->id)->first();
            if(!empty($cek)){
                $cek->update([
                    'unit_ke' => $request->unit_ke
                ]);
                return $this->successResponse($cek , 'Unit Telah Di perbaharui');
            }else{
                return $this->errorResponse('data', 'Data Tidak Ditemukan');
            }
        }else{
            return $this->errorResponse('data', 'Id Transaksi Tidak Ditemukan Atau Kosong');
        }
    }



    public function storeOrderRequest(OrderRequest $request, UserB2bService $userB2bService)
    {  
        $cekOutlet = BusinessToBusinessOutletTransaction::where('id', $request->ms_outlet_id)->count();
        $cekUnit = BusinessToBusinessOutletDetailTransaction::where('id', $request->ms_transaction_b2b_detail)->whereNull('is_parent_b2b_outlet_id')->count();
        if($cekOutlet == 1){
            if($cekUnit == 1){
                return $this->successResponse(
                    $userB2bService->requestOrderSingle($request->all()),
                    'success',
                    201
                );
            }else{
                return $this->errorResponse($cekUnit, 'Data Unit Tidak Ditemukan');
            }
        }else{
            return $this->errorResponse($cekOutlet, 'Data Outlet Tidak Ditemukan');
        }
        $request->validated();
    }

    public function requestOrderTmp(OrderRequest $request, UserB2bService $userB2bService)
    {  
        $cekOutlet = BusinessToBusinessOutletTransaction::where('id', $request->ms_outlet_id)->count();
        $cekUnit = BusinessToBusinessOutletDetailTransaction::where('id', $request->ms_transaction_b2b_detail)->whereNull('is_parent_b2b_outlet_id')->count();
        if($cekOutlet == 1){
            if($cekUnit == 1){
                return $this->successResponse(
                    $userB2bService->requestOrderTemporari($request->all()),
                    'success',
                    201
                );
            }else{
                return $this->errorResponse($cekUnit, 'Data Unit Tidak Ditemukan');
            }
        }else{
            return $this->errorResponse($cekOutlet, 'Data Outlet Tidak Ditemukan');
        }
        $request->validated();
    }

    public function storeOrderInTmp(Request $request)
    {
        $getCart = RequestOrderTmp::where('user_id', Auth::user()->id)->with('business_to_business_outlet_detail_transactions')->get();
        $delete = RequestOrderTmp::where('user_id', Auth::user()->id)->get('id');
        
        
        $coundTmp = RequestOrderTmp::where('user_id', Auth::user()->id)->count();
        if($coundTmp > 0){
            try{
                $reqOrder = RequestOrders::create([
                    'request_code' => $this->generateOrderCode(),
                    'user_id' => Auth::user()->id,
                    'created_at' => date('Y-m-d H:i:s')
                ]);

                for ($i = 0; $i < $coundTmp; $i++) {
                    $storeDetailOrder = RequestOrderDetail::create([
                        'request_code' => $reqOrder->request_code,
                        'ms_outlet_id' => $getCart[$i]->ms_outlet_id,
                        'ms_transaction_b2b_detail' => $getCart[$i]->ms_transaction_b2b_detail,
                        'service_type' => $getCart[$i]->service_type,
                        'user_id' => $getCart[$i]->user_id,
                        'status_quotation' => $getCart[$i]->status_quotation,
                        'ms_b2b_detail' => $getCart[$i]->ms_b2b_detail,
                        'vidio' => $getCart[$i]->vidio,
                        'image' => $getCart[$i]->image,
                        'remark' => $getCart[$i]->remark,
                    ]);
                }

                $this->deleteOrder($request);

            }catch (QueryException $e) {
                throw new \Exception($e->getMessage());
            }
            return $this->successResponse($reqOrder);
        }else{
            return $this->errorResponse('data', 'Cart Anda Kosong');
        }
        
        
        
    }

    public function showDataCart()
    {
        $showCart = RequestOrderTmp::where('user_id', Auth::user()->id)->get();
        return $this->successResponse($showCart);
    }

    public function destroyCartData(Request $request)
    {
        $cek = RequestOrderTmp::where('id', $request->id)->first();
        if(!empty($request->id)){
            if(!empty($cek)){
                $cek->delete();
                return $this->successResponse('data', 'Data Berhasil Dihapus');
            }else{
                return $this->errorResponse('data', 'Data Tidak Ditemukan');
            }
        }else{
            return $this->errorResponse('data', 'ID Tidak Ditemukan');
        }
        
    }

    public function listQuotation(Request $request)
    {
        // dd($request->id);
        $listQuotation = B2BDataTransactions::where('id_transaction', $request->id)->orderBy('created_at', 'desc')->get();
        return $this->successResponse($listQuotation, 'Data Berhasil Ditemukan');
    }

    public function detailQuotation(Request $request)
    {
        $showQuotation = B2BDataTransactions::with(['get_part', 'get_type',  'get_labor'])->where('id', $request->id)->first();
        return $this->successResponse($showQuotation, 'Data Berhasil Ditemukan');
    }

    public function detailInvoice(Request $request)
    {
        $account_no = GeneralSettingB2b::where('name', 'account_no')->first();
        $bank_name = GeneralSettingB2b::where('name', 'bank_name')->first();
        $bank_address = GeneralSettingB2b::where('name', 'bank_address')->first();
        $beneficary = GeneralSettingB2b::where('name', 'beneficary')->first();
        $showInv = B2BDataTransactions::with(['get_part', 'get_type', 'get_labor'])->where('id_transaction', $request->id)->orderBy('id', 'Desc')->first();
        return $this->successResponse([
            'account_no' => $account_no,
            'bank_name' => $bank_name,
            'bank_address' => $bank_address,
            'beneficary' => $beneficary,
            'showInv' => $showInv,
        ], 'Data Invoice Berhasil Ditemukan');
    }

    public function deleteOrder(Request $request)
    {
        $getCart = RequestOrderTmp::where('user_id', Auth::user()->id)->with('business_to_business_outlet_detail_transactions')->get();
        $delete = RequestOrderTmp::where('user_id', Auth::user()->id)->get('id');
        $coundTmp = RequestOrderTmp::where('user_id', Auth::user()->id)->count();
        for ($i = 0; $i < $coundTmp; $i++) {
            $deleteTmp = RequestOrderTmp::find($getCart[$i]->id)->delete();
        }
    }

    public function generateOrderCode()
    {
        $q          = RequestOrders::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('dmY');
        $number     = 1; //format
        $new_number = sprintf("%06s", $number);
        $code       = 'ORD-' . $date . ($separator) . ($new_number);
        $order_code   = $code;
        if ($q > 0) {
            $last     = RequestOrders::orderBy('id', 'desc')->value('request_code');
            $last     = explode("-", $last);

            $order_code = 'ORD-' . $date . ($separator) . (sprintf("%06s", $last[1] + 1));
        }
        return $order_code;
    }
    
    public function destroyRequestOrder($id)
    {
        $delete = RequestOrders::find($id)->delete();
        return $this->successResponse(
            $delete,
            'success',
            201
        );
    }
    
    public function datatables()
    {
        // $user = User::where('is_b2b_users', 1)->wherenull('is_b2b_teknisi')->orderBy('name', 'asc');
        $user = User::where('is_b2b_users', 1)->wherenull('is_b2b_teknisi')->orderBy('name', 'asc');

        return DataTables::of($user)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(Request $request)
    {
        $response = Company::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->orderBy('name', 'asc')
            ->get();

        return $this->successResponse($response);
    }

    public function store(UserStoreB2bRequest $request, UserB2bService $userB2bService)
    {
        $request->validated();
        // create user
        $user = $userB2bService->dontSendNotif()->createFromAdmin($request->all());

        // generate token
        $token = $user->createToken($userB2bService->getTokenName())->accessToken;

        // response
        $response = [
            'user'  => $user,
            'token' => $token,
        ];

        return $this->successResponse($response, $this->successStoreMsg(), 201);
    }

    public function update(UserB2bService $userService, UserUpdateB2bRequest $request, $id)
    {
        // validasi
        $request->validated();

        // user
        $user = User::where('id', $id)->firstOrFail();

        // update user
        $test = $userService
            ->update($request->all(), $user);

        // generate token
        $token = $user->createToken($userService->getTokenName())->accessToken;

        // set response
        $response = [
            'user'  => $user,
            'token' => $token,
        ];

        // send response
        return $this->successResponse($response, $this->successUpdateMsg());
    }

    public function destroy($id, UserB2bService $userService)
    {
        return $this->successResponse(
            $userService->deleteAll($id),
            'success delete',
            204
        );
    }

    public function changStatus(Request $request, $id)
    {
        // user
        $user = User::where('id', $id)->firstOrFail();

        $user->update([
            'status' => $user->status == 1 ? 0 : 1
        ]);

        // send response
        return $this->successResponse($user, $this->successUpdateMsg());
    }

    
}
