<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Product\ProductAdditionalRequest;
use App\Model\Product\ProductAdditional;
use App\Services\Product\ProductAdditionalService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductAdditionalController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(ProductAdditionalService $productAdditionalService)
    {
        $response = $productAdditionalService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = ProductAdditional::where('additional_code', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(ProductAdditionalService $productAdditionalService)
    {
        $query = $productAdditionalService->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(ProductAdditionalRequest $request, ProductAdditionalService $productAdditionalService)
    {
        return $this->successResponse(
            $productAdditionalService->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(ProductAdditional $product_additional)
    {
        $response = $product_additional;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ProductAdditionalRequest $request, ProductAdditional $product_additional, ProductAdditionalService $productAdditionalService)
    {
        return $this->successResponse(
            $productAdditionalService->setProductAdditional($product_additional)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(ProductAdditional $product_additional, ProductAdditionalService $productAdditionalService)
    {
        return $this->successResponse(
            $productAdditionalService->setProductAdditional($product_additional)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, ProductAdditionalService $productAdditionalService)
    {
        return $this->successResponse(
            $productAdditionalService->deleteById($request->id),
            'success',
            204
        );
    }
}
