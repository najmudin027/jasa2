<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\GradeRequest;
use App\Model\Master\Grade;
use Illuminate\Http\Request;
use App\Services\Master\GradeService as Service;

class GradeController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(GradeRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(Grade $grade)
    {
        $response = $grade;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(GradeRequest $request, Grade $grade, Service $service)
    {
        return $this->successResponse(
            $service->setGrade($grade)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(Grade $grade, Service $service)
    {
        return $this->successResponse(
            $service->setGrade($grade)->delete(),
            'success',
            204
        );
    }
}
