<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Model\Master\MsProduct;
use Yajra\DataTables\DataTables;
use App\Model\Product\ProductVarian;
use App\Model\Product\ProductVendor;
use App\Http\Controllers\ApiController;
use App\Model\Product\ProductAttribute;
use App\Services\Master\ProductService;
use App\Http\Requests\Master\ProductRequest;
use App\Model\Product\ProductVarianAttribute;

class ProductController extends ApiController
{
    /**
     * show list with paginate
     */

    public function index(ProductService $productService)
    {
        $response = $productService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = MsProduct::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }


    public function searchDatatables(ProductService $service, Request $request)
    {
        return $service->searchDatatables($request);
    }


    public function findVendorOfProduct($id)
    {
        $query = ProductVendor::with('product', 'vendor')->where('ms_product_id', $id)->get();
        return $this->successResponse($query);
    }

    public function findAttributeOfProduct($id)
    {
        $q = ProductVendor::leftJoin('product_varians', function ($join) {
            $join->on('product_vendors.id', '=', 'product_varians.product_vendor_id');
        })->leftJoin('product_varian_attributes', function ($join) {
            $join->on('product_varians.id', '=', 'product_varian_attributes.product_varian_id');
        })->leftJoin('product_attributes', function ($join) {
            $join->on('product_attributes.id', '=', 'product_varian_attributes.product_attribute_id');
        })->leftJoin('product_attribute_terms', function ($join) {
            $join->on('product_varian_attributes.product_attribute_term_id', '=', 'product_attribute_terms.id');
        })->select(
            'product_vendors.id as product_vendor_id',
            'product_varians.id as product_varian_id',
            'product_attributes.name as attribute_name',
            'product_attributes.id as product_attribute_id',
            'product_varian_attributes.product_attribute_term_id as product_term_id',
            'product_attribute_terms.name as term_name'
        )
            ->where('vendor_id', $id)->get();
        $combination = [];
        $att = [];
        foreach ($q as $pAttribute) {
            $combination[$pAttribute['product_attribute_id']][$pAttribute['product_term_id']] = $pAttribute;
        }
        // return $combination;
        foreach ($combination as $attribute_id => $com) {
            $att[] = [
                'attribute' => ProductAttribute::find($attribute_id),
                'terms' => $com,
            ];
        }
        return $att;
        $query = ProductVarianAttribute::where('product_varian_id', $id)->get();
        return $this->successResponse($query);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(ProductService $productService)
    {
        $query = MsProduct::with([
            'inventories',
            'productCombineCategories.product_category',
            'model',
            'additional',
            'productVendors.vendor',
            'productVendors.productVarians.productVarianAttributes.attribute',
            'productVendors.productVarians.productVarianAttributes.term',
        ]);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(ProductRequest $request, ProductService $productService)
    {
        return $this->successResponse(
            $productService->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function edit(MsProduct $product)
    {
        $response = $product;
        return $this->successResponse($response);
    }

    public function detailsProduct($id)
    {
        $query = MsProduct::with('model', 'additional', 'product_vendor.vendor', 'product_vendor.product_varian.product_varian_attribute.attribute', 'product_vendor.product_varian.product_varian_attribute.term')->where('id', $id)->get();
        return $query;
    }

    public function listProVenByProdId($product_id)
    { // list Product Vendor By product_id
        $query = ProductVendor::with('vendor')->where('ms_product_id', $product_id)->get();
        return $query;
    }

    public function listProVarByProdVenId($product_vendor_id)
    { // list Product Varian By product_vendor_id
        $query = ProductVarian::with('product_vendor.vendor')->where('product_vendor_id', $product_vendor_id)->get();
        return $query;
    }

    public function listProVarAttrByProdVarId($product_varian_id)
    { // list Product Varian Attribute By product_varian_id
        $query = ProductVarianAttribute::with('attribute', 'term')->where('product_varian_id', $product_varian_id)->get();
        return $query;
    }

    /**
     * update data
     */
    public function update(ProductRequest $request, MsProduct $product, ProductService $productService)
    {
        return $this->successResponse(
            $productService->setProduct($product)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(MsProduct $product, ProductService $productService)
    {
        return $this->successResponse(
            $productService->setProduct($product)->delete(),
            'success',
            204
        );
    }

    public function deleteProductVendor($id)
    {
        $productVendor   = ProductVendor::find($id);
        $productVendorId = $productVendor->pluck('id')->toArray();
        $productVarian   = ProductVarian::whereIn('product_vendor_id', $productVendorId);
        $productVarianId = $productVarian->pluck('id')->toArray();
        $productVarian   = $productVendor->delete();
        $productVendor   = $productVendor->delete();
        return $this->successResponse(
            'id ' . $id . ' Product Vendor success deleted!',
            'success',
            200
        );
    }

    public function deleteProductVarian($id)
    {
        ProductVarian::find($id)->delete();
        return $this->successResponse(
            'id ' . $id . ' Product Varian success deleted!',
            'success',
            200
        );
    }
}
