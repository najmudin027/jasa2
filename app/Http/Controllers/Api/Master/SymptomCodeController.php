<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Services\Master\SymptomCodeService as Service;
use App\Http\Requests\Master\SymptomCodeRequest;
use App\Model\Master\SymptomCode;

class SymptomCodeController extends ApiController
{
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $paginate = $request->paginate;
        $response = $service->list()
            ->when($q, function ($query, $q) {
                $query->where('name', 'like', '%' . $q . '%');
            });
        
        if ($paginate == 'no_paginate') {
            $response = $response->get();
        }else{
            $response = $response->paginate(10);
        }
        

        return $this->successResponse($response);
    }

    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    public function store(SymptomCodeRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    public function update(SymptomCodeRequest $request, SymptomCode $symptomCode, Service $service)
    {
        return $this->successResponse(
            $service->setSymptomCode($symptomCode)->update($request->all()),
            'success'
        );
    }

    public function delete(SymptomCode $symptomCode, Service $service)
    {
        return $this->successResponse(
            $service->setSymptomCode($symptomCode)->delete($symptomCode->id),
            'success',
            201
        );
    }
}
