<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Services\Master\EducationService as Service;
use App\Http\Requests\Master\EducationRequest;
use Yajra\DataTables\DataTables;
use App\Model\Master\Educations;

class EducationsController extends ApiController
{
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);
        return $this->successResponse($response);
    }

    public function select2(Request $request)
    {
        $response = Educations::where('institution', 'like', '%' . $request->q . '%')
                    ->limit(20)
                    ->get();
        return $this->successResponse($response);
    }

    public function dataTables(Service $service)
    {
        $query = $service->list();
        return DataTables::of($query)->toJson();
    }

    public function store(Service $service, EducationRequest $educationRequest)
    {
        $educationRequest->validated();

        return $this->successResponse(
            $service->create($educationRequest->all()),
            'Success',
            201
        );
    }

    public function update(Service $service, EducationRequest $educationRequest, Educations $education)
    {
        $educationRequest->validated();

        return $this->successResponse(
            $service->setEducation($education)->update($educationRequest->all()),
            'Success'
        );
    }

    public function destroy(Service $service, Educations $education)
    {
        return $this->successResponse(
            $service->setEducation($education)->delete(),
            'Success',
            204
        );
    }
}
