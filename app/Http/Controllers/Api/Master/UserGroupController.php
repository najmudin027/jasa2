<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\UserGroupRequest;
use App\Model\Master\UserGroup;
use App\Services\User\UserGroupService as Service;
use Illuminate\Http\Request;

class UserGroupController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(UserGroupRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(UserGroup $user_group)
    {
        return $this->successResponse($user_group);
    }

    /**
     * update data
     */
    public function update(UserGroupRequest $request, UserGroup $user_group, Service $service)
    {
        return $this->successResponse(
            $service->setUserGroup($user_group)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(UserGroup $user_group, Service $service)
    {
        return $this->successResponse(
            $service->setUserGroup($user_group)->delete(),
            'success',
            204
        );
    }
}
