<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Services\Master\TicketService as Service;
use App\Http\Requests\Master\TicketRequest;
use App\Model\Master\MsTicket as Ticket;

class TicketController extends ApiController
{
    /**
     * show list
     */
    public function index(Service $service)
    {
        return $this->successResponse(
            $service->list()->paginate(10)
        );
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(TicketRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * update data
     */
    public function update(TicketRequest $request, Ticket $ticket, Service $service)
    {
        return $this->successResponse(
            $service->setTicket($ticket)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * update data
     */
    public function updateStatus(Request $request, Ticket $ticket, Service $service)
    {
        return $this->successResponse(
            $service->setTicket($ticket)->updateStatus($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(Ticket $ticket, Service $service)
    {
        return $this->successResponse(
            $service->setTicket($ticket)->delete(),
            'success',
            204
        );
    }
}
