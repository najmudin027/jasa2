<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Model\Master\MsZipcode;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\ApiController;
use App\Services\Master\ZipcodeService;
use App\Http\Requests\Master\ZipcodeRequest;
use App\Traits\MasterImportTrait;

class ZipcodeController extends ApiController
{
    /**
     * show list with paginate
     */
    use MasterImportTrait;
    public function index(ZipcodeService $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = MsZipcode::where('zip_no', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(ZipcodeService $service)
    {
        $query = $service->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(ZipcodeRequest $request, ZipcodeService $service)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(MsZipcode $zipcode)
    {
        $response = $zipcode;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ZipcodeRequest $request, MsZipcode $zipcode, ZipcodeService $service)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $service->setZipcode($zipcode)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(MsZipcode $zipcode, ZipcodeService $service)
    {
        return $this->successResponse(
            $service->setZipcode($zipcode)->delete(),
            'success',
            204
        );
    }

    public function importExcel(Request $request)
	{
        return $this->importExcelTraits('zipcode', $request); // country is type of controller name
    }

    public function destroyBatch(Request $request, ZipcodeService $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }

}
