<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\BatchStatusRequest;
use App\Model\Master\BatchStatus;
use App\Services\Master\BatchStatusService as Service;
use Illuminate\Http\Request;

class BatchStatusController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(BatchStatusRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(BatchStatus $status)
    {
        $response = $status;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(BatchStatusRequest $request, BatchStatus $status, Service $service)
    {
        return $this->successResponse(
            $service->setStatus($status)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(BatchStatus $status, Service $service)
    {
        return $this->successResponse(
            $service->setStatus($status)->delete(),
            'success',
            204
        );
    }
}
