<?php

namespace App\Http\Controllers\Api\Master;

use DateTime;
use Exception;

use Carbon\Carbon;
use App\Model\Master\Batch;
use Illuminate\Http\Request;
use App\Imports\BatchInImport;
use App\Model\Master\BatchItem;
use App\Model\Master\Inventory;
use App\Model\Master\UnitTypes;
use App\Imports\AllMasterImport;
use Yajra\DataTables\DataTables;
use App\Model\Master\BatchStatus;
use App\Model\Master\MsWarehouse;
use Illuminate\Support\Facades\DB;
use App\Model\Master\PartsDataExcel;
use App\Model\Product\ProductVendor;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Model\Master\OrdersDataExcel;
use App\Services\Master\BatchService;
use App\Http\Controllers\ApiController;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Response;


class ImportExcelController extends ApiController
{
    /**
     * show list with paginate
     */

    private function isDuplicateEntryException(QueryException $e)
    {
        $sqlState = $e->errorInfo[0];
        $errorCode  = $e->errorInfo[1];
        if ($sqlState === "23000" && $errorCode === 1062) {
            return true;
        }
         return false;
    }

    public function importExcel(Request $request) {
        // type import
        // 1. master
        // 2. batchinventory
        // 3. orders_data_excel
        try {
            request()->validate([
            'file' => 'required',
            'file.*' => 'mimes:csv,xls,xlsx'
            ]);

            $file = $request->file('file');
            $nama_file = rand().$file->getClientOriginalName();
            $file->storeAs('public/file_import_all/',$nama_file);
            $import = null;
            if($request->get('type_import') == 'master') {
                $import = new AllMasterImport();
                $import->onlySheets('country', 'province', 'city', 'district', 'village');
                return $this->successResponse(
                    $data = Excel::import($import, storage_path('app/public/file_import_all/'.$nama_file)),
                    'success',
                    200
                );
            } else if ($request->get('type_import') == 'batchinventory') {
                return $this->uploadExcel($nama_file, 'batchinventory');
            } else if ($request->get('type_import') == 'ordersdataexcel') {
                return $this->uploadExcel($nama_file, 'ordersdataexcel');
            }

        } catch (Exception $e) {
            return $e;
        }
    }

    public function saveImportExcelOrdersDataExcel(Request $request) {
        // return json_decode($request->data, true);
        DB::beginTransaction();
        try {
            //

            $orders_data_excel = [];

            foreach(json_decode($request->data, true) as $key => $row) {

                $orders_data_excel = [
                    "service_order_no" => $row['service_order_no'],
                    "asc_name" => $row['asc_name'],
                    "customer_name" => $row['customer_name'],
                    "type_job" => $row['type_job'],
                    "defect_type" => $row['defect_type'],
                    "engineer_code" => $row['engineer_code'],
                    "engineer_name" => $row['engineer_name'],
                    "assist_engineer_name" => $row['assist_engineer_name'],
                    "merk_brand" => $row['merk_brand'],
                    "model_product" => $row['model_product'],
                    "status" => $row['status'],
                    "reason" => $row['reason'],
                    "remark_reason" => $row['remark_reason'],
                    "pending_aging_days" => $row['pending_aging_days'],
                    "street" => $row['street'],
                    "city" => $row['city'],
                    "phone_no_mobile" => $row['phone_no_mobile'],
                    "phone_no_home" => $row['phone_no_home'],
                    "phone_no_office" => $row['phone_no_office'],
                    "month" => $row['month'],
                    "date" => Carbon::parse($row['date'])->format('Y-m-d'),
                    "request_time" => Carbon::parse($row['request_time'])->format('H:i:s'),
                    // "created_times" => $row['created_times'],
                    "engineer_picked_order_date" => Carbon::parse($row['engineer_picked_order_date'])->format('Y-m-d'),
                    "engineer_picked_order_time" => Carbon::parse($row['engineer_picked_order_time'])->format('H:i:s'),
                    "admin_assigned_to_engineer_date" => Carbon::parse($row['admin_assigned_to_engineer_date'])->format('Y-m-d'),
                    "admin_assigned_to_engineer_time" => Carbon::parse($row['admin_assigned_to_engineer_time'])->format('H:i:s'),
                    "engineer_assigned_date" => Carbon::parse($row['engineer_assigned_date'])->format('Y-m-d'),
                    "engineer_assigned_time" => Carbon::parse($row['engineer_assigned_time'])->format('H:i:s'),
                    "tech_1st_appointment_date" => Carbon::parse($row['tech_1st_appointment_date'])->format('Y-m-d'),
                    "tech_1st_appointment_time" => Carbon::parse($row['tech_1st_appointment_time'])->format('H:i:s'),
                    "1st_visit_date" => Carbon::parse($row['1st_visit_date'])->format('Y-m-d'),
                    "1st_visit_time" => Carbon::parse($row['1st_visit_time'])->format('H:i:s'),
                    "repair_completed_date" => Carbon::parse($row['repair_completed_date'])->format('Y-m-d'),
                    "repair_completed_time" => Carbon::parse($row['repair_completed_time'])->format('H:i:s'),
                    "closed_date" => Carbon::parse($row['closed_date'])->format('Y-m-d'),
                    "closed_time" => Carbon::parse($row['closed_time'])->format('H:i:s'),
                    "rating" => $row['rating'],
                ];

                $insert_ode = OrdersDataExcel::create($orders_data_excel);
                if($row['part_data_excel'] != null) {
                    $part_data_excel = [];
                    foreach($row['part_data_excel'] as $key=> $row) {
                        $part_data_excel[] = [
                            'code_material'        => $row['code_material'],
                            'quantity'             => $row['quantity'],
                            'part_description'    => $row['part_description'],
                            'orders_data_excel_id' => $insert_ode->id
                        ];
                    }
                    $insert_pde = PartsDataExcel::insert($part_data_excel);
                }

            }
            DB::commit();
            return $this->successResponse('',
                    'success',
                    200
                );
        }  catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function saveImportExcel(Request $request) {
        DB::beginTransaction();
        try {
            foreach($request->data as $key => $row) {
                $batch = [
                    'batch_date'    => Carbon::parse($row['batch_date'])->format('Y-m-d'),
                    'batch_no'      => BatchService::generateBatchNo(),
                    'shipping_cost' => $row['shipping_cost'],
                    'extra_cost'    => $row['extra_cost'],
                    'is_confirmed'   => 1
                ];
                $insert_batch = Batch::create($batch);
                $product_vendor = ProductVendor::where('product_code', $row['product_code'])->select('ms_product_id','id')->first();
                $batch_status = BatchStatus::where(DB::raw("LOWER(name)"), strtolower($row['status']));
                if($batch_status->count() > 0) {
                    $batch_status = $batch_status->first(['id', 'name']);
                } else {
                    $batch_status = BatchStatus::create([
                        'name' => $row['status']
                    ]);
                }
                $batch_item = [
                    'batch_id'          => $insert_batch['id'],
                    'product_id'        => $product_vendor->ms_product_id,
                    'product_vendor_id' => $product_vendor->id,
                    'value'             => $row['value'],
                    'quantity'          => $row['quantity'],
                    'batch_status_id'   => $batch_status->id,
                    'is_accepted'       => 1,
                    'is_confirmed'      => 1
                ];
                $insert_batch_item = BatchItem::create($batch_item);
                $warehouse = MsWarehouse::where(DB::raw("LOWER(name)"), strtolower($row['warehouse_name']))
                                ->where(DB::raw("LOWER(address)"), strtolower($row['warehouse_address']));
                if($warehouse->count() > 0) {
                    $warehouse = $warehouse->first(['id', 'name', 'address']);
                } else {
                    $warehouse = MsWarehouse::create([
                        'name' => $row['warehouse_name'],
                        'address' => $row['warehouse_address']
                    ]);
                }
                $unit_types = UnitTypes::where(DB::raw("LOWER(unit_name)"), strtolower($row['unit_types']));
                if($unit_types->count() > 0) {
                    $unit_types = $unit_types->first(['id', 'unit_name']);
                } else {
                    $unit_types = UnitTypes::create([
                        'name' => $row['unit_types'],
                    ]);
                }
                $inventory = [
                    'product_id'       => $product_vendor->ms_product_id,
                    'ms_batch_item_id' => $insert_batch_item['id'],
                    'cogs_value'       => $row['cogs_value'],
                    'ms_unit_type_id'  => $unit_types->id,
                    'stock'            => $row['quantity'],
                    'ms_warehouse_id'  => $warehouse->id,
                ];
                $insert_inventory = Inventory::create($inventory);

            }
            DB::commit();
            return $this->successResponse('',
                    'success',
                    200
                );
        }  catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

    }

    public function uploadExcel($nama_file, $type_import) {
        // type import
        // 1. batchinventory
        // 2. orders_data_excel
        ini_set('memory_limit', '-1');
        if (isset($nama_file)) {
            $dataExcels = null;
            if ($type_import == 'batchinventory') {
                $dataExcels = $this->checkImport($nama_file);
            } else {
                $dataExcels = $this->checkImportOrdersDataExcel($nama_file);
            }
            if (isset($dataExcels['error']) && $dataExcels['error'] == 0) {
                return json_encode(array('error' => 0, 'preview' => $dataExcels['data']));

            } else {
                if (isset($dataExcels['data']) && $dataExcels['data'] == false) {
                    return json_encode(array('error' => 1, 'msg' => 'Excel format is incorrect or excel cannot be empty', 'showSubscribeModal' => 'hide'));
                } else {
                    return json_encode(array('error' => 2, 'data' => $dataExcels['data']));
                }
            }
        } else {
            //error checking goes here
            return json_encode(array('error' => 1, 'msg' => 'no file is selected'));
        }
    }

    public function checkImportOrdersDataExcel($nama_file) {

        ini_set('memory_limit', '-1');

        $collection = (new FastExcel)->sheet(1)->import(storage_path('app/public/file_import_all/'.$nama_file));


        if (sizeof($collection) == 0) {
            return ['error' => 1, 'data' => false];
        } else {
            $errorArray = array();
            // row 1 as heading on excel;
            $row = 2;
            $orders_data_excel = [];
            foreach ($collection->toArray() as $key => $getExcel) {
                $part_data_excel = [];
                foreach(range(1,13) as $val) {
                    if($getExcel['code_material_'.$val.''] == null && $getExcel['part_description_'.$val.''] == null && $getExcel['quantity_'.$val.''] == null) {
                        break;
                    }
                    $part_data_excel[] = [
                        'code_material' => $getExcel['code_material_'.$val.''],
                        'part_description' => $getExcel['part_description_'.$val.''],
                        'quantity' => $getExcel['quantity_'.$val.'']
                    ];
                }
                $orders_data_excel[] = [
                    "service_order_no" => $getExcel['service_order_no'],
                    "asc_name" => $getExcel['asc_name'],
                    "customer_name" => $getExcel['customer_name'],
                    "type_job" => $getExcel['type_job'],
                    "defect_type" => $getExcel['defect_type'],
                    "engineer_code" => $getExcel['engineer_code'],
                    "engineer_name" => $getExcel['engineer_name'],
                    "assist_engineer_name" => $getExcel['assist_engineer_name'],
                    "merk_brand" => $getExcel['merk_brand'],
                    "model_product" => $getExcel['model_product'],
                    "status" => $getExcel['status'],
                    "reason" => $getExcel['reason'],
                    "remark_reason" => $getExcel['remark_reason'],
                    "pending_aging_days" => $getExcel['pending_aging_days'],
                    "street" => $getExcel['street'],
                    "city" => $getExcel['city'],
                    "phone_no_mobile" => $getExcel['phone_no_mobile'],
                    "phone_no_home" => $getExcel['phone_no_home'],
                    "phone_no_office" => $getExcel['phone_no_office'],
                    "month" => $getExcel['month'],
                    "date" => Carbon::parse($getExcel['date'])->format('Y-m-d'),
                    "request_time" => $getExcel['request_time'],
                    "engineer_picked_order_date" => $getExcel['engineer_picked_order_date'],
                    "engineer_picked_order_time" => $getExcel['engineer_picked_order_time'],
                    "admin_assigned_to_engineer_date" => $getExcel['admin_assigned_to_engineer_date'],
                    "admin_assigned_to_engineer_time" => $getExcel['admin_assigned_to_engineer_time'],
                    "engineer_assigned_date" => $getExcel['engineer_assigned_date'],
                    "engineer_assigned_time" => $getExcel['engineer_assigned_time'],
                    "tech_1st_appointment_date" => $getExcel['tech_1st_appointment_date'],
                    "tech_1st_appointment_time" => $getExcel['tech_1st_appointment_time'],
                    "1st_visit_date" => $getExcel['1st_visit_date'],
                    "1st_visit_time" => $getExcel['1st_visit_time'],
                    "repair_completed_date" => $getExcel['repair_completed_date'],
                    "repair_completed_time" => $getExcel['repair_completed_time'],
                    "closed_date" => $getExcel['closed_date'],
                    "closed_time" => $getExcel['closed_time'],
                    "rating" => $getExcel['rating'],
                    "part_data_excel" => count($part_data_excel) > 0 ? $part_data_excel : null
                ];

                if (trim($getExcel['service_order_no']) == null) {
                    array_push($errorArray, 'Service Order No cannot be empty on cell A' . ($key+$row));
                }

                if (trim($getExcel['asc_name']) == null) {
                    array_push($errorArray, 'Asc Name cannot be empty on cell B' . ($key+$row));
                }

                if (trim($getExcel['customer_name']) == null) {
                    array_push($errorArray, 'Customer Name cannot be empty on cell C' . ($key+$row));
                }

                if (trim($getExcel['type_job']) == null) {
                    array_push($errorArray, 'Type Job cannot be empty on cell D' . ($key+$row));
                }

                if (trim($getExcel['defect_type']) == null) {
                    array_push($errorArray, 'Defect Type cannot be empty on cell E' . ($key+$row));
                }

                // if (trim($getExcel['engineer_code']) == null) {
                //     array_push($errorArray, 'Engineer Code cannot be empty on cell F' . ($key+$row));
                // }

                if (trim($getExcel['engineer_name']) == null) {
                    array_push($errorArray, 'Engineer Name cannot be empty on cell G' . ($key+$row));
                }

                if (trim($getExcel['assist_engineer_name']) == null) {
                    array_push($errorArray, 'Assist Engineer Name cannot be empty on cell H' . ($key+$row));
                }

                // if (trim($getExcel['merk_brand']) == null) {
                //     array_push($errorArray, 'Merk Brand cannot be empty on cell I' . ($key+$row));
                // }

                // if (trim($getExcel['model_product']) == null) {
                //     array_push($errorArray, 'Model Product cannot be empty on cell J' . ($key+$row));
                // }

                if (trim($getExcel['status']) == null) {
                    array_push($errorArray, 'Statuss cannot be empty on cell K' . ($key+$row));
                }

                /*
                if (trim($getExcel['reason']) == null) {
                    array_push($errorArray, 'Reason cannot be empty on cell L' . ($key+$row));
                }

                if (trim($getExcel['remark_reason']) == null) {
                    array_push($errorArray, 'Remark Reason cannot be empty on cell M' . ($key+$row));
                }

                if (trim($getExcel['pending_aging_days']) == null) {
                    array_push($errorArray, 'Pending Aging Days cannot be empty on cell N' . ($key+$row));
                }
                */

                // if (trim($getExcel['street']) == null) {
                //     array_push($errorArray, 'Street cannot be empty on cell O' . ($key+$row));
                // }

                if (trim($getExcel['city']) == null) {
                    array_push($errorArray, 'City cannot be empty on cell P' . ($key+$row));
                }

                // if (trim($getExcel['phone_no_mobile']) == null) {
                //     array_push($errorArray, 'Phone No Mobile cannot be empty on cell Q' . ($key+$row));
                // }

                /*
                if (trim($getExcel['phone_no_home']) == null) {
                    array_push($errorArray, 'Phone No Home cannot be empty on cell R' . ($key+$row));
                }

                if (trim($getExcel['phone_no_office']) == null) {
                    array_push($errorArray, 'Phone No Office cannot be empty on cell S' . ($key+$row));
                }
                */

                if (trim($getExcel['month']) == null) {
                    array_push($errorArray, 'Month cannot be empty on cell T' . ($key+$row));
                }

                if (trim($getExcel['date']) == null) {
                    array_push($errorArray, 'Date cannot be empty on cell U' . ($key+$row));
                } else {
                    $format = 'd-m-Y';
                    $d = DateTime::createFromFormat($format, $getExcel['date']);
                    if(!($d && $d->format($format) === $getExcel['date'])) {
                        array_push($errorArray, 'Invalid Date "'.$getExcel['date'].'" on row U' . ($key+$row));
                    }
                }

                if (trim($getExcel['request_time']) == null) {
                    array_push($errorArray, 'Request Time cannot be empty on cell V' . ($key+$row));
                } else {
                    $format = 'H:i:s';
                    $d = DateTime::createFromFormat($format, $getExcel['request_time']);
                    if(!($d && $d->format($format) === $getExcel['request_time'])) {
                        array_push($errorArray, 'Invalid Request Time "'.$getExcel['request_time'].'" on row V' . ($key+$row));
                    }
                }

                if (trim($getExcel['engineer_picked_order_date']) == null) {
                    array_push($errorArray, 'Engineer Picked Order Date cannot be empty on cell W' . ($key+$row));
                } else {
                    $format = 'd-m-Y';
                    $d = DateTime::createFromFormat($format, $getExcel['engineer_picked_order_date']);
                    if(!($d && $d->format($format) === $getExcel['engineer_picked_order_date'])) {
                        array_push($errorArray, 'Invalid Engineer Picked Order Date "'.$getExcel['engineer_picked_order_date'].'" on row W' . ($key+$row));
                    }
                }

                if (trim($getExcel['engineer_picked_order_time']) == null) {
                    array_push($errorArray, 'Engineer Picked Order Time cannot be empty on cell X' . ($key+$row));
                } else {
                    $format = 'H:i:s';
                    $d = DateTime::createFromFormat($format, $getExcel['engineer_picked_order_time']);
                    if(!($d && $d->format($format) === $getExcel['engineer_picked_order_time'])) {
                        array_push($errorArray, 'Engineer Picked Order Time "'.$getExcel['engineer_picked_order_time'].'" on row X' . ($key+$row));
                    }
                }

                if (trim($getExcel['admin_assigned_to_engineer_date']) == null) {
                    array_push($errorArray, 'Admin Assigned To Engineer Date cannot be empty on cell Y' . ($key+$row));
                } else {
                    $format = 'd-m-Y';
                    $d = DateTime::createFromFormat($format, $getExcel['admin_assigned_to_engineer_date']);
                    if(!($d && $d->format($format) === $getExcel['admin_assigned_to_engineer_date'])) {
                        array_push($errorArray, 'Invalid Admin Assigned To Engineer Date "'.$getExcel['admin_assigned_to_engineer_date'].'" on row Y' . ($key+$row));
                    }
                }

                if (trim($getExcel['admin_assigned_to_engineer_time']) == null) {
                    array_push($errorArray, 'Admin Assigned To Engineer Time cannot be empty on cell Z' . ($key+$row));
                } else {
                    $format = 'H:i:s';
                    $d = DateTime::createFromFormat($format, $getExcel['admin_assigned_to_engineer_time']);
                    if(!($d && $d->format($format) === $getExcel['admin_assigned_to_engineer_time'])) {
                        array_push($errorArray, 'Admin Assigned To Engineer Time "'.$getExcel['admin_assigned_to_engineer_time'].'" on row Z' . ($key+$row));
                    }
                }

                if (trim($getExcel['engineer_assigned_date']) == null) {
                    array_push($errorArray, 'Engineer Assigned Date cannot be empty on cell AA' . ($key+$row));
                } else {
                    $format = 'd-m-Y';
                    $d = DateTime::createFromFormat($format, $getExcel['engineer_assigned_date']);
                    if(!($d && $d->format($format) === $getExcel['engineer_assigned_date'])) {
                        array_push($errorArray, 'Engineer Assigned Date "'.$getExcel['engineer_assigned_date'].'" on row AA' . ($key+$row));
                    }
                }

                if (trim($getExcel['engineer_picked_order_time']) == null) {
                    array_push($errorArray, 'Engineer Picked Order Time cannot be empty on cell AB' . ($key+$row));
                } else {
                    $format = 'H:i:s';
                    $d = DateTime::createFromFormat($format, $getExcel['engineer_picked_order_time']);
                    if(!($d && $d->format($format) === $getExcel['engineer_picked_order_time'])) {
                        array_push($errorArray, 'Engineer Picked Order Time "'.$getExcel['engineer_picked_order_time'].'" on row AB' . ($key+$row));
                    }
                }

                if (trim($getExcel['tech_1st_appointment_date']) == null) {
                    array_push($errorArray, 'Tech 1st Appointment Date cannot be empty on cell AC' . ($key+$row));
                } else {
                    $format = 'd-m-Y';
                    $d = DateTime::createFromFormat($format, $getExcel['tech_1st_appointment_date']);
                    if(!($d && $d->format($format) === $getExcel['tech_1st_appointment_date'])) {
                        array_push($errorArray, 'Tech 1st Appointment Date "'.$getExcel['tech_1st_appointment_date'].'" on row AC' . ($key+$row));
                    }
                }

                if (trim($getExcel['tech_1st_appointment_time']) == null) {
                    array_push($errorArray, 'Tech 1st Appointment Time cannot be empty on cell AD' . ($key+$row));
                } else {
                    $format = 'H:i:s';
                    $d = DateTime::createFromFormat($format, $getExcel['tech_1st_appointment_time']);
                    if(!($d && $d->format($format) === $getExcel['tech_1st_appointment_time'])) {
                        array_push($errorArray, 'Tech 1st Appointment Time "'.$getExcel['tech_1st_appointment_time'].'" on row AD' . ($key+$row));
                    }
                }

                if (trim($getExcel['1st_visit_date']) == null) {
                    array_push($errorArray, '1st Visit Date cannot be empty on cell AE' . ($key+$row));
                } else {
                    $format = 'd-m-Y';
                    $d = DateTime::createFromFormat($format, $getExcel['1st_visit_date']);
                    if(!($d && $d->format($format) === $getExcel['1st_visit_date'])) {
                        array_push($errorArray, '1st Visit Date "'.$getExcel['1st_visit_date'].'" on row AE' . ($key+$row));
                    }
                }

                if (trim($getExcel['1st_visit_time']) == null) {
                    array_push($errorArray, '1st Visit Time cannot be empty on cell AF' . ($key+$row));
                } else {
                    $format = 'H:i:s';
                    $d = DateTime::createFromFormat($format, $getExcel['1st_visit_time']);
                    if(!($d && $d->format($format) === $getExcel['1st_visit_time'])) {
                        array_push($errorArray, '1st Visit Time "'.$getExcel['1st_visit_time'].'" on row AF' . ($key+$row));
                    }
                }

                if (trim($getExcel['repair_completed_date']) == null) {
                    array_push($errorArray, 'Repair Completed Date be empty on cell AG' . ($key+$row));
                } else {
                    $format = 'd-m-Y';
                    $d = DateTime::createFromFormat($format, $getExcel['repair_completed_date']);
                    if(!($d && $d->format($format) === $getExcel['repair_completed_date'])) {
                        array_push($errorArray, 'Repair Completed Date "'.$getExcel['repair_completed_date'].'" on row AG' . ($key+$row));
                    }
                }

                if (trim($getExcel['repair_completed_time']) == null) {
                    array_push($errorArray, 'Repair Completed Time cannot be empty on cell AH' . ($key+$row));
                } else {
                    $format = 'H:i:s';
                    $d = DateTime::createFromFormat($format, $getExcel['repair_completed_time']);
                    if(!($d && $d->format($format) === $getExcel['repair_completed_time'])) {
                        array_push($errorArray, 'Repair Completed Time "'.$getExcel['repair_completed_time'].'" on row AH' . ($key+$row));
                    }
                }

                if (trim($getExcel['closed_date']) == null) {
                    array_push($errorArray, 'Closed Date cannot be empty on cell AI' . ($key+$row));
                } else {
                    $format = 'd-m-Y';
                    $d = DateTime::createFromFormat($format, $getExcel['closed_date']);
                    if(!($d && $d->format($format) === $getExcel['closed_date'])) {
                        array_push($errorArray, 'Closed Date "'.$getExcel['closed_date'].'" on row AI' . ($key+$row));
                    }
                }

                if (trim($getExcel['closed_time']) == null) {
                    array_push($errorArray, 'Closed Time cannot be empty on cell AJ' . ($key+$row));
                } else {
                    $format = 'H:i:s';
                    $d = DateTime::createFromFormat($format, $getExcel['closed_time']);
                    if(!($d && $d->format($format) === $getExcel['closed_time'])) {
                        array_push($errorArray, 'Closed Time "'.$getExcel['closed_time'].'" on row AJ' . ($key+$row));
                    }
                }

                /*
                if (trim($getExcel['rating']) == null) {
                    array_push($errorArray, 'Rating cannot be empty on cell AK' . ($key+$row));
                }
                */


            }
            if (sizeof($errorArray) > 0) {
                unlink(storage_path('app/public/file_import_all/'.$nama_file));
                return ["error" => 1, 'data' => $errorArray];
            } else {
                return ["error" => 0, 'data' => $orders_data_excel];
            }
        }


    }

    public function checkImport($nama_file)
    {
        ini_set('memory_limit', '-1');

        // $collection = Excel::selectSheetsByIndex(1)->load($file, function ($reader) {
        //     $reader->formatDates(false);
        // })->get();

        $collection = (new FastExcel)->sheet(1)->import(storage_path('app/public/file_import_all/'.$nama_file));

        if (sizeof($collection) == 0) {
            return ['error' => 1, 'data' => false];
        } else {
            $errorArray = array();
            $row = 2;

            foreach ($collection->toArray() as $key => $getExcel) {
                if (trim($getExcel['batch_date']) == null) {
                    array_push($errorArray, 'Batch date cannot be empty on cell A' . ($key+$row));
                } else {
                    $format = 'd-m-Y';
                    $d = DateTime::createFromFormat($format, $getExcel['batch_date']);
                    if(!($d && $d->format($format) === $getExcel['batch_date'])) {
                        array_push($errorArray, 'Invalid Batch date "'.$getExcel['batch_date'].'" on row A' . ($key+$row));
                    }
                }

                if (trim($getExcel['shipping_cost']) == null) {
                    array_push($errorArray, 'Shipping Cost cannot be empty on cell B' . ($key+$row));
                }

                if (trim($getExcel['extra_cost']) == null) {
                    array_push($errorArray, 'Extra cost cannot be empty on cell C' . ($key+$row));
                }

                if (trim($getExcel['product_code']) == null) {
                    array_push($errorArray, 'Product code cannot be empty on cell D' . ($key+$row));
                } else {
                    $find = ProductVendor::where('product_code', $getExcel['product_code'])->first(['ms_product_id']);
                    if (!$find) {
                        array_push($errorArray, 'Product code "'.$getExcel['product_code'].'" is not found on cell D' . ($key+$row));
                    }
                }

                if (trim($getExcel['value']) == null) {
                    array_push($errorArray, 'Value cannot be empty on cell D' . ($key+$row));
                }

                if (trim($getExcel['quantity']) == null) {
                    array_push($errorArray, 'Quantity cannot be empty on cell F' . ($key+$row));
                }

                if (trim($getExcel['status']) == null) {
                    array_push($errorArray, 'Status cannot be empty on cell G' . ($key+$row));
                }
                // else {
                //     $status = array('Baru', 'Bekas');
                //     if ($getExcel['status'] != null) {
                //         if (!in_array($getExcel['status'], $status)) {
                //             array_push($errorArray, 'Invalid name status on cell G' . ($key+$row));
                //         }
                //     }
                // }

                if (trim($getExcel['cogs_value']) == null) {
                    array_push($errorArray, 'Cogs Value cannot be empty on cell H' . ($key+$row));
                }

                if (trim($getExcel['unit_types']) == null) {
                    array_push($errorArray, 'Unit Types cannot be empty on cell I' . ($key+$row));
                }
                // else {
                //     $unit_types = array('Kg', 'M', 'Cm', 'Pcs', 'Carton', 'Pack');
                //     if ($getExcel['unit_types'] != null) {
                //         if (!in_array($getExcel['unit_types'], $unit_types)) {
                //             array_push($errorArray, 'Invalid name Unit Types on cell I' . ($key+$row));
                //         }
                //     }
                // }

                if (trim($getExcel['warehouse_name']) == null) {
                    array_push($errorArray, 'Warehouse Name cannot be empty on cell J' . ($key+$row));
                }

                if (trim($getExcel['warehouse_address']) == null) {
                    array_push($errorArray, 'Warehouse Address cannot be empty on cell K' . ($key+$row));
                }

            }
            if (sizeof($errorArray) > 0) {
                unlink(storage_path('app/public/file_import_all/'.$nama_file));
                return ["error" => 1, 'data' => $errorArray];
            } else {
                return ["error" => 0, 'data' => $collection];
            }
        }
    }
}
