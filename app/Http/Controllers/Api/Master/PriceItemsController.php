<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Services\Master\PriceItemsService as Service;
use App\Http\Requests\Master\PriceItemsRequest;
use App\Model\Master\MsPriceItems;

class PriceItemsController extends ApiController
{
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);
        return $this->successResponse($response);
    }

    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    public function store(PriceItemsRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    public function update(PriceItemsRequest $request, MsPriceItems $priceItem, Service $service)
    {
        return $this->successResponse(
            $service->setPriceItems($priceItem)->update($request->all()),
            'success'
        );
    }

    public function delete(MsPriceItems $priceItem, Service $service)
    {
        return $this->successResponse(
            $service->setPriceItems($priceItem)->delete(),
            'success',
            201
        );
    }
}
