<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Model\Master\DevelopmentProgram;
use App\Services\Master\DevelopmentProgramService as Service;
use App\Http\Requests\Master\DevelopmentProgramRequest;
use Yajra\DataTables\DataTables;


class DevelopmentProgramController extends ApiController
{
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);
        return $this->successResponse($response);
    }

    public function select2(Request $request)
    {
        $response = DevelopmentProgram::where('program_name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function datatables(Service $service)
    {
        $query = $service->list();

        return DataTables::of($query)
            ->toJson();
    }

    public function store(Service $service, DevelopmentProgramRequest $request)
    {
        $request->validated();
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    public function update(Request $request, Service $service, DevelopmentProgram $dProgram)
    {
        return $this->successResponse(
            $service->setDevelopmentProgram($dProgram)->update($request->all()),
            'Success'
        );
    }

    public function destroy(Service $service, DevelopmentProgram $dProgram)
    {
       return $this->successResponse(
           $service->setDevelopmentProgram($dProgram)->delete(),
           'success',
            204
       );
    }


}
