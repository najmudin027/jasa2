<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\GeneralSettingRequest;
use App\Model\Master\GeneralSetting;
use App\Model\Master\BankTransfer;
use App\Services\Master\GeneralSettingService as Service;
use Illuminate\Http\Request;
use DB;

class GeneralSettingController extends ApiController
{

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {
        $value = $request->value;
        $type = $request->type;
        $ids = $request->id;

        foreach ($value as $key => $values) {
            // return $type;
            $id = $ids[$key];
            GeneralSetting::where('id', $id)->update([
                'type' => $type[$key],
                'value' => $value[$key],
            ]);
        }

        return $this->successResponse([], $this->successUpdateMsg());
    }

    public function storeServeyKeyMidtrans(Request $request, Service $service)
    {
        $value = $request->value;
        $ids = $request->id;
        $data = [];
        foreach ($value as $key => $values) {
            $id = $ids[$key];
            $input['value'] = $values;
            GeneralSetting::where('id', $id)->update($input);
        }
    }

    public function switchBtn($id, Request $request)
    {
        // dd($request->id);
        $isActive =  BankTransfer::where('id', $request->id)->first();
        // return $isActive->is_active == 1 ? 0 : 1;
        return $isActive->update([
            'is_active' => $isActive->is_active == 1 ? 0 : 1,
        ]);
    }

    public function aggrementSave(Request $request)
    {
        $getAgreement = GeneralSetting::whereIn('name', ['agreement_text', 'agreement_title'])->first();

        $value = $request->value;
        $name = $request->name;
        $ids = $request->id;


        if ($getAgreement) {
            foreach ($value as $key => $values) {
                $id = $ids[$key];
                GeneralSetting::where('id', $id)->update([
                    'value' => $value[$key],
                ]);
            }
        } else {
            foreach ($value as $key => $values) {
                $id = $ids[$key];
                GeneralSetting::create([
                    'name' => $name[$key],
                    'value' => $value[$key],
                ]);
            }
        }
    }

    public function aggrementOrderSave(Request $request)
    {
        $getAgreementOrder = GeneralSetting::whereIn('name', ['agreement_order_text', 'agreement_order_title'])->get();

        $value = $request->value;
        $name = $request->name;
        $ids = $request->id;


        if ($getAgreementOrder) {
            foreach ($value as $key => $values) {
                $id = $ids[$key];
                GeneralSetting::where('id', $id)->update([
                    'value' => $value[$key],
                ]);
            }
        } else {
            foreach ($value as $key => $values) {
                $id = $ids[$key];
                GeneralSetting::create([
                    'name' => $name[$key],
                    'value' => $value[$key],
                ]);
            }
        }
    }

    public function aggrementOrderCompletedSave(Request $request)
    {
        $getAgreementOrder = GeneralSetting::whereIn('name', ['term_order_completed_title', 'term_order_completed_desc'])->get();

        $value = $request->value;
        $name = $request->name;
        $ids = $request->id;


        if ($getAgreementOrder) {
            foreach ($value as $key => $values) {
                $id = $ids[$key];
                GeneralSetting::where('id', $id)->update([
                    'value' => $value[$key],
                ]);
            }
        } else {
            foreach ($value as $key => $values) {
                $id = $ids[$key];
                GeneralSetting::create([
                    'name' => $name[$key],
                    'value' => $value[$key],
                ]);
            }
        }
    }

    public function socialMediaSave(Request $request)
    {
        $getVal = $request->value;
        $ids = $request->id;

        foreach ($getVal as $key => $values) {
            $id = $ids[$key];
            GeneralSetting::where('id', $id)->update([
                'value' => $getVal[$key],
            ]);
        }

        return $this->successResponse([], $this->successUpdateMsg());
    }

    public function sociaLoginSave(Request $request)
    {
        $getSocialLogin = GeneralSetting::whereIn('name', [
            'client_key_facebook',
            'secret_key_facebook',
            'callback_url_facebook',
            'client_key_google',
            'secret_key_google',
            'callback_url_google',
        ])->get();

        $value = $request->value;
        $name = $request->name;
        $ids = $request->id;


        if ($getSocialLogin) {
            foreach ($value as $key => $values) {
                $id = $ids[$key];
                GeneralSetting::where('id', $id)->update([
                    'value' => $value[$key],
                ]);
            }
        } else {
            foreach ($value as $key => $values) {
                $id = $ids[$key];
                GeneralSetting::create([
                    'name' => $name[$key],
                    'value' => $value[$key],
                ]);
            }
        }

        return $this->successResponse([], $this->successUpdateMsg());
    }

    public function googleMapsKeySetting($id, Request $request)
    {


        if (is_array($request->value)) {
            $ids = $id;
            foreach ($request->value as $key => $value) {
                $id = $ids[$key];
                GeneralSetting::where('id', $id)->update([
                    'value' => $request->value[$key],
                ]);
            }
        } else {
            GeneralSetting::where('id', $id)->update([
                'value' => $request->value,
            ]);
        }
    }

    public function privacyPoliceStore(Request $request)
    {
        $getAgreementOrder = GeneralSetting::whereIn('name', ['privacy_police_title', 'privacy_police_desc'])->get();

        $value = $request->value;
        $name = $request->name;
        $ids = $request->id;


        if ($getAgreementOrder) {
            foreach ($value as $key => $values) {
                $id = $ids[$key];
                GeneralSetting::where('id', $id)->update([
                    'value' => $value[$key],
                ]);
            }
        } else {
            foreach ($value as $key => $values) {
                $id = $ids[$key];
                GeneralSetting::create([
                    'name' => $name[$key],
                    'value' => $value[$key],
                ]);
            }
        }
    }

    public function firebaseSettingUpdate(Request $request)
    {
        $value = $request->value;
        $ids = $request->id;

        foreach ($ids as $key => $values) {
            $id = $ids[$key];
            GeneralSetting::where('id', $id)->update([
                'value' => $value[$key],
            ]);
        }

        return $this->successResponse([], $this->successUpdateMsg());
    }

    public function socialLoginStore(Request $request)
    {
        $value = $request->value;
        $ids = $request->id;

        foreach ($ids as $key => $values) {
            $id = $ids[$key];
            GeneralSetting::where('id', $id)->update([
                'value' => $value[$key],
            ]);
        }

        return $this->successResponse([], $this->successUpdateMsg());
    }

    public function globalPublicSetting()
    {
        $resp = GeneralSetting::whereIn('name', [
            'server_key_midtrans',
            'garansi_due_date',
            'facebook',
            'youtube',
            'twiter',
            'instagram',
            'email',
            'addreses',
            'contact_us',
            'google_maps_key_setting',
            'term_order_completed_title',
            'term_order_completed_desc',
            'social_media_login_fb',
            'social_media_login_google',
        ])->orderBy('name', 'asc')->get();

        return $this->successResponse($resp, $this->successUpdateMsg());
    }
}
