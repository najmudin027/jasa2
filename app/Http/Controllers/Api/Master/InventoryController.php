<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Model\Master\Inventory;
use App\Http\Controllers\ApiController;
use Yajra\DataTables\Facades\DataTables;
use App\Model\Master\InventoryMutationLog;
use App\Model\Master\MutationJoinHistories;
use App\Model\Master\InventoryMutationHistories;
use App\Http\Requests\Master\InventoryRequest;
use App\Services\Master\InventoryService as Service;

class InventoryController extends ApiController
{
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);
        return $this->successResponse($response);
    }

    public function dataTable(Service $service)
    {
        return $service->datatables();
    }

    public function searchDatatables(Service $service, Request $request)
    {
        return $service->searchDatatables($request);
    }

    public function select2(Service $service, Request $request)
    {
        return $this->successResponse($service->select2($request));
    }

    public function approvalModalTemplate(Service $service, $id)
    {
        return $this->successResponse($service->approvalModalTemplate($id));
    }

    public function store(Service $service, InventoryRequest $inventoryRequest)
    {
        return $this->successResponse(
            $service->createMany($inventoryRequest->all()),
            'success',
            200
        );
    }

    public function update(Service $service, Inventory $inventory, InventoryRequest $inventoryRequest)
    {
        return $this->successResponse(
            $service->setInventory($inventory)->update($inventoryRequest->all()),
            'success'
        );
    }

    public function approve(Service $service, $id)
    {
        return $this->successResponse(
            $service->approve($id),
            'success'
        );
    }

    public function destroy(Request $request, Inventory $inventory, Service $service)
    {
        $this->deleteInventoryMutationLogs($inventory->id);
        return $this->successResponse(
            $service->setInventory($inventory)->delete(),
            'success',
            204
        );
    }

    public function deleteInventoryMutationLogs($id) {
        $find = InventoryMutationLog::where('inventories_id_out', $id);
        if($find->count() > 0) {
            $find = $find->delete();
        } else {
            $find = InventoryMutationLog::where('inventories_id_in', $id)->delete();
        }

    }

    public function mutation(Service $service, Request $request)
    {
        return $this->successResponse(
            $service->createMutation($request->all()),
            'success'
        );
    }

    // public function mutationLogsDatatables() {
    //     $query = InventoryMutationLog::with(['batch_item.batch','inventory_in.warehouse', 'inventory_out.warehouse']);
    //     return DataTables::of($query)
    //         ->addIndexColumn()
    //         ->toJson();
    // }

    public function inventoryMutationHistoriesDatatables($inventory_id = null) {
        $query = InventoryMutationHistories::with(['inventory.warehouse','order','inventory.batch_item.batch','mutation_join.history_in','mutation_join.history_out']);
        if($inventory_id != null) {
            $query->where('inventory_id', $inventory_id);
        }
        $query->orderBy('id', 'desc');
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }


}
