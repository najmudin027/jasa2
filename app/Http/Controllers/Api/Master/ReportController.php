<?php

namespace App\Http\Controllers\Api\Master;

use App\User;
use App\Model\Master\Order;
use Illuminate\Http\Request;
use App\Model\Technician\Technician;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\DB;

class ReportController extends AdminController
{

    /**
     * show list with datatables structur
     */
    public function datatables_order_service_search(Request $request)
    {
        $query = Order::with([
            'user',
            'garansi',
            'order_status',
            'service_detail.symptom',
            'service_detail.services_type',
        ]);

        if (!empty($request->technician_id)) {
            $query = $query->whereHas('service_detail', function ($q) use ($request) {
                $q->whereIn('technicians_id', $this->comaToArray($request->technician_id));
            });
        }

        if (!empty($request->customer_id)) {
            $query = $query->whereIn('users_id', $this->comaToArray($request->customer_id));
        }

        if (!empty($request->order_statuses_id)) {
            $query->whereIn('orders_statuses_id', $this->comaToArray($request->order_statuses_id));
        }

        if (!empty($request->payment_type)) {
            $query = $query->whereIn('payment_type', $this->comaToArray($request->payment_type));
        }

        if (!empty($request->order_date_range)) {
            $range = explode('-', $request->order_date_range);
            $date_from = date('Y-m-d', strtotime($range[0]));
            $date_to = date("Y-m-d", strtotime($range[1]));
            $query->where('created_at', '>=', $date_from)
                ->where('created_at', '<=', $date_to);
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatables_customer()
    {
        $query = User::with([
            'info',
            'last_order',
            'address',
        ])
            ->withCount([
                'orders',
                'orders AS money_spent' => function ($query) {
                    $query->select(DB::raw("SUM(grand_total) as grand_total_sum"));
                }
            ]);
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatables_customer_search(Request $request)
    {
        if (!empty($request->order_date_range)) {
            Session::put('date_range', $request->order_date_range);
        }

        $query = User::with([
            'info',
            'last_order',
            'address',
        ])
            ->withCount([
                'orders',
                'orders AS money_spent' => function ($query) {
                    $query->select(DB::raw("SUM(grand_total) as grand_total_sum"));
                }
            ]);

        if (!empty($request->customer_id)) {
            $query = $query->where('id', $request->customer_id);
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatables_technician(Request $request)
    {
        $date_from = null;
        $date_to = null;
        if (!empty($request->order_date_range)) {
            $range = explode(' - ', $request->order_date_range);
            $date_from = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $range[0])));
            $date_to = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $range[1])));
        }


        $query = Technician::with([
            'user.address',
        ])
            ->withCount([
                'price_services',
                'service_details AS pending_order_count' => function ($query) use ($date_from, $date_to) {
                    $query->whereHas('order', function ($queryOrder) use ($date_from, $date_to) {
                        $queryOrder->where('transfer_status', 0);

                        if ($date_from != null && $date_to != null) {
                            $queryOrder->where('schedule', '>=', $date_from)
                                ->where('schedule', '<=', $date_to);
                        }
                    });
                },
                'service_details AS done_order_count' => function ($query) use ($date_from, $date_to) {
                    $query->whereHas('order', function ($queryOrder) use ($date_from, $date_to) {
                        $queryOrder->where('transfer_status', 1);

                        if ($date_from != null && $date_to != null) {
                            $queryOrder->where('schedule', '>=', $date_from)
                                ->where('schedule', '<=', $date_to);
                        }
                    });
                },
                'service_detail AS service_detail_count' => function ($query) use ($date_from, $date_to) {
                    $query->whereHas('order', function ($queryOrder) use ($date_from, $date_to) {
                        if ($date_from != null && $date_to != null) {
                            $queryOrder->where('schedule', '>=', $date_from)
                                ->where('schedule', '<=', $date_to);
                        }
                    });
                },
                'service_detail AS total_orders_gross_revenue_sum' => function ($query) use ($date_from, $date_to) {
                    $query->join('orders', 'orders.id', '=', 'service_details.orders_id')
                        ->select(DB::raw("SUM(orders.grand_total) as total_orders_gross_revenue_sum"))
                        ->where('orders.transfer_status', 1);

                    if ($date_from != null && $date_to != null) {
                        $query->where('orders.schedule', '>=', $date_from)
                            ->where('orders.schedule', '<=', $date_to);
                    }
                },
                'service_detail AS total_orders_cut_commission_sum' => function ($query) use ($date_from, $date_to) {
                    $query->join('orders', 'orders.id', '=', 'service_details.orders_id')
                        ->select(DB::raw("SUM(orders.grand_total - orders.after_commission) as total_orders_net_revenue_sum"))
                        ->where('orders.transfer_status', 1);

                    if ($date_from != null && $date_to != null) {
                        $query->where('orders.schedule', '>=', $date_from)
                            ->where('orders.schedule', '<=', $date_to);
                    }
                },
                'service_detail AS total_orders_net_revenue_sum' => function ($query) use ($date_from, $date_to) {
                    $query->join('orders', 'orders.id', '=', 'service_details.orders_id')
                        ->select(DB::raw("SUM(orders.after_commission) as total_orders_cut_commission_sum"))
                        ->where('orders.transfer_status', 1);

                    if ($date_from != null && $date_to != null) {
                        $query->where('orders.schedule', '>=', $date_from)
                            ->where('orders.schedule', '<=', $date_to);
                    }
                },
                'service_detail AS total_orders_gross_revenue_pending_sum' => function ($query) use ($date_from, $date_to) {
                    $query->join('orders', 'orders.id', '=', 'service_details.orders_id')
                        ->select(DB::raw("SUM(orders.grand_total) as total_orders_gross_revenue_pending_sum"))
                        ->where('orders.transfer_status', 0);

                    if ($date_from != null && $date_to != null) {
                        $query->where('orders.schedule', '>=', $date_from)
                            ->where('orders.schedule', '<=', $date_to);
                    }
                },
                'sparepart_details AS total_qty_spartpart'  => function ($query) use ($date_from, $date_to) {
                    $query->select(DB::raw("SUM(quantity) as total_qty_spartpart"))
                        ->whereHas('order', function ($queryOrder) use ($date_from, $date_to) {
                            if ($date_from != null && $date_to != null) {
                                $queryOrder->where('schedule', '>=', $date_from)
                                    ->where('schedule', '<=', $date_to);
                            }
                        });
                },
                'sparepart_details AS total_price_spartpart'  => function ($query) use ($date_from, $date_to) {
                    $query->select(DB::raw("SUM(quantity * price) as total_price_spartpart"))
                        ->whereHas('order', function ($queryOrder) use ($date_from, $date_to) {
                            if ($date_from != null && $date_to != null) {
                                $queryOrder->where('schedule', '>=', $date_from)
                                    ->where('schedule', '<=', $date_to);
                            }
                        });
                }
            ]);

        if (!empty($request->technician_id)) {
            $query = $query->where('id', $request->technician_id);
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatables_technician_search(Request $request)
    {
        if (!empty($request->order_date_range)) {
            Session::put('date_range', $request->order_date_range);
        }
        $query = Technician::with('user.address');
        if (!empty($request->technician_id)) {
            $query = $query->where('id', $request->technician_id);
        }
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }


}
