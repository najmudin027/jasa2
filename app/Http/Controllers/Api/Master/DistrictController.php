<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\DistrictRequest;
use App\Model\Master\MsDistrict;
use App\Services\Master\DistrictService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Traits\MasterImportTrait;

class DistrictController extends ApiController
{
    /**
     * show list with paginate
     */
    use MasterImportTrait;

    public function index(DistrictService $districtService)
    {
        $response = $districtService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = MsDistrict::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(DistrictService $districtService)
    {
        $query = $districtService->list()->with('city.province.country');

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(DistrictRequest $request, DistrictService $districtService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $districtService->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(MsDistrict $district)
    {
        $response = $district;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(DistrictRequest $request, MsDistrict $district, DistrictService $districtService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $districtService->setDistrict($district)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(MsDistrict $district, DistrictService $districtService)
    {
        return $this->successResponse(
            $districtService->setDistrict($district)->delete(),
            'success',
            204
        );
    }

    public function importExcel(Request $request)
	{
        return $this->importExcelTraits('district', $request); // country is type of controller name
    }

    public function destroyBatch(Request $request, DistrictService $districtService)
    {
        return $this->successResponse(
            $districtService->deleteById($request->id),
            'success',
            204
        );
    }

}
