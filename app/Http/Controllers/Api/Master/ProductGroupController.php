<?php

namespace App\Http\Controllers\Api\Master;

use App\Helpers\ImageUpload;
use Illuminate\Http\Request;
use App\Model\Master\ProductGroup;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\ProductGroupRequest;
use App\Services\Master\ProductGroupService as Service;

class ProductGroupController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $ms_services_type_id = $request->ms_services_type_id;
        $paginate = $request->paginate;

        $response = $service->list()
            ->when($q, function ($query, $q) {
                $query->where('name', 'like', '%' . $q . '%');
            })
            ->when($ms_services_type_id, function ($query, $ms_services_type_id) {
                $query->where('ms_services_type_id', $ms_services_type_id);
            });
        
        if ($paginate == 'no_paginate') {
            $response = $response->get();
        }else{
            $response = $response->paginate(10);
        }

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service, $type_id = null)
    {
        return $this->successResponse($service->select2($request, $type_id));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {
        $user = auth()->user();
        $path = ProductGroup::getImagePathUpload();
        // dd($path);
        $filename = null;
        if ($request->hasFile('file')) {
            $image = (new ImageUpload)->upload($request->file('file'), $path);
            $filename = $image->getFilename();
        }
        $datas = [
            'ms_services_type_id' => $request->ms_services_type_id,
            'name'           => $request->name,
            'images'         => $filename
        ];
        return $this->successResponse(
            $service->create($datas),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(ProductGroupRequest $category)
    {
        $response = $category;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ProductGroupRequest $request, ProductGroup $product, Service $service)
    {
        $user = auth()->user();
        $path = ProductGroup::getImagePathUpload();
        // dd($path);
        $filename = null;
        if ($request->hasFile('file')) {
            $image = (new ImageUpload)->upload($request->file('file'), $path);
            $filename = $image->getFilename();
        }
        $datas = [
            'ms_services_type_id' => $request->ms_services_type_id,
            'name'           => $request->name,
            'images'         => $filename
        ];
        if ($product->images != null) {
            $file_path = storage_path('app/public/product-group/' . $product->images);
            if (file_exists($file_path)) {
                unlink($file_path);
            };
        }

        return $this->successResponse(
            $service->setProduct($product)->update($datas),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(ProductGroup $product, Service $service)
    {
        if ($product->images != null) {
            $file_path = storage_path('app/public/product-group/' . $product->images);
            if (file_exists($file_path)) {
                unlink($file_path);
            };
        }
        return $this->successResponse(
            $service->setProduct($product)->delete(),
            'success',
            204
        );
    }
}
