<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Master\PartsDataExcel;
use App\Http\Controllers\ApiController;
use App\Model\Master\PartDataExcelStock;
use App\Model\Master\PartDataExcelStockInventory;
use Yajra\DataTables\Facades\DataTables;
use App\Model\Master\GeneralJournalDetail;
use Illuminate\Database\Eloquent\Collection;
use App\Services\Master\PartDataExcelStockService as Service;

class PartDataExcelStockController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $response = $service->list()
            ->when($q, function ($query, $q) {
                $query->where('name', 'like', '%' . $q . '%');
            })
            ->orderBy('name', 'asc')
            ->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2($search_by, $asc_id=null, $filter_stock=null, Request $request, Service $service)
    {
        return $this->successResponse($service->select2($search_by, $asc_id, $filter_stock, $request));
    }

    public function select2B2B()
    {
        return $this->successResponse(PartDataExcelStockInventory::With('part_data_stock.asc')->limit('20')->get(), 'data success');
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function datatablesSearch(Service $service, Request $request)
    {
        return $service->datatables($request);
    }

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {
        $check_exist = PartDataExcelStock::where('code_material', $request->code_material)->where('asc_id', $request->asc_id)->count();
        if($check_exist > 0) {
            return $this->errorResponse(
                $check_exist,
                'Code Material dan ASC Name sudah pernah dibuat !',
                500
            );
        } else {
            return $this->successResponse(
                $service->create($request->all()),
                $this->successStoreMsg(),
                201
            );
        }
    }

    /**
     * show one data
     */
    public function show(PartDataExcelStock $pdes)
    {
        $response = $pdes;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, PartDataExcelStock $pdes, Service $service)
    {
        $datas = [
            'asc_id' => $request->asc_id,
            'asc_name' => $request->asc_name,
            'code_material' => $request->code_material,
            'part_description' => $request->part_description,
            // 'stock' => $pdes->stock
        ];
        return $this->successResponse(
            $service->setPartDataExcelStock($pdes)->update($datas),
            $this->successUpdateMsg()
        );
    }


    /**
     * delete data
     */
    public function destroy(PartDataExcelStock $pdes, Service $service)
    {
        $find_data_excel = PartsDataExcel::where('part_data_stock_id', $pdes->id)->count();
        $find_data_excel2 = GeneralJournalDetail::where('part_data_stock_id', $pdes->id)->count();
        if(($find_data_excel > 0) || ($find_data_excel2 > 0)) {
            return $this->errorResponse(
                $find_data_excel,
                'Tidak bisa di delete, karena terdapat part stock Out yang digunakan!',
                400
            );
        }
        return $this->successResponse(
            $service->setPartDataExcelStock($pdes)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }

    public function leftoverStockDatatables(Request $request) {
        $data = GeneralJournalDetail::select([
                DB::raw('(MIN(YEAR(general_journal_details.date))) AS min_year'),
                DB::raw('(MAX(YEAR(general_journal_details.date))) AS max_year')
            ])->first();
        $min_month = 1;
        $max_month = 12;
        $min_year = $data->min_year;
        $max_year = $data->max_year;
        if(!empty($request->month)) {
            $min_month = $request->month;
            $max_month = $request->month;
        }
        if(!empty($request->year)) {
            $min_year = $request->year;
            $max_year = $request->year;
        }
        $collection = new Collection();
        $getIdPart = PartDataExcelStock::pluck('id');
        foreach($getIdPart as $pdes_id) { //part data excel id
            foreach (range($min_year, $max_year) as $year) {
                foreach (range($min_month,$max_month) as $month) {
                    $data = GeneralJournalDetail::select([
                        DB::raw('(CEIL(SUM(general_journal_details.kredit) / SUM(general_journal_details.quantity))) AS hpp_average_month'),
                        DB::raw('SUM(general_journal_details.quantity) AS stock_in'),
                        DB::raw('SUM(part_data_excel.quantity) AS stock_out'),
                        'general_journal_details.part_data_stock_id',
                        ])
                        ->join('part_data_excel', 'part_data_excel.part_data_stock_id', '=', 'general_journal_details.part_data_stock_id')
                        ->where('general_journal_details.part_data_stock_id', $pdes_id)
                        ->where('category', 'material')
                        ->whereMonth('general_journal_details.date', $month)
                        ->whereYear('general_journal_details.date', $year)
                        ->groupBy('general_journal_details.part_data_stock_id')
                        ->whereNotNull('general_journal_details.part_data_stock_id')
                        ->first();

                    if($data){
                        $collection->push([
                            'part_data_stock_id' => $data->part_data_stock->id,
                            'code_material' => $data->part_data_stock->code_material,
                            'asc_name' => $data->part_data_stock->asc_name,
                            'part_description' => $data->part_data_stock->part_description,
                            'stock_in' => $data->stock_in,
                            'stock_out' => $data->stock_out,
                            'leftover_stock' => ($data->stock_in - $data->stock_out),
                            'hpp_average_month' => $data->hpp_average_month,
                            'month' => date("F", mktime(0, 0, 0, $month, 1)),
                            'month_number' => $month,
                            'year' => $year
                        ]);
                    }
                }
            }
        }


        // Datatables::of($results)->make(true)
        return DataTables::of($collection)
            ->addIndexColumn()
            ->toJson();
    }

}
