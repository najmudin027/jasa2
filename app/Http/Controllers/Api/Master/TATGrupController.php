<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\TatGroupRequest;
use App\Model\Master\TATGroup;
use App\Services\Master\TATGroupService as Service;
use Illuminate\Http\Request;

class TATGrupController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(TatGroupRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(TATGroup $grup)
    {
        $response = $grup;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(TatGroupRequest $request, TATGroup $grup, Service $service)
    {
        return $this->successResponse(
            $service->setGrup($grup)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(TATGroup $grup, Service $service)
    {
        return $this->successResponse(
            $service->setGrup($grup)->delete(),
            'success',
            204
        );
    }
}
