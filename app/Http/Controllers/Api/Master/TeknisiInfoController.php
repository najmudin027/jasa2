<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\TeknisiInfoRequest;
use App\Model\Master\TeknisiInfo;
use App\Model\Master\Religion;
use App\Model\Master\MsAdditionalInfo;
use App\Model\Master\AddressType;
use App\Services\Master\TeknisiInfoService as Service;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;
use App\Helpers\ImageUpload;
use DB;
use App\Model\Master\MsAddress;
use App\Model\Technician\Technician;

class TeknisiInfoController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);
        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        $response = TeknisiInfo::where('no_identity', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        $query = $service->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(TeknisiInfoRequest $request, Service $service)
    {

        // validasi
        $request->validated();
        $user = Auth::user();
        if ($user->hasRole('Technician')) {
            $url = '/teknisi/dashboard';
        }

        if ($user->hasRole('Customer')) {
            $userLogin = Auth::user()->id;
            $user = User::where('id', $userLogin)->first();

            if ($user->status == 1) {
                User::where('id', $userLogin)->update([
                    'status' => 7,
                ]);
            }
            $url = '/customer/dashboard';
        }

        $response = [
            'address' => $service->create($request->all()),
            'redirect' => $url
        ];

        return $this->successResponse(
            $response,
            'success',
            201
        );
    }





    /**
     * show one data
     */
    public function show(Religion $religion)
    {
        $response = $religion;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request)
    {
        // return $this->successResponse(
        //     $service->setTeknisiInfo($tekInfo)->update($request->all()),
        //     'success'
        // );

        $storage = TeknisiInfo::getImagePathUpload();
        $file = null;

        if ($request->hasFile('attachment')) {
            $image = (new ImageUpload)->upload($request->file('attachment'), $storage);
            $file = $image->getFilename();
        }

        // data teknisi
        $update = TeknisiInfo::where('id', Auth::user()->id)->update([
            'attachment' => $file,
            'no_identity' => $request->no_identity,
            'ms_curriculum_id' => $request->ms_curriculum_id,
            'user_id' => Auth::user()->id,
        ]);


        User::where('id', Auth::user()->id)->update([
            'status' => 7,
        ]);

        return $this->successResponse(
            $update,
            'success',
            201
        );
    }

    /**
     * delete data
     */
    public function destroy(TeknisiInfo $tekInfo, Service $service)
    {
        return $this->successResponse(
            $service->setReligion($religion)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }

    public function create(TeknisiInfoRequest $request)
    {
        DB::beginTransaction();
        try {
            $cekTypeAddress = AddressType::get();
            // return count($cekTypeAddress);
            if (count($cekTypeAddress) == 1) {
                $request->validated();

                $user = auth()->user();
                $isMain = MsAddress::where('user_id', $user->id)
                    ->first();

                $path = MsAdditionalInfo::getImagePathUpload();
                $filename = null;

                if ($request->hasFile('picture')) {
                    $image = (new ImageUpload)->upload($request->file('picture'), $path);
                    $filename = $image->getFilename();
                }

                MsAdditionalInfo::create([
                    'ms_religion_id' => $request->ms_religion_id,
                    'ms_marital_id' => $request->ms_marital_id,
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'date_of_birth' => $request->date_of_birth,
                    'place_of_birth' => $request->place_of_birth,
                    'gender' => $request->gender,
                    'user_id' => $user->id
                ]);

                $storage = TeknisiInfo::getImagePathUpload();
                $file = null;

                if ($request->hasFile('attachment')) {
                    $image = (new ImageUpload)->upload($request->file('attachment'), $storage);
                    $file = $image->getFilename();
                }

                // data teknisi
                TeknisiInfo::create([
                    'attachment' => $file,
                    'no_identity' => $request->no_identity,
                    'ms_curriculum_id' => $request->ms_curriculum_id,
                    'user_id' => $user->id,
                ]);

                MsAddress::create([
                    'is_main' => $isMain == null ? 1 : 0,
                    'is_teknisi' => 1,
                    'address' => $request->address,
                    'user_id' => Auth::user()->id,
                    'ms_address_type_id' => 1,
                    'phone1' => $request->phone1 ?? null,
                    'phone2' => $request->phone2 ?? null,
                    'latitude' => $request->lat ?? null,
                    'longitude' => $request->long ?? null,
                    'ms_city_id' => $request->ms_city_id ?? null,
                    'location_google' => $request->search_map ?? null,
                    'cities' => $request->cities ?? null,
                    'province' => $request->province ?? null,
                ]);

                $userLogin = Auth::user()->id;
                $user = User::where('id', $userLogin)->first();
                if ($user->is_login_as_teknisi == 1) {
                    User::where('id', $userLogin)->update([
                        'status' => 7,
                    ]);
                }

                if ($user->hasRole('Customer')) {
                    $userLogin = Auth::user()->id;
                    $user = User::where('id', $userLogin)->first();

                    if ($user->status == 1) {
                        User::where('id', $userLogin)->update([
                            'status' => 7,
                        ]);
                    }
                    $url = '/customer/preview';
                }

                $response = [
                    'redirect' => $url
                ];

                return $this->successResponse(
                    $response,
                    'success',
                    201
                );
            } else {
                return $this->errorResponse('This technician registration is not successful, please contact our admin');
            }
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function updateDataTechnician(TeknisiInfoRequest $request)
    {  
        $request->validated();

        $user = auth()->user();
        $isMain = MsAddress::where('user_id', $user->id)
            ->first();

        $path = MsAdditionalInfo::getImagePathUpload();
        $filename = null;

        if ($request->hasFile('picture')) {
            $image = (new ImageUpload)->upload($request->file('picture'), $path);
            $filename = $image->getFilename();
        }else{
            $filename = '';
        }
        
        $cek = MsAdditionalInfo::where('user_id', Auth::user()->id)->count();
        
        
        if($cek > 0){
            $cekGambar = MsAdditionalInfo::where('user_id', Auth::user()->id)->first();

            if(!empty($request->picture)){
                if ($request->hasFile('picture')) {
                    $image      = $request->file('picture');
                    $file_name   = time().'.' . $image->getClientOriginalExtension();
                    $img = Image::make($image);
                    $img->stream(); // <-- Key point
                    Storage::disk('local')->put('public/landing-page-images/' . $file_name, $img);
        
                    $exists = Storage::disk('local')->has('public/landing-page-images/' . $cekGambar->picture);
                    if ($exists) {
                        Storage::delete('public/landing-page-images/' . $cekGambar->picture);
                    }
                }
            }else{
                $file_name = $cekGambar->picture;
            }
            MsAdditionalInfo::where('user_id', Auth::user()->id)->update([
                'ms_religion_id' => $request->ms_religion_id,
                'ms_marital_id' => $request->ms_marital_id,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'date_of_birth' => $request->date_of_birth,
                'place_of_birth' => $request->place_of_birth,
                'gender' => $request->gender,
                'user_id' => $user->id,
                'picture' => $file_name
            ]);
        }else{
            MsAdditionalInfo::create([
                'ms_religion_id' => $request->ms_religion_id,
                'ms_marital_id' => $request->ms_marital_id,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'date_of_birth' => $request->date_of_birth,
                'place_of_birth' => $request->place_of_birth,
                'gender' => $request->gender,
                'user_id' => $user->id,
                'picture' => $filename
            ]);
        }

        $storage = TeknisiInfo::getImagePathUpload();
        $file = null;

        if ($request->hasFile('attachment')) {
            $image = (new ImageUpload)->upload($request->file('attachment'), $storage);
            $file = $image->getFilename();
        }

        // data teknisi
        $cekTeknisi = TeknisiInfo::where('user_id', Auth::user()->id)->count();
        if($cekTeknisi > 0){
            TeknisiInfo::where('user_id', Auth::user()->id)->update([
                'attachment' => $file,
                'no_identity' => $request->no_identity,
                'ms_curriculum_id' => $request->ms_curriculum_id,
                'user_id' => $user->id,
            ]);
        }else{
            TeknisiInfo::create([
                'attachment' => $file,
                'no_identity' => $request->no_identity,
                'ms_curriculum_id' => $request->ms_curriculum_id,
                'user_id' => $user->id,
            ]);
        }
            
        $cekAddress = MsAddress::where('user_id', Auth::user()->id)->count();
        if($cekAddress > 0){
            MsAddress::where('user_id', Auth::user()->id)->update([
                'is_main' => $isMain == null ? 1 : 0,
                'is_teknisi' => 1,
                'address' => $request->address,
                'user_id' => Auth::user()->id,
                'ms_address_type_id' => $request->ms_address_type_id ?? null,
                'phone1' => $request->phone1 ?? null,
                'phone2' => $request->phone2 ?? null,
                'latitude' => $request->lat ?? null,
                'longitude' => $request->long ?? null,
                'ms_city_id' => $request->ms_city_id ?? null,
                'location_google' => $request->search_map ?? null,
                // 'cities' => $request->cities ?? null,
                // 'province' => $request->province ?? null,
            ]);
        }else{
            MsAddress::create([
                'is_main' => $isMain == null ? 1 : 0,
                'is_teknisi' => 1,
                'address' => $request->address,
                'user_id' => Auth::user()->id,
                'ms_address_type_id' => $request->ms_address_type_id ?? null,
                'phone1' => $request->phone1 ?? null,
                'phone2' => $request->phone2 ?? null,
                'latitude' => $request->lat ?? null,
                'longitude' => $request->long ?? null,
                'ms_city_id' => $request->ms_city_id ?? null,
                'location_google' => $request->search_map ?? null,
                // 'cities' => $request->cities ?? null,
                // 'province' => $request->province ?? null,
            ]);
        }
            
            

        $userLogin = Auth::user()->id;
        $user = User::where('id', $userLogin)->first();

        // if ($user->status == 1) {
        User::where('id', $userLogin)->update([
            'status' => 7,
        ]);
        // }
        $url = '/customer/dashboard';
        

        $response = [
            'redirect' => $url
        ];

        return $this->successResponse(
            $response,
            'success',
            201
        );
        // } catch (QueryException $e) {
        //     DB::rollback();
        //     throw new \Exception($e->getMessage());
        // }
    }

    public function submitAppInfo()
    {
        $user = User::with([
            'info.religion',
            'info.marital',
            'address.city',
            'address.types',
        ])->where('id', Auth::id())->firstOrFail();

        $teknisi = Technician::where('user_id', $user->id)->first();
        $teknisi_info = TeknisiInfo::where('user_id', Auth::id())->first();
        $type = 'SUCCESS';
        $redirect_to_app_submmit_form = false;
        if ($user->status == 7) {
            $type = 'APP_SUBMIT_PROSSES';
            $redirect_to_app_submmit_form = true;
        }
        if ($user->status == 8) {
            $type = 'APP_SUBMIT_REJECT';
            $redirect_to_app_submmit_form = true;
        }
        $response = [
            'user' => $user,
            'teknisi' => $teknisi,
            'teknisi_info' => $teknisi_info,
            'status' => [
                'type' => $type,
                'redirect_to_app_submmit_form' => $redirect_to_app_submmit_form,
                'note' => $user->note_reject,
                'reject' => ($user->status == 8) ? true : false
            ]
        ];

        return $this->successResponse(
            $response,
            'success',
            200
        );
    }
}
