<?php

namespace App\Http\Controllers\Api\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Model\Master\ExcelIncome;
use App\Model\Master\PDESHistory;
use App\Model\Master\StatusExcel;

use Illuminate\Support\Facades\DB;
use App\Model\Master\JasaDataExcel;
use App\Model\Master\PartsDataExcel;
use Illuminate\Support\Facades\Auth;
use App\Model\Master\OrdersDataExcel;
use App\Http\Controllers\ApiController;
use App\Model\Master\EngineerCodeExcel;
use App\Model\Master\ExcelIncomeDetail;
use Illuminate\Database\QueryException;
use App\Model\Master\OrdersDataExcelLog;
use App\Model\Master\GeneralJournalDetail;
use App\Model\Master\PartsDataExcelBundle;
use App\Model\Master\PartDataExcelStockBundle;
use App\Services\Master\OrdersDataExcelService;
use App\Model\Master\PartDataExcelStockInventory;
use App\Model\Master\ExcelIncomeDetailDescription;
use App\Services\Master\PartDataExcelStockService;
use App\Model\Master\PDSInventoryMutationHistories;
use App\Model\Master\PartDataExcelStockBundleDetail;
use App\Services\Master\OrdersDataExcelService as Service;
use App\Services\Master\PartDataExcelStockInventoryService;

class OrdersDataExcelController extends ApiController
{

    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function datatablesSearch(Service $service, Request $request)
    {
        return $service->datatables($request);
    }

    public function datatablesLogs($id, $type=null)
    {
        $query = OrdersDataExcelLog::with('user');
        if($id != '0') {
            $query->where('orders_data_excel_id', $id);
        }
        if(!empty($type)) {
            $query->where('type', $type);
        }
        $query->orderBy('created_at','DESC')->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->toJson();
    }

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {


        return $service->create($request->all());

    }

    /**
     * show one data
     */
    public function show(OrdersDataExcel $marital)
    {
        $response = $marital;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, OrdersDataExcel $orders_data_excel, Service $service)
    {
        return $this->successResponse(
            $service->setData($orders_data_excel)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(OrdersDataExcel $orders_data_excel, Service $service)
    {
        return $this->successResponse(
            $service->setData($orders_data_excel)->deleteX(),
            'success',
            204
        );
    }

    public function destroyBundleData($bundle_excel_id)
    {
        $bundle = PartsDataExcelBundle::with('part_data_stock_bundle.part_data_stock_bundle_detail')->where('id', $bundle_excel_id)->firstOrFail();
        $service = new OrdersDataExcelService();
        DB::beginTransaction();
        try {
            $service->bundleDetailsDelete($bundle_excel_id);
            DB::commit();
            $bundle = $bundle->delete();
            return $this->successResponse([
                'uniq' => time(),
            ], $this->successDeleteMsg(), 200);
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function destroyPartData(Request $request, $part_data_id)
    {
        $part = PartsDataExcel::where('id', $part_data_id)->firstOrFail();

        $stock_inventory_service = new PartDataExcelStockInventoryService();
        DB::beginTransaction();
        try {
            if($part->part_data_stock_inventories_id != null) {
                $update_log_gjd_deleted = PDESHistory::where('part_data_excel_id', $part_data_id)->update(['is_deleted' => 1]);
                $this->destroyOrderByCustomer($part_data_id);
                $data_history_inventory = [
                    'date' => Carbon::parse($part->created_at)->format('Y-m-d'),
                    'pdesi_id_out' => null,
                    'pdesi_id_in' => $part->part_data_stock_inventories_id,
                    'amount' => $part->quantity,
                    'type_amount' =>  1,
                    'type_mutation' =>  5,
                    'general_journal_details_id' => null,
                    'part_data_excel_id' => $part->id,
                ];
                // return $data_history_inventory;
                $stock_inventory_service->historyLogProcess($data_history_inventory);
                // $pdes_service->historyLogProcess(null, $part->part_data_stock_id, $part->quantity, 1, 7, null, $part->id);
                $stock_inventory_service->restoreStock($part->part_data_stock_inventories_id,$part->quantity);

            }
            DB::commit();
            $part = $part->delete();

            return $this->successResponse([
                'uniq' => time(),
            ], $this->successDeleteMsg(), 200);
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function destroyJasaData($jasa_data_excel)
    {
        $jasa = JasaDataExcel::where('id', $jasa_data_excel)->firstOrFail();
        $jasa = $jasa->delete();
        return $this->successResponse([
            'uniq' => time(),
        ], $this->successDeleteMsg(), 200);
    }

    public function destroyOrderByCustomer($pde_id) {
        $cek_ei= ExcelIncomeDetail::where('part_data_excel_id', $pde_id); // Excel Income Detail Order By Customer;
        if($cek_ei->count() > 0) {
            $del_desc = ExcelIncomeDetailDescription::where('excel_income_detail_id', $cek_ei->first()->excel_income_detail_id);
            return $cek_ei->delete();
        }
    }

    public function updatePartData(Request $request, $orders_data_excel_id, Service $service)
    {
        $orders_data_excel = OrdersDataExcel::with(['part_data_excel'])
            ->where('id', $orders_data_excel_id)
            ->firstOrFail();

        $data_part_update = [];
        $data_part_insert = [];

        $part_data_id = $request->part_data_id;
        $code_material = $request->code_material;
        $part_description = $request->part_description;
        $quantity = $request->quantity;
        $part_data_stock_inventories_id = $request->part_data_stock_inventories_id;
        $part_data_stock_inventories_id_before = $request->part_data_stock_inventories_id_before;

        $stock_inventory_service = new PartDataExcelStockInventoryService();
        // loop request
        if(!empty($part_data_id)) {
            foreach ($part_data_id as $key => $value) {
                // check request
                $check_part = !empty($code_material[$key]) && !empty($part_description[$key]) && !empty($quantity[$key]);

                if ($check_part) {

                    // jika id tidak 0 == update
                    if ($value != 0) {
                        $data_part_update[] = [
                            'id' => $value,
                            'part_data_stock_inventories_id' => $part_data_stock_inventories_id[$key],
                            'part_data_stock_inventories_id_before' => $part_data_stock_inventories_id_before[$key],
                            'code_material' => $code_material[$key],
                            'part_description' => $part_description[$key],
                            'quantity' => $quantity[$key],
                            'is_order_by_customer' => $request->is_order_by_customer[$key],
                            'price_hpp_average' => ($request->is_order_by_customer[$key] == 1 ? $request->price_hpp_average[$key] : null),
                            'selling_price' => ($request->is_order_by_customer[$key] == 1 ? $request->selling_price[$key] : null),
                            'selling_price_pcs' => ($request->is_order_by_customer[$key] == 1 ? $request->selling_price_pcs[$key] : null)

                        ];

                    } else { // jika id 0 == insert
                        $data_part_insert[] = [
                            'orders_data_excel_id' => $orders_data_excel->id,
                            'part_data_stock_inventories_id' => $part_data_stock_inventories_id[$key],
                            'code_material' => $code_material[$key],
                            'part_description' => $part_description[$key],
                            'quantity' => $quantity[$key],
                            'is_order_by_customer' => $request->is_order_by_customer[$key],
                            'price_hpp_average' => ($request->is_order_by_customer[$key] == 1 ? $request->price_hpp_average[$key] : null),
                            'selling_price' => ($request->is_order_by_customer[$key] == 1 ? $request->selling_price[$key] : null),
                            'selling_price_pcs' => ($request->is_order_by_customer[$key] == 1 ? $request->selling_price_pcs[$key] : null),
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ];
                    }

                }
            }
        } else {
            return $this->errorResponse(
                $request->jasa_data_id,
                'Data Part Empty, Cannot Save !',
                400
            );
        }

        DB::beginTransaction();
        try {
            // update sparrepart
            if (count($data_part_update) > 0) {

                foreach ($data_part_update as $data) {
                    $stock_before = PartDataExcelStockInventory::where('id', $data['part_data_stock_inventories_id'])->value('stock');
                    if($data['quantity'] != $stock_before) {
                        $data_history_inventory = [
                            'date' => Carbon::now()->format('Y-m-d'),
                            'pdesi_id_out' => $data['part_data_stock_inventories_id'],
                            'pdesi_id_in' => null,
                            'amount' => ($data['quantity']),
                            'type_amount' => 0,
                            'type_mutation' => 4,
                            'general_journal_details_id' => null,
                            'part_data_excel_id' => $data['id']
                        ];
                        // $stock_inventory_service->historyLogProcess($data_stock_inventory->id, null, $data['quantity'], 0, 5, null, $data['id']);
                        $stock_inventory_service->historyLogProcess($data_history_inventory);
                        $decrease_stock = $this->decreaseStockUpdate($data['part_data_stock_inventories_id'], $data['id'], $data['quantity']);
                        if($decrease_stock['status'] == false) {
                            return $this->errorResponse(
                                $decrease_stock['stock_now'],
                                'Code Material = '.$data['code_material'].'. Quantity melebihi jumlah stock terupdate sekarang = '.$decrease_stock['stock_now'].'',
                                400
                            );
                        }
                    }

                    PartsDataExcel::where('id', $data['id'])->update([
                        'code_material' => $data['code_material'],
                        'part_description' => $data['part_description'],
                        'part_data_stock_inventories_id' => $data['part_data_stock_inventories_id'],
                        'quantity' => $data['quantity'],
                        'is_order_by_customer' => $data['is_order_by_customer'],
                        'price_hpp_average' => $data['price_hpp_average'],
                        'selling_price' => $data['selling_price'],
                        'selling_price_pcs' => $data['selling_price_pcs'],
                    ]);

                    if($data['is_order_by_customer']) {
                        if($data['part_data_stock_inventories_id_before'] != "") {
                            if($data['part_data_stock_inventories_id'] != $data['part_data_stock_inventories_id_before']) {
                                $this->destroyOrderByCustomer($data['id']);
                                $service->storeIsOrderByCustomerExcelIncome($request->repair_completed_date, $request->service_order_no, $request->defect_type, $data['price_hpp_average'], $data['selling_price'], $data['id'], $request->asc_name);
                            }
                        }

                    } else {
                        $this->destroyOrderByCustomer($data['id']);
                    }

                }
            }

            // insert part_datas
            if (count($data_part_insert) > 0) {
                // PartsDataExcel::insert($data_part_insert);
                foreach ($data_part_insert as $data) {
                    $duplicate = PartsDataExcel::where('orders_data_excel_id',$data['orders_data_excel_id'])->where('part_data_stock_inventories_id', $data['part_data_stock_inventories_id'])->count();
                    if($duplicate == 0) {
                        $pde = PartsDataExcel::create([
                            'orders_data_excel_id' => $data['orders_data_excel_id'],
                            'part_data_stock_inventories_id' => $data['part_data_stock_inventories_id'],
                            'code_material' => $data['code_material'],
                            'part_description' => $data['part_description'],
                            'quantity' => $data['quantity'],
                            'is_order_by_customer' => $data['is_order_by_customer'],
                            'price_hpp_average' => $data['price_hpp_average'],
                            'selling_price' => $data['selling_price'],
                            'selling_price_pcs' => $data['selling_price_pcs'],
                        ]);
                        // return $pde;
                        $data_history_inventory = [
                            'date' => Carbon::parse($pde->created_at)->format('Y-m-d'),
                            'pdesi_id_out' => $data['part_data_stock_inventories_id'],
                            'pdesi_id_in' => null,
                            'amount' => ($data['quantity']),
                            'type_amount' =>  0,
                            'type_mutation' =>  3,
                            'general_journal_details_id' => null,
                            'part_data_excel_id' => $pde->id
                        ];
                        $stock_inventory_service->historyLogProcess($data_history_inventory);
                        // $stock_inventory_service->historyLogProcess($data['part_data_stock_id'], null, $data['quantity'], 0, 3, null, $pde->id);
                        $decrease_stock = $this->decreaseStock($data['part_data_stock_inventories_id'],$data['quantity']);

                        if($decrease_stock['status'] == false) {
                            return $this->errorResponse(
                                $decrease_stock['stock_now'],
                                'Code Material = '.$data['code_material'].'. Quantity melebihi jumlah stock terupdate sekarang = '.$decrease_stock['stock_now'].'',
                                400
                            );
                        }
                        if($data['is_order_by_customer']) {
                            $service->storeIsOrderByCustomerExcelIncome($request->repair_completed_date, $request->service_order_no, $request->defect_type, $data['price_hpp_average'], $data['selling_price'], $pde->id, $request->asc_name);
                        }
                    }
                }
            }
            $service->logs($orders_data_excel, 2);
            DB::commit();
            return $this->successResponse(
                $data_part_update,
                $this->successStoreMsg(),
                200
            );
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function updateJasaData(Request $request, $orders_data_excel_id, Service $service)
    {
        $orders_data_excel = OrdersDataExcel::with(['jasa_data_excel'])
            ->where('id', $orders_data_excel_id)
            ->firstOrFail();

        $data_update = [];
        $data_insert = [];
        if(!empty($request->jasa_data_id)) {
            // loop request
            foreach ($request->jasa_data_id as $key => $value) {
                // check request
                $check_part = !empty($request->jasa_id[$key]) && !empty($request->price_j[$key]);
                if ($check_part) {
                    // jika id tidak 0 == update
                    if ($value != 0) {
                        $data_update[] = [
                            'id' => $value,
                            'jasa_id' => $request->jasa_id[$key],
                            'jasa_name' => $request->jasa_name[$key],
                            'price' =>$request->price_j[$key]
                        ];

                    } else { // jika id 0 == insert
                        $data_insert[] = [
                            'orders_data_excel_id' => $orders_data_excel->id,
                            'jasa_id' => $request->jasa_id[$key],
                            'jasa_name' => $request->jasa_name[$key],
                            'price' =>$request->price_j[$key],
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ];
                    }
                }
            }

            DB::beginTransaction();
            try {
                // update jasa
                if (count($data_update) > 0) {
                    foreach ($data_update as $data) {
                        JasaDataExcel::where('id', $data['id'])->update([
                            'jasa_id' => $data['jasa_id'],
                            'jasa_name' => $data['jasa_name'],
                            'price' => $data['price']
                        ]);
                    }
                }
                // insert jasa
                if (count($data_insert) > 0) {
                    foreach ($data_insert as $data) {
                        $duplicate = JasaDataExcel::where('orders_data_excel_id',$data['orders_data_excel_id'])->where('jasa_id', $data['jasa_id'])->count();
                        if($duplicate == 0) {
                            $jde = JasaDataExcel::create([
                                'orders_data_excel_id' => $data['orders_data_excel_id'],
                                'jasa_id' => $data['jasa_id'],
                                'jasa_name' => $data['jasa_name'],
                                'price' => $data['price'],
                                'created_at' => $data['created_at'],
                                'updated_at' => $data['updated_at'],
                            ]);
                        }
                    }
                }
                DB::commit();
                return $this->successResponse(
                    [$data_insert,$data_update],
                    $this->successStoreMsg(),
                    200
                );
            } catch (QueryException $e) {
                DB::rollback();
                throw new \Exception($e->getMessage());
            }
        } else {
            return $this->errorResponse(
                $request->jasa_data_id,
                'Data Jasa Empty, Cannot Save !',
                400
            );
        }

    }

    public function addBundleData(Request $request, Service $service) {
        $stock_inventory_service = new PartDataExcelStockInventoryService();
        return $service->insertBundleDetails($request->orders_data_excel_id, $request, $stock_inventory_service);
    }

    public function updateBundleData(Request $request, $orders_data_excel_id, Service $service)
    {
        $orders_data_excel = OrdersDataExcel::with(['jasa_data_excel'])
            ->where('id', $orders_data_excel_id)
            ->firstOrFail();
        $stock_inventory_service = new PartDataExcelStockInventoryService();
        $data_update = [];
        $data_insert = [];
        if(!empty($request->bundle_data_id)) {
            // loop request
            foreach ($request->bundle_data_id as $key => $value) {
                // check request
                $check_part = !empty($request->part_data_stock_bundle_id[$key]);
                if ($check_part) {
                    // jika id tidak 0 == update
                    if ($value != 0) {
                        $data_update[] = [
                            'id' => $value,
                            'bundle_name' => $request->bundle_name[$key],
                            'main_price' => $request->main_price[$key],
                            'part_data_stock_bundle_id' => $request->part_data_stock_bundle_id[$key],
                            'part_data_stock_bundle_id_before' => $request->part_data_stock_bundle_id_before[$key],
                        ];

                    } else { // jika id 0 == insert
                        $data_insert[] = [
                            'orders_data_excel_id' => $orders_data_excel->id,
                            'part_data_stock_bundle_id' => $request->part_data_stock_bundle_id[$key],
                            'bundle_name' => $request->bundle_name[$key],
                            'main_price' => $request->main_price[$key],
                        ];
                    }
                }
            }

            DB::beginTransaction();
            try {
                // update Bundle
                if (count($data_update) > 0) {
                    foreach ($data_update as $data) {
                        if($data['part_data_stock_bundle_id'] != $data['part_data_stock_bundle_id_before']) {
                            $service->bundleDetailsDelete($data['id']);
                        }
                        PartsDataExcelBundle::where('id', $data['id'])->update([
                            'part_data_stock_bundle_id' => $data['part_data_stock_bundle_id'],
                            'bundle_name' => $data['bundle_name'],
                            'main_price' => $data['main_price'],
                            'quantity' => 1
                        ]);
                        $data_bundle = PartsDataExcelBundle::with('part_data_stock_bundle.part_data_stock_bundle_detail')->where('id', $data['id'])->first();
                        foreach ($data_bundle->part_data_stock_bundle->part_data_stock_bundle_detail as $bundle_detail) {
                            $pde = PartsDataExcel::create([
                                'orders_data_excel_id' => $orders_data_excel_id,
                                'part_data_stock_inventories_id' => $bundle_detail->part_data_stock_inventories_id,
                                'part_data_excel_bundle_id' => $data['id'],
                                'part_data_stock_bundle_details_id' => $bundle_detail->id,
                                'code_material' => $bundle_detail->code_material,
                                'part_description' => $bundle_detail->part_description,
                                'quantity' => $bundle_detail->quantity,
                            ]);
                            $decrease_stock = $stock_inventory_service->decreaseStock($bundle_detail->part_data_stock_inventories_id, $bundle_detail->quantity);
                            $data_history_inventory = [
                                'date' => Carbon::parse($pde->created_at)->format('Y-m-d'),
                                'pdesi_id_out' => $bundle_detail->part_data_stock_inventories_id,
                                'pdesi_id_in' => null,
                                'amount' => $bundle_detail->quantity,
                                'type_amount' =>  0,
                                'type_mutation' =>  3,
                                'general_journal_details_id' => null,
                                'part_data_excel_id' => $pde->id,
                                'part_data_stock_bundle_details_id' => $bundle_detail->id
                            ];
                            $stock_inventory_service->historyLogProcess($data_history_inventory);
                            $apicontroller = new ApiController();
                            if($decrease_stock == false) {
                                return $apicontroller->errorResponse(
                                    $decrease_stock['stock_now'],
                                    'Code Material = '.$bundle_detail->code_material.'. Quantity melebihi jumlah stock terupdate sekarang = '.$decrease_stock['stock_now'].'',
                                    400
                                );
                            }
                        }

                    }
                }
                // insert jasa
                if (count($data_insert) > 0) {
                    foreach ($data_insert as $data) {
                        $duplicate = PartsDataExcelBundle::where('orders_data_excel_id',$data['orders_data_excel_id'])->where('jasa_id', $data['jasa_id'])->count();
                        if($duplicate == 0) {
                            $jde = PartsDataExcelBundle::create([
                            'orders_data_excel_id' => $data['orders_data_excel_id'],
                            'part_data_stock_bundle_id' => $data['part_data_stock_bundle_id'],
                            'bundle_name' => $data['bundle_name'],
                            'main_price' => $data['main_price'],
                            'quantity' => 1
                            ]);
                        }
                    }
                }
                DB::commit();
                return $this->successResponse(
                    [$data_insert,$data_update],
                    $this->successStoreMsg(),
                    200
                );
            } catch (QueryException $e) {
                DB::rollback();
                throw new \Exception($e->getMessage());
            }
        } else {
            return $this->errorResponse(
                $request->jasa_data_id,
                'Data Bundle Empty, Cannot Save !',
                400
            );
        }

    }


    public function decreaseStock($pdes_id,$quantity) {
        $service = new PartDataExcelStockInventoryService();
        return  $service->decreaseStock($pdes_id,$quantity);
    }

    public function decreaseStockUpdate($part_data_stock_id, $part_data_excel_id, $quantity) {
        $service = new PartDataExcelStockInventoryService();
        return  $service->decreaseStockUpdate($part_data_stock_id, $part_data_excel_id, $quantity);
    }

    public function autoCompleteField(Request $request)
    {
        if ($request->namefield == 'engineer_code') {
            return $data = EngineerCodeExcel::where('engineer_code', 'like', '%'.strtolower($request->val).'%')->get();

        } else if($request->namefield == 'engineer_name') {
            return $data = EngineerCodeExcel::where('engineer_name', 'like', '%'.strtolower($request->val).'%')->get();

        } else if($request->namefield == 'status') {
            return $data = StatusExcel::where('status', 'like', '%'.strtolower($request->val).'%')->get();
        }

    }


}
