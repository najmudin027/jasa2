<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\BankTransferRequest;
use App\Model\Master\BankTransfer;
use App\Services\Master\BankTransferService as Service;
use Illuminate\Http\Request;
use App\Helpers\ImageUpload;

class BankTransferController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $paginate = $request->paginate;

        $response = $service->list()->whereNull('is_active')->where('is_parent', 1)->when($q, function ($query, $q) {
            $query->where('name', 'like', '%' . $q . '%');
        });

        if ($paginate == 'no_paginate') {
            $response = $response->get();
        } else {
            $response = $response->paginate(10);
        }

        return $this->successResponse($response);
    }

    public function indexType(Service $service, Request $request)
    {
        $q = $request->q;
        $paginate = $request->paginate;

        $response = $service->list()->where('is_active', 0)->whereNull('is_parent')->when($q, function ($query, $q) {
            $query->where('name', 'like', '%' . $q . '%');
        });

        if ($paginate == 'no_paginate') {
            $response = $response->get();
        } else {
            $response = $response->paginate(10);
        }

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */

    public function store(BankTransferRequest $request)
    {
        // validasi
        $request->validated();
        $user = auth()->user();
        $path = BankTransfer::getImagePathUpload();
        // dd($path);
        $filename = null;

        if ($request->hasFile('icon')) {
            $image = (new ImageUpload)->upload($request->file('icon'), $path);
            $filename = $image->getFilename();
        }

        $info = BankTransfer::where('id', $request->id)->first();

        $dataInfo = [
            'bank_name' => $request->bank_name,
            'name' => $request->name,
            'virtual_code' => $request->virtual_code,
            'desc' => $request->desc,
            'icon' => $filename,
            'is_parent' => 1
        ];

        if ($info == null) {
            $userInfo = BankTransfer::create($dataInfo);
        } else {
            if ($filename == null) {
                unset($dataInfo['icon']);
            }
            $userInfo = $info->update($dataInfo);
        }

        if ($userInfo) {
            return $this->successResponse($userInfo, 'success', 201);
        } else {
            return $this->errorResponse($userInfo, 'error', 400);
        }
    }


    /**
     * show one data
     */
    public function show(BankTransfer $bankTransfer)
    {
        $response = $bankTransfer;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(BankTransferRequest $request, BankTransfer $bankTransfer, Service $service)
    {

        $path = BankTransfer::getImagePathUpload();
        // dd($path);
        $filename = null;

        if ($request->hasFile('icon')) {
            $image = (new ImageUpload)->upload($request->file('icon'), $path);
            $filename = $image->getFilename();
        }

        $dataInfo = [
            'bank_name' => $request->bank_name,
            'name' => $request->name,
            'virtual_code' => $request->virtual_code,
            'desc' => $request->desc,
            'icon' => $filename,
            'is_parent' => 1
        ];

        if ($filename == null) {
            unset($dataInfo['icon']);
        }

        return $this->successResponse(
            $service->setBankTransfer($bankTransfer)->update($dataInfo),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(BankTransfer $bankTransfer, Service $service)
    {
        return $this->successResponse(
            $service->setBankTransfer($bankTransfer)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }
}
