<?php

namespace App\Http\Controllers\Api\Master;

use App\Helpers\ImageUpload;
use Illuminate\Http\Request;
use App\Model\Technician\Technician;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use App\Model\Master\TechnicianSparepart;
use App\Http\Requests\Master\TechnicianSparepartRequest;
use App\Services\Master\TechnicianSparepartService as Service;

class TechnicianSparepartController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2($user_id = null, Request $request, Service $service)
    {
        return $this->successResponse($service->select2($user_id,$request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */

    public function store(TechnicianSparepartRequest $request)
    {
        // validasi

        $request->validated();
        $user = auth()->user();
        $path = TechnicianSparepart::getImagePathUpload();
        // dd($path);
        $filename = null;

        if ($request->hasFile('images')) {
            $image = (new ImageUpload)->upload($request->file('images'), $path);
            $filename = $image->getFilename();
        }

        $info = TechnicianSparepart::where('id', $request->id)->first();
        $technician_id = Technician::where('user_id', Auth::user()->id)->value('id');
        $dataInfo = [
            'ms_services_id' => $request->ms_services_id,
            'technicians_id' => $technician_id,
            'name'           => $request->name,
            'price'          => $request->price,
            'description'    => $request->description,
            'images'         => $filename,
        ];
        if ($info == null) {
            $userInfo = TechnicianSparepart::create($dataInfo);
        } else {
            if ($filename == null) {
                unset($dataInfo['images']);
            }
            $userInfo = $info->update($dataInfo);
        }

        if ($userInfo) {
            return $this->successResponse($userInfo, 'success', 201);
        } else {
            return $this->errorResponse($userInfo, 'error', 400);
        }
    }

    /**
     * show one data
     */
    public function show(TechnicianSparepart $technician_sparepart)
    {
        $response = $technician_sparepart;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(TechnicianSparepartRequest $request, TechnicianSparepart $technician_sparepart, Service $service)
    {
        $path = TechnicianSparepart::getImagePathUpload();
        $filename = null;
        if ($request->hasFile('images')) {
            $image = (new ImageUpload)->upload($request->file('images'), $path);
            $filename = $image->getFilename();
        }
        $technician_id = Technician::where('user_id', Auth::user()->id)->value('id');
        $dataInfo = [
            'ms_services_id' => $request->ms_services_id,
            'technicians_id' => $technician_id,
            'name'           => $request->name,
            'price'          => $request->price,
            'description'    => $request->description,
            'images'         => $filename,
        ];

        if ($filename == null) {
            unset($dataInfo['images']);
        }

        return $this->successResponse(
            $service->setServices($technician_sparepart)->update($dataInfo),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(TechnicianSparepart $technician_sparepart, Service $service)
    {
        return $this->successResponse(
            $service->setServices($technician_sparepart)->delete($technician_sparepart->id),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }

}
