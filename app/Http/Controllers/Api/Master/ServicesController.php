<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\ServicesRequest;
use App\Model\Master\Services;
use App\Services\Master\ServicesService as Service;
use Illuminate\Http\Request;
use App\Helpers\ImageUpload;

class ServicesController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $paginate = $request->paginate;

        $response = $service->list()
            ->with(['symptoms.service_types.product_group'])
            ->when($q, function ($query, $q) {
                $query->where('name', 'like', '%' . $q . '%');
            })
            ->orderBy('name', 'asc');

        if ($paginate == 'no_paginate') {
            $response = $response->get();
        }else{
            $response = $response->paginate(10);
        }

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */

    public function store(ServicesRequest $request, Service $service)
    {
        // validasi
        $request->validated();
        $user = auth()->user();
        $path = Services::getImagePathUpload();
        // dd($path);
        $filename = null;

        if ($request->hasFile('icon')) {
            $image = (new ImageUpload)->upload($request->file('icon'), $path);
            $filename = $image->getFilename();
        }

        $info = Services::where('id', $request->id)->first();

        $datas = [
            'name'   => $request->name,
            'slug'   => $request->slug,
            'images' => $filename
        ];

        return $this->successResponse(
            $service->create($datas),
            'success create!',
            201
        );
    }

    /**
     * show one data
     */
    public function show(Services $Services)
    {
        $response = $Services;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ServicesRequest $request, Services $Services, Service $service)
    {
        $user = auth()->user();

        $path = Services::getImagePathUpload();
        // dd($path);
        $filename = null;
        if ($request->hasFile('icon')) {
            $image = (new ImageUpload)->upload($request->file('icon'), $path);
            $filename = $image->getFilename();
        }
        $datas = [
            'name' => $request->name,
            'slug' => $request->slug,
            'images' => $filename != null ? $filename : isset($Services->images)
        ];
        if ($Services->images != null) {
            $file_path = storage_path('app/public/services/' . $Services->images);
            if (file_exists($file_path)) {
                unlink($file_path);
            }
        }
        return $this->successResponse(
            $service->setServices($Services)->update($datas),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(Services $Services, Service $service)
    {
        if ($Services->images != null) {
            $file_path = storage_path('app/public/services/' . $Services->images);
            if (file_exists($file_path)) {
                unlink($file_path);
            }
        }

        return $this->successResponse(
            $service->setServices($Services)->delete($Services->id),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }

    // public function showListServicesUser() {
    //     return Services::all();
    // }
}
