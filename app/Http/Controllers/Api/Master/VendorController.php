<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\VendorReuqest;
use App\Model\Master\Vendor;
use App\Services\Master\VendorService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class VendorController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(VendorService $vendorService)
    {
        $response = $vendorService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = Vendor::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(VendorService $vendorService)
    {
        $query = $vendorService->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(VendorReuqest $request, VendorService $vendorService)
    {
        return $this->successResponse(
            $vendorService->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(Vendor $vendor)
    {
        $response = $vendor;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(VendorReuqest $request, Vendor $vendor, VendorService $vendorService)
    {
        return $this->successResponse(
            $vendorService->setVendor($vendor)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(Vendor $vendor, VendorService $vendorService)
    {
        return $this->successResponse(
            $vendorService->setVendor($vendor)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, VendorService $vendorService)
    {
        return $this->successResponse(
            $vendorService->deleteById($request->id),
            'success',
            204
        );
    }
}
