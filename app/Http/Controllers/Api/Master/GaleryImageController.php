<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Services\Master\GaleryImageService as Service;
use App\Http\Requests\Master\GaleryImageRequest;
use App\Model\Master\ImageUpload;

class GaleryImageController extends ApiController
{
    public function show(Service $service, ImageUpload $galeryImages)
    {
        return $this->successResponse($galeryImages);
    }
    
    public function update(Service $service, GaleryImageRequest $request, ImageUpload $galeryImages)
    {
        $request->validated();

        return $this->successResponse(
            $service->setGaleryImage($galeryImages)->update($request->all()),
            'Success'
        );
    }
}
