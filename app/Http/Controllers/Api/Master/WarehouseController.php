<?php

namespace App\Http\Controllers\Api\Master;

use App\Model\Master\MsCity;
use Illuminate\Http\Request;
use App\Model\Master\MsVillage;
use App\Model\Master\MsZipcode;
use App\Model\Master\MsDistrict;
use Yajra\DataTables\DataTables;
use App\Model\Master\MsWarehouse;
use App\Traits\MasterImportTrait;
use App\Http\Controllers\ApiController;
use App\Services\Master\VillageService;
use App\Services\Master\WarehouseService;
use App\Http\Requests\Master\VillageRequest;
use App\Http\Requests\Master\WarehouseRequest;

class WarehouseController extends ApiController
{
    use MasterImportTrait;

    /**
     * show list with paginate
     */
    public function index(WarehouseService $warehouseService, Request $request)
    {
        $q = $request->q;
        $paginate = $request->paginate;

        $response = MsWarehouse::when($q, function ($query, $q) {
                $query->where('name', 'like', '%' . $q . '%');
            });

        if ($paginate == 'no_paginate') {
            $response = $response->get();
        }else{
            $response = $response->paginate(10);
        }
        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = MsWarehouse::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function select2p($id, Request $request)
    {
        $response = MsWarehouse::where('name', 'like', '%' . $request->q . '%')
            ->where('id', '<>', $id)
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(WarehouseService $warehouseService)
    {
        $query = $warehouseService->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(WarehouseRequest $request, WarehouseService $warehouseService)
    {
        return $this->successResponse(
            $warehouseService->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(MsWarehouse $warehouse)
    {
        $response = $warehouse;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(WarehouseRequest $request, MsWarehouse $warehouse, WarehouseService $warehouseService)
    {

        return $this->successResponse(
            $warehouseService->setWarehouse($warehouse)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(MsWarehouse $warehouse, WarehouseService $warehouseService)
    {
        return $this->successResponse(
            $warehouseService->setWarehouse($warehouse)->delete(),
            'success'
        );
    }
}
