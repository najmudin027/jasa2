<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class MasterReligionController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Religion'
        ])
        ->setBreadcrumb('List Religion', '/admin/religion/show')
        ->viewAdmin('admin.master.religion.index');
    }
}
