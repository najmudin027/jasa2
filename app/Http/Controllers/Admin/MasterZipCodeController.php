<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsZipcode;

class MasterZipCodeController extends AdminController
{
    public function showZipCode()
    {
        return $this
        ->setBreadcrumb('List ZipCode', '/admin/zipcode/show')
        ->viewAdmin('admin.master.location.zipcode.index');
    }

    public function createZipCode()
    {
        return $this
        ->setBreadcrumb('List ZipCode', '/admin/zipcode/show')
        ->setBreadcrumb('Create ZipCode', '/admin/zipcode/create')
        ->viewAdmin('admin.master.location.zipcode.create');
    }

    public function updateZipCode($id)
    {
        $editZipcode = MsZipcode::with('district.city.province.country','district.city.province','district.city','district')->where('id', $id)->first();
        return $this
        ->setBreadcrumb('List ZipCode', '/admin/zipcode/show')
        ->setBreadcrumb('Update ZipCode', '/admin/zipcode/update')
        ->viewAdmin('admin.master.location.zipcode.update', compact('editZipcode'));
    }
}
