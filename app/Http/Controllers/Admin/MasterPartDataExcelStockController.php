<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Model\Master\PartDataExcelStock;
use App\Http\Controllers\AdminController;
use App\Model\Master\GeneralJournalDetail;

class MasterPartDataExcelStockController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Part Data Excel Stock'
        ])
        ->setBreadcrumb('List Part Data Excel Stock', '/admin/part-data-excel-stock/show')
        ->viewAdmin('admin.master.part-data-excel-stock.index');
    }

    public function mutationPdes($id)
    {
        $pdes = PartDataExcelStock::where('id', $id)->firstOrFail();
        return $this
            ->setBreadcrumb('List Part Data Excel Stock', '/admin/part-data-excel-stock/show')
            ->viewAdmin('admin.master.part-data-excel-stock.mutation', [
                'title' => 'Mutation Part Data Excel Stock',
                'pdes' => $pdes
            ]);
    }

    public function historyInOutPdes($id)
    {
        $pdes = PartDataExcelStock::where('id', $id)->firstOrFail();
        return $this
            ->setBreadcrumb('List Part Data Excel Stock', '/admin/part-data-excel-stock/show')
            ->viewAdmin('admin.master.part-data-excel-stock.history-in-out', [
                'title' => 'History In Out Part Data Excel Stock',
                'pdes' => $pdes
            ]);
    }

    public function showMutationHistories()
    {

        return $this
            ->setBreadcrumb('Mutation Part Data Stock', '#')
            ->viewAdmin('admin.master.part-data-excel-stock.show-mutation-histories', [
                'title' => 'Mutation Part Data Stock',
            ]);
    }

    public function leftOverStock()
    {
        $data = GeneralJournalDetail::select([
            DB::raw('(MIN(YEAR(general_journal_details.date))) AS min_year'),
            DB::raw('(MAX(YEAR(general_journal_details.date))) AS max_year')
        ])->first();
        $min_month = 1;
        $max_month = 12;
        $min_year = $data->min_year;
        $max_year = $data->max_year;
        return $this
            ->setBreadcrumb('Left Over Part Data Stock', '#')
            ->viewAdmin('admin.master.part-data-excel-stock.leftover-stock', [
                'title' => 'Left Over Part Data Stock',
                'min_month' => $min_month,
                'max_month' => $max_month,
                'min_year' => $min_year,
                'max_year' => $max_year,
            ]);
    }

    public function exportData(Request $request) {
        $q = PartDataExcelStock::orderBy('id', 'ASC');
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
            $date_to = Carbon::parse($request->date_to)->format('Y-m-d');
            $q->where('created_at', '>=', $date_from)
            ->where('created_at', '<=', $date_to);
        }
        $q = $q->get();
        // return $q;
        $data = [];
        foreach($q as $row) {
            $data[] = [
                "asc_name" => $row->asc_name,
                "code_material" => $row->code_material,
                "part_description" => $row->part_description,
                "stock" => $row->stock,
                "created_at" => Carbon::parse($row->created_at)->format('Y-m-d'),
            ];
        }

        return (new FastExcel($data))->download('Export_Part_Data_Excel_Stock_'.$request->date_from.'-'.$request->date_to.'.xlsx');
    }
}
