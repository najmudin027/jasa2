<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Auth;

class DevelopmentProgramInfoController extends AdminController
{
    public function index()
    {
       return $this
            ->setBreadcrumb('Education', '/admin/development/show')
            ->viewAdmin('admin.setting.educations.development.index',[
                'title' => 'List Educations',
            ]);
    }
}
