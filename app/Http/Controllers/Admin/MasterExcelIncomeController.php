<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\Master\ExcelIncome;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Model\Master\ExcelIncomeDetail;
use Box\Spout\Writer\Style\StyleBuilder;
use App\Http\Controllers\AdminController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Services\Master\ExcelIncomeService;
use App\Model\Master\ExcelIncomeDetailDescription;
use function App\Helpers\thousanSparator;

class MasterExcelIncomeController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master Excel Income '
        ])
            ->setBreadcrumb('List Excel Income', '/admin/excel-income/show')
            ->viewAdmin('admin.master.excel-income.index');
    }


    public function create(ExcelIncomeService $service)
    {
        return $this->setData([
            'title' => 'Create Master Excel Income',
            'code' => $service->generateCode()
        ])
            ->setBreadcrumb('List Excel Income', '/admin/excel-income/show')
            ->viewAdmin('admin.master.excel-income.create');
    }

    public function update($id)
    {
        $getData = ExcelIncome::where('id', $id)->first();
        $detailsData = ExcelIncomeDetail::with('details_desc')->where('excel_income_id', $id)->get();

        return $this->setData([
            'title' => 'Update Master Excel Income',
            'getData' => $getData,
            'detailsData' => $detailsData
        ])
            ->setBreadcrumb('List Excel Income', '/admin/excel-income/show')
            ->viewAdmin('admin.master.excel-income.update');
    }

    public function detail($id)
    {
        $getData = ExcelIncome::where('id', $id)->first();
        $detailsData = ExcelIncomeDetail::where('excel_income_id', $id)->get();

        return $this->setData([
            'title' => 'Detail Master Excel Income',
            'getData' => $getData,
            'detailsData' => $detailsData
        ])
            ->setBreadcrumb('Detail Excel Income', '/admin/excel-income/detail')
            ->viewAdmin('admin.master.excel-income.detail');
    }

    public function addDetail($id)
    {
        $getData = ExcelIncome::where('id', $id)->first();
        return $this->setData([
            'title' => 'Add Detail Master Excel Income',
            'getData' => $getData
        ])
            ->setBreadcrumb('Add Detail Excel Income', '/admin/excel-income/add_detail/'.$id.'')
            ->viewAdmin('admin.master.excel-income.add_detail');
    }
    public function editDetail($detail_id)
    {

        $getDetail = ExcelIncomeDetail::with('details_desc')->where('id', $detail_id)->first();
        $getMaster = ExcelIncome::where('id', $getDetail->excel_income_id)->first();
        return $this->setData([
            'title' => 'Edit Detail Master Excel Income',
            'getDetail' => $getDetail,
            'getMaster' => $getMaster
        ])
            ->setBreadcrumb('Edit Detail Excel Income', '/admin/excel-income/edit_detail/'.$detail_id.'')
            ->viewAdmin('admin.master.excel-income.edit_detail');
    }

    public function logs()
    {
        return $this->setData([
            'title' => 'Excel Income Logs (Deleted Only)'
        ])
            ->setBreadcrumb('List Excel Income', '/admin/excel-income/logs')
            ->viewAdmin('admin.master.excel-income.logs');
    }

    public function getValueDetails($details_data, $field, $i) {
        if($details_data != null && $details_data[$i-1]->{$field} !== "") {
            $val = ($field == 'date_transfer' || $field == 'date_transaction') ? Carbon::parse($details_data[$i-1]->{$field})->format('d-m-Y') : $details_data[$i-1]->{$field};
        } else {
            $val = null;
        }
        return $val;
    }

    public function exportDetailExcel(Request $request, $id) {
        $data_excel = ExcelIncomeDetail::with('details_desc','excel_income')->where('excel_income_id', $id)->get();
        header('Content-Type: application/vnd.ms-excel');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getSheetView()->setZoomScale(80);
        $sheet->setShowGridlines(false);
        if(count($data_excel) > 0) {
            $sheet->getStyle("C1:I1")->getFont()->setSize(22)->setBold(true)->setName('Arial');
            $sheet->mergeCells('C1:I1');
            $sheet->setCellValue('C1',$data_excel[0]->excel_income->desc. ' '.$data_excel[0]->excel_income->month. ' '.$data_excel[0]->excel_income->year.' - CODE('. $data_excel[0]->excel_income->code.')');
            $sheet->getStyle("C1")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $sheet->getColumnDimension('A')->setWidth(2);
            $sheet->getColumnDimension('B')->setWidth(2);
            $sheet->getColumnDimension('C')->setWidth(15);
            $sheet->getColumnDimension('D')->setWidth(20);
            $sheet->getColumnDimension('E')->setWidth(50);
            $sheet->getColumnDimension('F')->setWidth(15);
            $sheet->getColumnDimension('G')->setWidth(20);
            $sheet->getColumnDimension('H')->setWidth(20);
            $sheet->getColumnDimension('I')->setWidth(25);
            $sheet->setCellValue('C3', 'Date Transaction')->getStyle('C3')->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('D3', 'Source')->getStyle('D3')->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('E3', 'Description')->getStyle('E3')->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('F3', 'Date Transfer')->getStyle('F3')->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('G3', 'In')->getStyle('G3')->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('H3', 'Refund')->getStyle('H3')->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('I3', 'Branch')->getStyle('I3')->getFont()->setSize(12)->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle('C3:I3')
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->getStyle('C3:I3')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
            $sheet->getStyle('C3:I3')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('2F75B5');

            foreach($data_excel as $key => $val) {

                $desc = "";
                foreach($val->details_desc as $key2=> $valdesc) {
                    $desc .="".($key2+1).".".$valdesc->description."\n"." ";
                }
                $row = $key + 4;

                $sheet->setCellValue('C'.$row, Carbon::parse($val->date_transaction)->format('d-m-Y'))->getStyle('C'.$row);
                $sheet->setCellValue('D'.$row, $val->source)->getStyle('D'.$row);
                $sheet->setCellValue('E'.$row, $desc)->getStyle('E'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('DDEBF7');
                $sheet->setCellValue('F'.$row, Carbon::parse($val->date_transfer)->format('d-m-Y'))->getStyle('F'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('DDEBF7');
                $sheet->setCellValue('G'.$row, 'Rp. '.($val->in_nominal != 0 ? thousanSparator($val->in_nominal) : 0))->getStyle('G'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('FFFF00');
                $sheet->setCellValue('H'.$row, 'Rp. '.($val->refund != 0 ? thousanSparator($val->refund) : 0))->getStyle('H'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('C6E0B4');
                $sheet->setCellValue('I'.$row, $val->branch)->getStyle('I'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('DDEBF7');
                $styleArray = [
                    'alignment' => [
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ]
                ];
                $sheet->getStyle('C'.$row)->applyFromArray($styleArray);
                $sheet->getStyle('D'.$row)->applyFromArray($styleArray);
                $sheet->getStyle('E'.$row)->getAlignment()->setWrapText(true)->applyFromArray($styleArray);
                $sheet->getStyle('F'.$row)->applyFromArray($styleArray);
                $sheet->getStyle('G'.$row)->applyFromArray($styleArray);
                $sheet->getStyle('H'.$row)->applyFromArray($styleArray);
                $sheet->getStyle('I'.$row)->applyFromArray($styleArray);

            }

            $row_after_load = 4 + count($data_excel);

            $sheet->setCellValue('C'.$row_after_load, 'Ending');
            $sheet->setCellValue('D'.$row_after_load, '');
            $sheet->setCellValue('E'.$row_after_load, '');
            $sheet->setCellValue('F'.$row_after_load, '');
            $sheet->setCellValue('G'.$row_after_load, 'Rp. '.thousanSparator($data_excel[0]->excel_income->total_in_nominal));
            $sheet->setCellValue('H'.$row_after_load, 'Rp. '.thousanSparator($data_excel[0]->excel_income->total_refund));
            $sheet->setCellValue('I'.$row_after_load, 'Rp. '.thousanSparator(($data_excel[0]->excel_income->total_in_nominal - $data_excel[0]->excel_income->total_refund)));
            $spreadsheet->getActiveSheet()->getStyle('C4:I'.($row_after_load))
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $row_info = $row_after_load+2;
            $sheet->setCellValue('D'.$row_info, 'Legenda')->getStyle('D'.$row_info)->getFont()->setSize(18)->setBold(true);
            $sheet->getStyle('D'.$row_info)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('D'.($row_info+1), 'Date Transaction');
            $sheet->setCellValue('D'.($row_info+2), 'Source');
            $sheet->setCellValue('D'.($row_info+3), 'Description');
            $sheet->setCellValue('D'.($row_info+4), 'Date Transfer');
            $sheet->setCellValue('D'.($row_info+5), 'In');
            $sheet->setCellValue('D'.($row_info+6), 'Refund');
            $sheet->setCellValue('E'.$row_info, '');
            $sheet->getStyle('E'.$row_info)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('E'.($row_info+1), 'Tanggal transaksi job dilakukan');
            $sheet->setCellValue('E'.($row_info+2), 'Nama customer');
            $sheet->setCellValue('E'.($row_info+3), 'Transaksi yang dilakukan');
            $sheet->setCellValue('E'.($row_info+4), 'Tanggal Transfer ke rekening kantor');
            $sheet->setCellValue('E'.($row_info+5), 'Nominal Transaksi yang ditransfer');
            $sheet->setCellValue('E'.($row_info+6), 'Nominal Pengembalian uang');

            header('Content-Disposition: attachment; filename="Excel_Income_Details_'.$data_excel[0]->excel_income->month.'_'.$data_excel[0]->excel_income->year.'_.xls"');

        } else {
            header('Content-Disposition: attachment; filename="Excel_Income_Details_0.xls"');
            $sheet->setCellValue('A1','Data Detail Kosong!');
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save("php://output");

    }

    public function exportData(Request $request) {

        $query = ExcelIncome::with('excel_income_detail');
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $query->whereHas('excel_income_detail', function ($q) use ($request) {
                $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
                $date_to = Carbon::parse($request->date_to)->format('Y-m-d');
                $q->where('date_transaction', '>=', $date_from)
                ->where('date_transaction', '<=', $date_to);
            });
        }
        $query = $query->get();
        // return $q;
        $data = [];

        foreach($query as $row) {
            $details_data = [];
            $count_detail = count($row->excel_income_detail);
            foreach(range(1,$count_detail) as $val) {
                $descs = ExcelIncomeDetailDescription::where('excel_income_detail_id', $this->getValueDetails($row->excel_income_detail, 'id', $val))->get();
                $text_desc = "";
                foreach ($descs as $desc) {
                    $text_desc .= "".$desc->description."\n"."";
                }
                $details_data['date_transaction_'.$val.''] = $this->getValueDetails($row->excel_income_detail, 'date_transaction', $val);
                $details_data['description_'.$val.''] = $text_desc;
                $details_data['date_transfer_'.$val.''] = $this->getValueDetails($row->excel_income_detail, 'date_transfer', $val);
                $details_data['in_nominal_'.$val.''] = $this->getValueDetails($row->excel_income_detail, 'in_nominal', $val);
                $details_data['refund_'.$val.''] = $this->getValueDetails($row->excel_income_detail, 'refund', $val);
                $details_data['branch_'.$val.''] = $this->getValueDetails($row->excel_income_detail, 'branch', $val);

            }

            $ei_data = [
                "code" => $row['code'],
                "title" => $row['title'],
                "description" => $row['description'],
                "created_at" => Carbon::parse($row['created_at'])->format('d-m-Y')
            ];

            $data [] = array_merge($ei_data, $details_data);
        }

        return (new FastExcel($data))->download('Export_Excel_Income_Data_'.$request->date_from.'-'.$request->date_to.'.xlsx');
    }


}
