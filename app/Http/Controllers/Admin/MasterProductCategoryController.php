<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use App\Model\Master\MsProductCategory;
use App\Http\Controllers\AdminController;
use App\Services\Master\ProductCategoryService;

class MasterProductCategoryController extends AdminController
{

    public function showProductCategory(Request $request)
    {
        $showData = MsProductCategory::orderBy('order')->get();
		$cat = new ProductCategoryService;
        $cat = $cat->getHTML($showData);

        // return $showData;
        return $this
        ->setBreadcrumb('List Product Category', '/admin/product-category/show')
        ->viewAdmin('admin.master.product.product-category.index', compact('showData','cat'));
    }

    public function createProductCategory()
    {
        return $this
        ->setBreadcrumb('List Product Category', '/admin/product-category/show')
        ->setBreadcrumb('Create Product Category', '#')
        ->viewAdmin('admin.master.product.product-category.create');
    }

    public function updateProductCategory($id)
    {
        $editProductCategory = MsProductCategory::findOrFail($id);
        return $this
        ->setBreadcrumb('List Product Category', '/admin/product-category/show')
        ->setBreadcrumb('Update Product Category', '#')
        ->viewAdmin('admin.master.product.product-category.update', compact('editProductCategory'));
    }
}
