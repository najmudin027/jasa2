<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Model\Master\PartDataExcelStock;
use Box\Spout\Writer\Style\StyleBuilder;
use App\Http\Controllers\AdminController;
use function App\Helpers\thousanSparator;
use App\Model\Master\GeneralJournalDetail;
use App\Model\Master\PartDataExcelStockInventory;

class MasterPartDataExcelStockInventoryController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master Part Data Excel Stock Inventories '
        ])
            ->setBreadcrumb('List Part Data Excel Stock Inventories', '/admin/part-data-excel-stock-inventory/show')
            ->viewAdmin('admin.master.part-data-excel-stock-inventories.index');
    }

    public function masterSellingIndex()
    {
        return $this->setData([
            'title' => 'Master Selling Price Stock Inventories '
        ])
            ->setBreadcrumb('Master Selling Price Stock Inventories', '/admin/part-data-excel-stock-inventory/master-selling-price')
            ->viewAdmin('admin.master.part-data-excel-stock-inventories.master-selling-price');
    }


    public function create()
    {
        return $this->setData([
            'title' => 'Create Master Part Data Excel Stock Inventories',
        ])
            ->setBreadcrumb('List Part Data Excel Stock Inventories', '/admin/part-data-excel-stock-inventory/show')
            ->viewAdmin('admin.master.part-data-excel-stock-inventories.create');
    }

    public function update($id)
    {
        $getData = PartDataExcelStockInventory::with('part_data_stock.asc')->where('id', $id)->first();


        return $this->setData([
            'title' => 'Update Master Part Data Excel Stock Inventories',
            'getData' => $getData,

        ])
            ->setBreadcrumb('List Part Data Excel Stock Inventories', '/admin/part-data-excel-stock-inventory/show')
            ->viewAdmin('admin.master.part-data-excel-stock-inventories.update');
    }

    public function detail($id)
    {
        $getData = PartDataExcelStockInventory::where('id', $id)->first();
        return $this->setData([
            'title' => 'Detail Master Part Data Excel Stock Inventories',
        ])
            ->setBreadcrumb('Detail Part Data Excel Stock Inventories', '/admin/part-data-excel-stock-inventory/detail')
            ->viewAdmin('admin.master.part-data-excel-stock-inventories.detail');
    }

    public function historyInOutPdesi($id)
    {
        $pdesi = PartDataExcelStockInventory::where('id', $id)->firstOrFail();
        return $this
            ->setBreadcrumb('List Part Data Excel Stock Inventory', '/admin/part-data-excel-stock-inventory/show')
            ->viewAdmin('admin.master.part-data-excel-stock-inventories.history-in-out', [
                'title' => 'History In Out Part Data Excel Stock Inventory',
                'pdesi' => $pdesi
            ]);
    }


    public function leftOverStock()
    {
        $data = GeneralJournalDetail::select([
            DB::raw('(MIN(YEAR(general_journal_details.date))) AS min_year'),
            DB::raw('(MAX(YEAR(general_journal_details.date))) AS max_year')
        ])->first();
        $min_month = 1;
        $max_month = 12;
        $min_year = $data->min_year;
        $max_year = $data->max_year;
        return $this
            ->setBreadcrumb('Left Over Part Data Stock Inventories', '#')
            ->viewAdmin('admin.master.part-data-excel-stock-inventories.leftover-stock', [
                'title' => 'Left Over Part Data Stock',
                'min_month' => $min_month,
                'max_month' => $max_month,
                'min_year' => $min_year,
                'max_year' => $max_year,
            ]);
    }

}
