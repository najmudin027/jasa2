<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Model\Master\MsAdditionalInfo;
use App\Model\Master\TeknisiInfo;
use App\Model\Master\MsAddress;
use App\User;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends AdminController
{
    public function showProfile()
    {
        $this->setBreadcrumb('List Additionals', route('profile.show.index'));

        $user = User::with(['info.religion', 'info.marital'])
            ->where('id', Auth::id())
            ->first();

        if ($user->info == null) {
            $view = 'admin.setting.profile.profileCreate';
        } else {
            $view = 'admin.setting.profile.profile';
        }

        return $this->viewAdmin('admin.customer.profile.profile', [
            'title' => 'Profile',
            'user'  => $user,
        ]);
    }

    public function preview()
    {
        $additionalInfo = MsAdditionalInfo::where('user_id', Auth::user()->id)->with(['religion', 'marital'])->first();
        $teknisiInfo = TeknisiInfo::where('user_id', Auth::user()->id)->first();
        $address = MsAddress::where('user_id', Auth::user()->id)->with(['city', 'types'])->first();
        return $this->viewAdmin('users.teknisiInfoUpdate', [
            'additionalInfo' => $additionalInfo,
            'teknisiInfo' => $teknisiInfo,
            'address' => $address,
        ]);
    }

    public function profileInfoEdit()
    {
        return $this->viewAdmin('admin.customer.profile.info', [
            'title' => 'EDIT INFO',
            'user'  => Auth::user(),
             'info'  => MsAdditionalInfo::where('user_id', Auth::id())->first(),
        ]);
    }

    public function profileCreateAddress()
    {
        return $this->viewAdmin('admin.customer.profile.create_address', [
            'title' => 'CREATE ADDRESS',
            'user'  => Auth::user(),
            'info'  => MsAdditionalInfo::where('user_id', Auth::id())->first(),
        ]);
    }

    public function profileEditAddress($id)
    {
        return $this->viewAdmin('admin.customer.profile.edit_address', [
            'title'  => 'EDIT INFO',
            'user'   => Auth::user(),
            'addres' => MsAddress::where('user_id', Auth::id())->where('id', $id)->first(),
        ]);
    }

    public function updateProfile($id)
    {
        $userLogin  = Auth::user()->id;
        $detailUser = User::with(['info.religion', 'info.marital'])->where('id', $userLogin)->first();
        return $this->viewAdmin('admin.setting.profile.profileUpdate', compact('detailUser'));
    }

    // address
    public function listCreate()
    {
        return $this->viewAdmin('admin.setting.listAddress.listAddress');
    }

    public function updateAddress($id)
    {
        $updateAddress = MsAddress::where('id', $id)->with(['city','city.province','city.province.country', 'district', 'village', 'zipcode', 'types'])->first();
        // return $updateAddress;
        return $this->viewAdmin('admin.setting.listAddress.updateAddress', compact('updateAddress'));
    }

    public function adminUpdateAddress($id)
    {
        $updateAddress = MsAddress::where('id', $id)->with(['city','city.province','city.province.country', 'district', 'village', 'zipcode', 'types'])->first();
        // return $updateAddress;
        return $this->viewAdmin('admin.setting.listAddress.adminUpdateAddress', compact('updateAddress'));
    }

    public function addressCreate()
    {
        return $this->viewAdmin('admin.setting.listAddress.addresCreate');
    }

    public function detailAddress($id)
    {
        $detailAddress = MsAddress::where('id', $id)->with(['city.province.country', 'district', 'village', 'zipcode', 'types'])->first();
        // return $detailAddress;
        return $this->viewAdmin('admin.setting.listAddress.detailAddress', compact('detailAddress'));
    }

    public function teknisiInfo()
    {
        return $this->viewAdmin('users.teknisiInfo');
    }

    public function infoUpdate(Request $request, $id)
    {
        $infoUpdate = User::with(['teknisidetail', 'teknisidetail.devPlant'])->where('id', Auth::user()->id)->firstOrFail();
        // return $infoUpdate;
        // return $this->successResponse([$user,$request->note], 'success Rejected');
        return $this->viewAdmin('users.teknisiInfoUpdate', compact('infoUpdate'));
    }

    public function usersInfo()
    {
        return $this->viewAdmin('users.userInfo');
    }

    public function teknisiAddress()
    {
        return $this->viewAdmin('admin.setting.listAddress.listAddressTeknisi');
    }
}
