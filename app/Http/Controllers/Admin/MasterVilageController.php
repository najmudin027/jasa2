<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsVillage;

class MasterVilageController extends AdminController
{
    public function showVilage()
    {
        return $this
        ->setBreadcrumb('List Villages', '/admin/village/show')
        ->viewAdmin('admin.master.location.vilage.index', [
            'title' => 'MASTER VILLAGE'
        ]);
    }

    public function createVilage()
    {
        return $this
        ->setBreadcrumb('List Villages', '/admin/village/show')
        ->setBreadcrumb('Create Villages', '/admin/village/create')
        ->viewAdmin('admin.master.location.vilage.create');
    }

    public function updateVilage($id)
    {
        $editVilage = MsVillage::with('district.city.province.country','district.city.province','district.city','district')->where('id', $id)->first();
        // return $editVilage;
        return $this
        ->setBreadcrumb('List Villages', '/admin/village/show')
        ->setBreadcrumb('Update Villages', '/admin/village/create')
        ->viewAdmin('admin.master.location.vilage.update', compact('editVilage'));
    }
}
