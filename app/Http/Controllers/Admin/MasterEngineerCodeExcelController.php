<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class MasterEngineerCodeExcelController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master Engineer Code Excel'
        ])
        ->setBreadcrumb('List Master Engineer Code Excel', '/admin/engineer-code-excel/show')
        ->viewAdmin('admin.master.engineer-code-excel.index');
    }
}
