<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsProvince;

class MasterProvinceController extends AdminController
{
    public function showProvince()
    {
        return $this
        ->setBreadcrumb('List Provice', '/admin/province/show')
        ->viewAdmin('admin.master.location.province.index', [
            'title' => 'MASTER PROVINCE'
        ]);
    }

    public function createProvince()
    {
        return $this
        ->setBreadcrumb('List Provice', '/admin/province/show')
        ->setBreadcrumb('Create Provice', '/admin/province/create')
        ->viewAdmin('admin.master.location.province.create',[
            'title' => 'CREATE PROVINCE'
        ]);
    }

    public function updateProvince($id)
    {
        $editProvince = MsProvince::with('country')->where('id', $id)->first();
        return $this
        ->setBreadcrumb('List Provice', '/admin/province/show')
        ->setBreadcrumb('Update Provice', '/admin/province/update')
        ->viewAdmin('admin.master.location.province.update', compact('editProvince'));
    }
}
