<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Badge;

class MasterBadgeController extends AdminController
{
    public function showBadge()
    {
        return $this
            ->setBreadcrumb('List Badges', '/admin/badge/show')
            ->viewAdmin('admin.master.badge.index', [
                'title' => 'Master Badge'
            ]);
    }

    public function createBadge()
    {
        return $this
            ->setBreadcrumb('List Badges', '/admin/badge/show')
            ->setBreadcrumb('Create Badges', '#')
            ->viewAdmin('admin.master.badge.create', [
                'title' => 'Create Badge'
            ]);
    }

    public function updateBadge($id)
    {
        $updateBadge = Badge::where('id', $id)->first();
        return $this
            ->setBreadcrumb('List Badges', '/admin/badge/show')
            ->setBreadcrumb('Update Badges', '#')
            ->viewAdmin('admin.master.badge.update', compact('updateBadge'));
    }
}
