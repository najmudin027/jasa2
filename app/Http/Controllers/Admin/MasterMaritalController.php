<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Setting\Marital;

class MasterMaritalController extends AdminController
{
    public function showListMarital()
    {
        return $this
        ->setBreadcrumb('list Maritals', '/admin/marital/show')
        ->viewAdmin('admin.setting.maritals.index', ['title' => 'list Maritals']);
    }

    public function createMarital()
    {
        return $this
        ->setBreadcrumb('list Maritals', '/admin/marital/show')
        ->setBreadcrumb('Create Maritals', '#')
        ->viewAdmin('admin.setting.maritals.create');
    }

    public function updateMarital($id)
    {
        $updateMarital = Marital::where('id', $id)->first();
        return $this
        ->setBreadcrumb('list Maritals', '/admin/marital/show')
        ->setBreadcrumb('Update Maritals', '#')
        ->viewAdmin('admin.setting.maritals.update', compact('updateMarital'));
    }
}
