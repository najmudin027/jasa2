<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class MasterTypeAddressController extends AdminController
{
    public function showListAddressType()
    {
        return $this
            ->setBreadcrumb('List Master Type', '/product')
            ->viewAdmin('admin.setting.typeAddress.index', [
                'title' => 'Master Type Address'
            ]);
    }
}
