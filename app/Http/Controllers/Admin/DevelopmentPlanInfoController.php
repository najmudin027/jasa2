<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\Educations;
use App\User;
use Auth;

class DevelopmentPlanInfoController extends AdminController
{
    public function index()
    {
        $getUser = User::get();
        // return $getUser;
       return $this
            ->setBreadcrumb('Education', '/admin/devPlant/show')
            ->viewAdmin('admin.setting.educations.developmentPlant.index',[
                'title' => 'List Curriculum',
                'getUser' => $getUser
            ]);
    }
}
