<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiController;
use DB;
use App\User;
use Carbon\Carbon;
use App\Model\Master\Order;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use App\Model\Master\Rating;
use App\Model\Master\Review;
use App\Model\GeneralSetting;
use Illuminate\Http\Request;
use App\Model\Master\HistoryOrder;
use App\Model\Master\Ewallet;
use App\Services\UserService;
use App\Model\Master\MsTicket;
use App\Model\Master\Services;
use App\Services\OrderService;
use App\Model\Master\MsAddress;
use App\Model\Master\MsProduct;
use App\Model\Master\MsSymptom;
use App\Model\Master\ItemDetail;
use App\Helpers\CollectionHelper;
use App\Model\Master\TopupWallet;
use App\Model\Master\ServiceDetail;
use App\Model\Master\BankTransfer;
use App\Model\Master\EwalletHistory;
use App\Model\Master\EwalletPayment;
use App\Model\Master\MsPriceService;
use App\Model\Master\MsServicesType;
use App\Model\Master\TransactionHistory;
use App\Model\Technician\Technician;
use Illuminate\Support\Facades\Auth;
use App\Model\Master\RatingAndReview;
use App\Model\Master\SparepartDetail;
use App\Model\Master\TechnicianSaldo;
use App\Model\Master\TmpSparepartDetail;
use App\Http\Requests\UserInfoRequest;
use App\Model\Master\MsOrderComplaint;
use App\Services\Master\TicketService;
use App\Http\Requests\ComplaintRequest;
use Illuminate\Support\Facades\Session;
use App\Model\Master\TechnicianSchedule;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\AdminController;
use App\Http\Requests\RatingStoreRequest;
use App\Http\Requests\UserAddressRequest;
use App\Model\Master\TechnicianSparepart;
use App\Http\Requests\Master\TicketRequest;
use App\Events\CustomerRequestPenawaranEvent;
use App\Http\Requests\AvailableHoursRequest;
use App\Http\Requests\UserProfileImageRequest;
use App\Http\Requests\ComplaintReplyRequest;
use App\Services\Master\ChatService;
use App\Services\Master\OrderComplaintService;
use App\Services\NotifikasiService;
use App\Services\Technician\TechnicianService;
use App\Model\Master\MsAdditionalInfo;
use App\Model\Master\TeknisiInfo;

class CustomerController extends AdminController
{

    public function showDataLessBalance(OrderService $service, ServiceDetail $orders)
    {
        return $this->successResponse($orders);
    }

    public function showChangeAdditionalParts(OrderService $service, $tmp_parts)
    {
        // return 'test';
        $getTmp   = Order::with([
            'user.masterwallet',
            'orderpayment.orderhistory',
            'symptom',
            'service',
            'order_status',
            'tmp_sparepart',
            'item_detail',
            'sparepart_detail',
            'tmp_item_detail',
            'service_detail.symptom',
            'service_detail.services_type',
        ])->where('id', $tmp_parts)->first();
        // $getTmp = Order::where('orders_id', $tmp_parts)->get();
        return $this->successResponse($getTmp);
    }

    public function dashboard()
    {
        $additionalInfo = MsAdditionalInfo::where('user_id', Auth::user()->id)->with(['religion', 'marital'])->first();
        $teknisiInfo = TeknisiInfo::where('user_id', Auth::user()->id)->first();
        $address = MsAddress::where('user_id', Auth::user()->id)->with(['city', 'types'])->first();
        // return $address;
        $getTitle = GeneralSetting::where('name', 'term_order_completed_title')->first();
        $getDesc = GeneralSetting::where('name', 'term_order_completed_desc')->first();

        $orderThisMonth = Order::where('users_id', Auth::user()->id)
            ->whereMonth('created_at', '=', date("m"))
            ->sum('grand_total');

        $completed = Order::where('users_id', Auth::user()->id)
            ->where('orders_statuses_id', 10)
            ->whereMonth('created_at', '=', date("m"))
            ->sum('grand_total');

        $walletBalanceCustomer = Ewallet::where('user_id', Auth::user()->id)->first();

        if (Auth::user()->is_login_as_teknisi == 1) {
            return $this->viewAdmin('admin.customer.dashboard', [
                'title'              => 'DASHBOARD',
                'orderThisMonth'     => $orderThisMonth,
                'completed'          => $completed,
                'walletBalanceCustomer' => $walletBalanceCustomer,
                'getTitle' => $getTitle,
                'getDesc' => $getDesc,
                'additionalInfo' => $additionalInfo,
                'teknisiInfo' => $teknisiInfo,
                'address' => $address,
            ]);
        }

        if (Auth::user()->is_login_as_teknisi == null) {
            return $this->viewAdmin('admin.customer.dashboard', [
                'title'              => 'DASHBOARD',
                'orderThisMonth'     => $orderThisMonth,
                'completed'          => $completed,
                'walletBalanceCustomer' => $walletBalanceCustomer,
                'getTitle' => $getTitle,
                'getDesc' => $getDesc,
                'additionalInfo' => $additionalInfo,
                'teknisiInfo' => $teknisiInfo,
                'address' => $address,
            ]);
        }
    }

    public function datatablesRequestJob($order_id)
    {
        $query = ServiceDetail::with('order', 'symptom', 'service_status', 'services_type', 'technician')
            ->whereHas('order', function ($q) {
                $q->where('users_id', Auth::user()->id);
            })
            ->where('orders_id', $order_id);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatables($query = null, Request $request)
    {
        if ($query == null) {
            $type_id = $request->type_id;
            $is_less_balance = $request->is_less_balance;
            $query   = Order::with([
                'user.masterwallet',
                'orderpayment.orderhistory',
                'symptom',
                'garansi',
                'product_group',
                'service',
                'order_status',
                'sparepart_detail',
                'tmp_sparepart',
                'tmp_item_detail',
                'item_detail',
                'service_detail.symptom',
                'service_detail.services_type',
            ])->where('users_id', Auth::user()->id);

            if ($type_id != '') {
                $query = $query->where('orders_statuses_id', $type_id)->orderBy('id', 'DESC')->get();
            }
            if ($is_less_balance != '') {
                $query = $query->where('is_less_balance', 1)->orderBy('id', 'DESC')->get();
            }
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatablesAll($query = null, Request $request)
    {
        $query = Order::with([
            'user',
            'symptom',
            'service',
            'order_status',
            'service_detail.symptom',
            'service_detail.services_type',
        ])
            ->where('users_id', Auth::user()->id);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function request_list(Request $request)
    {
        // dd($request->all());
        $sparepart_details = SparepartDetail::where('orders_id', $request->id)
            ->with([
                'order',
                'technician_sparepart',
            ])->orderBy('id', 'DESC')->get();

        $getTitle = GeneralSetting::where('name', 'term_order_completed_title')->first();
        $getDesc = GeneralSetting::where('name', 'term_order_completed_desc')->first();

        $item_details = ItemDetail::where('orders_id', $request->id)
            ->with(['order'])->orderBy('id', 'DESC')->get();
        // return $order;
        return $this
            ->viewAdmin('admin.customer.job_request.request_list', [
                'title' => 'LIST REQUEST',
                'sparepart_details' => $sparepart_details,
                'item_details' => $item_details,
                'getTitle' => $getTitle,
                'getDesc' => $getDesc,
            ]);
    }

    public function job_request_create()
    {
        $getSettingDesc = GeneralSetting::where('name', 'agreement_order_text')->first();
        $getSettingTitle = GeneralSetting::where('name', 'agreement_order_title')->first();
        $addresses = Auth::user()->addresses;
        $getTitle = GeneralSetting::where('name', 'agreement_order_title')->first();
        $getDesc = GeneralSetting::where('name', 'agreement_order_text')->first();

        if (count($addresses) == 0) {
            return redirect('/customer/profile/address/create');
        }

        return $this
            ->viewAdmin('admin.customer.job_request.create', [
                'title'         => 'CREATE REQUEST',
                'addresses'     => $addresses,
                'getSettingDesc'     => $getSettingDesc,
                'getSettingTitle'     => $getSettingTitle,
                'main_address'  => $addresses->where('is_main', 1)->first(),
                'getWalletUser' => Ewallet::where('user_id', Auth::user()->id)->first(),
                'getTitle' => $getTitle,
                'getDesc' => $getDesc,
            ]);
    }

    public function new_job_request_create()
    {
        $getSettingDesc = GeneralSetting::where('name', 'agreement_order_text')->first();
        $getSettingTitle = GeneralSetting::where('name', 'agreement_order_title')->first();
        $addresses = Auth::user()->addresses;
        $getTitle = GeneralSetting::where('name', 'agreement_order_title')->first();
        $getDesc = GeneralSetting::where('name', 'agreement_order_text')->first();

        if (count($addresses) == 0) {
            return redirect('/customer/profile/address/create');
        }

        return $this
            ->viewAdmin('admin.customer.job_request.new_create', [
                'title'         => 'CREATE REQUEST',
                'addresses'     => $addresses,
                'getSettingDesc'     => $getSettingDesc,
                'getSettingTitle'     => $getSettingTitle,
                'main_address'  => $addresses->where('is_main', 1)->first(),
                'getWalletUser' => Ewallet::where('user_id', Auth::user()->id)->first(),
                'getTitle' => $getTitle,
                'getDesc' => $getDesc,
            ]);
    }

    public function job_request_create_json(Request $request, ApiController $apiController)
    {
        $data              = [];
        $addresses         = MsAddress::with('types')->where('user_id', $request->user_id);
        $data['service']   = Services::where('id', $request->service_id)->firstOrFail();
        $data['symptoms']  = MsSymptom::where('ms_services_id', $request->service_id)->get();
        $data['addresses'] = $addresses->count() != 0 ? $addresses->get() : null;

        $content = view('admin.master.transactions._request_form_template', ['data' => $data])->render();

        // send response
        return $apiController->successResponse([
            'content' => $content,
            'data'    => $data,
        ], 'success', 201);
    }

    public function service_detail($order_id)
    {
        // dd('asd');
        $garansi_setting = GeneralSetting::where('name', 'garansi_due_date')->first();

        $order = Order::where('id', $order_id)
            ->with([
                'user.masterwallet',
                'orderpayment.orderhistory',
                'service',
                'symptom',
                'order_status',
                'sparepart_detail',
                'item_detail',
                'tmp_sparepart',
                'tmp_item_detail',
                'service_detail.symptom',
                'service_detail.services_type',
                'service_detail.technician.user',
                'service_detail.price_service',
                'history_order.order_status'
            ])
            ->where('users_id', Auth::id())
            ->firstOrFail();
        // return $order;
        $getTitle = GeneralSetting::where('name', 'term_order_completed_title')->first();
        $getDesc = GeneralSetting::where('name', 'term_order_completed_desc')->first();

        $order->markAsRead();

        $price_service_id = $order->service_detail->sum('price');
        $sparepart_details = $order->sparepart_detail;
        $item_details = $order->item_detail;


        $saldo  = $price_service_id;
        $grand_total = 0; //

        if (!empty($order->sparepart_detail)) {
            foreach ($order->sparepart_detail as $sd) {
                $grand_total += $sd->price * $sd->quantity;
            }
        } elseif (!empty($order->item_detail)) {
            foreach ($order->item_detail as $it) {
                $grand_total += $it->price * $it->quantity;
            }
        } elseif (!empty($order->tmp_item_detail)) {
            foreach ($order->tmp_item_detail as $tid) {
                $grand_total += $tid->price * $tid->quantity;
            }
        } elseif (!empty($order->tmp_sparepart)) {
            foreach ($order->tmp_sparepart as $ts) {
                $grand_total += $ts->price * $ts->quantity;
            }
        }

        if ($order->user->ewallet !== null) {
            $difference  =  abs($grand_total - $order->user->ewallet->nominal);
        }

        $tanggalSekarang = date('Y-m-d H:i:s'); //date('2020-08-13 04:00:00');
        $tanggalvisit = carbon::parse($order->schedule)->subHours('3')->format('Y-m-d H:i:s');
        $diff    = strtotime($tanggalvisit) - strtotime($tanggalSekarang);
        $jam     = floor($diff / (60 * 60));

        return $this
            ->viewAdmin('admin.customer.job_request.service_detail', [
                'title'             => 'DETAIL TRANSACTION CUSTOMER',
                'order'             => $order,
                'order_id'          => $order_id,
                'garansi_setting' => $garansi_setting,
                'sparepart_details' => $sparepart_details,
                'item_details'      => $item_details,
                'diff' => $diff,
                'jam' => $jam,
                'difference' => $difference,
                'grand_total' => $grand_total,
                'getTitle' => $getTitle,
                'getDesc' => $getDesc
            ]);
    }

    public function getHoursAvailable(AvailableHoursRequest $request, OrderService $orderService)
    {
        return $orderService->getHoursAvailable($request);
    }

    public function cancelOrder(Request $request, $id, OrderService $orderService)
    {
        $resp = $orderService->setOrder($id)
            ->checkCustomerHasOrder()
            ->onlyStatus([2, 3, 9])
            ->changeToCancelByCustomer($request);

        return $this->successResponse($resp, 'success', 200);
    }

    // fungsi cancel saat customer accept penawaran lalu di cancel
    public function cancelOrderIsApprove(Request $request, $id, OrderService $order_service)
    {
        DB::beginTransaction();
        try {
            $order = Order::where('id', $id)
                ->with([
                    'orderpayment',
                    'user.ewallet',
                    'service',
                    'symptom',
                    'order_status',
                    'sparepart_detail',
                    'item_detail',
                    'service_detail.symptom',
                    'service_detail.services_type.product_group',
                    'service_detail.technician.user',
                    'service_detail.price_service',
                    'history_order.order_status',
                ])
                ->firstOrFail();

            if (count($order->sparepart_detail) > 0) {
                $allParts = json_encode($order->sparepart_detail);
                $totalPart = 0;
                foreach ($order->sparepart_detail as $getTotalSparepartDetail) {
                    $totalPart += $getTotalSparepartDetail->price * $getTotalSparepartDetail->quantity;
                }
            } elseif (count($order->item_detail) > 0) {
                $allParts = json_encode($order->item_detail);
                $totalPart = 0;
                foreach ($order->sparepart_detail as $getItemDetail) {
                    $totalPart += $getItemDetail->price * $getItemDetail->quantity;
                }
            } else {
                $allParts = null;
                $totalPart = 0;
            }

            $settings = GeneralSetting::all();
            $totalService = 0;
            $totalSpareparts = 0;

            foreach ($order->service_detail as $detail) {
                $totalService =  $detail->price * $detail->unit;
            }

            if (count($order->sparepart_detail) == 0) {
                // return 'item detail';
                foreach ($order->item_detail as $sparepart) {
                    $totalSpareparts +=  $sparepart->price * $sparepart->quantity;
                }
            } else {
                // return 'service detail';
                foreach ($order->sparepart_detail as $sparepart) {
                    $totalSpareparts +=  $sparepart->price * $sparepart->quantity;
                }
            }


            $potongKomisi = $settings[5]->value / 100 * $totalService;
            $commission = $settings[2]->value / 100 * $totalService - $potongKomisi;
            $tanggalSekarang = date('Y-m-d H:i:s');
            $tanggalvisit = carbon::parse($order->schedule)->subHours('3')->format('Y-m-d H:i:s');
            $diff    = strtotime($tanggalvisit) - strtotime($tanggalSekarang);
            $jam     = floor($diff / (60 * 60));
            $hargaJasa = $order->service_detail[0]->price;
            // return $jam;
            if ($settings[4]->value > 0) {
                if ($jam >= $settings[4]->value) {

                    if ($settings[2]->type == 2) {
                        $nominal = $settings[2]->value;
                        $getCommission =  $settings[5]->value / 100 * $nominal;
                        $saldo = $totalSpareparts != "" ? ($totalService - $settings[2]->value + $totalSpareparts + $order->user->ewallet->nominal) : $totalService - $settings[2] + $order->user->ewallet->nominal;
                    } else {
                        $nominal = $settings[2]->value / 100 * $totalService;
                        $getCommission =  $settings[5]->value / 100 * $nominal;
                        $saldo = $totalSpareparts != "" ? ($totalService - $settings[2]->value / 100 * $totalService + $totalSpareparts + $order->user->ewallet->nominal) : $totalService - $settings[2]->value / 100 * $totalService + $order->user->ewallet->nominal;
                    }

                    // return ['nominals', $nominal, 'saldos', $saldo, 'comisis', $getCommission, 'totalService', $totalService];
                    $this->saveHistoryOrder($order->id, 5);
                    if ($order->is_canceled == 0) {
                        $order->update([
                            'transfer_status' => 0,
                            'orders_statuses_id' => 5,
                            'penalty_order_type' => $settings[2]->type,
                            'penalty_order_value' => $settings[2]->value,
                            'commission_penalty' => $settings[5]->value,
                            'type_commission_penalty' => $settings[5]->type,
                            'is_canceled' => 1,
                            'canceled_at' => date('Y-m-d H:i:s')
                        ]);

                        $createHistory = EwalletHistory::where('id', $order->orderpayment->wallet_history_id)->where('type_transaction_id', 1)->update([
                            'ms_wallet_id' => $order->user->ewallet->id,
                            'type_transaction_id' => 3, // cancel with customer
                            'nominal' => $nominal,
                            'saldo' => $saldo,
                            // 'nominal' => $settings[2]->value / 100 * $totalService,
                            //'saldo' => $order->grand_total + $order->user->ewallet->nominal - $settings[5]->value / 100 * $order->grand_total,// 100000 + 359950 - 50 / 100 * 100
                            'user_id' => $order->user->id
                        ]);

                        $updateWallet = Ewallet::where('user_id', $order->user->id)->update([
                            'nominal' =>  $saldo,
                        ]);

                        TechnicianSaldo::where('order_id', $id)->update([
                            'voucer_id'        => 0,
                            'commission_value' => 0,
                            'early_total' => $order->grand_total,
                            'total' => $nominal - $getCommission,
                            'transfer_status' => 0,
                        ]);

                        TransactionHistory::create([
                            'type_history'      => 'This Order Has Been Canceled By Customer',
                            'order_code'        => $order->code,
                            'order_id'          => $order->id,
                            'service_type'      => $order->service->name,
                            'symptom_name'      => $order->symptom->name,
                            'product_group_name' => $order->product_group_name,
                            'price'             => 0,
                            'beginning_balance' => $order->user->ewallet->nominal + $hargaJasa + $totalPart,
                            'ending_balance'    => $order->user->ewallet->nominal + $hargaJasa + $totalPart - $nominal,
                            'number_of_pieces'  => $nominal,
                            'status'            => 5,
                            'technician_name'   => $order->service_detail[0]->technician->name,
                            'parts'             => $allParts,
                            'commission_to_technician' => $nominal - $getCommission,
                        ]);
                    } else {
                        $order->update([
                            'orders_statuses_id' => 5,
                            'is_canceled' => 1,
                        ]);
                    }
                } else {
                    $potonganKomisi = $settings[5]->value / 100 * $totalService;
                    $commission = $settings[3]->value / 100 * $totalService - $potonganKomisi;

                    if ($settings[3]->type == 2) { //(type fix)
                        $nominal = $settings[3]->value;
                        $getCommission =  $settings[5]->value / 100 * $nominal;
                        $saldo = $totalSpareparts != "" ? ($totalService - $settings[3]->value + $totalSpareparts + $order->user->ewallet->nominal) : $totalService - $settings[3] + $order->user->ewallet->nominal;
                    } else { //(type percent)
                        $nominal = $settings[3]->value / 100 * $totalService;
                        $getCommission =  $settings[5]->value / 100 * $nominal;
                        $saldo = $totalSpareparts != "" ? ($totalService - $settings[3]->value / 100 * $totalService + $totalSpareparts + $order->user->ewallet->nominal) : $totalService - $settings[3]->value / 100 * $totalService + $order->user->ewallet->nominal;
                    }

                    // return ['nominal', $nominal, 'saldo', $saldo, 'comisi', $getCommission];
                    if ($order->is_canceled == 0) {
                        $order->update([
                            'transfer_status' => 0,
                            'orders_statuses_id' => 5,
                            'penalty_order_type' => $settings[3]->type,
                            'penalty_order_value' => $settings[3]->value,
                            'commission_penalty' => $settings[5]->value,
                            'type_commission_penalty' => $settings[5]->type,
                            'is_canceled' => 1,
                            'canceled_at' => date('Y-m-d H:i:s')
                        ]);

                        $createHistory = EwalletHistory::where('id', $order->orderpayment->wallet_history_id)->where('type_transaction_id', 1)->update([
                            'ms_wallet_id' => $order->user->ewallet->id,
                            'type_transaction_id' => 3, // cancel with customer
                            'nominal' => $nominal,
                            'saldo' => $saldo,
                            //'saldo' => $order->grand_total + $order->user->ewallet->nominal - $settings[5]->value / 100 * $order->grand_total,// 100000 + 359950 - 50 / 100 * 100
                            'user_id' => $order->user->id
                        ]);

                        $updateWallet = Ewallet::where('user_id', $order->user->id)->update([
                            'nominal' =>  $saldo,
                        ]);

                        TechnicianSaldo::where('order_id', $id)->update([
                            'voucer_id'        => 0,
                            'commission_value' => 0,
                            'early_total' => $order->grand_total,
                            'total' => $nominal - $getCommission,
                            'transfer_status' => 0,
                        ]);

                        TransactionHistory::create([
                            'type_history'      => 'This Order Has Been Canceled By Customer',
                            'order_code'        => $order->code,
                            'order_id'          => $order->id,
                            'service_type'      => $order->service->name,
                            'symptom_name'      => $order->symptom->name,
                            'product_group_name' => $order->product_group_name,
                            'price'             => 0,
                            'beginning_balance' => $order->user->ewallet->nominal + $hargaJasa + $totalPart,
                            'ending_balance'    => $order->user->ewallet->nominal + $hargaJasa + $totalPart - $nominal,
                            'number_of_pieces'  => $nominal,
                            'status'            => 5,
                            'technician_name'   => $order->service_detail[0]->technician->name,
                            'parts'             => $allParts,
                            'commission_to_technician' => $nominal - $getCommission,
                        ]);
                    } else {
                        $order->update([
                            'orders_statuses_id' => 5,
                            'is_canceled' => 1,
                        ]);
                    }
                }
            } else {
                $order->update([
                    'orders_statuses_id' => 5,
                ]);

                $createHistory = EwalletHistory::where('id', $order->orderpayment->wallet_history_id)->where('type_transaction_id', 1)->update([
                    'ms_wallet_id' => $order->user->ewallet->id,
                    'type_transaction_id' => 3, // cancel with customer
                    'nominal' => $totalService,
                    'saldo' => $totalSpareparts != "" ? ($totalService + $totalSpareparts + $order->user->ewallet->nominal) : $totalService + $order->user->ewallet->nominal,
                    //'saldo' => $order->grand_total + $order->user->ewallet->nominal - $settings[5]->value / 100 * $order->grand_total,// 100000 + 359950 - 50 / 100 * 100
                    'user_id' => $order->user->id
                ]);

                $updateWallet = Ewallet::where('user_id', $order->user->id)->update([
                    'nominal' =>  $totalSpareparts != "" ? ($totalService + $totalSpareparts + $order->user->ewallet->nominal) : $totalService + $order->user->ewallet->nominal,
                ]);

                TechnicianSaldo::where('order_id', $id)->update([
                    'voucer_id'        => 0,
                    'commission_value' => 0,
                    'early_total' => 0,
                    'total' => 0,
                    'transfer_status' => 0,
                ]);

                TransactionHistory::create([
                    'type_history'      => 'This Order Has Been Canceled By Customer',
                    'order_code'        => $order->code,
                    'order_id'          => $order->id,
                    'service_type'      => $order->service->name,
                    'symptom_name'      => $order->symptom->name,
                    'product_group_name' => $order->product_group_name,
                    'price'             => 0,
                    'beginning_balance' => $order->user->ewallet->nominal + $hargaJasa + $totalPart,
                    'ending_balance'    => $order->user->ewallet->nominal + $hargaJasa + $totalPart,
                    'number_of_pieces'  => 0,
                    'status'            => 5,
                    'technician_name'   => $order->service_detail[0]->technician->name,
                    'parts'             => $allParts,
                    'commission_to_technician' => $nominal - $getCommission,
                ]);
            }
            $schedule = TechnicianSchedule::where('order_id', $order->id)->update(['is_deleted' => 1]);
            DB::commit();
            //email status order
            // $this->sendEmailOrder($order);
            //Order Logs
            // $order_service->orderLog(null,null,$order->id, $order->order_status->name);
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function infoDurasi(Request $request, $id, ApiController $apiController)
    {
        $order = Order::where('id', $id)
            ->with([
                'orderpayment',
                'user.ewallet',
                'service',
                'symptom',
                'order_status',
                'sparepart_detail',
                'service_detail.symptom',
                'service_detail.services_type',
                'service_detail.technician.user',
                'service_detail.price_service',
                'history_order.order_status',
            ])
            ->first();
        // return $order;
        $settings = GeneralSetting::all();
        // return $settings;
        $tanggalSekarang = date('Y-m-d H:i:s'); //date('2020-08-13 04:00:00');
        $tanggalvisit = carbon::parse($order->schedule)->subHours('3')->format('Y-m-d H:i:s');
        $diff    = strtotime($tanggalvisit) - strtotime($tanggalSekarang);
        $jam     = floor($diff / (60 * 60) - 12);
        if ($jam >= $settings[4]->value) {
            $getSettingType = $settings[2]->type;
            $getSettingValue = $settings[2]->value;
            $msg = 'If this order is canceled you will be charged a deduction fee for your balance of ' . $settings[2]->value;
        } else {
            $getSettingType = $settings[3]->type;
            $getSettingValue = $settings[3]->value;
            $msg = 'If this order is canceled you will be charged a deduction fee for your balance of ' . $settings[3]->value;
        }

        return $apiController->successResponse([
            'jam' => $jam,
            'setting1' => $settings[1]->value,
            'setting2' => $settings[1]->type,
            'getSettingType' => $getSettingType,
            'getSettingValue' => $getSettingValue,
        ], $msg, 200);
    }

    public function infoLastTopup(Request $request, ApiController $apiController)
    {
        $getLastTopup = EwalletHistory::where('id', Auth::user()->id)->orderBy('created_at', 'DESC')->first();

        return $apiController->successResponse([
            'getLastTopup' => $getLastTopup,
        ], 'success', 201);
    }

    public function acceptOrder(Request $request, $id, OrderService $orderService) //accept order customer
    {
        $order = $orderService->setOrder($id)
            ->checkCustomerHasOrder()
            ->onlyStatus([3])
            ->changeToProcessing($request);

        return $this->successResponse($order, 'success', 200);
    }

    public function getListTechnicians(Request $request, OrderService $orderService)
    {
        $sort_by = $request->get('sort_by');
        $id_service_type = $request->service_type_id;
        $order_by = $request->order_by == null ? 'desc' : $request->order_by;
        $order_code = $orderService->generateOrderCode();
        $city_id_customer = MsAddress::where('is_main', 1)->where('user_id', Auth::id())->value('ms_city_id');
        $technicians = Technician::select([
            'users.name as name',
            'technicians.id',
            'ms_addresses.ms_city_id as ms_city_id',
            'technicians.user_id',
            'ms_price_services.value',
            'ms_price_services.created_at as created_service',
            DB::raw('ms_price_services.value AS total_service_price'),
            DB::raw('COUNT(ratings.id) AS jumlah_rating'),
            DB::raw('SUM(ratings.value) AS total_rating'),
            DB::raw('COUNT(reviews.id) AS total_review'),
        ])

            ->withCount('order_completes')
            ->with('user.badge', 'price_services', 'user.address.city')
            ->join('users', 'users.id', '=', 'technicians.user_id')
            ->leftJoin('ms_addresses', 'ms_addresses.user_id', '=', 'technicians.user_id')
            ->leftJoin('ratings', 'ratings.technician_id', '=', 'technicians.id')
            ->leftJoin('reviews', 'reviews.technician_id', '=', 'technicians.id')
            ->join('ms_price_services', 'ms_price_services.technicians_id', '=', 'technicians.id')
            ->where('ms_price_services.ms_services_types_id', $id_service_type)
            ->where('ms_price_services.product_group_id', $request->product_group_id)
            ->where('technicians.user_id', '!=', Auth::id())
            ->where('technicians.status', 1)
            ->where('ms_addresses.is_main', 1)
            ->groupBy('technicians.id');


        if ($sort_by != null) {
            if ($sort_by == 'by_name') {
                $technicians->orderBy('name', 'asc');
            } else if ($sort_by == 'by_newest') {
                $technicians->orderBy('ms_price_services.created_at', 'desc');
            } else if ($sort_by == 'by_rating') {
                $technicians->orderBy('total_rating', $order_by);
            } else if ($sort_by == 'by_review') {
                $technicians->orderBy('total_review', $order_by);
            } else if ($sort_by == 'by_low_price') {
                $technicians->orderBy('ms_price_services.value', 'asc');
            } else if ($sort_by == 'by_high_price') {
                $technicians->orderBy('ms_price_services.value', 'desc');
            }
        } else {
            if ($city_id_customer != null) {
                $technicians->orderByRaw("FIELD(ms_city_id , '" . $city_id_customer . "') DESC");
            }
        }

        $layanan = MsServicesType::with(['symptom.services', 'price_service'])->where('id', $id_service_type)->get();
        $getWalletUser       = Ewallet::where('user_id', Auth::user()->id)->first();
        $querystringArray = ['service_type_id' => $request->service_type_id, 'product_group_id' => $request->product_group_id, 'sort_by' => $sort_by];
        $pageSize = 20;
        $returnHTML = view('admin.customer.job_request.list_technicians', [
            'title'         => 'LIST TECHNICIAN',
            'technicians'   => $technicians->paginate($pageSize)->appends($querystringArray),
            'layanan'       => $layanan,
            'order_code'    => $order_code,
            'getWalletUser' => $getWalletUser,
        ])->render();

        return response()->json([
            'success' => true,
            'html' => $returnHTML
        ]);
    }

    public function getListTechniciansSebelumnya(Request $request, ApiController $apiController, OrderService $orderService)
    {
        $sort_by          = $request->get('sort_by');
        $order_code       = $orderService->generateOrderCode();
        $product_group_id = $request->product_group_id;
        $service_type_id  = $request->service_type_id;

        $customer_ewallet = Ewallet::where('user_id', Auth::id())->value('nominal');

        Session::put('service_type_id', $service_type_id);
        Session::put('product_group_id', $product_group_id);
        // $url = '/customer/request-job/list-technician?service_type_id=' . $request->service_type_id . '';
        $url = '/customer/request-job/list-technician?service_type_id=' . $request->service_type_id . '&product_group_id=' . $request->product_group_id . '';
        $technicians = Technician::with([
            'user.info',
            'user.badge',
            'price_services' => function ($query) use ($service_type_id, $product_group_id) {
                $query->where('ms_services_types_id', $service_type_id);
                $query->where('product_group_id', $product_group_id);
            },
        ])
            ->withCount('order_completes')
            ->where('technicians.user_id', '!=', Auth::id())
            ->where('technicians.status', 1)
            ->get();
        // return $technicians;
        if (isset($sort_by)) {
            $url = $url . '&sort_by=' . $sort_by;
            if ($sort_by == 'by_name') {
                $technicians = $technicians->sortBy('name');
            } else if ($sort_by == 'by_newest') {
                $technicians = $technicians->sortByDesc('created_service');
            } else if ($sort_by == 'by_rating') {
                $technicians = $technicians->sortByDesc('avg_rating');
            } else if ($sort_by == 'by_review') {
                $technicians = $technicians->sortByDesc('total_review');
            } else if ($sort_by == 'by_low_price') {
                $technicians = $technicians->sortBy('total_prices');
            } else if ($sort_by == 'by_high_price') {
                $technicians = $technicians->sortByDesc('total_prices');
            }
        }
        $technicians         = $technicians->where('total_prices', '!=', 0);
        $pageSize            = 4;
        $technicians         = CollectionHelper::paginate($technicians, $pageSize)->withPath($url);
        $getWalletUser       = Ewallet::where('user_id', Auth::user()->id)->first();
        // $layanan = MsServicesType::with(['symptom.services', 'price_service'])->where('id', $id_service_type)->get();


        $returnHTML = view('admin.customer.job_request.list_technicians', [
            'title'         => 'LIST TECHNICIAN',
            'technicians'   => $technicians,
            // 'layanan'       => $layanan,
            'order_code'    => $order_code,
            'getWalletUser' => $getWalletUser,

        ])->render();

        return response()->json([
            'success' => true,
            'html' => $returnHTML
        ]);
    }
    public function saveRequestJob(Request $request, OrderService $orderService, ApiController $apiController)
    {
        $getSaldo = TopupWallet::select(array(DB::raw('SUM(nominal) as Total')))
            ->where('user_id', Auth::user()->id)
            ->where('status_transfer_id', 1)
            ->first();

        return $apiController->successResponse($orderService->createFromCustomer($request), 'success', 201);
    }

    public function replayItem()
    {
        return $this->viewAdmin('admin.customer.tiket._replay_item');
    }

    public function saveOrderComplaint(Request $request, ApiController $apiController)
    {
        $service = new OrderComplaintService;

        return $apiController->successResponse($service->createAsCustomer($request->all()), 'success', 201);
    }

    public function replyOrderComplaint(ComplaintReplyRequest $request, ApiController $apiController, $order_id)
    {
        if ($request->attachment != null) {
            request()->validate([
                'attachment' => 'mimes:jpeg,jpg,png',
            ]);
        }

        $service = new OrderComplaintService;

        return $apiController->successResponse($service->reply($request), 'success', 201);
    }

    public function technician($id)
    {
        $technician = Technician::with([
            'job_title.job_title_category',
            'user.info',
        ])->where('id', $id)->firstOrFail();

        $rating_breakdown = $technician->getRatingBreakDown();
        $reviews          = Review::with(['user.info', 'order.rating_and_review.rating'])->where('technician_id', $technician->id)->paginate(5);
        return $this
            ->viewAdmin('admin.teknisi.users.detail_teknisi', [
                'title'      => 'TECHNICIAN DETAIL',
                'technician' => $technician,
                'rr'         => $rating_breakdown,
                'reviews'    => $reviews,
            ]);
    }

    public function checkout(Request $request, $order_id, OrderService $order_service) // checkouts Customer

    {
        $ms_payment_methods_id = $request->ms_payment_methods_id;
        $grand_total           = $request->grand_total;
        $ppn                   = $request->ppn;
        $discount              = $request->discount;
        $ppn_nominal           = $request->ppn_nominal;
        $discount_nominal      = $request->discount_nominal;

        $order = Order::with(
            'orderpayment',
            'user.ewallet',
            'service',
            'symptom',
            'order_status',
            'sparepart_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician.user',
            'service_detail.price_service',
            'history_order.order_status'
        )->where('id', $order_id)->first();
        $order->update([
            'ms_payment_methods_id' => $ms_payment_methods_id,
            'grand_total' => $grand_total,
            'ppn' => $ppn,
            'discount' => $discount,
            'ppn_nominal' => $ppn_nominal,
            'discount_nominal' => $discount_nominal,
            'orders_statuses_id' => 8
        ]);
        //email status order
        $this->sendEmailOrder($order);
        //Order Logs
        $order_service->orderLog(null, null, $order->id, $order->order_status->name);
        $ApiController = new ApiController();
        return $ApiController->successResponse('success', 201);
    }

    public  function complaint(ComplaintRequest $request, $order_id, OrderService $order_service)
    {
        $note_complaint = $request->note_complaint;
        $technician_id  = $request->technician_id;

        $order = Order::with(
            'orderpayment',
            'user.ewallet',
            'service',
            'symptom',
            'order_status',
            'sparepart_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician.user',
            'service_detail.price_service',
            'history_order.order_status'
        )->where('id', $order_id)
            ->where('users_id', Auth::id())
            ->firstOrFail();

        if ($order->orders_statuses_id == 7) {
            Order::where('id', $order_id)->update([
                'orders_statuses_id' => 11,
                'note_complaint'     => $note_complaint,
            ]);

            MsOrderComplaint::create([
                'message'   => $note_complaint,
                'ms_orders_id'  => $order->id,
                'user_id'  => Auth::id(),
                'created_by'   => 'Customer',
            ]);
        }
        //email status order
        $this->sendEmailOrder($order);
        $this->sendEmailOrder($order, 'complaint'); // argument ke 2 adalah name_module hardcode dr table email_order
        //Order Logs
        $order_service->orderLog(null, null, $order->id, $order->order_status->name);
        (new NotifikasiService)->kirimNotifikasiComplain($order);
        return $this->successResponse($order, $this->successUpdateMsg(), 200);
    }

    public function approve(Request $request, $order_id, OrderService $orderService) // Approve Customer
    {
        $order = Order::with(
            'orderpayment',
            'user.ewallet',
            'service',
            'symptom',
            'order_status',
            'sparepart_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician.user',
            'service_detail.price_service',
            'history_order.order_status'
        )->where('id', $order_id)->first();

        $resp = $orderService->setOrder($order_id)
            ->checkCustomerHasOrder()
            ->onlyJobDoneStatus()
            ->setNotificationVia(['chat'])
            ->changeToComplate($request);

        $order = $orderService->getNewOrder();

        //email status order
        $this->sendEmailOrder($order);
        $this->sendEmailOrder($order, 'review', 1);
        //Order Logs
        $orderService->orderLog(null, null, $order->id, $order->order_status->name);

        return $this->successResponse($resp, $this->successStoreMsg(), 200);
    }

    public function complaintClose(Request $request, $order_id, OrderService $order_service) //Customer & Admin
    {
        DB::beginTransaction();
        try {
            $note_complaint = $request->message;
            $order          = Order::with(
                'orderpayment',
                'user.ewallet',
                'service',
                'symptom',
                'order_status',
                'sparepart_detail',
                'tmp_sparepart',
                'item_detail',
                'tmp_item_detail',
                'service_detail.symptom',
                'service_detail.services_type.product_group',
                'service_detail.technician.user',
                'service_detail.price_service',
                'history_order.order_status'
            )->where('id', $order_id)->first();

            if ($order->orders_statuses_id == 11) {
                Order::where('id', $order_id)->update([
                    'orders_statuses_id' => 10,
                    'transfer_status' => 0,
                    'note_complaint' => $note_complaint != "" ? $note_complaint : "",
                    'updated_at' => date('Y-m-d H:i:s'),
                    'date_complete' => date('Y-m-d H:i:s')
                ]);

                // create history order
                HistoryOrder::create([
                    'orders_id'          => $order_id,
                    'orders_statuses_id' => 10
                ]);

                // jika online bayar
                if ($order->payment_type == 1 && $order->garansi == null) {
                    TechnicianSaldo::where('order_id', $order->id)->update([
                        'voucer_id'        => 0,
                        'commission_value' => 0,
                        'early_total' => $order->grand_total,
                        'total' => $order->after_commission,
                        'transfer_status' => 0,
                    ]);
                }

                if (count($order->sparepart_detail) > 0) {
                    $allParts = json_encode($order->sparepart_detail);
                    $totalPart = 0;
                    foreach ($order->sparepart_detail as $getTotalSparepartDetail) {
                        $totalPart += $getTotalSparepartDetail->price * $getTotalSparepartDetail->quantity;
                    }
                } elseif (count($order->item_detail) > 0) {
                    $allParts = json_encode($order->item_detail);
                    $totalPart = 0;
                    foreach ($order->item_detail as $getItemDetail) {
                        $totalPart += $getItemDetail->price * $getItemDetail->quantity;
                    }
                } else {
                    $allParts = null;
                    $totalPart = 0;
                }

                $transactionHistory = TransactionHistory::create([
                    'type_history'      => 'Technician Job Done',
                    'order_code'        => $order->code,
                    'order_id'          => $order->id,
                    'service_type'      => $order->service->name,
                    'symptom_name'      => $order->symptom->name,
                    'product_group_name' => $order->product_group_name,
                    'price'             => $totalPart,
                    'beginning_balance' => $order->user->ewallet->nominal + $order->service_detail[0]->price + $totalPart,
                    'ending_balance'    => $order->user->ewallet->nominal + $order->service_detail[0]->price + $totalPart - $totalPart,
                    'number_of_pieces'  => $totalPart,
                    'status'            => 10,
                    'technician_name'   => $order->service_detail[0]->technician->name,
                    'parts'             => $allParts,
                    'commission_to_technician' => $order->after_commission,
                ]);
            }

            //Schedule teknisi hilang.
            TechnicianSchedule::where('order_id', $order->id)->update(['is_deleted' => 1]);

            //email status order
            $this->sendEmailOrder($order);
            $this->sendEmailOrder($order, 'complaint_solved'); // argument ke 2 adalah name_module hardcode dr table email_order
            //Order Logs
            $order_service->orderLog(null, null, $order->id, $order->order_status->name);
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $this->successResponse($order, $this->successUpdateMsg(), 200);
    }

    public function showListComplaint()
    {
        return $this->viewAdmin('admin.customer.complaint.index');
    }

    public function detailComplaint($order_id)
    {
        $query = Order::with([
            'user.technician',
            'symptom',
            'service',
            'order_status',
            'service_detail.symptom',
            'service_detail.services_type',
            'service_detail.technician'
        ])->where('id', $order_id)->first();

        MsOrderComplaint::where('ms_orders_id', $order_id)->where('user_id', '!=', Auth::id())->update([
            'read_at' => now()
        ]);

        $user          = Auth::user()->id;
        $showComplaint = MsOrderComplaint::with(['user', 'order'])->where('ms_orders_id', $order_id)->get();
        return $this->viewAdmin('admin.customer.complaint.detailComplain', compact('query', 'showComplaint', 'user'));
    }

    public function detailComplaintJson($order_id, Request $request)
    {
        $order_by       = empty($request->order_by) ? 'id' : $request->order_by;
        $sort_by        = in_array($request->sort_by, ['asc', 'desc']) ? $request->sort_by : 'desc';

        $order = Order::where('id', $order_id)
            ->where('users_id', Auth::id())
            ->firstOrFail();

        MsOrderComplaint::where('ms_orders_id', $order_id)->where('user_id', '!=', Auth::id())->update([
            'read_at' => now()
        ]);

        $complaints = MsOrderComplaint::with(['user'])
            ->where('ms_orders_id', $order_id)
            ->orderBy($order_by, $sort_by)
            ->get();

        $resp = [
            'order' => $order,
            'complaints' => $complaints
        ];

        return $this->successResponse($resp, 'ok', 200);
    }

    public function datatablesComplaint($query = null, Request $request)
    {
        $query = Order::with([
            'user',
            'symptom',
            'service',
            'unread_complaint',
            'order_status',
            'service_detail.symptom',
            'service_detail.services_type'
        ])->where('users_id', Auth::user()->id)->where('orders_statuses_id', 11);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function indexComplaint($query = null, Request $request)
    {
        $order_by       = empty($request->order_by) ? 'id' : $request->order_by;
        $sort_by        = in_array($request->sort_by, ['asc', 'desc']) ? $request->sort_by : 'desc';

        $query = Order::with([
            'user',
            'symptom',
            'service',
            'unread_complaint',
            'order_status',
            'service_detail.symptom',
            'service_detail.services_type'
        ])->where('users_id', Auth::user()->id)
            ->where('orders_statuses_id', 11)
            ->orderBy($order_by, $sort_by);

        return $query->paginate(10);
    }

    public function customerDoneJobs($order_id)
    {
        # code...
    }


    // function customer topup
    public function showListTopup(Request $request)
    {
        $wallet = Ewallet::where('user_id', Auth::user()->id)->first();
        $countCustomerTopup = EwalletPayment::with(['orderhistory.user', 'getproduct.service_detail.services_type'])
            ->whereHas('orderhistory.user', function ($q) {
                $q->where('id', Auth::user()->id);
            })->get();
        return $this->viewAdmin('admin.customer.EwalletCustomer.walletList', ['countCustomerTopup' => $countCustomerTopup, 'wallet' => $wallet]);
    }

    public function showHistoryPayment(Request $request)
    {
        $wallet = Ewallet::where('user_id', Auth::user()->id)->first();
        $countCustomerTopup = EwalletPayment::with([
            'orderhistory.user',
            'order.service_detail.services_type',
            'order.sparepart_detail',
            'order.order_status',
            'order.item_detail'
        ])
            ->whereHas('orderhistory.user', function ($q) {
                $q->where('id', Auth::user()->id);
            })
            ->orderBy('created_at', 'Desc')
            ->get();
        // return $countCustomerTopup;
        return $this->viewAdmin('admin.customer.EwalletCustomer.walletHistoryPayment', [
            'countCustomerTopup' => $countCustomerTopup,
            'wallet' => $wallet
        ]);
    }

    public function topupCheckout(Request $request, $id)
    {
        $getDataUser = User::where('id', Auth::user()->id)
            ->with(['info', 'address', 'address.city', 'address.district', 'address.village', 'address.zipcode',])
            ->first();
        $getBankName = BankTransfer::where('bank_name', 'like', '%' . $request->q . '%')
            ->get();

        $getPaymentType = BankTransfer::whereIn('id', [1, 2])
            ->where('is_active', 0)
            ->orderBy('id', 'desc')
            ->get();
        // return $getPaymentType;
        return $this
            ->viewAdmin('admin.customer.EwalletCustomer.walletCheckouts', [
                'title' => 'CHECKOUT',
                'auth'  => Auth::user(),
                'wallet' => EwalletHistory::where('id', $id)->with('voucer')->firstOrFail(),
                'getBankName' => $getBankName,
                'getDataUser' => $getDataUser,
                'getPaymentType' => $getPaymentType
            ]);
    }

    public function topupDetails(Request $request, $id)
    {
        $getDataUser = User::where('id', Auth::user()->id)
            ->with(['info', 'address', 'address.city', 'address.district', 'address.village', 'address.zipcode',])
            ->first();
        $getBankName = BankTransfer::where('bank_name', 'like', '%' . $request->q . '%')
            ->get();

        $getPaymentType = BankTransfer::whereIn('id', [1, 2])
            ->where('is_active', 0)
            ->get();
        // return $getPaymentType;
        return $this
            ->viewAdmin('admin.customer.EwalletCustomer.detailTopup', [
                'title' => 'CHECKOUT',
                'auth'  => Auth::user(),
                'wallet' => EwalletHistory::where('id', $id)->firstOrFail(),
                'getBankName' => $getBankName,
                'getDataUser' => $getDataUser,
                'getPaymentType' => $getPaymentType
            ]);
    }

    public function topupConfirmation(Request $request, $id)
    {
        return $this
            ->viewAdmin('admin.customer.EwalletCustomer.walletConfirmation', [
                'title' => 'CHECKOUT',
                'auth'  => Auth::user(),
                'wallet' => EwalletHistory::with(['bank', 'user'])->where('id', $id)->firstOrFail()
            ]);
    }

    public function showDetailHistory($order_id)
    {
        return $this
            ->viewAdmin('admin.customer.EwalletCustomer.detailHistoryPayment');
    }

    public function sendHistoryOrder()
    {
        $getLastHistoryOrder = EwalletHistory::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->first();
    }
}
