<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Model\Master\ImageUpload;
use App\Model\Master\MsProduct;
use App\Model\Product\ProductAttribute;
use App\Model\Product\ProductAttributeTerm;
use Illuminate\Http\Request;

class MasterProductController extends AdminController
{
    public function showListProduct()
    {
        return $this
            ->setBreadcrumb('List Product', '/admin/product/show')
            ->viewAdmin('admin.master.product.products.index', [
                'title' => 'MASTER PRODUCT',
            ]);
    }

    public function fetch_data(Request $request)
    {
        if ($request->ajax()) {
            if ($request->from_date != '' && $request->to_date != '') {
                $data = DB::table('ms_products')
                    ->whereBetween('date', array($request->from_date, $request->to_date))
                    ->get();
            } else {
                $data = DB::table('ms_products')->get();
            }
            echo json_encode($data);
        }
    }

    public function createProduct()
    {
        $showImage = ImageUpload::get();
        // return $showImage;
        return $this
            ->setBreadcrumb('List Product', '/admin/product/show')
            ->setBreadcrumb('Create Product', '#')
            ->viewAdmin('admin.master.product.products.create2', [
                'title'     => 'CREATE PRODUCT',
                'showImage' => $showImage,
            ]);
    }

    public function editProduct($id)
    {
        $product = MsProduct::where('id', $id)
            ->with([
                'productCombineCategories.product_category',
                'product_images.gallery',
                'gallery',
                'status',
                'model',
                'additional',
                'productVendors.vendor',
                'productVendors.productVarians.productVarianAttributes.attribute',
                'productVendors.productVarians.productVarianAttributes.term',
            ])
            ->firstOrFail();

        $get_attr = $product->product_attribute_header;
        $data     = [];
        if ($product->product_attribute_header != null) {
            foreach ($get_attr->variation as $key => $val) {
                $term_name = [];
                foreach ($val->terms as $key => $val2) {
                    $term_name[] = [
                        'term_id'   => $val2,
                        'term_name' => ProductAttributeTerm::where('id', $val2)->value('name'),
                    ];
                }
                $data[$val->attribute_id] = [
                    'attribute_id'   => $val->attribute_id,
                    'attribute_name' => ProductAttribute::where('id', $val->attribute_id)->value('name'),
                    'term_name'      => $term_name,
                ];
            }
        }

        // return $product;

        return $this
            ->setBreadcrumb('List Product', '/admin/product/show')
            ->setBreadcrumb('Update Product', '#')
            ->viewAdmin('admin.master.product.products.edit', [
                'title'   => 'EDIT PRODUCT',
                'product' => $product,
                'data'    => $data,
            ]);
    }

    public function detailProduct($id)
    {
        $showDataProd = MsProduct::where('id', $id)
            ->with([
                'model',
                'additional',
            ])
            ->first();

        $showDataVendor = MsProduct::where('id', $id)
            ->with([
                'product_vendor.vendor',
                'product_vendor.product_varian.product_varian_attribute.attribute',
                'product_vendor.product_varian.product_varian_attribute.term',
            ])
            ->first();
// return $showDataVendor;
        return $this
            ->setBreadcrumb('List Product', '/product')
            ->setBreadcrumb('Detail Product', '/product/edit')
            ->viewAdmin('admin.master.product.products.detail', [
                'title'          => 'Detail product',
                'showDataProd'   => $showDataProd,
                'showDataVendor' => $showDataVendor,
            ]);
    }

}
