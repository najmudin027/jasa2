<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Product\ProductPartCategory;

class MasterPartCategoryController extends AdminController
{
    public function showPartCategory()
    {
        return $this
        ->setBreadcrumb('list Part Category', '/admin/product-part-category/show')
        ->viewAdmin('admin.master.product.partCategory.index', [
            'title' => 'Master Part Category',
            'isActive' => ''
        ]);
    }

    public function createPartCategory()
    {
        return $this
        ->setBreadcrumb('list Part Category', '/admin/product-part-category/show')
        ->setBreadcrumb('Create Part Category', '#')
        ->viewAdmin('admin.master.product.partCategory.create');
    }

    public function updatePartCategory($id)
    {
        $updatePart = ProductPartCategory::where('id', $id)->first();
        return $this
        ->setBreadcrumb('list Part Category', '/admin/product-part-category/show')
        ->setBreadcrumb('Create Part Category', '#')
        ->viewAdmin('admin.master.product.partCategory.update', compact('updatePart'));
    }
}
