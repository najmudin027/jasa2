<?php

namespace App\Http\Controllers\Admin;

use App\Model\Master\Order;
use Illuminate\Http\Request;
use App\Model\Master\EmailOther;
use App\Model\Master\BankTransfer;
use App\Model\Master\EmailStatusOrder;
use App\Http\Controllers\AdminController;

class MasterEmailTemplateController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master Email Status Order'
        ])
        ->setBreadcrumb('List Template', '/admin/email-status-order/show')
        ->viewAdmin('admin.master.email_status_order.index');
    }


    public function create()
    {
        return $this->setData([
            'title' => 'Create Template Email Status Order'
        ])
        ->setBreadcrumb('List Template', '/admin/email-status-order/create')
        ->viewAdmin('admin.master.email_status_order.create');
    }

    public function update($id)
    {
        $getData = EmailStatusOrder::where('orders_statuses_id', $id)->get();
        $id_email = $getData->pluck('id');
        return $this->setData([
            'title' => 'Update Email Status Order',
            'getData' => $getData,
            'id_email' => $id_email,
        ])
        ->setBreadcrumb('List Template', '/admin/email-status-order/edit')
        ->viewAdmin('admin.master.email_status_order.update');
    }

    public function indexEmailOther()
    {


        return $this->setData([
            'title' => 'Master Email Other'
        ])
        ->setBreadcrumb('List Template', '/admin/email-other/show')
        ->viewAdmin('admin.master.email_other.index');
    }


    public function createEmailOther()
    {
        return $this->setData([
            'title' => 'Create Template Email Other'
        ])
        ->setBreadcrumb('List Template', '/admin/email-other/create')
        ->viewAdmin('admin.master.email_other.create');
    }

    public function updateEmailOther($module)
    {
        $getData = EmailOther::where('name_module', $module)->get();
        // return $getData;
        $id_email = $getData->pluck('id');
        $types = $getData->pluck('type');
        return $this->setData([
            'title' => 'Update Email Other',
            'getData' => $getData,
            'id_email' => $id_email,
            'types' => $types
        ])
        ->setBreadcrumb('List Template', '/admin/email-other/edit')
        ->viewAdmin('admin.master.email_other.update');
    }
}
