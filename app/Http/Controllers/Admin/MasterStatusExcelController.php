<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class MasterStatusExcelController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master Status Excel'
        ])
        ->setBreadcrumb('List Master Status Excel', '/admin/status-excel/show')
        ->viewAdmin('admin.master.status-excel.index');
    }
}
