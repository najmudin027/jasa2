<?php

namespace App\Http\Controllers\Admin;

use App\Model\Master\WebSetting;
use Illuminate\Http\Request;
use App\Services\WebSettingService;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\AdminController;
use App\Exceptions\SendErrorMsgException;
use Intervention\Image\ImageManagerStatic as Image;
use DB;

class WebSettingController extends AdminController
{

    public function webSetting(Request $request)
    {
        $showData = WebSetting::first();
        // if ($showData == '') {
            return $this
                ->setBreadcrumb('Web Setting', '/product')
                ->viewAdmin('admin.setting.webSetting.create', ['showData' => $showData]);
        // }else{
        //     return $this
        //         ->setBreadcrumb('Web Setting', '/product')
        //         ->setBreadcrumb('Update', '/product')
        //         ->view('admin.setting.webSetting.index', [
        //             'showData' => $showData,
        //             'title' => 'Web Setting Update'
        //         ]);
        // }
    }

    public function create(Request $request)
    {
        $webSettings = WebSetting::first();

        if ($request->hasFile('logo')) {
            $image      = $request->file('logo');
            if($image != ''){
                $file_name   = time() . '.' . $image->getClientOriginalExtension();
            }
            $img = Image::make($image);

            

            $img->stream(); // <-- Key point
            if($file_name){
                Storage::disk('local')->put('public/web_setting_image/' . $file_name, $img);
            }
            if($webSettings != null){
                $exists = Storage::disk('local')->has('public/web_setting_image/' . $webSettings->logo);
                if ($exists) {
                    Storage::delete('public/web_setting_image/' . $webSettings->logo);
                }
            }
        }

        // DB::beginTransaction();
        // try {
            if ($webSettings == null) {
                if(!empty($file_name)){
                    WebSetting::create([
                        'title_setting' => $request->title_setting,
                        'metta_setting' => $request->metta_setting,
                        'image_url' => asset('/get/logo/from-storage/' . $file_name),
                        'logo' => $file_name != "" ? $file_name : "",
                    ]);
                }else{
                    WebSetting::create([
                        'title_setting' => $request->title_setting,
                        'metta_setting' => $request->metta_setting,
                    ]);
                }
            } else {
                if(!empty($file_name)){
                    $webSettings->update([
                        'title_setting' => $request->title_setting,
                        'metta_setting' => $request->metta_setting,
                        'image_url' => asset('/get/logo/from-storage/' . $file_name),
                        'logo' => $file_name != "" ? $file_name : "",
                    ]);
                }else{
                    $webSettings->update([
                        'title_setting' => $request->title_setting,
                        'metta_setting' => $request->metta_setting,
                    ]);
                }
                
            }

            // commit db
        //     DB::commit();
        // } catch (\Exception $e) {
        //     // rollback db
        //     DB::rollback();
        //     throw new SendErrorMsgException($e->getMessage());
        // }

        return WebSetting::first();
    }
}
