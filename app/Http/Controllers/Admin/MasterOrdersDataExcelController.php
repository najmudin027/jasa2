<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\Master\PartsDataExcel;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Model\Master\OrdersDataExcel;
use App\Http\Controllers\AdminController;
use App\Model\Master\JasaDataExcel;
use App\Model\Master\PartsDataExcelBundle;

class MasterOrdersDataExcelController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master Orders Data Excel '
        ])
            ->setBreadcrumb('List Orders Data Excel', '/admin/orders-data-excel/show')
            ->viewAdmin('admin.master.orders-data-excel.index');
    }

    public function create()
    {
        return $this->setData([
            'title' => 'Create Master Orders Data Excel'
        ])
            ->setBreadcrumb('List services', '/admin/orders-data-excel/show')
            ->viewAdmin('admin.master.orders-data-excel.create');
    }

    public function update($id)
    {
        $getData = OrdersDataExcel::where('id', $id)->firstOrFail();
        // return $getDataservices;
        $partData = PartsDataExcel::with('part_data_stock_inventory.part_data_stock')->where('orders_data_excel_id', $id)->whereNull('part_data_excel_bundle_id')->get();
        $jasaData = JasaDataExcel::with('jasa')->where('orders_data_excel_id', $id)->get();
        $bundleData = PartsDataExcelBundle::with('part_data_stock_bundle')->where('orders_data_excel_id',$id)->get();

        return $this->setData([
            'title' => 'Update Master Orders Data Excel',
            'getData' => $getData,
            'partData' => $partData,
            'jasaData' => $jasaData,
            'bundleData' => $bundleData,
        ])
            ->setBreadcrumb('List Orders Data Excel', '/admin/orders-data-excel/show')
            ->viewAdmin('admin.master.orders-data-excel.update');
    }

    public function detail($id)
    {
        $getData = OrdersDataExcel::where('id', $id)->first();
        $partData = PartsDataExcel::where('orders_data_excel_id', $id)->get();
        $jasaData = JasaDataExcel::where('jasa', $id)->get();
        // return $getDataservices;
        return $this->setData([
            'title' => 'Detail Master Orders Data Excel',
            'getData' => $getData,
            'partData' => $partData,
            'jasaData' => $jasaData
        ])
            ->setBreadcrumb('Detail Orders Data Excel', '/admin/orders-data-excel/detail')
            ->viewAdmin('admin.master.orders-data-excel.detail');
    }

    public function dataLogs()
    {
        return $this->setData([
            'title' => 'Orders Data Excel & General Journal Logs (Deleted Only)'
        ])
            ->setBreadcrumb('List Orders Data Excel', '/admin/data-logs')
            ->viewAdmin('admin.master.orders-data-excel.logs');
    }

    public function getValuePart($part_data_excel, $field, $i) {
        if($part_data_excel != null && isset($part_data_excel[$i-1]->{$field})) {
            $val = $part_data_excel[$i-1]->{$field};
        } else {
            $val = null;
        }
        return $val;
    }

    public function exportData(Request $request) {
        $q = OrdersDataExcel::with('part_data_excel');
        $by_date = $request->type_date == 'request_date' ? 'request_date' : 'repair_completed_date';
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
            $date_to = Carbon::parse($request->date_to)->format('Y-m-d');
            $q->where($by_date, '>=', $date_from)
            ->where($by_date, '<=', $date_to);
        }
        $q = $q->get();
        // return $q;
        $data = [];
        foreach($q as $row) {
            $part_data = [];
            // $row->part_data_excel != null ? $row->part_data_excel->code_material : null;
            foreach(range(1,13) as $val) {
                $part_data['code_material_'.$val.''] = $this->getValuePart($row->part_data_excel, 'code_material', $val);
                $part_data['part_description_'.$val.''] = $this->getValuePart($row->part_data_excel, 'part_description', $val);
                $part_data['quantity_'.$val.''] = $this->getValuePart($row->part_data_excel, 'quantity', $val);
            }
            $order_data = [
                "service_order_no" => $row['service_order_no'],
                "asc_name" => $row['asc_name'],
                "customer_name" => $row['customer_name'],
                "type_job" => $row['type_job'],
                "defect_type" => $row['defect_type'],
                "engineer_code" => $row['engineer_code'],
                "engineer_name" => $row['engineer_name'],
                "assist_engineer_name" => $row['assist_engineer_name'],
                "merk_brand" => $row['merk_brand'],
                "model_product" => $row['model_product'],
                "status" => $row['status'],
                "reason" => $row['reason'],
                "remark_reason" => $row['remark_reason'],
                "pending_aging_days" => $row['pending_aging_days'],
                "street" => $row['street'],
                "city" => $row['city'],
                "phone_no_mobile" => $row['phone_no_mobile'],
                "phone_no_home" => $row['phone_no_home'],
                "phone_no_office" => $row['phone_no_office'],
                "month" => $row['month'],
                "date" => Carbon::parse($row['date'])->format('Y-m-d'),
                "request_time" => Carbon::parse($row['request_time'])->format('H:i:s'),
                // "created_times" => $row['created_times'],
                "engineer_picked_order_date" => Carbon::parse($row['engineer_picked_order_date'])->format('Y-m-d'),
                "engineer_picked_order_time" => Carbon::parse($row['engineer_picked_order_time'])->format('H:i:s'),
                "admin_assigned_to_engineer_date" => Carbon::parse($row['admin_assigned_to_engineer_date'])->format('Y-m-d'),
                "admin_assigned_to_engineer_time" => Carbon::parse($row['admin_assigned_to_engineer_time'])->format('H:i:s'),
                "engineer_assigned_date" => Carbon::parse($row['engineer_assigned_date'])->format('Y-m-d'),
                "engineer_assigned_time" => Carbon::parse($row['engineer_assigned_time'])->format('H:i:s'),
                "tech_1st_appointment_date" => Carbon::parse($row['tech_1st_appointment_date'])->format('Y-m-d'),
                "tech_1st_appointment_time" => Carbon::parse($row['tech_1st_appointment_time'])->format('H:i:s'),
                "1st_visit_date" => Carbon::parse($row['1st_visit_date'])->format('Y-m-d'),
                "1st_visit_time" => Carbon::parse($row['1st_visit_time'])->format('H:i:s'),
                "repair_completed_date" => Carbon::parse($row['repair_completed_date'])->format('Y-m-d'),
                "repair_completed_time" => Carbon::parse($row['repair_completed_time'])->format('H:i:s'),
                "closed_date" => Carbon::parse($row['closed_date'])->format('Y-m-d'),
                "closed_time" => Carbon::parse($row['closed_time'])->format('H:i:s'),
                "rating" => $row['rating'],
            ];

            $data [] = array_merge($order_data, $part_data);
        }

        return (new FastExcel($data))->download('Export_Orders_data_Excel_Data_'.$request->date_from.'-'.$request->date_to.'.xlsx');
    }


}
