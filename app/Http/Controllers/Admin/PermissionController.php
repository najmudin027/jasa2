<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use DB;
use Spatie\Permission\Models\Role;

class PermissionController extends AdminController
{
    public function showIndexPermission()
    {
        return $this
            ->setBreadcrumb('Permissions', '/admin/permission')
            ->viewAdmin('admin.setting.permission.permissionIndex', [
                'title' => 'Show List Permissions'
            ]);
    }

    public function showIndexRole()
    {
        return $this
            ->setBreadcrumb('Roles', '/admin/role/show')
            ->viewAdmin('admin.setting.permission.roles.rolesIndex', [
                'title' => 'Show List Role'
            ]);
    }

    public function showCreateRole()
    {
        $viewPermission = DB::table('permissions')->get();
        return $this
            ->setBreadcrumb('Roles Create', '#')
            ->viewAdmin('admin.setting.permission.roles.rolesCreate', compact('viewPermission'));
    }

    public function showUpdateRole($id)
    {
        $viewRoles = Role::where('id', $id)->first();
        $viewPermission = DB::table('permissions')->get();
        return $this
            ->setBreadcrumb('Roles Update', '#')
            ->viewAdmin('admin.setting.permission.roles.rolesUpdate', compact('viewPermission', 'viewRoles'));
    }
}
