<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Model\Master\MsTicket as Ticket;
use App\Model\Master\MsTicketDetail;
use Auth;

class MasterTicketController extends AdminController
{
    public function index()
    {
        return $this
            ->setBreadcrumb('list Ticket', '/admin/symptom/show')
            ->viewAdmin('admin.master.ticket.index', [
                'title' => 'List Ticket',
            ]);
    }

    public function detail($id)
    {
        MsTicketDetail::where('ms_ticket_id', $id)->where('user_id', '!=' ,Auth::id())->update([
            'read_at' => now()
        ]);

        return $this
            ->viewAdmin('admin.customer.tiket.detail', [
                'title' => 'DETAIL TICKET',
                'auth'  => Auth::user(),
                'tiket' => Ticket::with(['user.info', 'ticket_details.user.info'])->where('id', $id)->firstOrFail(),
            ]);
    }
}
