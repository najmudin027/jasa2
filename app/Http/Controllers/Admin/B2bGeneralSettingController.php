<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\GeneralSettingB2b;

class B2bGeneralSettingController extends AdminController
{
    public function index(Type $var = null)
    {
        $getDataSettingB2b = GeneralSettingB2b::get();
        return $this
            ->setBreadcrumb('General Settings', '#')
            ->viewAdmin('admin.business_to_business_general_setting.index', [
                'title' => 'GENERAL SETTING BUSINESS TO BUSINESS',
                'getDataSettingB2b' => $getDataSettingB2b
            ]);
    }

    public function store(Request $request)
    {
        $getAgreement = GeneralSettingB2b::whereIn('name', [
            'status_waranty', 
            'kerusakan',
            'validity',
            'payment',
            'minimum_order',
            'time_of_delivery',
            'status_price',
            ])->first();

        $value = $request->value;
        $name = $request->name;
        $ids = $request->id;


        if ($getAgreement) {
            foreach ($value as $key => $values) {
                $id = $ids[$key];
                GeneralSettingB2b::where('id', $id)->update([
                    'value' => $request->value[$key],
                ]);
            }
        } else {
            foreach ($value as $key => $values) {
                return 'create';
                $id = $ids[$key];
                GeneralSettingB2b::create([
                    'name' => $name[$key],
                    'value' => $value[$key],
                ]);
            }
        }
    }
}
