<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Company;

class ManageUsersB2BController extends AdminController
{
    public function index()
    {
        return $this
            ->setBreadcrumb('Users Business To Business', '/admin/user-business-to-business/show')
            ->viewAdmin('admin.setting.manageusers_b2b.index', [
                'title' => 'LIST USER',
            ]);
    }

    public function create()
    {
        $company = Company::get();
        return $this
            ->setBreadcrumb('Users', '/admin/user-business-to-business/show')
            ->setBreadcrumb('Users Create', '#')
            ->viewAdmin('admin.setting.manageusers_b2b.create', [
                'title'    => 'CREATE USER',
                'company'    => $company,
            ]);
    }

    public function update($id)
    {
        $updateUsers = User::find($id);

        $company = Company::get();

        return $this
            ->setBreadcrumb('Users', '/admin/user-business-to-business/show')
            ->setBreadcrumb('Users Update', '#')
            ->viewAdmin('admin.setting.manageusers_b2b.update', [
                'updateUsers' => $updateUsers,
                'company'    => $company,
                'title'       => 'EDIT USER',
            ]);
    }
}
