<?php

namespace App\Http\Controllers\Admin;

use Excel;
use App\User;
use Carbon\Carbon;
use App\Exports\DataOrder;
use App\Model\Master\Order;
use App\Model\Master\Rating;
use App\Model\Master\Review;
use Illuminate\Http\Request;
use App\Model\GeneralSetting;
use App\Model\Master\Ewallet;
use App\Model\Master\MsTicket;
use App\Model\Master\OrderLog;
use App\Services\OrderService;
use App\Model\Master\MsAddress;
use App\Model\Master\MsProduct;
use App\Model\Master\ItemDetail;
use App\Model\Master\WebSetting;
use App\Model\Master\TopupWallet;
use App\Model\Master\GaleryImages;
use App\Model\Master\HistoryOrder;
use App\Model\Master\OrdersStatus;
use Illuminate\Support\Facades\DB;
use App\Model\Master\ServiceDetail;
use App\Model\Master\ConfrimPayment;
use App\Model\Master\EwalletHistory;
use App\Model\Master\EwalletPayment;
use App\Model\Master\MsPriceService;
use App\Model\Master\MsTicketDetail;
use App\Model\Technician\Technician;
use Illuminate\Support\Facades\Auth;
use App\Model\Master\RatingAndReview;
use App\Model\Master\SparepartDetail;
use App\Model\Master\TechnicianSaldo;
use App\Http\Controllers\ApiController;
use Illuminate\Database\QueryException;
use App\Http\Requests\OrderStoreRequest;
use App\Model\Master\TechnicianSchedule;
use Maatwebsite\Excel\Concerns\FromView;
use Yajra\DataTables\Facades\DataTables;

use App\Http\Controllers\AdminController;
use App\Model\Master\TechnicianSparepart;
use App\Http\Controllers\CustomerController;
use App\Services\Technician\TechnicianService;


class MasterOrderController extends AdminController
{
    public function index()
    {
        return $this
            ->setBreadcrumb('List Order Customer', '/Transaction List')
            ->viewAdmin('admin.order.index', [
                'title' => 'TRANSACTION ORDER LIST'
            ]);
    }

    public function garansiIndex()
    {
        return $this
            ->setBreadcrumb('List Order Customer', '/Transaction List')
            ->viewAdmin('admin.order.garansi.index', [
                'title' => 'TRANSACTION WARRANTY CLAIM ORDER LIST'
            ]);
    }

    public function reviewIndex()
    {
        return $this
            ->viewAdmin('admin.order.review.index', [
                'title' => 'TRANSACTION REVIEW ORDER LIST'
            ]);
    }

    public function create()
    {
        return $this
            ->setBreadcrumb('List Order Customer', '/Transaction List')
            ->viewAdmin('admin.order.index', [
                'title' => 'TRANSACTION ORDER LIST',
            ]);
    }

    public function service_detail($order_id)
    {
        // dd('asd');
        $garansi_setting = GeneralSetting::where('name', 'garansi_due_date')->first();

        $order = Order::where('id', $order_id)
            ->with([
                'user.info',
                'user.address',
                'user.ewallet',
                'service',
                'symptom',
                'ticket',
                'order_status',
                'sparepart_detail',
                'item_detail',
                'order_log.order_status',
                'service_detail.symptom',
                'service_detail.services_type',
                'service_detail.price_service',
                'service_detail.technician',
                'service_detail.technician.user'
            ])
            ->firstOrFail();
// return $order->garansi;
        $order->markAsRead();

        $showDataTransfer = ConfrimPayment::where('ms_order_id', $order_id)->first();
        $history_status_id = HistoryOrder::where('orders_id', $order_id)->get('orders_statuses_id', 'created_at')
            ->mapWithKeys(function ($item) {
                return [
                    $item['orders_statuses_id'] => Carbon::parse($item['created_at'])->format('Y-m-d h:i:s')
                ];
            })->toArray();

        $sparepart_details = $order->sparepart_detail;

        $item_details = $order->item_detail;

        $order_status = OrdersStatus::all();
        $order_log = $order->order_log;

        return $this
            ->viewAdmin('admin.order.service_detail', [
                'title' => 'DETAIL TRANSACTION ADMIN',
                'order' => $order,
                'order_id' => $order_id,
                'sparepart_details' => $sparepart_details,
                'item_details' => $item_details,
                'showDataTransfer' => $showDataTransfer,
                'order_status' => $order_status,
                'history_status_id' => $history_status_id,
                'garansi_setting' => $garansi_setting,
                'order_log' => $order_log,
                'fa_icon' => $this->getIconStatusOrder()
            ]);
    }



    public function datatables($query = null, Request $request)
    {
        if ($query == null) {
            $type_id = $request->type_id;
            $query   = Order::with([
                'user',
                'order_status',
                'service_detail.symptom',
                'service_detail.services_type',

            ]);
            if ($type_id != '') {
                $query = $query->where('orders_statuses_id', $type_id)->get();
            }
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }



    public function trashDatatables($trash = null, Request $request)
    {
        $trash = Order::with([
            'user',
            'symptom',
            'service',
            'order_status',
            'service_detail.symptom',
            'service_detail.services_type',
        ])->onlyTrashed()->get();

        return DataTables::of($trash)
            ->addIndexColumn()
            ->toJson();
    }

    public function updateOrderStatus(Request $request, OrderService $orderService)
    {

        if($request->order_status == 3) { // status Waiting Approval ;
            return $orderService
                    ->setOrder($request->order_id)
                    ->onlyJobPendingStatus() // hanya status 2 ;
                    ->changeToWaitingApproval($request);

        } else if($request->order_status == 4) { // status On Working ;
            return $orderService
                    ->setOrder($request->order_id)
                    ->onlyStatus([3])
                    ->changeToOnWorking($request);

        } else if($request->order_status == 9) { // status Processing ;
            return $orderService
                    ->setOrder($request->order_id)
                    ->onlyStatus([4])
                    ->changeToProcessing($request);

        } else if($request->order_status == 7) { // status Job Done ;
            return $orderService
                    ->setOrder($request->order_id)
                    ->onlyStatus([9])
                    ->changeToDone($request);

        } else if($request->order_status == 10) { // status Jobs Completed ;
            return $orderService
                    ->setOrder($request->order_id)
                    ->changeToComplate($request);

        } else if($request->order_status == 11) { // status Jobs Complaint ;
            return $orderService
                    ->setOrder($request->order_id)
                    ->changeToComplate($request);

        } else if($request->order_status == 5) { // status Cancel ;

        }

        if ($request->get('is_resend_email')) {
            // event(new NotifyEmail(Order::findOrFail($request->get('orders_id')))); every changes update by admin notify to customer email.
        }
        $this->saveHistoryOrder($request->order_id, $request->order_status);
        Order::where('id', $request->order_id)->first()->update([
            'orders_statuses_id' => $request->order_status,
        ]);
        $order = Order::with(
            'orderpayment',
            'user.ewallet',
            'service',
            'symptom',
            'sparepart_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician.user',
            'service_detail.price_service.service_type.symptom.services',
            'history_order.order_status',
            'order_status',
            'user'
        )->where('id', $request->order_id)->first();
        // return $order;
        if ($request->order_status == 5) {
            TechnicianSchedule::where('order_id', $order->id)->update(['is_deleted' => 1]);
        } else if ($request->order_status == 3) {
            TechnicianSchedule::where('order_id', $order->id)->update(['is_deleted' => 0]);
        }
        $orderService->orderLog(null, null, $order->id, $order->order_status->name);
        $this->sendEmailOrder($order);
    }

    public function updateOrderDetail(Request $request, $type_update)
    {
        $update = array();
        if ($type_update == 'address_edit') {
            $update = [
                'address' => $request->get('address')
            ];
        } else {
            $update = [
                'schedule' => $request->get('schedule'),
                'note' => $request->get('note'),
            ];
        }
        // return $update;
        return Order::where('id', $request->get('order_id'))->update($update);
    }

    public function updatePart(Request $request)
    {
    }

    public function find_technicians(Request $request, TechnicianService $technicianService)
    {
        return $technicianService->listQuery()
            ->where('technicians.status', 1)
            ->where('technicians.user_id', '!=', $request->customer_id)
            ->whereHas('price_services', function ($query) use ($request) {
                if ($request->product_group_id == null) {
                    $query->whereIn('ms_services_types_id', $request->service_type_id);
                } else {
                    $query->where('product_group_id', $request->product_group_id)
                        ->whereIn('ms_services_types_id', $request->service_type_id);
                }
            })
            ->where(function ($query) use ($request) {
                $query
                    ->whereHas('user', function ($x) use ($request) {
                        $x->where('name', 'like', '%' . $request->q . '%')
                            ->orWhere('email', 'like', '%' . $request->q . '%');
                    })
                    ->orWhereHas('user.address.city', function ($x) use ($request) {
                        $x->where('name', 'like', '%' . $request->q . '%');
                    });
            })->limit(10)->get();
    }

    public function getListTeknisi(User $user)
    {
        return $getTeknisi = $user->where('is_b2b_teknisi', 1)->limit(10)->get();
    }

    public function AdminCreateOrderSave(OrderStoreRequest $request, ApiController $apiController, OrderService $orderService)
    {
        return $apiController->successResponse($orderService->createFromAdmin($request), 'success', 201);
    }

    public function displayPriceService(Request $request)
    {
        // return $request->all();
        $query = MsPriceService::with(['service_type', 'technician'])->whereIn('ms_services_types_id', $request->get('service_type_id'))->where('technicians_id', $request->get('technician_id'))->get();
        $total = 0;
        $html = '';
        $html .= '<div class="price scroll-area-sm">';
        $html .= '<div class="scrollbar-container ps--active-y ps">';
        foreach ($query as $key => $val) {
            $total_harga = $val->value * $request->get('unit_service_type')[$key];
            $html .= '<p><b>' . $val->service_type->name . '</b></p>';
            $html .= '<div class="row">';
            $html .= '<div class="col-md-8">Rp. ' . number_format($val->value) . ' </div>';
            $html .= '<div class="col-md-4">x <b>' . $request->get('unit_service_type')[$key] . '</b></div>';
            $html .= '</div>';
            $total += $total_harga;
        }
        $html .= '<input type="hidden" id="total_service" value="' . $total . '">';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="text-right" style="margin-bottom: 15px;">
                    <label>Invoice: &nbsp;&nbsp;</label>
                    <button class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-sm btn-outline-danger"><i class="fa fa-file-pdf"></i></button>
                    <small>.pdf</small>
                 </div>';
        $html .= '<div class="btn-actions-pane-right" style="margin-left:-7px">
                    <h6><b>TOTAL&nbsp; : </b>Rp. ' . number_format($total) . '</h6>
                 </div>';
        return $html;
    }

    public function newOrder()
    {
        return $this
            ->viewAdmin('admin.order.statusTransaction.newOrder');
    }

    public function pending()
    {
        return $this
            ->viewAdmin('admin.order.statusTransaction.pendingRequest');
    }

    public function approveRequest()
    {
        return $this
            ->viewAdmin('admin.order.statusTransaction.approveRequest');
    }

    public function pendingPayment()
    {
        return $this
            ->viewAdmin('admin.order.statusTransaction.pendingPayment');
    }

    public function processing()
    {
        return $this
            ->viewAdmin('admin.order.statusTransaction.processing');
    }

    public function confrimPay()
    {
        return $this
            ->viewAdmin('admin.order.statusTransaction.confrimPayment');
    }

    public function cancel()
    {
        return $this
            ->viewAdmin('admin.order.statusTransaction.cancel');
    }

    public function done()
    {
        return $this
            ->viewAdmin('admin.order.statusTransaction.done');
    }

    // hapus sementara
    public function softDeletes($order_id)
    {
        $orders = Order::find($order_id);
        $orders->delete();

        // return redirect('/guru');
    }

    // Undo
    public function undo($order_id)
    {
        $order = Order::onlyTrashed()->where('id', $order_id);
        $order->restore();
    }

    // undo all

    public function undoAll()
    {
        $order = Order::onlyTrashed();
        $order->restore();
    }

    //delete only one
    public function deletedTrash($order_id)
    {
        DB::beginTransaction();
        try {
            $o = Order::onlyTrashed()->where('id', $order_id)->first();
            // return $o;
            $teknisiSaldo = TechnicianSaldo::where('order_id', $order_id);
            $order = Order::onlyTrashed()->where('id', $order_id);
            Review::where('order_id', $order_id)->delete();
            Rating::where('order_id', $order_id)->delete();
            $order->forceDelete();
            $teknisiSaldo->delete();
            if ($o != null) {
                if($o->rating_and_review_id != null){
                    RatingAndReview::where('id', $o->rating_and_review_id)->delete();
                }
            }
        } catch (QueryException $e) {
            DB::rollback();
            throw new \SendErrorMsgException($e->getMessage());
        }
    }

    public function deletedTrashAll()
    {
        DB::beginTransaction();
        try {
            $order = Order::onlyTrashed();
            $teknisiSaldo = TechnicianSaldo::where('order_id', $order->id);
            $order = Order::onlyTrashed()->where('id', $order->id);
            Review::where('order_id', $order->id)->delete();
            Rating::where('order_id', $order->id)->delete();
            $teknisiSaldo->delete();
            $order->forceDelete();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \SendErrorMsgException($e->getMessage());
        }
    }

    public function showTrash()
    {

        $trash = Order::onlyTrashed()->count();
        // return $trash;

        return $this
            ->viewAdmin('admin.order.trash', [
                'trash' => $trash
            ]);
    }

    public function showComplaintList(Request $request)
    {
        return $this
            ->viewAdmin('admin.order.complaintOrder');
    }

    public function datatablesComplaint()
    {
        $query = Order::with([
            // 'user',
            'unread_complaint',
            // 'symptom',
            // 'service',
            'order_status',
            // 'service_detail.symptom',
            // 'service_detail.services_type',
        ])->where('orders_statuses_id', 11);

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function showTransactionSuccess(Request $request)
    {
        $no = 1;
        $countTeknisiOrder  = Technician::get();

        $subset = $countTeknisiOrder->map(function ($teknisi) {
            return collect($teknisi->toArray())
                ->only(['id', 'name', 'email', 'total_pending_orders', 'total_success_orders'])
                ->all();
        });
        // return $subset;
        return $this
            ->viewAdmin('admin.order.orderSuccess', compact('countTeknisiOrder', 'no'));
    }

    public function detailSuccessOrder(Request $request, $id)
    {
        $date = date('m');
        $pendingTransfer = Technician::with(['pendingOrder',  'pendingOrder.price_services.service_type'])
            ->where('id', $request->id)
            ->orderBy('id', 'desc')
            ->first();
        $succcessTransfer = Technician::with(['doneOrder', 'getteknisi.teknisi'])
            ->where('id', $request->id)
            ->orderBy('id', 'desc')
            ->first();

        $getAllOrders = Technician::with(['allOrder', 'getteknisi.teknisi'])
            ->where('id', $request->id)
            ->orderBy('id', 'desc')
            ->first();
            
        $getOtherOrders = TechnicianSaldo::doesntHave('order')
                    ->where('technician_id', $request->id)
                    ->where('transfer_status', 0)
                    ->get();
        // return $getAllOrders;
        // return ['succcessTransfer' => $succcessTransfer];
        $no = 1;
        return $this
            ->viewAdmin('admin.order.detailOrderSuccess', compact('pendingTransfer', 'no', 'succcessTransfer', 'getAllOrders', 'getOtherOrders'));
    }

    public function allTransferdatatables(Request $request, $id)
    {
        // dd($request->user_id);
        // if ($query == null) {
        // $type_id = $request->type_id;
        // $user_id = $request->user_id;
        $query   = TechnicianSaldo::with(['order', 'teknisi'])->where('technician_id', $id);
        // if ($type_id != '') {
        //     $query = $query->where('transfer_status', $type_id)->get();
        // }
        // }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function statusUpdate(Request $request)
    {
        Order::whereIn('id', $request->ids)->update([
            'transfer_status' => 1,
        ]);

        TechnicianSaldo::whereIn('order_id', $request->ids)->update([
            'transfer_status' =>  1,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
    
    public function statusUpdateBalance(Request $request)
    {
        Order::whereIn('id', $request->ids)->update([
            'transfer_status' => 1,
        ]);

        TechnicianSaldo::whereIn('order_id', $request->ids)->update([
            'transfer_status' =>  1,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public function successTransfer(Request $request, $order_id)
    {
        // dd($order_id);
        Order::where('id', $order_id)->update([
            'transfer_status' => 1,
        ]);

        TechnicianSaldo::where('order_id', $order_id)->update([
            'transfer_status' =>  1,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
    
    public function successTransferBalance(Request $request, $order_id)
    {
        // dd($order_id);
        Order::where('id', $order_id)->update([
            'transfer_status' => 1,
        ]);

        TechnicianSaldo::where('order_id', $order_id)->update([
            'transfer_status' =>  1,
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    public function showListTopup(Request $request)
    {
        $no = 1;
        $countCustomerTopup  = Ewallet::with('history')->get();
        // return $countCustomerTopup;

        return $this->viewAdmin('admin.listTopup.showList', [
            'countCustomerTopup' => $countCustomerTopup,
            'no' => $no
        ]);
    }

    public function showListDetailTopup(Request $request, $id)
    {
        $no = 1;
        $detailTopupData  = EwalletHistory::where('ms_wallet_id', $id)
            ->with(['bank', 'user', 'voucer'])
            ->where('type_transaction_id', 0)
            ->get();
        // return $detailTopupData;

        return $this->viewAdmin('admin.listTopup.showDataListTopup', [
            'detailTopupData' => $detailTopupData,
            'no' => $no
        ]);
    }

    public function detailListTopup(Request $request, $id)
    {
        $getDataTopupWallet = EwalletHistory::with(['bank', 'user', 'voucer'])->where('id', $request->id)->firstOrFail();
        // return $getDataTopupWallet;
        return $this->viewAdmin('admin.listTopup.detailListTopup', [
            'getDataTopupWallet' => $getDataTopupWallet
        ]);
    }

    public function galeryUpload(Request $request, OrderService $orderService, ApiController $apiController)
    {
        $file = $request->file('filename');
        if ($file != null) {
            return $apiController->successResponse($orderService->uploadGalery($request), 'success', 201);
        } else {
            return $apiController->errorResponse('Error', 'Images cannot be null');
        }
    }

    public function previewDataOrder(Request $request)
    {
        // $nama_file = 'report_history_order_'.date('Y-m-d_H-i-s').'.xlsx';
        // return Excel::download(new DataOrder, $nama_file);
        // return (new InvoicesExport)->download('invoices.xlsx');
        // $type_id = $request->get('type_id');
        $nama_file = 'list_orders_' . date('Y-m-d_H-i-s') . '.xlsx';
        return (new DataOrder)->download($nama_file);
    }
}
