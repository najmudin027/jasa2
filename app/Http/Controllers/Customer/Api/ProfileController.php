<?php

namespace App\Http\Controllers\Customer\Api;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserAddressRequest;
use App\Http\Requests\UserInfoRequest;
use App\Http\Requests\UserProfileImageRequest;
use App\Model\Master\Ewallet;
use App\Model\Master\EwalletHistory;
use App\Model\Master\MsAddress;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class ProfileController extends ApiController
{
    /**
     * user info view
     */
    public function index(UserService $userService)
    {
        $resp = $userService->profile(Auth::id());
        return $this->successResponse($resp);
    }

    /**
     * user status info
     */
    public function statusInfo(UserService $userService)
    {
        $user = Auth::user();
        $msg = '';

        if ($user->status == 6) {
            $msg = 'One more step! Complete your registration';
        }

        if ($user->status == 7) {
            $msg = 'your application is in the process of verifying our admin';
        }

        if ($user->status == 8) {
            $msg = 'Your application does not meet the requirements';
        }

        $resp = [
            'info' => $msg
        ];
        return $this->successResponse($resp);
    }

    /**
     * update profile info
     */
    public function profileUpdateInfo(UserInfoRequest $request, UserService $userService)
    {
        return $this->successResponse($userService->createOrUpdateInfo($request, Auth::id()), $this->successUpdateMsg(), 200);
    }

    /**
     * create address
     */
    public function profileCreateAddress(UserAddressRequest $request, UserService $userService)
    {
        return $this->successResponse($userService->addressCreate($request, Auth::id()), $this->successStoreMsg(), 200);
    }

    /**
     * update profile image
     */
    public function updateProfileImage(UserProfileImageRequest $request, UserService $userService)
    {
        return $this->successResponse($userService->updateProfileImage($request, Auth::id()), $this->successUpdateMsg(), 200);
    }

    /**
     * create address
     */
    public function profileUpdateAddress(UserAddressRequest $request, UserService $userService)
    {
        return $this->successResponse($userService->updateAddress($request, Auth::id()), $this->successStoreMsg());
    }

    /**
     * create address
     */
    public function profileGetOneAddress($id)
    {
        $resp = MsAddress::with('city')->where('id', $id)->where('user_id', Auth::id())->firstOrFail();

        return $this->successResponse($resp, $this->successStoreMsg());
    }

    /**
     * main address
     */
    public function profileMainAddress(UserService $userService, $id)
    {
        return $this->successResponse($userService->mainAddress($id, Auth::id()), $this->successUpdateMsg());
    }

    /**
     * delete address
     */
    public function profileDeleteAddress(UserService $userService, $id)
    {
        $is_main = MsAddress::where('id', $id)->where('is_main', 1)->count();
        if ($is_main) {
            return $this->errorResponse(
                $is_main,
                'Address utama tidak bisa di delete',
                400
            );
        } else {
            return $this->successResponse($userService->deleteAddress($id, Auth::id()), $this->successDeleteMsg(), 204);
        }
    }

    /**
     * list address customer
     * as datatables
     */
    public function addressList()
    {
        $query = MsAddress::with('user', 'city', 'district', 'village', 'zipcode', 'types')->where('user_id', Auth::id());

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * list address customer
     */
    public function addressIndex()
    {
        $resp = MsAddress::with('user', 'city', 'district', 'village', 'zipcode', 'types')
            ->where('user_id', Auth::id())
            ->orderBy('id', 'desc')
            ->paginate(10);

        return $this->successResponse($resp);
    }

    /**
     * info wallet usar
     */
    public function walletInfo()
    {
        $fields = [
            'user_id',
            'nominal',
            'topup_code'
        ];
        $resp = Ewallet::select($fields)->where('user_id', Auth::id())->first();

        return $this->successResponse($resp);
    }

    /**
     * top up history list
     */
    public function walletTopupHistoryList()
    {
        $resp = EwalletHistory::with(['bank', 'voucer'])
            ->where('user_id', Auth::user()->id)
            ->where('type_transaction_id', 0)
            ->orderBy('id', 'desc')
            ->paginate(10);

        return $this->successResponse($resp);
    }
}
