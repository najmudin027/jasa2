<?php

namespace App\Http\Controllers\Customer\Api;

use App\Events\UserOfflineEvent;
use App\Events\UserOnlineEvent;
use App\Http\Controllers\ApiController;
use App\Http\Requests\ChatReplyRequest;
use App\Model\Master\Order;
use App\Services\Master\ChatService;
use App\Services\NotifikasiService;
use App\Services\OrderService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends ApiController
{
    /**
     * online user
     */
    public function onlineUser($user_id)
    {
        User::where('id', $user_id)->update([
            'is_online' => 1,
        ]);

        $user = User::where('id', $user_id)->first();

        broadcast(new UserOnlineEvent($user));
    }

    /**
     * offline user
     */
    public function offlineUser($user_id)
    {
        User::where('id', $user_id)->update([
            'is_online' => 0,
        ]);

        $user = User::where('id', $user_id)->first();

        broadcast(new UserOfflineEvent($user));
    }

    /**
     * list chat user
     */
    public function lists(ChatService $chatService)
    {
        return $this->successResponse(
            $chatService->users(Auth::id(), 10)
        );
    }

    /**
     * cari user di chat as html string
     */
    public function search(Request $request, ChatService $chatService)
    {
        return $this->successResponse($chatService->search($request));
    }

    /**
     * cari user di chat as json
     */
    public function searchUserInMyChat(Request $request, ChatService $chatService)
    {
        return $this->successResponse($chatService->searchUserInMyChat($request));
    }

    /**
     * balas chat
     */
    public function reply(ChatReplyRequest $request, ChatService $chatService)
    {
        return $this->successResponse($chatService->reply($request), $this->successStoreMsg());
    }

    /**
     * chat detail
     */
    public function detail($chat_id, ChatService $chatService)
    {
        return $this->successResponse($chatService->detail($chat_id), $this->successStoreMsg(), 200);
    }

    /**
     * chat detail
     * but as json
     */
    public function detailJson($chat_id, ChatService $chatService)
    {
        return $this->successResponse($chatService->detailJson($chat_id), $this->successStoreMsg(), 200);
    }

    /**
     * chat detail 
     */
    public function detailAdmin($user_id, $chat_id, ChatService $chatService)
    {
        return $this->successResponse($chatService->detailAdmin($chat_id, $user_id), $this->successStoreMsg(), 200);
    }

    /**
     * job complete
     */
    public function actionJobComplate($order_id, OrderService $orderService, Request $request)
    {
        $order = $orderService->setOrder($order_id)
            ->checkCustomerHasOrder()
            ->onlyJobDoneStatus($request->chat_message_id)
            ->changeToComplate($request);

        $response = $orderService->getContentChat();

        return $this->successResponse($response, $this->successStoreMsg(), 200);
    }

    /**
     * job work
     */
    public function actionJobWorking($order_id, OrderService $orderService, Request $request)
    {
        $order = $orderService->setOrder($order_id)
            ->checkTeknisiHasOrder()
            ->onlyJobProsesStatus($request->chat_message_id)
            ->changeToOnWorking($request);

        $response = $orderService->getContentChat();

        return $this->successResponse($response, $this->successStoreMsg(), 200);
    }

    /**
     * job done
     */
    public function actionJobDone($order_id, OrderService $orderService, Request $request)
    {
        $order = $orderService->setOrder($order_id)
            ->checkTeknisiHasOrder()
            ->onlyJobWorkingStatus($request->chat_message_id)
            ->changeToDone($request);

        $response = $orderService->getContentChat();

        return $this->successResponse($response, $this->successStoreMsg(), 200);
    }

    public function firebaseSaveToken(Request $request)
    {
        $user = User::findOrFail(Auth::id());
        $user->device_token = $request->fcm_token;
        $user->save();

        $dataPush = [
            'title' => 'firebaseSaveToken',
            'body' => 'body firebaseSaveToken',
        ];
        // (new NotifikasiService)->firebasePushNotifikasi(User::where('id', 95)->first(), $dataPush);

        if ($user) {
            return $this->successResponse($user, 'ser token updated', 200);
        }

        return $this->errorResponse($user, 'error');
    }

    public function firebaseSendPushNotification(Request $request)
    {
        $user = User::findOrFail($request->id);
        $data = [
            "to" => $user->device_token,
            "notification" =>
            [
                "title" => 'Web Push',
                "body" => "Sample Notification",
                "icon" => url('/logo.png')
            ],
        ];
        $dataString = json_encode($data);

        $serverKey = 'AAAAmuHrohs:APA91bFqsLpKCzxfm-8ft1-Uu1gFOlnC6TIjkVS543OdHEZ_GZ03lfviOguI1s3y0wu24keoVBc_eDfuJ8IZwXewwP1_7k_XMGnbvsl9Ik3LqoA1rKjJXml-VxuSxBsCOrE-5DxD9zQ3';

        $headers = [
            'Authorization: key=' . $serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        curl_exec($ch);

        return $this->successResponse($user, 'ok', 200);
    }
}
