<?php

namespace App\Http\Controllers\Customer\Web;

use App\Http\Controllers\AdminController;
use App\Model\Master\Chat;
use App\Model\Master\ChatMember;
use App\Model\Master\Order;
use App\Model\Technician\Technician;
use App\Services\Master\ChatService;
use App\User;
use App\Model\Master\WebSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChatController extends AdminController
{
    public function index(Request $request)
    {
        $chats = Chat::with([
            'last_message',
            'members_group.user.info',
            'members_group' => function ($query) {
                $query->where('user_id', '!=', Auth::id());
            }
        ])
            ->withCount('unread_message')
            ->whereHas('members_group', function ($query) {
                $query->where('user_id', Auth::id());
            })
            ->orderBy('updated_at', 'DESC')
            ->get();

        $to = User::where('id', $request->to)->first();
        $member = null;
        if (count($chats) > 0 && $to != null) {
            $member = ChatMember::where('user_id', $to->id)->where('user_id', '!=', Auth::id())->whereIn('chat_id', $chats->pluck('id'))->first();
        }

        return $this
            ->viewAdmin('admin.customer.chats.uji_coba', [
                'title' => 'Chat list',
                'auth'  => Auth::user(),
                'chats'  => $chats,
                'to'  => $to,
                'member'  => $member,
                'req'  => $request,
                'get_logo_astech' => $get_logo_astech = WebSetting::value('logo')
            ]);
    }

    public function firebaseIndex(Request $request)
    {
        $chats = Chat::with([
            'last_message',
            'members_group.user.info',
            'members_group' => function ($query) {
                $query->where('user_id', '!=', Auth::id());
            }
        ])
            ->withCount('unread_message')
            ->whereHas('members_group', function ($query) {
                $query->where('user_id', Auth::id());
            })
            ->orderBy('updated_at', 'DESC')
            ->get();

        $to = User::where('id', $request->to)->first();
        $member = null;
        if (count($chats) > 0 && $to != null) {
            $member = ChatMember::where('user_id', $to->id)->where('user_id', '!=', Auth::id())->whereIn('chat_id', $chats->pluck('id'))->first();
        }

        return $this
            ->viewAdmin('admin.customer.chats.firebase.index', [
                'title' => 'Chat list',
                'auth'  => Auth::user(),
                'chats'  => $chats,
                'to'  => $to,
                'member'  => $member,
                'req'  => $request,
            ]);
    }

    public function adminIndex(Request $request)
    {
        $room_no = $request->room_no;
        $chats = Chat::with([
            'last_message',
            'members_group.user.info',
            'members_group' => function ($query) {
                $query->where('user_id', '!=', Auth::id());
            }
        ])
            ->withCount('unread_message')
            ->whereHas('members_group', function ($query) {
                $query->where('user_id', Auth::id());
            })
            ->when($room_no, function ($query, $room_no) {
                $query->where('room_no', $room_no);
            })
            ->orderBy('updated_at', 'DESC')
            ->get();

        $to = User::where('id', $request->to)->first();
        $member = null;
        if (count($chats) > 0 && $to != null) {
            $member = ChatMember::where('user_id', $to->id)->where('user_id', '!=', Auth::id())->whereIn('chat_id', $chats->pluck('id'))->first();
        }

        return $this
            ->viewAdmin('admin.customer.chats.admin_index', [
                'title' => 'Chat list',
                'auth'  => Auth::user(),
                'chats'  => $chats,
                'to'  => $to,
                'member'  => $member,
                'req'  => $request,
            ]);
    }

    public function technician(Request $request, $user_id)
    {
        $techician = Technician::where('user_id', $user_id)->firstOrFail();
        $chats = Chat::with([
            'last_message',
            'members_group.user',
            'members_group' => function ($query) use ($user_id) {
                $query->where('user_id', '!=', $user_id);
            }
        ])
            ->withCount('unread_message')
            ->whereHas('members_group', function ($query) use ($user_id) {
                $query->where('user_id', $user_id);
            })
            ->orderBy('updated_at', 'DESC')
            ->get();

        return $this
            ->viewAdmin('admin.customer.chats.admin_list', [
                'title' => 'Chat list',
                'chats'  => $chats,
                'techician'  => $techician,
            ]);
    }

    public function show($id)
    {
        return $this
            ->viewAdmin('admin.customer.chat.detail', [
                'title' => 'DETAIL TICKET',
                'auth'  => Auth::user(),
            ]);
    }
}
