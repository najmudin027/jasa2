<?php

namespace App\Http\Controllers\Customer\Web;

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Model\Master\MsTicket;
use App\Model\Master\MsTicketDetail;
use App\Notifications\Ticket\TicketNewNotification;
use App\Notifications\Ticket\TicketReplyNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends AdminController
{
    public function index()
    {
        return $this
            ->viewAdmin('admin.customer.tiket.list', [
                'title' => 'LIST TICKET',
                'auth'  => Auth::user(),
            ]);
    }

    public function show($id)
    {
        MsTicketDetail::where('ms_ticket_id', $id)->where('user_id', '!=', Auth::id())->update([
            'read_at' => now()
        ]);

        return $this
            ->viewAdmin('admin.customer.tiket.detail', [
                'title' => 'DETAIL TICKET',
                'auth'  => Auth::user(),
                'tiket' => MsTicket::with(['user.info', 'ticket_details.user.info'])->where('id', $id)->firstOrFail(),
            ]);
    }
}
