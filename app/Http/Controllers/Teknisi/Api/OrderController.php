<?php

namespace App\Http\Controllers\Teknisi\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\AvailableHoursRequest;
use App\Http\Requests\ListTeknisiRequest;
use App\Http\Requests\OrderCustomerRequest;
use App\Http\Resources\OrderResource;
use App\Model\Master\Order;
use App\Model\Technician\Technician;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\Master\TechnicianSaldo;

class OrderController extends ApiController
{
    /**
     * statistic total review
     */
    public function statistic(Request $request)
    {
        $month = empty($request->month) ? date("m") : $request->month;

        $orderThisMonth = Order::whereHas('service_detail.technician', function ($query) use ($request) {
            $query->where('user_id', Auth::user()->id);
        })
            ->whereMonth('schedule', '=', $month)
            ->sum('grand_total');

        $orderCompleteThisMonth = Order::whereHas('service_detail.technician', function ($query) use ($request) {
            $query->where('user_id', Auth::user()->id);
        })
            ->where('orders_statuses_id', 10)
            ->whereMonth('schedule', '=', $month)
            ->sum('grand_total');

        $balance = TechnicianSaldo::where('technician_id', Auth::user()->technician->id)
            ->where('transfer_status', 0)
            ->sum('total');

        $response = [
            'pending_balance' => $balance,
            'total_order' => $orderThisMonth,
            'total_order_complete' => $orderCompleteThisMonth,
        ];

        return $this->successResponse($response);
    }

    /**
     * list order teknisi
     */
    public function index(Request $request)
    {
        $order_by       = empty($request->order_by) ? 'id' : $request->order_by;
        $sort_by        = in_array($request->sort_by, ['asc', 'desc']) ? $request->sort_by : 'desc';
        $limit       = empty($request->limit) ? 10 : $request->limit;
        $paginate = $request->paginate;

        $query = Order::with([
            'garansi',
            'orderpayment',
            'user.ewallet',
            'service',
            'symptom',
            'product_group',
            'sparepart_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician.user',
            'service_detail.price_service.service_type.symptom.services',
            'history_order.order_status',
            'order_status',
            'user',
            'payment_method'
        ]);

        if (!empty($request->order_status_id)) {
            $query->whereIn('orders_statuses_id', explode(',', $request->order_status_id));
        }

        if (!empty($request->klaim_garansi)) {
            $query->whereHas('garansi');
        }

        if (!empty($request->payment_type)) {
            $query->where('payment_type', $request->payment_type);
        }


        if (!empty($request->order_date_range)) {
            $range = explode(' - ', $request->order_date_range);
            $date_from = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $range[0])));
            $date_to = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $range[1])));
            $query->where('schedule', '>=', $date_from)
                ->where('schedule', '<=', $date_to);
        }

        $query->whereHas('service_detail.technician', function ($query) use ($request) {
            $query->where('user_id', Auth::user()->id);
        })->orderBy($order_by, $sort_by);

        if ($paginate == 'no_paginate') {
            $response = $query->limit($limit)->get();
        }else{
            $response = $query->paginate($limit);
        }

        return $this->successResponse($response);
    }

    /**
     * cari order teknisi
     */
    public function search(Request $request)
    {
        $order_by       = empty($request->order_by) ? 'id' : $request->order_by;
        $sort_by        = in_array($request->sort_by, ['asc', 'desc']) ? $request->sort_by : 'desc';
        $limit       = empty($request->limit) ? 10 : $request->limit;
        
        $query = Order::with([
            'garansi',
            'orderpayment',
            'user.ewallet',
            'service',
            'symptom',
            'product_group',
            'sparepart_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician.user',
            'service_detail.price_service.service_type.symptom.services',
            'history_order.order_status',
            'order_status',
            'user',
            'payment_method'
        ]);

        if (!empty($request->order_status_id)) {
            $query->whereIn('orders_statuses_id', explode(',', $request->order_status_id));
        }

        if (!empty($request->klaim_garansi)) {
            $query->whereHas('garansi');
        }

        if (!empty($request->payment_type)) {
            $query->where('payment_type', $request->payment_type);
        }


        if (!empty($request->order_date_range)) {
            $range = explode(' - ', $request->order_date_range);
            $date_from = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $range[0])));
            $date_to = date("Y-m-d H:i:s", strtotime(str_replace('/', '-', $range[1])));
            $query->where('schedule', '>=', $date_from)
                ->where('schedule', '<=', $date_to);
        }

        $query->whereHas('service_detail.technician', function ($query) use ($request) {
            $query->where('user_id', Auth::user()->id);
        })->orderBy($order_by, $sort_by);

        return $this->successResponse($query->paginate($limit));
    }

    /**
     * detail order
     */
    public function show($order_id)
    {
        $order = Order::where('id', $order_id)
            ->with([
                'chat',
                'product_group',
                'user.info',
                'user.masterwallet',
                'orderpayment.orderhistory',
                'order_status',
                'sparepart_detail',
                'item_detail.inventory.warehouse',
                'tmp_item_detail.inventory.warehouse',
                'service_detail.technician',
                'service_detail.technician.sum_total_rating',
                'service_detail.technician' => function($query){
                    $query->withCount('order_completes');
                    $query->withCount('count_total_review');
                },
                'garansi',
                'symptom_code',
                'repair_code',
                'rating_and_review',
                'order_media_problem',
                'history_order.order_status'
            ])
            ->whereHas('service_detail.technician', function ($query) {
                $query->where('user_id', Auth::user()->id);
            })
            ->firstOrFail();
        // return date('Y-m-d H:i:s', strtotime('2021-05-26T03:52:41.000000Z'));
        $resp = new OrderResource($order);

        return $this->successResponse($resp);
    }

    /**
     * list teknisi saat mau order
     */
    public function listTeknisi(ListTeknisiRequest $request)
    {
        $id_service_type = $request->service_type_id;
        $sort_by = $request->sort_by;
        $order_by = $request->order_by == null ? 'desc' : $request->order_by;

        $callbackServiceType = function ($query) use ($id_service_type) {
            $query->whereIn('ms_price_services.id', $id_service_type);
        };

        $technicians = Technician::select([
            'technicians.id',
            'technicians.user_id',
            DB::raw('SUM(ms_price_services.value) AS total_service_price'),
            DB::raw('COUNT(ratings.id) AS jumlah_rating'),
            DB::raw('SUM(ratings.value) AS total_rating'),
            DB::raw('COUNT(reviews.id) AS total_review'),
        ])
            ->with('user', 'price_services')
            ->where('technicians.user_id', '!=', Auth::id())
            ->where('technicians.status', 1)
            ->join('users', 'users.id', '=', 'technicians.user_id')
            ->leftJoin('ratings', 'ratings.technician_id', '=', 'technicians.id')
            ->leftJoin('reviews', 'reviews.technician_id', '=', 'technicians.id')
            ->join('ms_price_services', $callbackServiceType)
            ->groupBy('technicians.id');

        if ($sort_by != null) {
            if ($sort_by == 'name') {
                $technicians = $technicians->orderBy('users.name', $order_by);
            }

            if ($sort_by == 'price') {
                $technicians = $technicians->orderBy('total_service_price', $order_by);
            }
        }

        return $technicians->paginate(5);
    }

    /**
     * save order
     */
    public function store(OrderCustomerRequest $request, OrderService $orderService)
    {
        return $this->successResponse($orderService->createFromCustomer($request));
    }

    /**
     * available hours teknisi
     */
    public function getHoursAvailable(AvailableHoursRequest $request, OrderService $orderService)
    {
        $hours = $orderService->getHoursAvailable($request);
        $response = $hours['all_jam'];

        $dates = [];
        foreach ($hours['jam_available'] as $key => $value) {
            $dates[str_replace(':00', '', $value)] = [
                'date' => $request->schedule . ' ' . $value,
                'available' => true,
                'hold' => false,
                'not_available' => false,
            ];
        }
        foreach ($hours['jam_teknisi_hold'] as $key => $value) {
            $dates[str_replace(':00', '', $value)] = [
                'date' => $request->schedule . ' ' . $value,
                'available' => false,
                'hold' => true,
                'not_available' => false,
            ];
        }
        foreach ($hours['jam_disable'] as $key => $value) {
            $dates[str_replace(':00', '', $value)] = [
                'date' => $request->schedule . ' ' . $value,
                'available' => false,
                'hold' => false,
                'not_available' => true,
            ];
        }
        return $this->successResponse($dates);
    }

    public function garansiAvail()
    {
        $teknisi = Technician::where('user_id', Auth::id())->firstOrFail();

        $count = Order::whereHas('garansi', function ($query) {
            $query->where('status', '!=', 'done')->where('customer_can_reply', 1);
        })
            ->whereHas('service_detail.technician', function ($query) use ($teknisi) {
                $query->where('technicians_id', $teknisi->id);
            })->count();

        return $this->successResponse([
            'count' => $count
        ], 'ok');
    }
}
