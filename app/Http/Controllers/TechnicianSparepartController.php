<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Technician\Technician;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AdminController;
use App\Model\Master\TechnicianSparepart;

class TechnicianSparepartController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master Technicians Sparepart'
        ])
        ->setBreadcrumb('List Technicians Sparepart', '/admin/technician-sparepart/show')
        ->viewAdmin('admin.teknisi.technician_sparepart.index');
    }


    public function create()
    {
        return $this->setData([
            'title' => 'Create Technicians Sparepart'
        ])
        ->setBreadcrumb('List services', '/admin/technician-sparepart/show')
        ->viewAdmin('admin.teknisi.technician_sparepart.create');
    }

    public function update($id)
    {
        $getData = TechnicianSparepart::where('id', $id)->first();
        // return $getDataservices;
        $technicians_id = Technician::where('user_id', Auth::user()->id)->value('id');
        $tech_spareparts = TechnicianSparepart::where('technician_id', $technicians_id);
        return $this->setData([
            'title' => 'Update Technicians Sparepart',
            'getData' => $getData,
            'technicians_id' => $technicians_id,
            'tech_spareparts' => $tech_spareparts
        ])
        ->setBreadcrumb('List services', '/admin/technician-sparepart/show')
        ->viewAdmin('admin.teknisi.technician_sparepart.update');
    }

}
