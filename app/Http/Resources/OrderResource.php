<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'id' => $this->id,
            'code' => $this->code,
            'service_name' => $this->service_name,
            'symptom_name' => $this->symptom_name,
            'product_group_name' => $this->product_group_name,
            'repair_desc' => $this->repair_desc,
            'payment_type' => $this->payment_type,
            
            'note' => $this->note,
            'note_complaint' => $this->note_complaint,
            'address' => $this->address,
            'bill_to' => $this->bill_to,

            'date_complete' => $this->date_complete,
            'schedule' => $this->schedule,
            'estimation_hours' => $this->estimation_hours,

            'grand_total' => $this->grand_total,
            'repair_code' => $this->repair_code,
            'rating_and_review' => $this->rating_and_review,
            'symptom_code' => $this->symptom_code,
            'garansi' => $this->garansi,
            'status' => $this->order_status,
            'histories' => $this->history_order,
            'chat' => $this->chat,
            'spareparts' => $this->sparepart_detail,
            'items' => $this->item_detail,
            'items_tmp' => $this->tmp_item_detail,
            'product_group' => $this->product_group,
            'services' => $this->service_detail,
            'attachments' => $this->order_media_problem,
            'customer' => $this->customer,
        ];
    }
}
