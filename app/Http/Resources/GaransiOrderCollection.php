<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class GaransiOrderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = $this->collection;
        
        return [
            'id' => $collection->id,
            'user_id' => $collection->user_id,
            'order_id' => $collection->order_id,
        ];
    }
}
