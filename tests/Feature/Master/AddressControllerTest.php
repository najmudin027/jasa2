<?php

namespace Tests\Feature\Master;

use App\Model\Master\MsAddress;
use App\Model\Master\MsCity;
use App\Model\Master\MsDistrict;
use App\Model\Master\MsVillage;
use App\Model\Master\MsZipcode;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AddressControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return [
            'id',
            'address',
            'latitude',
            'longitude',
            'phone1',
            'phone2',
            'is_main',

            'user_id',
            'ms_city_id',
            'ms_district_id',
            'ms_village_id',
            'ms_zipcode_id',

            'created_at',
            'updated_at'
        ];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'address' => $this->faker->address,
            'phone1' => $this->faker->phoneNumber,
            'phone2' => $this->faker->phoneNumber,
            'user_id' => factory(User::class)->create()->id,
            'ms_city_id' => factory(MsCity::class)->create()->id,
            'ms_district_id' => factory(MsDistrict::class)->create()->id,
            'ms_village_id' => factory(MsVillage::class)->create()->id,
            'ms_zipcode_id' => factory(MsZipcode::class)->create()->id,
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(MsAddress::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('addresses.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(MsAddress::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('addresses.datatables.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('addresses.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $address = factory(MsAddress::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('addresses.show.api', ['address' => $address->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('addresses.show.api', ['address' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }


    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $address = factory(MsAddress::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('addresses.update.api', ['address' => $address->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_set_main()
    {
        $address = factory(MsAddress::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('addresses.setMain.api', ['address' => $address->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $address = factory(MsAddress::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('addresses.destroy.api', ['address' => $address->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('addresses.destroy.api', ['address' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
