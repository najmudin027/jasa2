<?php

namespace Tests\Feature\Master;

use App\Model\Master\MsCountry;
use App\Model\Master\MsProvince;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CountryControllerTest extends TestCase
{
    use WithFaker;

    public function resourceArray()
    {
        return ['id', 'name', 'code', 'created_at', 'updated_at'];
    }

    /** @test */
    public function test_index()
    {
        factory(MsCountry::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('countries.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(MsCountry::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('countries.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_store()
    {
        $playload = [
            'name' => $this->faker->name,
            'code' => $this->faker->word,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('countries.store.api'), $playload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('countries.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $country = factory(MsCountry::class)->create();

        $playload = [
            'name' => $country->name,
            'code' => $this->faker->word,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('countries.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_show()
    {
        $country = factory(MsCountry::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('countries.show.api', ['country' => $country->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('countries.show.api', ['country' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update()
    {
        $country = factory(MsCountry::class)->create();

        $playload = [
            'name' => $this->faker->name,
            'code' => $this->faker->word,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('countries.update.api', ['country' => $country->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $playload = [
            'name' => $this->faker->name,
            'code' => $this->faker->word,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('countries.update.api', ['country' => 'test']), $playload)
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_sukses()
    {
        $country = factory(MsCountry::class)->create();

        $playload = [
            'name' => $country->name,
            'code' => $this->faker->word,
            'id' => $country->id,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('countries.update.api', ['country' => $country->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_fail()
    {
        $country = factory(MsCountry::class)->create();

        $playload = [
            'name' => $country->name,
            'code' => $this->faker->word,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('countries.update.api', ['country' => $country->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }


    /** @test */
    public function test_update_required_name()
    {
        $country = factory(MsCountry::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('countries.update.api', ['country' => $country->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $country = factory(MsCountry::class)->create();

        $playload = [
            'name' => $country->name,
            'code' => $this->faker->word,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('countries.update.api', ['country' => $country->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_destroy()
    {
        $country = factory(MsCountry::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('countries.destroy.api', ['country' => $country->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_has_country()
    {
        $country = factory(MsCountry::class)->create();

        factory(MsProvince::class)->create([
            'ms_country_id' => $country->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('countries.destroy.api', ['country' => $country->id]))
            ->assertStatus(400);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('countries.destroy.api', ['country' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
