<?php

namespace Tests\Feature\Master;

use App\Model\Master\MsDistrict;
use App\Model\Master\MsZipcode;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ZipcodeControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'zip_no', 'ms_district_id', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'zip_no' => $this->faker->name,
            'ms_district_id' => factory(MsDistrict::class)->create()->id
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(MsZipcode::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('zipcode.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(MsZipcode::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('zipcode.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(MsZipcode::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('zipcode.select2.api'))
            ->assertStatus(200);
    }


    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('zipcode.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_distrik_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('zipcode.store.api'), $this->getPlayload([
                'ms_district_id' => 0
            ]))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_store_required_zip_no()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('zipcode.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'zip_no',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $zipcode = factory(MsZipcode::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('zipcode.store.api'), $this->getPlayload(['zip_no' => $zipcode->zip_no]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'zip_no',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $zipcode = factory(MsZipcode::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('zipcode.show.api', ['zipcode' => $zipcode->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('zipcode.show.api', ['zipcode' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }


    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $zipcode = factory(MsZipcode::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('zipcode.update.api', ['zipcode' => $zipcode->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_distrik_not_found()
    {
        $zipcode = factory(MsZipcode::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('zipcode.update.api', ['zipcode' => $zipcode->id]), $this->getPlayload([
                'ms_district_id' => 0
            ]))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('zipcode.update.api', ['zipcode' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_sukses()
    {
        $zipcode = factory(MsZipcode::class)->create();

        $playload = $this->getPlayload([
            'id' => $zipcode->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('zipcode.update.api', ['zipcode' => $zipcode->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_fail()
    {
        $zipcode = factory(MsZipcode::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('zipcode.update.api', ['zipcode' => $zipcode->id]),
                $this->getPlayload([
                    'zip_no' => $zipcode->zip_no
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_zip_no()
    {
        $zipcode = factory(MsZipcode::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('zipcode.update.api', ['zipcode' => $zipcode->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'zip_no',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $zipcode = factory(MsZipcode::class)->create();

        $playload = $this->getPlayload([
            'zip_no' => $zipcode->zip_no
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('zipcode.update.api', ['zipcode' => $zipcode->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'zip_no',
                ]
            ]);
    }


    # test delete
    # 
    # ==============
    /** @test */
    public function test_destroy()
    {
        $zipcode = factory(MsZipcode::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('zipcode.destroy.api', ['zipcode' => $zipcode->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('zipcode.destroy.api', ['zipcode' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
