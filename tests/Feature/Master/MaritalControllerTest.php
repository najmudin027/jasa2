<?php

namespace Tests\Feature\Master;

use App\Model\Master\Marital;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MaritalControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'status', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'status' => $this->faker->name,
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(Marital::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('maritals.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(Marital::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('maritals.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(Marital::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('maritals.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('maritals.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('maritals.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'status',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $marital = factory(Marital::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('maritals.store.api'), $this->getPlayload(['status' => $marital->status]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'status',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $marital = factory(Marital::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('maritals.show.api', ['marital' => $marital->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('maritals.show.api', ['marital' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }


    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $marital = factory(Marital::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('maritals.update.api', ['marital' => $marital->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('maritals.update.api', ['marital' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_sukses()
    {
        $marital = factory(Marital::class)->create();

        $playload = $this->getPlayload([
            'id' => $marital->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('maritals.update.api', ['marital' => $marital->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_fail()
    {
        $marital = factory(Marital::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('maritals.update.api', ['marital' => $marital->id]),
                $this->getPlayload([
                    'status' => $marital->status
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name()
    {
        $marital = factory(Marital::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('maritals.update.api', ['marital' => $marital->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'status',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $marital = factory(Marital::class)->create();

        $playload = $this->getPlayload([
            'status' => $marital->status
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('maritals.update.api', ['marital' => $marital->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'status',
                ]
            ]);
    }

    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $marital = factory(Marital::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('maritals.destroy.api', ['marital' => $marital->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('maritals.destroy.api', ['marital' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
