<?php

namespace Tests\Feature\Master;

use App\Model\Master\MsCity;
use App\Model\Master\MsProvince;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CityControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'name', 'ms_province_id', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'name' => $this->faker->name,
            'ms_province_id' => factory(MsProvince::class)->create()->id
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('cities.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('cities.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('cities.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('cities.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_province_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('cities.store.api'), $this->getPlayload([
                'ms_province_id' => 0
            ]))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('cities.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $city = factory(MsCity::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('cities.store.api'), $this->getPlayload(['name' => $city->name]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $city = factory(MsCity::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('cities.show.api', ['city' => $city->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('cities.show.api', ['city' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }


    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $city = factory(MsCity::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('cities.update.api', ['city' => $city->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_provinsi_not_found()
    {
        $city = factory(MsCity::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('cities.update.api', ['city' => $city->id]), $this->getPlayload([
                'ms_province_id' => 0
            ]))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('cities.update.api', ['city' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_sukses()
    {
        $city = factory(MsCity::class)->create();

        $playload = $this->getPlayload([
            'id' => $city->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('cities.update.api', ['city' => $city->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_fail()
    {
        $city = factory(MsCity::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('cities.update.api', ['city' => $city->id]),
                $this->getPlayload([
                    'name' => $city->name
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name()
    {
        $city = factory(MsCity::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('cities.update.api', ['city' => $city->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $city = factory(MsCity::class)->create();

        $playload = $this->getPlayload([
            'name' => $city->name
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('cities.update.api', ['city' => $city->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $city = factory(MsCity::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('cities.destroy.api', ['city' => $city->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('cities.destroy.api', ['city' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
