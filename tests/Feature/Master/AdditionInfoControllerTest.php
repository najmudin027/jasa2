<?php

namespace Tests\Feature\Master;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class AdditionInfoControllerTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function test_store()
    {
        $playload = [
            'first_name' => $this->faker->name,
            'last_name' => $this->faker->name,
            'date_of_birth' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'place_of_birth' => $this->faker->city,
            'gender' => rand(0, 1),
            'picture' => UploadedFile::fake()->image('avatar.jpg')->size(100),
        ];
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('additional_info.store.api'), $playload)
            ->assertStatus(201);
    }

    /** @test */
    public function test_store_only_firstname()
    {
        $playload = [
            'first_name' => $this->faker->name,
        ];
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('additional_info.store.api'), $playload)
            ->assertStatus(201);
    }

    /** @test */
    public function test_store_no_data_send()
    {
        $playload = [];
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('additional_info.store.api'), $playload)
            ->assertStatus(422);
    }

    /** @test */
    public function test_store_image_over_size()
    {
        $playload = [
            'first_name' => $this->faker->name,
            'last_name' => $this->faker->name,
            'date_of_birth' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'place_of_birth' => $this->faker->city,
            'gender' => rand(0, 1),
            'picture' => UploadedFile::fake()->image('avatar.jpg')->size(2000),
        ];
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('additional_info.store.api'), $playload)
            ->assertStatus(422);
    }
}
