<?php

namespace Tests\Feature;

use App\Services\UserService;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function can_create_user()
    {
        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => 'password'
        ];

        $user = (new UserService())->create($data);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($data['name'], $user->name);
        $this->assertEquals($data['email'], $user->email);
    }

    /** @test */
    public function can_update_user()
    {
        $user = factory(User::class)->create();

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
        ];

        $update = (new UserService())->update($data, $user);

        $this->assertTrue($update);
    }

    /** @test */
    public function can_delete_user()
    {
        $user = factory(User::class)->create();

        $delete = $user->delete();

        $this->assertTrue($delete);
    }

    /** @test */
    public function test_index_user()
    {
        factory(User::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('users.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => ['id', 'name', 'email', 'api_token', 'auth_token', 'created_at', 'updated_at']
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_user()
    {
        factory(User::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('users.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_store_user()
    {
        $playload = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'role_id' => Role::first()->id,
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('users.store.api'), $playload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'token',
                    'user' => [
                        'id',
                        'name',
                        'email',
                        'api_token',
                        'updated_at',
                        'created_at',
                        'roles'
                    ]
                ]
            ]);
    }

    /** @test */
    public function test_store_empty_value_user()
    {
        $playload = [
            'name' => '',
            'email' => '',
            'role_id' => Role::first()->id,
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('users.store.api'), $playload)
            ->assertStatus(422);
    }

    /** @test */
    public function test_store_not_found_role_user()
    {
        $playload = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'role_id' => 'test',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('users.store.api'), $playload)
            ->assertStatus(404);
    }

    /** @test */
    public function test_store_user_required_field()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('users.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                    'email',
                    'role_id',
                    'password',
                ]
            ]);
    }

    /** @test */
    public function test_show_user()
    {
        $user = factory(User::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('users.show.api', ['id' => $user->id]))
            ->assertStatus(200);
    }

    /** @test */
    public function test_show_not_found_user()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('users.show.api', ['id' => 'test']))
            ->assertStatus(404);
    }

    /** @test */
    public function test_update_user()
    {
        // create user
        $user = (new UserService)->create([
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'role_id' => Role::first()->id,
            'password' => 'password',
        ]);

        $playload = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'role_id' => Role::first()->id,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('users.update.api', ['id' => $user->id]), $playload)
            ->assertStatus(200);
    }

    /** @test */
    public function test_update_user_require_field()
    {
        // create user
        $user = (new UserService)->create([
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'role_id' => Role::first()->id,
            'password' => 'password',
        ]);

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('users.update.api', ['id' => $user->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                    'email',
                    'role_id',
                ]
            ]);
    }

    /** @test */
    public function test_update_not_found_user()
    {
        $playload = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'role_id' => Role::first()->id,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('users.update.api', ['id' => 'test']), $playload)
            ->assertStatus(404);
    }

    /** @test */
    public function test_update_role_not_found_user()
    {
        $playload = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'role_id' => 'test',
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('users.update.api', ['id' => 'test']), $playload)
            ->assertStatus(404);
    }

    /** @test */
    public function test_destroy_user()
    {
        $user = factory(User::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('users.destroy.api', ['id' => $user->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found_user()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('users.destroy.api', ['id' => 'test']))
            ->assertStatus(404);
    }
}
