<?php

namespace Tests\Feature\Product;

use App\Model\Product\ProductAttribute;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductAttributeControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'name', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'name' => $this->faker->name,
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(ProductAttribute::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_attribute.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(ProductAttribute::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_attribute.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(ProductAttribute::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_attribute.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_attribute.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_store_batch()
    {
        $playload = [
            'name' => $this->faker->name,
            'childs' => ['merah'],
            'color_code' => ['merah'],
            'hex_rgb' => ['#45555'],
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_attribute.store.api'), $playload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_attribute.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $attribute = factory(ProductAttribute::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_attribute.store.api'), $this->getPlayload(['name' => $attribute->name]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $attribute = factory(ProductAttribute::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('product_attribute.show.api', ['attribute' => $attribute->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('product_attribute.show.api', ['attribute' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }


    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $attribute = factory(ProductAttribute::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('product_attribute.destroy.api', ['attribute' => $attribute->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('product_attribute.destroy.api', ['attribute' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
