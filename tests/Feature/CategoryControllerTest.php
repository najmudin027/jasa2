<?php

namespace Tests\Feature;

use App\Model\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function test_index_category()
    {
        factory(Category::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('categories.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => ['id', 'name', 'slug', 'created_at', 'updated_at']
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list_category()
    {
        factory(Category::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('categories.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_store_category()
    {
        $playload = [
            'name' => $this->faker->word
        ];
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('categories.store.api'), $playload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id', 'name', 'slug', 'created_at', 'updated_at'
                ],
            ]);
    }

    /** @test */
    public function test_store_with_parent_category()
    {
        $category = factory(Category::class)->create();

        $playload = [
            'name' => $this->faker->word,
            'parent_id' => $category->id
        ];
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('categories.store.api'), $playload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id', 'name', 'slug', 'created_at', 'updated_at'
                ],
            ]);
    }

    /** @test */
    public function test_store_with_parent_not_found_category()
    {
        $playload = [
            'name' => $this->faker->word,
            'parent_id' => 'test'
        ];
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('categories.store.api'), $playload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id', 'name', 'slug', 'created_at', 'updated_at'
                ],
            ]);
    }

    /** @test */
    public function test_store_category_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('categories.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_show_category()
    {
        $category = factory(Category::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('categories.show.api', ['category' => $category->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id', 'name', 'slug', 'created_at', 'updated_at'
                ],
            ]);
    }

    /** @test */
    public function test_show_not_found_category()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('categories.show.api', ['category' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_category()
    {
        $category = factory(Category::class)->create();

        $playload = [
            'name' => $this->faker->word
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('categories.update.api', ['category' => $category->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_with_parent_category()
    {
        $category = factory(Category::class)->create();
        $parent = factory(Category::class)->create();

        $playload = [
            'name' => $this->faker->word,
            'parent_id' => $parent->id
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('categories.update.api', ['category' => $category->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_with_parent_not_found_category()
    {
        $category = factory(Category::class)->create();

        $playload = [
            'name' => $this->faker->word,
            'parent_id' => 'test'
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('categories.update.api', ['category' => $category->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found_category()
    {
        $playload = [
            'name' => $this->faker->word
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('categories.update.api', ['category' => 'test']), $playload)
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_category_required_name()
    {
        $category = factory(Category::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('categories.update.api', ['category' => $category->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_destroy_category()
    {
        $category = factory(Category::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('categories.destroy.api', ['category' => $category->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_category_has_childs()
    {
        $category = factory(Category::class)->create();
        factory(Category::class)->create([
            'parent_id' => $category->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('categories.destroy.api', ['category' => $category->id]))
            ->assertStatus(400);
    }

    /** @test */
    public function test_destroy_not_found_category()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('categories.destroy.api', ['category' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
